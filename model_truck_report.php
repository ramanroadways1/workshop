<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">
     <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Jobcard And Inspection</h3>

        </div>

          <div style="overflow-x:auto;">
              <table class="table table-hover" border="4";>

                <tbody>
                  <tr class="table-active">
                        <td>Truck Numbur</td>
                        <td>Truck Driver</td>
                        <td>Inspection No</td>
                        <td>JobCard Numbur</td>
                        <td >Mistry</td>
                        <td>Job Card Creat Date</td>
                         
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              
            
              $demo=$_POST['tno'];

            
              $show1 = "SELECT inspection_record.date1,inspection_record.job_card_work,inspection_record.mistry,inspection_record.job_card_no,inspection_record.inspection_no,inspection_record.truck_driver,own_truck.tno,own_truck.model  from dairy.own_truck 
              inner join rrpl_workshop.inspection_record 
              on own_truck.tno = inspection_record.truck_no1 where own_truck.tno='$demo' group by tno";
              $result = $conn->query($show1);


              if ($result->num_rows > 0) {
                 
                  while($row = $result->fetch_assoc()) {
                  
                    $tno = $row['tno'];
                    $truck_driver = $row['truck_driver'];
                    $inspection_no = $row['inspection_no'];
                    $job_card_no = $row['job_card_no'];
                    $mistry = $row['mistry'];
                    $date1 = $row['date1'];
                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                      <td><?php echo $tno; ?></td>
                      <td><?php echo $truck_driver; ?></td>
                      <td><?php echo $inspection_no; ?></td>
                      <td><?php echo $job_card_no; ?></td>
                      <td><?php echo $mistry; ?></td>
                      <td><?php echo $date1; ?></td>
                  

                    </tr>
                  </div>
                </div>
                <?php 
                
                    }  echo "<tr>

                  </tr>";
                  } else {
                       echo "Truck Insperction and Jobcards Not Created";
                    exit();
                  }
                  ?>

              </table>
             <center><a href="model_truck.php">
    <button type="button">
        Back
    </button>
</a>
</center> 
            </div>

          </div>
        </section>

      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
