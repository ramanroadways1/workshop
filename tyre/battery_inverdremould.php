<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>

<?php include("header.php"); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<?php include("aside_main.php"); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
       <a href='upload_invice_on_send_rmd.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Upload Invoice On Casing Send</a>
      <h1>Receive Tyre From Casing</h1>
    </section>
    
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                        <?php  
                        
                           $username = $_SESSION['username']; 

                            $query = "SELECT  unique_no,count(tyre_no) as tyre_no, timestamp ,vendor,vendor_office FROM  final_outward_stock WHERE receive_or_not = '0'  and status='send_remould'  and  office='$username' group by unique_no";

                            $result = mysqli_query($conn,$query);
                             $id_customer = 0;

                             if(mysqli_num_rows($result) > 0){
                        ?>

                                    <thead>  
                                        <tr>  
                                            <th style="display: none;" scope="row">Id</th>
                                            <td style="width: 20px;">GRN Number</td>
                                            <td>Vendor Name</td>
                                            <td>Total Tyre</td>
                                            <td>Stock In Date</td>
                                        </tr>  
                                    </thead>  

                                     <?php


                                     $i = 1;

                                    while($row = mysqli_fetch_array($result)){
                                        
                                          $grn_no=$row['unique_no'];
                                          $tyre_count=$row['tyre_no'];
                                          $timestamp = $row['timestamp'];
                                          $grn_no=$row['unique_no'];
                                          $vendor=$row['vendor'];

                                      ?>
                            <tr> 
                                <td>
                              <form method="POST" action="show_receive_remould.php">
                                   <input type="submit" class="btn btn-primary btn-sm" name="grn_no"  value="<?php echo $grn_no?>" >
                                      <input type="hidden" name="grn_no" id="grn_no<?php echo $grn_no?>" value="<?php echo $grn_no?>">
                                </td>
                              </form>
                                    <td><?php echo $vendor; ?><input type="hidden" name="vendor[]"  id="tyre_count<?php echo $vendor; ?>" value='<?php echo $vendor; ?>'>
                                    </td>
                                    <td> <?php echo $tyre_count; ?><input type="hidden" name="tyre_count[]"  id="tyre_count<?php echo $id_customer; ?>" value='<?php echo $tyre_count; ?>'>
                                    </td>
                                    <td> <?php echo $timestamp; ?><input type="hidden" name="stock_in_date[]"  id="grn_date<?php echo $id_customer; ?>" value='<?php echo $timestamp; ?>'>
                                   </td>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                       echo "No Data";
                  }
                  ?>  
                </table>
                 <br>
              </div>  
            </div> 
      </div>
      <div id="result"></div> 
    </div>
    </section>
  <br><br><br>
  </div>
<?php include("footer.php");?>
</div>
</body>
</html>
