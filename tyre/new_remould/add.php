<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<?php include("../aside_main.php"); ?>
<script>  
  $(document).ready(function(){  
    $.datepicker.setDefaults({  
      dateFormat: 'dd-mm-yy'   
    });  
    $(function(){  
      $("#challan_date").datepicker(); 
      $("#invoice_date").datepicker();
      $('#product_month').datepicker({ dateFormat: 'mm/yy' }).val();   
    });  
  });

</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Tyre GRN</h1>
    </section>

    <section class="content">
     
      <div class="box box-info">
        <form method="post" action="insert_final_remould_stock.php" id="main_form">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <h3>Basic Details</h3>
            </div>
          </div>
          <div class="col-md-6"> 
            <div class="row">
              <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>
              <div class="col-md-4"> <label><font color="red">*</font>Vendor:</label></div>
              <div class="col-md-8">
                <input type="text" name="vendor" id="vendor" class="form-control" style="width: 338px;" placeholder="Vendor" required="required" />
              </div>
            </div><br>  
            <div class="row">
              <div class="col-md-4"> <label><font color="red">*</font>Vendor Office:</label></div>
              <div class="col-md-8">
                <input type="text" name="vendor_office" id="vendor_office" class="form-control" style="width: 338px;" placeholder="Vendor Office" required="required" />
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-4"> <label>State:</label></div>
              <div class="col-md-8">
                <input type="text" name="state" id="state" class="form-control" style="width: 338px;" placeholder="State" readonly />
              </div>
            </div><br>  
            <div class="row">
              <div class="col-md-4"> <label>GSTIN No:</label></div>
              <div class="col-md-8">
                <input type="text" name="gstin" id="gstin" class="form-control" style="width: 338px;" placeholder="GSTIN No." readonly />
              </div>
            </div><br>
            <div class="row">
            <div class="col-md-4"> <label>Remark:</label></div>
            <div class="col-md-8">
              <input type="text" name="remark" id="remark" class="form-control" style="width: 338px;" placeholder="Remark" />
            </div>
          </div> 
          </div> 
          <div class="col-md-6"> 
          <script type="text/javascript">
            function ShowHideDiv(chkPassport) {
                var dvinvoice_no = document.getElementById("dvinvoice_no");
                dvinvoice_no.style.display = chkPassport.checked ? "block" : "none";
                invoice_no.setAttribute( 'required', 'required' );
                var dvinvoice_date = document.getElementById("dvinvoice_date");
                dvinvoice_date.style.display = chkPassport.checked ? "block" : "none";
                invoice_date.setAttribute( 'required', 'required' );
                var dvinvoice_file = document.getElementById("dvinvoice_file");
                dvinvoice_file.style.display = chkPassport.checked ? "block" : "none";
                invoice_file.setAttribute( 'required', 'required' );
            }
          </script>
          <label for="chkPassport">
            <input type="checkbox" id="chkPassport" onclick="ShowHideDiv(this)" />
              Challan cum Invoice:
          </label>
          <div class="row">
              <br><div class="col-md-4"><label><font color="red">*</font>Challan no:</label></div>
              <div class="col-md-8">
                <input type="text" name="challan_no" id="challan_no" class="form-control" style="width: 338px;" placeholder="Challan No" required="required" />
              </div>
            </div><br> 
            <div class="row">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Date:</label></div>
              <div class="col-md-8">
                <input type="text" name="challan_date" id="challan_date" class="form-control" style="width: 338px;" placeholder="dd-mm-yyyy" required="required" />
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="challan_file" class="form-control" name="challan_file[]" onclick="challan_select()" style="width: 338px;" multiple="multiple" required>
              </div>
            </div>
            <div class="row" id="dvinvoice_no" style="display: none"><br>
              <div class="col-md-4"> <label><font color="red">*</font>Invoice No:</label></div>
              <div class="col-md-8">
                <input type="text" name="invoice_no" id="invoice_no" class="form-control" style="width: 338px;" placeholder="Invoice No" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_date" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Date:</label></div>
              <div class="col-md-8">
                <input type="text" name="invoice_date" id="invoice_date" class="form-control" style="width: 338px;" placeholder="dd-mm-yyyy" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_file" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="invoice_file" class="form-control" name="invoice_file[]" onclick="invoice_select()" style="width: 338px;" multiple="multiple" required>
              </div>
              <script>
              function invoice_select() {
                $(document).ready(function() {
                  $ ('#invoice_file').bind('change', function() {
                      var fileSize = this.files[0].size/1024/1024;
                      if (fileSize > 2) { // 2M
                        alert('Your max file size exceeded');
                        $('#invoice_file').val('');
                      }
                  });
                });
              }
              function challan_select() {
                $(document).ready(function() {
                  $ ('#challan_file').bind('change', function() {
                      var fileSize = this.files[0].size/1024/1024;
                      if (fileSize > 2) { // 2M
                        alert('Your max file size exceeded');
                        $('#challan_file').val('');
                      }
                  });
                });
              }
            </script>
          </div><br>
        </div>
      </div>

      <?php
       $sql2 =  "select count(grn_no) as total_tyre,ROUND(SUM(total_gst),0) as total_gst, ROUND(SUM(total_amount),0) as total_amount, ROUND(SUM(discount_amount),0) as discount_amount from grn_over_tyre WHERE office='$usernameSet' and temp_status='0' ";

        if ($result1=mysqli_query($conn,$sql2))
        {
          $row4=mysqli_fetch_array($result1);
          $total_tyre=$row4['total_tyre'];
          $total_gst = $row4['total_gst'];
          $discount_amount = $row4['discount_amount'];
          $total_amount = $row4['total_amount'];
         ?>
        <div class="box-body">
          <h3>Totals</h3>
            <div class="col-md-6"> 
              <div class="row">
                <div class="col-md-4"> <label>Total Tyre Count:</label></div>
                <div class="col-md-8">
                  <input type="text" value="<?php echo $total_tyre; ?>" class="form-control" name="total_tyre" style="width: 338px;" readonly="readonly"/>
                </div>
              </div><br> 

              <div class="row">
                <div class="col-md-4"> <label>Total GST:</label></div>
                <div class="col-md-8">
                  <input type="text" name="total_gst" id="total_gst" class="form-control"  style="width: 338px;" value="<?php echo $total_gst; ?>" readonly="readonly" />
                </div>
              </div>
            </div> 

            <div class="col-md-6">
              <div class="row">
                <div class="col-md-4"> <label>Total Discount:</label></div>
                <div class="col-md-8">
                  <input type="text" name="discount_amount" id="discount_amount" class="form-control" style="width: 338px;" value="<?php echo $discount_amount; ?>" readonly="readonly" />
                </div>
              </div><br> 
              <div class="row">
                <div class="col-md-4"> <label>Total Amount:</label></div>
                <div class="col-md-8">
                  <input type="text" name="total_amount2" class="form-control" style="width: 338px;" value="<?php echo $total_amount ?>" readonly="readonly" />
                </div>
              </div>
            </div>
          </div>
         <?php
        }
        ?>
        <center>
          <button type="submit" value="Submit" style="width: 130px;"  class="btn btn-primary ml-5" >Add To Remould</button>
        </center>
      </form>
      
      <div class="box-body">
        <form method="post" action="insert_temp_new_new_remould.php" autocomplete="off">
          <h3>Tyre Details</h3>
          <div class="row-3" style="background-color: #ddd;"><br>
            <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px;">
              <tr>
                  <div class="col-md-3">

                    <input type="hidden" name="office_temp" value="<?php echo $usernameSet; ?>"class="form-control"/>

                    <label>Tyre No</label>
                    <input type="text" name="tyre_no" style="width: 250px;" class="form-control" placeholder="tyre no" required="required">
              
                    <label>Card No:</label>
                    <input type="text" name="card_no" required="required" style="width: 250px;" id="Card No"  class="form-control" placeholder="card no">
                  
                    <label>Production Month:</label>
                    <input type="text" name="product_month"  required="required" style="width: 250px;" id="product_month" placeholder="mm/yyyy" class="form-control">
                  
                    <label>Brand</label>
                    <input type="text" name="brand" required="required" style="width: 250px;" id="brand" placeholder="brand" class="form-control">
                  </div>
              </tr>
              
              <tr>
                <div class="col-md-3">
                  <label>Rubber Brand:</label>
                  <input type="text" name="rubber_brand" id="rubber_brand" onchange="get_data1(this.value);"  style="width: 200px;" class="form-control" placeholder="Rubber Brand" required="required" disabled />
              
                  <label>Rubber Type:</label>
                  <input type="text" name="rubber_type" required="required" style="width: 200px;" id="rubber_type" placeholder="Rubber Type"  class="form-control" disabled />

                  <br>
                  <input type="hidden" value='0' name="remould">
                  <input type="checkbox" value='1' id="checkBox" name="remould"> Remould<br><br>

                  <label>Remarks:</label>
                  <input type="text" name="remark"  required="required" style="width: 200px;" id="remark" placeholder="Remarks" class="form-control">

                </div>
              </tr>
              
              <tr>
                <div class="col-md-3">
                  <label>Amount:</label>
                  <input type="Number" name="amount" oninput="sum1()" id="amount"  style="width: 250px;" class="form-control" placeholder="Amount" required="required">
              
                  <label>Discount %:</label>
                  <input type="Number" name="discount" oninput="sum1()"  required="required" style="width: 250px;" id="discount" placeholder="Discount" class="form-control">
                  
                  <label>CGST:</label>
                  <input type="Number" name="CGST" min='0' oninput="sum2()" required="required" style="width: 250px;" id="CGST" placeholder="cgst" class="form-control">
                  
                  <label>SGST:</label>
                  <input type="Number" name="SGST" min='0' oninput="sum2()" required="required" style="width: 250px;" id="SGST" placeholder="sgst" class="form-control">
                </div>
              </tr>
              <tr>
                <div class="col-md-3">
                  <label>IGST:</label>
                  <input type="text" name="IGST" id="IGST" style="width: 250px;" class="form-control" placeholder="0" readonly="readonly">
              
                  <label>Discount Amt:</label>
                  <input type="text" name="discount_amt" placeholder="discount amount" style="width: 250px;" id="discount_amt" readonly="readonly"  class="form-control">
                  
                  <label>Total GST:</label>
                  <input type="text" name="total_GST" placeholder="total GST" style="width: 250px;" id="total_GST" readonly="readonly" class="form-control">
                  
                  <label>Total Amount:</label>
                  <input type="text" name="total_amount" readonly="readonly" required="required" style="width: 250px;" id="total_amount" placeholder="Total amount" class="form-control">
                </div>
              </tr>
            </table>

            <button type="submit" value="Submit"  style="float: right; margin-right: 15px;" class="btn btn-success ml-5" >Add</button><br><br>
          </div>
        </form><br>

      <div style="overflow-x:auto;">
        <table id="employee_data1" class="table table-bordered table-striped">
          <tbody>
            <tr>
              <th style="display: none;"></th>
              <th>Tyre no</th>
              <th>Card No</th>
              <th>Production month </th>
              <th>Rubber Brand</th>
              <th>Rubber Type</th>
              <th>Remark</th>
              <th>Amount</th>
              <th>Discount Percent</th>
              <th>CGST</th>
              <th>SGST</th>
              <th>IGST</th>
              <th>Discount Amount</th>
              <th>Total Gst</th>
              <th>Total Amount</th>
              <th>Remove</th>
            </tr>
          </tbody>  
          <?php
            $query = "SELECT * FROM grn_over_tyre where temp_status='0'";
            $result = mysqli_query($conn, $query);
            while($row = mysqli_fetch_array($result)){
              $sl_no = $row['sl_no'];
              $tyre_no = $row['tyre_no'];
              $card_no= $row['card_no']; 
              $production_month = $row['production_month'];
              /*$brand_name= $row['brand_name'];*/
              $rubber_brand = $row['rubber_brand'];
              $rubber_type = $row['rubber_type'];
              $remark = $row['remark'];
              $amount = $row['amount'];
              $discount_percent = $row['discount_percent'];
              $cgst = $row['cgst'];
              $sgst = $row['sgst'];
              $igst = $row['igst'];
              $discount_amount = $row['discount_amount'];
              $total_gst = $row['total_gst'];
              $total_amount = $row['total_amount'];     
          ?>
              <tr>
                 <!-- <input type="hidden" name="id" value="<?php echo $id; ?>"> -->
                <td style="display: none;"><?php echo $sl_no?></td>
                <td ><?php echo $tyre_no?></td>
                <td ><?php echo $card_no?> </td>
                <td ><?php echo $production_month?></td>
                <!-- <td ><?php echo $brand_name?></td> -->
                <td ><?php echo $rubber_brand?></td>
                <td ><?php echo $rubber_type?></td>
                <td ><?php echo $remark?></td>
                <td ><?php echo $amount?></td>
                <td ><?php echo $discount_percent?></td>
                <td ><?php echo $cgst?></td>
                <td ><?php echo $sgst?></td>
                <td ><?php echo $igst?></td>
                <td ><?php echo $discount_amount?></td>
                <td ><?php echo $total_gst?></td>
                <td ><?php echo $total_amount?></td>
                <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $sl_no;?>')" name="sl_no" value="X" class="btn btn-danger" />
                </td>
              </tr>
              <script>
                function DeleteModal(sl_no)
                {
                  var sl_no = sl_no;
                  if (confirm("Do you want to delete this data..?"))
                  {
                    if(sl_no!='')
                    {
                      $.ajax({
                        type: "POST",
                        url: "delete_temp_new_new_remould.php",

                        data: 'sl_no='+sl_no,
                        success: function(data){
                        
                            $("#result22").html(data);
                        }
                      });
                    }
                  }
                }     
              </script>
              <div id="result22"></div> 
            <?php } ?>
            </table>
          </div>

          <!-- <form method="post" action="insert_final_remould_stock.php" id="main_form">
            <center>
              <button type="submit" value="Submit" style="width: 130px;"  class="btn btn-primary ml-5" >Add To Remould</button>
            </center>
          </form> -->
        </div>
      </div>

    </section>

    </div>
<?php include("../footer.php");?>
</div>
</body>
</html>

<script type="text/javascript">
  $(function()
  { 
    $("#brand").autocomplete({
    source: 'search_brand_new_new_remould_auto.php',
    change: function (event, ui) {
    if(!ui.item){
    $(event.target).val(""); 
    $(event.target).focus();
    alert('Brand Name does not exists');
    } 
  },
  focus: function (event, ui) { return false; } }); });


  $(document).ready(function(){
  $('#checkBox').click(function(){
    if($(this).is(":checked"))
    {
       $("#rubber_brand").removeAttr("disabled");
       $("#rubber_type").removeAttr("disabled");
    }
    else
    {
       $("#rubber_brand").attr("disabled" , "disabled");
       $("#rubber_type").attr("disabled" , "disabled");
    }
  });
});
</script>
<script>
function sum1()
{
  var actual_price = Number($('#amount').val());  
  var discount = Number($('#discount').val());  
  var selling_price = actual_price - (actual_price * (discount / 100));
  $('#total_amount').val(selling_price);
  $('#discount_amt').val(discount);
}
function sum2()
{
  var CGST = Number($('#CGST').val());  
  var SGST = Number($('#SGST').val());  
  var total_GST = CGST + SGST;
  $('#total_GST').val(total_GST);
}
</script>