<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
     <!--   <a href='upload_invice_on_newtyre.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a> -->
      <h1>Tyre Detail</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px;" >  
                        <thead>
                          <tr>
                             <th>Sno</th>
                              <th>Tyre No</th>
                              <th>Production Month</th>
                              <th>Brand</th>
                              <th>Size</th>
                              <th>Ply</th>
                              <th>Nsd</th>
                            <!--   <th>Min Nsd</th> -->
                              <th >GST(%)</th>
                              <th >Amount</th>
                              <th >Discount(%)</th>
                              <th >Discount(Rs.)</th>
                              <th >Total Amount</th>
                            </tr>
                 </thead>
                    <?php
                    $valuetosearch = $_POST['grn_no'];
                      $sql1 = "select * from grn_over_tyre where grn_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $id_customer = 0;
                       $sno = 1;
                      while ($row = mysqli_fetch_array($result1)) 
                          { 
                            $id = $row['sl_no'];
                            $tyre_no1 = $row['tyre_no'];
                            $product_month1 = $row['production_month'];
                            $brand1 = $row['brand_name'];
                             $amount = $row['amount'];
                            $dis_percent = $row['discount_percent'];
                              $total_gst = $row['total_gst'];
                            $dis_amt = $row['discount_amount'];
                            $total_amt = $row['total_amount'];
                            $tyre_remarks = $row['tyre_remarks'];
                            $ProductSize = $row['ProductSize'];
                            $ProductPlyRating = $row['ProductPlyRating'];
                            $productMaxNsd = $row['productMaxNsd'];
                            $productMinNsd = $row['productMinNsd'];
                          ?>
        <tbody >
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?></td>
              <td><?php echo $tyre_no1; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no" value="<?php echo $tyre_no1; ?>" ></td>

              <td><?php echo $product_month1; ?><input readonly type="hidden" class="form-control" style="width: 120px;"  name="product_month" value="<?php echo $product_month1; ?>" ></td>

              <td><?php echo $brand1; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="brand" readonly value="<?php echo $brand1; ?>" ></td>
              
              <td><?php echo $ProductSize; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductSize" readonly value="<?php echo $ProductSize; ?>" ></td>

              <td><?php echo $ProductPlyRating; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductPlyRating" readonly value="<?php echo $ProductPlyRating; ?>" ></td>

              <td><?php echo $productMaxNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMaxNsd" readonly value="<?php echo $productMaxNsd; ?>" ></td>

             <!--  <td><?php echo $productMinNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMinNsd" readonly value="<?php echo $productMinNsd; ?>" ></td> -->

                                    <td><?php echo $total_gst; ?>
                                       <input type="hidden" name="total_gst" value="<?php echo $total_gst; ?>">
                                     </td>

                                     <td><?php echo $amount; ?>
                                      <input type='hidden' required="required" style='width: 120px;'  value="<?php echo $amount; ?>" />
                                     </td>

                                     <td><?php echo $dis_percent; ?>
                                      <input type='hidden' required="required" style='width: 120px;'  value="<?php echo $dis_percent; ?>" />
                                     </td>

                                     <td><?php echo $dis_amt; ?>
                                      <input type='hidden' required="required" style='width: 120px;'  value="<?php echo $dis_amt; ?>" />
                                     </td>

                                      <td><?php echo $total_amt; ?>
                                      <input type='hidden' required="required" style='width: 120px;'  value="<?php echo $total_amt; ?>" />
                                     </td>
                                    
                                  </tr>
                                          <?php  
                                          $id_customer++;
                                            $sno++; }

                                        ?>
                        </tbody>               
                    </table><br>
              </div>  

            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

   <script type="text/javascript">
       function GetSumFunc(id)
            {
                var amount = Number($('#amount'+id).val());
                var dis_perc = Number($('#dis_percent'+id).val());
                if (dis_perc > 100) {
                  alert("more then 100 discount is not allowed");
                   $('#dis_percent'+id).val('');
                }else{
                var tg = Number($('#total_gst1'+id).val());
                var dis_amt = Number($('#dis_amt'+id).val());
                var total_amt = Number($('#total_amt'+id).val());
                var total_gst1 = amount*tg/100;
                var dis_value = (amount*dis_perc/100).toFixed(2);
                var discount = Math.round(dis_value);
                $('#dis_amt'+id).val(discount);
                var total = (((amount-discount)+total_gst1).toFixed(2));
                $('#total_amt'+id).val(total);
              }
            }

               function DisCountFunc(id)
              {
                var amount = Number($('#amount'+id).val());
                var dis_amt = Number($('#dis_amt'+id).val());
                var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
              }
     </script>
