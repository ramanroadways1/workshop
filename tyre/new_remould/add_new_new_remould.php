<?php 
 require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <a href='approve_newtyre.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>&nbsp;Approve New Tyre</a>

       <a href='upload_invice_on_newtyre.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Upload Invoice On New Tyre</a>
      <h1>New Tyre Purchase</h1>
    </section>
    <section class="content">
      <form method="post" action="insert_temp_new_tyre.php" enctype="multipart/form-data" autocomplete="off">
      <div class="box box-info">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <h3>Basic Details</h3>
            </div>
          </div>

          <?php 

          $usernameSet=$_SESSION['username'];
          if(isset($_SESSION['usernameSet'])) {
              echo "  <SCRIPT>
                      window.location.href='fetch_cross_chk_data.php';
                   </SCRIPT>";
                   exit();
               }
           ?>
          <div class="col-md-6"> 
            <!-- <?php echo $usernameSet; ?> -->
            <div class="row">
              <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>
              <div class="col-md-4"> <label><font color="red">*</font>Vendor:</label></div>
              <div class="col-md-8">
                <input type="text" name="vendor" onblur="getParty(this.value);" id="vendor" class="form-control" style="width: 338px;" placeholder="Vendor" required="required" />
              </div>
            </div><br>  
            <div class="row">
              <div class="col-md-4"> <label><font color="red">*</font>Vendor Office:</label></div>
              <div class="col-md-8">
                <input type="text" name="vendor_office" id="vendor_office" class="form-control" style="width: 338px;" placeholder="Vendor Office" readonly />
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-4"> <label>State:</label></div>
              <div class="col-md-8">
                <input type="text" name="state" id="state" class="form-control" style="width: 338px;" placeholder="State" readonly />
              </div>
            </div><br>  
            <div class="row">
              <div class="col-md-4"> <label>GSTIN No:</label></div>
              <div class="col-md-8">
                <input type="text" name="gstin" id="gstin" class="form-control" style="width: 338px;" placeholder="GSTIN No." readonly />
              </div>
            </div><br>
            <div id="result"></div> 
            <div class="row">
              <div class="col-md-4"> <label>Remark:</label></div>
              <div class="col-md-8">
                <input type="text" name="remark" id="remark" class="form-control" style="width: 338px;" placeholder="Remark" />
              </div>
            </div> <br>
           
          </div> 
          <div class="col-md-6"> 
            <label for="chkinvoice">
              <input type="checkbox" id="chkinvoice" class="pno2" required="required"  onclick="ShowHideDiv(this);" />
              Upload Invoice
            </label>
            
              <label for="chkchallan">
              <input type="checkbox" id="chkchallan"  class="pno1" required="required" onclick="ShowHideDiv2(this);" />
              Upload Challan
            </label>

            <div class="row"  id="dvchallan_no" style="display: none;">
              <br><div class="col-md-4"><label><font color="red">*</font>Challan no:</label></div>
              <div class="col-md-8">
                <input type="text" name="challan_no" id="challan_no" class="form-control" style="width: 338px;" placeholder="Challan No" />
              </div>
            </div><br> 
            <div class="row" id="dvchallan_date" style="display: none;">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Date:</label></div>
              <div class="col-md-8">
                <input type="text" name="challan_date" id="challan_date"  class="form-control" style="width: 338px;" placeholder="dd-mm-yyyy"  />
              </div>
            </div><br>
            <div class="row" style="display: none" id="dvchallan_file">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="challan_file" class="form-control" name="challan_file[]" onclick="challan_select()" style="width: 338px;" multiple="multiple" />
              </div>
            </div>
            <div class="row" id="dvinvoice_no" style="display: none"><br>
              <div class="col-md-4"> <label><font color="red">*</font>Invoice No:</label></div>
              <div class="col-md-8">
                <input type="text" name="invoice_no" id="invoice_no" class="form-control" style="width: 338px;" placeholder="Invoice No" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_date" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Date:</label></div>
              <div class="col-md-8">
                <input type="text" name="invoice_date" id="invoice_date" class="form-control" style="width: 338px;" placeholder="yyyy-mm-dd" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_file" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="invoice_file" class="form-control" name="invoice_file[]" onclick="invoice_select()" style="width: 338px;"  multiple="multiple"/>
              </div>
            </div><br>
          </div>
          <!-- </form> -->
          <div class="container" style="width: 100%">
            <div class="table-wrapper">
              <div class="table-title">
                <div class="row">
                  <div class="col-sm-10"><h3>Tyre Details</h3></div>
                  <div class="col-sm-2">
                    <div align="right">
                      <button type="button" class="btn btn-info add-new" id="add" onclick="addTyre('tyre_table')"> <i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive"> 
                <table id="tyre_table" class="table table-bordered">
                  <thead>
                    <tr>
                        <th><font color="red">*</font>Tyre No</th>
                        <th><font color="red">*</font>Production Month</th>
                        <th><font color="red">*</font>Brand</th>
                        <th><font color="red">*</font>Product Size</th>
                        <th><font color="red">*</font>Ply</th>
                        <th><font color="red">*</font>Max Nsd</th>
                        <th><font color="red">*</font>Min Nsd</th>
                        <th>Remarks</th>
                        <th >GST(%)</th>
                        <th >Amount</th>
                        <th >Discount(%)</th>
                        <th >Discount(Rs.)</th>
                        <th >Total Amount</th>
                        <th>Remove</th>
                        <th>Copy</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div><br><br>
           
            <center>
              <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
            </center>
          </div><br>
      </div>
    </div>
    </form>
    </section>
  
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>

<script type="text/javascript">


function ShowHideDiv(chkinvoice) {
     if(document.getElementById('chkinvoice').checked){
       $("#chkchallan").attr('disabled',true); 
       $("#dvinvoice_no").css("display","block");
       document.getElementById("invoice_no").required = true;
       $("#dvinvoice_date").css("display","block");
        document.getElementById("invoice_date").required = true;
       $("#dvinvoice_file").css("display","block");
        document.getElementById("invoice_file").required = true;
       $('#challan_no').val('');
       $('#challan_date').val('');
       $('#challan_file').val('');
       document.getElementById("challan_no").required = false;
        document.getElementById("challan_date").required = false;
        document.getElementById("challan_file").required = false;
     }
     else{
        $("#chkchallan").attr('disabled',false); 
         $("#dvinvoice_no").css("display","none");
         $("#dvinvoice_date").css("display","none");
         $("#dvinvoice_file").css("display","none");
        }

       $(".total_gst1").attr('required',true); 
       $(".amount").attr('required',true); 
       $(".dis_percent").attr('required',true); 
       $(".dis_amt").attr('required',true); 
       $(".total_amt").attr('required',true); 
     
          $('.total_gst1').attr("style", "display:block");
           $('.amount').attr("style", "display:block");
           $('.dis_percent').attr("style", "display:block");
           $('.dis_amt').attr("style", "display:block");
           $('.total_amt').attr("style", "display:block");
      } 


      function ShowHideDiv2(chkchallan) {
         if(document.getElementById('chkchallan').checked){

           $("#chkinvoice").attr('disabled',true); 
         
           $("#dvchallan_no").css("display","block");
           challan_no.setAttribute( 'required', 'required' );

           $("#dvchallan_date").css("display","block");
           challan_date.setAttribute( 'required', 'required' );

           $("#dvchallan_file").css("display","block");
            challan_file.setAttribute( 'required', 'required' );

               $('#invoice_no').val('');
               $('#invoice_date').val('');
               $('#invoice_file').val('');
       document.getElementById("invoice_no").required = false;
        document.getElementById("invoice_date").required = false;
       document.getElementById("invoice_file").required = false;

         }
         else{
             $("#chkinvoice").attr('disabled',false); 
             $("#dvchallan_no").css("display","none");
             $("#dvchallan_date").css("display","none");
             $("#dvchallan_file").css("display","none");
          }
            $(".total_gst1").attr('required',false); 
           $(".amount").attr('required',false); 
           $(".dis_percent").attr('required',false); 
           $(".dis_amt").attr('required',false); 
           $(".total_amt").attr('required',false); 

          $('.total_gst1').attr("style", "display:none");
           $('.amount').attr("style", "display:none");
           $('.dis_percent').attr("style", "display:none");
           $('.dis_amt').attr("style", "display:none");
           $('.total_amt').attr("style", "display:none");
         }
  </script>

<script>
$("#total_gst1").change(function(){
  var vendor =  document.getElementById("vendor").value;
  var str =  document.getElementById("gstin").value;
  var res = str.substring(0,2);
  var total_gst =  document.getElementById("total_gst1").value;
  if(vendor!='')
  {
    if(res=='24')
    {
      $('#cgst').val(total_gst/2);
      $('#sgst').val(total_gst/2);
    }
    else{
      $('#igst').val(total_gst);
    }
  }
  else{
    alert('select vendor name firstly');
  }
});
function GetSumFunc(id)
{
  var amount = Number($('#amount'+id).val());
  var dis_perc = Number($('#dis_percent'+id).val());
  if (dis_perc > 100) {
    alert("more then 100 discount is not allowed");
     $('#dis_percent'+id).val('');
  }else{
  var tg = Number($('#total_gst1'+id).val());
  var dis_amt = Number($('#dis_amt'+id).val());
  var total_amt = Number($('#total_amt'+id).val());
  var total_gst1 = amount*tg/100;
  var dis_value = (amount*dis_perc/100).toFixed(2);
  var discount = Math.round(dis_value);
  $('#dis_amt'+id).val(discount);

  var total = (((amount-discount)+total_gst1).toFixed(2));
  $('#total_amt'+id).val(total);
}

}

  function DisCountFunc(id)
  {
    var amount = Number($('#amount'+id).val());
    var dis_amt = Number($('#dis_amt'+id).val());
    var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
  }
</script>

<script>

    function copy_text(id) {
            var id2  = id+1;
            //alert(id2);
               var weekPicker1 = document.getElementById('weekPicker1'+id).value; 
               $('#weekPicker1'+id2).val(weekPicker1);

                var brand = document.getElementById('brand'+id).value; 
               $('#brand'+id2).val(brand);

                var remarks = document.getElementById('remarks'+id).value; 
               $('#remarks'+id2).val(remarks);

                var amount = document.getElementById('amount'+id).value; 
               $('#amount'+id2).val(amount);

                var dis_percent = document.getElementById('dis_percent'+id).value; 
               $('#dis_percent'+id2).val(dis_percent);

                var dis_amt = document.getElementById('dis_amt'+id).value; 
               $('#dis_amt'+id2).val(dis_amt);

               var total_amt = document.getElementById('total_amt'+id).value; 
               $('#total_amt'+id2).val(total_amt);

              /* extra column*/

               var ProductSize = document.getElementById('ProductSize'+id).value; 
               $('#ProductSize'+id2).val(ProductSize);

               var ProductPlyRating = document.getElementById('ProductPlyRating'+id).value; 
               $('#ProductPlyRating'+id2).val(ProductPlyRating);

               var productMaxNsd = document.getElementById('productMaxNsd'+id).value; 
               $('#productMaxNsd'+id2).val(productMaxNsd);

               var productMinNsd = document.getElementById('productMinNsd'+id).value; 
               $('#productMinNsd'+id2).val(productMinNsd);
             }
</script>
  <div id="result22"></div>
        <script>
              function MyFunc(id)
              { id = id;
                var office = '<?php echo $usernameSet; ?>';
                var tyre_no = $('#tyre_no'+id).val();
                if(tyre_no!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chktyreno.php",
                      data:{tyre_no:tyre_no,office:office,id:id},
                      success: function(data){
                       //alert(data);
                         //$('#tyre_no'+id).val('');
                          $("#result22").html(data);
                      }
                      });
                }
              }  

              function not_success_msg(id) {
              alert("this tyre no is in pending queue");
              $('#tyre_no'+id).val('');
               //alert("success");
              }

               function not_success_msg2(id) {
               alert("this tyre no is inserted in stock");
              $('#tyre_no'+id).val('');
               //alert("success");
              }
            </script>

 <div id="result1"></div>
 <script type="text/javascript">
  

 </script>
<script>
    function addTyre(tyre_table) {
      /*$('#gst_hide').hide();*/
      var vendor =  document.getElementById("vendor").value;
      /* var total_gst =  document.getElementById("total_gst1").value;*/
       var chkchallan =  document.getElementById("chkchallan").value;
       var chkinvoice =  document.getElementById("chkinvoice").value;
        
		if(vendor!='')
        {
        if((document.getElementById('chkchallan').checked)||(document.getElementById('chkinvoice').checked)){
                
        var rowCount = document.getElementById("tyre_table").rows.length;
        var row = document.getElementById("tyre_table").insertRow(rowCount);
      
        var cell1 = row.insertCell(0);
        cell1.innerHTML ="<input type='text' style='width: 120px;' name='tyre_no[]' onblur='MyFunc("+rowCount+",this.id)'  id='tyre_no"+rowCount+"' required/>";
        
        var cell2 = row.insertCell(1);
        cell2.innerHTML = "<input type='text' style='width: 120px;' name='product_month[]' id='weekPicker1"+rowCount+"' required/> ";
        
        var cell3 = row.insertCell(2);
        cell3.innerHTML = "<input type='text' style='width: 120px;' name='brand[]' id='brand"+rowCount+"' required/>";

       /*extra column*/
         var cell4 = row.insertCell(3);
         cell4.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductSize[]' id='ProductSize"+rowCount+"' required/>";

          var cell5 = row.insertCell(4);
         cell5.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductPlyRating[]' id='ProductPlyRating"+rowCount+"' required/>";

          var cell6 = row.insertCell(5);
         cell6.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMaxNsd[]' id='productMaxNsd"+rowCount+"' required/>";

          var cell7 = row.insertCell(6);
         cell7.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMinNsd[]' id='productMinNsd"+rowCount+"' required/>";

        /*End of extra column*/
        
        var cell8 = row.insertCell(7);
        cell8.innerHTML =  "<input type='text' style='width: 120px;' name='tyre_remarks[]' id='remarks"+rowCount+"'/>";

         var cell9 = row.insertCell(8);
        cell9.innerHTML = "<select required id='total_gst1"+rowCount+"'  onchange='GetSumFunc("+rowCount+",this.id)' class='total_gst1' style='width: 150px; height: 27px;' name='total_gst[]' >"+
         "<option value='18'>18</option>"+
           "<option value='0'>0</option>"+
            "<option value='5'>5</option>"+
             "<option value='12'>12</option>"+
               "<option value='28'>28</option>"+
          "</select>";

        var cell10 = row.insertCell(9);
        cell10.innerHTML =  "<input type='number' style='width: 120px;'  oninput='GetSumFunc("+rowCount+",this.id)' min='0' name='amount[]' class='amount' id='amount"+rowCount+"'  />";

        var cell11 = row.insertCell(10);
        cell11.innerHTML =  "<input type='number'  class='dis_percent' style='width: 120px;' oninput='GetSumFunc("+rowCount+",this.id)' min='0' max='100' name='dis_percent[]' id='dis_percent"+rowCount+"'/>";

        var cell12 = row.insertCell(11);
        cell12.innerHTML =  "<input type='number' class='dis_amt' oninput='DisCountFunc("+rowCount+");GetSumFunc("+rowCount+",this.id)' style='width: 120px;' min='0' name='dis_amt[]' id='dis_amt"+rowCount+"' />";

        var cell13 = row.insertCell(12);
        cell13.innerHTML =  "<input type='number' oninput='GetSumFunc("+rowCount+",this.id)' style='width: 120px;' name='total_amt[]' class='total_amt' id='total_amt"+rowCount+"' readonly  />";

        var cell14 = row.insertCell(13);
        cell14.innerHTML =  "<button type='button' class='btn btn-danger' id='remove"+rowCount+"'><i class='fa fa-remove'></i></button>";

         var cell15 = row.insertCell(14);
        cell15.innerHTML =  "<input type='button' id='copy"+rowCount+"' onclick='copy_text("+rowCount+",this.id)' value='copy'>";

        $("#remove"+rowCount).click(function(){
         $(this).parents("tr").remove();
        });

    


    convertToWeekPicker($("#weekPicker1"+rowCount))

    $(function()
    { 
      $("#brand"+rowCount).autocomplete({
      source: 'search_brand_name_auto.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre Brand Name does not exists');
      } 
    },
    focus: function (event, ui) { return false; } }); });

    $("#brand"+rowCount).change(function(){
      var brand=$("#brand"+rowCount).val();
      $.get("search_brand_data.php",{txt:brand},function(data,status){
        var arr = $.parseJSON(data);
        $("#ProductSize"+rowCount).val(arr[0]);
        $("#ProductPlyRating"+rowCount).val(arr[1]);
        $("#productMaxNsd"+rowCount).val(arr[2]);
        $("#productMinNsd"+rowCount).val(arr[3]);
         });
    });

        if(document.getElementById('chkchallan').checked)
          {
            ShowHideDiv2(this);
               }
           else{
            if(document.getElementById('chkinvoice').checked)
            {
              ShowHideDiv(this);
            }
          }

  }else{
     $('#add').show();
    alert('Please select Bill Type...');
  }

  }
  else{
    alert('Please select vendor name...');
  }
    
}
</script>
<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}
</style>
<script>
  function invoice_select() {
    $(document).ready(function() {
      $ ('#invoice_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#invoice_file').val('');
          }
      });
    });
  }
  function challan_select() {
    $(document).ready(function() {
      $ ('#challan_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#challan_file').val('');
          }
      });
    });
  }
</script>
<script type="text/javascript">         
$(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'yy-mm-dd'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    $("#challan_date").datepicker();
 });
});

$(function()
{ 
  $("#vendor").autocomplete({
  source: 'search_vendor_name_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vendor Name does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

function getParty(elem)
{
  var vednor_name = elem;
  if(vednor_name!='')
  {
    $.ajax({
      type: "POST",
      url: "search_party.php",
      data:'vendor='+vednor_name,
      success: function(data){
        $("#result").html(data);
      }
    });
  }
}
</script>


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="src/weekPicker.js"></script>
   

