<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

<script type="text/javascript">
   $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    alert("data");
 });
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
       <a href='add_new_new_remould.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a>
      <h1>Approve New Tyre</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_tyre_detail_on_approval.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php  
                                 $username =  $_SESSION['username']; 
                                  $query = "SELECT  sl_no, grn_no, tyre_count,challan_no,challan_date, challan_file,date(timestamp1) as grn_date,challan_no,challan_date,challan_file,invoice_no,invoice_date,invoice_file  FROM  main_grn WHERE type = 'New'  and  office='$username' AND  length(invoice_file)> 15 and approve_status='0'";

                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){
                                    ?>
                                    <thead>  
                                       <tr>  
                                         <th style="display: none;" scope="row">Id</th>
                                                <td>GRN Number</td>
                                                <td>Total Tyre</td>
                                                <td>Grn Date</td>
                                               <td>Challan File</td>
                                                <td>Invoice File</td>
                                                <td>Approve</td>
                                       </tr>  
                                  </thead>  
                                     <?php
                                    while($row = mysqli_fetch_array($result)){
                                   $id = $row['sl_no'];
                                    $grn_no=$row['grn_no'];
                                    $grn_date=$row['grn_date'];
                                   $tyre_count=$row['tyre_count'];
                                   $challan_no = $row['challan_no'];
                                   $challan_date = $row['challan_date'];
                                   $challan_file = $row['challan_file'];
                                   $challan_file1=explode(",",$challan_file);
                                   $count3=count($challan_file1);

                                   $invoice_no = $row['invoice_no'];
                                   $invoice_date = $row['invoice_date'];
                                   $invoice_file = $row['invoice_file'];
                                   $invoice_file1=explode(",",$invoice_file);
                                   $count4=count($invoice_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                 <td>
                                   <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $grn_no?>" >
                                      <input type="hidden" id="grn_no<?php echo $grn_no?>" value="<?php echo $grn_no?>">
                                    </td>

                                    <td> <?php echo $tyre_count; ?><input type="hidden" name="tyre_count[]"  id="tyre_count<?php echo $id_customer; ?>" value='<?php echo $tyre_count; ?>'></td>

                                    <td> <?php echo $grn_date; ?><input type="hidden" name="grn_date[]"  id="grn_date<?php echo $id_customer; ?>" value='<?php echo $grn_date; ?>'></td>


                                    
                                     <td>
                                     Challan No: <?php echo $challan_no; echo "<br>"; ?>
                                      Challan Date: <?php echo $challan_date; echo "<br>"; ?>
                                     <?php
                                     if ($challan_file!=="") {
                                       # code...
                                     
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="../<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]"  value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                     }else{
                                      ?>
                                       Challan File: <?php  echo "<br>"; ?>
                                      <?php
                                     }
                                      ?>
                                     </td>

                                      <td>
                                       Invoice No: <?php echo $invoice_no; echo "<br>"; ?>
                                        Invoice Date: <?php echo $invoice_date; echo "<br>"; ?>
                                       <?php
                                         for($i=0; $i<$count4; $i++){
                                          ?>
                                           <a href="../<?php echo $invoice_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                           <input type="hidden" name="invoice_file[]"  value='<?php echo $invoice_file; ?>'>
                                           <?php
                                         }
                                      ?>
                                     </td>
                                    <td>
                                     <button type="button"  onclick="addComplainFunc(<?php echo $id; ?>);" >Approve</button>
                                    </td>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                       echo "No Data";
                  }
                  ?>  
                </table>
                        <script type="text/javascript">
                              function addComplainFunc(val)
                                      {
                                        var id = val;
                                        var username = '<?php echo $username ?>'
                                          $.ajax({
                                             type:"post",
                                             url:"update_approve_newtyre.php",
                                            data:'id='+id + '&username='+username,
                                              success:function(data){
                                                 alert(data)
                                                 location.reload(); 
                                                      
                                                }
                                            });
                                       }
                          </script>  
                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  


 </script> 
