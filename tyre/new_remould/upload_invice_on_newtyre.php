<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php include("../aside_main.php"); ?>

<script>
    $( function() {
      $( "#invoice_date" ).datepicker();
    } );
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
       <a href='upload_invoice_battry.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a>
      <h1>Upload Invoice Over Challan For New Tyre</h1>
    </section>

    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  
                  <?php    
                      
                      $username =  $_SESSION['username'];

                        $query = "SELECT  sl_no, grn_no, tyre_count,challan_no,challan_date, challan_file,date(timestamp1) as grn_date,vendor,vendor_office FROM  main_grn WHERE type = 'New'  and  office='$username' AND length(challan_file) > 15 and length(invoice_file)< 15";

                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){

                                    ?>
                                    <thead>  
                                       <tr>  
                                              <th style="display: none;" scope="row">Id</th>
                                                <th>GRN Number</th>
                                                <th style="width: 100px;">Total Tyre</th>
                                                 <th>Vendor</th>
                                                <th >Vendor Office</th>
                                                <th style="width: 100px;">Grn Date</th>
                                               <th style="width: 190px;">Challan File</th>
                                         </tr>  
                                  </thead>  
                                     <?php
                                    while($row = mysqli_fetch_array($result)){
                                    
                                        $id = $row['sl_no'];
                                        $grn_no=$row['grn_no'];
                                        $grn_date=$row['grn_date'];
                                        $tyre_count=$row['tyre_count'];
                                        $vendor=$row['vendor'];
                                        $vendor_office=$row['vendor_office'];
                                        $challan_no = $row['challan_no'];
                                        $challan_date = $row['challan_date'];
                                        $challan_file = $row['challan_file'];
                                        $challan_file1=explode(",",$challan_file);
                                        $count3=count($challan_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                 <td>
                                   <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $grn_no?>" >
                                      <input type="hidden" id="grn_no<?php echo $grn_no?>" value="<?php echo $grn_no?>">
                                    </td>

                                    <td><?php echo $tyre_count; ?></td>
                                     <td><?php echo $vendor; ?></td>
                                    <td> <?php echo $vendor_office; ?></td>
                                    <td> <?php echo $grn_date; ?></td>
                                   

                                     <td >
                                     Challan No: <?php echo $challan_no; echo "<br>"; ?>
                                      Challan Date: <?php echo $challan_date; echo "<br>"; ?>
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="../<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]"  value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                      ?>
                                     </td>
                       </tr>

                     <?php   
                     
                      $id_customer++;
                      $i++;

                    }
                  } else {
                       echo "No Data";
                  }

                  ?>  
                </table>  
                      

                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
</div>

<?php include("../footer.php");?>
</div>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
 });
});

</script>
