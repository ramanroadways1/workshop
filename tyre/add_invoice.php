<?php 
require_once("connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php include("../aside_main.php"); ?>     
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
     <h1>Upload Invoice On Battry</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px;" >  
                        <thead>
                          <tr>
                              <th>id</th>
                              <th>Battery No</th>
                              <th>Purchase Date</th>
                              <th>Model</th>
                              <th>Amount</th>
                              <th>GST(%)</th>
                              <th>Amount</th>
                              <th>Discount(%)</th>
                              <th>Discount(Rs.)</th>
                              <th>Total Amount</th>

                            </tr>
                 </thead>
                    <?php
                    
                     $usernameSet=$_SESSION['username'];
                    // print_r($usernameSet);
                    $valuetosearch = $_POST['battery_no'];
                    // print_r($valuetosearch);
                      $sql1 = "select * from battery_master where battery_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $id_customer = 0;
                       $sno = 1;
                      while ($row = mysqli_fetch_array($result1)) 
                          { 
                              $id = $row['id'];
                              $battery_no = $row['battery_no'];
                              $pur_date = $row['pur_date'];
                              $model = $row['model'];
                              $amount = $row['amount'];

                            // $dis_percent1 = $row['discount_percent'];
                            // $dis_amt1 = $row['discount_amount'];
                            // $total_amt1 = $row['total_amount'];
                            // $tyre_remarks = $row['tyre_remarks'];
                            // $ProductSize = $row['ProductSize'];
                            // $ProductPlyRating = $row['ProductPlyRating'];
                            // $productMaxNsd = $row['productMaxNsd'];
                            // $productMinNsd = $row['productMinNsd'];
                          ?>
        <tbody >
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?></td>
              <td><?php echo $battery_no; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no" value="<?php echo $battery_no; ?>" ></td>

              <td><?php echo $pur_date; ?><input readonly type="hidden" class="form-control" style="width: 120px;"  name="product_month" value="<?php echo $pur_date; ?>" ></td>

              <td><?php echo $model; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="brand" readonly value="<?php echo $model; ?>" ></td>
              
              <td><?php echo $amount; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="amount" readonly value="<?php echo $amount; ?>" ></td>

  <!--             <td><?php echo $ProductPlyRating; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductPlyRating" readonly value="<?php echo $ProductPlyRating; ?>" ></td>

              <td><?php echo $productMaxNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMaxNsd" readonly value="<?php echo $productMaxNsd; ?>" ></td> -->

             <form method="post" autocomplete="off" action="invoice_overchallan_in_newtyre.php" enctype="multipart/form-data">
                                  <td>
                                      <select required id='total_gst1<?php echo $id; ?>'  onchange="GetSumFunc('<?php echo $id;?>')"   class='total_gst1' style='width: 50px; height: 27px;' name='total_gst[]' >"
                                         <option value='18'>18</option>
                                         <option value='0'>0</option>
                                         <option value='5'>5</option>
                                         <option value='12'>12</option>
                                         <option value='28'>28</option>
                                       </select>
                                     </td>

                                     <td>
                                      <input type="hidden" name="grn_no" value="<?php echo $valuetosearch; ?>">

                                       <input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no1; ?>">

                                      <input type="hidden" name="id[]" value="<?php echo $id; ?>">
                                      
                                       <input type="hidden" name="username" id="username" value="<?php echo $usernameSet; ?>" class="form-control" />

                                      <input type='number' required="required" style='width: 120px;'  oninput="GetSumFunc('<?php echo $id;?>')" min='0' name='amount[]' class='amount' id='amount<?php echo $id; ?>'  />
                                     </td>

                                     <td>
                                      <input type='number'  required="required"  class='dis_percent' style='width: 120px;' oninput="GetSumFunc('<?php echo $id;?>')" min='0' max='100' name='dis_percent[]' id='dis_percent<?php echo $id; ?>'/>
                                     </td>

                                     <td>
                                      <input type='number'  required="required"  class='dis_amt' oninput="DisCountFunc('<?php echo $id;?>');GetSumFunc('<?php echo $id;?>')" style='width: 120px;' min='0' name='dis_amt[]' id='dis_amt<?php echo $id; ?>' />
                                     </td>

                                     <td>
                                      <input type='number' required="required"  oninput="GetSumFunc('<?php echo $id;?>')" style='width: 120px;' name='total_amt[]' class='total_amt' id='total_amt<?php echo $id; ?>' readonly  />
                                     </td>

                                   <!--  <td>
                                      <input type="checkbox" name="id_customer[]" value='<?php echo $id_customer; ?>'>
                                    </td> -->
                                  </tr>
                                          <?php  
                                          $id_customer++;
                                            $sno++; }

                                        ?>
                        </tbody>               
                    </table><br>
                      
                     <div class="col-md-6">
                      <label>Invoice No</label>
                       <input type="text" required id="invoice_no" placeholder="Invoice No" name="invoice_no" autocomplete='off'  required> 

                       <label>Invoice Date</label>
                       <input type="text" required name="invoice_date" id="invoice_date"  placeholder="" />
                      </div>
                      <script>
                           $(document).ready(function() {
                              $("#invoice_date").datepicker({
                                 dateFormat: "yy-mm-dd",
                                 });
                            });
                      </script>
                      <div class="col-md-6">
                        <input type="file"  required id="invoice_file" name="invoice_file[]" multiple="multiple"  required/>
                      </div>
                       <br><br><br>
                       <center>
                        <input type="submit" value="Upload Invoice" class="btn-sm btn-success" name="submit" multiple="multiple"  required/>
                      </center><br>
                  </form>
              </div>  

            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

   <script type="text/javascript">
       function GetSumFunc(id)
            {
                var amount = Number($('#amount'+id).val());
                var dis_perc = Number($('#dis_percent'+id).val());
                if (dis_perc > 100) {
                  alert("more then 100 discount is not allowed");
                   $('#dis_percent'+id).val('');
                }else{
                var tg = Number($('#total_gst1'+id).val());
                var dis_amt = Number($('#dis_amt'+id).val());
                var total_amt = Number($('#total_amt'+id).val());
                var total_gst1 = amount*tg/100;
                var dis_value = (amount*dis_perc/100).toFixed(2);
                var discount = Math.round(dis_value);
                $('#dis_amt'+id).val(discount);
                var total = (((amount-discount)+total_gst1).toFixed(2));
                $('#total_amt'+id).val(total);
              }
            }

               function DisCountFunc(id)
              {
                var amount = Number($('#amount'+id).val());
                var dis_amt = Number($('#dis_amt'+id).val());
                var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
              }
     </script>
