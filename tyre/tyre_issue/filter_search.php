<?php
//fetch.php
require("connect.php"); 
$columns = array('sl_no','issue_date', 'office', 'issue_tyre_no', 'receipt_tyre_no','vehicle_no');

$query = "SELECT * FROM tyre_issue1 WHERE ";

if($_POST["is_date_search"] == "yes")
{
 $query .= 'issue_date BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}

if(isset($_POST["search"]["value"]))
{
 $query .= '
  (sl_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR vehicle_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR office LIKE "%'.$_POST["search"]["value"].'%" 
  OR issue_tyre_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR receipt_tyre_no LIKE "%'.$_POST["search"]["value"].'%")
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY sl_no ASC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<input type="checkbox" name="sl_no[]" value='.$row["sl_no"].'>';
 $sub_array[] = $row["issue_date"];
 $sub_array[] = $row["office"];
 $sub_array[] = $row["issue_tyre_no"];
 $sub_array[] = $row["receipt_tyre_no"];
 $sub_array[] = $row["vehicle_no"];
 $data[] = $sub_array;
}

function get_all_data($conn)
{
 $query = "SELECT * FROM tyre_issue1";
 $result = mysqli_query($conn, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>