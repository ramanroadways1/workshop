<?php
include ("connect.php");

$tyre_no = $_POST['rec_tyre_no'];
$office = $_POST['office'];
$wp = $_POST['wp'];

$reason_for_receiving= $_POST['reason_for_receiving'];
$vehicle_no = $_POST['vehicle_no'];
$km_reading = $_POST['km_reading'];
$receiving_date = $_POST['receiving_date'];
$reason_remark = $_POST['reason_remark'];
$grn_date = $_POST['grn_date'];
$brand = $_POST['brand'];
$ProductSize = $_POST['ProductSize'];
$stock_operation_date = date('d-m-y');
@$vendor = $_POST['vendor'];
@$vendor_office = $_POST['vendor_office'];

if ($reason_for_receiving== "temp_remould") {
  
      $datevar= date('dmY');
      $grn_no1 =  "TEMP_RMD-".$datevar; 
      $query= "SELECT unique_no FROM outward_stock where unique_no like '$grn_no1%' order by id desc limit 1";
      $result = mysqli_query($conn, $query);
      if(mysqli_num_rows($result) > 0)  
      {
          $row = mysqli_fetch_array($result);
          $grn_no = $row["unique_no"];
          
          $last_digits = substr($grn_no, -4);
          $next = $last_digits + 1;
          $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
          $temp_outward = $grn_no1."-".$next1;
      }
      else
      {
          $temp_outward =  $grn_no1."-0001";
      }

            $sql2 ="INSERT INTO  outward_stock(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_remould','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld2 = $conn->query($sql2);


            $sql4 ="INSERT INTO  outward_stock_record(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_remould','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld4 = $conn->query($sql4);
          
             $sql2 ="UPDATE grn_over_tyre set status='temp_remould',stock_operation_date='$receiving_date',vehicle_no = '',wp = '',opening_km='' where tyre_no='$rec_tyre_no'";
            $sqld2 = $conn->query($sql2);
           
           
              echo "<script>
                  alert('tyre receive successfully,check in Reserved Remould');
                  window.location.href='add.php';
                  </script>";
            
	          
  
    }elseif ($reason_for_receiving == "temp_claim") {
         
       $datevar= date('dmY');
      $grn_no1 =  "TEMP_CLM-".$datevar; 
      $query= "SELECT unique_no FROM outward_stock where unique_no like '$grn_no1%' order by id desc limit 1";
      $result = mysqli_query($conn, $query);
      if(mysqli_num_rows($result) > 0)  
	       {
	          $row = mysqli_fetch_array($result);
	          $grn_no = $row["unique_no"];
	          $last_digits = substr($grn_no, -4);
	          $next = $last_digits + 1;
	          $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
	          $temp_outward = $grn_no1."-".$next1;
	       }
         else
          {
            $temp_outward =  $grn_no1."-0001";
          }
           $sql2 ="INSERT INTO  outward_stock(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_claim','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld2 = $conn->query($sql2);


            $sql4 ="INSERT INTO  outward_stock_record(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_claim','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld4 = $conn->query($sql4);
          
             $sql2 ="UPDATE grn_over_tyre set status='temp_claim',stock_operation_date='$receiving_date',vehicle_no = '',wp = '',opening_km='' where tyre_no='$rec_tyre_no'";
            $sqld2 = $conn->query($sql2);
           

             echo "<script>
                  alert('tyre receive successfully,check in Reserved Claim');
                  window.location.href='add.php';
                  </script>";


    }elseif ($reason_for_receiving == "temp_scrap") {
         
       $datevar= date('dmY');
      $grn_no1 =  "TEMP_SCRAP-".$datevar; 
      $query= "SELECT unique_no FROM outward_stock where unique_no like '$grn_no1%' order by id desc limit 1";
      $result = mysqli_query($conn, $query);
      if(mysqli_num_rows($result) > 0)  
	       {
	          $row = mysqli_fetch_array($result);
	          $grn_no = $row["unique_no"];
	          $last_digits = substr($grn_no, -4);
	          $next = $last_digits + 1;
	          $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
	          $temp_outward = $grn_no1."-".$next1;
	       }
         else
          {
          $temp_outward =  $grn_no1."-0001";
          }

            $sql2 ="INSERT INTO  outward_stock(office,stock_operation_date,status, tyre_no,grn_date,unique_no,remark) VALUES('$office','$receiving_date','temp_scrap','$rec_tyre_no','$grn_date','$temp_outward','$reason_remark')";
            $sqld2 = $conn->query($sql2);

             $sql4 ="INSERT INTO  outward_stock_record(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_scrap','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld4 = $conn->query($sql4);
          
            $sql2 ="UPDATE grn_over_tyre set status='temp_scrap',stock_operation_date='$receiving_date',vehicle_no = '',wp = '' where tyre_no='$rec_tyre_no' ";
            $sqld2 = $conn->query($sql2);
           
             echo "<script>
                  alert('tyre receive successfully,check in Reserved Scrap');
                  window.location.href='add.php';
                  </script>";

    }elseif ($reason_for_receiving == "null") {
         
       $datevar= date('dmY');
      $grn_no1 =  "TEMP_REUSE-".$datevar; 
      $query= "SELECT unique_no FROM outward_stock where unique_no like '$grn_no1%' order by id desc limit 1";
      $result = mysqli_query($conn, $query);
      if(mysqli_num_rows($result) > 0)  
	       {
	          $row = mysqli_fetch_array($result);
	          $grn_no = $row["unique_no"];
	          $last_digits = substr($grn_no, -4);
	          $next = $last_digits + 1;
	          $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
	          $temp_outward = $grn_no1."-".$next1;
	       }
         else
          {
          $temp_outward =  $grn_no1."-0001";
          }
          
           $sql2 ="INSERT INTO  outward_stock(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_reuse','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld2 = $conn->query($sql2);

            echo $sql2;

            $sql4 ="INSERT INTO  outward_stock_record(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_reuse','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld4 = $conn->query($sql4);
          
             $sql2 ="UPDATE grn_over_tyre set status='null',stock_operation_date='$receiving_date',vehicle_no = '',wp = '',opening_km='' where tyre_no='$rec_tyre_no'";
            $sqld2 = $conn->query($sql2);
           
             echo "<script>
                  alert('tyre receive successfully');
                  window.location.href='add.php';
                  </script>";
    }
    elseif ($reason_for_receiving == "temp_theft") {
         
       $datevar= date('dmY');
      $grn_no1 =  "TEMP_theft-".$datevar; 
      $query= "SELECT unique_no FROM outward_stock where unique_no like '$grn_no1%' order by id desc limit 1";
      $result = mysqli_query($conn, $query);
      if(mysqli_num_rows($result) > 0)  
         {
            $row = mysqli_fetch_array($result);
            $grn_no = $row["unique_no"];
            $last_digits = substr($grn_no, -4);
            $next = $last_digits + 1;
            $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
            $temp_outward = $grn_no1."-".$next1;
         }
         else
          {
          $temp_outward =  $grn_no1."-0001";
          }
          
           $sql2 ="INSERT INTO  outward_stock(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_theft','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld2 = $conn->query($sql2);

            echo $sql2;

            $sql4 ="INSERT INTO  outward_stock_record(office,stock_operation_date,status, tyre_no,grn_date,unique_no,BrandName,ProductSize,vendor,vendor_office,remark) VALUES('$office','$receiving_date','temp_theft','$rec_tyre_no','$grn_date','$temp_outward','$brand','$ProductSize','$vendor','$vendor_office','$reason_remark')";
            $sqld4 = $conn->query($sql4);
          
             $sql2 ="UPDATE grn_over_tyre set status='temp_theft',stock_operation_date='$receiving_date',vehicle_no = '',wp = '',opening_km='' where tyre_no='$rec_tyre_no'";
            $sqld2 = $conn->query($sql2);
           
           /* $sql3 ="INSERT INTO tyre_log(office,type, tyre_no,store_in_operation_date) VALUES('$office','temp_theft','$rec_tyre_no','$receiving_date')";
            $sqld3 = $conn->query($sql3);*/
             echo "<script>
                  alert('tyre receive successfully');
                  window.location.href='add.php';
                  </script>";
    }

$conn->close();
?>