<?php 
  require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
      
  <?php include("header.php"); ?>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <?php include("../aside_main.php"); ?>

  <style>
        ul.ui-autocomplete{
          z-index: 999999 !important;
        }

    button.btnBlack {
        background-color: #000000 !important;
        width:20px; 
        height:50px;
    }
  </style>

    <style type="text/css">

            .tim{
            font-size: 18px;
            border: 2px solid #AD235E;
            border-radius: 100px;
            width: 150px;
            height: 150px; background-image: url(images/ty.png);
            }

      </style>

      <style>

          .button1 {
            background-color: white; 
            color: black; 
            border: 2px;
            }

          .button1:hover {
          background-color: #4CAF50;
          color: white;
          }
          .first-txt {
            position: absolute;
            top: 17px;
            left: 50px;
          }

          .second-txt {
            position: absolute;
            bottom: 20px;
            left: 10px;
          }

      </style>

  <!--<style type="text/css">
              .button {
              display: none;
            }
            .wrapper:hover img {
              
            }
            .wrapper:hover .button {
              display: inline-block;
            }
      </style> -->

</head>
<body class="hold-transition skin-blue sidebar-mini">

  <!-- This Code For Using Select Box To SELECT VALUES Show Input Box -->
<!--   <script type="text/javascript">
    $(document).ready(function(){
    $('#chkvendor').on('change', function() {
      if ( this.value == 'null')
      {
        $("#business").show();
      }
      else
      {
        $("#business").hide();
      }
    });
});
  </script> -->
<div class="wrapper">
	  <div class="content-wrapper">
		    <section class="content-header">
		      <h1>Tyre Issue/Receive</h1>
		    </section>
		    <section class="content">
		    	<div class="box box-info">
		            <div class="box-body">
		              	<h3 class="box-title" style="font-size:30px;">Dismount Tyre From Vehicle:</h3>

	             <form method="post" autocomplete="off">
		           	<div class="row-md-2">
			           		<div class="col-md-4">
				              	<div class="form-group">
				                <label>Tyre No.</label>
								        	<input type="text" id="rec_tyre_no" name="rec_tyre_no" required="required" class="form-control"/>
				              	</div>
                        <?php 
                        $usernameSet=$_SESSION['username'] ;
                        ?>
				              <div class="form-group">
				                	<label>Receiving Date:</label>
				                	<input type="hidden" name="office" id="office"  class="form-control" value="<?php echo $usernameSet; ?>"/>
								        	<input type="text" required="required" name="receiving_date" id="receiving_date" class="form-control" />
				              </div>
                        <input type="hidden" name="brand" id="brand1"  class="form-control" />
                        <input type="hidden" name="grn_date" id="grn_date"  class="form-control" />
                        <input type="hidden" name="ProductSize" id="ProductSize"  class="form-control"/>
				            	<div class="form-group">
				                	<label for="chkvendor"><font color="red">*</font>Reason For Receiving</label>
				              <select class="form-control" id="chkvendor" onclick="ShowHideDiv(this);" name="reason_for_receiving" required="required">
  									    	   <option  value="">---Reason For Receiving---</option>
  										       <option  value="temp_remould">Send For Remoluld</option>
  									    	   <option  value="temp_claim">Send For Claim</option>
  										       <option  value="temp_scrap">Send For Scrap</option>
                             <option  value="temp_theft">Send For Theft</option>
  										       <option  value="null">On Vehicle To Another</option>
  									     </select>
<!--                             <div style='display:none;' id='business'>Business Name<br/>&nbsp;
                            <br/>&nbsp;
                            <input type='text' class='text' name='business' value size='20' />
                            <br/>
                            </div> -->
							      	</div>
                      <div class="form-group">      
                          <label>Remark:</label>
                         <input type="text"  name="reason_remark" required="required" class="form-control" id="reason_remark" />
                      </div>
					        </div>
  					        <div class="col-md-4"> 
        					        <div class="form-group">
        				            <label>Vehicle No:</label>
        									  <input type="text" onblur="getKM(this.value);" readonly="readonly" name="vehicle_no" class="form-control" id="vehicle_no_to_receive" required/>
  				              	</div>

  				              	<div class="form-group">
  				                	<label>KM Reading:</label>
  				                	<input type="text"  id="km_run" name="km_reading" readonly="readonly" class="form-control" required/>
  				              	</div>

  				              	<div class="form-group">
  				                	<label>Wheel Position :</label>
  				                	<input type="text"  id="wp_for_recv" name="wp" readonly="readonly" class="form-control" required/>
  				              	</div>

                          <div class="form-group">
                            <label>Tyre Brand :</label>
                            <input type="text"  id="brand_name" name="brand_name" readonly="readonly" class="form-control" required/>
                          </div>
    					         </div>

                          <div class="col-md-4"> 
                                 <div class="form-group">
                                    <label>Receive Psi:</label>
                                      <input type="text" onblur="getKM(this.value);"  name="receive_psi" class="form-control" id="receive_psi" required/>
                                  </div>

                                  <div class="form-group">
                                    <div class="col-md-6">Nsd1<input type="Number" required class="form-control" name="nsd1"></div>
                                     <div class="col-md-6">Nsd2<input type="Number" required class="form-control" name="nsd2"></div><br><br><br>
                                  </div>

                                  <div class="form-group">
                                     <div class="col-md-6">Nsd3<input type="Number" class="form-control" name="nsd3"></div>
                                      <div class="col-md-6">Nsd4<input type="Number" class="form-control" name="nsd4"></div><br><br><br><br>
                                  </div>
                          </div>


                                 <button style="float: right; margin-right: 20px;" type="submit" class="btn btn-info" formaction="insert_tyre_receive_add.php">Receive Save</button>
                                 <br><br><br>
                                 <div id="result"></div> 
                        </div>
                     </form>
                        <!-- Issue Tyre on Vehicle: -->
                     <div class="row-md-2">
                       <div class="col-md-6">
                        <form>
                                <h3 class="box-title" style="font-size:30px;">Mount Tyre on Vehicle:</h3>
                                <div class="form-group">
                                  <label>Vehicle No:</label>
                                  <input type="text"  name="vehicle_no" class="form-control" id="vehicle_no" required/>
                                </div>
                                <div class="form-group">
                                  <label>Wheeler:</label>
                                  <input type="text" name="wheeler" id="wheeler" class="form-control"  placeholder="Wheeler" readonly />
                                </div>
                          </form> 
                          </div> <br><br>
                            <div class="col-md-6">
                                 <center><div id="axis"></div></center>
                            </div>
                     </div>
			           </div>
				  	</div>  
        </section>
      </div>
        <?php include("../footer.php");?>
		</div>
</body>
</html>
<form id="form" method="POST" autocomplete="off"> 
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Tyre Details</h4>
        </div>

        <div class="modal-body">
          <div id="alert1" style="font-size:18px; color: red; " hidden>
            Tyre already inserted on this position...
          </div><br>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Wheel Position:</label>
                <input type="text" class="form-control" name="wp" id="wp" readonly/>
                 <input type="hidden" name="office" id="office2" class="form-control" value="<?php echo $usernameSet; ?>"/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Number:</label>
                <input type="text" class="form-control" name="vehicle_no_modal" id="vehicle_no_modal" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
              <label>Odometer Reading:</label>
              <input type="text" class="form-control" name="odo_read_modal" id="odo_read_modal" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Tyre Number:</label>
                <input type="text" class="form-control" name="tyre_no" id="iss_tyre_no" onblur="getTyreNo(this.value);" required/>
                <div id="result3"></div> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Opening KM:</label>
                <input type="text" class="form-control" required="required" name="open_km" id="open_km" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label>Production Month:</label>
                  <input type="text" class="form-control" name="product_month" id="product_month" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Brand Name:</label>
                <input type="text" class="form-control" name="brand" id="brand" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Tyre Type:</label>
                <input type="text" class="form-control" name="type" id="type" readonly/>
              </div>
            </div>
           
            <div class="col-md-4">
              <div class="form-group">
                <label>PSI:</label>
                <input type="text" class="form-control"  required="required" name="psi" id="psi" />
              </div>
            </div>
          

            <div class="col-md-4">
              <div class="form-group">
                <label>Amount:</label>
                <input type="number" class="form-control"  required="required" name="amount" id="total_amount" readonly />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Vertual Amount:</label>
                <input type="number" class="form-control"  required="required" name="vertual_amount" id="vertual_amount" readonly />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Remarks:</label>
                <input type="text" class="form-control"  required="required" name="remarks" id="remarks"/>
              </div>
            </div>

               <div class="col-md-4">
              <div class="form-group">
                <label>Issue Date:</label>
                <input type="text" class="form-control" required="required" name="issue_date" id="issue_date"/>
              </div>
            </div>
           <!--  <button onclick="myFunction()">Refresh</button> -->
          </div>
        </div>

        <script type="text/javascript">

              	$(document).ready(function(){  
      			   $.datepicker.setDefaults({  
      			        dateFormat: 'yy-mm-dd'   
      			   });  
      			   $(function(){  
      			        $("#issue_date").datepicker();
      			   });
      			});
                
        </script>

        <div class="modal-footer">
          <button type="submit" id="Submit_me" onclick="myfunction();" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</form> 
<div id="result22"></div>
<div id="save_result"></div>


<!-- <script type="text/javascript">
    $(document).ready(function() {
   $('[data-toggle="popover"]').popover({
      placement: 'top',
      trigger: 'hover'
   });
});
</script> -->

<script type="text/javascript">
  
  function buttonFunction(el) {
  el.style.backgroundColor = "red";
}

</script>
<script> 
	function myfunction()
		{
        var wp = $('#wp').val();
			  var vehicle_no_modal = $('#vehicle_no_modal').val();
			  var odo_read_modal = $('#odo_read_modal').val();
			  var iss_tyre_no = $('#iss_tyre_no').val();
			  var open_km = $('#open_km').val();
			  var product_month = $('#product_month').val();
			  var brand = $('#brand').val();
			  var remould = $('#remould').val();
			  var rubber_brand = $('#rubber_brand').val();
			  var rubber_type = $('#rubber_type').val();
			  var psi = $('#psi').val();
			  var total_amount = $('#total_amount').val();
			  var remarks = $('#remarks').val();
		    var issue_date = $('#issue_date').val();
        var vertual_amount = $('#vertual_amount').val();
		    var office = $('#office2').val();  
                 if( (iss_tyre_no!='')&&(open_km!='')&&(psi!='')&&(issue_date!=''))
                    {
                      $.ajax({
                        type: "POST",
                        url: "save_tyre_log.php",
                        data:'wp='+wp + '&vehicle_no_modal='+vehicle_no_modal + '&odo_read_modal='+odo_read_modal + '&iss_tyre_no='+iss_tyre_no + '&open_km='+open_km + '&product_month='+product_month+ '&brand='+brand + '&rubber_brand='+rubber_brand + '&rubber_type='+rubber_type + '&psi='+psi + '&total_amount='+total_amount + '&remarks='+remarks+ '&remould='+remould+ '&issue_date='+issue_date+ '&office='+office+ '&vertual_amount='+vertual_amount,
                          success: function(data){
                           $("#save_result").html(data);
                          } 
                      });
                    }
                
		}

    function success_msg(){
      alert("tyre Add Successfuly");

    }
    function change_btn_color(wp){

      alert(wp);
      alert("funtion call");
    }

function setEventId(event_id){
  $('#wp').val(event_id);
  var total_tyre=document.getElementById('wheeler').value;
  $('#total_tyre').val(total_tyre);
  var vehicle_no=document.getElementById('vehicle_no').value;
  $('#vehicle_no_modal').val(vehicle_no);
   $.ajax({
    type: "POST",
    url: "check_added_tyre.php",
    data:'wp=' + event_id + '&vehicle_no=' + $('#vehicle_no').val(),
    success: function(data){
       $("#result22").html(data);
      }
    });
  }

  $(function()
{ 
  $("#vehicle_no").autocomplete({
  source: 'search_vehicle_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vehicle Name does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

$("#vehicle_no").change(function(){
  var vehicle_no=$("#vehicle_no").val();
  $.ajax({
    type: "POST",
    url: "chk_tyre_for_color.php",
    data:'vehicle_no=' + vehicle_no,
    success: function(data){
      
      }
    });


  $.get("search_vehicle_no.php",{txt:vehicle_no},function(data,status){
    $("#vehicle_no").empty();
    $("#wheeler").empty();
    
    var arr = $.parseJSON(data);
    $("#vehicle_no").val(arr[0]);
    $("#wheeler").val(arr[1]);
     var wheeler=arr[1];
      if(wheeler=='4')    
      {  
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1'  value='LF' onclick='setEventId(this.value);'type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
          "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><br><br><br><br>"+
          "<button id='LR' data-toggle='modal' data-target='#myModal' class='button button1' value='LR'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button> <img src='image/axsel.jpeg' style='height: 10px;'/>"+
          "<button id='RR' data-toggle='modal' data-target='#myModal' class='button button1' value='RR'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><br><br>"+
          "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1'class='button button1' value='STP'  onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;' /></button>";
      }
      else if(wheeler=='6'){
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;' /></button>";
      }
      else if(wheeler=='10'){
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br><br>"+
        "<button id='RL1' data-toggle='modal' data-target='#myModal' class='button button1' value='RL1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='RL3' data-toggle='modal' data-target='#myModal' class='button button1' value='RL3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL4' data-toggle='modal' data-target='#myModal' class='button button1' value='RL4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";
         }
      else if(wheeler=='12'){

        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px ;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='14'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF2' data-toggle='modal' data-target='#myModal' class='button button1' value='LF2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF2' data-toggle='modal' data-target='#myModal' class='button button1' value='RF2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='16'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR5' data-toggle='modal' data-target='#myModal' class='button button1' value='LR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR6' data-toggle='modal' data-target='#myModal' class='button button1' value='LR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR6' data-toggle='modal' data-target='#myModal' class='button button1' value='RR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR5' data-toggle='modal' data-target='#myModal' class='button button1' value='RR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='18'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='22'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='button button1' value='FL' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='button button1' value='FR' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='RL1' data-toggle='modal' data-target='#myModal' class='button button1' value='RL1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      });

    });

		$(function()
		{ 
		  $("#tyre_no").autocomplete({
		  source: 'search_tyre_no_auto.php',
		  change: function (event, ui) {
		  if(!ui.item){
		  $(event.target).val(""); 
		  $(event.target).focus();
		  alert('Tyre Number does not exists');
		  } 
		},
		focus: function (event, ui) { return false; } }); });

		function getTyreNo(elem)
		{
		  var tyre_no = elem;
		  if(tyre_no!='')
		  {
		    $.ajax({
		      type: "POST",
		      url: "search_tyre_no.php",
		      data:'tyre_no='+tyre_no,
		      success: function(data){
		        $("#result3").html(data);
		      }
		    });
		  }
		}

 $(function()
    { 
      $("#rec_tyre_no").autocomplete({
      source: 'autocomplete_tyre_no.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre number does not exist in our stock');
      $("#rec_tyre_no").val('');
      } 
    },
    focus: function (event, ui) { return false; } }); });


 $(function()
    { 
      $("#iss_tyre_no").autocomplete({
      source: 'autocomplete_tyre_no_for_issue.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre number does not exist in our stock');
      $("#iss_tyre_no").val('');
      } 
    },
    focus: function (event, ui) { return false; } }); });

 $("#rec_tyre_no").change(function(){
      var rec_tyre_no=$("#rec_tyre_no").val();
      $.get("fetch_data_on_tyreno.php",{txt:rec_tyre_no},function(data,status){
        var arr = $.parseJSON(data);
          $("#vehicle_no_to_receive").val(arr[0]);
            $("#km_run").val(arr[1]);
             $("#wp_for_recv").val(arr[2]);
              $("#brand_name").val(arr[3]);
             $("#ProductSize").val(arr[4]);
            $("#grn_date").val(arr[5]);
      });
    });

$(document).ready(function(){  
   $.datepicker.setDefaults({  
        dateFormat: 'yy-mm-dd'   
   });  
   $(function(){  
        $("#receiving_date").datepicker();
   });
});

$(function()
{ 
  $("#vehicle_no").autocomplete({
  source: 'search_vehicle_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vehicle Name does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

</script>


