<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_tyre_with_chassis.php" enctype="multipart/form-data" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Tyre With Chassis</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
       
          <div class="container" style="width: 100%">
            <div class="table-wrapper">
              <div class="table-title">
                <div class="row">
                  <div class="col-sm-10">
                      <div class="row">
                        <div class="col-md-2"> <label>Chassis No:</label></div>
                         <input type="text" name="chassis_no" id="chassis_no" class="form-control" style="width: 338px;" required="required" placeholder="Chassis No." />
                          <input type="hidden" name="office" id="office" value="<?php echo $usernameSet; ?>" class="form-control" style="width: 338px;" required="required" placeholder="Chassis No." />
                      </div>  
                    </div>
                     <div class="col-sm-2">
                      <div align="right">
                        <button type="button" class="btn btn-info add-new" onclick="addTyre('tyre_table')"> <i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                </div>
              </div><br>
              <div class="table-responsive"> 
                <table id="tyre_table" class="table table-bordered">
                  <thead>
                    <tr>
                        <th><font color="red">*</font>Tyre No</th>
                        <th><font color="red">*</font>Tyre Width(W)</th>
                        <th><font color="red">*</font>Section Height(H)</th>
                        <th>Rim Diameter</th>
                        <th><font color="red">*</font>Outer Diameter</th>
                       <th>Remove</th>
                         <th>Copy</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
           
            <center>
              <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
            </center>
          </div><br>
      </div>
    </div>
    </section>
     
  <br><br><br>
  </div>
</form>
<?php include("../footer.php");?>
</div>
</body>
</html>

<script type="text/javascript">
  function ShowHideDiv2(chkchallan) {
  var dvchallan_no = document.getElementById("dvchallan_no");
  dvchallan_no.style.display = chkchallan.checked ? "block" : "none";
  invoice_no.setAttribute( 'required', 'required' );
  var dvchallan_date = document.getElementById("dvchallan_date");
  dvchallan_date.style.display = chkchallan.checked ? "block" : "none";
  invoice_date.setAttribute( 'required', 'required' );
  var dvchallan_file = document.getElementById("dvchallan_file");
  dvchallan_file.style.display = chkchallan.checked ? "block" : "none";
  challan_file.setAttribute( 'required', 'required' );

  document.getElementById('chkchallan').onchange = function() {
                var yourText =   document.getElementById('chkinvoice').disabled = this.checked;
              };
}
</script>

<script>
function GetSumFunc(id)
{
  var amount = Number($('#amount'+id).val());
  var dis_perc = Number($('#dis_percent'+id).val());
  var tg = Number($('#total_gst1').val());
  var dis_amt = Number($('#dis_amt'+id).val());
  var total_amt = Number($('#total_amt'+id).val());
  
  var total_gst1 = amount*tg/100;
  var dis_value = (amount*dis_perc/100).toFixed(2);
  
  $('#dis_amt'+id).val(dis_value);
  $('#total_amt'+id).val(((amount-dis_value)+total_gst1).toFixed(2));
}

function DisCountFunc(id)
{
  var amount = Number($('#amount'+id).val());
  var dis_amt = Number($('#dis_amt'+id).val());
  var dis_perc1 = (dis_amt/amount*100).toFixed(2);
 
}
</script>

<script>
   function copy_text(id) {
    var id2  = id+1;
   
        var tyre_width1 = document.getElementById('tyre_width'+id).value; 
       $('#tyre_width'+id2).val(tyre_width1);

        var section_height1 = document.getElementById('section_height'+id).value; 
       $('#section_height'+id2).val(section_height1);

        var rim_diameter1 = document.getElementById('rim_diameter'+id).value; 
       $('#rim_diameter'+id2).val(rim_diameter1);

        var outer_diameter1 = document.getElementById('outer_diameter'+id).value; 
       $('#outer_diameter'+id2).val(outer_diameter1);
        }
</script>
<script>
function addTyre(tyre_table) {

    var rowCount = document.getElementById("tyre_table").rows.length;
    var row = document.getElementById("tyre_table").insertRow(rowCount);
  
    var cell1 = row.insertCell(0);
    cell1.innerHTML ="<input type='text' style='width: 180px;' name='tyre_no[]' id='tyre_no"+rowCount+"' required/>";
    
    var cell2 = row.insertCell(1);
    cell2.innerHTML = "<input type='text' style='width: 150px;' name='tyre_width[]' id='tyre_width"+rowCount+"' required/> ";
    
    var cell3 = row.insertCell(2);
    cell3.innerHTML = "<input type='text' style='width: 120px;' name='section_height[]' id='section_height"+rowCount+"' required/>";
 

    var cell4 = row.insertCell(3);
    cell4.innerHTML =  "<input type='text' style='width: 120px;' name='rim_diameter[]' id='rim_diameter"+rowCount+"'/>";

    var cell5 = row.insertCell(4);
    cell5.innerHTML =  "<input type='number' style='width: 120px;'  min='0' name='outer_diameter[]' id='outer_diameter"+rowCount+"' required />";

    var cell6 = row.insertCell(5);
    cell6.innerHTML =  "<button type='button' class='btn btn-danger' id='remove"+rowCount+"'><i class='fa fa-remove'></i></button>";

     var cell7 = row.insertCell(6);
    cell7.innerHTML =  "<input type='button' id='copy"+rowCount+"' onclick='copy_text("+rowCount+",this.id)' value='copy'>";

    $("#remove"+rowCount).click(function(){
     $(this).parents("tr").remove();
    });

   }
</script>



