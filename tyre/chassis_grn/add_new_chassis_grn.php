<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_temp_new_chassis.php" enctype="multipart/form-data" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      
       <a href='approve_chassistyre.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Approve Chassis Tyre</a>

      <h1>Add Tyre in New Chassis</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
         
         </div> 
         <?php  $usernameSet=$_SESSION['username'];   ?>
           <?php if(isset($_SESSION['new_chassis'])) {
              echo "  <SCRIPT>
                      window.location.href='fetch_cross_chk_new_chassis.php';
                   </SCRIPT>";
                   exit();
               }
           ?>
           <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>
        
          <div class="col-md-6">
           <!--    <?php echo $usernameSet;?> -->
            <div class="row" >
              <div class="col-md-3"> <label><font color="red">*</font>Chassis No:</label></div>
              <div class="col-md-7">
                <input type="text" name="chassis_no" id="chassis_no"  onblur="chk_chassis()" required="required" class="form-control" style="width: 282px;" placeholder="Chassis No" />
              </div>
            </div>
          </div>

          <div id="result22"></div>
                <script>
                function chk_chassis()
                {
                  var chassis_no = $('#chassis_no').val();
                  if(chassis_no!='')
                  {
                    $.ajax({
                        type: "POST",
                        url: "chkchassis_no.php",
                        data:'chassis_no='+chassis_no,
                        success: function(data){
                            $("#result22").html(data);
                          }
                      });
                  }
                }     

               function not_success_chassis()
                {
                    alert("This Chassis No is alrady inserted in stock");
                       $("#chassis_no").val("");
                } 
              </script>


          <!-- </form> -->
          <div class="container" style="width: 100%">
            <div class="table-wrapper">
              <div class="table-title">
                <div class="row">
                  <div class="col-sm-10"><h3>Tyre Details</h3></div>
                  <div class="col-sm-2">
                    <div align="right">
                      <button type="button" class="btn btn-info add-new" id="add" onclick="addTyre('tyre_table')"> <i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive"> 
                <table id="tyre_table" class="table table-bordered">
                  <thead>
                    <tr>
                        <th><font color="red">*</font>Tyre No</th>
                        <th><font color="red">*</font>Production Month</th>
                        <th><font color="red">*</font>Brand</th>
                              <th><font color="red">*</font>Product Size</th>
                              <th><font color="red">*</font>Ply</th>
                              <th><font color="red">*</font>Max Nsd</th>
                              <th><font color="red">*</font>Min Nsd</th>
                        <th>Remarks</th>
                        <th >Vertual Amount</th>
                        <th>Remove</th>
                        <th>Copy</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div><br>
           
            <center>
              <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
            </center>
          </div><br>
      </div>
      </section>
    </div>
    
   </form>  
  </div>

<?php include("../footer.php");?>

</body>
</html>


<script>
$("#total_gst1").change(function(){
  var vendor =  document.getElementById("vendor").value;
  var str =  document.getElementById("gstin").value;
  var res = str.substring(0,2);
  var total_gst =  document.getElementById("total_gst1").value;
  if(vendor!='')
  {
    if(res=='24')
    {
      $('#cgst').val(total_gst/2);
      $('#sgst').val(total_gst/2);
    }
    else{
      $('#igst').val(total_gst);
    }
  }
  else{
    alert('select vendor name firstly');
  }
});
function GetSumFunc(id)
{
  var amount = Number($('#amount'+id).val());
  var dis_perc = Number($('#dis_percent'+id).val());
  if (dis_perc > 100) {
    alert("more then 100 discount is not allowed");
     $('#dis_percent'+id).val('');
  }else{
  var tg = Number($('#total_gst1'+id).val());
  var dis_amt = Number($('#dis_amt'+id).val());
  var total_amt = Number($('#total_amt'+id).val());
  var total_gst1 = amount*tg/100;
  var dis_value = (amount*dis_perc/100).toFixed(2);
  var discount = Math.round(dis_value);
  $('#dis_amt'+id).val(discount);

  var total = (((amount-discount)+total_gst1).toFixed(2));
  $('#total_amt'+id).val(total);
}

}

  function DisCountFunc(id)
  {
    var amount = Number($('#amount'+id).val());
    var dis_amt = Number($('#dis_amt'+id).val());
    var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
  }
</script>

<script>

    function copy_text(id) {
            var id2  = id+1;
            //alert(id2);
               var weekPicker1 = document.getElementById('weekPicker1'+id).value; 
               $('#weekPicker1'+id2).val(weekPicker1);

                var brand = document.getElementById('brand'+id).value; 
               $('#brand'+id2).val(brand);

                var remarks = document.getElementById('remarks'+id).value; 
               $('#remarks'+id2).val(remarks);

              /* extra column*/

               var ProductSize = document.getElementById('ProductSize'+id).value; 
               $('#ProductSize'+id2).val(ProductSize);

               var ProductPlyRating = document.getElementById('ProductPlyRating'+id).value; 
               $('#ProductPlyRating'+id2).val(ProductPlyRating);

               var productMaxNsd = document.getElementById('productMaxNsd'+id).value; 
               $('#productMaxNsd'+id2).val(productMaxNsd);

               var vertual_amount = document.getElementById('vertual_amount'+id).value; 
               $('#vertual_amount'+id2).val(vertual_amount);

               var productMinNsd = document.getElementById('productMinNsd'+id).value; 
               $('#productMinNsd'+id2).val(productMinNsd);
             }
</script>
  <div id="result22"></div>
         <script>
              function MyFunc(id)
              { id = id;
                var office = '<?php echo $usernameSet; ?>';
                var tyre_no = $('#tyre_no'+id).val();
                if(tyre_no!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chktyreno.php",
                      data:{tyre_no:tyre_no,office:office,id:id},
                      success: function(data){
                       $("#result22").html(data);
                       }
                    });
                }
              }  

              function not_success_msg(id) {
              alert("this tyre no is in pending queue");
              $('#tyre_no'+id).val('');
               //alert("success");
              }

               function not_success_msg2(id) {
               alert("this tyre no is inserted in stock");
              $('#tyre_no'+id).val('');
               //alert("success");
              }
            </script>

 <div id="result1"></div>
 <script type="text/javascript">
  

 </script>
<script>
    function addTyre(tyre_table) {
    
        var rowCount = document.getElementById("tyre_table").rows.length;
        var row = document.getElementById("tyre_table").insertRow(rowCount);
      
        var cell1 = row.insertCell(0);
        cell1.innerHTML ="<input type='text' style='width: 120px;' name='tyre_no[]' onblur='MyFunc("+rowCount+",this.id)'  id='tyre_no"+rowCount+"' required/>";
        
        var cell2 = row.insertCell(1);
        cell2.innerHTML = "<input type='text' style='width: 120px;' name='product_month[]' id='weekPicker1"+rowCount+"' required/> ";
        
        var cell3 = row.insertCell(2);
        cell3.innerHTML = "<input type='text' style='width: 120px;' name='brand[]' id='brand"+rowCount+"' required/>";

       /*extra column*/
         var cell4 = row.insertCell(3);
         cell4.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductSize[]' id='ProductSize"+rowCount+"' required/>";

          var cell5 = row.insertCell(4);
         cell5.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductPlyRating[]' id='ProductPlyRating"+rowCount+"' required/>";

          var cell6 = row.insertCell(5);
         cell6.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMaxNsd[]' id='productMaxNsd"+rowCount+"' required/>";

          var cell7 = row.insertCell(6);
         cell7.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMinNsd[]' id='productMinNsd"+rowCount+"' required/>";

        /*End of extra column*/
        
        var cell8 = row.insertCell(7);
        cell8.innerHTML =  "<input type='text' style='width: 120px;' name='tyre_remarks[]' id='remarks"+rowCount+"'/>";

         var cell9 = row.insertCell(8);
        cell9.innerHTML =  "<input type='text' style='width: 120px;' name='vertual_amount[]' id='vertual_amount"+rowCount+"'/>";

        var cell10 = row.insertCell(9);
        cell10.innerHTML =  "<button type='button' class='btn btn-danger' id='remove"+rowCount+"'><i class='fa fa-remove'></i></button>";

        var cell11 = row.insertCell(10);
        cell11.innerHTML =  "<input type='button' id='copy"+rowCount+"' onclick='copy_text("+rowCount+",this.id)' value='copy'>";

        $("#remove"+rowCount).click(function(){
         $(this).parents("tr").remove();
        });

    convertToWeekPicker($("#weekPicker1"+rowCount))
    $(function()
    { 
      $("#brand"+rowCount).autocomplete({
      source: 'search_brand_name_auto.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre Brand Name does not exists');
      } 
    },
    focus: function (event, ui) { return false; } }); });

    $("#brand"+rowCount).change(function(){
      var brand=$("#brand"+rowCount).val();
      $.get("search_brand_data.php",{txt:brand},function(data,status){
        var arr = $.parseJSON(data);
        $("#ProductSize"+rowCount).val(arr[0]);
        $("#ProductPlyRating"+rowCount).val(arr[1]);
        $("#productMaxNsd"+rowCount).val(arr[2]);
        $("#productMinNsd"+rowCount).val(arr[3]);
         });
    });

        if(document.getElementById('chkchallan').checked)
          {
            ShowHideDiv2(this);
               }
           else{
            if(document.getElementById('chkinvoice').checked)
            {
              ShowHideDiv(this);
            }
          }
}
</script>
<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}
</style>
<script>
  function invoice_select() {
    $(document).ready(function() {
      $ ('#invoice_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#invoice_file').val('');
          }
      });
    });
  }
  function challan_select() {
    $(document).ready(function() {
      $ ('#challan_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#challan_file').val('');
          }
      });
    });
  }
</script>
<script type="text/javascript">            
          
$(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    $("#challan_date").datepicker();
 });
});
</script>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="src/weekPicker.js"></script>
   

