<?php 
  require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Chassis GRN</h1>
    </section>

    <section class="content">
     
    <div class="box box-info">
       <div class="box-body">
          <div class="table-responsive"><br>
            <div class="row">
              <div class="col-md-12" align="right"> 
                <a href='pending_chassis_truck.php' type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i>&nbsp;Pending Chassis Truck GRN</a>&nbsp;&nbsp;
                <a href='add_chassis_grn.php' type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Add Tyre on Chassis</a>
              </div>
              <div class="input-daterange">
                <div class="col-md-5">
                  <label><font color="red">*</font>From Date:</label>
                  <input type="text" name="start_date" id="start_date" class="form-control"/>
                </div>
                <div class="col-md-5">
                  <label><font color="red">*</font>To Date:</label>
                  <input type="text" name="end_date" id="end_date" class="form-control" />
                </div>      
              </div>
              <div class="col-md-2"><br>
                <input type="button" name="search" id="search" value="Search" class="btn btn-info" />
              </div>
            </div><br>
            <table id="order_data" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ACTION</th>
                  <th>GRN NO.</th>
                  <th>TYPE</th> 
                  <th>DATE</th> 
                  <th>VENDOR</th> 
                  <th>OFFICE</th>
                  <th>CHALLAN NO.</th>
                  <th>INVOICE NO.</th> 
                  <th>COUNT</th> 
                  <th>AMOUNT</th> 
                </tr>
              </thead>
            </table>
            
           </div>
          </div>
        </div>

      </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html><script type="text/javascript" language="javascript" >
$(document).ready(function(){
 
 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "dd-mm-yyyy",
  autoclose: true
 });

 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#order_data').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order" : [],
   "ajax" : {
    url:"fetch_main_grn.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#search').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#order_data').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 }); 
 
});
</script>