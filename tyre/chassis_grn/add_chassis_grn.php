<?php 
  require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_add_chassis_grn.php" enctype="multipart/form-data" autocomplete="off">
    <div class="content-wrapper">
      <section class="content-header">
        <h1>Chassis Tyre</h1>
      </section>
      <section class="content">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <h3>Basic Details</h3>
              </div>
            </div>
            <div class="col-md-6"> 
              <div class="row">
                <div class="col-md-4"> <label><font color="red">*</font>Vehicle No.:</label></div>
                <div class="col-md-8">
                  <input type="text" name="vehicle_no" id="vehicle_no" class="form-control vno1" style="width: 338px;" placeholder="Vehicle No." required="required" />
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-4"> <label>Wheeler:</label></div>
                <div class="col-md-8">
                  <input type="text" name="wheeler" id="wheeler" class="form-control" style="width: 338px;" placeholder="Wheeler" readonly />
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-4"> <label>Odometer Reading:</label></div>
                <div class="col-md-8">
                  <input type="text" name="odo_read" id="odo_read" class="form-control" style="width: 338px;" placeholder="Odometer Reading" readonly/>
                </div>
              </div><br>
             <!--  <div class="row">
                <div class="col-md-4"> <label>Remark:</label></div>
                <div class="col-md-8">
                  <input type="text" name="remark" id="remark" class="form-control" style="width: 338px;" placeholder="Remark" />
                </div>
              </div> <br> -->
            </div>

            <div class="col-md-6">
              <center> 
                <div id="axis">
                </div>
              </center>
            </div>
           
          </div>
          
          <br><br><br><br>
        </div>
      </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>

<form id="form" method="POST" autocomplete="off">
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Tyre Details</h4>
        </div>

        <div class="modal-body">
          <div id="alert1" style="font-size:18px; color: red; " hidden>
            Tyre already inserted on this position...

          </div><br>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Wheel Position:</label>
                <input type="text" class="form-control" name="wp" id="wp" readonly/>
                <!-- <input type="text" class="form-control" name="total_tyre" id="total_tyre" readonly/> -->
                <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Number:</label>
                <input type="text" class="form-control" name="vehicle_no_modal" id="vehicle_no_modal" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Odometer Reading:</label>
                <input type="text" class="form-control" name="odo_read_modal" id="odo_read_modal" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Tyre Number:</label>
                <input type="text" class="form-control" name="tyre_no" id="tyre_no" onblur="getTyreNo(this.value);" required/>
                <div id="result3"></div> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Opening KM:</label>
                <input type="text" class="form-control" name="open_km" id="open_km" readonly />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label>Production Month:</label>
                  <input type="text" class="form-control" name="product_month" id="product_month" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Brand Name:</label>
                <input type="text" class="form-control" name="brand" id="brand" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Is Remould:</label>
                <input type="text" class="form-control" name="remould" id="remould" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Rubber Brand:</label>
                <input type="text" class="form-control" name="rubber_brand" id="rubber_brand" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Rubber Type:</label>
                <input type="text" class="form-control" name="rubber_type" id="rubber_type" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>PSI:</label>
                <input type="text" class="form-control" name="psi" id="psi" readonly/>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Amount:</label>
                <input type="number" class="form-control" name="amount" id="amount" readonly />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Remarks:</label>
                <input type="text" class="form-control" name="remarks" id="remarks"/>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" id="Submit_me" onclick="myfunction();" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</form>
<div id="result22"></div>
<div id="save_result"></div>
<script>

  $(document).ready(function(){
  $("#Submit_me").click(function(e){
      e.preventDefault();  //add this line to prevent reload
      var wp = $('#wp').val();
    // alert(wp)
      var office = $('#office').val();
      var vehicle_no_modal = $('#vehicle_no_modal').val();
      var odo_read_modal = $('#odo_read_modal').val();
      var tyre_no = $('#tyre_no').val();
      var open_km = $('#open_km').val();
      var product_month = $('#product_month').val();
      var brand = $('#brand').val();
      var remould = $('#remould').val();
      var rubber_brand = $('#rubber_brand').val();
      var rubber_type = $('#rubber_type').val();
      var psi = $('#psi').val();
      var amount = $('#amount').val();
      var remarks = $('#remarks').val();
      if(tyre_no!='')
      {
        $.ajax({
          type: "POST",
          url: "save_tyre_log.php",
          data:'wp='+wp +'&office='+  office + '&vehicle_no_modal='+vehicle_no_modal + '&odo_read_modal='+odo_read_modal + '&tyre_no='+tyre_no + '&open_km='+open_km + '&product_month='+product_month+ '&brand='+brand + '&rubber_brand='+rubber_brand + '&rubber_type='+rubber_type + '&psi='+psi + '&amount='+amount + '&remarks='+remarks+ '&remould='+remould,
          success: function(data){
            //alert(data);
            $("#save_result").html(data);
          }
          });
      }
     });
});

function myfunction()
{
  var wp = $('#wp').val();
  var vehicle_no_modal = $('#vehicle_no_modal').val();
  var odo_read_modal = $('#odo_read_modal').val();
  var tyre_no = $('#tyre_no').val();
  var open_km = $('#open_km').val();
  var product_month = $('#product_month').val();
  var brand = $('#brand').val();
  var remould = $('#remould').val();
  var rubber_brand = $('#rubber_brand').val();
  var rubber_type = $('#rubber_type').val();
  var psi = $('#psi').val();
  var amount = $('#amount').val();
  var remarks = $('#remarks').val();

if(tyre_no!='')
{
  $.ajax({
    type: "POST",
    url: "save_tyre_log.php",
    data:'wp='+wp + '&vehicle_no_modal='+vehicle_no_modal + '&odo_read_modal='+odo_read_modal + '&tyre_no='+tyre_no + '&open_km='+open_km + '&product_month='+product_month+ '&brand='+brand + '&rubber_brand='+rubber_brand + '&rubber_type='+rubber_type + '&psi='+psi + '&amount='+amount + '&remarks='+remarks+ '&remould='+remould,
    success: function(data){
      $("#save_result").html(data);
    }
    });
}
}     
</script>
<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}

button.btnBlack {
    background-color: #000000 !important;
    width:20px; 
    height:50px;
}
</style>
      <style>

      .button1 {
        background-color: white; 
        color: black; 
        border: 2px;

      }

      .button1:hover {
        background-color: #4CAF50;
        color: white;
      }
              .first-txt {
                  position: absolute;
                  top: 17px;
                  left: 50px;
              }
        
              .second-txt {
                  position: absolute;
                  bottom: 20px;
                  left: 10px;
              }

      </style>
<script>

function setEventId(event_id){
  //alert(event_id)
  $('#wp').val(event_id);
  var total_tyre=document.getElementById('wheeler').value;
  $('#total_tyre').val(total_tyre);
  var vehicle_no=document.getElementById('vehicle_no').value;
  $('#vehicle_no_modal').val(vehicle_no);
  var odometer_read=document.getElementById('odo_read').value;
  $('#odo_read_modal').val(odometer_read);

  $.ajax({
    type: "POST",
    url: "check_added_tyre.php",
    data:'wp=' + event_id + '&vehicle_no=' + $('#vehicle_no').val(),
    success: function(data){
       changeColor();
        $("#result22").html(data);
      }
    });
}

// function changeColor() {
//              alert("button");
//         }

$(function()
{ 
  $("#vehicle_no").autocomplete({
  source: 'search_vehicle_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vehicle Number does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

/*function getVehicleNo(elem)
{
  var vehicle_no = elem;
  if(vehicle_no!='')
  {
    $.ajax({
      type: "POST",
      url: "search_vehicle_no.php",
      data:'vehicle_no='+vehicle_no,
      success: function(data){
        $("#result2").html(data);
      }
    });
  }
}*/
$("#vehicle_no").change(function(){
  var vehicle_no=$("#vehicle_no").val();
  $.get("search_vehicle_no.php",{txt:vehicle_no},function(data,status){
    $("#vehicle_no").empty();
    $("#wheeler").empty();
    $("#odo_read").empty();
    
    var arr = $.parseJSON(data);
    $("#vehicle_no").val(arr[0]);
    $("#wheeler").val(arr[1]);
    $("#odo_read").val(arr[2]);
    $('#vehicle_no').attr('readonly',true);
    var wheeler=arr[1];
      if(wheeler=='4')
      {
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1'  value='LF' onclick='setEventId(this.value);'type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
          "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><br><br><br><br>"+
          "<button id='LR' data-toggle='modal' data-target='#myModal' class='button button1' value='LR'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button> <img src='image/axsel.jpeg' style='height: 10px;'/>"+
          "<button id='RR' data-toggle='modal' data-target='#myModal' class='button button1' value='RR'  onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 60px;' /></button><br><br>"+
          "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1'class='button button1' value='STP'  onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;' /></button>";
      }
      else if(wheeler=='6'){
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;'/> </button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;' /></button>";
      }
      else if(wheeler=='10'){
        document.getElementById("axis").innerHTML = "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br><br>"+
        "<button id='RL1' data-toggle='modal' data-target='#myModal' class='button button1' value='RL1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='RL3' data-toggle='modal' data-target='#myModal' class='button button1' value='RL3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL4' data-toggle='modal' data-target='#myModal' class='button button1' value='RL4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='12'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px ;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";       
      }
      else if(wheeler=='14'){
       document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF2' data-toggle='modal' data-target='#myModal' class='button button1' value='LF2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF2' data-toggle='modal' data-target='#myModal' class='button button1' value='RF2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";         
      }
      else if(wheeler=='16'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LF1' data-toggle='modal' data-target='#myModal' class='button button1' value='LF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF1' data-toggle='modal' data-target='#myModal' class='button button1' value='RF1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR5' data-toggle='modal' data-target='#myModal' class='button button1' value='LR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR6' data-toggle='modal' data-target='#myModal' class='button button1' value='LR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR6' data-toggle='modal' data-target='#myModal' class='button button1' value='RR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR5' data-toggle='modal' data-target='#myModal' class='button button1' value='RR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";          
      }
      else if(wheeler=='18'){
        document.getElementById("axis").innerHTML = 
        "<button id='LF' data-toggle='modal' data-target='#myModal' class='button button1' value='LF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='RF' data-toggle='modal' data-target='#myModal' class='button button1' value='RF' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='button button1' value='LR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='button button1' value='LR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";        
      }
      else if(wheeler=='22'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='button button1' value='FL' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 15px;'/>"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='button button1' value='FR' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br>"+
        "<button id='RL1' data-toggle='modal' data-target='#myModal' class='button button1' value='RL1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RL2' data-toggle='modal' data-target='#myModal' class='button button1' value='RL2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='button button1' value='RR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='button button1' value='RR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='button button1' value='LR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='button button1' value='LR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='button button1' value='RR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='button button1' value='RR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR2' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR1' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR4' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR3' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TLR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><img src='image/axsel.jpeg' style='height: 10px;'/>"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR6' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal' class='button button1' value='TRR5' onclick='setEventId(this.value);' type='button'><img src='image/ty12.jpg' style='height: 70px;' /></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' class='button button1' value='STP' onclick='setEventId(this.value);' type='button'><img src='image/re.png' style='height: 70px;'/></button>";         
      }
      });

    });

$(function()
{ 
  $("#tyre_no").autocomplete({
  source: 'search_tyre_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Tyre Number does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

function getTyreNo(elem)
{
  var tyre_no = elem;
  if(tyre_no!='')
  {
    $.ajax({
      type: "POST",
      url: "search_tyre_no.php",
      data:'tyre_no='+tyre_no,
      success: function(data){
        $("#result3").html(data);
      }
    });
  }
}
</script>