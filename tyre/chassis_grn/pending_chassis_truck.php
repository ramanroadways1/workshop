<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Pending Chassis GRN</h1>
    </section>

    <section class="content">
      <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-bordered table-striped">
                  <thead>  
                   <tr>  
                    <th>Vehicle No.</th>
                    <th>GRN NO.</th>
                   <!--  <th>GRN Date.</th> -->
                    <th>Total Tyre</th>
                    <th>Added Tyre</th>
                    <!-- <th>STATUS</th> -->
                   </tr>  
                  </thead>  
                  <?php  
                  $query = "SELECT count(grn_no) as grn_no,grn_date,tno,wheeler,count(tyre_no) as tyre_no FROM grn_over_tyre inner join own_truck where tno=vehicle_no group by vehicle_no";

                  $result = mysqli_query($conn, $query);
                  if(mysqli_num_rows($result) > 0)
                  {
                    while($row = mysqli_fetch_array($result))
                    { 
                    $grn_no = $row["grn_no"];
                    $grn_date = $row["grn_date"];
                    $tno = $row["tno"];
                    $wheeler = $row["wheeler"];
                    $tyre_no = $row['tyre_no'];
                   
                  ?>
                  <tr> 
                    <td>
                      <form method="POST">
                        <input formaction="add_chassis_grn2.php" type="submit" name="tno" value="<?php echo $tno; ?>" class="btn btn-xs btn-info" />
                      </form>
                    </td>
                    <td><?php echo $grn_no; ?></td>
                   <!--  <td><?php echo $grn_date?></td> -->
                    <td><?php echo $wheeler?></td>
                    <td><?php echo $tyre_no?></td>
                  </tr>  
                  <?php   
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>  
    </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 