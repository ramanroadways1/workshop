<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

<script type="text/javascript">
   $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    alert("data");
 });
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
       <a href='add_old_chassis_grn.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a>
      <h1>Upload Invoice Over Challan For Old Chassis Tyre</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php  
                                 $username =  $_SESSION['valid_user']; 
                                  $query = "SELECT  sl_no, grn_no,grn_date, tyre_count,challan_no,challan_date, challan_file FROM  main_grn WHERE type = 'OLD_CHS'  and  office='$username' AND length(challan_file) > 15 and length(invoice_file)< 15";

                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){
                                    ?>
                                    <thead>  
                                       <tr>  
                                         <th style="display: none;" scope="row">Id</th>
                                                <th>Chassis Number</th>
                                                <th>Total Tyre</th>
                                                <th style="width: 120px;">Chassis Date</th>
                                               <th style="width: 120px;">Challan File</th>

                                                <th >GST(%)</th>
                                                <th >Amount</th>
                                                <th >Discount(%)</th>
                                                <th >Discount(Rs.)</th>
                                                <th >Total Amount</th>

                                                <th >Invoice No</th>
                                                <th >Invoice Date</th>
                                                <th >Invoice File</th>

                                                <th>Submit</th>
                                       </tr>  
                                  </thead>  
                                     <?php
                                    while($row = mysqli_fetch_array($result)){
                                   $id = $row['sl_no'];
                                    $grn_no=$row['grn_no'];
                                    $grn_date=$row['grn_date'];
                                   $tyre_count=$row['tyre_count'];
                                   $challan_no = $row['challan_no'];
                                   $challan_date = $row['challan_date'];
                                   $challan_file = $row['challan_file'];
                                   $challan_file1=explode(",",$challan_file);
                                   $count3=count($challan_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                 <td>
                                   <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $grn_no?>" >
                                      <input type="hidden" id="grn_no<?php echo $grn_no?>" value="<?php echo $grn_no?>">
                                    </td>

                                    <td> <?php echo $tyre_count; ?><input type="hidden" name="tyre_count[]"  id="tyre_count<?php echo $id_customer; ?>" value='<?php echo $tyre_count; ?>'></td>

                                    <td> <?php echo $grn_date; ?><input type="hidden" name="grn_date[]"  id="grn_date<?php echo $id_customer; ?>" value='<?php echo $grn_date; ?>'></td>


                                    
                                     <td >
                                     Challan No: <?php echo $challan_no; echo "<br>"; ?>
                                      Challan Date: <?php echo $challan_date; echo "<br>"; ?>
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="../<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]"  value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                      ?>
                                     </td>
                                   <form method="post" action="invoice_overchallan_in_oldchassis.php" enctype="multipart/form-data">
                                     <td>
                                      <select required id='total_gst1<?php echo $id; ?>'  onchange="GetSumFunc('<?php echo $id;?>')"   class='total_gst1' style='width: 50px; height: 27px;' name='total_gst' >"
                                         <option value='18'>18</option>
                                         <option value='0'>0</option>
                                         <option value='5'>5</option>
                                         <option value='12'>12</option>
                                         <option value='28'>28</option>
                                        </select>
                                     </td>

                                     <td>

                                      <input type="hidden" name="grn_no" value="<?php echo $grn_no; ?>">

                                      <input type="hidden" name="id" value="<?php echo $id; ?>">
                                       <input type="hidden" name="username" value="<?php echo $username; ?>">

                                      <input type='number' style='width: 120px;'  oninput="GetSumFunc('<?php echo $id;?>')" min='0' required name='amount' class='amount' id='amount<?php echo $id; ?>'  />
                                     </td>

                                     <td>
                                      <input type='number' required="required" class='dis_percent' style='width: 120px;' oninput="GetSumFunc('<?php echo $id;?>')" min='0' max='100' name='dis_percent' id='dis_percent<?php echo $id; ?>'/>
                                     </td>

                                     <td>
                                      <input type='number' required class='dis_amt' oninput="DisCountFunc('<?php echo $id;?>');GetSumFunc('<?php echo $id;?>')" style='width: 120px;' min='0' name='dis_amt' id='dis_amt<?php echo $id; ?>' />
                                     </td>

                                     <td>
                                      <input type='number' required oninput="GetSumFunc('<?php echo $id;?>')" style='width: 120px;' name='total_amt' class='total_amt' id='total_amt<?php echo $id; ?>' readonly  />
                                     </td>

                                     <td>
                                       <input type="text" required id="invoice_no" placeholder="Invoice No" name="invoice_no" autocomplete='off'  required> 
                                     </td>

                                     <td>
                                        <input type="text" required name="invoice_date" id="invoice_date"  placeholder="dd-mm-yyyy" />
                                     </td>

                                     <td>
                                       <input type="file"  required id="invoice_file" name="invoice_file[]" multiple="multiple"  required/>
                                     </td>

                                     <td>
                                       <input type="submit" value="Upload" class="btn-sm btn-success" name="submit" multiple="multiple"  required/>
                                     </td>
                                  </form>

                                     <script type="text/javascript">
                                       function GetSumFunc(id)
                                            {
                                                var amount = Number($('#amount'+id).val());
                                                var dis_perc = Number($('#dis_percent'+id).val());
                                                if (dis_perc > 100) {
                                                  alert("more then 100 discount is not allowed");
                                                   $('#dis_percent'+id).val('');
                                                }else{
                                                var tg = Number($('#total_gst1'+id).val());
                                                var dis_amt = Number($('#dis_amt'+id).val());
                                                var total_amt = Number($('#total_amt'+id).val());
                                                var total_gst1 = amount*tg/100;
                                                var dis_value = (amount*dis_perc/100).toFixed(2);
                                                var discount = Math.round(dis_value);
                                                $('#dis_amt'+id).val(discount);
                                                var total = (((amount-discount)+total_gst1).toFixed(2));
                                                $('#total_amt'+id).val(total);
                                              }
                                            }

                                               function DisCountFunc(id)
                                              {
                                                var amount = Number($('#amount'+id).val());
                                                var dis_amt = Number($('#dis_amt'+id).val());
                                                var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
                                              }
                                     </script>


                                   <!--  <td>
                                       <form method="POST" action="invoice_overchallan_in_newtyre.php"  enctype="multipart/form-data">
                                        
                                        <input type="text"  id="invoice_no" placeholder="Invoice No" name="invoice_no" autocomplete='off'  required> 

                                        <input type="text" name="invoice_date" id="invoice_date"  placeholder="dd-mm-yyyy" />

                                        <br><br>
                                          <input type="hidden" name="username" value="<?php echo $username; ?>">
                                        <input type="file" id="invoice_file" name="invoice_file[]" multiple="multiple"  required/>
                                        <br>
                                         <input type="hidden" name="id"  value='<?php echo $id; ?>'>
                                      <input type="hidden" name="grn_no"  value='<?php echo $grn_no; ?>'>
                                      <input type="submit" class="btn btn-success btn-sm" name="submit" value="submit">
                                    </form>
                                    </td> -->
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                       echo "No Date";
                  }
                  ?>  
                </table>  
                      

                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>
