<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Pending Chassis GRN</h1>
    </section>

    <section class="content">
      <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body">
          <?php  
            $tno=$_POST['tno'];
            $query = "SELECT * FROM own_truck where tno='$tno'";
            
            $result = mysqli_query($conn, $query);
            if(mysqli_num_rows($result) > 0)
            {
              $row1 = mysqli_fetch_array($result);
              /*$id_customer = 0;*/
              $tno = $row1["tno"];
              $wheeler = $row1["wheeler"];
             /* echo $tno; echo "<br>";
              echo $wheeler;*/
              ?>
            <form method="post" style="font-size:18px;">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6"> 
                    <label>Vehicle Number:</label>
                    <?php echo $tno; ?>
                  </div>
                  <div class="col-md-6"> 
                    <label>Wheeler:</label>
                    <?php echo $wheeler; ?>
                  </div>
                </div>
              </div>
              <table border="1"; style="font-family:verdana; font-size:  13px; height: auto; width: 100%; outline:none;border:none;">


                <tbody>
                    <tr class="table-active">
                   <th>Tyre Number</th>
                     <th>position of Tyre</th>
                     <th>Tyre Status</th>
                    <th>Grn Number</th>
                     <th>Grn Date</th>
                   </tr>
               </tbody>
                <?php 

                $sql  = "select * from  grn_over_tyre where vehicle_no = '$tno'";
                $result = $conn->query($sql);
                while ($row = mysqli_fetch_array($result)) {
                   $tyre_no = $row['tyre_no'];
                   $wp = $row['wp'];
                   $type = $row['type'];
                   $grn_no = $row['grn_no'];
                   $grn_date = $row['grn_date'];
                   ?>
                   <tr>
                     <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $tyre_no ?>"></td>
                     <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $wp ?>"></td>
                     <td><input type="text" readonly="readonly"  class="form-control" value="<?php echo $type ?>"></td>
                     <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $grn_no ?>"></td>
                     <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $grn_date ?>"></td>
                   </tr>
                   <?php
                      }
                 ?>

                 </table>
            </form>


          <!--   <?php if($wheeler=='4'){ ?>
            <div class="col-md-12">
              <center>
                <button id='FL' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id='FR' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>
                <button id='RL' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RL' onclick='setEventId(this.value);' type='button'></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id='RR' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR' onclick='setEventId(this.value);' type='button'></button><br><br>
                <button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #000000; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>
              </center>
            </div>
          <?php } else if($wheeler=='6'){ ?>
            <div class="col-md-12">
              <center>
                <button id='FL' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id='FR' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>
                <button id='LR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;
                <button id='LR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button id='RR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;
                <button id='RR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br>
                <button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #000000; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>
              </center>
            </div>

            <?php } else if($wheeler=='10'){ ?>
              <div class="col-md-12">
                <center>
                  <button id='FL' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='FR' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br><br>
                  <button id='LR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR1' onclick='setEventId(this.value);' type='button'></button>
                  <button id='LR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='RR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR2' onclick='setEventId(this.value);' type='button'></button>
                  <button id='RR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>
                  <button id='LR3' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR3' onclick='setEventId(this.value);' type='button'></button>
                  <button id='LR4' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='RR4' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR4' onclick='setEventId(this.value);' type='button'></button>
                  <button id='RR3' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>
                  <button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #000000; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>
                </center>
              </div>

            <?php } else if($wheeler=='12'){ ?>
              <div class="col-md-12">
                <center>
                  <button id='FL' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='FR' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br>
                  <button id='FL1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FL1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='FR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='FR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br><br>
                  <button id='LR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR1' onclick='setEventId(this.value);' type='button'></button>
                  <button id='LR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='RR2' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR2' onclick='setEventId(this.value);' type='button'></button>
                  <button id='RR1' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br>
                  <button id='LR3' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR3' onclick='setEventId(this.value);' type='button'></button>
                  <button id='LR4' data-toggle='modal' data-target='#myModal' class='btnBlack' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button id='RR4' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR4' onclick='setEventId(this.value);' type='button'></button>
                  <button id='RR3' data-toggle='modal' data-target='#myModal' class='btnBlack' value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>
                  <button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #000000; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>
                </center>
              </div>

            <?php
                  }
                }
            ?>  
        </div> -->
            
      </div>  
    </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
 <style>
 button.btnBlack {
    background-color: #000000 !important;
    width:20px; 
    height:50px;
}
</style>