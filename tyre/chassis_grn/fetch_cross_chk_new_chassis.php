<?php
include ("connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>New Chassis Tyre</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <?php   
            $valuetosearch=$_POST['new_chassis'];
            // $valuetosearch =  $_SESSION['new_chassis'];
          ?>
         <font style="font-size: 16px;"> Chassis No: <?php echo $valuetosearch; ?></font>
          </div>
               <div class="row"> 
                <div class="col-md-12"> 
                <div class="table-responsive"> 
                <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px; width: 100%;" >  
                        <thead>
                          <tr>
                             <th>Sno</th>
                              <th>Tyre No</th>
                              <th>Production Month</th>
                              <th>Brand</th>
                              <th>Size</th>
                              <th>Ply</th>
                              <th>MaxNsd</th>
                              <th>Min Nsd</th>
                              <th>Remarks</th>
                              <th>Vertual Amount</th>
                               <th>Remove</th>
                          </tr>
                 </thead>
                    <?php
                      $sql1 = "select * from temp_new_new_remould where grn_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $sno = 1;
                      while ($row = mysqli_fetch_array($result1)) 
                          { 
                            $id = $row['sl_no'];
                            $tyre_no1 = $row['tyre_count'];
                            $product_month1 = $row['production_month'];
                            $brand1 = $row['brand_name'];
                            $remarks1 = $row['remark'];
                            $vertual_amount = $row['vertual_amount'];
                            $tyre_remarks = $row['tyre_remarks'];
                            $ProductSize = $row['ProductSize'];
                            $ProductPlyRating = $row['ProductPlyRating'];
                            $productMaxNsd = $row['productMaxNsd'];
                            $productMinNsd = $row['productMinNsd'];
                          ?>
        <tbody >
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?></td>
              <td><?php echo $tyre_no1; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no" value="<?php echo $tyre_no1; ?>" ></td>

              <td><?php echo $product_month1; ?><input readonly type="hidden" class="form-control" style="width: 120px;"  name="product_month" value="<?php echo $product_month1; ?>" ></td>

              <td><?php echo $brand1; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="brand" readonly value="<?php echo $brand1; ?>" ></td>
              
              <td><?php echo $ProductSize; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductSize" readonly value="<?php echo $ProductSize; ?>" ></td>

              <td><?php echo $ProductPlyRating; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductPlyRating" readonly value="<?php echo $ProductPlyRating; ?>" ></td>

              <td><?php echo $productMaxNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMaxNsd" readonly value="<?php echo $productMaxNsd; ?>" ></td>

              <td><?php echo $productMinNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMinNsd" readonly value="<?php echo $productMinNsd; ?>" ></td>

              <td><?php echo $tyre_remarks;?><input type="hidden" class="form-control" readonly style="width: 120px;" name="tyre_remarks" value="<?php echo $tyre_remarks;?>" ></td>

              <td><?php echo $vertual_amount; ?><input type="hidden" class="form-control"  readonly style="width: 120px;" name="vertual_amount" value="<?php echo $vertual_amount; ?>" ></td>

             
              <td><button type="button" class="btn btn-danger" onclick="DeleteModal('<?php echo $id ?>')" name="remove_first" id="remove_first<?php echo $id ?>">X</button></td>
          </tr>
                                <?php  
                                  $sno++; }
                              ?>
                             </tbody>               
                            </table>
                          </div>
                        </div>
                      </div>
                    <form method="post" action="insert_add_new_chassis.php" enctype="multipart/form-data" autocomplete="off">
                       <div class="container" style="width: 100%">
                          <div class="table-wrapper">
                            <div class="table-title">
                              <div class="row">
                                <div class="col-sm-10"><h3>Add Tyre</h3></div>
                                <div class="col-sm-2">
                                  <div align="right">
                                    <button type="button" class="btn btn-info add-new" onclick="addTyre('tyre_table')"> <i class="fa fa-plus"></i></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="table-responsive"> 
                              <table id="tyre_table" class="table table-bordered">
                                <thead>
                                  <tr>
                                  <input type="hidden" name="grn_no" value="<?php echo $valuetosearch ?>">
                                   <input type="hidden" name="office" value="<?php echo $usernameSet ?>">
                                      <th><font color="red">*</font>Tyre No</th>
                                      <th><font color="red">*</font>Production Month</th>
                                      <th><font color="red">*</font>Brand</th>
                                            <th><font color="red">*</font>Product Size</th>
                                            <th><font color="red">*</font>Ply</th>
                                            <th><font color="red">*</font>Max Nsd</th>
                                            <th><font color="red">*</font>Min Nsd</th>
                                        <th>Remarks</th>
                                        <th >Vertual Amount</th>
                                        <th>Remove</th>
                                        <th>Copy</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                              </table>
                            </div>
                       </div>

                       <div class="col-md-10">
                        <div class="col-md-4"><h3>Set Tyre as Moniter Tyre</h3></div>
                       <div class="col-md-4"><br>
                         <select name="moniter_tyre" required="required" class="form-control">
                           <option value="">Select Tyre For Moniter</option>
                           <option value="yes">Yes</option>
                           <option value="no">No</option>
                         </select>
                       </div>
                     </div><br>
                  
                  
                    <input type="submit" name="submit" value="Final Submit" style="float: right;" class="btn btn-primary"/>

               </div><br>
             </form>

              </div>
            </div>
        </section>  
      </div>
      <?php include("../footer.php");?>
    </div>
  
</body>
</html>
        <script>
          function DeleteModal(id)
            {
              var id = id;
                 var valuetosearch = '<?php echo $valuetosearch ?>'
              if (confirm("Do you want to delete this Tyre?"))
              {
                if(id!='')
                {
                  $.ajax({
                        type: "POST",
                        url: "delete_temp_tyre.php",
                        data:'id='+id + '&valuetosearch='+valuetosearch,
                        success: function(data){
                           location.reload();
                      }
                    });
                }
              }
            }     
        </script>
<script>
$("#total_gst1").change(function(){
  var vendor =  document.getElementById("vendor").value;
  var str =  document.getElementById("gstin").value;
  var res = str.substring(0,2);
  var total_gst =  document.getElementById("total_gst1").value;
  if(vendor!='')
  {
    if(res=='24')
    {
      $('#cgst').val(total_gst/2);
      $('#sgst').val(total_gst/2);
    }
    else{
      $('#igst').val(total_gst);
    }
  }
  else{
    alert('select vendor name firstly');
  }
});
function GetSumFunc(id)
{
  var amount = Number($('#amount'+id).val());
  var dis_perc = Number($('#dis_percent'+id).val());
  if (dis_perc > 100) {
    alert("more then 100 discount is not allowed");
     $('#dis_percent'+id).val('');
  }else{
  var tg = Number($('#total_gst1'+id).val());
  var dis_amt = Number($('#dis_amt'+id).val());
  var total_amt = Number($('#total_amt'+id).val());
  var total_gst1 = amount*tg/100;
  var dis_value = (amount*dis_perc/100).toFixed(2);
  var discount = Math.round(dis_value);
  $('#dis_amt'+id).val(discount);

  var total = (((amount-discount)+total_gst1).toFixed(2));
  $('#total_amt'+id).val(total);
}

}

  function DisCountFunc(id)
  {
    var amount = Number($('#amount'+id).val());
    var dis_amt = Number($('#dis_amt'+id).val());
    var dis_perc1 = Math.round(dis_amt/amount*100).toFixed(2);
  }
</script>

<script>

    function copy_text(id) {
            var id2  = id+1;
            //alert(id2);
               var weekPicker1 = document.getElementById('weekPicker1'+id).value; 
               $('#weekPicker1'+id2).val(weekPicker1);

                var brand = document.getElementById('brand'+id).value; 
               $('#brand'+id2).val(brand);

                var remarks = document.getElementById('remarks'+id).value; 
               $('#remarks'+id2).val(remarks);

               var vertual_amount = document.getElementById('vertual_amount'+id).value; 
               $('#vertual_amount'+id2).val(vertual_amount);

              /* extra column*/

               var ProductSize = document.getElementById('ProductSize'+id).value; 
               $('#ProductSize'+id2).val(ProductSize);

               var ProductPlyRating = document.getElementById('ProductPlyRating'+id).value; 
               $('#ProductPlyRating'+id2).val(ProductPlyRating);

               var productMaxNsd = document.getElementById('productMaxNsd'+id).value; 
               $('#productMaxNsd'+id2).val(productMaxNsd);

               var productMinNsd = document.getElementById('productMinNsd'+id).value; 
               $('#productMinNsd'+id2).val(productMinNsd);
             }
</script>
  <div id="result22"></div>
         <script>
              function MyFunc(id)
              {
                //alert(id);
                var id = id;
                var office = '<?php echo $usernameSet; ?>';
                var tyre_no = $('#tyre_no'+id).val();
                if(tyre_no!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chktyreno.php",
                       data:{tyre_no:tyre_no,office:office,id:id},
                      success: function(data){
                       $("#result22").html(data);
                       }
                  });

                }
              }   

              function not_success_msg(id) {
              alert("this tyre no is in pending queue");
              $('#tyre_no'+id).val('');
               //alert("success");
              }

               function not_success_msg2(id) {
              alert("this tyre no is inserted in stock");
              $('#tyre_no'+id).val('');
               //alert("success");
              }


 
            </script>
        <script>
        function addTyre(tyre_table) {
            var rowCount = document.getElementById("tyre_table").rows.length;
        var row = document.getElementById("tyre_table").insertRow(rowCount);
      
        var cell1 = row.insertCell(0);
        cell1.innerHTML ="<input type='text' style='width: 120px;' name='tyre_no[]' onblur='MyFunc("+rowCount+",this.id)'  id='tyre_no"+rowCount+"' required/>";
        
        var cell2 = row.insertCell(1);
        cell2.innerHTML = "<input type='text' style='width: 120px;' name='product_month[]' id='weekPicker1"+rowCount+"' required/> ";
        
        var cell3 = row.insertCell(2);
        cell3.innerHTML = "<input type='text' style='width: 120px;' name='brand[]' id='brand"+rowCount+"' required/>";

       /*extra column*/
         var cell4 = row.insertCell(3);
         cell4.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductSize[]' id='ProductSize"+rowCount+"' required/>";

          var cell5 = row.insertCell(4);
         cell5.innerHTML = "<input type='text' style='width: 90px;' readonly name='ProductPlyRating[]' id='ProductPlyRating"+rowCount+"' required/>";

          var cell6 = row.insertCell(5);
         cell6.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMaxNsd[]' id='productMaxNsd"+rowCount+"' required/>";

          var cell7 = row.insertCell(6);
         cell7.innerHTML = "<input type='text' style='width: 90px;' readonly name='productMinNsd[]' id='productMinNsd"+rowCount+"' required/>";

        /*End of extra column*/
        
        var cell8 = row.insertCell(7);
        cell8.innerHTML =  "<input type='text' style='width: 120px;' name='tyre_remarks[]' id='remarks"+rowCount+"'/>";
        
        var cell9 = row.insertCell(8);
      cell9.innerHTML =  "<input type='number' oninput='GetSumFunc("+rowCount+",this.id)' style='width: 120px;' name='vertual_amount[]' class='vertual_amount' id='vertual_amount"+rowCount+"' required  />";

        var cell10 = row.insertCell(9);
        cell10.innerHTML =  "<button type='button' class='btn btn-danger' id='remove"+rowCount+"'><i class='fa fa-remove'></i></button>";

         var cell11 = row.insertCell(10);
        cell11.innerHTML =  "<input type='button' id='copy"+rowCount+"' onclick='copy_text("+rowCount+",this.id)' value='copy'>";

        $("#remove"+rowCount).click(function(){
         $(this).parents("tr").remove();
        });

    convertToWeekPicker($("#weekPicker1"+rowCount))

    $(function()
    { 
      $("#brand"+rowCount).autocomplete({
      source: 'search_brand_name_auto.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre Brand Name does not exists');
      } 
    },
    focus: function (event, ui) { return false; } }); });

    $("#brand"+rowCount).change(function(){
      var brand=$("#brand"+rowCount).val();
      $.get("search_brand_data.php",{txt:brand},function(data,status){
        var arr = $.parseJSON(data);
        $("#ProductSize"+rowCount).val(arr[0]);
        $("#ProductPlyRating"+rowCount).val(arr[1]);
        $("#productMaxNsd"+rowCount).val(arr[2]);
        $("#productMinNsd"+rowCount).val(arr[3]);
         });
    });

   
            if($('.challan').val(''))
            {  ShowHideDiv(this);

                     }
                 else{
                 if($('.invoice').val(''))
                  {
                    alert("invoice empty");
                    ShowHideDiv2(this);
                  }
              }

        
            
        }
        </script>
        <style>
        ul.ui-autocomplete{
          z-index: 999999 !important;
        }
        </style>
        <script>
          function invoice_select() {
            $(document).ready(function() {
              $ ('#invoice_file').bind('change', function() {
                  var fileSize = this.files[0].size/1024/1024;
                  if (fileSize > 2) { // 2M
                    alert('Your max file size exceeded');
                    $('#invoice_file').val('');
                  }
              });
            });
          }
          function challan_select() {
            $(document).ready(function() {
              $ ('#challan_file').bind('change', function() {
                  var fileSize = this.files[0].size/1024/1024;
                  if (fileSize > 2) { // 2M
                    alert('Your max file size exceeded');
                    $('#challan_file').val('');
                  }
              });
            });
          }
        </script>
        <script type="text/javascript">
        
        $(document).ready(function(){  
         $.datepicker.setDefaults({  
            dateFormat: 'dd-mm-yy'   
         });  
         $(function(){  
            $("#invoice_date").datepicker();
            $("#challan_date").datepicker();
         });
        });
         
        </script>


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="src/weekPicker.js"></script>
   



   

