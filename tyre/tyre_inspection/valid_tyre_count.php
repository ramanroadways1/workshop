<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Valid Tyre Count</h1>
    </section>

    <section class="content">
     
    <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-hover" border="2">  
                  <thead>  
                   <tr>  
                    <th>EDIT</th>
                    <th>REJECT</th>
                    <th>POSITION</th>
                    <th>TYRE NO.</th>
                    <th>MODIFIED TYRE NO.</th>
                    <th>TD1</th>
                    <th>TD2</th>
                    <th>TD3</th>
                    <th>TDF</th>
                    <th>ACTION</th>
                    <th>ACTION DATE</th>
                    <th>WEAR/TEAR</th>
                    <th>CORRECTIVE ACTION</th>
                    <!-- <th>REMARK</th> -->
                    <th>AIR</th>
                    <th>AUDIT REMARK</th>
                    <th>CURRENT VEHICLE</th>
                    <th>CURRENT STATUS</th>
                   </tr>  
                  </thead>  
                  <?php  
                  $vehicle_no=$_REQUEST['link'];
                  $query = "SELECT * FROM tyre_insp_details where vehicle_no='$vehicle_no' and status='chassis'";
                  $result = mysqli_query($conn, $query);
                  if(mysqli_num_rows($result) > 0)
                  {
                    while($row = mysqli_fetch_array($result))
                    {
                    ?>
                    <tr> 
                      <td><a href="edit_total_tyre_count.php?link=<?php echo $row["tyre_no"]; ?>" class="btn btn-info btn-xs">EDIT</a></td>
                      <td>Reject</td>
                      <td><?php echo $row["wp"] ?></td>
                      <td><?php echo $row["tyre_no"]?></td>
                      <td><?php echo $row["modified_tyre"]?></td>
                      <td><?php echo $row["td1"]?></td>
                      <td><?php echo $row["td2"] ?></td>
                      <td><?php echo $row["td3"]?></td>
                      <td><?php echo $row["tdf"]?></td>
                      <td><?php echo $row["action"]?></td>
                      <td><?php echo $row["action_date"] ?></td>
                      <td><?php echo $row["wear"]?></td>
                      <td><?php echo $row["recommendation"]?></td>
                      <!-- <td><?php echo $row["remark"]?></td> -->
                      <td><?php echo $row["air"] ?></td>
                      <td><?php echo $row["audit_remark"]?></td>
                      <td><?php echo $row["vehicle_no"]?></td>
                      <td><?php echo $row["status"]?></td>
                    </tr>  
                    <?php   
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
      </section>
    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 