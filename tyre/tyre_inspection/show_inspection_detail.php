<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <a href='index.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
      <h1>Tyre Inspection</h1>
    </section>

    <section class="content">
     
    <div class="box box-info">
       <div class="box-body">
          <div class="table-responsive"><br>
            
            <table class="table table-bordered table-striped">
             
              <?php
              $vehicle_no = $_POST['vehicle_no'];
               $sql  =  "select * from tyre_insp_details where vehicle_no='$vehicle_no'";
              $result  = $conn->query($sql);
               if(mysqli_num_rows($result) > 0){
                                    ?>
                                     <thead>
                                      <tr>
                                        <th>Action</th>
                                        <th>Tyre No</th> 
                                        <th>Wheel Position</th>
                                        <th>Nsd1</th> 
                                        <th>Nsd2</th> 
                                        <th>Nsd3</th>
                                        <th>Nsd4</th> 
                                        <th>Old PAsi</th> 
                                        <th>New Psi</th>
                                         <th>Wear</th>
                                        <th>Wall Cap</th> 
                                        <th>Remark</th> 
                                        <th>Time&Date</th>
                                      </tr>
                                    </thead>
                                      <?php
                                    while($row = mysqli_fetch_array($result)){
                                      $sl_no = $row['sl_no'];
                                   $vehicle_no = $row['vehicle_no'];
                                    $tyre_no=$row['tyre_no'];
                                     $wp =$row['wp'];
                                    $nsd1 =$row['nsd1'];
                                     $nsd2 = $row['nsd2'];
                                    $nsd3=$row['nsd3'];
                                    $nsd4=$row['nsd4'];
                                     $target_psi = $row['target_psi'];
                                    $new_psi=$row['new_psi'];
                                    $wear=$row['wear'];
                                     $wall_cap=$row['wall_cap'];
                                    $remark=$row['remark'];
                                     $timestamp=$row['timestamp'];
                              ?>
                       <tr>   <td><a class="fa fa-edit" onclick='javascript:EditModal(<?php echo $sl_no; ?>);return false;'  ></a>&nbsp;&nbsp;<a onclick='javascript:confirmationDelete(<?php echo $sl_no; ?>);return false;' class="fa fa-remove" ></a></td>
                              <td><?php echo $tyre_no; ?><input type="hidden" id="tyre_no<?php echo $sl_no ?>" name="tyre_no"></td> 
                              <td><?php echo $wp; ?></td> 
                              <td> <?php echo $nsd1; ?></td>
                               <td><?php echo $nsd2; ?></td>
                              <td><?php echo $nsd3; ?></td>
                              <td> <?php echo $nsd4; ?></td>
                               <td><?php echo $target_psi; ?></td>
                              <td><?php echo $new_psi; ?></td>
                              <td> <?php echo $wear; ?></td>
                               <td><?php echo $wall_cap; ?></td>
                              <td><?php echo $remark; ?></td>
                              <td> <?php echo $timestamp; ?></td>

              <?php } } ?>

            </table>
            <div id="result_main"></div>
             <div id="result"></div>
           </div>
          </div>
        </div>

      </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>

  <script>
    function confirmationDelete(sl_no)
      {  
        var sl_no = sl_no;
      var tyre_no = $('#tyre_no'+sl_no).val();
       if (confirm("Do you want to delete this tyre from inspection?"))
        {
          if(vehicle_no!='')
          {
            $.ajax({
                  type: "POST",
                  url: "delete_insp.php",
                  data:'tyre_no='+tyre_no + '&sl_no='+sl_no ,
                  success: function(data){
                    alert(data)
                     location.reload(); 
                      $("#result22").html(data);
                  }
              });
          }
        }
    }

    function EditModal(id)
    {
      jQuery.ajax({
          url: "fetch_edit_data.php",
          data: 'id=' + id,
          type: "POST",
          success: function(data) {
           $("#result_main").html(data);
          },
              error: function() {}
          });
      document.getElementById("myModal").click();
      $('#myModal_id').val(id);
      }

</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormPartyUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_insp_data.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
      alert(data);
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 
           
              
<button type="button" id="myModal" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<form  id="FormPartyUpdate"  action="update_insp_data.php">
  <div id="myModal22" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Tyre Details</h4>
        </div>
 
        <div class="modal-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Wheel Position:</label>
                <input type="text" class="form-control" name="wp" id="wp" readonly/>
                <input type="hidden" name="office" id="office2" class="form-control" value="<?php echo $usernameSet; ?>"/>
                 <input type="hidden" name="sl_no" id="sl_no" class="form-control" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Number:</label>
                <input type="text" class="form-control" name="vehicle_no_modal" id="vehicle_no_modal" readonly/>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Old Psi:</label>
                <input type="text" class="form-control" name="target_psi" id="target_psi" readonly/>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Current Nsd:</label><br>
                <div class="col-md-2">
                  <font>
                 NSD1<input type="Number" style="width: 70px;" class="form-control" name="nsd1" id="nsd1" required="required"/>
                 NSD2<input type="Number" style="width: 70px;" class="form-control" name="nsd2" id="nsd2" required="required" />
                 </font>
                </div>

                <div class="col-md-2">
                 <font style="float: right;margin-right: -190px;">
                 NSD3<input type="Number" style="width: 70px;" class="form-control" name="nsd3" id="nsd3" required="required"/>
                 NSD4<input type="Number" style="width: 70px;" class="form-control" name="nsd4" id="nsd4" required="required"/>
                 </font> 
                </div>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Wear:</label>
                 <select  class="form-control" id="wear" name="wear[]" multiple="multiple">
                        <option hidden value="">---Select Wear---</option>
                        <?php 
                          $get=mysqli_query($conn,"SELECT * FROM wear");
                            while($row = mysqli_fetch_assoc($get))
                            {
                          ?>
                          <option value="<?php echo ($row['wear']); ?>"><?php echo ($row['wear']); ?></option>
                            <?php
                            }
                          ?>
                      </select>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>New Psi:</label>
                <input type="Number" class="form-control" name="new_psi" id="new_psi" required="required" />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Wall Cap:</label>
                 <select onchange="SelectStatus(this.value)" class="form-control" id="wall_cap" name="wall_cap">
                        <option hidden value="">---Select Wall Cap---</option>
                        <option value="wall_cap_yes">Yes</option>
                        <option value="wall_cap_no">No</option>
                      </select>
              </div>
            </div> 

            <div class="col-md-4">
              <div class="form-group">
                <label>Tyre No:</label>
                 <input type="text" name="tyre_no" readonly="readonly" id="tyre_no"  class="form-control" >
              </div>
            </div> 

            <div class="col-md-4">
              <div class="form-group">
                <label>Remark:</label>
                 <input type="text" name="remark" id="remark" class="form-control" required="required">
              </div>
            </div> 

          </div>
        </div>
    
     
        <div class="modal-footer">
          <button type="submit" class="btn btn-info">Update</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    </form>

<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 }); 
</script>