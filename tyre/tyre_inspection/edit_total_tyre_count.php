<?php 
  require_once("connect.php");
  $tyre_no=$_REQUEST['link'];
  $query = "SELECT * FROM tyre_insp_details where tyre_no='$tyre_no'";
  $result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)
  {
    $row = mysqli_fetch_array($result);
    $vehicle_no =$row["vehicle_no"];
    $tyre_no =$row["tyre_no"];
    $status =$row["status"];
    $position =$row["wp"];
    $td1 =$row["td1"];
    $td2 =$row["td2"];
    $td3 =$row["td3"];
    $tdf =$row["tdf"];
    $air =$row["air"];
    $wear =$row["wear"];
    $remark =$row["remark"];
    $recommendation =$row["recommendation"];
    $action =$row["action"];
    $action_dt =$row["action_date"];
    $audit_remark =$row["audit_remark"];
    $modified_tyre_no =$row["modified_tyre"];
  ?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Edit Tyre Inspection</h1>
    </section>

    <section class="content">
      <!-- <div class="form-group" align="right" style="margin-right: 2%;">
        <b><a href='total_tyre_count.php?link=<?php echo $vehicle_no; ?>'>Back To Index</a></b>
      </div> -->
      <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12"><br>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Tyre No.:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $tyre_no;?>" class="form-control" name="tyre_no" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Position:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $position;?>" id="position" class="form-control" name="position" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>TD1:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $td1;?>" id="td1" class="form-control" name="td1" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>TD2:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $td2;?>" id="td2" class="form-control" name="td2" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>TD3:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $td3;?>" id="td3" class="form-control" name="td3" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>TDF:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $tdf;?>" id="tdf" class="form-control" name="tdf" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Air:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $air;?>" id="air" class="form-control" name="air" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Current Vehicle:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $vehicle_no;?>" id="vehicle_no" class="form-control" name="vehicle_no" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Current Status:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $status;?>" id="status" class="form-control" name="status" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Remark:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" id="remark" value="<?php echo $remark;?>" class="form-control" name="remark" />
                    </div>
                  </div><br><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Wear/Tear:</b>
                    </div>
                    <div class="col-md-8">
                      <select onchange="SelectStatus(this.value)" class="form-control" id="wear" name="wear">
                        <option hidden value="">---Select Wear---</option>
                        <?php 
                          $get=mysqli_query($conn,"SELECT * FROM wear");
                            while($row = mysqli_fetch_assoc($get))
                            {
                          ?>
                          <option value="<?php echo ($row['wear']); ?>"><?php echo ($row['wear']); ?></option>
                            <?php
                            }
                          ?>
                      </select>
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Corrective Action:</b>
                    </div>
                    <div class="col-md-8">
                      <select value="<?php echo $recommendation;?>" class="form-control" id="recommendation" name="recommendation">
                        <option hidden value="">---Select Corrective Action---</option>
                      </select>
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Action:</b>
                    </div>
                    <div class="col-md-8">
                      <select value="<?php echo $action;?>" class="form-control" id="action" name="action">
                        <option value="Pending">Pending</option>
                        <option value="Complete">Complete</option>
                      </select>
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Action Date:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $action_dt;?>" name="action_dt" id="action_dt" class="form-control" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Audit Remark:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $audit_remark;?>" id="audit_remark" class="form-control" name="audit_remark" />
                    </div>
                  </div><br><br>
                  <div class="form-group">
                    <div class="col-md-4">
                      <b>Modified Tyre No.:</b>
                    </div>
                    <div class="col-md-8">
                      <input type="text" value="<?php echo $modified_tyre_no;?>" id="modified_tyre_no" class="form-control" name="modified_tyre_no" />
                    </div>
                  </div><br><br>
                  <div class="form-group" align="right" style="margin-right: 2%;"><br>
                    <button type="submit" class="btn btn-info" formaction="update_tyre_details.php"> Update</button>
                  </div>
                </div>
              </div>
            </div>  
          </div>
            <!-- /.table-responsive -->
        </div>
              
        </div>
      </section>
    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
<?php }
?>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<script>  
$(document).ready(function(){  
     $.datepicker.setDefaults({  
          dateFormat: 'dd-mm-yy'   
     });  
     $(function(){  
          $("#action_dt").datepicker();
     });
});
</script>
<script>
$('#wear').on('change', function(){
  var wear = $(this).val();
  if(wear!=''){
    $.ajax({
        type:'POST',
        url:'get_action.php',
        data:'wear='+wear,
        success:function(html){
            $('#recommendation').html(html);
           /* alert(data);*/
        }
    }); 
  }
});
</script>