<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <div class="content-wrapper">
    <section class="content-header">
      <h1>Tyre Inspection</h1>
    </section>

    <section class="content">
     
    <div class="box box-info">
       <div class="box-body">
          <div class="table-responsive"><br>
            
            <table id="employee_data" class="table table-bordered table-striped">
             
              <?php
               $sql  =  "select vehicle_no,total_tyre,count(tyre_no) as tyre_no,date(timestamp) as date from tyre_insp_details group by vehicle_no";
              $result  = $conn->query($sql);
               if(mysqli_num_rows($result) > 0){
                                    ?>
                                     <thead>
                                      <tr>
                                        <th>Total Tyre</th> 
                                        <th>Tyre in Inspection</th> 
                                        <th>Inspection Date</th>
                                         <th>Current Status</th>
                                      </tr>
                                    </thead>
                                      <?php
                                    while($row = mysqli_fetch_array($result)){
                                   $vehicle_no = $row['vehicle_no'];
                                    $total_tyre = $row['total_tyre'];
                                    $tyre_no=$row['tyre_no'];
                                    $timestamp=$row['date'];
                              ?>
                       <tr> 
                             
                               <td><?php echo $total_tyre; ?></td>
                              <td>
                                <form action="show_inspection_detail.php" method="post">
                                   <input type="submit" class="btn btn-primary btn-sm" value="<?php echo $tyre_no?>">
                                   <input type="hidden" value="<?php echo $vehicle_no?>" name="vehicle_no">
                                   </form>
                                </td>
                              <td> <?php echo $timestamp; ?></td>

                                <td>
                                  <?php
                                  if ($total_tyre==$tyre_no) {
                                    ?>
                                    <a class="btn btn-sm btn-success" href="save_final_inspection.php">Submit Inspection</a>
                                    <?php
                                  }else{
                                    ?>
                                    <a class="btn btn-sm btn-danger">Pending Inspection</a>
                                    <?php
                                  }
                                      
                                   ?>
                                </td>

              <?php } } ?>

            </table>
            
           </div>
          </div>
        </div>

      </section>

    </div>

<?php include("../footer.php");?>
</div>
</body>
</html>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 }); 
</script>