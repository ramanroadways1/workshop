<?php
require("connect.php");
// include('header.php');
?>
<html>
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style type="text/css">
        
        @page {
      size: landscape;
    }

</style>

<style type="text/css">
          @media print {
  {page-break-after: always;}
      }
      @media print {
        pre, blockquote {page-break-inside: avoid;}
      }
</style>
<style>
   {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

   td,  th {
    border: 1px;
    padding: 8px;
  }

   tr:nth-child(even)

   tr:hover 

   th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
  }
</style>

</head>

<body class="hold-transition skin-blue sidebar-mini">
          <input type="button" onclick="tableToExcel('tblCustomers', 'W3C Example Table')" value="Export to Excel">
          <input type="button" class="btn btn-warning" id="btnExport" value="Download PDF" />
          <input type="button" class="btn btn-success" onclick="getPDF()" value="Export to PDF">
          <button onclick="goBack()">Go Back</button>
                      
<div class="wrapper">
  <div class="canvas_div_pdf">
  <div class="content-wrapper">
  
    <section class="content">

      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"> Tyre Detils(Inventry)</h3>
         </div>

            <script>
            function goBack() {
              window.history.go(-2);
            }
            </script>
            
          <div class=demo id="tblCustomers">
          <div class="box-body">
<!-- ------------(1)--------------------->            
   <?php           
     
      $valueToSearchtest=$_POST['TyreNo'];
       
     echo "Tyre Number: ".$valueToSearchtest;
   
     ?>

        <div id="mydiv">
              <div class="" ass="leftDiv">
                 <div class="row" id="first" style="margin-left: 2px; margin-top: 4px;"  >
                   <div class="col-md-6" style=" margin-top: 9px;">
                    <u><b><p style="color:#000000  font-size: 40px;">Tyre Reports(Inventory)</p></b></u>
                  </div>
                </div>
              </div>
        </div>
       <?php


          $valueToSearch=$_POST['tyre_no'];


          
          $sql = "SELECT * FROM `grn_over_tyre` WHERE tyre_no='$valueToSearchtest'";

              $result = $conn->query($sql);

             ?>
             <table class="table-responsive" id="table1" style="font-family:arial; font-size:  13px; border: 1px solid black; margin-top: 20px;">
                
               <tr>
                    <th style="width: 150px;">office</th> 
                    <th style="width: 150px;">type</th>
                    <th style="width: 150px;">vehicle_no</th>
                    <th style="width: 150px;">issue_date</th>
                    <th style="width: 150px;">opening_km</th>
                    <th style="width: 150px;">production_month</th> 
                    <th style="width: 150px;">TYRE_NO</th>
                    <th style="width: 150px;">TYRE_SIZE</th>
                    <th style="width: 150px;">TRUCK_NO</th>
                    <th style="width: 150px;">FITTING_KM</th>
                    <th style="width: 150px;">FITTING_DATE</th>
                    <th style="width: 150px;">REMOVE_TYRE_NO</th>
                    <th style="width: 150px;">REMOVE_TYRE_MAKE</th>
                    <th style="width: 150px;">REMOVE_FITTING_KM</th>
                    <th style="width: 150px;">REMOVE_FITTING_DATE</th>
                    <th style="width: 150px;">REMOVE_TYRE_MILEAGE</th>
                    <th style="width: 150px;">SCRAPCASINGREUSECLAIM</th>
                    <th style="width: 150px;">REMARKS</th>

               </tr>
                 
             <?php     
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
                     
                      $office= $row['office'];
                      $type= $row['type'];
                      $vehicle_no= $row['vehicle_no'];
                      $issue_date= $row['issue_date'];
                      $opening_km= $row['opening_km'];
                      $production_month= $row['production_month'];
                      $brand_name= $row['brand_name'];
                      $ProductPlyRating= $row['ProductPlyRating'];
                      $productMaxNsd= $row['productMaxNsd'];
                      $productMinNsd= $row['productMinNsd'];
                      $is_remould=$row['is_remould'];
                      $rubber_brand=$row['rubber_brand'];
                      $REMOVE_TYRE_MAKE=$row['REMOVE_TYRE_MAKE'];
                      $REMOVE_FITTING_KM=$row['REMOVE_FITTING_KM'];
                      $REMOVE_FITTING_DATE=$row['REMOVE_FITTING_DATE'];
                      $REMOVE_TYRE_MILEAGE=$row['REMOVE_TYRE_MILEAGE'];
                      $SCRAPCASINGREUSECLAIM=$row['SCRAPCASINGREUSECLAIM'];
                      $REMARKS=$row['REMARKS'];
           

                  ?>
  
                  <tr>
                      <td><?php echo $office; ?></td>
                      <td><?php echo $type; ?></td>
                      <td><?php echo $vehicle_no; ?></td>
                      <td><?php echo $issue_date; ?></td>
                      <td><?php echo $opening_km; ?></td>
                      <td><?php echo $production_month; ?></td>
                      <td><?php echo $brand_name; ?></td>
                      <td><?php echo $ProductPlyRating; ?></td>
                      <td><?php echo $productMaxNsd; ?></td>
                      <td><?php echo $productMinNsd; ?></td>

                      <td><?php echo $is_remould; ?></td>

                      <td><?php echo $rubber_brand; ?></td>
                      <td><?php echo $REMOVE_TYRE_MAKE; ?></td>
                      <td><?php echo $REMOVE_FITTING_KM; ?></td>
                      <td><?php echo $REMOVE_FITTING_DATE; ?></td> 
                      <td><?php echo $REMOVE_TYRE_MILEAGE; ?></td>
                      <td><?php echo $SCRAPCASINGREUSECLAIM; ?></td>
                      <td><?php echo $REMARKS; ?></td>
          

                  </tr>

                     <?php
                  }
               ?> 
         </table>
            <?php
              }
            ?> 
      
     
<!-- --------------------------------End------------------------------------------------------- -->
        </div>
      </div>
      </section>
<!------------------(End)--------------------- -->
    </div>
  </div>


 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
      <script type="text/javascript">

        $("body").on("click", "#btnExport", function () {
            html2canvas($('#tblCustomers')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500

                        }]

                    };
                    pdfMake.createPdf(docDefinition).download("Invoic.pdf");
                }
            });
        });

    </script>

<script type="text/javascript">
      var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
</script>
<script type="text/javascript">
        document.getElementById('export').onclick=function(){
          var tableId= document.getElementById('tableData').id;
          htmlTableToExcel(tableId, filename = '');
      }
     var htmlTableToExcel= function(tblCustomers, fileName = ''){
      var excelFileName='excel_table_data';
      var TableDataType = 'application/vnd.ms-excel';
      var selectTable = document.getElementById(tableId);
      var htmlTable = selectTable.outerHTML.replace(/ /g, '%20');
      
      filename = filename?filename+'.xls':excelFileName+'.xls';
      var excelFileURL = document.createElement("a");
      document.body.appendChild(excelFileURL);
      
      if(navigator.msSaveOrOpenBlob){
          var blob = new Blob(['\ufeff', htmlTable], {
              type: TableDataType
          });
          navigator.msSaveOrOpenBlob( blob, fileName);
      }else{
          
          excelFileURL.href = 'data:' + TableDataType + ', ' + htmlTable;
          excelFileURL.download = fileName;
          excelFileURL.click();
      }
  }
</script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script type="text/javascript">

  function getPDF(){

    var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var top_left_margin = 5;
    var PDF_Width = HTML_Width+(top_left_margin*1);
    var PDF_Height = (PDF_Width*1)+(top_left_margin*1);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    

    html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
      canvas.getContext('2d');
      
      console.log(canvas.height+"  "+canvas.width);
      
      
      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
      
      
      for (var i = 1; i <= totalPDFPages; i++) { 
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
      }
      
        pdf.save("Jobcard.pdf");
        });
  };
</script>
  </body>
  </html>

