<?php
session_start();

if(isset($_SESSION['valid_user']))
{
	echo "<script>
		window.location.href='./index.php';
	</script>";
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Tyre Management System</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  
  <div class="header">
  	<h2>Login</h2>
  </div>
	 
  <form method="post" action="" autocomplete="off">
  
  	<div class="input-group">
  		<label>Username</label>
  		<input type="text" name="username" required>
  	</div>
  		
	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password" required>
  	</div>
  	
	<div class="input-group">
  		<button type="submit" class="btn" name="login_user">Login</button>
  	</div>
  	
  </form>
</body>
</html>

<?php
$conn=mysqli_connect("localhost","root","","tyre_mngt");
if(!$conn)
{
	echo mysqli_error($conn);
	exit();
}

if(isset($_POST['login_user']))
{
	$username=mysqli_real_escape_string($conn,strtoupper($_POST['username']));
	$password=md5($_POST['password']);
	$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
	$result = $conn->query($query);

    if (mysqli_num_rows($result)>0) {
    	$_SESSION['valid_user']=$username;
		echo "<script>
			window.location.href='index.php';
		</script>";
		exit();
		
    }else{
    	echo "<script>
			window.location.href='./login.php';
		</script>";
		exit();
		
    }
	
	/*$qry=mysqli_query($conn,"SELECT * FROM users WHERE username='$username' AND password='$password'");	
	print_r($qry);*/

	/*if(mysqli_num_rows($qry)==1)
	{
		$_SESSION['valid_user']=$username;
		echo "<script>
			window.location.href='index.php';
		</script>";
		exit();
	}
	else
	{
		echo "<script>
			window.location.href='./login.php';
		</script>";
		exit();
	}*/
}
?>