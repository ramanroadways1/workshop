<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.answer { display:none }

	</style>
	<title></title>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<fieldset class="question">
  <label for="coupon_question">Do you have a coupon?</label>
  <input id="coupon_question" type="checkbox" name="coupon_question" value="1" />
  <span class="item-text">Yes</span>
</fieldset>

<fieldset class="answer" style="display: none;">
  <label for="coupon_field">Your coupon:</label>
  <input type="text" name="coupon_field" id="coupon_field" />
</fieldset>
<script type="text/javascript">
	$(function() {
  $("#coupon_question").on("click",function() {
    $(".answer").toggle(this.checked);
  });
});
</script>
</body>
</html>


