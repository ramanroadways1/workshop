<?php

include('header.php');
include('connect.php');
include('aside_main.php');

?>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    .input-container {
    display: flex;
    width: 250px;
    border: 1px solid #a9a9a9;
    justify-content: space-between;
}
.input-container input:focus, .input-container input:active {
    outline: none;
}
.input-container input {
    border: none;
}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<div class="wrapper">

 <form method="post" action="master_addbattery.php" autocomplete="off" enctype="multipart/form-data">
  <div class="wrapper">
  <div class="content-wrapper">

    <section class="content">
      <div class="box box-default">
      <section class="content-header">
     <!--  <a href='approve_newtyre.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>&nbsp;Approve New Tyre</a> -->


       <a href='upload_invoice_battry.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Upload Invoice On New Battery</a>
      <h1>New Battery Purchase</h1>
    </section>
        <div class="box-header with-border">

     
        <div class="box-body">

          <div class="row" style="height: 10px; padding: 10px 5px 10px 5px">
<!--     <form> -->
     <div class="col-md-6" >
     <div class="form-group row">
<!--       <div  class="form-group">
        <label class="a">Battery Sr.No.<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="srn" name="srn" type="text" >
      </div>
 -->       <div class="form-group">
        <label class="a">Battery No<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="battery_no" name="battery_no" type="text" >
      </div>
    </div>
      <div class="form-group row">
 
        <div class="form-group">
        <label class="a">Volt<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="volt" name="volt" type="text" >
      </div>

      <div class="form-group">
        <label class="a">Watt<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="watt" name="watt" type="text" >
      </div>

  
  </div>
        <div class="form-group row">
        <div class="form-group">
        <label class="a">Full Charge Time <font color="#FF0000">*</font></label>
        <input type="text"  class="form-control" style=" width: 380px;" id="time" name="charze_time" placeholder="Time">
      </div>
      <script type="text/javascript">
            var timepicker = new TimePicker('time', {
            lang: 'en',
            theme: 'dark'
            });
            timepicker.on('change', function(evt) {

            var value = (evt.hour || '00') + ':' + (evt.minute || '00');
            evt.element.value = value;

            });
      </script>
      <div class="form-group">
        <label class="a">Run After Charge(Days)<font color="#FF0000">*</font></label>
        <input type="text"  class="form-control" style=" width: 380px;" id="run_aftercharz" name="run_aftercharz" >
      </div>

  </div>
     <p>Purchase Info.</p>
       <div class="form-group row">
<!--             <div class="input-container">
    <input type="text"  class="input-field"/>
    <button class="input-button">Ok</button>
</div> -->
          

<!-- <div class="input-container">
          <input type="text"  class="input-field"/>
          <button class="input-button">Ok</button>
          </div> -->

        <div class="col-md-10" style="margin-left: 0px;">
        <label class="a">Vendor<font color="#FF0000">*</font></label>
        <div  class="input-container">
        <input type="text"  class="form-control" style=" width: 380px;" id="party_name" name="party_name" >
       <!--  <button class="input-button"><i class="fa fa-cogs" aria-hidden="true"></i></button> -->
       </div>
      </div>

                <div id="rate_master1"></div>
                <script type="text/javascript">     
                      $(function()
                      {    
                          $("#party_name").autocomplete({
                          source: 'fetch_party_name_autocomplete.php',
                          change: function (event, ui) {
                          if(!ui.item){
                          $(event.target).val(""); 
                          $(event.target).focus();
                      } 
                        },
                         focus: function (event, ui) {  return false;
                        }
                        });
                     });

                      function get_data1(val) {
                            $.ajax({
                              type: "POST",
                              url: "fetch_truck_gatelogin.php",
                              data:'truck_no='+val,
                              success: function(data){
                               
                                  $("#rate_master1").html(data);
                              }
                              });
                          }
              </script>


  </div>

    <div class="form-group row">
      <div class="col-md-4">
        <label class="a">Amount (Cost)<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="amount" name="amount" type="text" >

    </div>
  </div>
            <div class="col-md-6"> 
            <label for="chkinvoice">
              <input type="checkbox" id="chkinvoice" class="pno2" name=""  required="required" name="invoice_file[]"  onclick="ShowHideDiv(this);" />
              Upload Invoice
            </label>

              <label for="chkchallan">
              <input type="checkbox" id="chkchallan"  class="pno1" name="challan_file[]" required="required" onclick="ShowHideDiv2(this);" />
              Upload Challan
            </label>


            <div class="row"  id="dvchallan_no" style="display: none;">
              <br><div class="col-md-4"><label><font color="red">*</font>Challan no:</label></div>
              <div class="col-md-8">
                <input type="text"  name="challan_no" id="challan_no" class="form-control" style="width: 338px;" placeholder="Challan No" />
              </div>
            </div><br> 
            <div class="row" id="dvchallan_date" style="display: none;">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Date:</label></div>
              <div class="col-md-8">
                <input type="text"  name="challan_date" id="challan_date"  class="form-control" style="width: 338px;" placeholder="dd-mm-yyyy"  />
              </div>
            </div><br>
            <div class="row" style="display: none" id="dvchallan_file">
              <div class="col-md-4"> <label><font color="red">*</font>Challan Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="challan_file" class="form-control" name="challan_file[]" onclick="challan_select()" style="width: 338px;" multiple="multiple" />
              </div>
            </div>
            <div class="row" id="dvinvoice_no" style="display: none"><br>
              <div class="col-md-4"> <label><font color="red">*</font>Invoice No:</label></div>
              <div class="col-md-8">
                <input type="text"  name="invoice_no" id="invoice_no" class="form-control" style="width: 338px;" placeholder="Invoice No" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_date" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Date:</label></div>
              <div class="col-md-8">
                <input type="text"  name="invoice_date" id="invoice_date" class="form-control" style="width: 338px;" placeholder="yyyy-mm-dd" />
              </div>
            </div><br>

            <div class="row" id="dvinvoice_file" style="display: none">
              <div class="col-md-4"> <label><font color="red">*</font>Invoice Upload:</label></div>
              <div class="col-md-8">
                <input type="file" id="invoice_file" class="form-control" name="invoice_file[]" onclick="invoice_select()" style="width: 338px;"  multiple="multiple"/>
              </div>
            </div><br>
          </div>
          <script type="text/javascript">


function ShowHideDiv(chkinvoice) {
     if(document.getElementById('chkinvoice').checked){
       $("#chkchallan").attr('disabled',true); 
       $("#dvinvoice_no").css("display","block");
       document.getElementById("invoice_no").required = true;
       $("#dvinvoice_date").css("display","block");
        document.getElementById("invoice_date").required = true;
       $("#dvinvoice_file").css("display","block");
        document.getElementById("invoice_file").required = true;
       $('#challan_no').val('');
       $('#challan_date').val('');
       $('#challan_file').val('');
       document.getElementById("challan_no").required = false;
        document.getElementById("challan_date").required = false;
        document.getElementById("challan_file").required = false;
     }
     else{
        $("#chkchallan").attr('disabled',false); 
         $("#dvinvoice_no").css("display","none");
         $("#dvinvoice_date").css("display","none");
         $("#dvinvoice_file").css("display","none");
        }

       $(".total_gst1").attr('required',true); 
       $(".amount").attr('required',true); 
       $(".dis_percent").attr('required',true); 
       $(".dis_amt").attr('required',true); 
       $(".total_amt").attr('required',true); 
     
          $('.total_gst1').attr("style", "display:block");
           $('.amount').attr("style", "display:block");
           $('.dis_percent').attr("style", "display:block");
           $('.dis_amt').attr("style", "display:block");
           $('.total_amt').attr("style", "display:block");
      } 


      function ShowHideDiv2(chkchallan) {
         if(document.getElementById('chkchallan').checked){

           $("#chkinvoice").attr('disabled',true); 
         
           $("#dvchallan_no").css("display","block");
           challan_no.setAttribute( 'required', 'required' );

           $("#dvchallan_date").css("display","block");
           challan_date.setAttribute( 'required', 'required' );

           $("#dvchallan_file").css("display","block");
            challan_file.setAttribute( 'required', 'required' );

               $('#invoice_no').val('');
               $('#invoice_date').val('');
               $('#invoice_file').val('');
       document.getElementById("invoice_no").required = false;
        document.getElementById("invoice_date").required = false;
       document.getElementById("invoice_file").required = false;

         }
         else{
             $("#chkinvoice").attr('disabled',false); 
             $("#dvchallan_no").css("display","none");
             $("#dvchallan_date").css("display","none");
             $("#dvchallan_file").css("display","none");
          }
            $(".total_gst1").attr('required',false); 
           $(".amount").attr('required',false); 
           $(".dis_percent").attr('required',false); 
           $(".dis_amt").attr('required',false); 
           $(".total_amt").attr('required',false); 

          $('.total_gst1').attr("style", "display:none");
           $('.amount').attr("style", "display:none");
           $('.dis_percent').attr("style", "display:none");
           $('.dis_amt').attr("style", "display:none");
           $('.total_amt').attr("style", "display:none");
         }
  </script>
  <script type="text/javascript">
    $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'yy-mm-dd'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    $("#challan_date").datepicker();
 });
});
  </script>

<!--                   <div class="row-2" >  
                   <div class="col-md-6">
                      <b>Challan Upload</b>
                      <input type="file" id="myfile1" name="myfile1[]" onclick="invoice_disable()" multiple="multiple" required><br>
                       <input type="number" id="challan_no" readonly="readonly" placeholder="Challan No" name="challan_no"  required>
                   </div>

                   <div class="col-md-6">
                       <b>Invoice Upload</b>
                       <input type="file" id="myfile2" name="myfile2[]" onclick="challan_disable()" multiple="multiple" required> <br>
                        <input type="number" id="invoice_no" readonly="readonly" placeholder="Invoice No" name="invoice_no"  required>         
                   </div>
                </div>  -->
                <script type="text/javascript">
                      function challan_disable() {
      document.getElementById("myfile1").disabled = true;
       document.getElementById("invoice_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
       /* $("#generatenumber").html(randomnumber );
        $("#invoice_no").val(randomnumber);*/
      $(document).ready(function() {
          $ ('#myfile2').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile2').val('');
              }
          });
      });
    }

    function invoice_disable() {
      document.getElementById("myfile2").disabled = true;
       
          document.getElementById("challan_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
        /*$("#generatenumber").html(randomnumber );
        $("#challan_no").val(randomnumber);*/
        $(document).ready(function() {
          $ ('#myfile1').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile1').val('');
              }
          });
      });
    }
                </script>
  </div>

          <div class="col-md-6">
            <div class="form-group row">
                  <div class="form-group">
                    <label class="a">Battery Type<font color="#FF0000">*</font></label>
                       <select style=" width: 280px;" class="form-control" required id="battry_type" name="battry_type">
                          <option value="">---Select---</option>
                          <option value="Maintenance Free">Maintenance Free</option>
                          <option value="Maintenance Required">Maintenance Required</option>
                       </select>
                   </div>

<!--       <div  class="form-group">
        <label class="a">Battery Type<font color="#FF0000">*</font></label>
        <select class="form-control" style=" width: 280px;" id="sel1">
            <option>Maintenance Free</option>
            <option>Maintenance Required</option>
       </select>
      </div> -->
<!--                     <div class="form-group">
                    <label>Model</label>
                       <select style=" width: 280px;" class="form-control" required id="model" name="model">
                          <option value="">---Select---</option>
                          <option value="PNO">AMARON</option>
                          <option value="PNAME">CAPRO CPR01</option>
                       </select>
                   </div> -->

              <div class="form-group">
              <label for="Model" >Model<font color="red">*</font></label>
              <div class="row">
                <div class="col-md-8">
                 <select  style="width: 100%;" id="model" name="model" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Model</option>
                      <?php 

                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT model FROM model");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['model'])?>" >
                        <?php 
                              echo ($row['model']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
               <div class="col-md-4">
                  <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
              </div> 
                <div id="result"></div>
              </div>
             </div> 
<!--        <div class="form-group">
        <label class="a">Model <font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="" name="" type="text" >
      </div> -->
    </div>
  <div class="form-group row">
          <div class="form-group">
            <label class="a">Amp. <font color="#FF0000">*</font></label>
            <input class="form-control" style=" width: 380px;" id="amp" name="amp" type="text" >
        </div>
      <div class="form-group">
        <label class="a">No. Of Plates<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="no_plates" name="no_plates" type="text" >
      </div>
  </div>
    <div class="form-group row">
        <div class="form-group">
        <label class="a">Warranty Years <font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="warranty_yers" name="warranty_yers" type="text" >
      </div>

        <div class="form-group">
            <label class="a">Status<font color="#FF0000">*</font></label>
              <select style=" width: 280px;" class="form-control" required id="battry_status" name="battry_status">
                  <option value="">---Select---</option>
                  <option value=" New">New</option>
                  <option value="Old">Old</option>
            
              </select>
          </div>
<!--       <div class="form-group">
        <label class="a">Status<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="" name="" type="text" >
      </div> -->
  </div>
      <div class="form-group row">
<!--         <div class="form-group">
        <label class="a">Invoice No.<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="invoice_no" name="invoice_no" type="text" >
      </div> -->
      <div class="form-group">
        <label class="a">Pur. Date<font color="#FF0000">*</font></label>
        <input class="form-control" style=" width: 380px;" id="pur_date" name="pur_date" type="date">
      </div>
  </div>

            </div>
   <!--          </form> -->
          </div>

        </div>
           </div>
      </div>
    </section>
        <div class="col-sm-offset-2 col-sm-8">
     <center>
        <button type="submit" name="submit"color="Primary" class="btn btn-primary">Save</button>
       <!--<a href="party_view.php" class="btn btn-warning">Close</a> -->
       <a href="battry_view.php" class="btn btn-warning">View</a>
     </center>
    </div>
    <div class="col-sm-offset-2 col-sm-8">

    </div>
  <br><br><br>
  </div> 
  </div> 
</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form method="POST" action="insert_model.php" id="model">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Model</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>model<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text"  style="width: 100%;" autocomplete="off" class="form-control" id="model" name="model" placeholder="model"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#model").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_model.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>
