<?php 
include("connect.php");

?>
<!DOCTYPE html>
<html>
  <head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <?php include("aside_main.php"); ?>

  <script>  
    $(document).ready(function(){  
     $.datepicker.setDefaults({  
          dateFormat: 'yy-mm-dd'   
     });  
     $(function(){  
          $("#claim_date").datepicker();
     });
    });  
  </script>
  </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
     <form method="post" action="insert_temp_battery.php" enctype="multipart/form-data" autocomplete="off">
      <div class="content-wrapper">
        <section class="content-header">
           <a href='hold_tyreservice.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>&nbsp;Reserved Remould Repairing</a>
           
          <!--  <a href='add_remould.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Send for Remould</a> -->

          <h1>Battery Send Repairing</h1>
        </section>
        <section class="content">
          <div class="box box-info">
            <div class="box-body">
              <?php $usernameSet=$_SESSION['username'];
                // echo $usernameSet;
               ?>

                <div class="row-md-3">
                    <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>" required="required">

                    <input type="hidden" name="type" id="type" class="form-control" value="temp_remould" required="required">
            
                    <div class="col-md-4">
                      <label>Repairing Date:</label>
                      <input type="text" name="remould_date" id="claim_date" class="form-control" placeholder="Repairing Date" required="required"/>  
                    </div>  

                 </div>

              <!-- <input type="hidden" name="office" id="office" value="<?php echo $usernameSet; ?>" class="form-control" /> -->
              <div class="container" style="width: 100%">
                <div class="table-wrapper">
                  <div class="table-title">
                    <div class="row">
                      <div class="col-sm-10"><h3>Battery Details</h3></div>
                      <div class="col-sm-2">
                        <div align="right">
                          <button type="button" class="btn btn-info add-new" onclick="addTyre('tyre_table')"> <i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive"> 
                    <table id="tyre_table" class="table table-bordered">
                      <thead>
                        <tr>
                          <th><font color="red">*</font>Battery No</th>
                           <th><font color="red">*</font>Volt</th>
                          <th><font color="red">*</font>Watt</th>
                           <th><font color="red">*</font>Model</th>
                          <th><font color="red">*</font>Purchase Date</th>
                          <th>Remove</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <center>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
                </center>
              </div><br>
          </div>
        </div>
        </section>
         
      <br><br><br>
      </div>
    </form>
    <?php include("footer.php");?>
    </div>
    </body>
</html>
<script type="text/javascript"> 

    $(function()
    {    
    $( "#battery_no" ).autocomplete({
    source: 'bnor_fetch.php',
    change: function (event, ui) {
    if(!ui.item){
    $(event.target).val(""); 
    $(event.target).focus();

    } 
    },
    focus: function (event, ui) {  return false;
    }
    });
    });

</script> 

<script>

    function addTyre(tyre_table) {

        var rowCount = document.getElementById("tyre_table").rows.length;
        var row = document.getElementById("tyre_table").insertRow(rowCount);
      
        var cell1 = row.insertCell(0);
        cell1.innerHTML ="<input type='text' style='width: 200px;' name='battery_no[]' id='battery_no"+rowCount+"' style='width: 250px;' required/>";
        
        var cell2 = row.insertCell(1);
        cell2.innerHTML = "<input type='text' style='width: 200px;' name='volt[]' id='volt"+rowCount+"' readonly/>";

        var cell3 = row.insertCell(2);
        cell3.innerHTML = "<input type='text' style='width: 200px;' name='watt[]' id='watt"+rowCount+"' readonly/>";

         var cell4 = row.insertCell(3);
         cell4.innerHTML = "<input type='text' style='width: 200px;' name='model[]' id='model"+rowCount+"' readonly/>";

         var cell5 = row.insertCell(4);
         cell5.innerHTML = "<input type='text' style='width: 200px;' name='pur_date[]' id='pur_date"+rowCount+"' readonly/>";

        var cell6 = row.insertCell(5);
        cell6.innerHTML =  "<button type='button' class='btn btn-danger' id='remove"+rowCount+"'><i class='fa fa-remove'></i></button>";
      
        $("#remove"+rowCount).click(function(){
         $(this).parents("tr").remove();
        });

        $("#battery_no"+rowCount).change(function(){
          var tyre_number=$("#battery_no"+rowCount).val();
          $.get("all_bateryservicefetch.php",{txt:tyre_number},function(data,status){
            var arr = $.parseJSON(data);
              $("#volt"+rowCount).val(arr[0]);
              $("#watt"+rowCount).val(arr[1]);
              $("#model"+rowCount).val(arr[2]);
              $("#pur_date"+rowCount).val(arr[3]);
              });
        });

         $(function()
        { 
          $("#vendor"+rowCount).autocomplete({
          source: 'vendor_name_auto.php',
          change: function (event, ui) {
                if(!ui.item){
          $(event.target).val(""); 
          $(event.target).focus();
          alert('Vendor Name does not exist in our Record');
            $("#vendor_office"+rowCount).val("");
          } 
        },
        focus: function (event, ui) { return false; } }); }); 

         $("#vendor"+rowCount).change(function(){
          var vendor=$("#vendor"+rowCount).val();
          $.get("fetch_data_on_vendor.php",{term:vendor},function(data,status){
            var arr = $.parseJSON(data);
             $("#vendor"+rowCount).val(arr[0]);
             $("#vendor_office"+rowCount).val(arr[1]);
          });
        });

        $(function()
        { 
          $("#battery_no"+rowCount).autocomplete({
          source: 'fetch_no_battery.php',
          change: function (event, ui) {
          if(!ui.item){
          $(event.target).val(""); 
          $(event.target).focus();
          alert('Battery number does not exist in our stock');
          $("#brand"+rowCount).val('');
          $("#production_month"+rowCount).val('');
          $("#grn_date"+rowCount).val('');
           $("#vendor_office"+rowCount).val('');
          $("#vendor"+rowCount).val('');
         
          } 
        },
        focus: function (event, ui) { return false; } }); });
    }
</script>
