<?php

require_once("connect.php");
$usernameSet=$_SESSION['valid_user'];
//fetch.php
/*$connect = mysqli_connect("localhost", "root", "", "hms");*/
$columns = array('sl_no', 'office', 'grn_no', 'vendor','challan_no','invoice_no');
$searchTerm = "GRN-N";
$query = "SELECT * FROM main_grn WHERE  grn_no LIKE '%$searchTerm%' AND office = '$usernameSet' AND ";

if($_POST["is_date_search"] == "yes")
{
 $query .= 'grn_date BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}

if(isset($_POST["search"]["value"]))
{
 $query .= '
  (sl_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR grn_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR office LIKE "%'.$_POST["search"]["value"].'%" 
  OR vendor LIKE "%'.$_POST["search"]["value"].'%"
  OR challan_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR invoice_no LIKE "%'.$_POST["search"]["value"].'%" )
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY sl_no ASC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<a href="edit_tyre_grn.php?link='.$row["grn_no"].'" class="btn btn-xs fa fa-pencil"></a>&nbsp;<a href="view_tyre_grn_detail.php?link='.$row["grn_no"].'" class="btn btn-xs fa fa-eye"></a>';
 $sub_array[] = $row["grn_no"];
 $sub_array[] = $row["type"];
 $sub_array[] = $row["grn_date"];
 $sub_array[] = $row["vendor"];
 $sub_array[] = $row["office"];
 $sub_array[] = $row["challan_no"];
 $sub_array[] = $row["invoice_no"];
 $sub_array[] = $row["tyre_count"];
 $sub_array[] = $row["amount"];
 $data[] = $sub_array;
}

function get_all_data($conn)
{
	$usernameSet=$_SESSION['valid_user'];
	$searchTerm = "GRN-N";
	 $query = "SELECT * FROM main_grn  WHERE grn_no  LIKE '%$searchTerm%' AND office = '$usernameSet' ";
	 $result = mysqli_query($conn, $query);
	 return mysqli_num_rows($result);
}
$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>