<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">


<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">

       <!-- <a href='receive_remould_grn.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-back"></i>&nbsp;Back</a> -->

       <a href='upload_invice_on_send_rmd.php' style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i>&nbsp;Upload Invoice On Remould Send</a>

      <h1>Receive Tyre From Claim</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <div class="col-md-12">
              <div class="table-responsive">  
               <form action="insert_receive_outward.php" method="post" autocomplete="off" enctype="multipart/form-data">
            
                <input type="hidden" name="grn_no" value="<?php echo $remould_no; ?>" >
                  <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size: 12px;width: 95%;">  
                            <tbody>
                              <tr class="table-active">
                                <th>Id</th>
                                  <th>Tyre No</th>
                                  <th>Brand</th>
                                  <th>Product Size</th>
                                    <th style="display: none;">State</th>
                                    <th>Remark</th>
                                   <th>Change State</th>
                                   <th>Update</th>
                                   <th>Receive</th>
                              </tr>
                            </tbody>  
                             <?php
                             $valuetosearch = $_POST['grn_no'];
                             $query = "SELECT * FROM final_outward_stock WHERE unique_no = '$valuetosearch' and receive_or_not='0' and status='send_claim'";
                              $result = mysqli_query($conn,$query);
                              $id_customer = 0;
                              $i = 1;
                                while($row = mysqli_fetch_array($result)){
                                 $id = $row['id'];
                                 $tyre_no=$row['tyre_no'];
                                 $BrandName = $row['BrandName'];
                                 $ProductSize = $row['ProductSize'];
                                 $status = $row['status'];
                                 $remark = $row['remark'];
                                 $vendor = $row['vendor'];
                                 $vendor_office = $row['vendor_office'];
                                 $stock_operation_date = $row['stock_operation_date'];
                              ?>
                                <tr>
                                  <input type="hidden" name="vendor[]" value="<?php echo $vendor; ?>" >

                                    <input type="hidden" name="grn_no" value="<?php echo $valuetosearch; ?>" >

                                  <input type="hidden" name="vendor_office[]" value="<?php echo $vendor_office; ?>" >
                                  <td ><?php echo $i; ?></td>

                                  <input type="hidden" name="stock_operation_date[]" id="stock_operation_date<?php echo $id; ?>" value="<?php echo $stock_operation_date; ?>" >

                                  <td style="display: none;"><?php echo $id; ?><input type="hidden" id="id<?php echo $id; ?>" name="id[]" value="<?php echo $id; ?>" ></td>
                                  
                                  <td><?php echo $tyre_no; ?><input type="hidden" id="tyre_no<?php echo $id; ?>" name="tyre_no[]" value="<?php echo $tyre_no; ?>"></td>
                                  
                                  <td><?php echo $BrandName; ?><input type="hidden" id="brand<?php echo $id; ?>" name="BrandName[]" value="<?php echo $BrandName; ?>" ></td>

                                   <td><?php echo $ProductSize; ?> <input type="hidden" id="ProductSize<?php echo $id; ?>" name="ProductSize[]" value="<?php echo $ProductSize; ?>" ></td>

                                   <td style="display: none;"><?php echo $status; ?> <input type="hidden" name="status"  value="<?php echo $status; ?>" ></td>

                                   <td><?php echo $remark; ?> <input type="hidden" id="remark<?php echo $id; ?>" name="remark[]" value="<?php echo $remark; ?>" ></td>

                                    <td>
                                      <select  required="required" id="update<?php echo  $id; ?>" style="width: 150px;-webkit-appearance: none;height: 28px;" >
                                         <option disabled="disabled" selected="selected">Change State</option>
                                         <option value="temp_remould">Remould</option>
                                         <option value="temp_scrap">Scrap</option>
                                         <option value="temp_theft">Theft</option>
                                         <option value="null">Reuse</option>
                                      </select>
                                    </td>

                                    <td><input type="button" onclick="Editdata(<?php echo $id; ?>)" value="Update"></td>
                                    
                                      <script>
                                        function Editdata(id)
                                        {     var id = id;
                                              var vendor = '<?php echo $vendor ?>';
                                              var vendor_office = '<?php echo $vendor_office ?>';
                                              var grn_no = '<?php echo $valuetosearch ?>';
                                              var office = '<?php echo $usernameSet ?>';
                                              var tyre_no = document.getElementById('tyre_no'+id).value;
                                              var brand = document.getElementById('brand'+id).value;
                                              var stock_operation_date = document.getElementById('stock_operation_date'+id).value;
                                              var ProductSize = document.getElementById('ProductSize'+id).value;
                                               var update = document.getElementById('update'+id).value;
                                                if (update=="Change State") {
                                                 alert("Select State to update");
                                              }else{
                                                  jQuery.ajax({
                                                  url: "update_outward_status.php",
                                                  data: {id:id,tyre_no:tyre_no,update:update,grn_no:grn_no,stock_operation_date:stock_operation_date,office:office,brand:brand,ProductSize:ProductSize,vendor:vendor,vendor_office:vendor_office},
                                                  type: "POST",
                                                  success: function(data) {
                                                    alert(data)
                                                    location.reload(); 
                                                  $("#result_main").html(data);
                                                  },
                                                      error: function() {}
                                                  });
                                              }
                                          }
                                        </script>

                                    <td>
                                        <input type="checkbox" name="id_customer[]" value='<?php echo $id_customer; ?>'>
                                    </td>
                                  
                                </tr>
                            
                                <?php $i++; $id_customer++;
                                }
                                ?>

                            </table> 
                              
                                   <button style="float: right;" type="submit" href= "" id="submit_disable"  class="btn btn-primary">Save Claim</button>
                                
                             
                        
              </form><br><br>
              </div>  
            </div> 
          <!--   Second Form to receive claim -->
                <?php
                     $query = "SELECT * FROM final_outward_stock WHERE unique_no = '$valuetosearch' and receive_or_not='0' ";
                      $result = mysqli_query($conn,$query);
                      $id_customer = 0;
                      $i = 1;
                      if (mysqli_num_rows($result)>0) {
                        ?>

            <h3>Reserved Tyre From Claim</h3>
           <div class="col-md-12">
              <div class="table-responsive">  
               <form action="insert_receive_outward.php" method="post" autocomplete="off" enctype="multipart/form-data">

                 <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size: 12px;width: 95%;"> 
                            
                                 <tbody>
                                  <tr class="table-active">
                                    <th>Id</th>
                                      <th>Old Tyre No</th>
                                      <th><font color="red">*</font>New Tyre No</th>
                                      <th><font color="red">*</font>New Tyre Price</th>
                                      <th><font color="red">*</font>Production Month</th>
                                      <th><font color="red">*</font>Brand</th>
                                       <th><font color="red">*</font>Product Size</th>
                                      <th><font color="red">*</font>Ply</th>
                                      <th><font color="red">*</font>Max Nsd</th>
                                      <th><font color="red">*</font>Min Nsd</th>
                                  </tr>
                                </tbody> 
                                <?php
                                while($row = mysqli_fetch_array($result)){
                                 $id1 = $row['id'];
                                 $tyre_no1=$row['tyre_no'];
                                 $BrandName1 = $row['BrandName'];
                                 $ProductSize1 = $row['ProductSize'];
                                 $status1 = $row['status'];
                                 $remark1 = $row['remark'];
                                 $vendor1 = $row['vendor'];
                                 $vendor_office = $row['vendor_office'];
                                 $stock_operation_date = $row['stock_operation_date'];
                              ?>
                                <tr>
                                  <input type="hidden" name="vendor[]" value="<?php echo $vendor; ?>" >

                                    <input type="hidden" name="grn_no" value="<?php echo $valuetosearch; ?>" >

                                  <input type="hidden" name="vendor_office[]" value="<?php echo $vendor_office; ?>" >
                                  <td ><?php echo $i; ?></td>

                                   <td style="display: none;"><input type= "hidden" onclick="fetch_id(<?php echo $id1; ?>)" id="id<?php echo $id1; ?>" name="tyre_no[]" value="<?php echo $id1; ?>"></td>
                                  
                                  <td><?php echo $tyre_no1; ?><input type="hidden" id="tyre_no<?php echo $id1; ?>" name="tyre_no[]" value="<?php echo $tyre_no; ?>"></td>

                                   <td><input type='text' style='width: 120px;' name='new_tyre[]' id='new_tyre<?php echo $id1; ?>"' required/></td>

                                   <td><input type='text' style='width: 120px;' name='new_tyre_price[]' id='new_tyre_price<?php echo $id1; ?>"' required/></td>
                                  
                                  <td><input type='text' style='width: 120px;' name='product_month[]' id='weekPicker1"' required/></td>

                                   <td><input type='text' style='width: 120px;' onclick="chk_id('<?php echo $id1; ?>')" name='brand[]' id='brand<?php echo $id1; ?>' required/></td>

                                   <td><input type='text' style='width: 90px;' readonly name='ProductSize[]' id='ProductSize<?php echo $id1; ?>' required/></td>

                                   <td><input type='text' style='width: 90px;' readonly name='ProductPlyRating[]' required id='ProductPlyRating<?php echo $id1; ?>'/></td>

                                   <td><input type='text' style='width: 90px;' readonly name='productMaxNsd[]' required id='productMaxNsd<?php echo $id1; ?>'/></td>

                                   <td><input type='text' style='width: 90px;' readonly name='productMinNsd[]' required id='productMinNsd<?php echo $id1; ?>'/></td>
                                  
                                </tr>
                            
                                <?php $i++; $id_customer++;
                                }
                                ?>
                            </table>
                            <script type="text/javascript">

                              convertToWeekPicker($("#weekPicker1"))

                              function chk_id(id1){
                                 $(function(id1)
                                    { $("#brand"+id1).autocomplete({
                                      source: 'search_brand_name_auto.php',
                                      change: function (event, ui) {
                                      if(!ui.item){
                                      $(event.target).val(""); 
                                      $(event.target).focus();
                                      alert('Tyre Brand Name does not exists');
                                      }
                                    },
                                    focus: function (event, ui) { return false; } }); });
                              }

                             // convertToWeekPicker($("#weekPicker1"+id))
                                   

                                 /*   $("#brand"+id1).change(function(){
                                      var brand=$("#brand"+id1).val();
                                      $.get("search_brand_data.php",{txt:brand},function(data,status){
                                        alert(data);
                                        var arr = $.parseJSON(data);
                                        $("#ProductSize"+id1).val(arr[0]);
                                        $("#ProductPlyRating"+id1).val(arr[1]);
                                        $("#productMaxNsd"+id1).val(arr[2]);
                                        $("#productMinNsd"+id1).val(arr[3]);
                                         });
                                    });*/
                            </script>                       
                        <div class="row-2">  
                            <div class="col-md-6">
                                 <b>Invoice Upload</b>
                                 <input type="file" id="myfile2" name="invoice_file[]" required multiple="multiple" > <br>
                                  <input type="number" id="invoice_no" required placeholder="Invoice No" name="invoice_no"  >         
                             </div>
                             
                             <div class="col-md-6">
                                <b>Claim Settlement Latter</b>
                                <input type="file" id="myfile1" required name="challan_file[]"  multiple="multiple" ><br>
                                 <input type="number" id="challan_no" required placeholder="Claim Settlement No" name="challan_no"  >
                             </div>
                          </div> 
                            <button style="float: right;" type="submit" href= "" id="submit_disable"  class="btn btn-primary">Receive Claim</button>
                 </form><br><br><br><br><br><br><br><br><br>
              </div> 
            </div> 
             <?php  }else{
                  //echo "No Data";
                        } ?>

                        




      </div>
      <div id="result"></div> 
    
    </div>
    </section>
     
  
  </div>
<?php include("../footer.php");?>
</div>
</body>
</html>

<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}
</style>
<script>
  function invoice_select() {
    $(document).ready(function() {
      $ ('#invoice_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#invoice_file').val('');
          }
      });
    });
  }
  function challan_select() {
    $(document).ready(function() {
      $ ('#challan_file').bind('change', function() {
          var fileSize = this.files[0].size/1024/1024;
          if (fileSize > 2) { // 2M
            alert('Your max file size exceeded');
            $('#challan_file').val('');
          }
      });
    });
  }
</script>
<script type="text/javascript">
function ShowHideDiv(chkPassport) {
  var dvinvoice_no = document.getElementById("dvinvoice_no");
  dvinvoice_no.style.display = chkPassport.checked ? "block" : "none";
  invoice_no.setAttribute( 'required', 'required' );
  var dvinvoice_date = document.getElementById("dvinvoice_date");
  dvinvoice_date.style.display = chkPassport.checked ? "block" : "none";
  invoice_date.setAttribute( 'required', 'required' );
  var dvinvoice_file = document.getElementById("dvinvoice_file");
  dvinvoice_file.style.display = chkPassport.checked ? "block" : "none";
  invoice_file.setAttribute( 'required', 'required' );
}

$(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    $("#challan_date").datepicker();
 });
});

$(function()
{ 
  $("#remould_no").autocomplete({
  source: 'autocomplete_remould_no.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Remould No does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

function Getremould_data(elem)
{
  var remould_no = elem;
  if(remould_no!='')
  {
    $.ajax({
      type: "POST",
      url: "search_remould_data.php",
      data:'remould_no='+remould_no,
      success: function(data){
        $("#result").html(data);
      }
    });
  }
}

function challan_disable() {
      document.getElementById("myfile1").disabled = true;
       document.getElementById("invoice_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
       /* $("#generatenumber").html(randomnumber );
        $("#invoice_no").val(randomnumber);*/
      $(document).ready(function() {
          $ ('#myfile2').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile2').val('');
              }
          });
      });
    }

    function invoice_disable() {
      document.getElementById("myfile2").disabled = true;
       
          document.getElementById("challan_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
        /*$("#generatenumber").html(randomnumber );
        $("#challan_no").val(randomnumber);*/
        $(document).ready(function() {
          $ ('#myfile1').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile1').val('');
              }
          });
      });
    }

    
</script>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="src/weekPicker.js"></script>