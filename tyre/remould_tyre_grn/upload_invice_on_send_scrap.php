<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

<script type="text/javascript">
   $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
    alert("data");
 });
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
       <a href='receive_scrap_grn.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a>
      <h1>Upload Invoice Over Challan For Receive Scrap</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php  
                                 $username =  $_SESSION['username']; 
                                  $query = "SELECT  id,receiving_no,receiving_date,challan_no,challan_file,status FROM  outward_stock WHERE receive_or_not = '1'  and  office='$username' AND length(challan_file) > 15 and status = 'send_scrap' and length(invoice_file) < 15 group by receiving_no";

                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;
                                   if(mysqli_num_rows($result) > 0){
                                    ?>
                                    <thead>  
                                       <tr>  
                                         <th style="display: none;" scope="row">Id</th>
                                                <th>Receiving No</th>
                                                <th style="width: 120px;">Receiving Date</th>
                                                <!--  <th>Total Tyre</th> -->
                                                <th style="width: 120px;">Challan File</th>
                                               <th >Invoice No</th>
                                                <th >Invoice File</th>
                                                <th>Submit</th>
                                       </tr>  
                                  </thead>  
                                     <?php
                                    while($row = mysqli_fetch_array($result)){
                                   $id = $row['id'];
                                    $receiving_no=$row['receiving_no'];
                                    $receiving_date=$row['receiving_date'];
                                     $status=$row['status'];
                                     $challan_no = $row['challan_no'];
                                    $challan_file = $row['challan_file'];
                                   $challan_file1=explode(",",$challan_file);
                                   $count3=count($challan_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                 <td>
                                   <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $receiving_no?>" >
                                      <input type="hidden" id="receiving_no<?php echo $receiving_no?>" value="<?php echo $receiving_no?>">
                                    </td>

                                     <td> <?php echo $receiving_date; ?><input type="hidden" name="grn_date[]"  id="receiving_date<?php echo $id_customer; ?>" value='<?php echo $receiving_date; ?>'></td>

                                  <!--    <td> <?php echo $tyre_no; ?><input type="hidden" name="tyre_no[]"  id="tyre_no<?php echo $id_customer; ?>" value='<?php echo $tyre_no; ?>'></td> -->
                                    
                                     <td>
                                     Challan No: <?php echo $challan_no; echo "<br>"; ?>
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="../<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]"  value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                      ?>
                                     </td>

                                   <form method="post" action="insert_overchallan_in_recv_rmd.php" enctype="multipart/form-data">

                                     <td>
                                       <input type="number"  required id="invoice_no" name="invoice_no"   required/>

                                       <input type="hidden" value="<?php echo $receiving_no; ?>" required id="receiving_no" name="receiving_no"   required/>

                                       <input type="hidden" value="<?php echo $status; ?>" required id="status" name="status"   required/>

                                        <input type="hidden" value="<?php echo $username; ?>" required id="username" name="username"   required/>
                                     </td>
                                    
                                     <td>
                                       <input type="file"  required id="invoice_file" name="invoice_file[]" multiple="multiple"  required/>
                                     </td>

                                     <td>
                                       <input type="submit" value="Upload" class="btn-sm btn-success" name="submit" multiple="multiple"  required/>
                                     </td>
                                  </form>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }  }

                   else {
                       echo "No Data";
                  }
                  ?>  
                </table>  
                      

                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>
