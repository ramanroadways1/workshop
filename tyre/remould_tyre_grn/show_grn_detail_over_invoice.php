<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
     <!--   <a href='upload_invice_on_newtyre.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a> -->
      <h1>Tyre Detail</h1>
    </section>
    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="show_grn_detail_over_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px;" >  
                        <thead>
                          <tr>
                             <th>Sno</th>
                              <th>Tyre No</th>
                              <th>Production Month</th>
                              <th>Brand</th>
                              <th>Size</th>
                              <th>Ply</th>
                              <th>MaxNsd</th>
                              <th>Min Nsd</th>
                             <!--   <th>Remove</th> -->
                          </tr>
                 </thead>
                    <?php
                    $valuetosearch = $_POST['grn_no'];
                      $sql1 = "select * from grn_over_tyre where receiving_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $sno = 1;
                      while ($row = mysqli_fetch_array($result1)) 
                          { 
                            $id = $row['sl_no'];
                            $tyre_no1 = $row['tyre_no'];
                            $product_month1 = $row['production_month'];
                            $brand1 = $row['brand_name'];
                             $amount1 = $row['amount'];
                            $dis_percent1 = $row['discount_percent'];
                            $dis_amt1 = $row['discount_amount'];
                            $total_amt1 = $row['total_amount'];
                            $tyre_remarks = $row['tyre_remarks'];
                            $ProductSize = $row['ProductSize'];
                            $ProductPlyRating = $row['ProductPlyRating'];
                            $productMaxNsd = $row['productMaxNsd'];
                            $productMinNsd = $row['productMinNsd'];
                          ?>
        <tbody >
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?></td>
              <td><?php echo $tyre_no1; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no" value="<?php echo $tyre_no1; ?>" ></td>

              <td><?php echo $product_month1; ?><input readonly type="hidden" class="form-control" style="width: 120px;"  name="product_month" value="<?php echo $product_month1; ?>" ></td>

              <td><?php echo $brand1; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="brand" readonly value="<?php echo $brand1; ?>" ></td>
              
              <td><?php echo $ProductSize; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductSize" readonly value="<?php echo $ProductSize; ?>" ></td>

              <td><?php echo $ProductPlyRating; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="ProductPlyRating" readonly value="<?php echo $ProductPlyRating; ?>" ></td>

              <td><?php echo $productMaxNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMaxNsd" readonly value="<?php echo $productMaxNsd; ?>" ></td>

              <td><?php echo $productMinNsd; ?><input  readonly type="hidden" class="form-control" style="width: 120px;" name="productMinNsd" readonly value="<?php echo $productMinNsd; ?>" ></td>

              <!--  <td><button type="button" class="btn btn-danger" onclick="DeleteModal('<?php echo $id ?>')" name="remove_first" id="remove_first<?php echo $id ?>">X</button></td> -->
          </tr>
                                <?php  
                                  $sno++; }
                              ?>
          </tbody>               
                            </table>
                      

                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
  </div>

<?php include("../footer.php");?>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
