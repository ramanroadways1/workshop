  <?php 
  require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <?php include("../aside_main.php");
     ?>

     <style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
	  	<div class="content-wrapper">
		    <section class="content-header">
		      <h1>Create Inspection</h1>
		    </section>

		    <section class="content">
		    		<div class="box box-info">
		            <div class="box-body">
		              	 <div class="row-md-2">
                       <div class="col-md-6">
                           <form>
                                <div class="form-group">
                                  <label>Vehicle No:</label>
                                  <input type="text"  name="vehicle_no" class="form-control" id="vehicle_no" required/>
                                </div>
                                <div class="form-group">
                                  <label>Wheeler:</label>
                                  <input type="text" name="wheeler" id="wheeler" class="form-control"  placeholder="Wheeler" readonly />
                                </div>
                          </form> 
                          </div> <br><br>
                            <div class="col-md-6">
                                 <center><div id="axis"></div></center>
                            </div>
                     </div>
			           </div>
               
					</div>  
        </section>
				

	              
          		</div>
          		
<?php include("../footer.php");?>
	    
		</div>

</body>
</html>

<form id="form" method="POST" autocomplete="off" enctype="multipart/form-data"> 
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Tyre Details</h4>
        </div>

        <div class="modal-body">
          <div id="alert1" style="font-size:18px; color: red; " hidden>
            Tyre already is in Inspection on this position...
          </div><br>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Wheel Position:</label>
                <input type="text" class="form-control" name="wp" id="wp" readonly/>
                <input type="hidden" name="office" id="office2" class="form-control" value="<?php echo $usernameSet; ?>"/>
                 <input type="hidden" name="office" id="total_tyre" name="total_tyre" class="form-control" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Number:</label>
                <input type="text" class="form-control" name="vehicle_no_modal" id="vehicle_no_modal" readonly/>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Target Psi:</label>
                <input type="text" class="form-control" name="target_psi" id="target_psi" readonly/>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Current Nsd:</label><br>
                <div class="col-md-2">
                  <font>
                 NSD1<input type="Number" style="width: 70px;" class="form-control" name="nsd1" id="nsd1" required="required"/>
                 NSD2<input type="Number" style="width: 70px;" class="form-control" name="nsd2" id="nsd2" required="required" />
                 </font>
                </div>

                <div class="col-md-2">
                 <font style="float: right;margin-right: -190px;">
                 NSD3<input type="Number" style="width: 70px;" class="form-control" name="nsd3" id="nsd3" required="required"/>
                 NSD4<input type="Number" style="width: 70px;" class="form-control" name="nsd4" id="nsd4" required="required"/>
                 </font> 
                </div>
              </div>
            </div>

             <div class="col-md-4">
              <div class="form-group">
                <label>Wear:</label>
                 <select onchange="SelectStatus(this.value)" style="height: 70px;" class="form-control" id="wear" name="wear" multiple>
                        <option hidden value="">---Select Wear---</option>
                        <?php 
                          $get=mysqli_query($conn,"SELECT * FROM wear");
                            while($row = mysqli_fetch_assoc($get))
                            {
                          ?>
                          <option value="<?php echo ($row['wear']); ?>"><?php echo ($row['wear']); ?></option>
                            <?php
                            }
                          ?>
                      </select>
              </div>
            </div>


             <div class="col-md-4"> 
              <div class="form-group">
                <label>New Psi:</label>
                <input type="Number" class="form-control" name="new_psi" id="new_psi" required="required" />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Wall Cap:</label>
                 <select onchange="SelectStatus(this.value)" class="form-control" id="wall_cap" name="wall_cap">
                        <option hidden value="">---Select Wall Cap---</option>
                        <option value="wall_cap_yes">Yes</option>
                        <option value="wall_cap_no">No</option>
                      </select>
              </div>
            </div> 

            <div class="col-md-4">
              <div class="form-group">
                <label>Upload Image:</label>
                 <input type="file" name="insp_img" id="insp_img" class="form-control" required="required">
              </div>
            </div> 

            <div class="col-md-4">
              <div class="form-group">
                <label>Tyre No:</label>
                 <input type="text" name="tyre_no" readonly="readonly" id="tyre_no"  class="form-control" >
              </div>
            </div> 

            <div class="col-md-4">
              <div class="form-group">
                <label>Remark:</label>
                 <input type="text" name="remark" id="remark" class="form-control" required="required">
              </div>
            </div> 

          </div>
        </div>
        <script type="text/javascript">
        	$(document).ready(function(){  
			   $.datepicker.setDefaults({  
			        dateFormat: 'dd-mm-yy'   
			   });  
			   $(function(){  
			        $("#issue_date").datepicker();
			   });
			});

        </script>

        <div class="modal-footer">
          <button type="submit" id="Submit_me" onclick="myfunction();" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</form>
<div id="result22"></div>
<div id="result33"></div>
<script type="text/javascript"></script>

<script> 

  var changeColor = function(obj){

  if(obj.value = wp){
    obj.style.backgroundColor = 'green';
  } else if(obj.value >= 6 && obj.value <= 9){
    obj.style.backgroundColor = 'orange';
  } else if(obj.value > 9){
    obj.style.backgroundColor = 'red';
  }
};

 function chk_data_for_color_red(wp){
  alert(wp);

  /*document.getElementByClassName("btn").innerHTML = wp;*/
 // alert(wp);
 }
  function chk_data_for_color_blk(all_wp){
   //alert(all_wp);
/*document.getElementByClassName("btn").innerHTML = all_wp;*/
 }

  function success_msg() {
    alert("Inspection done");
  }
   function not_success_msg() {
    alert("Error...Inspection not done");
  }

	function myfunction()
		{
        var wp = $('#wp').val();
			  var vehicle_no_modal = $('#vehicle_no_modal').val();
			  var old_psi = $('#old_psi').val();
			  var nsd1 = $('#nsd1').val();
			  var nsd2 = $('#nsd2').val();
			  var nsd3 = $('#nsd3').val();
			  var nsd4 = $('#nsd4').val();
			  var wear = $('#wear').val();
         var total_tyre = $('#total_tyre').val();
			  var wall_cap = $('#wall_cap').val();
			  var remark = $('#remark').val();
        var tyre_no = $('#tyre_no').val();
        var new_psi = $('#new_psi').val();
		    var insp_img = $('#insp_img').val();
		    var office = $('#office2').val();
            if((nsd4!='')&&(wear!='')&&(wall_cap!='')&&(insp_img!='')&&(nsd2!=''))
            {
                $.ajax({
                  type: "POST",
                  url: "save_tyre_log.php",
                  data:'wp='+wp + '&vehicle_no_modal='+vehicle_no_modal + '&old_psi='+old_psi + '&nsd1='+nsd1 + '&nsd2='+nsd2 + '&nsd3='+nsd3+ '&nsd4='+nsd4 + '&wear='+wear + '&wall_cap='+wall_cap + '&remark='+remark + '&insp_img='+insp_img + '&office='+office+ '&tyre_no='+tyre_no + '&new_psi='+new_psi+ '&total_tyre='+total_tyre,
                    success: function(data){
                      alert(data);
                      $("#save_result").html(data);
                    } 
                });
              }
                
		}

   

function setEventId(event_id){
  var wp = event_id;
  $('#wp').val(event_id);
  var total_tyre=document.getElementById('wheeler').value;
  $('#total_tyre').val(total_tyre);
  var vehicle_no=document.getElementById('vehicle_no').value;
  $('#vehicle_no_modal').val(vehicle_no);
  $.ajax({
    type: "POST",
    url: "check_added_tyre.php",
    data:'wp=' + event_id + '&vehicle_no=' + $('#vehicle_no').val(),
    success: function(data){
       $("#result22").html(data);
      }
    });
  }

  $(function()
{ 
  $("#vehicle_no").autocomplete({
  source: 'search_vehicle_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vehicle Name does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

$("#vehicle_no").change(function(){
  var vehicle_no=$("#vehicle_no").val();
  $.get("search_vehicle_no.php",{txt:vehicle_no},function(data,status){
    $("#vehicle_no").empty();
    $("#wheeler").empty();
     $("#target_psi").empty();
     
    var arr = $.parseJSON(data);
    $("#vehicle_no").val(arr[0]);
    $("#wheeler").val(arr[1]);
     $("#target_psi").val(arr[2]);
     var target_psi =  document.getElementById('target_psi').value;

    var wheeler=arr[1];

    if (target_psi =='0') {
      alert("target Psi is not set on this vehicle no...set Psi first");
       $("#wheeler").val('');
       $("#vehicle_no").val('');

    }else{

     $.ajax({
        type: "POST",
        url: "chk_tyre_for_color.php",
        data:'vehicle_no=' + vehicle_no,
        success: function(data){
          $("#result33").html(data);
          }
       });
   
      if(wheeler=='4')
      {
        document.getElementById("axis").innerHTML = "<button id='FL' data-toggle='modal' data-target='#myModal'  value='FL' class='btn' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
          "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn'  value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
          "<button id='RL' data-toggle='modal' data-target='#myModal'  value='RL' onclick='setEventId(this.value);' type='button'></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
          "<button id='RR' data-toggle='modal' data-target='#myModal' class='btn'  value='RR' onclick='setEventId(this.value);' type='button'></button><br><br>"+
          "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";
      }
      else if(wheeler=='6'){
        document.getElementById("axis").innerHTML = "<button id='FL' data-toggle='modal' data-target='#myModal' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal'class='btn'  value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn'  value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn'  value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";
      }
      else if(wheeler=='10'){
        document.getElementById("axis").innerHTML = "<button id='FL' data-toggle='modal' data-target='#myModal' ' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn'  value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn'  value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn'  value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal'class='btn'  value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn'  value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='btn'  value='LR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='btn'  value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal'  value='RR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='btn'  value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";
         }
      else if(wheeler=='12'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='btn' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br>"+
        "<button id='FL1' data-toggle='modal' data-target='#myModal' class='btn' value='FL1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR1' data-toggle='modal' data-target='#myModal' class='btn' value='FR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn' value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='btn' value='LR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='btn' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='btn' value='RR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='btn' value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";        
      }
      else if(wheeler=='14'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='btn' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='FL1' data-toggle='modal' data-target='#myModal' class='btn' value='FL1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR1' data-toggle='modal' data-target='#myModal' class='btn' value='FR1' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='FL2' data-toggle='modal' data-target='#myModal' class='btn' value='FL2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR2' data-toggle='modal' data-target='#myModal' class='btn' value='FR2' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn' value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal'  class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='btn' value='LR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='btn' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='btn' value='RR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='btn' value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";        
      }
      else if(wheeler=='16'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='btn' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='FL1' data-toggle='modal' data-target='#myModal' class='btn' value='FL1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR1' data-toggle='modal' data-target='#myModal' class='btn' value='FR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn' value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='btn' value='LR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='btn' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='btn' value='RR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal' class='btn' value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='LR5' data-toggle='modal' data-target='#myModal'class='btn'  value='LR5' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR6' data-toggle='modal' data-target='#myModal' class='btn' value='LR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR6' data-toggle='modal' data-target='#myModal' class='btn' value='RR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR5' data-toggle='modal' data-target='#myModal' class='btn' value='RR5' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";        
      }
      else if(wheeler=='18'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='btn' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal' class='btn' value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='btn' value='TLR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal'class='btn'  value='TLR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal'value='TRR2' class='btn' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal'  value='TRR1' class='btn' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal'class='btn' value='TLR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal' class='btn' value='TLR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal'value='TRR4' class='btn' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal'class='btn'  value='TRR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal' class='btn' value='TLR5' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='btn' value='TLR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='btn' value='TRR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal' class='btn' value='TRR5' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";        
      }
      else if(wheeler=='22'){
        document.getElementById("axis").innerHTML = 
        "<button id='FL' data-toggle='modal' data-target='#myModal' class='btn' value='FL' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='FR' data-toggle='modal' data-target='#myModal' class='btn' value='FR' onclick='setEventId(this.value);' type='button'></button><br><br><br>"+
        "<button id='LR1' data-toggle='modal' data-target='#myModal'class='btn'  value='LR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR2' data-toggle='modal' data-target='#myModal' class='btn' value='LR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR2' data-toggle='modal' data-target='#myModal' class='btn' value='RR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR1' data-toggle='modal' data-target='#myModal' class='btn' value='RR1' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='LR3' data-toggle='modal' data-target='#myModal' class='btn' value='LR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='LR4' data-toggle='modal' data-target='#myModal' class='btn' value='LR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='RR4' data-toggle='modal' data-target='#myModal' class='btn' value='RR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='RR3' data-toggle='modal' data-target='#myModal'  value='RR3' onclick='setEventId(this.value);' type='button'></button><br><br><br><br>"+
        "<button id='TLR1' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TLR1' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR2' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TLR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR2' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TRR2' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR1' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TRR1' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='TLR3' data-toggle='modal' data-target='#myModal' class='btn'class='btn' value='TLR3' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR4' data-toggle='modal' data-target='#myModal'class='btn'class='btn'   value='TLR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR4' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TRR4' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR3' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TRR3' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='TLR5' data-toggle='modal' data-target='#myModal'class='btn'class='btn'  value='TLR5' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TLR6' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TLR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
        "<button id='TRR6' data-toggle='modal' data-target='#myModal' class='btn'class='btn'  value='TRR6' onclick='setEventId(this.value);' type='button'></button>&nbsp;&nbsp;"+
        "<button id='TRR5' data-toggle='modal' data-target='#myModal'  value='TRR5' onclick='setEventId(this.value);' type='button'></button><br><br>"+
        "<button id='STP' data-toggle='modal' data-target='#myModal' style='background-color: #676f6e ; width:50px; height:20px;' value='STP' onclick='setEventId(this.value);' type='button'></button>";        
       }
      }
      });

    });


		$(function()
		{ 
		  $("#tyre_no").autocomplete({
		  source: 'search_tyre_no_auto.php',
		  change: function (event, ui) {
		  if(!ui.item){
		  $(event.target).val(""); 
		  $(event.target).focus();
		  alert('Tyre Number does not exists');
		  } 
		},
		focus: function (event, ui) { return false; } }); });

		function getTyreNo(elem)
		{
		  var tyre_no = elem;
		  if(tyre_no!='')
		  {
		    $.ajax({
		      type: "POST",
		      url: "search_tyre_no.php",
		      data:'tyre_no='+tyre_no,
		      success: function(data){
		        $("#result3").html(data);
		      }
		    });
		  }
		}


 $(function()
    { 
      $("#rec_tyre_no").autocomplete({
      source: 'autocomplete_tyre_no.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre number does not exist in our stock');
      $("#rec_tyre_no").val('');
      } 
    },
    focus: function (event, ui) { return false; } }); });


 $(function()
    { 
      $("#iss_tyre_no").autocomplete({
      source: 'autocomplete_tyre_no_for_issue.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Tyre number does not exist in our stock');
      $("#iss_tyre_no").val('');
      } 
    },
    focus: function (event, ui) { return false; } }); });

 $("#rec_tyre_no").change(function(){
      var rec_tyre_no=$("#rec_tyre_no").val();
      $.get("fetch_data_on_tyreno.php",{txt:rec_tyre_no},function(data,status){
        var arr = $.parseJSON(data);
          $("#vehicle_no_to_receive").val(arr[0]);
            $("#km_run").val(arr[1]);
             $("#wp_for_recv").val(arr[2]);
              $("#brand1").val(arr[3]);
             $("#ProductSize").val(arr[4]);
            $("#grn_date").val(arr[5]);
      });
    });

$(document).ready(function(){  
   $.datepicker.setDefaults({  
        dateFormat: 'dd-mm-yy'   
   });  
   $(function(){  
        $("#receiving_date").datepicker();
   });
});

$(function()
{ 
  $("#vehicle_no").autocomplete({
  source: 'search_vehicle_no_auto.php',
  change: function (event, ui) {
  if(!ui.item){
  $(event.target).val(""); 
  $(event.target).focus();
  alert('Vehicle Name does not exists');
  } 
},
focus: function (event, ui) { return false; } }); });

</script>