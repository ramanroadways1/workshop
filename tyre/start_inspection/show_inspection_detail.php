<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <?php include("../aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Tyre Inspection</h1>
    </section>

    <section class="content">
     
    <div class="box box-info">
       <div class="box-body">
          <div class="table-responsive"><br>
            
            <table id="employee_data" class="table table-bordered table-striped">
             
              <?php
              $vehicle_no = $_POST['vehicle_no'];
               $sql  =  "select * from tyre_insp_details ";
              $result  = $conn->query($sql);
               if(mysqli_num_rows($result) > 0){
                                    ?>
                                     <thead>
                                      <tr>
                                        <th>Vechile No.</th> 
                                        <th>Total Tyre</th> 
                                        <th>Inspection Time</th>
                                      </tr>
                                    </thead>
                                      <?php
                                    while($row = mysqli_fetch_array($result)){
                                   $vehicle_no = $row['vehicle_no'];
                                    $tyre_no=$row['tyre_no'];
                                    $timestamp=$row['date'];
                              ?>
                       <tr> 
                              <td>
                                <form action="show_inspection_detail.php" method="post"></form>
                                   <input type="submit" class="btn btn-primary btn-sm" name="vehicle_no" value="<?php echo $vehicle_no?>">
                                      <input type="hidden" id="vehicle_no<?php echo $id?>" value="<?php echo $vehicle_no?>">
                              </td>
                              <td><?php echo $tyre_no; ?></td>
                              <td> <?php echo $timestamp; ?></td>

              <?php } } ?>

            </table>
            
           </div>
          </div>
        </div>

      </section>

    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 }); 
</script>