<?php
require("connect.php");
?>
  
<!DOCTYPE html>
<html>
<head>
 <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    <?php include("../aside_main.php"); ?>
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>All Brands</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <script>
          function myFunction() {
            window.print();
          }
          
          function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
          }
        </script>
       
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div id="order_table"> 
                <div class="table-responsive">  
                  <table id="employee_data" class="table table-striped table-bordered" style="font-family:arial; font-size:  13px;" style="width: auto;">  
                    <thead>  
                      <tr class="table-active">
                        <th>Tyre Brand Name</th>  
                        <th>Manufacturer Name</th>  
                        <th>Product Size</th>  
                        <th>Construction Name</th>  
                        <th>Product Nature</th>  
                        <th>Product PlyRating</th>  
                        <th>product Max Nsd</th>  
                        <th>product Min Nsd</th>  
                        <th>Is Rubber</th>  
                      </tr>  
                    </thead>  
                    <?php  
                      $sql = "SELECT * from  tyre_brand";
                      $result = $conn->query($sql);
                      if(mysqli_num_rows($result) > 0){
                        while($row = mysqli_fetch_array($result))  
                        {  
                          $TyreBrandName = $row["TyreBrandName"];
                          $ManufacturerName = $row['ManufacturerName'];
                          $ProductSize = $row["ProductSize"];
                          $ConstructionName = $row['ConstructionName'];
                          $ProductNature = $row["ProductNature"];
                          $ProductPlyRating = $row['ProductPlyRating'];
                          $productMaxNsd = $row["productMaxNsd"];
                          $productMinNsd = $row["productMinNsd"];
                          $IsRubber = $row['IsRubber'];
                        ?>
                          <tr>
                            <td><?php echo $TyreBrandName?></td>
                            <td><?php echo $ManufacturerName?></td>
                            <td><?php echo $ProductSize?></td>
                            <td><?php echo $ConstructionName?></td>
                            <td><?php echo $ProductNature?></td>
                            <td><?php echo $ProductPlyRating?></td>
                            <td><?php echo $productMaxNsd?></td>
                            <td><?php echo $productMinNsd?></td>
                            <td><?php echo $IsRubber?></td>
                          </tr>
                        <?php 
                        } 
                      } else 
                      {
                        echo "no data";
                      }   
                      ?>  
                  </table> 
                </div>  
              </div>  
            </div>
              <!-- /.table-responsive -->
        </div>
         
      </div>
          <!-- /.box -->
    </section>
  </div>
  <?php include("../footer.php") ?>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

<!-- 
 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_external_job.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script> -->

