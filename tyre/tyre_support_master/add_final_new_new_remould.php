<?php
require("connect.php");
include('header.php');

?>
<!DOCTYPE html>
<html>
<head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

           <script>  
                $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'yy-mm-dd'   
                     });  
                     $(function(){  
                          $("#challan_date").datepicker();  
                          
                     });  
                   
                });  

                 $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'yy-mm-dd'   
                     });  
                     $(function(){  
                          $("#invoice_date").datepicker();  
                          
                     });  
                   
                });  
           </script>

               <div id="rate_master222"></div>

                  <script type="text/javascript">
                        
                          function get_data1(val) {
                                $.ajax({
                                  type: "POST",
                                  url: "fetch_data_on_tyreno.php",
                                  data:'tyre_no='+val,
                                  success: function(data){

                                      $("#rate_master222").html(data);
                                  }
                                  });
                              }

                               $(function()
                                { 
                                  $("#tyre_no").autocomplete({
                                  source: 'autocomplete_tyre_no.php',
                                  change: function (event, ui) {
                                  if(!ui.item){
                                  $(event.target).val(""); 
                                  $(event.target).focus();
                                  alert('select Truck No.');
                                  } 
                                },
                                focus: function (event, ui) { return false; } }); });

                          </script>

                    <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var avl_qty = Number($('#avl_qty').val());
                 var trf_qty = $('#trf_qty').val();
                 if(avl_qty=="")
                 {
                   alert("Avalaible Quantity is Null");
                     $('#trf_qty').val('');
                 }else{
                 
                       if(trf_qty>avl_qty)
                       {
                         alert('You can not exceed Avl Qty  Quantity  is '+ avl_qty);
                         $('#trf_qty').val('');
                          $('#amount').val('');
                       }else{
                        var amt = trf_qty*myrate;
                         $('#amount').val(amt);

                       }
                     }

                }
               </script>

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('../aside_main.php'); 
?> 

  <div class="content-wrapper">
    

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
      <h3 class="box-title" style="font-color:black;color: #1585CD;">Tyre GRN</h3>
       
      </div>
        <!-- /.box-header -->
     


        <div class="box-body">
         <!--  <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">    

                       <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-3">  
                             <input type="text" name="from_stock" id="from_stock" class="form-control" placeholder="From Stock" />  
                        </div>  
                        <div class="col-md-3">
                             <input type="text" name="to_stock" id="to_stock" class="form-control" placeholder="To Stock" />  
                        </div>  
                      </div> 
                      </div> 

            </div>  
          </div>
             
        </div> -->
            <!-- /.box-body -->
                  <?php 
                       $branch =  $_SESSION['branch']; 
                       $password =  $_SESSION['password']; 
                       $username =  $_SESSION['username'];
                  ?>
            <form action="insert_final_new_new_remould.php" autocomplete="off" method="POST">
             <div class="row-md-2">
                          <div class="col-md-6"> 
                           <div class="row">
                              <div class="col-md-3"> <label>Your Office:</label></div>
                                  <div class="col-md-3">
                                     <input type="text" readonly="readonly" class="form-control" value="<?php echo $branch ?>" style="width: 338px;" name="office" placeholder="<?php echo $branch ?>" required="required" />
                              </div>
                            </div><br> 

                             <div class="row">
                              <div class="col-md-3"> <label>Vendor:</label></div>
                                  <div class="col-md-3">
                                      <input type="text" name="vendor" id="vendor" class="form-control" style="width: 338px;" placeholder="vendor" required="required" />
                              </div>
                            </div><br>  

                             <!-- <div class="row">
                              <div class="col-md-3"> <label>Vendor Office:</label></div>
                                  <div class="col-md-3">
                                      <input type="text" name="vendor_office" id="vendor_office" class="form-control" style="width: 338px;" placeholder="Vendor Office" required="required" />
                              </div>
                            </div>  -->

                            <div class="row">
                              <div class="col-md-3"> <label>Challan no:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="challan_no" name="" id="challan_no" class="form-control" style="width: 338px;" placeholder="Challan No" required="required" />
                              </div>
                            </div><br> 
                             <div class="row">
                              <div class="col-md-3"> <label>Challan Date:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="challan_date" id="challan_date" class="form-control" style="width: 338px;" placeholder="yy-mm-dd" required="required" />
                              </div>
                            </div><br>

                            <div class="row">
                              <div class="col-md-3"> <label>State:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="state" id="state" class="form-control" style="width: 338px;" placeholder="State" required="required" />
                              </div>
                            </div><br>
                             <div class="row">
                              <div class="col-md-3"> <label>GSTN No:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="gstin_no" id="gstin_no" class="form-control" style="width: 338px;" placeholder="GSTIN no." required="required" />
                              </div>
                        </div>
                            </div> 






                            </div>

                             
                            <div class="col-md-3"> 
                             <script type="text/javascript">
                                function ShowHideDiv(chkPassport) {
                                    var dvinvoice_no = document.getElementById("dvinvoice_no");
                                    dvinvoice_no.style.display = chkPassport.checked ? "block" : "none";
                                    var dvinvoice_date = document.getElementById("dvinvoice_date");
                                    dvinvoice_date.style.display = chkPassport.checked ? "block" : "none";
                                }
                            </script>
                        <label for="chkPassport">
                            <input type="checkbox" id="chkPassport" onclick="ShowHideDiv(this)" />
                            Challan cum Invoice:
                        </label>


                        <div class="row" id="dvinvoice_no" style="display: none">
                              <div class="col-md-3"> <label>Invoice No:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="invoice_no" id="invoice_no" class="form-control" style="width: 338px;" placeholder="Invoice no"  />
                              </div>
                            </div> <br>

                             <div class="row" id="dvinvoice_date" style="display: none">
                              <div class="col-md-3"> <label>Invoice Date:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="invoice_date" id="invoice_date" class="form-control" style="width: 338px;" placeholder="yy-mm-dd"  />
                              </div>
                            </div> <br>

                       
                             <div class="row">
                              <div class="col-md-3"> <label>Remark:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="remark" id="remark" class="form-control" style="width: 338px;" placeholder="Remark" required="required" />
                              </div>
                            </div> 
                          </div> 
                        </div>

                  <input type="hidden" name="username" id="username"  onchange="get_data1(this.value);"  style="width: 250px;" class="form-control" value="<?php echo $username ?>" required="required">

                  <input type="hidden" name="password" id="password" value="<?php echo $password ?>" onchange="get_data1(this.value);"  style="width: 250px;" class="form-control"  required="required">
                   <?php 
                   $sql="SELECT tyre_no FROM temp_new_new_remould_data where username='$username'";
                        if ($result=mysqli_query($conn,$sql))
                          {
                           $tyre_no=mysqli_num_rows($result);
                          }
                       
                    $sql2 =  "select ROUND(SUM(total_GST),0) as total_GST, ROUND(SUM(total_amount),0) as total_amount, ROUND(SUM(discount),0) as discount from  temp_new_new_remould_data WHERE username='$username' ";

                    if ($result1=mysqli_query($conn,$sql2))
                      $row4=mysqli_fetch_array($result1);
                       $total_GST = $row4['total_GST'];
                      $discount = $row4['discount'];
                      $total_amount = $row4['total_amount'];
                   ?>
                  <h3  style="font-color:black;color: #1585CD;">Totals</h3>
                  <div class="row-md-2">
                          <div class="col-md-6"> 
                           <div class="row">
                              <div class="col-md-3"> <label>Total Tyre Count:</label></div>
                                  <div class="col-md-3">
                                     <input type="text" value="<?php echo $tyre_no; ?>" class="form-control" name="total_tyre_count" placeholder="total tyre"  style="width: 338px;"  readonly="readonly"/>
                              </div>
                            </div><br> 
 
                             <div class="row">
                              <div class="col-md-3"> <label>Total GST:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="total_GST_amount" id="total_GST_amount" class="form-control"  style="width: 338px;" placeholder="toal gst" value="<?php echo $total_GST; ?>" readonly="readonly" />
                              </div>
                            </div>
                            </div> 

                            <div class="col-md-3">
                        <div class="row">
                              <div class="col-md-3"> <label>Total Discount:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="total_discount" id="total_discount" class="form-control" style="width: 338px;" placeholder="total discount" value="<?php echo $discount; ?>" readonly="readonly" />
                              </div>
                            </div><br> 
                             <div class="row">
                              <div class="col-md-3"> <label>Total Amount:</label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="final_amount" id="final_amount" class="form-control" style="width: 338px;" placeholder="total amount" value="<?php echo $total_amount ?>" readonly="readonly" />
                              </div>
                            </div>
                          </div> 
                        </div> 
                             
              
                   <br><br>
                   <br><br><br>             
                   <button type="submit" value="Submit"  style="float: right; margin-right: 15px;" class="btn btn-success ml-5" >Submit</button>
              
              </form>


             <br>  
             <form method="post" action="insert_temp_new_new_remould.php" autocomplete="off">
                  <?php 
                       $branch =  $_SESSION['branch']; 
                       $password =  $_SESSION['password']; 
                       $username =  $_SESSION['username'];
                  ?>
                       
              <h3  style="font-color:black;color: #1585CD;">Tyre Details</h3>
              <div class="row-3" style="background-color: #ddd;"><br>
                
                 <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px;">

                   <input type="hidden" name="username" style="width: 250px;" class="form-control" value="<?php echo $username ?>" required="required">

                    <input type="hidden" name="password" style="width: 250px;" class="form-control" value="<?php echo $password ?>" required="required">

                 
                  <tr>
                     <div class="col-md-3">
                     <label>Tyre No</label>
                  <input type="text" name="tyre_no" style="width: 250px;" class="form-control" placeholder="tyre no" required="required">
              
                     <label>Card No:</label>
                    <input type="text" name="card_no" required="required" style="width: 250px;" id="Card No"  class="form-control" placeholder="card no">
                  
                     <label>Production Month:</label>
                    <input type="text" name="product_month"  required="required" style="width: 250px;" id="product_month" placeholder="yy-mm" class="form-control">
                  
                     <label>Brand</label>
                    <input type="text" name="brand" required="required" style="width: 250px;" id="brand" placeholder="brand" class="form-control">
                      </div>
                </tr>

                      <script type="text/javascript">
                          jQuery(document).ready(function () {
                               $("#checkBox").click(function () {
                                  $('#rubber_brand').attr("disabled", $(this).is(":checked"));
                                  $('#rubber_type').attr("disabled", $(this).is(":checked"));
                                  $('#rubber_brand').val('');
                                  $('#rubber_type').val('');
                               });
                            });
                      </script>
                <tr>
                  <div class="col-md-3">
                     <label>Rubber Brand:</label>
                  <input type="text" name="rubber_brand" id="rubber_brand" onchange="get_data1(this.value);"  style="width: 200px;" class="form-control" placeholder="Rubber Brand" required="required">
              
                     <label>Rubber Type:</label>
                    <input type="text" name="rubber_type" required="required" style="width: 200px;" id="rubber_type" placeholder="Rubber Type"  class="form-control">

                   <br>
                    <input type="checkbox" id="checkBox" onclick="enableDisable(this.checked, 'textBox')"><br><br>

                     <label>Remarks:</label>
                    <input type="text" name="remark"  required="required" style="width: 200px;" id="remark"  class="form-control">

                   </div>
                </tr>
              
              
               <tr>
                 <div class="col-md-3">
                     <label>Amount:</label>
                  <input type="Number" name="amount" oninput="sum1()" id="amount"  style="width: 250px;" class="form-control" placeholder="Amount" required="required">
              
                     <label>Discount %:</label>
                    <input type="Number" name="discount" oninput="sum1()"  required="required" style="width: 250px;" id="discount" placeholder="Discount" class="form-control">
                  
                     <label>CGST:</label>
                    <input type="Number" name="CGST" oninput="sum2()" required="required" style="width: 250px;" id="CGST"  class="form-control">
                  
                     <label>SGST:</label>
                    <input type="Number" name="SGST" oninput="sum2()" required="required" style="width: 250px;" id="SGST"  class="form-control">
                   </div>
                </tr>

              <script>
              function sum1()
              {
               var actual_price = Number($('#amount').val());  
               var discount = Number($('#discount').val());  
               var selling_price = actual_price - (actual_price * (discount / 100));
               $('#total_amount').val((selling_price).toFixed(2));
               $('#discount_amt').val((discount).toFixed(2));
             }

               function sum2()
              {
               var CGST = Number($('#CGST').val());  
               var SGST = Number($('#SGST').val());  
               var total_GST = CGST + SGST;
               $('#total_GST').val((total_GST).toFixed(2));
                }
              </script>

                <tr>
                 <div class="col-md-3">
                     <label>IGST:</label>
                  <input type="text" name="IGST" id="IGST" style="width: 250px;" class="form-control" placeholder="0" readonly="readonly">
              
                     <label>Discount Amt:</label>
                    <input type="text" name="discount_amt" placeholder="discount amount" style="width: 250px;" id="discount_amt" readonly="readonly"  class="form-control">
                  
                     <label>Total GST:</label>
                    <input type="text" name="total_GST" placeholder="total GST" style="width: 250px;" id="total_GST" readonly="readonly" class="form-control">
                  
                     <label>Total Amt:</label>
                    <input type="text" name="total_amount" readonly="readonly" required="required" style="width: 250px;" id="total_amount"  class="form-control">
                   </div>
                </tr>
              </table>

              <button type="submit" value="Submit"  style="float: right; margin-right: 15px;" class="btn btn-success ml-5" >Add</button>

               <!-- <input type="submit" name="submit"  style="float: right;" class="btn btn-success ml-5" > -->
               <br><br>
               
                </div>
               </form>
              <br>
               <div style="overflow-x:auto;">
            <table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
                    <tbody>
                       <tr>
                  <th style="display: none;">Id</th>
                  <th>Tyre no</th>
                   <th>Card No</th>
                   <th>Product month </th>
                     <th>Brand</th>
                  <th>Rubber Brand</th>
                  <th>Rubber Type</th>
                  <th>Remark</th>
                  <th>Amount</th>
                  <th>Discount</th>
                  <th>CGST</th>
                  <th>SGST</th>
                  <th>IGST</th>
                  <th>Discount Amt</th>
                  <th>Total Gst</th>
                  <th>Total Amount</th>
                   <th>Remove</th>
                  </tr> </tbody>  
              <?php
              $query2 = "SELECT * FROM temp_new_new_remould_data WHERE username='$username'  ";
              $result11 = mysqli_query($conn, $query2);
              while($row = mysqli_fetch_array($result11)){
                    $id = $row['id'];
                    $tyre_no = $row['tyre_no'];
                    $card_no= $row['card_no']; 
                    $product_month = $row['product_month'];
                    $brand= $row['brand'];
                    $rubber_brand = $row['rubber_brand'];
                    $rubber_type = $row['rubber_type'];
                    $remark = $row['remark'];
                    $amount = $row['amount'];
                    $discount = $row['discount'];
                    $CGST = $row['CGST'];
                    $SGST = $row['SGST'];
                    $IGST = $row['IGST'];
                    $discount_amt = $row['discount_amt'];
                    $total_GST = $row['total_GST'];
                    $total_amount = $row['total_amount'];     
          ?>
              <tr>
                 <input type="hidden" name="id" value="<?php echo $id; ?>">
                <td ><?php echo $tyre_no?></td>
                <td ><?php echo $card_no?> </td>
                <td ><?php echo $product_month?></td>
                <td ><?php echo $brand?></td>
                <td ><?php echo $rubber_brand?></td>
                <td ><?php echo $rubber_type?></td>
                <td ><?php echo $remark?></td>
                <td ><?php echo $amount?></td>
                <td ><?php echo $discount?></td>
                <td ><?php echo $CGST?></td>
                <td ><?php echo $SGST?></td>
                <td ><?php echo $IGST?></td>
                <td ><?php echo $discount_amt?></td>
                <td ><?php echo $total_GST?></td>
                <td ><?php echo $total_amount?></td>
               <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="id" value="X" class="btn btn-danger" />
                </td>
                <!-- <td>
                    <input type="button" onclick="DeleteModal('<?php /*echo*/ $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                </td> --> <script>
                     
                      function DeleteModal(id)
                      {
                       
                        var id = id;
                          var username = '<?php echo $username; ?>'
                            
                        if (confirm("Do you want to delete this data..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_temp_new_new_remould.php",

                                  data: 'id='+id + '&username='+username,
                                  success: function(data){
                                  
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div> 
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="insert_final_remould_stock.php" id="main_form">

               <input type="hidden" name="username" id="username" class="form-control" fo value="<?php echo $username ?>" />  

                <input type="hidden" name="password" id="password" class="form-control" value="<?php echo $password ?>" />  

            
           </form>
              
          </section>
          <div class="col-sm-offset-2 col-sm-8">
     
       
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  

  <?php include("../footer.php") ?>
</div>
</body>
</html>

