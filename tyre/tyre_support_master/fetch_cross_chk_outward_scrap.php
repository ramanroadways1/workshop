<?php
include ("connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_final_outward_for_fetch.php" enctype="multipart/form-data" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">

    <a href='edit_tyre_status_for_scrap.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>&nbsp;Reserved Scrap Tyre</a>
       
      <h1>Send Tyre For Scrap</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <h3 style="display: none;">Basic Details</h3>
            </div>
          </div>

          <?php 
          $valuetosearch =  $_SESSION['temp_scrap_outward'];
           // echo $valuetosearch;
            $sql = "select * from outward_stock where unique_no = '$valuetosearch' and status = 'temp_scrap' group by  unique_no";
            $result = $conn->query($sql);
            while ($row = mysqli_fetch_array($result)) 
            {
              $id = $row['id'];
               $office = $row['office'];
                        @$vendor = $row['vendor'];
                        @$status = $row['status'];
                        @$vendor_office = $row['vendor_office'];
                        @$tyre_no = $row['tyre_no'];
                        @$ProductSize = $row['ProductSize'];
                        @$stock_operation_date = $row['stock_operation_date'];
                        $brnad = $row['BrandName'];
                        ?>
        <div class="row">
            <div class="col-md-6"> 
              <div class="row">
                <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>

                <div class="col-md-4"> <label>Outward Date:</label></div> <?php echo $stock_operation_date; ?>
                 <div class="col-md-8">
                  <input type="hidden" name="stock_operation_date"  id="stock_operation_date<?php echo $id; ?>" class="form-control" style="width: 338px;" value="<?php echo $stock_operation_date; ?>" readonly  required="required" />
                </div>

              <!--   <div class="col-md-4"> <label>Vendor:</label></div> <?php echo $vendor; ?>
                <div class="col-md-8">
                  <input type="hidden" name="vendor" onblur="getParty(this.value);" id="vendor" class="form-control" style="width: 338px;" value="<?php echo $vendor; ?>" readonly placeholder="Vendor" required="required" />

                   <input type="hidden" name="grn_no"   style="width: 338px;" value="<?php echo $valuetosearch; ?>" readonly placeholder="grn_no" required="required" />
                </div> -->
              </div>
             <!--  <div class="row">
                <div class="col-md-4"> <label>Vendor Office:</label></div> <?php echo $vendor_office; ?>
                <div class="col-md-8">
                  <input type="hidden" name="vendor_office" value="<?php echo $vendor_office; ?>" id="vendor_office" class="form-control" style="width: 338px;" placeholder="Vendor Office" readonly />
                </div>
              </div> -->
          </div> 

          <div class="col-md-6"> 
              <div class="row">
                <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>

                 <input  readonly type="hidden" class="form-control" style="width: 120px;"  id="status" name="status" readonly value="<?php echo $status; ?>" >


               
              </div>
          </div> 
        </div>
      <?php } ?>
      </div>
        
               <div class="row"> 
                <div class="col-md-12"> 
                <div class="table-responsive"> 
                    <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px;" >  
                        <thead>
                          <tr>
                             <th>Sno</th>
                              <th>Tyre No</th>
                              <th>Brand</th>
                              <th>Size</th>
                             <!--  <th>Strip Select</th> -->
                               <th>Change State</th>
                               <th>Update</th>
                               <th>Select</th>
                          </tr>
                 </thead>
                    <?php
                      $sql1 = "select * from outward_stock where unique_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $sno = 1;
                        $id_customer = 0;
                      while ($row = mysqli_fetch_array($result1)) 
                          { 
                             @$vendor = $row['vendor'];
                             $status = $row['status'];
                             @$vendor_office = $row['vendor_office'];
                             @$stock_operation_date = $row['stock_operation_date'];
                       
                            $id1 = $row['id'];
                            $tyre_no1 = $row['tyre_no'];
                               $brand1 = $row['BrandName'];
                            $ProductSize = $row['ProductSize'];
                                $Product_Old_Nsd = $row['Product_Old_Nsd'];
                          ?>

              <input id="vendor<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="vendor[]" readonly value="<?php echo $vendor; ?>" >

              <input id="vendor_office<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="vendor_office[]" readonly value="<?php echo $vendor_office; ?>" >

              <input id="stock_operation_date<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="stock_operation_date[]" readonly value="<?php echo $stock_operation_date; ?>" >

              <input  readonly type="hidden" class="form-control" style="width: 120px;"  id="ProductSize<?php echo $id1 ?>" name="ProductSize[]" readonly value="<?php echo $ProductSize; ?>" ></td>
        <tbody >
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id1; ?>" value="<?php echo $id; ?>">

              <input type="hidden" name="productMaxNsd[]" id="Product_Old_Nsd<?php echo $id1; ?>" value="<?php echo $Product_Old_Nsd; ?>">
            </td>
              <td><?php echo $tyre_no1; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no[]" id="tyre_no<?php echo $id1; ?>" value="<?php echo $tyre_no1; ?>" ></td>

              <td><?php echo $brand1; ?><input id="brand<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="brand[]" readonly value="<?php echo $brand1; ?>" ></td>
              
              <td><?php echo $ProductSize; ?>
              <td>
                <select  required="required" id="update<?php echo  $id1; ?>" style="width: 150px;-webkit-appearance: none;height: 28px;" >
                                   <option disabled="disabled" selected="selected">Change State</option>
                                   <option value="temp_remould">Remould</option>
                                   <option value="temp_claim">Claim</option>
                                   <option value="null">Reuse</option>
                </select>
              </td>

                <td>  <input type="button" onclick="Editdata(<?php echo  $id1; ?>)" value="Update"></td>
                <td> <input type="checkbox" name="id_customer[]" value='<?php echo $id_customer; ?>'></td>
                  </tr>
                                        <?php  
                                         $id_customer++;
                                          $sno++; }
                                      ?>
                  </tbody>               
              </table>
            </div>
            <div class="row-md-4">
              <div class="col-md-4">
                 <div class="form-group" >
                        <label id="vendor2" >Vendor:</label>
                       <input type="text"   name="vendor" required="required" onblur="getParty(this.value);" class="form-control" id="vendor" />
                 </div>
              </div>
              <div class="col-md-6">
                    <div class="form-group">
                        <label id="vendor_office2" >Vendor office:</label>
                         <input type="text" readonly="readonly" required="required" name="vendor_office" class="form-control" id="vendor_office" />
                    </div>
              </div>
            </div><br>
            <div id="result"></div>
            <script type="text/javascript">
               $(function()
                          { 
                            $("#vendor").autocomplete({
                            source: 'vendor_name_auto.php',
                            change: function (event, ui) {
                            if(!ui.item){
                            $(event.target).val(""); 
                            $(event.target).focus();
                            alert('Vendor Name does not exists');
                            } 
                          },
                          focus: function (event, ui) { return false; } }); });

                          function getParty(elem)
                              {
                               var vednor_name = elem;
                                if(vednor_name!='')
                                {
                                  $.ajax({
                                    type: "POST",
                                    url: "../tyre_issue/search_party.php",
                                    data:'vendor='+vednor_name,
                                    success: function(data){
                                      $("#result").html(data);
                                    }
                                  });
                                }
                              }

            </script>

            <center> <input type="submit" name="submit" value="Scrap" class="btn btn-sm btn-primary"></center>
            <br>
          </div>
        </div>
</div>
</section>  
</div>
</form>
      
      </div>
      <?php include("../footer.php");?>
    </div>
  
</body>
</html>


<script>
function Editdata(id)
{
     var status = '<?php echo $status ?>';
       var vendor = '<?php echo $vendor ?>';
         var vendor_office = '<?php echo $vendor_office ?>';
          var grn_no = '<?php echo $valuetosearch ?>';
      var tyre_no = document.getElementById('tyre_no'+id).value;
       var brand = document.getElementById('brand'+id).value;
      var stock_operation_date = document.getElementById('stock_operation_date'+id).value;
      var ProductSize = document.getElementById('ProductSize'+id).value;
      var update = document.getElementById('update'+id).value; 
       var office = '<?php echo $usernameSet ?>';
         if (update=="Change State") {
         alert("Select State to update");
      }else{
           jQuery.ajax({
          url: "update_outward_status.php",
          data: {id:id,tyre_no:tyre_no,update:update,grn_no:grn_no,stock_operation_date:stock_operation_date,status:status,office:office,brand:brand,ProductSize:ProductSize,vendor:vendor,vendor_office:vendor_office},
          type: "POST",
          success: function(data) {
            alert(data)
            location.reload(); 
          $("#result_main").html(data);
          },
              error: function() {}
          });
        }
  }
</script>

      


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="src/weekPicker.js"></script>
   



   

