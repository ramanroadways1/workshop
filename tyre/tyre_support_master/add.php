<?php
require("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    <?php include("../aside_main.php"); ?>
    <script>  
    $(document).ready(function(){  
       $.datepicker.setDefaults({  
          dateFormat: 'dd-mm-yy'   
       });  
       $(function(){  
          $("#remould_date").datepicker();
       });
    });  
    </script>
    <script type="text/javascript">
            
    function get_data1(val) {
      $.ajax({
        type: "POST",
        url: "fetch_data_on_tyreno.php",
        data:'tyre_no='+val,
        success: function(data){

            $("#rate_master222").html(data);
        }
      });
    }
     $(function()
      { 
        $("#tyre_no").autocomplete({
        source: 'autocomplete_tyre_no.php',
        change: function (event, ui) {
        if(!ui.item){
        $(event.target).val(""); 
        $(event.target).focus();
        alert('Tyre number does not exist in our stock');
        } 
      },
      focus: function (event, ui) { return false; } }); });

    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Send For Remould</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <form method="post" action="insert_temp_remould.php" autocomplete="off">
            <div class="row">
              <input type="hidden" name="office" id="office" value="<?php echo $usernameSet; ?>" class="form-control" />
              <div class="col-md-4">
                <label><font color="red">*</font>Vendor:</label>
                <input type="text" name="vendor" id="vendor" class="form-control" placeholder="Send To Vendor" required />  
              </div>  
              <div class="col-md-4">
                <label><font color="red">*</font>Remould Date:</label>
                <input type="text" name="remould_date" required="required" id="remould_date" class="form-control" placeholder="Remould Date" required/>  
             </div>
             <div class="col-md-4">
                <label>Remark:</label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Remark" />  
             </div>  
            </div>
              <h3>Tyre Details</h3>
              <div class="row-4">
                 <table class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px; background-color: #EEEDEC ">
                  <tr>
                    <td>
                      <label><font color="red">*</font>Tyre No</label>
                      <input type="text" name="tyre_no" id="tyre_no"  onchange="get_data1(this.value);"  style="width: 250px;" class="form-control"  required="required">
                    </td>v 
                    <td>
                      <label>Vehicle No:</label>
                      <input type="text" name="VehicleNo" readonly="readonly" style="width: 250px;" id="VehicleNo"  class="form-control">
                    </td>
                    <td>
                      <label><font color="red">*</font>Brand Name</label>
                      <input type="text" name="BrandName" readonly="readonly" style="width: 250px;" id="BrandName"  class="form-control">
                    </td>
                    <!-- <td>
                      <label>Manufacture</label>
                      <input type="text" name="Manufacture" readonly="readonly" style="width: 250px;" id="Manufacture"  class="form-control">
                    </td> -->
                    <td>
                      <label ><font color="red">*</font>Production Month:</label>
                      <input type="text" name="MakeYear" readonly="readonly" style="width: 250px;" id="MakeYear"  class="form-control">
                    </td>
                  </tr>
                  <tr>
                    <td colspan='3'></td>
                    <td>
                      <input type="submit" name="submit" style="width: 80px;"  class="btn btn-success ml-5" >
                    </td>
                  </tr>
                  <div id="rate_master222"></div>
                </table>
              </div>
            </form>
            
            <div style="overflow-x:auto;">
              <table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
                <tbody>
                  <tr>
                    <th style="display: none;">Id</th>
                    <th>Office</th>
                    <th>Vendor</th>
                    <th>Remould Date</th>
                    <th>Tyre No</th>
                    <!-- <th>Vehicle No</th> -->
                    <th>Brand Name</th>
                    <th>Make Year</th>
                    <!-- <th>Manufacture</th> -->
                    <th>Remove</th>
                  </tr>
                </tbody>  
              <?php
              $query = "SELECT grn_over_tyre.sl_no, grn_over_tyre.office,vendor , remould_no, remould_date, tyre_no, brand_name, production_month FROM grn_over_tyre inner join main_grn WHERE grn_over_tyre.office='$usernameSet' and grn_over_tyre.type='send_remound' ";
              $result = mysqli_query($conn, $query);
              while($row = mysqli_fetch_array($result)){
                $id = $row['sl_no'];
                $branch = $row['office'];
                $vendor=$row['vendor'];
                $remould_date=$row['remould_date'];
                $tyre_no = $row['tyre_no'];
                /*$VehicleNo = $row['VehicleNo'];*/
                $BrandName = $row['brand_name'];
                $MakeYear = $row['production_month'];
                /*$Manufacture = $row['Manufacture'];*/
                $unique_no = $row['remould_no'];
              ?>
              <tr>
                 <input type="hidden" name="id" value="<?php echo $id; ?>">
                <td ><?php echo $branch?>
                  <input type="hidden" name="branch" value="<?php echo $branch; ?>">
                </td>
              
                <td ><?php echo $vendor?>
                  <input  type="hidden" readonly="readonly" name="vendor[]" value="<?php echo $vendor; ?>">
                </td>
                <td ><?php echo $remould_date?>
                  <input  type="hidden" readonly="readonly" name="remould_date[]" value="<?php echo $remould_date; ?>">
                </td>
                <td ><?php echo $tyre_no?>
                  <input  type="hidden" readonly="readonly" name="tyre_no[]" value="<?php echo $tyre_no; ?>">
                </td>
                <!-- <td ><?php echo $VehicleNo?>
                  <input  type="hidden" readonly="readonly" name="state[]" value="<?php echo $VehicleNo; ?>">
                </td> -->

                 <td ><?php echo $BrandName?>
                  <input  type="hidden" readonly="readonly" name="BrandName[]" value="<?php echo $BrandName; ?>" >
                </td>
                
                <td ><?php echo $MakeYear?>
                  <input  type="hidden" readonly="readonly" name="MakeYear[]" value="<?php echo $MakeYear; ?>">
                </td>
                <!-- <td ><?php echo $Manufacture?>
                  <input  type="hidden" readonly="readonly" name="Manufacture[]" value="<?php echo $Manufacture; ?>">
                </td> -->
                
                 <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="id" value="X" class="btn btn-danger" />
                </td>
                <!-- <td>
                    <input type="button" onclick="DeleteModal('<?php /*echo*/ $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                </td> --> <script>
                     
                      function DeleteModal(id)
                      {
                        var id = id;
                        var username = '<?php echo $usernameSet; ?>'
                            
                        if (confirm("Do you want to delete this data To Add Remould..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_temp_remould_data.php",
                                  data: 'id='+id + '&username='+username,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div> 
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="insert_final_remould_stock.php" id="main_form">
              
               <input  type="hidden" readonly="readonly" name="unique_no" value="<?php echo $unique_no; ?>">

               <input type="hidden" name="branch" id="branch" class="form-control" fo value="<?php echo $branch ?>" />  

             <center>
              <button type="submit" value="Submit" style="width: 130px;"  class="btn btn-primary ml-5" >Add To Remould</button>
             </center>

           </form>
              
          </section>
      
  </div>
  

  <?php include("../footer.php") ?>
</div>
</body>
</html>






















