<div class="sidebar app-aside" id="sidebar">
	<div class="sidebar-container perfect-scrollbar">
		<nav>
			<!-- start: MAIN NAVIGATION MENU -->
			<div class="navbar-title">
				<span>Main Navigation</span>
			</div>
			<ul class="main-navigation-menu">
				<li>
					<a href="javascript:void(0)">
						<div class="item-content">
							<div class="item-media">
								<i class="fa fa-life-ring"></i>
							</div>
							<div class="item-inner">
								<span class="title"> Tyre Support Masters </span><i class="icon-arrow"></i>
							</div>
						</div>
					</a>

					<ul class="sub-menu">
						<li><a href="tier_support_master_index.php">Manufacturer</a></li>
			            <li><a href="tyre_brand.php">Tyre Brand</a></li>
			            <li><a href="product_log.php">Product Log</a></li>
					</ul>
				</li>

				<li>
					<a href="javascript:void(0)">
						<div class="item-content">
							<div class="item-media">
								<i class="fa fa-empire"></i>
							</div>
							<div class="item-inner">
								<span class="title">Tyre Function</span><i class="icon-arrow"></i>
							</div>
						</div>
					</a>

					<ul class="sub-menu">
						<li><a href="tyre_stock.php">Tyre Stock</a></li>
			            <li><a href="tyre_issue.php">Issue/Receive Tyre</a></li>
			            <li><a href="GRNs.php">All Inwards/Outwards</a></li>
			            <li><a href="tyre_inspection.php">Tyre Inspection</a></li>
			            <li><a href="trip_GPS_upload.php">Excel Upload KM Reading</a></li>
					</ul>
				</li>

				<li>
					<a href="javascript:void(0)">
						<div class="item-content">
							<div class="item-media">
								<i class="fa fa-arrow-circle-o-right"></i>
							</div>
							<div class="item-inner">
								<span class="title"> Tyre Inward </span><i class="icon-arrow"></i>
							</div>
						</div>
					</a>

					<ul class="sub-menu">
						<li><a href="new_remould.php">New/New-Remould GRN</a></li>
			            <li><a href="chassis_grn.php">Chassis Tyre GRN</a></li>
			            <li><a href="remould_tyre_grn.php">Remould Tyre GRN</a></li>
			            <li><a href="grn_tyre_claim.php">Against Claim GRN</a></li>
					</ul>
				</li>

				<li>
					<a href="javascript:void(0)">
						<div class="item-content">
							<div class="item-media">
								<i class="fa fa-arrow-circle-o-left"></i>
							</div>
							<div class="item-inner">
								<span class="title"> Tyre Outward </span><i class="icon-arrow"></i>
							</div>
						</div>
					</a>

					<ul class="sub-menu">
						<li><a href="send_to_remould.php">Send to Remould</a></li>
			            <li><a href="send_for_claim.php">Send to Claim</a></li>
			            <li><a href="resale_outward.php">Resale to Buyer</a></li>
			            <li><a href="theft_outward.php">Theft</a></li>
			            <li><a href="scrap_outward.php">Scrap</a></li>
					</ul>
				</li>
			</ul>
			
		</nav>
	</div>
</div>