<style>
li:hover {
  background-color: #2980B9;
}

li{
  font-color: #333;
}
</style>
 
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300">
     <!--  <link rel="stylesheet" href="style.css"> -->
<header class="main-header">
    <a class="logo" style="background-color: #ccc;">
      <span class="logo-mini" ><b>TMS</b></span>
      <span class="logo-lg" style="font-color:black;color: #1A5276; background-color: #ccc;">Tyre Management System</span>
    </a>
    <nav class="navbar navbar-static-top" style="background-color: #ccc;">
      <a href="#" style="font-color:black;color: #333;" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    
      </a>

      <br>

      <?php  if (isset($_SESSION['branch'])&&($_SESSION['username'])&&($_SESSION['password'])) : ?>
     
      <p style="float:right; margin-right:20px;">Welcome <strong><?php echo $_SESSION['branch']; ?>   <?php echo $_SESSION['username']; ?></strong>
      <a href="../logout.php?logout='1'" style="color: red;">logout</a> </p>
    <?php endif ?>
      

    </nav>
  </header>


  <aside class="main-sidebar"  style="background-color: #eee;">
    <section class="sidebar">




      <ul class="sidebar-menu" data-widget="tree">


       <!--  <li  style=" background-color: #2980B9;">
              <a href="add_new_tyre.php"  style="background-color: #f5f5f5;">
                <i class="fa fa-plus" style="color: #2980B9;" class="fa fa-check"></i> 
                <span style="font-color:black;color: #333;">Add New Tyre</span>
              </a>
            </li>  -->

        <li class="treeview" >
          <a href="#" style="background-color: #f5f5f5;">
            <i  class="fa fa-tasks" style="color: #2980B9"></i>
            <span style="font-color:black;color: #333;">Tyre Function</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right" style="color: #2980B9"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: white;">
           <li ><a href="tier_support_master_index.php">Manufacturer</a></li>
                  <li><a href="tyre_brand.php">Tyre Brand</a></li>
                  <li><a href="product_log.php">Product Log</a></li>
          </ul>
        </li>
   
        <li class="treeview">
          <a href="#" style="background-color: #f5f5f5;">
            <i class="fa fa-arrow-circle-o-left" style="color: #2980B9"></i>
            <span style="font-color:black;color: #333;">Tyre Outward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right" style="color: #2980B9"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: white;" >
            <li><a href="available_remould_stock.php"><i class="fa fa-circle-o"></i>Send to Remould</a></li>
            <li><a href="available_claim_stock.php"><i class="fa fa-circle-o"></i>Send to Claim</a></li>
            <li><a href="avalaible_resaleoutward_stock.php"><i class="fa fa-circle-o"></i>Resale to Buyer</a></li>
            <li><a href="avalaible_theft_stock.php"><i class="fa fa-circle-o"></i>Theft</a></li>
            <li><a href="avalaible_scrap_stock.php"><i class="fa fa-circle-o"></i>Scrap</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#" style="background-color: #f5f5f5;">
            <i class="fa fa-arrow-circle-o-left" style="color: #2980B9"></i>
            <span style="font-color:black;color: #333;">Tyre Inward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right" style="color: #2980B9"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: white;" >
            <li><a href="add_final_new_new_remould.php"><i class="fa fa-circle-o"></i>New/New Remould</a></li>
            <li><a href="available_claim_stock.php"><i class="fa fa-circle-o"></i>Chassis GRN</a></li>
            <li><a href="resaleoutward_stock.php"><i class="fa fa-circle-o"></i>Remould Tyre GRNs</a></li>
            <li><a href="theft_outward.php"><i class="fa fa-circle-o"></i>Tyre Against Claim GRN List</a></li>
          </ul>
        </li>
  
      </ul>
 

    </section>
  </aside>
