<?php
include ("connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<?php include("../aside_main.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_final_remould_stock.php" enctype="multipart/form-data" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">

       <a href='edit_tyre_grn.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>&nbsp;Reserved Casing Tyre</a>
       
      <h1>Send Tyre For Casing</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <h3 style="display: none;">Basic Details</h3>
            </div>
          </div>

          <?php 

            @$valuetosearch =  $_SESSION['temp_outward'];
            $sql = "select * from outward_stock where unique_no = '$valuetosearch' and status = 'temp_remould' group by  unique_no";
            $result = $conn->query($sql);
            $row = mysqli_fetch_array($result);
            @$stock_operation_date = $row['stock_operation_date'];
            $office = $row['office'];

          ?>
        <div class="row">
            <div class="col-md-6"> 
              <div class="row">
                <input type="hidden" name="office" id="office" class="form-control" value="<?php echo $usernameSet; ?>"/>

                <div class="col-md-4"> <label>Outward Date:</label></div> <?php echo $stock_operation_date; ?>
                 <div class="col-md-8">
                  <input type="hidden" name="stock_operation_date"  id="stock_operation_date<?php echo $id; ?>" class="form-control" style="width: 338px;" value="<?php echo $stock_operation_date; ?>" readonly  required="required" />
                </div>
              </div>
          </div> 
        </div>
      </div>
        
               <div class="row"> 
                <div class="col-md-12"> 
                <div class="table-responsive"> 
                    <table id="employee_data"  class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  12px;" >  
                        <thead>
                          <tr>
                             <th>Sno</th>
                              <th>Tyre No</th>
                              <th>Brand</th>
                              <th>Size</th>
                              <th>Product Nsd</th>
                              <th>Change State</th>
                               <th>Update</th>
                               <th>Select</th>
                          </tr>
                 </thead>
                    <?php
                      $sql1 = "select * from outward_stock where unique_no = '$valuetosearch' ";
                       $result1 = $conn->query($sql1);
                       $sno = 1;
                        $id_customer = 0;
                      while ($row = mysqli_fetch_array($result1)) 
                          {  $status = $row['status'];
                              $id1 = $row['id'];
                            $tyre_no1 = $row['tyre_no'];
                              $grn_date = $row['grn_date'];
                               $brand1 = $row['BrandName'];
                            $ProductSize = $row['ProductSize'];
                            $Product_Old_Nsd = $row['Product_Old_Nsd'];
                          ?>

        <tbody>
          <tr>
            <td > <?php echo $sno; ?></td>
            <td style="display: none;"> <?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id1; ?>" value="<?php echo $id; ?>"></td>
              <td><?php echo $tyre_no1; ?><input type="hidden" readonly style="width: 120px;" class="form-control" name="tyre_no[]" id="tyre_no<?php echo $id1; ?>" value="<?php echo $tyre_no1; ?>" ></td>

              <td><?php echo $brand1; ?><input id="brand<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="brand[]" readonly value="<?php echo $brand1; ?>" >

                <input id="grn_date<?php echo $id1; ?>" readonly type="hidden" class="form-control" style="width: 120px;" name="grn_date[]" readonly value="<?php echo $grn_date; ?>" >
              </td>
              
              <td><?php echo $ProductSize; ?><input type="hidden" value="<?php echo $ProductSize; ?>" name="ProductSize" id="ProductSize<?php echo $id1 ?>" ></td>
                <td><?php echo $Product_Old_Nsd; ?><input type="hidden" value="<?php echo $Product_Old_Nsd; ?>" name="productMaxNsd[]" id="productMaxNsd<?php echo $id1 ?>" ></td>

              <td>
                <select  required="required" id="update<?php echo  $id1; ?>" style="width: 150px;-webkit-appearance: none;height: 28px;">

                      <option disabled="disabled" selected="selected">Change State</option>
                      <option value="temp_claim">Claim</option>
                      <option value="temp_scrap">Scrap</option>
                      <option value="null">Reuse</option>
                </select>
              </td>

                 <td><input type="button" onclick="edit_data(<?php echo  $id1; ?>)" value="Update"></td>
                <td><input type="checkbox" name="id_customer[]" value='<?php echo $id_customer; ?>'></td>
                </tr>
                    <?php  
                     $id_customer++;
                      $sno++; }
                  ?>
                  </tbody>               
              </table>
            </div>
            <div class="row-md-4">
              <div class="col-md-4">
                 <div class="form-group" >
                    <label id="vendor2">Vendor:</label>
                    <input type="text" name="vendor" required="required" onblur="getParty(this.value);" class="form-control" id="vendor"/>
                 </div>
              </div>
              <div class="col-md-6">
                    <div class="form-group">
                        <label id="vendor_office2" >Vendor office:</label>
                         <input type="text" readonly="readonly" required="required" name="vendor_office" class="form-control" id="vendor_office" />
                    </div>
              </div>
          </div><br>
          <div id="result"></div>
          <script type="text/javascript">
               $(function()
                          { 
                            $("#vendor").autocomplete({
                            source: 'vendor_name_auto.php',
                            change: function (event, ui) {
                            if(!ui.item){
                            $(event.target).val(""); 
                            $(event.target).focus();
                            alert('Vendor Name does not exists');
                            } 
                          },
                          focus: function (event, ui) { return false; } }); });

                          function getParty(elem)
                              {
                               var vednor_name = elem;
                                if(vednor_name!='')
                                {
                                  $.ajax({
                                    type: "POST",
                                    url: "../tyre_issue/search_party.php",
                                    data:'vendor='+vednor_name,
                                    success: function(data){
                                      $("#result").html(data);
                                    }
                                  });
                                }
                              }
            </script>

            <center>
              <input type="submit" name="submit" value="Remould" class="btn btn-sm btn-primary">
            </center>
            <br>
          </div>
        </div>
</div>
</section>  
</div>
</form>
      
      </div>
      <?php include("../footer.php");?>
    </div>
  
</body>
</html>

<script>
function edit_data(id)
{
     id = id;
      var tyre_no = document.getElementById('tyre_no'+id).value; 
       var brand = document.getElementById('brand'+id).value;
        var productMaxNsd = document.getElementById('productMaxNsd'+id).value;
        var ProductSize = document.getElementById('ProductSize'+id).value;
       var type = '<?php echo $status ?>';
       var stock_operation_date =  '<?php echo $stock_operation_date ?>';
       var grn_date = document.getElementById('grn_date'+id).value;
       var office = '<?php echo $usernameSet ?>';
       var grn_no = '<?php echo $valuetosearch ?>';
       var update = document.getElementById('update'+id).value; 
        if (update=="Change State") {
         alert("Select State to update");
      }else{
            jQuery.ajax({
          url: "update_outward_status.php",
          data: {id:id,tyre_no:tyre_no,stock_operation_date:stock_operation_date,type:type,office:office,brand:brand,ProductSize:ProductSize,update:update,grn_date:grn_date,productMaxNsd:productMaxNsd,grn_no:grn_no},
           type: "POST",
          success: function(data) {
            alert(data);
            location.reload(); 
          $("#result_main").html(data);
          },
              error: function() {}
          });
      }    
  }
</script>

      


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="src/weekPicker.js"></script>
   



   

