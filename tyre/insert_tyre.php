<!DOCTYPE html>
<html>

<head>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
include ("connect.php");

// $empcode = $conn->real_escape_string(htmlspecialchars($_POST['empcode']));
// $username = $conn->real_escape_string(htmlspecialchars($_POST['username']));
$SR_NO = $conn->real_escape_string(htmlspecialchars($_POST['SR_NO']));
$RECIVED_DATE = $conn->real_escape_string(htmlspecialchars($_POST['RECIVED_DATE']));
$BRANCH = $conn->real_escape_string(htmlspecialchars($_POST['BRANCH']));
$RECIVED_FROM = $conn->real_escape_string(htmlspecialchars($_POST['RECIVED_FROM']));
$BILLCHALLAN = $conn->real_escape_string(htmlspecialchars($_POST['BILLCHALLAN']));
$TYRE_MAKE = $conn->real_escape_string(htmlspecialchars($_POST['TYRE_MAKE']));
$REMOLD_BELT_MAKE_TYPE = $conn->real_escape_string(htmlspecialchars($_POST['REMOLD_BELT_MAKE_TYPE']));
$TYRE_NO = $conn->real_escape_string(htmlspecialchars($_POST['TYRE_NO']));
$TYRE_SIZE = $conn->real_escape_string(htmlspecialchars($_POST['TYRE_SIZE']));
$TRUCK_NO = $conn->real_escape_string(htmlspecialchars($_POST['TRUCK_NO']));
$FITTING_KM = $conn->real_escape_string(htmlspecialchars($_POST['FITTING_KM']));
$FITTING_DATE = $conn->real_escape_string(htmlspecialchars($_POST['FITTING_DATE']));
$REMOVE_TYRE_NO = $conn->real_escape_string(htmlspecialchars($_POST['REMOVE_TYRE_NO']));
$REMOVE_TYRE_MAKE = $conn->real_escape_string(htmlspecialchars($_POST['REMOVE_TYRE_MAKE']));
$REMOVE_FITTING_KM = $conn->real_escape_string(htmlspecialchars($_POST['REMOVE_FITTING_KM']));
$REMOVE_FITTING_DATE = $conn->real_escape_string(htmlspecialchars($_POST['REMOVE_FITTING_DATE']));
$REMOVE_TYRE_MILEAGE = $conn->real_escape_string(htmlspecialchars($_POST['REMOVE_TYRE_MILEAGE']));
$SCRAPCASINGREUSECLAIM = $conn->real_escape_string(htmlspecialchars($_POST['SCRAPCASINGREUSECLAIM']));
$REMARKS = $conn->real_escape_string(htmlspecialchars($_POST['REMARKS']));


try{

$conn->query("START TRANSACTION"); 


	$sql = "INSERT INTO `tyre_report`(`SR_NO`, `RECIVED_DATE`, `BRANCH`, `RECIVED_FROM`, `BILLCHALLAN`, `TYRE_MAKE`, `REMOLD_BELT_MAKE_TYPE`, `TYRE_NO`, `TYRE_SIZE`, `TRUCK_NO`, `FITTING_KM`, `FITTING_DATE`, `REMOVE_TYRE_NO`, `REMOVE_TYRE_MAKE`, `REMOVE_FITTING_KM`, `REMOVE_FITTING_DATE`, `REMOVE_TYRE_MILEAGE`, `SCRAPCASINGREUSECLAIM`, `REMARKS`) VALUES  ('$SR_NO','$RECIVED_DATE','$BRANCH','$RECIVED_FROM','$BILLCHALLAN', '$TYRE_MAKE','$REMOLD_BELT_MAKE_TYPE','$TYRE_NO','$TYRE_SIZE','$TRUCK_NO','$FITTING_KM','$FITTING_DATE', '$REMOVE_TYRE_NO', '$REMOVE_TYRE_MAKE','$REMOVE_FITTING_KM','$REMOVE_FITTING_DATE','$REMOVE_TYRE_MILEAGE','$SCRAPCASINGREUSECLAIM','$REMARKS')"; 
 // print_r($sql);
 // exit();

	if($conn->query($sql) === FALSE) 
	{ 
		throw new Exception("Code 001 : ".mysqli_error($conn));        
	}



// $file_name= basename(__FILE__);
// $type=1;
// $val="Party Add :"."Party Name-".$party_name.",Party Location-".$party_location;
          
// $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$username','$empcode','$val','$type')";
// if ($conn->query($sql)===FALSE) {
// throw new Exception("Error");  
        
//               } 

 $conn->query("COMMIT");

echo '<script>
swal({
     title: "Records Insert Successfully...",
      text: "Created Tyres Records",
      type: "success"
      }).then(function() {
          window.location = "tryr_add.php";
      });
</script>';
}

catch (Exception $e) {

$conn->query("ROLLBACK");
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

echo '<script>
  swal({
     title: "Records Not Insert...",
     text: "",
     icon: "error",
     button: "Back"
      }).then(function() {
          window.location = "tryr_add.php";
      });
</script>';
$username="pintu";
$empcode="12";
$file_name = basename(__FILE__);        
$sql = "INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
if ($conn->query($sql) === FALSE) { 
echo "Error: " . $sql . "<br>" . $conn->error;
}
}

$conn->close();
?>