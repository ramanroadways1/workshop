
<!DOCTYPE html>
<html>
<head>
  <?php 
  // session_start();
    include("header.php");
    include("aside_main.php");
    // $empcode = $_SESSION['empcode'];

  ?>
  <script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("txtMobId");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = ^(\+[\d]{1,5}|0)?[7-9]\d{9}$;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#txtMobId').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                  }
            }
      } 

    function ValidatePAN() { 
      var Obj = document.getElementById("textPanNo");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                if (ObjVal.search(panPat) == -1) {
                    alert("Invalid Pan No... Please Enter In This Format('ABCDE1234F')");
                    $('#textPanNo').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Pan No.");
                }
            }
      } 
    </script>

  <script type="text/javascript">

     function validateEmail(sEmail) {
     var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

        if(!sEmail.match(reEmail)) {
          alert("Invalid email address");
          document.getElementById('email').value = "";
        }

        return true;

      }

  </script>

    <script>
      function ValidateGSTIN() { 
      var Obj = document.getElementById("textGSTINNo");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var gstinPat = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
                if (ObjVal.search(gstinPat) == -1) {
                    alert("Invalid GSTIN No... Please Enter In This Format('12ABCDE1234F1ZQ')");
                    $('#textGSTINNo').val('');
                    $('#gst_type').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct GSTIN No.");
                  }
            }
      }
      function ValidateIFSC() { 
      var Obj = document.getElementById("textIFSC");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var ifscPat = /^[A-Za-z]{4}0[A-Z0-9a-sz]{6}$/;
                if (ObjVal.search(ifscPat) == -1) {
                    alert("Invalid IFSC No... Please Enter In This Format('ABCD0123456')");
                    $('#textIFSC').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct IFSC No.");
                  }
            }
      }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_tyre.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Masters</h1>
    </section> -->
    <section class="content">
     
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add New</h3>

         <!--  <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">

                <label>SR NO<font color="red">*</font></label>
                <input type="Number" style="width: 100%;"  class="form-control"  id="SR_NO" name="SR_NO" placeholder="SR NO" required/>
              </div>

              <div class="form-group">
                <label for="RECIVED_DATE">Recived Date<font color="red">*</font></label>
                <input type="date" style="width: 100%;" class="form-control" name="RECIVED_DATE" id="RECIVED_DATE" placeholder="Recived Date" required/>
              </div>
              <div class="form-group">
                 <label for="BRANCH">BRANCH<font color="red">*</font></label>
                 <input type="text" style="width: 100%;"  class="form-control" name="BRANCH" id="BRANCH"  placeholder="BRANCH" required/>
              </div>
              <div class="form-group">
                 <label for="RECIVED_FROM">RECIVED FROMS<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" id="RECIVED_FROM" name="RECIVED_FROM" placeholder="RECIVED FROM" required/>
              </div>
              
              <div class="form-group">
                 <label for="BILLCHALLAN">BILL/CHALLAN NO<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" id="BILLCHALLAN"  class="form-control" name="BILLCHALLAN" placeholder="BILL CHALLAN" required/>
              </div>


              <div class="form-group">
                 <label for="TYRE_MAKE">TYRE MAKE</label>
                 <input type="text" style="width: 100%;" id="TYRE_MAKE" class="form-control" name="TYRE_MAKE" placeholder="TYRE MAKE"  required/>
              </div>

              <div class="form-group">
                 <label for="REMOLD_BELT_MAKE_TYPE">REMOLD BELT MAKE TYPE</label>
                 <input type="text" style="width: 100%;" id="REMOLD_BELT_MAKE_TYPE" class="form-control" name="REMOLD_BELT_MAKE_TYPE" placeholder="REMOLD BELT MAKE TYPE"  required/>
              </div>

              <div class="form-group">
                 <label for="TYRE_NO">TYRE NO</label>
                 <input type="text" style="width: 100%;" id="TYRE_NO" class="form-control" name="TYRE_NO" placeholder="TYRE MAKE"  required/>
              </div>  

              <div class="form-group">
                 <label for="TYRE_SIZE">TYRE SIZE</label>
                 <input type="text" style="width: 100%;" id="TYRE_SIZE" class="form-control" name="TYRE_SIZE" placeholder="TYRE MAKE"  required/>
              </div>  



            </div>
            <!-- /.col -->
            <div class="col-md-6">

                <div class="form-group">

                <label>TRUCK NO<font color="red">*</font></label>
                <input type="text" style="width: 100%;"  class="form-control"  id="TRUCK_NO" name="TRUCK_NO" placeholder="TRUCK NO" required/>
              </div>

              <div class="form-group">
                <label for="FITTING_KM">FITTING KM<font color="red">*</font></label>
                <input type="text" style="width: 100%;" class="form-control" name="FITTING_KM" id="FITTING_KM" placeholder="FITTING KM" required/>
              </div>
              <div class="form-group">
                 <label for="FITTING DATE">FITTING_DATE<font color="red">*</font></label>
                 <input type="date" style="width: 100%;"  class="form-control" name="FITTING_DATE" id="FITTING_DATE"  placeholder="FITTING DATE" required/>
              </div>
              <div class="form-group">
                 <label for="REMOVE_TYRE_NO">REMOVE TYRE NO<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" id="REMOVE_TYRE_NO" name="REMOVE_TYRE_NO" placeholder="REMOVE TYRE NO" required/>
              </div>
              
              <div class="form-group">
                 <label for="REMOVE_TYRE_MAKE">REMOVE TYRE MAKE<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" id="REMOVE_TYRE_MAKE"  class="form-control" name="REMOVE_TYRE_MAKE" placeholder="REMOVE TYRE MAKE" required/>
              </div>


              <div class="form-group">
                 <label for="REMOVE_FITTING_KM">REMOVE FITTING KM</label>
                 <input type="text" style="width: 100%;" id="REMOVE_FITTING_KM" class="form-control" name="REMOVE_FITTING_KM" placeholder="REMOVE FITTING KM"  required/>
              </div>

              <div class="form-group">
                 <label for="REMOVE_FITTING_DATE">REMOVE FITTING DATE</label>
                 <input type="date" style="width: 100%;" id="REMOVE_FITTING_DATE" class="form-control" name="REMOVE_FITTING_DATE" placeholder="REMOVE FITTING DATE"  required/>
              </div>

              <div class="form-group">
                 <label for="REMOVE_TYRE_MILEAGE">REMOVE TYRE MILEAGE</label>
                 <input type="text" style="width: 100%;" id="REMOVE_TYRE_MILEAGE" class="form-control" name="REMOVE_TYRE_MILEAGE" placeholder="REMOVE TYRE MILEAG "  required/>
              </div>  

              <div class="form-group">
                 <label for="SCRAPCASINGREUSECLAIM">SCRAP\CASING\REUSECLAIM</label>
                 <input type="text" style="width: 100%;" id="SCRAPCASINGREUSECLAIM" class="form-control" name="SCRAPCASINGREUSECLAIM" placeholder="SCRAP"  required/>
              </div>  
              <div class="form-group">
                 <label for="REMARKS">Remarks</label>
                 <input type="text" style="width: 100%;" id="REMARKS" class="form-control" name="REMARKS" placeholder="REMARKS"  required/>
              </div> 

            </div>
 
          </div>

        </div>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
      <a href="master_view.php" class="btn btn-warning">Show All</a>
     <!--  <a href="show_party.php" class="btn btn-warning">Show Party</a> -->
     </center>
    </div>
        
  <br><br><br>
  </div>
</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


