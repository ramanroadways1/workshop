<?php 
  require_once("connect.php");
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <?php include("aside_main.php");?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off" action="update_final_outward.php">
  <div class="content-wrapper">
    <section class="content-header">
       <a href='battery_serviceadd.php'  style="float: right;margin-right: 1%;" type="submit" name="add" id="add" class="btn btn-xs btn-warning"><i class="fa fa-back"></i>&nbsp;Back</a>
      <h1>Repairing Battery</h1>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-body">
          <div class="table-responsive"><br>
            <table id="order_data" class="table table-bordered table-striped">
              <?php 
              $usernameSet=$_SESSION['username'];
              // print_r($usernameSet)

              ?>
            <?php 
            // $tyre_no = $_POST['tyre_no'];
             $sql = "select * from final_outward_battery where battry_status='REPAIRING_IN' and receive_or_not='0' ";
             $result = $conn->query($sql);
             $id_customer = 0;
             if ($result->num_rows > 0){
              ?>
              <thead>
                <tr>
<!--                     <th style="display: none;">Send</th>
                    <th  style="display: none;">Update</th>  -->
                    <!--<th>Send</th> -->
<!--                     <th>Update</th> 
                    <th>Change Status</th>  -->
                    <th>Battry No.</th>
                    <th>Repairing  Date</th> 
                    <th>Model</th> 
                    <th>Volt</th>   
                    <th style="display: none;">State</th> 
                    <th>Select</th> 
                </tr>
              </thead>
              <?php

             while ($row = mysqli_fetch_array($result)) {
                  
                  $id = $row['id'];
                  $battery_no = $row['battery_no'];
                  $battry_status = $row['battry_status'];
                  $volt = $row['volt'];
                  $model = $row['model'];

                ?>

              <tr>
               

            <input type="hidden" name="battry_status" id="battry_status"  class="form-control" value="<?php echo $battry_status; ?>"/>
          <input type="hidden" name="office" id="office"  class="form-control" value="<?php echo $usernameSet; ?>"/>
           <td style="display: none;"> <input type="text" readonly="readonly" name="id[]" value="<?php echo $id; ?>"></td>
          <td> <?php echo $battery_no; ?> <input type="hidden" readonly="readonly" name="battery_no[]" id="battery_no"  value="<?php echo $battery_no; ?>">
           </td>
            <td></td>
            <td><?php echo $model; ?> <input type="hidden" readonly="readonly" name="model[]" id="model" value="<?php echo $model; ?>">
              <input type="hidden" readonly="readonly" name="grn_date[]" id="grn_date" value="<?php echo $grn_date; ?>">
            </td>
            <td><?php echo $volt; ?> <input type="hidden" readonly="readonly" name="volt[]" id="volt" value="<?php echo $volt; ?>">
            </td>
           <td style="display: none;"><input type="text" readonly="readonly" name="battry_status" id="battry_status" value="<?php echo $battry_status; ?>">
           </td>
           <td><input type="checkbox" name="id_customer[]"value="<?php echo $id_customer; ?>"></td>
           </tr>
                <?php
                 $id_customer++;  }
               ?>
            </table>
          </div>
            <div class="row-md-4">
              <div class="col-md-4">
                 <div class="form-group" >
                    <label id="vendor2">Vendor:</label>
                    <input type="text"name="vendor" required="required" onblur="getParty(this.value);" class="form-control" id="vendor" />
                 </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label id="vendor_office2">Vendor office:</label>
                  <input type="text" readonly="readonly" required="required" name="vendor_office" class="form-control" id="vendor_office" />
                 </div>
              </div>
            </div><br>

          <?php } else{
            echo "No Data";
            exit();
          }  ?>
            <div id="result"></div>

            <script type="text/javascript">

               $(function()
                          { 
                            $("#vendor").autocomplete({
                            source: 'vendor_name_auto.php',
                            change: function (event, ui) {
                            if(!ui.item){
                            $(event.target).val(""); 
                            $(event.target).focus();
                            alert('Vendor Name does not exists');
                            } 
                          },
                          focus: function (event, ui) { return false; } }); });

                          function getParty(elem)
                              {
                               var vednor_name = elem;
                                if(vednor_name!='')
                                {
                                  $.ajax({
                                    type: "POST",
                                    url: "tyre_issue/search_party.php",
                                    data:'vendor='+vednor_name,
                                    success: function(data){
                                      $("#result").html(data);
                                    }
                                  });
                                }
                              }

                    </script>

        <center><input type="submit" name="submit" value="Remould" class="btn btn-sm btn-primary"></center>
        </div>
      </div>
    </section>
  </div>
</form>
<?php include("footer.php");?>
</div>
</body>
</html>

<!-- <script>
function edit_data(id)
{
     id = id;
      var tyre_no = document.getElementById('tyre_no'+id).value; 
       var brand = document.getElementById('brand'+id).value;
          var productMaxNsd = document.getElementById('productMaxNsd'+id).value;
       var ProductSize = document.getElementById('ProductSize'+id).value;
       var type = '<?php echo $status ?>';
        var stock_operation_date = document.getElementById('stock_operation_date'+id).value; 
       var ProductSize = document.getElementById('ProductSize'+id).value;

       var grn_date = document.getElementById('grn_date'+id).value;
       var office = '<?php echo $usernameSet ?>';

       var update = document.getElementById('update'+id).value; 
        if (update=="Change State") {
         alert("Select State to update");
      }else{
            jQuery.ajax({
          url: "update_reserved_outward_status.php",
          data: {id:id,tyre_no:tyre_no,stock_operation_date:stock_operation_date,type:type,office:office,brand:brand,ProductSize:ProductSize,update:update,grn_date:grn_date,productMaxNsd:productMaxNsd},
           type: "POST",
          success: function(data) {
          
            location.reload(); 
          $("#result_main").html(data);
          },
              error: function() {}
          });
      }     
  }
</script> -->

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="src/weekPicker.js"></script>
