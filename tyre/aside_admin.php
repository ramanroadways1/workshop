<?php

  $test1=basename(dirname(__DIR__)); 

  $test2= basename(dirname(__FILE__)); 
  $server_path='/'.$test1.'/'.$test2.'/';

 ?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>TMS</b></span>
      <span class="logo-lg">ADMIN (TMS)</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
        <p align="right" style="margin-right: 20px; margin-top: 13px;">Welcome <strong><?php echo $admin; ?></strong>
        <a href="<?php echo $server_path; ?>logout.php" style="color: red;">logout</a> </p>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-empire"></i>
            <span>Tyre Function</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path; ?>admin/manufacturer/add_manufacturer.php"><i class="fa fa-circle-o"></i>Add manufacturer</a></li>
            <li><a href="<?php echo $server_path; ?>admin/manufacturer/add_tyre_brand.php"><i class="fa fa-circle-o"></i>Add tyre brand</a></li>
         </ul>
        </li>
        
        <li>
          <a href="<?php echo $server_path; ?>admin/target_psi/set_target_psi.php"><i class="fa fa-dashboard"></i> <span>Set Target Psi</span></a>
        </li>
      </ul>
    </section>
  </aside>
