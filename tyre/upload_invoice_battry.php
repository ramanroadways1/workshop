<?php 
require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include("header.php"); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php include("aside_main.php"); ?>

<script>
    $( function() {
      $( "#invoice_date" ).datepicker();
    } );
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
    <section class="content-header">
       <a href='battry_master.php' style="float: right;" type="submit" name="add" id="add" class="btn btn-xs btn-warning">&nbsp;Back</a>
      <h1>Upload Invoice Over Challan For New Battry</h1>
    </section>

    <section class="content">
      <div class="box box-info">

         <form id="temp_form" action="add_invoice.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  
                  <?php    
                      
                      $username =  $_SESSION['username'];
                      // echo $username;
                        $query = "SELECT battery_no, model, vendor_name,challan_no, challan_file FROM battery_master WHERE username='$username' AND length(challan_file) > 15 and length(invoice_file)< 15";
                        // echo $query;

                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){

                                    ?>
                                    <thead>  
                                       <tr>  
                                              <th style="display: none;" scope="row">Id</th>
                                                <th>Battry Number</th>
                                                <th style="width: 100px;">Party Name</th>
                                                 <th>Model</th>
                                                <th> Challan No.</th>
                                                  
                                               <th style="width: 190px;">Challan File</th>
                                         </tr>  
                                  </thead>  
                                     <?php

                                    while($row = mysqli_fetch_array($result)){
                                    
                                        $battery_no = $row['battery_no'];
                                        $model=$row['model'];
                                        $vendor_name=$row['vendor_name'];
                                        // $vendor=$row['vendor'];
                                        $challan_no = $row['challan_no'];
                                        // $challan_date = $row['challan_date'];
                                        $challan_file = $row['challan_file'];
                                        $challan_file1=explode(",",$challan_file);
                                        $count3=count($challan_file1);
                                  ?>
                       <tr> 
                            <td style="display: none;"><?php echo $battery_no; ?><input type="hidden" name="battery_no" id="id<?php echo $battery_no; ?>" value='<?php echo $battery_no; ?>'></td>

                                 <td>
                                   <input type="submit" class="btn btn-primary btn-sm" name="battery_no" form="temp_form"  value="<?php echo $battery_no?>" >
                                      <input type="hidden" id="battery_no <?php echo $battery_no?>" value="<?php echo $battery_no?>">
                                    </td>
                                    <td><?php echo $vendor_name; ?></td>
                                     <td><?php echo $model; ?></td>
                                    <td><?php echo $challan_no; ?></td>
                                  <!--   <td><?php echo $date; ?></td> -->
                                     <td>
                                       Challan No: <?php echo $challan_no; echo "<br>"; ?>
                                     <!--    Challan Date: <?php echo $challan_date; echo "<br>"; ?> -->
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]"  value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                      ?>
                                     </td>
                       </tr>

                     <?php   
                     
                      $id_customer++;
                      $i++;

                    }
                  } else {
                       echo "No Data";
                  }

                  ?>  
                </table>  
                      

                 <br>
              </div>  
            </div>  
          </div>
            
          </div>
        
    </div>
    </section>
  <br><br><br>
</div>

<?php include("../footer.php");?>
</div>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){  
 $.datepicker.setDefaults({  
    dateFormat: 'dd-mm-yy'   
 });  
 $(function(){  
    $("#invoice_date").datepicker();
 });
});

</script>
