<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0 beta
    </div>
    <strong>Copyright <?php echo date("Y"); ?> <a>Tyre Management System.</a></strong>
  </footer>

  <div class="control-sidebar-bg"></div>