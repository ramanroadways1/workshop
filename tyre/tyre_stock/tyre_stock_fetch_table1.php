<table id="employee_data" class="table table-bordered table-striped">
<?php
include("connect.php");
$tyre_status=$_POST['tyre_status'];
if($tyre_status=='0')
{
?>
<tr>
  <th>TYRE NO.</th>
  <th>STATUS</th>
  <th>VEHICLE</th>
  <th>WHEEL</th>
  <th>OFFICE</th>
  <th>BRAND</th>
  <th>MAKE YR</th>
</tr>
<?php 
$query = "SELECT * FROM grn_over_tyre";
$result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)  
  {
    $i=0;
    while($row = mysqli_fetch_array($result))  
    {
      $tyre_no = $row["tyre_no"];
      $status =  $row["type"];
      $vehicleno =  $row["vehicle_no"];
      $wheelposition = $row["wp"];
      $office =  $row["office"];
      $brandname = $row["brand_name"];
      $makeyear =  $row["production_month"];
      ?>
    <tr>
       <td><?php echo $tyre_no;?><input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no?>"></td>
       <td><?php echo $status;?><input type="hidden" name="status[]" value="<?php echo $status?>"></td>
       <td><?php echo $vehicleno;?><input type="hidden" name="vehicleno[]" value="<?php echo $vehicleno?>"></td>
       <td><?php echo $wheelposition;?><input type="hidden" name="wheelposition[]" value="<?php echo $wheelposition?>"></td>
       <td><?php echo $office;?><input type="hidden" name="office[]" value="<?php echo $office?>"></td>
      <!--<td><?php echo $documentdate;?><input type="hidden" name="documentdate[]" value="<?php echo $documentdate?>"></td>-->
       <td><?php echo $brandname;?><input type="hidden" name="brandname[]" value="<?php echo $brandname?>"></td>
       <td><?php echo $makeyear;?><input type="hidden" name="makeyear[]" value="<?php echo $makeyear?>"></td>
   </tr>

<?php
    $i++;
    }
  }
}
else if($tyre_status=='1')
{
  ?>
  <tr>
    <th>SELECT</th>
    <th>TYRE NO.</th>
    <th>STATUS</th>
    <th>VEHICLE</th>
    <th>WHEEL</th>
    <th>OFFICE</th>
    <!--<th>DOCUMENT DATE</th>-->
    <th>BRAND</th>
    <th>MAKE YR</th>
  </tr>
  <?php
$query = "SELECT * FROM grn_over_tyre where type='New'";
$result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)  
  {
    $i=0;
    while($row = mysqli_fetch_array($result))  
    {
      $tyre_no = $row["tyre_no"];
      $status =  $row["type"];
      $vehicleno =  $row["vehicle_no"];
      $wheelposition = $row["wp"];
      $office =  $row["office"];
      $brandname = $row["brand_name"];
      $makeyear =  $row["production_month"];
      ?>
    <tr>
      <td>
        <input type="checkbox" onclick="select1(<?php echo $srno; ?>)" name="srno" value="srno" />
      </td>
      <td><?php echo $tyre_no;?><input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no?>"></td>
      <td><?php echo $status;?><input type="hidden" name="status[]" value="<?php echo $status?>"></td>
      <td><?php echo $vehicleno;?><input type="hidden" name="vehicleno[]" value="<?php echo $vehicleno?>"></td>
      <td><?php echo $wheelposition;?><input type="hidden" name="wheelposition[]" value="<?php echo $wheelposition?>"></td>
      <td><?php echo $office;?><input type="hidden" name="office[]" value="<?php echo $office?>"></td>
      <!-- <td><?php echo $documentdate;?><input type="hidden" name="documentdate[]" value="<?php echo $documentdate?>"></td> -->
      <td><?php echo $brandname;?><input type="hidden" name="brandname[]" value="<?php echo $brandname?>"></td>
      <td><?php echo $makeyear;?><input type="hidden" name="makeyear[]" value="<?php echo $makeyear?>"></td>
   </tr>

<?php
    $i++;
    }
  }
}
else if($tyre_status=='2')
{
  ?>
  <tr>
    <th>SELECT</th>
    <th>TYRE NO.</th>
    <th>STATUS</th>
    <th>VEHICLE</th>
    <th>WHEEL</th>
    <th>OFFICE</th>
    <!--<th>DOCUMENT DATE</th>-->
    <th>BRAND</th>
    <th>MAKE YR</th>
  </tr>
  <?php
$query = "SELECT * FROM grn_over_tyre where status='on_vehicle'";
// print_r($query);
// echo $query;
$result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)  
  {
    $i=0;
    while($row = mysqli_fetch_array($result))  
    {
      $tyre_no = $row["tyre_no"];
      $status =  $row["type"];
      $vehicleno =  $row["vehicle_no"];
      $wheelposition = $row["wp"];
      $office =  $row["office"];
      $brandname = $row["brand_name"];
      $makeyear =  $row["production_month"];
      ?>
    <tr>
      <td>
        <input type="checkbox" onclick="select1(<?php echo $srno; ?>)" name="srno" value="srno" />
      </td>
      <td><?php echo $tyre_no;?><input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no?>"></td>
      <td><?php echo $status;?><input type="hidden" name="status[]" value="<?php echo $status?>"></td>
      <td><?php echo $vehicleno;?><input type="hidden" name="vehicleno[]" value="<?php echo $vehicleno?>"></td>
      <td><?php echo $wheelposition;?><input type="hidden" name="wheelposition[]" value="<?php echo $wheelposition?>"></td>
      <td><?php echo $office;?><input type="hidden" name="office[]" value="<?php echo $office?>"></td>
      <!--<td><?php echo $documentdate;?><input type="hidden" name="documentdate[]" value="<?php echo $documentdate?>"></td> -->
      <td><?php echo $brandname;?><input type="hidden" name="brandname[]" value="<?php echo $brandname?>"></td>
      <td><?php echo $makeyear;?><input type="hidden" name="makeyear[]" value="<?php echo $makeyear?>"></td>
   </tr>

<?php
    $i++;
    }
  }
}
else if($tyre_status=='3')
{
  ?>
  <tr>
    <th>SELECT</th>
    <th>TYRE NO.</th>
    <th>STATUS</th>
    <th>VEHICLE</th>
    <th>WHEEL</th>
    <th>OFFICE</th>
    <!-- <th>DOCUMENT DATE</th> -->
    <th>BRAND</th>
    <th>MAKE YR</th>
  </tr>
  <?php
$query = "SELECT * FROM grn_over_tyre where type='off vehicle'";
$result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)  
  {
    $i=0;
    while($row = mysqli_fetch_array($result))  
    {
      $tyre_no = $row["tyre_no"];
      $status =  $row["type"];
      $vehicleno =  $row["vehicle_no"];
      $wheelposition = $row["wp"];
      $office =  $row["office"];
      $brandname = $row["brand_name"];
      $makeyear =  $row["production_month"];
      ?>
    <tr>
      <td>
        <input type="checkbox" onclick="select1(<?php echo $srno; ?>)" name="srno" value="srno" />
      </td>
      <td><?php echo $tyre_no;?><input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no?>"></td>
      <td><?php echo $status;?><input type="hidden" name="status[]" value="<?php echo $status?>"></td>
      <td><?php echo $vehicleno;?><input type="hidden" name="vehicleno[]" value="<?php echo $vehicleno?>"></td>
      <td><?php echo $wheelposition;?><input type="hidden" name="wheelposition[]" value="<?php echo $wheelposition?>"></td>
      <td><?php echo $office;?><input type="hidden" name="office[]" value="<?php echo $office?>"></td>
      <!-- <td><?php echo $documentdate;?><input type="hidden" name="documentdate[]" value="<?php echo $documentdate?>"></td> -->
      <td><?php echo $brandname;?><input type="hidden" name="brandname[]" value="<?php echo $brandname?>"></td>
      <td><?php echo $makeyear;?><input type="hidden" name="makeyear[]" value="<?php echo $makeyear?>"></td>
   </tr>

<?php
    $i++;
    }
  }
}
else if($tyre_status=='4' || $tyre_status=='5' || $tyre_status=='6' || $tyre_status=='7' || $tyre_status=='8' || $tyre_status=='9' || $tyre_status=='10' || $tyre_status=='11' || $tyre_status=='12')
{
  ?>
  <tr>
    <th>SELECT</th>
    <th>TYRE NO.</th>
    <th>STATUS</th>
    <th>VEHICLE</th>
    <th>WHEEL</th>
    <th>OFFICE</th>
    <!-- <th>DOCUMENT DATE</th> -->
    <th>BRAND</th>
    <th>MAKE YR</th>
  </tr>
  <?php
$query = "SELECT * FROM grn_over_tyre where type='scrapped' or type='off vehicle'";
$result = mysqli_query($conn, $query);
  if(mysqli_num_rows($result) > 0)  
  {
    $i=0;
    while($row = mysqli_fetch_array($result))  
    {
      $tyre_no = $row["tyre_no"];
      $status =  $row["type"];
      $vehicleno =  $row["vehicle_no"];
      $wheelposition = $row["wp"];
      $office =  $row["office"];
      $brandname = $row["brand_name"];
      $makeyear =  $row["production_month"];
      ?>
    <tr>
      <td>
        <input type="checkbox" onclick="select1(<?php echo $srno; ?>)" name="srno" value="srno" />
      </td>
      <td><?php echo $tyre_no;?><input type="hidden" name="tyre_no[]" value="<?php echo $tyre_no?>"></td>
      <td><?php echo $status;?><input type="hidden" name="status[]" value="<?php echo $status?>"></td>
      <td><?php echo $vehicleno;?><input type="hidden" name="vehicleno[]" value="<?php echo $vehicleno?>"></td>
      <td><?php echo $wheelposition;?><input type="hidden" name="wheelposition[]" value="<?php echo $wheelposition?>"></td>
      <td><?php echo $office;?><input type="hidden" name="office[]" value="<?php echo $office?>"></td>
      <!--<td><?php echo $documentdate;?><input type="hidden" name="documentdate[]" value="<?php echo $documentdate?>"></td>-->
      <td><?php echo $brandname;?><input type="hidden" name="brandname[]" value="<?php echo $brandname?>"></td>
      <td><?php echo $makeyear;?><input type="hidden" name="makeyear[]" value="<?php echo $makeyear?>"></td>
      
   </tr>

<?php
    $i++;
    }
  }
}
else
{
 echo 'Data Not Found';
}

?>
</table>
