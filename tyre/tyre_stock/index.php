<?php 
  require_once("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <?php include("../aside_main.php");
     ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Tyre Stock</h1>
    </section>
  <section class="content">
    <div class="box box-info">
       <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-sm-6">
                  <label>select Tyre Status:</label>
                  <select onchange="SelectStatus(this.value)" class="form-control" id="tyre_status" name="tyre_status">
                    
                          <option hidden value="undefined">---Select Tyre Status---</option>
                          <option value="0">Get All Status</option>
                          <option value="1">New</option>
                          <option value="2">On Vehicle</option>
                          <option value="3">Off Vehicle</option>
                          <option value="4">Sent for Remould/Refurbish</option>
                          <option value="5">Sent for Claim</option>
                          <option value="6">Remould/Refurbish Received</option>
                          <option value="7">Claim Received</option>
                          <option value="8">Remould/Refurbish Reject</option>
                          <option value="9">Claim Reject</option>
                          <option value="10">Scrapped</option>
                          <option value="11">Missing</option>
                          <option value="12">Resale</option>

                  </select>
                </div>
                <div id="new" style="display: none;">
                  <div class="col-sm-4">
                    <label>Action:</label>
                    <select onchange="SearchByNew(this.value)" class="form-control" id="action1" name="action1">

                       <option hidden value="undefined">---Select Action---</option>
                       <option required hidden id="13" value="13">Issue/Receive</option>
                       <option required hidden id="14" value="14">Send to Claim</option>
                       <option required hidden id="15" value="15">Send to Remould</option>
                       <option required hidden id="16" value="16">Scrap</option>
                       <option required hidden id="17" value="17">Remould Tyre</option>
                       <option required hidden id="18" value="18">Tyre Against Claim</option>

                    </select>
                  </div>
                  <div class="col-sm-2"><br>
                    <button class="btn btn-info text-color" id="process_btn" disabled>Process</button>
                  </div>
                </div>
            </div>  
          </div>
          <div class="table-responsive"><br>
            <div id="result0"></div> 
          </div>
          </div>
        </div>
      </section>
    </div>
  </form>
<?php include("../footer.php");?>
</div>
</body>
</html>
<script>  
  $(document).ready(function(){  
      $('#employee_data').DataTable();  
  });  
</script>

<script>
  function SelectStatus(elem)
  {
    document.getElementById("process_btn").disabled = true;
    $("#13").hide();
    $("#14").hide();
    $("#15").hide();
    $("#16").hide();
    $("#17").hide();
    $("#18").hide();
    if (elem == '0' || elem == '9' || elem == '10' || elem == '11' || elem == '12' )
    {
      document.getElementById("process_btn").disabled = true;
      $("#new").hide();
      $("#13").hide();
      $("#14").hide();
      $("#15").hide();
      $("#16").hide();
      $("#17").hide();
      $("#18").hide();
    }
    else if(elem == '1')
    {
      $("#action1").val('');
      $("#new").show();
      $("#13").show();
      $("#14").show();
    }
    else if(elem == '2')
    {
      $("#action1").val('');
      $("#new").show();
      $("#13").show();
    }
    else if(elem == '3')
    {
      $("#action1").val('');
      $("#new").show();
      $("#13").show();
      $("#14").show();
      $("#15").show();
      $("#16").show();
    }
    else if(elem == '4')
    {
      $("#action1").val('');
      $("#new").show();
      $("#17").show();
    }
    else if(elem == '5')
    {
      $("#action1").val('');
      $("#new").show();
      $("#18").show();
    }
    else if(elem == '6')
    {
      $("#action1").val('');
      $("#new").show();
      $("#13").show();
    }
    else if(elem == '7')
    {
      $("#action1").val('');
      $("#new").show();
      $("#13").show();
    }
    else if(elem == '8')
    {
      $("#action1").val('');
      $("#new").show();
      $("#16").show();
    }
    else{
      document.getElementById("process_btn").disabled = true;
      $("#action1").val('');
      $("#new").hide();
    }
    $.ajax({
        type: "POST",
        url: "tyre_stock_fetch_table1.php",
        data:'tyre_status='+elem,
        success: function(data){
          /*alert("okkk");*/
            $("#result0").html(data);
        }
      });
  }
</script>

<script>
  function SearchByNew(elem)
  { 
    document.getElementById("process_btn").disabled = false;
   
      if(elem=='13')
      {
        $(document).ready(function(){
        $("#process_btn").click(function(e){
          /*alert(elem);
          window.location.href='<?php echo $server_path ; ?>tyre_issue/add.php';
          alert("<?php echo $server_path ; ?>tyre_issue/add.php");*/
          e.preventDefault();  //add this line to prevent reload
          $.ajax({
                 type:"post",
                 url:"<?php echo $server_path ; ?>tyre_issue/add.php",
                 data:"product="+elem,
                    success:function(data){
                      alert(data);
                      $("#result").html(data);
                      alert('Product Inserted Successfully.');
                    }
                 });
             });
          });
      }
      else if(elem=='14')
      {
        alert(elem);
       /* $.ajax({
          type: "POST",
          url: "send_for_claim.php",
          data:'send_for_claim='+elem,
          success: function(data){
            $("#result1").html(data);
          }
        });*/
      }
  }
  </script> 
  <div id="result1"></div>


