<?php
$table = 'tyre_report';
 
$primaryKey = 'id';

 $columns = array(
     
      // array( 'db' => 'id', 'dt' => 0 ),

      // array( 'db' => 'SR_NO',  'dt' => 1 ),

      array( 'db' => 'RECIVED_DATE', 'dt' => 0,
          'formatter' => function( $d, $row ) {
           return date( 'd-m-Y', strtotime($d));
       }),

       array( 'db' => 'BRANCH', 'dt' => 1 ),

     array( 'db' => 'RECIVED_FROM', 'dt' => 2 ),

    array( 'db' => 'BILLCHALLAN', 'dt' => 3 ),

     array( 'db' => 'TYRE_MAKE', 'dt' => 4 ),

       array( 'db' => 'REMOLD_BELT_MAKE_TYPE', 'dt' => 5 ),

      array( 'db' => 'TYRE_NO', 'dt' => 6 ),

      array( 'db' => 'TYRE_SIZE', 'dt' => 7 ),

    array( 'db' => 'TRUCK_NO', 'dt' => 8 ),

      array( 'db' => 'FITTING_KM', 'dt' => 9 ),


    array( 'db' => 'FITTING_DATE','dt' => 10,
        'formatter' => function( $d, $row ) {
            return date( 'd-m-Y', strtotime($d));
        }
    ),
    array( 'db' => 'REMOVE_TYRE_NO', 'dt' => 11 ),
    array( 'db' => 'REMOVE_TYRE_MAKE', 'dt' => 12 ),

    array( 'db' => 'REMOVE_FITTING_KM', 'dt' => 13 ),

    array( 'db' => 'REMOVE_FITTING_DATE', 'dt' => 14,
      'formatter' => function( $d, $row ) {
            return date( 'd-m-Y', strtotime($d));
        }
         ),

    array( 'db' => 'REMOVE_TYRE_MILEAGE', 'dt' => 15 ),

    array( 'db' => 'SCRAPCASINGREUSECLAIM', 'dt' => 16 ),

    array( 'db' => 'REMARKS', 'dt' => 17 )
   
);

 
$sql_details = array(
    'user' => 'root',
    'pass' => '',
    'db'   => 'tyre_mngt',
    'host' => 'localhost'
);
 
 
include("scripts/ssp.class.php");
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
?>