<?php
    include('connect.php');
    include('header.php');
    include('aside_main.php');
?>
<!DOCTYPE html>
<html>
<head>
      <link href="../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../assets/jquery-3.5.1.js"></script>
      <script src="../assets/jquery-ui.min.js"></script>
      <link href="../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../assets/custom.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="inser_modelmaster.php" autocomplete="off">
  <div class="content-wrapper">

    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add Model</h3>
 
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="col-md-5">
              <div class="form-group">
                <label>Model Number<font color="red">*</font></label>
                <input type="text" style="width: 100%;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" onblur="MyFunc()" id="model" name="model" placeholder="model" required/>
              </div>
              </div>
              <div id="result22"></div>
              <script>
              function MyFunc()
              {
                var model = $('#model').val();
                if(model!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chkPartyname.php",
                      data:'model='+model ,
                      success: function(data){
                          $("#result22").html(data);
                      }
                      });
                }
              }     
            </script>
               <div class="col-md-5">
               <div class="form-group">
                      <label>Number Of Battry</label>
                      <select   class="form-control"  id="nobattery" name="nobattery">
                      <option value="">---Select---</option>
                      <option value="1">one</option>
                      <option value="2">Two</option>
                      </select>
              </div>
            </div>

             </div>
<!--     
            <div class="col-md-6">
              <div class="col-md-5">
              <div class="form-group">
                <label for="product_no">Date<font color="red">*</font></label>
                <input type="date" style="width: 100%;" class="form-control" id="date1" name="date1"  required/>
              </div>
            </div>
            </div> -->

          </div>
        </div>
      </div>
              <div class="col-sm-offset-2 col-sm-8" style="margin-bottom: 60px;">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
<!--       <a href="party_view.php" class="btn btn-warning">Show Party</a>
 -->    
     </center>
    </div>
    </section>
    <!-- ----------------------------------Add Table -->
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>

                                
                          <td>Model Number</td>
                          <td>Number Of Battry</td>
                         <!--  <td>Date</td> -->
                          
          
                    
                            </tr>
                         </thead>
                         <tfoot class="thead-light">
                            <tr>
                               <th style=" ">  </th>
                               <th style=" ">  </th>
                           
 

                            </tr>
                         </tfoot>
                      </table>


                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "model_fetchbattry.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV'
                             
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                              "criteria": [
                                  {
                                      "data": ' VEHICLE ',
                                      "condition": '=',
                                      "value": ['']
                                  }
                              ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          // $('title').html(" RRPL");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "txn_home_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>

                  </div>
                </section>
              </div>




  <div class="control-sidebar-bg"></div>
</div>

        <script src="../assets/bootstrap.js"></script>

        <script src="../assets/bootstrap.bundle.js"></script>
        <script src="../assets/scripts.js"></script>
        <script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>


</body>
</html>



<!-- --------------------------------------------Close Table------ -->


  <br><br><br>

  </div>
</form>


<!--   <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer> -->
<?php include ('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


