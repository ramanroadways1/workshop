<!DOCTYPE html>
<html>

<head>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
session_start();
$empcode = $_SESSION['empcode'];
$username= $_SESSION['username'];
include ("connect.php");

// $empcode = $conn->real_escape_string(htmlspecialchars($_POST['empcode']));
// $username = $conn->real_escape_string(htmlspecialchars($_POST['username']));
$yes_no = $conn->real_escape_string(htmlspecialchars($_POST['yes_no']));
$party_name = $conn->real_escape_string(htmlspecialchars($_POST['party_name']));

$state = $conn->real_escape_string(htmlspecialchars($_POST['state']));
$party_location = $conn->real_escape_string(htmlspecialchars($_POST['party_location']));
$mobile_no = $conn->real_escape_string(htmlspecialchars($_POST['mobile_no']));
$address = $conn->real_escape_string(htmlspecialchars($_POST['address']));
$contact_person = $conn->real_escape_string(htmlspecialchars($_POST['contact_person']));
$pan = $conn->real_escape_string(htmlspecialchars($_POST['pan']));
$gstin = $conn->real_escape_string(htmlspecialchars($_POST['gstin']));
$gst_type = $conn->real_escape_string(htmlspecialchars($_POST['gst_type']));
$bank = $conn->real_escape_string(htmlspecialchars($_POST['bank']));
$acc_no = $conn->real_escape_string(htmlspecialchars($_POST['acc_no']));
$acc_holder_name = $conn->real_escape_string(htmlspecialchars($_POST['acc_holder_name']));
$ifsc_code = $conn->real_escape_string(htmlspecialchars($_POST['ifsc_code']));
$email = $conn->real_escape_string(htmlspecialchars($_POST['email']));
$branch = $conn->real_escape_string(htmlspecialchars($_POST['branch']));
try{

$conn->query("START TRANSACTION"); 

// $username = $_POST['username'];
// $yes_no = $_POST['yes_no'];
// $party_name = $_POST['party_name'];
// $party_location= $_POST['party_location'];
// $mobile_no = $_POST['mobile_no'];
// $address = $_POST['address'];
// $contact_person = $_POST['contact_person'];
// $pan = $_POST['pan'];
// $gstin = $_POST['gstin'];
// $gst_type = $_POST['gst_type'];
// $bank = $_POST['bank'];
// $acc_no = $_POST['acc_no'];
// $acc_holder_name = $_POST['acc_holder_name'];
// $ifsc_code = $_POST['ifsc_code'];
// $email = $_POST['email'];
// $branch= $_POST['branch'];

// $datevar= date('Y');
// $party_code1 =  "WPVIS".$datevar;
//     $query= "SELECT party_code FROM party where party_code like '$party_code1%' order by id desc limit 1";
//     echo $query;
// $result = mysqli_query($conn, $query);
// if(mysqli_num_rows($result) > 0)  
//   {
//     $row = mysqli_fetch_array($result);
//     $party_code = $row["party_code"];

//     $last_digits = substr($party_code, -4);

//     $next = $last_digits + 1;

//     $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
//     $p_c = $party_code1.$next1;

// }
// else
// { 
//   $p_c =  $party_code1."0001";

// }
$datevar= date('Y');
$party_code1 =  "WPVIS".$datevar;


$sql = "SELECT party_code FROM vendor where party_code like '$party_code1%' order by id desc limit 1"; 
if($conn->query($sql) === FALSE) 
{ 
throw new Exception("Code 002 : ".mysqli_error($conn));        
}
$result = mysqli_query($conn, $sql);

if(mysqli_num_rows($result) > 0)  
{
$row = mysqli_fetch_array($result);
$party_code = $row["party_code"];

$last_digits = substr($party_code, -4);

$next = $last_digits + 1;

$next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
$p_c = $party_code1.$next1;

}
else
{ 
$p_c =  $party_code1."0002";

}
$sql = "insert into vendor(party_name,state,party_location,mobile_no,address,contact_person,  pan, yes_no, gstin,gst_type, bank,branch, acc_no, acc_holder_name, ifsc_code,party_code,email) value ('$party_name','$state','$party_location','$mobile_no','$address','$contact_person', '$pan','$yes_no','$gstin','$gst_type','$bank','$branch','$acc_no', '$acc_holder_name', '$ifsc_code','$p_c','$email')"; 
 // print_r($sql);
 // exit();
if($conn->query($sql) === FALSE) 
{ 
throw new Exception("Code 001 : ".mysqli_error($conn));        
}



$file_name= basename(__FILE__);
$type=1;
$val="Party Add :"."Party Name-".$party_name.",Party Location-".$party_location;
$username="Test";
$empcode="123";        
$sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$username','$empcode','$val','$type')";
if ($conn->query($sql)===FALSE) {
throw new Exception("Error");  
        
              }  
 $conn->query("COMMIT");

echo '<script>
swal({
     title: "Party Add Successfully...",
      text: "Created Party",
      type: "success"
      }).then(function() {
          window.location = "party.php";
      });
</script>';
}

catch (Exception $e) {

$conn->query("ROLLBACK");
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

echo '<script>
  swal({
     title: "Party Not Insert...",
     text: "",
     icon: "error",
     button: "Back"
      }).then(function() {
          window.location = "party.php";
      });
</script>';

$file_name = basename(__FILE__);        
$sql = "INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
if ($conn->query($sql) === FALSE) { 
echo "Error: " . $sql . "<br>" . $conn->error;
}
}

$conn->close();
?>