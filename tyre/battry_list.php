<?php  
include('header.php');
include('aside_main.php');
include('connect.php');

?>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.1.0/css/searchBuilder.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.1.0/css/dataTables.dateTime.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.1.0/js/dataTables.searchBuilder.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/datetime/1.1.0/js/dataTables.dateTime.min.js"></script> 
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_report.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>
<div class="wrapper">
 <form method="post" action="insert_party.php" autocomplete="off">
  <div class="content-wrapper">
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
          <div class="row" style="height: 10px; padding: 10px 5px 10px 5px">
    <form>

<div class="row">  
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="box box-default  with-border">
<div class="box-header with-border" style="text-align:center">     
<h4 class="box-title small-title pull-left">Search Criteria </h4>         

 <div class="box-tools pull-right">                    
<button type="button" class="btn btn-box-tool btn-default  btn-flat" data-widget="collapse">
<i class="fa fa-minus"></i></button>
</div>
</div>
<div class="box-body"  >
<table width="100%" border="0" cellpadding="0" cellspacing="0"  align="center" > 
        <div class="col-md-12"style=" margin-top: 20px;">
           <div class="form-group row">
             <div class="col-md-2" >
            
              </div>
              <div class="col-md-2">  
                   <input type="text" name="from_date" id="from_date" autocomplete="off"  class="form-control" placeholder="From Date"/> 
                    <i style="margin-right: 10px;" class="fa fa-calendar form-control-feedback"></i> 
              </div> 

              <div class="col-md-2">  
                   <input type="text"  name="to_date" id="to_date" autocomplete="off" class="form-control" placeholder="To Date" />  <i style="margin-right: 10px;" class="fa fa-calendar form-control-feedback"></i> 
              </div>
                 <div class="col-md-2" >
              </div>

             <div class="col-md-4">
                <label class="a">Battery Model</label>
                <select style=" width: 250px;" class="form-control" required id="search_product" name="search_product">
                <option value="">---Select---</option>
                <option value=" AMROL">AMROL</option>
                <option value="CAPRO CPR01">CAPRO CPR01</option>
                <option value="DYNEX 100L">DYNEX 100L</option>
                </select>
              </div>
            </div>
        </div>
         


         <div class="col-md-12">
            <div class="form-group row">
            <div class="col-md-2" >
            </div>
            <div class="col-md-2" >
             <label class="a">Battery No.</label>
            <input  class="form-control" style="height: 25px; width: 450px;" id="ex1" type="text">
            </div>
                        <div class="col-md-2" >
            </div>
                        <div class="col-md-2" >
            </div>
                         <div class="col-md-4">
                <label class="a">Status</label>
                <select style=" width: 250px;" class="form-control" required id="search_product" name="search_product">
                <option value="">---Select---</option>
                <option value="Issue">Issue</option>
                <option value="Recive">Recive</option>
                <option value="Replace">Replace</option>
                </select>
              </div>


                        <div class="col-md-2" >

            </div>
            <div class="col-md-2" >

            </div>


            </div>
         </div>
  
         <div class="col-md-12">
          <div class="col-md-2">
         </div>

    </div>
    <div class="col-sm-offset-2 col-sm-8" style="margin-top: 30px;">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Search</button>
      <a href="party_view.php" class="btn btn-warning">Close</a>
    
     </center>
    </div>
</table>
<?php
   $connection = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );
   $RECIVED_FROM = '';
   $statement = $connection->prepare("SELECT RECIVED_FROM FROM tyre_report ORDER BY RECIVED_FROM ASC"); 
   $statement->execute();
   $result = $statement->fetchAll();
   $count = $statement->rowCount();
   $data = array();
   
   foreach($result as $row)
    {
      $RECIVED_FROM .= '<option value="'.$row['RECIVED_FROM'].'">'.$row['RECIVED_FROM'].'</option>';
  }

  ?>
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">

    <table id="contact-detail"  name="contact-detail" class="display nowrap" cellspacing="0" width="100%">
    <thead>
      <tr>

        <th>SrNo.</th>
        <th>Battery SrNo.</th>
        <th>Battery No. </th>
        <th>Machine Category </th>
        <th>Model</th>
        <th>Battery Type </th>
        <th>Volt </th>
        <th>Watt</th>
        <th>Amp. </th>
        <th>No. of Plates </th>
        <th>Full Charge time</th>
        <th>Run After Full Charge </th>
        <th>Warranty </th>
        <th>Status </th>
        <th>Vendor Name</th>
        <th>Invoice No. </th>
        <th>Date Of Purchase </th>
        <th>Amount (Cost) </th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tfoot>
       <tr>
        <th>SrNo.</th>
        <th>Battery SrNo.</th>
        <th>Battery No. </th>
        <th>Machine Category </th>
        <th>Model</th>
        <th>Battery Type </th>
        <th>Volt </th>
        <th>Watt</th>
        <th>Amp. </th>
        <th>No. of Plates </th>
        <th>Full Charge time</th>
        <th>Run After Full Charge </th>
        <th>Warranty </th>
        <th>Status </th>
        <th>Vendor Name</th>
        <th>Invoice No. </th>
        <th>Date Of Purchase </th>
        <th>Amount (Cost) </th>
     </tr>
    </tfoot>
    </table>
    </div>

    <script type="text/javascript">
              $(document).ready(function() {
            // Setup - add a text input to each footer cell
            $('#contact-detail tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
         
            // DataTable
            var table = $('#contact-detail').DataTable({
                initComplete: function () {
                    // Apply the search
                    this.api().columns().every( function () {
                        var that = this;
         
                        $( 'input', this.footer() ).on( 'keyup change clear', function () {
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        } );
                    } );    
                },        
                "processing": true,
                "scrollX": true,
            "serverSide": true,
            "ajax": "pintu_fetch.php"
            });
         
        } );

    </script>

        </div>
    </div>
  </section>
</div></div></div></div>


</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</section>
</div>
</form>
</div>

</body>





