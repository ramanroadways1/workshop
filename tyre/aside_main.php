<?php

  $test1=basename(dirname(__DIR__)); 

  $test2= basename(dirname(__FILE__)); 
  $server_path='/'.$test1.'/'.$test2.'/';

 ?>
<?php 
  
    session_start(); 

    if (!isset($_SESSION['userid'])) {
      $_SESSION['msg'] = "You must log in first";
      header('location: login.php');
    }
    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['username']);
      unset($_SESSION['userid']);
      header("location: login.php");
    }

?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>TMS</b></span>
      <span class="logo-lg">Tyre Management System</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
       <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success">
        <h3>
          <?php
            echo $_SESSION['success']; 
            unset($_SESSION['success']);
          ?>
        </h3>
      </div>
    <?php endif ?>
    <!-- logged in user information and Show Informatision index file -->
    <?php  if (isset($_SESSION['username'])) : ?>

      <p style="float:right; margin-right:20px;">

<strong>
    <?php 
     
        echo $_SESSION['username'];
        echo "/";
        echo $_SESSION['employee'];


    ?>
</strong>

      <a href="../index.php" style="color: #FFFF00;">Back(Vendor)</a> </p><br>
    <?php endif ?>
    </nav>
  </header>

  <aside class="main-sidebar" >

    <section class="sidebar">

      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="<?php echo $server_path ;?>index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>

      <!--   --------------------- Add New-->
              <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i>
            <span>Battery Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo $server_path ;?>party.php"><i class="fa fa-circle-o"></i>Add Party</a></li>
            <li><a href="<?php echo $server_path ;?>battry_master.php"><i class="fa fa-circle-o"></i>Add Battery</a></li>
            <li><a href="<?php echo $server_path ;?>model_add.php"><i class="fa fa-circle-o"></i>Add Model</a></li>
          </ul>
        </li>
       <!--  ----------------- -->  

        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo $server_path ;?>party.php"><i class="fa fa-circle-o"></i>Add Party</a></li>
 
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i>
            <span>Tyre Add(New/Remould)</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>new_remould/add_new_new_remould.php"><i class="fa fa-circle-o"></i>New Tyre Purchase</a></li>
            <li><a href="<?php echo $server_path ;?>new_remould/remould_tyre_purchase.php"><i class="fa fa-circle-o"></i>Remould Tyre Purchase</a></li>
            <li><a href="<?php echo $server_path ;?>chassis_grn/add_new_chassis_grn.php"><i class="fa fa-circle-o"></i>New Chassis Purchase</a></li>
            <li><a href="<?php echo $server_path ;?>chassis_grn/add_old_chassis_grn.php"><i class="fa fa-circle-o"></i>Old Chassis Purchase</a></li>
            <li><a href="<?php echo $server_path ;?>second_hand_tyre/add_second_hand_tyre.php"><i class="fa fa-circle-o"></i>Second Hand Tyre Purchase</a></li>
          </ul>
        </li>
        <li class="treeview" >
          <a href="#">
            <i  class="fa fa-tasks" ></i>
            <span>Tyre Support Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" >
        <!--     <li><a href="<?php echo $server_path ;?>tyre_support_master/tier_support_master_index.php"><i class="fa fa-circle-o"></i>Manufacturer</a></li> -->
            <li><a href="<?php echo $server_path ;?>tyre_support_master/tyre_brand.php"><i class="fa fa-circle-o"></i>Tyre Brand</a></li>
           <!--  <li><a href="tyre_support_master/product_log.php"><i class="fa fa-circle-o"></i>Product Log</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-empire"></i>
            <span>Tyre Function</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>tyre_stock/"><i class="fa fa-circle-o"></i>Tyre Stock</a></li>
            <li><a href="<?php echo $server_path ;?>tyre_issue/add.php"><i class="fa fa-circle-o"></i>Issue/Receive Tyre</a></li>
          <!--   <li><a href="GRNs/"><i class="fa fa-circle-o"></i>All Inwards/Outwards</a></li> -->
          <!-- Code Are Commntes On Those ARE Diffrient -->
             <!--  <li><a href="<?php echo $server_path ;?>chassis_grn/"><i class="fa fa-circle-o"></i>Chassis Tyre GRN</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Tyre Inward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>remould_tyre_grn/receive_remould_grn.php"><i class="fa fa-circle-o"></i>Casing Tyre GRN</a></li>
            <li><a href="<?php echo $server_path ;?>remould_tyre_grn/receive_claim_grn.php"><i class="fa fa-circle-o"></i>Against Claim GRN</a></li>
            <li><a href="<?php echo $server_path ;?>remould_tyre_grn/receive_scrap_grn.php"><i class="fa fa-circle-o"></i>Against Scrap GRN</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-left"></i>
            <span>Tyre Outward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>tyre_support_master/add_remould.php"><i class="fa fa-circle-o"></i>Send to Casing(Remould)</a></li>
            <li><a href="<?php echo $server_path ;?>tyre_support_master/add_claim.php"><i class="fa fa-circle-o"></i>Send to Claim</a></li>
            <li><a href="<?php echo $server_path ;?>tyre_support_master/add_scrap.php"><i class="fa fa-circle-o"></i>Send to Scrap</a></li> 
             <li><a href="<?php echo $server_path ;?>tyre_support_master/add_theft.php"><i class="fa fa-circle-o"></i>Send to Theft</a></li>
          </ul>
        </li>
<!--         <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-left"></i>
            <span>Tyre Inspection</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>tyre_inspection/"><i class="fa fa-circle-o"></i> Inspection Record</a></li>
            <li><a href="<?php echo $server_path ;?>start_inspection/add.php"><i class="fa fa-circle-o"></i>Start inspection</a></li>
          </ul>
        </li> -->
        <li>
          <a href="<?php echo $server_path ;?>moniter_tyre/moniter_stock.php"><i class="fa fa-dashboard"></i> <span>Moniter Tyre</span></a>
        </li>
   <!--------------------------Add New File(Paper Work)----------------------------->
   
        <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Paper Work(Excel)</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>tryr_add.php"><i class="fa fa-circle-o"></i>Add Tyre</a></li>
            <li><a href="<?php echo $server_path ;?>file_upload.php"><i class="fa fa-circle-o"></i>Bulk Uploding</a></li>
            <li><a href="<?php echo $server_path ;?>Truck_report.php"><i class="fa fa-circle-o"></i>Truck No.Report</a></li>
            <li><a href="<?php echo $server_path ;?>Tyreno_report.php"><i class="fa fa-circle-o"></i>Tyre No.Report</a></li>
            <li><a href="<?php echo $server_path ;?>master_view.php"><i class="fa fa-circle-o"></i>All Excel Data</a></li>
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Battery Issue</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>battry_issue.php"><i class="fa fa-circle-o"></i>Battery Issue</a></li>
            <li><a href="<?php echo $server_path ;?>battry_list.php"><i class="fa fa-circle-o"></i>Battery List</a></li>
             <li><a href="<?php echo $server_path ;?>battry_list.php"><i class="fa fa-circle-o"></i>Battery List</a></li>
              <li><a href="<?php echo $server_path ;?>repo_battery.php"><i class="fa fa-circle-o"></i>Battery Reports</a></li>
              <li><a href="<?php echo $server_path ;?>truck_battery.php"><i class="fa fa-circle-o"></i>Truck Battery Reports</a></li>
          <!--
            <li><a href="file_upload.php"><i class="fa fa-circle-o"></i>Bulk Uploding</a></li>
            <li><a href="Truck_report.php"><i class="fa fa-circle-o"></i>Truck No.Report</a></li>
            <li><a href="Tyreno_report.php"><i class="fa fa-circle-o"></i>Tyre No.Report</a></li>
            <li><a href="master_view.php"><i class="fa fa-circle-o"></i>All Excel Data</a></li> 
            -->
          </ul>
        </li>
                  <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Battery Inward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>service_battery.php"><i class="fa fa-circle-o"></i>Battery Service</a></li>
<!--             <li><a href="<?php echo $server_path ;?>battery_serviceadd.php"><i class="fa fa-circle-o"></i>Battery List</a></li>
             <li><a href="<?php echo $server_path ;?>battry_list.php"><i class="fa fa-circle-o"></i>Battery List</a></li>
              <li><a href="<?php echo $server_path ;?>repo_battery.php"><i class="fa fa-circle-o"></i>Battery Reports</a></li>
              <li><a href="<?php echo $server_path ;?>truck_battery.php"><i class="fa fa-circle-o"></i>Truck Battery Reports</a></li> -->
   
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Battery outward</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>battery_serviceadd.php"><i class="fa fa-circle-o"></i>Battery Send Repairing</a></li>
            <li><a href="<?php echo $server_path ;?>store_battery.php"><i class="fa fa-circle-o"></i>Store In Battery</a></li>
             <li><a href="<?php echo $server_path ;?>scrap_battery.php"><i class="fa fa-circle-o"></i>Scrap In Battery</a></li>
              <li><a href="<?php echo $server_path ;?>service.php"><i class="fa fa-circle-o"></i>Service In Battery</a></li>
   
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-arrow-circle-o-right"></i>
            <span>Tyre Status</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $server_path ;?>Tyre_record.php"><i class="fa fa-circle-o"></i>Tyre Numbur</a></li>
            <li><a href="<?php echo $server_path ;?>battry_list.php"><i class="fa fa-circle-o"></i>Battery List</a></li>
          </ul>
        </li>
<!--
    <li class="treeview">
            <a href="#">
              <i class="fa fa-arrow-circle-o-right"></i>
              <span>Master Data</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="master_view.php"><i class="fa fa-circle-o"></i>All Data</a></li>
            </ul>
      </li>-->

<!--------------------------End File--------------------------------->
      </ul>
    </section>
  </aside>
