<?php
$table = 'battery_master';
 
$primaryKey = 'id';

 $columns = array(


  // `srn`, `battery_no`, `volt`, `watt`, `charze_time`, `run_aftercharz`, `battry_type`, `model`, `amp`, `no_plates`, `warranty_yers`, `battry_status`, `invoice_no`, `pur_date`, `vendor_name`, `amount`, `invoice_uplod`, `challan_upload`, `challan_no`, `username`, `employee`, `date`
     
      array( 'db' => 'id', 'dt' => 0 ),

      // array( 'db' => 'SR_NO',  'dt' => 1 ),

      array( 'db' => 'srn', 'dt' => 1),
       //    'formatter' => function( $d, $row ) {
       //     return date( 'd-m-Y', strtotime($d));
       // }),

       array( 'db' => 'battery_no', 'dt' => 2 ),

       array( 'db' => 'volt', 'dt' => 3 ),

        array( 'db' => 'watt', 'dt' => 4 ),

       array( 'db' => 'charze_time', 'dt' => 5 ),

       array( 'db' => 'run_aftercharz', 'dt' => 6 ),

        array( 'db' => 'battry_type', 'dt' => 7 ),

        array( 'db' => 'model', 'dt' => 8),

      array( 'db' => 'amp', 'dt' => 9 ),

      array( 'db' => 'no_plates', 'dt' => 10 ),


      array( 'db' => 'warranty_yers','dt' => 11 ),

      array( 'db' => 'battry_status', 'dt' => 12 ),

      array( 'db' => 'invoice_no', 'dt' => 13 ),
      array( 'db' => 'pur_date', 'dt' => 14 ),

      array( 'db' => 'vendor_name', 'dt' => 15 ),

      array( 'db' => 'amount', 'dt' => 16),

      array( 'db' => 'invoice_file', 'dt' => 17),

      array( 'db' => 'challan_file', 'dt' => 18),

      array( 'db' => 'challan_no', 'dt' => 19),
      
       array( 'db' => 'username', 'dt' => 20 ),

       array( 'db' => 'employee', 'dt' => 21 ),

      array( 'db' => 'date', 'dt' => 22 )
   
);

 
$sql_details = array(
    'user' => 'root',
    'pass' => '',
    'db'   => 'tyre_mngt',
    'host' => 'localhost'
);
 
 
include("scripts/ssp.class.php");
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
?>