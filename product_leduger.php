    
<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
     <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
     <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <form method="POST" name="productname" action="product_leg_view.php?id">
<?php include('aside_main.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">
    <div class="box box-info">
     <!--  <div class="box-header with-border">
        <h3 class="box-title">Stock Transfer Record</h3>
         <input type="button" style="float: right;" onclick="window.location.href = 'add_stock_transfer.php';" name="filter" id="filter" value="Add New" class="btn btn-info" />  
        
      </div> -->
<!--       <script>
        function myFunction() {
          window.print();
        }
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }
            
       </script> -->
         <div class="row">
             <!--    
                <div class="col-md-12"> -->
               <!--  <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-2">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>
            </div>  -->
           
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row-2">
           
            <div class="col-md-12">
              <div id="order_table"> 
              <div class="table-responsive">  
               <table  id="employee_data" name="employee_data"  class="table table-bordered" border="2"; style=" font-family:verdana; font-size:  13px; background-color: #EEEDEC ">
                  <thead>  
                       <tr class="table-active">
                            <td>View</td>  
                       <!--      <td>GRN Number</td>  
                            <td >Purches Number</td>-->
                            <td>Product Name</td> 
                            <!--  <td>Party Name</td -->

                       </tr>  
                  </thead>  
                  <?php 
                   $username =  $_SESSION['username'];
                   $query = "SELECT product.id,product.productno,product.productname,product.quantity,product.company,product.producttype,product.pro_loc,product.date,date_purchase_key.purchase_order,files_upload.grn_no,files_upload.party_name
                      FROM product
                      INNER JOIN date_purchase_key
                      ON product.id = date_purchase_key.id
                      INNER JOIN files_upload
                      ON product.id=files_upload.id";
              $result = mysqli_query($conn, $query); 
                 while($row = mysqli_fetch_array($result))  
                       {    
                                 // $id = $row['id'];
                                 // $grn_no=$row['grn_no'];
                                 // $purchase_order=$row['purchase_order'];
                                 $productname=$row['productname'];
                                  $party_name=$row['party_name']; 
                             
                        ?>
                   <tr>
                   <td>
                      <input type="hidden" name="productname" value="<?php echo $productname ?>">
                      <input type="submit"  name="submit" value="view" class="btn btn-primary btn-sm"> 
                    </form>
                    </td>
                    <!-- <td><?php echo $store_in_operation_date?></td> -->
                <!--     <td><?php echo $grn_no?></td>
                    <td><?php echo $purchase_order?></td> -->
                    <td><?php echo $productname?></td>
                   <td><?php echo $party_name?></td>
                </tr>
                <?php }  ?>
                
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
      
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({ 
                          url:"filter_internal_job.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>



