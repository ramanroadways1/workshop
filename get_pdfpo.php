
<!DOCTYPE html>
<html>
<head>
   <?php 
    include("header.php");
    include("aside_main.php");
  ?>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script >
  function getrate() {

      var rate = document.getElementById("rate").value;

      var quantity = document.getElementById("quantity").value;

      var amount = document.getElementById("amount").value;
       
      if(!(rate==0))
       {
        if(!(quantity==0))
        {
           document.getElementById("amount").value=rate*quantity;
        }
      }
      else if(!(quantity==0))
         {
          if(!(amount==0))
          {
             document.getElementById("rate").value=amount/quantity;
          }
         }
      }
function getgst()

    {
        var amount1 = document.getElementById("amount").value;
       
        var gst1=document.getElementById("gst").value;
        var total_gst1=document.getElementById("gst_amount").value;
        if(!(amount1==0))
        {
          if(!(gst1==0))
          {
            document.getElementById("gst_amount").value=amount1*gst1/100;
          }
          else if(!(total_gst1==0))
          {
            document.getElementById("gst").value=total_gst1*100/amount1;
          }
        }

    }


    function get_total(){
  /*    alert("hi");*/
      var gst_amount = document.getElementById("gst_amount").value;
      //alert(gst_amount);
      var amount = document.getElementById("amount").value;
      //alert(amount);
      document.getElementById("total_amount").value=parseInt(amount)+parseInt(gst_amount);
    }


     function get_gst()
    { 
        var gst_value1=document.getElementById("gst").value;
        var gst_type1= document.getElementById("gst_type").value;
        if(gst_type1=="cgst_sgst"){
            var result = gst_type1+ "," +gst_value1/2;
            //alert(result);
          }else
          {
            var result = gst_type1+ "," +gst_value1;
          }
          //var result= parseInt(gst_value1)*parseInt(gst_type1);
          document.getElementById("sel_gst").value=result;
          
    }

</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
      <!-- <section class="content-header">
        <h1>Update Purchase Order</h1><br>
      </section> -->
      
        <section class="content">
          
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Purchase Order PDF Download</h3>

         <!--  <div class="box-tools pull-right">
            
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div><br>
          <!-- SELECT2 EXAMPLE -->
         <form method="POST" action="print_po1.php">
            <div class="row">
              <div class="col-md-0"></div> 
                <div class="col-md-12">
                  <!-- <div class="box box-default">
                    <div class="box-header with-border"> -->
                      <div class="col-md-8">
                        <div class="form-group">
                          <div class="col-md-3">
                              <b>Purchase Order</b>
                          </div>
                          <div class="col-md-7">
                            <input type="text" id="fetch_purchase_order" name="purchase_order" placeholder="Enter Purchase Order..." class="form-control" required autocomplete="off" />
                          </div>
                          <div class="col-md-2">
                              <button type="submit" class="btn btn-search" ><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                      <!-- <div align="right">
                        <a href='../generate_po.php' type="submit" name="add" id="add" class="btn btn-info">Add</a>
                      </div> -->
                    </div>
                    <!-- </div>
                  </div> -->
                </div> 
              </div>
            </form><br>

            <div class="table-responsive">
                 <div id="result"></div> 
            </div>
        </div>
            
         </section>

      <div class="col-sm-offset-2 col-sm-8">
      </div>
        
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<div id="result_main"></div>

<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_po.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>

<script>
        $(document).ready(function (e) {
        $("#MyForm1").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "search_po.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
</script> 

<script type="text/javascript">     
$(function()
{    
$( "#fetch_purchase_order" ).autocomplete({
source: 'fetch_pdfautocompletpo.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});

</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormPOUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_po.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 
<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

      <form action="print_po1.php" autocomplete="off" id="FormPOUpdate" method="POST">
     
			 <div id="myModal22" class="modal fade" role="dialog">
			  <div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Update PO</h4>
				  </div>
				  
			<script type="text/javascript"> 
			$(function()
			{ 
			$( "#productno" ).autocomplete({
			source: 'po_no_search.php',
			change: function (event, ui) {
			if(!ui.item){
			$(event.target).val(""); 
			$(event.target).focus();
			alert('Product does not exists.');
			$('#form')[0].reset();
			} 
			},
			focus: function (event, ui) { $('#form')[0].reset(); return false; } }); });
			</script>  
				  
				  <div class="modal-body">
					<div class="row">
						<div class="col-md-6">
						  <div class="form-group">
                <input type="text" style="width: 100%;" class="form-control" id="id" name="id" readonly/>
							<label>Product number</label>
					 
							<input type="text" style="width: 100%;" class="form-control" id="productno" onblur="get_data(this.value);" name="productno"  placeholder="product number" readonly required />
						  </div>
							<div id="rate_master222"></div>
								  <script>
									 function get_data(val) {
									$.ajax({
									  type: "POST",
									  url: "get_data1.php",
									  data:'productno='+val,
									  success: function(data){
										  $("#rate_master222").html(data);
									  }
									  });
								  }
								</script>
						   <div class="form-group">
							<label>Product name</label>
							<div>
							  <div  class="rs-select2 js-select-simple select--no-search">
									 <input class="form-control" name="productname" id="productname"  placeholder="product Name" readonly>
								</div>
						  </div>
						  </div>

						   <div class="form-group">
							<label>Rate master</label>
							<div  class="rs-select2 js-select-simple select--no-search">
							  <input class="form-control" name="rate_master" id="rate_master" onblur="getdiff(this.value);" placeholder="rate master"  readonly>
							</div>
						  </div>

						   <input type="hidden" class="form-control" name="lock_unlock" id="lock_unlock" readonly required>
						  
						  <div class="form-group">
							 <label>Quantity</label>
							 <input type="number" oninput="sum1()" style="width: 100%;"  id="quantity" class="form-control" name="quantity" placeholder="Quantity" required/>
						  </div>

						  <script>
						  function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var fix_rate = Number($('#rate_master').val());
                 var lock = $('#lock_unlock').val();
                 if(lock==1)
                 {
                   if(myrate>fix_rate)
                   {
                     alert('You can not exceed Master Rate. Master rate is '+ fix_rate);
                     $('#rate').val('');
                   }
                 }
                }
							</script>

						  <div class="form-group">
							 <label>Rate unit</label>
							 <input type="Number" step="any" oninput="ChkForRateMaxValue();sum1();" onblur="getdiff(this.value);" style="width: 100%;" id="rate" class="form-control" name="rate" placeholder="rate unit" required/>
						  </div>
						  
							  <script>
							  function sum1()
							  {
								 var qty = Number($('#quantity').val());  
								 var rate = Number($('#rate').val());  
								 var rate_master = Number($('#rate_master').val());  
                 var gst = Number($('#gst').val());  
								 var difference = rate_master-rate;
                 var amt = qty*rate;
								 
								 $('#amount').val(amt);
								 $('#gst_amount').val((amt*gst/100).toFixed(2));
								 $('#total').val(Number($('#gst_amount').val())+Number($('#amount').val()));
                 $('#diff').val(difference);
							  }
							  </script>

						  <div class="form-group">
							 <label>Rate Difference</label>
							 <input type="text" style="width: 100%;" id="diff" class="form-control" name="diff" placeholder="Rate Difference" readonly required/>
						  </div>
						  <script>
							/*function getdiff(i)
							{
							  var rate1 = Number($('#rate_master').val());  
							  var rate2 = Number($('#rate').val());
							  var difference = rate1-rate2;
							  $('#diff').val(difference);
							 // $('#diff').val(Number($('#rate_master').val())-Number($('#rate').val()));
							}*/
						  </script>
						  
						  <div class="form-group">
							 <label>Amount</label>
							 <input type="text" style="width: 100%;" id="amount" class="form-control" name="amount" placeholder="amount" readonly required/>
						  </div>

						  
						</div>
						<!-- /.col -->
						<div class="col-md-6">
						  
					   <div class="form-group">
							 <label>gst value</label>
							 <select onchange="sum1();MyGst(this.value);" class="form-control" style="width: 100%;" required id="gst" name="gst_value">
								  <option value="">0</option>
    	            <option value="5">5</option>
                  <option value="12">12</option>
                  <option value="18">18</option>
                  <option value="28">28</option>
							  </select>
						  </div>
					

							  <div class="form-group">
								<label>gst_amount</label>
								   <input type="text" style="width: 100%;" class="form-control" id="gst_amount" name="gst_amount" readonly placeholder="gst amount" />
							  </div>

							  <div class="form-group">
								<label >gst_type</label>
								  <input type="text" style="width: 100%;" class="form-control" id="gst_type" name="gst_type" readonly/>
							  </select> 
							  </div>
							  
							<script>
						  function MyGst(i)
						  { 
                var gst = Number($('#gst').val());  
                var gst_type=document.getElementById("gst_type").value;
                if(gst_type=='cgst_sgst')
                {
                  $('#gst_acc_type').val(gst/2);
                }
                else{
                  $('#gst_acc_type').val(gst);
                }
						  }
						  </script>

							  <div class="form-group">
								<label>Selected Gst</label>
									<input type="text" style="width: 100%;" class="form-control prc" id="gst_acc_type" name="gst_acc_type" readonly placeholder="Selected G.S.T" required>
							  </div>

							  <div class="form-group">
								<label>total amount</label>
								  <input type="text" style="width: 100%;" id="total" class="form-control" name="total_amount"  placeholder="Total Amount" required readonly>
							  </div>
							  
						</div>
						<!-- /.col -->

					  </div>
				  </div>
				  <div class="modal-footer">
					<!--  <input type="reset" align="right" name="Reset" value="Reset" tabindex="50"> -->
						  
					<button type="submit" class="btn btn-danger">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				</div>

			  </div>
			</div>
		</form>
