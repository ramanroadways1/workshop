    

<?php


require("connect.php");
include('header.php');

?>
  
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include('aside_main.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Add New Theft Stock</h3>
         
      </div>


      <script>
        function myFunction() {
          window.print();
        }
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }

            $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'Y-m-d'   
                     });  
                     $(function(){  
                          $("#theft_out_date").datepicker();  
                          
                     });  
                   
                });  
            
       </script>
         <div class="row">
             
        <div class="box-body">
          <div class="row-2">
           
            <div class="col-md-12">
              <div id="order_table"> 
               <form method="post" action="outward_stock_theft.php" autocomplete="off">
                  <?php 
                      $username =  $_SESSION['username'];
                  ?>
              <div class="row">
                             <div class="col-md-3">
                                <div style="width: 285px;">
                                  <label>Office:</label>
                                  <input type="text" readonly="readonly"  class="form-control"name="office" value="<?php echo $username; ?>">
                                </div>
                            </div>   
                           <div class="col-md-6">
                                <div  style="width: 285px; float: right;">
                                 <label>Theft Date:</label>
                                   <input type="text" name="theft_out_date" id="theft_out_date" class="form-control" required="required" placeholder="Theft Date" /> 
                              </div> 
                          </div>
                           <div class="col-md-3">
                                <div >
                                 <label>Driver/Work incharge:</label>
                                   <input type="text" name="vendor" id="vendor" class="form-control" required="required" placeholder="Driver/Work incharge" /> 
                              </div> 
                          </div>
                </div>
                
              <br>
              <div class="row-4">
                  <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:verdana; font-size:  13px; background-color: #EEEDEC ">
                  <tr>
                 <td>
                    <div class="form-group">
                    <label>Search Product By</label>
                       <select onchange="SearchBy(this.value)" class="form-control" required id="search_product" name="search_product">
                          <option value="">---Select---</option>
                          <option value="PNO">Product Number</option>
                          <option value="PNAME">Product Name</option>
                       </select>
                   </div>

                   <script>
            function SearchBy(elem)
            {
              /*if($('#party_set').val()=='0' || $('#party_set').val()=='')
              {
                alert('Please select party first !');
                window.location.href='generate_po.php';
                $('#search_product').val('');
              }*/
              /*else
              {*/
              $('.pname1').val(''); 
              $('.pno1').val(''); 
              $('.pno1').attr('id','productno');  
              $('.pname1').attr('id','productname');          
                
              if(elem=='PNO')
              {
                $('.pname1').attr('onblur',""); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',false);  
                $('.pno1').attr('onblur',"get_data1(this.value)");  
                $('.pno1').attr('id','productno');  
                $('.pname1').attr('id','');   
              }
              else if(elem=='PNAME')
              {
                $('.pno1').attr('onblur',""); 
                $('.pname1').attr('onblur',"get_data2(this.value)");  
                $('.pname1').attr('readonly',false);  
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('id',''); 
                $('.pname1').attr('id','productname');  
              }
              else
              {
                $('.pname1').attr('onblur',''); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('onblur',''); 
                $('.pno1').attr('id','productno');  
                $('.pname1').attr('id','productname');  
              }
             /* }*/
            }
            </script>
            
            <script type="text/javascript">
               function get_data1(val) {
                  $.ajax({
                    type: "POST",
                    url: "stock_transfer_detail_by_pno.php",
                    data:'productno='+val,
                    success: function(data){
                      //alert(data);
                        $("#rate_master222").html(data);
                    }
                    });
                }
               
                $(function()
                  { 
                  $( "#productno" ).autocomplete({
                  source: 'autocomplete_product.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                  alert('Product No does not exists.');
                  } 
                  },
                  focus: function (event, ui) {return false; } }); });
                
            </script>
            
                
            <div id="rate_master222"></div>

                  </td>
                  <td>
                     <label>Product No</label>
                   <!--  <input type="text" name="productno" id="productno"  onchange="get_data1(this.value);"   class="form-control"  required="required"> -->
                    <input type="text" class="form-control pno1" id="productno" name="productno" placeholder="product number" required />
                   </td>

                   <script type="text/javascript">
               function get_data2(val) {
                  $.ajax({
                    type: "POST",
                    url: "stock_transfer_detail_by_pname.php",
                    data:'productname='+val,
                    success: function(data){
                      $("#rate_master222").html(data);
                    }
                    });
                }
                $(function()
                { 
                  $("#productname").autocomplete({
                  source: 'autocomplete_product_by_name.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    location.reload();
                  alert('Product Name does not exists MAIN PAGE.');

                  } 
                },
                focus: function (event, ui) { return false; } }); });
            </script>

                   <td>
                     <label>Product Name</label>
                     <input type="text" class="form-control pname1" name="productname" id="productname"  placeholder="product Name" required/>
                    </td>

                    <td>
                       <label>Company</label>
                      <input type="text" name="company" readonly="readonly" id="company"  class="form-control">
                    </td>

                     <input type="hidden" name="producttype" readonly="readonly"  id="producttype"  class="form-control">

                     <input type="hidden" name="unit" id="unit" class="form-control"  required="required">
                 </td>
                </tr>

                <tr>
                   
                   <td>
                     <label>Rate/Unit</label>
                    <input type="number" id="rate" name="rate" readonly="readonly" required="required"   class="form-control">
                   </td>


                  <td>
                     <label>Avl/Qty</label>
                    <input type="text" name="avl_qty" readonly="readonly" id="avl_qty" class="form-control">
                  </td>

                  <td>
                     <label>Theft/Qty</label>
                    <input type="number" id="trf_qty" oninput="ChkForRateMaxValue();" name="stock_operation_qty"  required="required" class="form-control">
                   </td>

                  <td>
                     <label>Theft Amount</label>
                    <input type="Number" name="stock_operation_amount" autocomplete="off" id="amount" class="form-control" step="any">
                  
                  </td>
                  <td><br> <input type="submit" name="submit" style="width: 80px;"  class="btn btn-success ml-5" ></td>
                  </tr>
                  
                  
                </table>
                </div>
               </form>


                <div style="overflow-x:auto;">
            <table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
                    <tbody>
                       <tr>
                  <th style="display: none;">Id</th>
                  <th>Driver</th>
                  <th>Product No</th>
                   <th>Product Name</th>
                   <th>Company</th>
                  <th>Rate</th>
                  <th>Available Quantity</th>
                  <th>Theft Quantity</th>
                  <th>Theft Amount</th>
                  <th>Theft Date</th>
                  <th>Remove</th>
    
            </tr>
            </tbody>  
              <?php
              $query = "SELECT * FROM outward_stock WHERE office='$username' and status='theft_temp' ";
          
              $result = mysqli_query($conn, $query);
              while($row = mysqli_fetch_array($result)){
              $id = $row['id'];
               $branch = $row['office'];
               $vendor=$row['vendor'];
                $stock_operation_date=$row['stock_operation_date'];
               $productno = $row['productno'];
               $productname = $row['productname'];
               $company = $row['company'];
               $rate = $row['rate'];
                  $avl_qty = $row['avl_qty'];
               $stock_operation_qty = $row['stock_operation_qty'];
               $stock_operation_amount = $row['stock_operation_amount'];
              ?>
              <tr>
                 <input type="hidden" name="id" value="<?php echo $id; ?>">
              <!--   <td ><?php echo $branch?>
                  <input type="hidden" name="office" value="<?php echo $branch; ?>">
                </td> -->
              
               <!--  <td ><?php echo $vendor?>
                  <input  type="hidden" readonly="readonly" name="vendor[]" value="<?php echo $vendor; ?>">
                </td> -->
                 <td ><?php echo $vendor?>
                  <input  type="hidden" readonly="readonly" name="vendor[]" value="<?php echo $vendor; ?>">
                </td>
               
                <td ><?php echo $productno?>
                  <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>">
                </td>
                <td ><?php echo $productname?>
                  <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>">
                </td>

                 <td ><?php echo $company?>
                  <input  type="hidden" readonly="readonly" name="company[]" value="<?php echo $company; ?>" >
                </td>
                
                <td ><?php echo $rate?>
                  <input  type="hidden" readonly="readonly" name="rate[]" value="<?php echo $rate; ?>">
                </td>
                 <td ><?php echo $avl_qty?>
                  <input  type="hidden" readonly="readonly" name="avl_qty[]" value="<?php echo $avl_qty; ?>">
                </td>
                <td ><?php echo $stock_operation_qty?>
                  <input  type="hidden" readonly="readonly" name="stock_operation_qty[]" value="<?php echo $stock_operation_qty; ?>">
                </td>
                <td ><?php echo $stock_operation_amount?>
                  <input  type="hidden" readonly="readonly" name="stock_operation_amount[]" value="<?php echo $stock_operation_amount; ?>">
                </td>
               
                 <td ><?php echo $stock_operation_date?>
                  <input  type="hidden" readonly="readonly" name="stock_operation_date[]" value="<?php echo $stock_operation_date; ?>">
                </td>
                
                 <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="id" value="X" class="btn btn-danger" />
                </td>
                <!-- <td>
                    <input type="button" onclick="DeleteModal('<?php /*echo*/ $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                </td> --> 
                <script>
                      function DeleteModal(id)
                      {
                        var id = id;
                        var username = '<?php echo $username; ?>'
                        if (confirm("Do you want to delete this data To Add Theft..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_temp_theft_data.php",
                                  data: 'id='+id + '&username='+username,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div> 
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="insert_final_theft_stock.php" id="main_form">
              
            <!--    <input  type="hidden" readonly="readonly" name="unique_no" value="<?php echo $unique_no; ?>"> -->

               <input type="hidden" name="office" id="branch" class="form-control" fo value="<?php echo $branch ?>" />  

             <center>
              <button type="submit" value="Submit" style="width: 130px;"  class="btn btn-primary ml-5" >Add To Theft</button>
             </center>

           </form>
              
            </div>  
          </div>
             
        </div>
            
      </div>
          
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
             <script>  
             $(document).ready(function(){  
                  $('#employee_data').DataTable();  
             });  
             </script> 

               <script>  
                    $(document).ready(function(){  
                         $.datepicker.setDefaults({  
                              dateFormat: 'yy-mm-dd'   
                         });  
                         $(function(){  
                              $("#from_date").datepicker();  
                              $("#to_date").datepicker();  
                         });  
                         $('#filter').click(function(){  
                              var from_date = $('#from_date').val();  
                              var to_date = $('#to_date').val();  
                              if(from_date != '' && to_date != '')  
                              {  
                                   $.ajax({  

                                        url:"filter_internal_job.php",  
                                        method:"POST",  
                                        data:{from_date:from_date, to_date:to_date},  
                                        success:function(data)  
                                        {  
                                        //alert(data)
                                             $('#order_table').html(data);  
                                        }  
                                   });  
                              }  
                              else  
                              {  
                                   alert("Please Select Date");  
                              }  
                         });  
                    });  
               </script>

             <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var avl_qty = Number($('#avl_qty').val());
                 var trf_qty = $('#trf_qty').val();
                 if(avl_qty=="")
                 {
                   alert("Avalaible Quantity is Null");
                     $('#trf_qty').val('');
                 }else{
                 
                       if(trf_qty>avl_qty)
                       {
                         alert('You can not exceed Avl Qty  Quantity  is '+ avl_qty);
                         $('#trf_qty').val('');
                          $('#amount').val('');
                       }else{
                        var amt = trf_qty*myrate;
                         $('#amount').val(amt);

                       }
                     }

                }
               </script>


