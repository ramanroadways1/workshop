<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>


  <table id="employee_data" class="table table-hover" border="4"; >
<?php
include("connect.php");
$purchase_order=$_POST['purchase_order'];
?>
<input type="hidden" name="purchase_order[]" value="<?php echo $purchase_order;?>"/>
<?php
 $query = "SELECT insert_po.id as id,insert_po.key1 as key1,purchase_order,productname,party_name,date1, rate ,diff,lock_unlock, quantity, amount ,total ,gst ,gst_type,gst_acc_type, gst_amount,rate_master ,productno FROM date_purchase_key,insert_po WHERE insert_po.key1 = date_purchase_key.key1  and purchase_order = '$purchase_order'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
 echo '
  <div class="table-responsive">
    <tr>
      <td scope="row">id</td>
      <td>Product Number</td>
      <td>Product Name</td>
      <td>Rate(from Rate Master)</td>
      <td>Rate/Unit</td>
	  <td>Rate Difference</td>
      <td>Quantity</td>
      <td>Amount</td>
      <td>Gst</td>
	  <td>Gst Type</td>
	  <td>Gst Acc. Type</td>
      <td>Gst Amount</td>
      <td>Total Amount</td>
      <td>Update</td>
      <td>Delete</td>
    </tr>
 ';
 while($row = mysqli_fetch_array($result))

 {
  echo '
   <tr>
   <input type="hidden" name="key1[]" id="key1'.$row["id"].'" value='.$row["key1"].'>
    <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
    
     <td>'.$row["productno"].'<input type="hidden" name="productno[]" value="'.$row["productno"].'"></td>
     <td>'.$row["productname"].'<input type="hidden" name="productname[]" value="'.$row["productname"].'"></td>
     <td>'.$row["rate_master"].'<input type="hidden" name="rate[]" value="'.$row["rate_master"].'"></td>
     <td>'.$row["rate"].'<input type="hidden" name="rate[]" value="'.$row["rate"].'"></td>
	 <td>'.$row["diff"].'<input type="hidden" name="diff[]" value="'.$row["diff"].'"></td>
     <td>'.$row["quantity"].'<input type="hidden" name="quantity[]" value="'.$row["quantity"].'"></td>
     <td>'.$row["amount"].'<input type="hidden" name="amount[]" value="'.$row["amount"].'"></td>
     <td>'.$row["gst"].'<input type="hidden" name="gst[]" value="'.$row["gst"].'"></td>
	 <td>'.$row["gst_type"].'<input type="hidden" name="gst_type[]" value="'.$row["gst_type"].'"></td>
	 <td>'.$row["gst_acc_type"].'<input type="hidden" name="gst_acc_type[]" value="'.$row["gst_acc_type"].'"></td>
     <td>'.$row["gst_amount"].'<input type="hidden" name="gst_amount[]" value="'.$row["gst_amount"].'"></td>
     <td>'.$row["total"].'<input type="hidden" name="total[]" value='.$row["total"].'></td>
     <td>
      <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="update" class="btn btn-info" />
    </td>
    
    <td>
      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
    </td>
       
   </tr>
        ';
       } 

      }
      else
      {
       echo 'Data Not Found';
      }

      ?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var key1 = $('#key1'+id).val();
    if (confirm("Do you want to delete this purchase order?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_po.php",
              data:'id='+id + '&key1='+key1 ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>