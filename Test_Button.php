
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

     <?php include('aside_main.php');?>

     <script>
                      $(document).ready(function() {
                        $("#datepicker").datepicker({
                           dateFormat: "yy-mm-dd",
                           maxDate: '90',
                            minDate: '0'
                        });
                      });


                      $(document).ready(function() {
                        $("#datepicker2").datepicker({
                           dateFormat: "yy-mm-dd",
                           maxDate: '90',
                            minDate: '0'
                        });
                      });
                    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
 
    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Payment</h3>
        
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
             <?php $username =  $_SESSION['username'];  ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">
              <div class="table-responsive"> 
                 <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">   
                  <thead>  
                       <tr>  

                        <th>Action</th>
                       </tr>  
                  </thead>  
                   <?php
              include 'connect.php';
              $show = "SELECT id, grn_no, purchase_order, party_name, key1, count(productno) as productno,  sum(new_total) as new_total, challan_file,  GROUP_CONCAT(remain) as remain, invoice_file,invoice_no,timestamp1,payment_date,back_date FROM files_upload where approve_status='1' and username='$username' and remain>0 group by grn_no";
            
              $result = $conn->query($show);
              if ($result->num_rows > 0) {
                 
                  while($row = $result->fetch_assoc()) {
                      $id = $row['id'];
                      $grn_no = $row['grn_no'];
                      $purchase_order = $row['purchase_order'];
                      $party_name = $row['party_name'];
                      $productno = $row['productno'];
                      $new_total = $row['new_total'];
                      $remain = $row['remain'];
                       $timestamp1 = $row['timestamp1'];
                       $payment_date = $row['payment_date'];
                       $challan_file = $row['challan_file'];
                      $back_date = $row['back_date'];
                      $challan_file1=explode(",",$challan_file);
                      $count3=count($challan_file1);
                      $invoice_file = $row['invoice_file'];
                      $invoice_file1=explode(",",$invoice_file);
                     $count4=count($invoice_file1);
                      ?>
                     
                   
                      <?php
                      $sql21 = "SELECT * FROM insert_pay_invoice  WHERE  grn_no='".$grn_no."' and username = '$username' ";
                      //echo $sql21;echo "<br>";

                     $result21 = $conn->query($sql21);
                    //$row21 = mysqli_fetch_array($result21);
                   ?>
               <?php if(mysqli_num_rows($result21) > 0){ 
                $date_now = date("d-m-Y");
                 //echo $date_now;

                if ($date_now > $payment_date) {
                  //echo "hi";
                ?>
                   <td>
                     <button type="button" onclick="Editdate('<?php echo $grn_no; ?>');" class="btn btn-danger btn-sm" >pay immediately</button>
                    </td>

                <?php } else{
                  ?>
                   <td>
                     <button type="button" onclick="Editdate('<?php echo $grn_no; ?>');" class="btn btn-warning btn-sm" >Panding</button>
                    </td>
                  <?php
                 } 
               }
                      else {
                       
                      ?>
                  <td>
                     <button type="button" onclick="EditModal('<?php echo $grn_no; ?>');" class="btn btn-success btn-sm" >Pay</button>
                    </td>
                   
                <?php } ?>

                       </tr>  
                  <?php   
                      
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>  
                <script type="text/javascript">
                   function EditModal(id)
                              {
                                 var grn_no = id;
                                 var username = '<?php echo $username ?>';
                                 //alert(username);
                                jQuery.ajax({
                                    url: "fetch_grn_for_payment.php",
                                    data: 'username=' + username+ '&grn_no='+grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main").html(data);
                                    }, 
                                        error: function() {}
                                    });
                                document.getElementById("modal_button1").click();
                                $('#ModalId').val(id);

                                }
                </script><div id="result_main"></div>
                 <script type="text/javascript">
                   function Editdate(id)
                              {
                                 var grn_no = id;
                                 var username = '<?php echo $username ?>';
                                 //alert(grn_no);
                                jQuery.ajax({
                                    url: "fetch_update_date.php",
                                    data: 'username=' + username+ '&grn_no='+grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main1").html(data);
                                    }, 
                                        error: function() {}
                                    });
                                document.getElementById("modal_button5").click();
                                $('#ModalId5').val(id);

                                }
                </script><div id="result_main1"></div>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 900px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Detail</h4>
      </div>
      
      <div class="modal-body">
        <form action="insert_payment_Approve.php" method="post" id="FormGRNUpdate">
          <div class="row">
             <input type="hidden" id="ModalId" name="id"/>
             <div class="col-md-10">
              <div class="form-group">
                <input type="hidden" name="username" value="<?php echo $username ?>">         
              </div>
          
             
            </div>

        <div class="col-md-4">
              <div class="form-group">
                <label>GRN No:</label>
               <input type="text" id="grn_no1" readonly="readonly" class="form-control" name="grn_no"/>
              </div>
          
              <div class="form-group">
                <label>Party Name:</label>
                <input type="text" id="party_name1"  readonly="readonly" class="form-control" name="party_name"  />
            </div>
            <div class="form-group">
                <label>Purchase Order:</label>
              <input type="text" id="purchase_order1" readonly="readonly"  class="form-control" name="purchase_order">
              </div>
              <div class="form-group">
                <label>Bank Name:</label><br>
              <textarea id="bank" readonly="readonly"  class="form-control" name="bank_name">
            
              </textarea>
              </div>

               <div class="form-group">
                <label>Acc Holder:</label>
                <textarea id="acc_holder_name" readonly="readonly"  class="form-control" name="acc_holder_name">
              </textarea>
              </div>

              <div class="form-group">
                <label>Email:</label>
                <input type="text" id="email" readonly="readonly"  class="form-control" name="email">
              </div>

          </div>
            <div class="col-md-4">
               <div class="form-group">
                <label>Invoice No:</label>
               <input type="text" id="invoice_no1" readonly="readonly" class="form-control" name="invoice_no" >
              </div>
               <div class="form-group">
                <label>Mobile No:</label>
               <input type="text" id="mobile_no" readonly="readonly" class="form-control" name="mobile_no" >
              </div>
              <div class="form-group">
                <label>Pan:</label>
               <input type="text" id="pan" readonly="readonly" class="form-control" name="pan"  />
              </div>

            </div>

             <div class="col-md-4">

              <div class="form-group">
                <label>gstin No:</label>
               <input type="text" id="gstin" readonly="readonly"  class="form-control" name="gstin" >
              </div>
              
               <div class="form-group">
                <label>Acount No:</label>
               <input type="text" id="acc_no" readonly="readonly"  class="form-control" name="acc_no" >
              </div>
              <div class="form-group">
                <label>IFSC Code:</label>
               <input type="text" id="ifsc_code" readonly="readonly" class="form-control" name="ifsc_code"  />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>GRN Amount:</label>
               <input type="text" id="new_total1"  readonly="readonly" class="form-control" name="new_total" />
              </div>
            </div>

            
            <div class="col-md-4">
              <div class="form-group">
                <label>Total(After deduction):</label>
               <input type="text" readonly="readonly" id="remain" class="form-control" name="remain"  />
              </div>
            </div>

           <div class="col-md-4"><br>
              <div class="form-group">
                <label>Other(Deduct Amount):</label>
               <input type="Number" id="other" required="required" oninput="sum1();" autocomplete="off" class="form-control" name="other_amount" min="0" />
              </div>
               <div class="form-group">
                <label>Payment Due Date:</label><br>
              <input type="text" id="datepicker" required="required" autocomplete="off" class="form-control" name="payment_date" >
              </div>
            </div>
            <div class="col-md-4"><br>
              <div class="form-group">
                <label>Remark:</label>
               <input type="text" id="remark" required="required"  autocomplete="off" class="form-control" name="remark"  />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Company:</label>
                <select onchange="SearchBy(this.value)" required="required" class="form-control office_type"  id="company" name="company">
                    <option value="">--Company--</option>
                     <option value="rrpl">RRPL</option>
                     <option value="rr">RR</option>
                    </select>
              </div>

            </div>
            
              <script>
              function sum1()
              {
                 var new_total1 = Number($('#new_total1').val());
               //var remain_amount = Number($('#remain').val());  
               var other_amount = Number($('#other').val()); 
                 
                 if (other_amount > 0) {
                  $("#remark").attr('readonly',false); 
                 }if (other_amount == 0) {
                   $("#remark").attr('readonly',true);
                   $("#remark").val(''); 
                 }


                if(other_amount>=new_total1) 
                   {
                         alert('You can not exceed  or same Amount '+ new_total1);
                         $('#other').val('');
                          $('#remain').val('');
                    }

              
              var final_amount = new_total1-other_amount;
               $('#remain').val((final_amount).toFixed(2));
              
              }
              </script>
           
          </div>
           </div>
       <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
        </form>
    </div>
    </div>
</div>
</div>
<button type="button" id="modal_button5" data-toggle="modal" data-target="#myModal23" style="display:none">DEMO</button>
<div id="myModal23" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 900px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Detail</h4>
      </div>
      
      <div class="modal-body">
        <form action="update_date_payment_Approve.php" method="post" id="FormGRNUpdate">
          <div class="row">
             <input type="hidden" id="ModalId5" name="id"/>
             <div class="col-md-10">
              <div class="form-group">
           <input type="hidden" name="username" value="<?php echo $username ?>">
              </div>
             
            </div>

        <div class="col-md-4">
              <div class="form-group">
                <label>GRN No:</label>
               <input type="text" id="grn_no2" readonly="readonly" class="form-control" name="grn_no"/>
              </div>
          
              <div class="form-group">
                <label>Party Name:</label>
                <input type="text" id="party_name2"  readonly="readonly" class="form-control" name="party_name"  />
            </div>
            <div class="form-group">
                <label>Purchase Order:</label>
              <input type="text" id="purchase_order2" readonly="readonly"  class="form-control" name="purchase_order">
              </div>
              <div class="form-group">
                <label>Bank Name:</label><br>
              <textarea id="bank2" readonly="readonly"  class="form-control" name="bank_name">
            
              </textarea>
              </div>

              <div class="form-group">
                <label>Payment Due Date:</label><br>
              <input type="text" id="datepicker2" required="required" autocomplete="off" class="form-control" name="payment_date" >
              </div>
          </div>
            <div class="col-md-4">
               <div class="form-group">
                <label>Invoice No:</label>
               <input type="text" id="invoice_no2" readonly="readonly" class="form-control" name="invoice_no" >
              </div>
               <div class="form-group">
                <label>Mobile No:</label>
               <input type="text" id="mobile_no2" readonly="readonly" class="form-control" name="mobile_no" >
              </div>
              <div class="form-group">
                <label>Pan:</label>
               <input type="text" id="pan2" readonly="readonly" class="form-control" name="pan"  />
              </div>

            </div>

             <div class="col-md-4">

              <div class="form-group">
                <label>gstin No:</label>
               <input type="text" id="gstin2" readonly="readonly"  class="form-control" name="gstin" >
              </div>
              
               <div class="form-group">
                <label>Acount No:</label>
               <input type="text" id="acc_no2" readonly="readonly"  class="form-control" name="acc_no" >
              </div>
              <div class="form-group">
                <label>IFSC Code:</label>
               <input type="text" id="ifsc_code2" readonly="readonly" class="form-control" name="ifsc_code"  />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>GRN Amount:</label>
               <input type="text" id="new_total2"  readonly="readonly" class="form-control" name="new_total" />
              </div>
              
            </div>

            
            <div class="col-md-4">
              <div class="form-group">
                <label>Total(After deduction):</label>
               <input type="text" readonly="readonly" id="payment2" class="form-control" name="remain"  />
              </div>
            </div>
          </div>
           </div>

       <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
        </form>
    </div>
  
    </div>
  
</div>
</div>





