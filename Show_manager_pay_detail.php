
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('manager_aside.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper">
   
    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
       Approved Payment</h3>
       <!--  <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button> 
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
               <?php $username =  $_SESSION['username'];  ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">
              
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                        
                        <th style="display: none;">ID</th>
                          <td>Grn No</td>
                         <td style="display: none;">Grn No</td>
                        <td style="display: none;">GRN No</td>
                        <td style="display: none;">Purchase Order</td> 
                        <td>Party Name</td>
                        <td>Company</td>
                         <td>Account Detail</td> 
                        <td>Other Amount</td>
                         <td>Payment</td>
                           <td>Payment Date</td>
                        <td>Invoice Bill</td>
                        <td>Approve Time</td>
                       </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $show = "SELECT * FROM insert_pay_invoice where  username='$username' and length(invoice_file) > 10  and approve='1' group by grn_no order by id DESC";
                  $result = $conn->query($show);
                  if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { $id = $row['id'];
                    $grn_no = $row['grn_no'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                    $mobile_no = $row['mobile_no'];
                    $pan = $row['pan'];
                    $gstin = $row['gstin'];
                    $bank_name = $row['bank_name'];
                     $acc_holder_name = $row['acc_holder_name'];
                     $company = $row['company'];
                    $payment = $row['payment'];
                     $remark = $row['remark'];
                       $payment_date = $row['payment_date'];
                    $other_amount = $row['other_amount'];
                    $invoice_file = $row['invoice_file'];
                    $invoice_file1=explode(",",$invoice_file);
                    $count4=count($invoice_file1);
                    $timestamp1 = $row['timestamp1'];
                  ?>

                       <tr>
                         <td style="display: none;">
                            <form method="POST" action="manager_approval_page_detail.php" target="_blank">
                              <input type="submit"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-xs fa fa-eye">
                            </form>
                          </td>

                         <td>
                          <form method="POST" action="grn_after_approval.php" target="_blank">
                            <input type="submit"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-xs fa fa-eye">
                          </form>
                         </td>

                          <td style="display: none;"><?php echo $id; ?>
                            <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                          </td>
                       <td style="display: none;"><?php echo $grn_no; ?>
                        <input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'>
                      </td>
                      <td style="display: none;"><?php echo $purchase_order; ?>
                        <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                      </td> 
                      <td><?php echo $party_name; ?>
                        <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                      </td>
                       <td><?php echo $company; ?>
                        <input type="hidden" name="acc_holder_name[]" value='<?php echo $acc_holder_name; ?>'>
                      </td>
                     
                         <td>
                          Bank:<?php echo $bank_name; ?>
                            <?php echo "<br>"; ?>
                             Acc Holder:<?php echo $acc_holder_name; ?>
                            <?php echo "<br>"; ?>
                         Mobile No: <?php echo $mobile_no; ?>
                          <?php echo "<br>"; ?>AC No:<?php echo $pan; ?>
                        <?php echo "<br>"; ?>GSTIN:<?php echo $gstin; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>
                       
                         <td>Other:<?php echo $other_amount; ?>
                          <?php echo "<br>" ?>
                         Remark: <?php echo $remark; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>
                       <td><?php echo $payment; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>

                       <td><?php echo $payment_date; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>
                         
                      
                      <td>
                         <?php
                          if (strlen($invoice_file) > 15) {
                         for($j=0; $j<$count4; $j++){
                         
                          ?>
                           <a href="<?php echo $invoice_file1[$j]; ?>" target="_blank">Invoice <?php echo $j+1; ?></a>
                           <input type="hidden" name="invoice_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $invoice_file; ?>'>
                           <?php
                         }
                          }  
                     else{
                      echo "no file";
                     }

                        ?>
                        <!-- <a href="<?php echo $invoice_file; ?>" target="_blank"><?php echo $invoice_file; ?></a>
                        <input type="hidden" name="invoice_file[]" value='<?php echo $invoice_file; ?>'> -->
                      </td>
                    
                      <td><?php echo $timestamp1; ?>
                        <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                      </td>
                    

                     <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var grn_no = val;
                                  //alert(grn_no);
                                  var username = '<?php echo $username ?>'
                                    if (confirm("Are You Sure To Approve this GRN?"))
                                   {
                                        $.ajax({
                                           type:"post",
                                           url:"payment_final_approve.php",
                                          data:'grn_no='+grn_no + '&username='+username,
                                            success:function(data){
                                               alert(data)
                                               location.reload(); 
                                                    
                                              }
                                          });

                                     }
                                 }
                    </script>

                    <script>
                      function DeleteModal(grn_no)
                      {
                        var grn_no = grn_no;
                        //alert(grn_no);
                         var username = '<?php echo $username ?>'

                       
                        if (confirm("Do you want to delete this grn?"))
                        {
                          if(grn_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_grn_from_payment.php",
                                  data:'grn_no='+grn_no + '&username='+username ,
                                  success: function(data){
                                    alert(data)
                                           location.reload(); 
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                    </tr>  
                 <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>
                </table>  
               <!--  <center>
                  <input type="submit" class="btn btn-primary" name="action[]" value="Approved">
                </center> -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

