
 <?php  
 include("connect.php");
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
   
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
  
    <section class="content">


    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Goods Received Notes Detail</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        

        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
               
<?php  
 include("connect.php");
 $query = "SELECT * FROM grn ORDER BY grn_no desc";  
 $result = mysqli_query($conn, $query);  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  
      </head>  
      <body>  

           <div class="container">  
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                      <input type="button" style="float: right; " class="btn btn-info add-new" name="" value="Print" onclick="myprint()"><br>  
                      <script type="text/javascript">
                        function myprint() {
                                window.print();
                              }
                      </script>
                </div>  
                <div style="clear:both"></div>                 
                <br />  
                <center><div id="order_table">  
              
                     <table  class="table table-striped table-bordered" id="employee_data">  
                          <tr>  
                               <th width="5%">ID</th>  
                               <th width="30%">Grn No.</th>  
                               <th width="10%">Product no</th>  
                               <th width="40%">Product name</th>  
                               <th width="22%">Party name</th>  
                               <th width="30%">party code</th>  
                               <th width="10%">quantity</th>  
                               <th width="40%">rate</th>  
                               <th width="22%">amount</th>  
                               <th width="30%">gst</th>  
                               <th width="10%">gst amount</th>  
                               <th width="40%">total amount</th>  
                               <th width="22%">received qty</th>  
                               <th width="22%">new total</th>
                               <th width="22%">date</th>  
                          </tr>  
                     <?php  
                     while($row = mysqli_fetch_array($result))  
                     {  
                     ?>  
                          <tr>  
                               <td><?php echo $row["id"]; ?></td>  
                               <td><?php echo $row["grn_no"]; ?></td>  
                               <td><?php echo $row["productno"]; ?></td>  
                               <td><?php echo $row["productname"]; ?></td>  
                               <td><?php echo $row["party_name"]; ?></td>  
                               <td><?php echo $row["party_code"]; ?></td>  
                               <td><?php echo $row["quantity"]; ?></td>  
                               <td><?php echo $row["rate"]; ?></td>  
                               <td><?php echo $row["amount"]; ?></td>  
                               <td><?php echo $row["gst"]; ?></td>  
                                <td><?php echo $row["gst_amount"]; ?></td>  
                               <td><?php echo $row["total_amount"]; ?></td>  
                               <td><?php echo $row["received_qty"]; ?></td> 
                               <td><?php echo $row["new_total"]; ?></td>  
                                 <td><?php echo $row["date1"]; ?></td>  
                                 
                          </tr>  
                     <?php  
                     }  
                     ?>  
                     </table> </center> 
                </div>  


           </div>  
      </body>  
 </html>  
 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"filter.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

              </div>  
            </div> 
          </div>
              <!-- /.table-responsive -->
        </div>
            
      </div>
          <!-- /.box -->
    </section>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

