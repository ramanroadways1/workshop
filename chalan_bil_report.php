<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">

     <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Invoice Detils</h3>

        
        </div>
      <button id="export">Export Now</button>
          <div style="overflow-x:auto;">
              <table class="table table-hover" id="tableData" border="4";>
           
                <tbody>
                  <tr class="table-active">
                      
                        <td>GRN Numbur</td>
                        <td >Purchase Order</td>
                        <td>Party Name</td> 
                        <td>Chalan File</td>
                        <td>Chalan Numbur</td> 
                        <td>Invoice File</td>
                        <td >Invoice Numbur</td>

                         
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $demo=$_POST['productno'];
              $val=$_POST['party_name'];

              // echo $demo=$_POST['id'];
             
              $show1 = "SELECT * from `files_upload` where party_name='$val' and productno='$demo'";
              $result = $conn->query($show1);

              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) {
                  
                    $grn_no = $row['grn_no'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                    $challan_no = $row['challan_no'];
                    $invoice_file = $row['invoice_file'];
                    $invoice_no = $row['invoice_no'];
                    $challan_file = $row['challan_file'];
                    $challan_file1=explode(",",$challan_file);
                    $count3=count($challan_file1);

                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                       <td><?php echo $grn_no; ?></td> 
                       <td><?php echo $purchase_order; ?></td>
                       <td><?php echo $party_name; ?></td>

                      <!-- <td><?php echo $challan_file;?></td> -->
                      <td><?php echo $challan_file; ?><a href="../vendor_may/upload_invoice/<?php echo $challan_file?>" ><img src="<?php echo $challan_file?>" height=30px;></a>
                      </td>
                      <td><?php echo $challan_no; ?></td>
                      <td><?php echo $invoice_file; ?></td>
                      <td><?php echo $invoice_no; ?></td>

                    </tr></div></div>
                <?php 
                      // $id_customer++;
                    }  echo "<tr>

                  </tr>";
                  } else {
                       echo "Truck not add";
                    exit();
                  }
                  ?>

              </table>
                <center><a href="chanlan_bill.php">
    <button type="button">
        Back
    </button>
</a></center> 
            </div>

          </div>
        </section>
      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
  $(".clickable-row").click(function() {
  window.open = $(this).data("href");
  });
});


</script>
<script type="text/javascript">
      document.getElementById('export').onclick=function(){
        var tableId= document.getElementById('tableData').id;
        htmlTableToExcel(tableId, filename = '');
    }
   var htmlTableToExcel= function(tableId, fileName = ''){
    var excelFileName='excel_table_data';
    var TableDataType = 'application/vnd.ms-excel';
    var selectTable = document.getElementById(tableId);
    var htmlTable = selectTable.outerHTML.replace(/ /g, '%20');
    
    filename = filename?filename+'.xls':excelFileName+'.xls';
    var excelFileURL = document.createElement("a");
    document.body.appendChild(excelFileURL);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', htmlTable], {
            type: TableDataType
        });
        navigator.msSaveOrOpenBlob( blob, fileName);
    }else{
        
        excelFileURL.href = 'data:' + TableDataType + ', ' + htmlTable;
        excelFileURL.download = fileName;
        excelFileURL.click();
    }
}
</script>

</body>
</html>

</body>
</html>
