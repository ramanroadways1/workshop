
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Goods Received Notes</h3>
        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
           <?php $username =  $_SESSION['username'];  ?>
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size: 12px; width: 100%">  
                  <thead>  
                       <tr>  
                        <th style="display: none;" class="col-md-2">Id</th>
                        <th class="col-md-2">Purchase order</th>
                           <!--  <th class="col-md-2">Purchase order</th> -->
                        <th class="col-md-2">Total Product</th>
                        <!-- <th class="col-md-2">Product Name</th> -->
                        <th class="col-md-2">Party name</th>
                        <th class="col-md-2">Party Code</th>
                        <th style="display: none;" class="col-md-2">Quantity</th>
                         <th class="col-md-2">Date/Time</th>
                        </tr>  
                  </thead>  
                  <?php   
                   include("connect.php");
                    $query = "SELECT date_purchase_key.id , purchase_order, party_name, party_code, count(productno) as productno, GROUP_CONCAT(productname) as productname, date1, GROUP_CONCAT(rate) as rate, GROUP_CONCAT(rate_master) as rate_master , GROUP_CONCAT(quantity) as quantity, GROUP_CONCAT(amount) as amount , GROUP_CONCAT(gst) as gst,GROUP_CONCAT(gst_amount) as gst_amount,GROUP_CONCAT(total) as total,date1 FROM date_purchase_key INNER JOIN  insert_po ON insert_po.key1=date_purchase_key.key1 WHERE approve_status='1'  and date_purchase_key.username ='$username' AND insert_po.username = '$username' GROUP by purchase_order order by date_purchase_key.id ASC ";
                  $result = mysqli_query($conn, $query);
                  if(mysqli_num_rows($result) > 0)
                  {
                  $id_customer = 0;
                    while($row = mysqli_fetch_array($result))
                    { 
                    $id = $row["id"];
                    $purchase_order = $row["purchase_order"];
                    $productno = $row["productno"];
                    $productname = $row['productname'];
                    $party_name = $row["party_name"];
                    $party_code = $row["party_code"];
                    $rate = $row["rate"];
                    $quantity = $row["quantity"];
                    $amount=$row["amount"];
                    $gst=$row["gst"];
                    $gst_amount=$row["gst_amount"];
                    $total = $row["total"];
                    $timestamp1 = $row['date1'];
                    
                  ?>
                       <tr> 
                           <td style="display: none;"><?php echo $id?>
                            <input type="hidden" name="id[]" value='<?php echo $id; ?>'>
                          </td>
                          <td>
                             <form method="POST" action="show_grn_detail.php">
                              <input type="hidden" name="id" value='<?php echo $id; ?>'>
                              <input type="submit"  name="purchase_order" value="<?php echo $purchase_order; ?>" class="btn btn-primary btn-sm" >
                            </form>
                          </td>
                          <!-- <td class="col-md-2"><?php echo $purchase_order?> <br>
                            <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>

                          </td> -->
                          <td class="col-md-2"><?php echo $productno?> <br>
                            <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>

                        <!--   </td>
                           <td ><?php echo $productname?>
                            <input type="hidden" name="productname[]" value='<?php echo $productname; ?>'>
                          </td> -->
                          <td><?php echo $party_name?>
                            <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                          </td>
                          <td><?php echo $party_code?>
                            <input type="hidden" name="party_code[]" value='<?php echo $party_code; ?>'>
                          </td>
                          <td style="display: none;"><?php echo $quantity?>
                            <input type="hidden" name="quantity[]" value='<?php echo $quantity; ?>'>
                          </td>
                           <td><?php echo $timestamp1?>
                            <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                          </td>
                       </tr>  
                  <?php   
                      $id_customer++;
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

