<?php

 $curl = curl_init();
 
 curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://rrpl.online/RRPL_API/vendor_payment.php',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
    
    "vou_no":"621310384527",
    "branch":"VISALPUR",
    "company":"RAMAN_ROADWAYS",
    "amount":"100",
    "ac_holder":"TEST AC HOLDER",
    "ac_no":"TEST AC HOLDER",
    "bank_name":"TEST AC HOLDER",
    "ifsc":"TEST AC HOLDER",
    "pan_no":"TEST AC HOLDER",
    "payment_date":"2021-07-23",
    "vou_date":"2021-07-23"
    }',
    CURLOPT_HTTPHEADER => array(
      'Content-Type: text/plain'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;
    // On Success response :

    // {
    //   "status":"SUCCESS",
    //   "response":"Payment Request Inserted Successfully.",
    //   "crn":"RR-V21000001"
    // }

    // Error Response :
    // {
    //   "status":"error",
    //   "errormsg":"Invalid Voucher date. Acceptable format is YYYY-MM-DD"
    // }