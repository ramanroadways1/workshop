<?php
//fetch.php
include('connect.php');
$columns = array('id', 'productno');

$query = "SELECT * FROM rate ";

if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE productno LIKE "%'.$_POST["search"]["value"].'%" 
 
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'group BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'group BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="id">' . $row["id"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="productno">' . $row["productno"] . '</div>';
/* $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="party_name">' . $row["party_name"] . '</div>';
*/
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="rate">' . $row["rate"] . '</div>';

  /*$sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="lock_unlock">' . $row["lock_unlock"] . '</div>';
 */
 


 $sub_array[] = '<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row["id"].'">Delete</button>';
 
 $data[] = $sub_array;
}

function get_all_data($conn)
{
 $query = "SELECT * FROM product";
 $result = mysqli_query($conn, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
