

<!DOCTYPE html>
<html>
<head>

    <?php include("header.php"); ?>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <?php include('ho_aside.php');?>

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
<!--   <form action="insert_approve_invoice.php" method="post"> -->
  <div class="content-wrapper">
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
       Payment Approval(Head Office)</h3>
      </div>
    <div id="result11"></div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
            
                <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%;">  
                  <thead>  

                       <tr> 
                            <th style="display: none;">ID</th>
                            <td>Grn No</td>
                            <td style="display: none;">GRN No</td>
                            <td style="display: none;">Purchase Order</td> 
                            <td>Party Name</td>
                            <td>Comapany</td>
                            <td style="width: 100px;">Account Detail</td>
                            <td>Other Amount</td>
                            <td>GRN Date</td>
                            <td>Payment Date</td>
                            <td>Challan Bill</td>
                            <td>Invoice Bill</td>
                            <td style="display: none;">Time</td>
                            <td>Payment</td>
                            <td>TDS Amount</td>
                            <td>Other</td> 
                            <td>Final Payment</td> 
                            <td>Remark H.o</td>
                            <td >Payment Method</td>
                            <td>Cheque No</td>
                            <td>Bank Name</td>
                            <td>Cheque Acc No</td>
                            <td>Cheque Date</td>
                            <td>Approve</td> 
                       </tr>  

                  </thead>  
                  <?php  
                  include("connect.php");
                  $show = "SELECT * FROM insert_pay_invoice where  length(invoice_file) > 10  and approve='1' group by grn_no order by id DESC";
                  $result = $conn->query($show);

                  if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { 
                    $id = $row['id'];
                    $grn_no = $row['grn_no'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                    $acc_holder_name = $row['acc_holder_name'];
                    $mobile_no = $row['mobile_no'];
                    $username = $row['username'];
                    $pan = $row['pan'];
                    $gstin = $row['gstin'];
                    $bank_name = $row['bank_name'];
                    $payment = $row['payment'];
                    $remark = $row['remark'];
                    $payment_date = $row['payment_date'];
                    $other_amount = $row['other_amount'];
                    $invoice_no = $row['invoice_no'];
                    $invoice_file = $row['invoice_file'];
                    $invoice_file1=explode(",",$invoice_file);
                    $count4=count($invoice_file1);
                    $timestamp1 = $row['timestamp1'];
                    $grn_date = $row['grn_date'];
                    $company = $row['company'];
                    $acc_no = $row['acc_no'];
                    $ifsc_code = $row['ifsc_code'];

                    $sum2=mysqli_query($conn,"SELECT * FROM files_upload where  grn_no='$grn_no'  group by grn_no ");
                    $row2=mysqli_fetch_array($sum2);

                    $challan_no = $row2['challan_no'];
                    $challan_file = $row2['challan_file'];
                    $challan_file1=explode(",",$challan_file);
                    $count5=count($challan_file1);

                  ?>
                       <tr> 
                         <td>
                           <form method="POST" action="show_grn_from_ho.php" target="_blank">
                              <input type="submit"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-primary btn-sm">
                           </form>
                        </td>
                   
                          <td style="display: none;"><?php echo $id; ?>
                              <input type="hidden" name="id" id="id<?php echo $id; ?>" value='<?php echo $id; ?>' >

                              <input type="hidden" name="grn_no" id="grn_no<?php echo $id; ?>" value='<?php echo $grn_no; ?>' >

                              <input type="hidden" name="username"  value='<?php echo $username; ?>' >
                          </td>

                         <td style="display: none;"><?php echo $grn_no; ?>
                
                        <td style="display: none;"><?php echo $purchase_order; ?>
                          <input type="hidden" name="purchase_order"  id="purchase_order<?php echo $id; ?>" value='<?php echo $purchase_order; ?>'>
                        </td> 
                        <td><?php echo $party_name; ?>
                          <input type="hidden" name="party_name"  id="party_name<?php echo $id; ?>" value='<?php echo $party_name; ?>'>
                        </td>

                       <td><?php echo $company; ?>
                        <input type="hidden"  name="company"  id="company<?php echo $id; ?>" value='<?php echo $company; ?>'>
                        </td>
                     
                      <td>
                            Bank:<?php echo $bank_name; ?>
                          <input type="hidden"  name="bank_name" id="bank_name<?php echo $id; ?>" value="<?php echo $bank_name ?>">
                              <?php  echo "<br>"; ?>
                               AccNO:<?php echo $acc_no; ?>
                            <input type="hidden"  name="acc_no"  id="acc_no<?php echo $id; ?>" value="<?php echo $acc_no ?>">
                            <input type="hidden" name="acc_holder_name"  id="acc_holder_name<?php echo $id; ?>" value="<?php echo $acc_holder_name ?>">
                              <?php  echo "<br>"; ?>
                           Mobile No:<?php echo $mobile_no; ?>
                          <input type="hidden" name="mobile_no" id="mobile_no<?php echo $id; ?>"  value="<?php echo $mobile_no ?>">
                           
                          <input type="hidden" name="pan" id="pan<?php echo $id; ?>"  value="<?php echo $pan ?>">
                             
                          IFSC:<?php echo $ifsc_code; ?>
                          <input type="hidden" name="gstin" id="gstin<?php echo $id; ?>"  value="<?php echo $gstin ?>">
                          <input type="hidden" name="ifsc_code" id="ifsc_code<?php echo $id; ?>" value="<?php echo $ifsc_code ?>">
                          <input type="hidden" name="productno" id="productno<?php echo $id; ?>" value='<?php echo $productno; ?>'>
                      </td>
                       
                        <td>Other:<?php echo $other_amount; ?>
                          <input type="hidden" name="other_amount"  value="<?php echo $other_amount ?>">
                         
                          <?php echo "<br>"; ?>
                          Remark:<?php echo $remark; ?>
                         <input type="hidden" name="remark" value='<?php echo $remark; ?>'>
                       </td>
                      

                       <td><?php echo $grn_date; ?>
                        <input type="hidden" name="grn_date" id="grn_date<?php echo $id; ?>" value='<?php echo $grn_date; ?>'>
                      </td>

                       <td><?php echo $payment_date; ?>
                        <input type="hidden" name="payment_date"  id="payment_date<?php echo $id; ?>" value='<?php echo $payment_date; ?>'>
                      </td>

                       <td>
                        Challan No:<?php echo $challan_no; ?>
                         <?php
                          if (strlen($challan_file) > 15) {
                         for($j=0; $j<$count5; $j++){
                         
                          ?>
                           <a href="<?php echo $challan_file1[$j]; ?>" target="_blank">Challan <?php echo $j+1; ?></a>
                           <input type="hidden" name="challan_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $challan_file; ?>'>
                           <?php
                         }
                          }  
                     else{
                      echo "no file";
                     }
                        ?>
                      </td>
                      
                      <td>
                        Invoice No:<?php echo $invoice_no; ?>
                         <?php
                          if (strlen($invoice_file) > 15) {
                         for($j=0; $j<$count4; $j++){
                         
                          ?>
                           <a href="<?php echo $invoice_file1[$j]; ?>" target="_blank">Invoice <?php echo $j+1; ?></a>
                           <input type="hidden" name="invoice_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $invoice_file; ?>'>
                           <?php
                         }
                          }  
                     else{
                      echo "no file";
                     }
                        ?>
                      </td>
                    
                      <td style="display: none;"><?php echo $timestamp1; ?>
                        <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                      </td>

                     <td><?php echo $payment; ?>
                        <input type="hidden" name="payment" id="payment<?php echo $id; ?>" value='<?php echo $payment; ?>'>
                      </td>
                     <td>
                      <input type="Number" ondrop="return false;" onpaste="return false;" name="tds" oninput="sum1(<?php echo $id; ?>);" required="required" placeholder="Tds Amount" id="tds<?php echo $id; ?>" />
                    </td> 

                    <td>
                      <input type="Number" ondrop="return false;" onpaste="return false;" name="other_h_o" oninput="sum1(<?php echo $id; ?>);" required="required"  placeholder="Other" id="other_h_o<?php echo $id; ?>" />
                    </td> 

                     <td>
                     <input type="Number" ondrop="return false;" onpaste="return false;" name="final_amount" readonly="readonly"  placeholder="Final Amount" id="final_amount<?php echo $id; ?>" />
                     </td> 

                      <td>
                        <textarea style="height: 25px;" ondrop="return false;" onpaste="return false;" id="remark_h_o<?php echo $id; ?>" oninput="chk_string(<?php echo $id;; ?>)" required="required" name="remark_h_o"  placeholder="Remark H.O" ></textarea>
                     </td> 
                      <script>
                                  function sum1(id)
                                  {
                                   var payment = Number($('#payment'+id).val()); 
                                    var payment1 =Math.round(payment);
                                   var tds = Number($('#tds'+id).val());
                                   var tds1 =Math.round(tds);
                                   var other_h_o = Number($('#other_h_o'+id).val()); 
                                   var other_h_o1 =Math.round(other_h_o);
                                   if (tds1<0) {
                                       $('#tds'+id).val('');
                                        $('#final_amount'+id).val('');
                                    }
                                    if (other_h_o1<0) {
                                        $('#other_h_o'+id).val('');
                                         $('#final_amount'+id).val('');
                                    }else{

                                    var deduct_Amt = tds1+other_h_o1;
                                    var final_amount1 = payment1- deduct_Amt;
                                    if ((final_amount1<0)&&(deduct_Amt>payment1)) {
                                      alert("Payment amount Not Varified");
                                       $('#tds'+id).val('');
                                        $('#other_h_o'+id).val('');
                                         $('#final_amount'+id).val('');
                                    }else{
                                      if (final_amount1 !==0) {  
                                        $('#final_amount'+id).val((final_amount1).toFixed(2));
                                        //$('#final_amount'+id).val((final_amount1).toFixed(2));

                                       // var_dump(roundDown(final_amount1, 0.5));
                                    }else{
                                       $('#tds'+id).val('');
                                        $('#other_h_o'+id).val('');
                                         $('#final_amount'+id).val('');
                                    }
                                   }

                                    }
                                  }
                          </script>
                                  
                              <td><!--  -->
                                <select style="height: 25px;" onchange="SearchBy('<?php echo $id; ?>');SearchBy(this.value);"   id="pay_type<?php echo $id; ?>" name="pay_type">
                                    <option value="">---Select Bill Type---</option> 
                                     <option value="neft">Neft</option>
                                     <option value="cheque">Cheque</option>
                                  </select>
                              </td> 
                          <script type="text/javascript">
                            function SearchBy(id)
                            { 
                              
                              var elem =  document.getElementById("pay_type"+id).value;
                              if(elem=='neft')
                                      {
                                        $('#cheque_no'+id).css('display','none'); 
                                        $('#cheque_bank_name'+id).css('display','none'); 
                                        $('#cheque_date'+id).css('display','none'); 
                                        $('#cheque_acc_no'+id).css('display','none'); 

                                        document.getElementById("cheque_no"+id).required = false;
                                        document.getElementById("cheque_bank_name"+id).required = false;
                                        document.getElementById("cheque_date"+id).required = false; 
                                        } 
                                     else if(elem=='cheque')
                                      {
                                        $('#cheque_no'+id).css('display','block'); 
                                        $('#cheque_bank_name'+id).css('display','block'); 


                                        $('#cheque_date'+id).css('display','block');
                                        $('#cheque_acc_no'+id).css('display','block');

                                        $("#cheque_date"+id).datepicker({
                                               dateFormat: "yy-mm-dd",
                                            });
                                          
                                        document.getElementById("cheque_no"+id).required = true;
                                        document.getElementById("cheque_bank_name"+id).required = true;
                                        document.getElementById("cheque_date"+id).required = true; 
                                      }
                                      else{
                                     
                                      }
                                              
                                }
                                
                            </script>
                              <td >
                                <input style="display: none;"  type="Number" ondrop="return false;" onpaste="return false;" name="cheque_no" placeholder="Cheque No" id="cheque_no<?php echo $id; ?>" />
                              </td>
                              <td>
                                <textarea style="display: none;height: 26px;" ondrop="return false;" onpaste="return false;" oninput="chk_string(<?php echo $id;; ?>)" name="cheque_bank_name" placeholder="Bank Name" id="cheque_bank_name<?php echo $id; ?>" ></textarea>
                             </td>
                         <script type="text/javascript">
                          
                             function chk_string(val){
                              var cheque_bank_name  = document.getElementById('cheque_bank_name'+val).value;
                                  var cheque_bank_name1 = cheque_bank_name.replace("'", "");
                                  $('#cheque_bank_name'+val).val(cheque_bank_name1);

                                  var remark_h_o  = document.getElementById('remark_h_o'+val).value;
                                  var remark_h_o1 = remark_h_o.replace("'", "");
                                  $('#remark_h_o'+val).val(remark_h_o1);
                                    }
                         </script>

                               <td>
                                <input  style="display: none;" ondrop="return false;" onpaste="return false;" type="Number" name="cheque_acc_no" placeholder="Cheque Account No" id="cheque_acc_no<?php echo $id; ?>" />
                              </td>

                              <td>
                                <input  style="display: none;" ondrop="return false;" onpaste="return false;" type="text" name="cheque_date" placeholder="Cheque Date" id="cheque_date<?php echo $id; ?>" />
                              </td> 
                             <!--  <script type="text/javascript">
                                 $(document).ready(function() {
                                    $("#cheque_date").datepicker({
                                       dateFormat: "yy-mm-dd",
                                    });
                                  });
                              </script> -->

                              <td>
                                <button type="button" id="Submit_me" class="btn btn-success btn-sm" onclick="addComplainFunc(<?php echo $id; ?>)">Approve</button>
                              </td>
                  

                   <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var id = val;
                                  var grn_no = $('#grn_no'+val).val()
                                  var acc_no = $('#acc_no'+val).val();
                                  var acc_holder_name = $('#acc_holder_name'+val).val();
                                  var pan = $('#pan'+val).val();
                                  var grn_date = $('#grn_date'+val).val();
                                  var payment_date = $('#payment_date'+val).val();
                                  var payment = $('#payment'+val).val();
                                  var company = $('#company'+val).val();
                                  var bank_name = $('#bank_name'+val).val();
                                  var ifsc_code = $('#ifsc_code'+val).val();
                                  var mobile_no = $('#mobile_no'+val).val();
                                  var gstin = $('#gstin'+val).val();
                                  var tds = $('#tds'+val).val();
                                  var other_h_o = $('#other_h_o'+val).val();
                                  var remark_h_o = $('#remark_h_o'+val).val();
                                  var final_amount = $('#final_amount'+val).val();
                                  var pay_type = $('#pay_type'+val).val();
                                  var cheque_no = $('#cheque_no'+val).val();
                                  var cheque_bank_name = $('#cheque_bank_name'+val).val();
                                  var cheque_date = $('#cheque_date'+val).val(); 
                                  var cheque_acc_no = $('#cheque_acc_no'+val).val(); 
                                  var username = '<?php echo $username ?>'

                               if(tds!='' && other_h_o!='')
                             { 
                              if(remark_h_o!='' )
                                   { 
                                      if (pay_type!="") {
                                       if (pay_type=="cheque") {
                                        if (cheque_no!='' && cheque_bank_name!=''&& cheque_date!=''&& cheque_acc_no!='') {
                                          if (confirm("Are You Sure To Approve this GRN With Cheque?"))
                                               { $.ajax({
                                                     type:"post",
                                                     url:"payment_Ho_with_cheque.php",
                                            data:'grn_no='+grn_no + '&username='+username+ '&acc_no='+acc_no+ '&acc_holder_name='+acc_holder_name+ '&pan='+pan+ '&grn_date='+grn_date+ '&payment_date='+payment_date+ '&final_amount='+final_amount+ '&company='+company+ '&bank_name='+bank_name+ '&ifsc_code='+ifsc_code+ '&mobile_no='+mobile_no+ '&gstin='+gstin+ '&id='+id+ '&tds='+tds+ '&other_h_o='+other_h_o+ '&remark_h_o='+remark_h_o+ '&cheque_no='+cheque_no+ '&cheque_bank_name='+cheque_bank_name+ '&cheque_date='+cheque_date+ '&cheque_acc_no='+cheque_acc_no,
                                                      success:function(data){
                                                          $('#result11').html(data);
                                                        }
                                                      });
                                                 }
                                        }else{
                                            alert("fill Complete Cheque Detail");
                                        }
                                       }
                                       else{
                                           
                                            if (confirm("Are You Sure To Approve this GRN With NEFT?"))
                                               { 
                                               $.ajax({
                                                     type:"post",
                                                     url:"payment_H_O_approve.php",
                                            data:'grn_no='+grn_no + '&username='+username+ '&acc_no='+acc_no+ '&acc_holder_name='+acc_holder_name+ '&pan='+pan+ '&grn_date='+grn_date+ '&payment_date='+payment_date+ '&final_amount='+final_amount+ '&company='+company+ '&bank_name='+bank_name+ '&ifsc_code='+ifsc_code+ '&mobile_no='+mobile_no+ '&gstin='+gstin+ '&id='+id+ '&tds='+tds+ '&other_h_o='+other_h_o+ '&remark_h_o='+remark_h_o,

                                                      success:function(data){
                                                       $('#result11').html(data);
                                                        }
                                                      });
                                                 }
                                       }
                                     }else{
                                      alert("Select Payment Method");
                                     }
                                 }else{
                                  alert("Fill Remark");
                                 }

                               }else{
                                alert("Fill Tds And Other Amount");
                               }
                        }    
                    </script>

                    <script>
                      function DeleteModal(grn_no)
                      {
                        var grn_no = grn_no;
                        //alert(grn_no);
                         var username = '<?php echo $username ?>'

                       
                        if (confirm("Do you want to delete this grn?"))
                        {
                          if(grn_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_grn_from_payment.php",
                                  data:'grn_no='+grn_no + '&username='+username ,
                                  success: function(data){
                                    alert(data)
                                           location.reload(); 
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                  </script>
                    <div id="result22"></div>
                    </tr>  
                   <?php 
                        $id_customer++;
                      } 
                    } else {
                        echo "0 results";
                    }
                  ?>
                </table>  
               <!-- 

               <center>
                  <input type="submit" class="btn btn-primary" name="action[]" value="Approved">
               </center>
               
                 -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
<!--   </form> -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

