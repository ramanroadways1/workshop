<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once 'database.php';
include_once 'api_fetch_data.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product = new Product($db);
 
// query products
$stmt = $product->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "vou_no" => $grn_no,
            "party_name" => $party_name,
             "branch" => $username,
            "company" => $company,
            "amount" => $payment,
             "bank_name" => $bank_name,
            "pan" => $pan,
            "ifsc_code" => $ifsc_code,
             "mobile_no" => $mobile_no,
            "vou_date" => $grn_date,
            "payment_date" => $payment_date,
            "ac_holder" => $acc_holder_name,
             "ifsc" => $ifsc_code,
             "pan_no" => $pan,
            "ac_no" => $acc_no
        );
        print_r($product_item);
    }

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://rrpl.online/RRPL_API/vendor_payment.php?action=VENDOR_PAY&vou_no='$grn_no'&company=company&amount=amount&ac_holder=ac_holder&ac_no=ac_no&bank_name=bank_name&ifsc=ifsc&pan_no=pan_no&payment_date=payment_date&vou_date=vou_date&branch=branch",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "Accept: */*",
    "Accept-Encoding: gzip, deflate",
    "Cache-Control: no-cache",
    "Connection: keep-alive",
    "Postman-Token: f2705bfb-a102-4d55-9357-544286c382a5,503b0416-3c51-4a3d-9c2b-e24235a2a265",
    "Referer: http://rrpl.online/RRPL_API/vendor_payment.php?action=VENDOR_PAY&vou_no=12&company=rrpl&amount=1000&ac_holder=abc&ac_no=12213&bank_name=sbi&ifsc=1234&pan_no=wer34234&payment_date=2019-11-04&vou_date=2019-11-05&branch=visalpur",
    "User-Agent: PostmanRuntime/7.19.0",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
}