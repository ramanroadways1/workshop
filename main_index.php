<!DOCTYPE html>
<html>
<head>
  <?php 
           // session_start();
             include("header.php");
             include("aside_main.php");
            $username =  $_SESSION['username'];
            $employee =  $_SESSION['employee'];
            $empcode =  $_SESSION['empcode'];
          
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <div class="content-wrapper">
    <section class="content-header">
        <h1>
          <!--Inventory-->
          <!--<small>Control panel</small>-->
        </h1>
    </section>
    <section class="content">
      <div class="row">

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-yellow"><i class="fa  fa-cube"></i></span>

            <div class="info-box-content">
             <!--  <span class="info-box-text">Total <a href="show_product_master.php">product(Master)</a></span> -->

             <span class="info-box-text">Total <a href="product_view_dashboard.php">product(Master)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT productno FROM product ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="fa  fa-cubes"></i></i></span>

            <div class="info-box-content">
             <!--  <span class="info-box-text">Total <a href="show_product.php">product(Inventory)</a></span> -->
             <span class="info-box-text">Total <a href="show_dashboard_inventry_view.php">product(Inventory)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT productno FROM product_inventory ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>
            <!--info-box-content-->
          </div>
          <!--info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa  fa-users"></i></span>

            <div class="info-box-content">
              <!-- <span class="info-box-text">Total<a href="show_party.php">party(master)</a></span> -->
              <span class="info-box-text">Total<a href="party_view_dashboard.php">party(master)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT party_name FROM party";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              
              </span>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"><i class="fa  fa-file-text-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="grn_detail_by_date.php">grn Report</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT grn_no FROM grn where username='$username'  group by grn_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"><i class="fa  fa-files-o"></i></span>

            <div class="info-box-content">
              <!-- <span class="info-box-text">Total <a href="po_grn_detail.php">Created Grn On Po</a></span> -->

               <span class="info-box-text">Total <a href="po_view.php">Created Grn On Po</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

             /* $sql="SELECT grn_no FROM grn where username='$username'  group by grn_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);*/
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-red"><i class="fa  fa-clipboard"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="product_record_in_grn.php">Received Product </a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql3="SELECT productno FROM grn where username='$username' group by productno";

              if ($result3=mysqli_query($conn,$sql3))
                {
                $rowcount=mysqli_num_rows($result3);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result3);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="fa  fa-file-text"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="grn_detail_filter.php">Grn (Date Wise)</a></span>
              <span class="info-box-number">
             <!--  <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql3="SELECT productno FROM grn where username='$username' ";

              if ($result3=mysqli_query($conn,$sql3))
                {
                $rowcount=mysqli_num_rows($result3);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result3);
                }
              mysqli_close($conn);
              ?><br> -->
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-yellow"><i class="fa  fa-bus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="direct_issue_grn_inventory.php">Direct Issue Grn</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql3="SELECT * FROM files_upload where username='visalpur' and type_of_issue !='' GROUP by grn_no";

              if ($result3=mysqli_query($conn,$sql3))
                {
                $rowcount=mysqli_num_rows($result3);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result3);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

      </div>
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
