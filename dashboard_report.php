<?php  
include('header.php');
include('aside_main.php');
include('connect.php');

?>
<head>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.1.0/css/searchBuilder.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.1.0/css/dataTables.dateTime.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.1.0/js/dataTables.searchBuilder.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/datetime/1.1.0/js/dataTables.dateTime.min.js"></script>  -->
  


<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
</script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js">
</script>
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<script>
$(document).ready(function() {
$('#mltislct').multiselect({
includeSelectAllOption: true,
enableFiltering: true,
enableCaseInsensitiveFiltering: true,
filterPlaceholder:'Search Here..'
});
});
</script>
<div class="wrapper">
 <form method="post" action="insert_party.php" autocomplete="off">
  <div class="content-wrapper">
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
          <div class="row" style="height: 10px; padding: 10px 5px 10px 5px">
    <form>

<div class="row">  
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="box box-default  with-border">
<div class="box-header with-border" style="text-align:center">     
<h4 class="box-title small-title pull-left">Search Criteria </h4>         

 <div class="box-tools pull-right">                    
<button type="button" class="btn btn-box-tool btn-default  btn-flat" data-widget="collapse">
<i class="fa fa-minus"></i></button>
</div>
</div>
<div class="box-body"  >
<table width="100%" border="0" cellpadding="0" cellspacing="0"  align="center" > 
<!--------------------------------------->
        <div class="col-md-12"style=" margin-top: 20px;">
          <div class="form-group">
           <div class="col-md-4">
           <label class="a">Model</label>
                 <select name="model" style="width: 250px;" id="model" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Model</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT dairy.own_truck.model FROM dairy.own_truck  group by model");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['model'])?>" >
                        <?php 
                              echo ($row['model']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
          </div>

          <div class="bs-multiselect">
<select id = "mltislct" multiple = "multiple">
<option value = " HTML"> HTML
</option>
<option value = "CSS"> CSS
</option>
<option value = "Bootstrap"> Bootstrap
</option>
<option value = "Angular"> Angular </option>
<option value = "JavaScript"> JavaScript </option>
<option value = "jquery"> jquery </option>
</select>
</div>

          <div class="form-group">
            <div class="col-md-4">
           <label class="a">Truck Number</label>
                 <select name="model" style="width: 250px;" id="model" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Model</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT dairy.own_truck.tno FROM dairy.own_truck  ");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['tno'])?>" >
                        <?php 
                              echo ($row['tno']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
          </div>
          </div>
           <div class="form-group">
            <div class="col-md-4">
           <label class="a">Truck Wheel</label>
                 <select name="model" style="width: 250px;" id="model" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Model</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT dairy.own_truck.wheeler  FROM dairy.own_truck group by wheeler");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['wheeler '])?>" >
                        <?php 
                              echo ($row['wheeler']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
          </div>
          </div>
        </div>
          <div class="col-md-12" style="margin-top: 30px;">
          <div class="form-group row">
          <div class="form-group">
          <div class="col-md-2" >
          <label class="form-check-label" for="flexCheckDefault">
          Internal Job Card
          </label>
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
          </div>
          <div class="col-md-2" >
          <label class="form-check-label" for="flexCheckDefault">
          External Job Card
          </label>
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
          </div>
          </div>
          </div>
          </div>
          <div class="col-md-12" style="margin-top: 25px;">
          <div class="form-group row">
          <div class="form-group">
          <div class="col-md-2" >
          <b>Select Date</b>
          </div>
          <div class="col-md-2">  
          <input type="date" name="from_date" id="from_date" autocomplete="off" class="form-control" placeholder="From Date" />  
          </div>

        <div class="col-md-2">  
        <input type="date"  name="to_date" id="to_date" autocomplete="off" class="form-control" placeholder="To Date" />  
        </div>
          </div>
          </div>
          </div>
          </div>
            </div>
        </div>
         


<!--          <div class="col-md-12">
            <div class="form-group row">
            <div class="col-md-2" >

            </div>
            <div class="col-md-2">
            <label>Model</label>
            <select style=" width: 180px;" class="form-control" required id="search_product" name="search_product">
            <option value="">All</option>
            <option value="PNO">AMARON</option>
            <option value="PNAME">CAPRO CPR01</option>
            </select>
            </div>
            <div class="col-md-2">
            <label class="a">Status</label>
            <select style=" width: 180px;" class="form-control" required id="search_product" name="search_product">
            <option value="">---Select---</option>
            <option value=" New">New</option>
            <option value="Old">Old</option>
            <option value="Scrap">Scrap</option>
            </select>
            </div>
            <div class="col-md-2" >

            </div>
            <div class="col-md-2">
            <label class="a">Battery Type</label>
            <select style=" width: 180px;" class="form-control" required id="search_product" name="search_product">
            <option value="">---Select---</option>
            <option value=" New">New</option>
            <option value="Old">Old</option>
         
            </select>
            </div>
            <div class="col-md-2">
            <label class="a">Amp.</label>
            <input class="form-control" style="height: 25px; width: 180px;" id="ex1" type="text">
            </div>
            </div>
         </div> -->
  

    <div class="col-sm-offset-2 col-sm-8" style="margin-top: 30px;">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Search</button>
      <a href="party_view.php" class="btn btn-warning">Close</a>
    
     </center>
    </div>
</table>

    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">

<!--     <table id="contact-detail"  name="contact-detail" class="display nowrap" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>SrNo.</th>
        <th>Battery SrNo.</th>
        <th>Battery No. </th>
        <th>Volt</th>
        <th>Watt</th>
        <th>Chrzing Time</th>
        <th>Run After Charz</th>
        <th>Battery Type </th>
        <th>Model</th>
        <th>Amp. </th>
        <th>No. of Plates </th>
        <th>warranty</th>
        <th>Battry Status </th>
        <th>Invoice No. </th>
        <th>Date Of Purchase</th>
        <th>Vendor Name</th>
        <th>Amount</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tfoot>
       <tr>
        <th>SrNo.</th>
        <th>Battery SrNo.</th>
        <th>Battery No. </th>
        <th>Volt</th>
        <th>Watt</th>
        <th>Chrzing Time</th>
        <th>Run After Charz</th>
        <th>Battery Type </th>
        <th>Model</th>
        <th>Amp. </th> 
        <th>No. of Plates </th>
        <th>Invoice No.</th>
        <th>Battry Status </th>
        <th>Warranty </th>
        <th>Status </th>
        <th>Vendor Name</th>
        <th>Amount</th>
     </tr>
    </tfoot>
    </table> -->
    </div>

<!--     <script type="text/javascript">
              $(document).ready(function() {
            // Setup - add a text input to each footer cell
            $('#contact-detail tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
         
            // DataTable
            var table = $('#contact-detail').DataTable({
                initComplete: function () {
                    // Apply the search
                    this.api().columns().every( function () {
                        var that = this;
         
                        $( 'input', this.footer() ).on( 'keyup change clear', function () {
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        } );
                    } );    
                },        
                "processing": true,
                "scrollX": true,
            "serverSide": true,
            "ajax": "battry_masterfetch.php"
            });
         
        } );

    </script> -->

        </div>
    </div>
  </section>
</div></div></div></div>


</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</section>
</div>
</form>
</div>

</body>





