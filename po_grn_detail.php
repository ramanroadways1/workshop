
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

 <style type="text/css">
  @media print {

     #from_date,#to_date,#filter,#print,#heading{
    display: none;
  }
 header,footer,h3 {
    
    display: none !important;
  }
  body{
      margin-top: -2cm;
    margin-bottom: 0cm;
  }
 
/* #employee_data{
   page-break-after: always; 
 }*/

  
}
</style>
 

        
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
  <div class="box ">
      <div class="box-header with-border">
        <h3 class="box-title" id="heading" style="font-family: verdana; font-size:16px; ">G R N Detail by date</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
             <?php $username =  $_SESSION['username'];  
              ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">

            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <div id="order_table">
                <table id="employee_data" class="table table-striped table-bordered" style="font-family: verdana; font-size:13px; ">  
                  <thead>  
                       <tr>  
                        <th>Purchase Order</th>
                        <th>Purchase Order</th>
                         <th>Product Count</th>
                        <th>Back Date</th>
                         <th>P.o Date</th>
                       </tr>  
                  </thead>  
                   <?php
              include 'connect.php';

              $show = "SELECT date_purchase_key.date1,date_purchase_key.username,date_purchase_key.purchase_order,grn.grn_no,grn.back_date FROM date_purchase_key INNER join grn on grn.purchase_order = date_purchase_key.purchase_order where date_purchase_key.username='$username' and grn.username='$username' GROUP by purchase_order ORDER by grn.id  DESC";
              $result = $conn->query($show);

              if ($result->num_rows > 0) {
                 
                  while($row = $result->fetch_assoc()) {
                      $purchase_order = $row['purchase_order'];
                       $back_date = $row['back_date'];
                       $date1 = $row['date1'];
                     ?>
                       <tr> 
                          <td>
                             <form method="POST" action="po_grn_date_filter_detail_data.php">
                              <input type="submit"  name="purchase_order" value="<?php echo $purchase_order; ?>" class="btn btn-primary btn-sm" >
                            </form>
                          </td>
                          <td ><?php echo $purchase_order; ?></td>
                          <td>
                              <?php
                                   $sql2 = "SELECT count(productno) as productno FROM grn WHERE purchase_order='$purchase_order' and username ='$username' GROUP by purchase_order ORDER by id  DESC";
                                   $result2 = mysqli_query($conn, $sql2);
                                  if(mysqli_num_rows($result2) > 0)  
                                    {
                                      $row2 = mysqli_fetch_array($result2);
                                      $productno = $row2["productno"];
                                    }
                              ?>
                            </td>
                            <td><?php echo $back_date; ?></td>
                            <td><?php echo $date1; ?></td>
                        </tr>  
                  <?php 
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
         </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  <footer id="footer" class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"filter_grn_interval.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  

                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>


 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

