
<!DOCTYPE html>
<html>
<head>
 <?php 
    include("header.php");
    include("aside_main.php");
  ?>
     <link href="assets/font-awesome.min.css" rel="stylesheet">
      <script src="assets/jquery-3.5.1.js"></script>
      <script src="assets/jquery-ui.min.js"></script>
      <link href="assets/jquery-ui.css" rel="stylesheet"/>
      <link href="assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="assets/custom.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
  
                                    <th>Product No</th>
                                     <th>Product Name</th>
                                     <th>Company</th>
                                    <th>Available Quantity</th>
     
                            </tr>
                         </thead>
                        
                      </table>
                      <div id="dataModal" class="modal fade">
                         <div class="modal-dialog modal-lg">
                            <div class="modal-content" id="employee_detail">
                            </div>
                         </div>
                      </div>


                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "stoct_vendor_report_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV' 
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                              "criteria": [
                                  {
                                      "data": ' VEHICLE ',
                                      "condition": '=',
                                      "value": ['']
                                  }
                              ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=assets/loading.gif height=25> </center>"
                          },
                         "aaSorting": [],
                         
                         // },// "footerCallback": function ( row, data, start, end, display ) {
                         //    var api = this.api(), data;
                         //    // Remove the formatting to get integer data for summation
                         //    var intVal = function ( i ) {
                         //        return typeof i === 'string' ?
                         //            i.replace(/[\$,]/g, '')*1 :
                         //            typeof i === 'number' ?
                         //                i : 0;
                         //    };
                         //    // Total over all pages
                         //    total = api
                         //        .column( 6 )
                         //        .data()
                         //        .reduce( function (a, b) {
                         //            return intVal(a) + intVal(b);
                         //        }, 0 );
                         //    // Total over this page
                         //    pageTotal = api
                         //        .column( 6, { page: 'current'} )
                         //        .data()
                         //        .reduce( function (a, b) {
                         //            return intVal(a) + intVal(b);
                         //        }, 0 );
                         //    // Update footer (Total: '+ total +')
                         //    $( api.column( 6 ).footer() ).html(
                         //        ''+pageTotal.toFixed(2) +''
                         //    );
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          // $('title').html("Report");

                          // $("#getPAGE").submit(function(e) {  
                          //   e.preventDefault();
                          //   var data = $(this).serialize();
                          //   loadurl = "product_fetch.php".concat(data);
                          //   table.ajax.url(loadurl).load();  
                          // }); 

                         });     
                      </script> 
                     <!-- <div class="col-md-12">
                        <div id="getPAGEDIV"></div>
                     </div> -->
                  </div>
                  </div>
                  </div>
                  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.

    </footer>

  <div class="control-sidebar-bg"></div>
</div>

       <script src="assets/bootstrap.js"></script>

        <script src="assets/bootstrap.bundle.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="assets/jszip.min.js"></script>
        <script type="text/javascript" src="assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="assets/dataTables.searchBuilder.min.js"></script>

  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


 <!-- <th>ID</th>
          <th>Product Number</th>
          <th>Company Name</th>
          <th>Product Name</th> 
           <th>Product Type</th>
           <th>Unit</th>        
          <th>Group</th>
          <th>Sub Group</th>
          
          <th>Product Location(Rack Loction)</th> -->