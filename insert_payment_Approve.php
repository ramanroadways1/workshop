<?php
include ("connect.php");
//header("Location: approve_challan.php");
if (isset($_POST['submit']))
 { 
    $username = $conn->real_escape_string(htmlspecialchars($_POST['username']));
    $grn_no = $conn->real_escape_string(htmlspecialchars($_POST['grn_no']));
    $purchase_order = $conn->real_escape_string(htmlspecialchars($_POST['purchase_order']));
    $party_name = $conn->real_escape_string(htmlspecialchars($_POST['party_name']));
    $new_total = $conn->real_escape_string(htmlspecialchars($_POST['new_total']));
    $invoice_no = $conn->real_escape_string(htmlspecialchars($_POST['invoice_no']));
    $remain = $conn->real_escape_string(htmlspecialchars($_POST['remain']));
    $other_amount = $conn->real_escape_string(htmlspecialchars($_POST['other_amount']));
    $acc_holder_name = $conn->real_escape_string(htmlspecialchars($_POST['acc_holder_name']));
    $mobile_no = $conn->real_escape_string(htmlspecialchars($_POST['mobile_no']));
    $pan = $conn->real_escape_string(htmlspecialchars($_POST['pan']));
    $gstin = $conn->real_escape_string(htmlspecialchars($_POST['gstin']));
    $bank_name = $conn->real_escape_string(htmlspecialchars($_POST['bank_name']));

    $acc_no = $conn->real_escape_string(htmlspecialchars($_POST['acc_no']));
    $ifsc_code = $conn->real_escape_string(htmlspecialchars($_POST['ifsc_code']));
    $company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
    $remark = $conn->real_escape_string(htmlspecialchars($_POST['remark']));
    $payment_date = $conn->real_escape_string(htmlspecialchars($_POST['payment_date']));
    $date = date_default_timezone_set('Asia/Kolkata');
        
        $start_time = date(" Y-m-d g:i a");

        $insert_date  = date('Y-m-d');
   try{

    $conn->query("START TRANSACTION"); 
    //print_r($new_total);
    $sql = "INSERT into insert_pay_invoice(grn_no,purchase_order,party_name,new_total,invoice_no,payment,username,other_amount,mobile_no,pan,gstin,acc_no, ifsc_code,timestamp1,bank_name,remark,payment_date,invoice_file,grn_date,company,acc_holder_name) select '$grn_no','$purchase_order','$party_name','$new_total','$invoice_no','$remain','$username','$other_amount','$mobile_no','$pan','$gstin','$acc_no','$ifsc_code','$start_time','$bank_name','$remark','$payment_date',invoice_file,back_date,'$company','$acc_holder_name' FROM files_upload
    WHERE grn_no='$grn_no' and username='$username' order by grn_no desc limit 1";

    $sqld2 = $conn->query($sql);
    $sql3 = mysqli_query($conn,"UPDATE files_upload SET payment_date='$payment_date' WHERE grn_no='$grn_no' and username='$username'");
    $result2 = $conn->query($sql3);
    $conn->query("COMMIT");
    echo "
    <script>
    alert('Payment Done wait for Approve...');
    window.location.href='grn_for_payment_view.php';
    </script>";
    // exit();
    }

  catch (Exception $e) {
  //role back to using
    $conn->query("ROLLBACK");
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

            echo "<SCRIPT>
               alert('Error: Payment Not Approve');
               window.location.href='grn_for_payment_view.php';
            </SCRIPT>";



    $file_name = basename(__FILE__);        
    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name ','$username','$content')";
    if ($conn->query($sql) === FALSE) { 
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

  }
?>    