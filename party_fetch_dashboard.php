<?php

session_start(); 
if (!isset($_SESSION['userid'])) {
$_SESSION['msg'] = "You must log in first";
header('location: login.php');
}
if (isset($_GET['logout'])) {
session_destroy();
unset($_SESSION['username']);
unset($_SESSION['userid']);
header("location: login.php");
}

require 'connect.php';

$username = $_SESSION['username']; 

$conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );

$statement = $conn->prepare("SELECT * from party"); 

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

foreach($result as $row)
{

$sub_array = array();

      $sub_array[] = $row['party_name'];
        // $sub_array[] = $row['id'];
       $sub_array[] = $row['party_code'];
       $sub_array[] = $row['party_location'];
       $sub_array[] = $row['mobile_no'];
       $sub_array[] = $row['address'];
       $sub_array[] = $row['contact_person'];
 
      $sub_array[] = $row['email'];

       $sub_array[] = $row['pan']; 
       $sub_array[] = $row['yes_no']; 
       $sub_array[] = $row['gstin'];
       $sub_array[] = $row['gst_type'];
       $sub_array[] = $row['bank'];
       $sub_array[] = $row['branch'];
       $sub_array[] = $row['acc_no'];
       $sub_array[] = $row['acc_holder_name'];
       $sub_array[] = $row['ifsc_code'];  
       $sub_array[] = $row['date_time'];
      
      $data[] = $sub_array;
}

$results = array(
"sEcho" => 1,
"iTotalRecords" => $count,
"iTotalDisplayRecords" => $count,
"aaData"=>$data);

echo json_encode($results);
exit;
?>
