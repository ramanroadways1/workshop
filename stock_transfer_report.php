<?php
require("connect.php");
include('header.php');
?>
  
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include('aside_main.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Stock Report Workshop Product</h3>
         
      </div>


      <script>
        function myFunction() {
          window.print();
        }
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }

            $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'Y-m-d'   
                     });  
                     $(function(){  
                          $("#theft_out_date").datepicker();  
                          
                     });  
                   
                });  
            
       </script>
         <div class="row">
             
        <div class="box-body">
          <div class="row-2">
           
            <div class="col-md-12">
              <div id="order_table"> 
               <form method="post" action="outward_stock_transfer.php" autocomplete="off">
                  <?php 
                      $username =  $_SESSION['username'];
                  ?>

                
              <br>
              <div class="row-4">
                 <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:verdana; font-size:  13px; background-color: #EEEDEC ">
                  <tr>


                   <script type="text/javascript">
               function get_data2(val) {
                  $.ajax({
                    type: "POST",
                    url: "stock_transfer_detail_by_pname.php",
                    data:'productname='+val,
                    success: function(data){
                      $("#rate_master222").html(data);
                    }
                    });
                }
                $(function()
                { 
                  $("#productname").autocomplete({
                  source: 'autocomplete_product_by_name.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    location.reload();
                  alert('Product Name does not exists MAIN PAGE.');

                  } 
                },
                focus: function (event, ui) { return false; } }); });
            </script>

                
                  </tr>
                  
                </table>
                </div>
               </form>


                <div style="overflow-x:auto;">
            <table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
                    <tbody>
               <tr>
                  <th style="display: none;">Id</th>
                  <!-- <th>To Branch</th> -->
                  <th>Product No</th>
                   <th>Product Name</th>
                   <th>Company</th>
                 <!--  <th>Rate</th> -->
                  <th>Available Quantity</th>
             <!--      <th>Remove</th> -->
            </tr>
            </tbody>  
              <?php
  
              $query = "SELECT productno,company,quantity,productname FROM product_inventory";
          
              $result = mysqli_query($conn, $query);
              while($row = mysqli_fetch_array($result)){

               $productno = $row['productno'];
               $productname = $row['productname'];
               $company = $row['company'];
               // $rate = $row['rate'];
               $quantity = $row['quantity'];
             
              ?>
              <tr>
                 <input type="hidden" name="id" value="<?php echo $id; ?>">
                <td ><?php echo $productno?>
                  <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>">
                </td>
                <td ><?php echo $productname?>
                  <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>">
                </td>

                 <td ><?php echo $company?>
                  <input  type="hidden" readonly="readonly" name="company[]" value="<?php echo $company; ?>" >
                </td>
                
               <!--  <td ><?php echo $rate?>
                  <input  type="hidden" readonly="readonly" name="rate[]" value="<?php echo $rate; ?>">
                </td> -->
                 <td ><?php echo $quantity?>
                  <input  type="hidden" readonly="readonly" name="quantity[]" value="<?php echo $quantity; ?>">
                </td>

                
<!--                  <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="id" value="X" class="btn btn-danger" />
                </td> -->
                <script>
                     
                      function DeleteModal(id)
                      {
                        var id = id;
                        var username = '<?php echo $username; ?>'
                            
                        if (confirm("Do you want to delete this data To Transfer..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_temp_transfer_data.php",
                                  data: 'id='+id + '&username='+username,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div> 
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="insert_qun_stock.php" id="main_form">
            
               <input type="hidden" name="office" id="branch" class="form-control" fo value="<?php echo $branch ?>" /> 
           </form>
            </div>  
          </div>

        </div>
      </div>

    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_internal_job.php",  
                          method:"POST",  
                          data:{from_date:from_date,to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

  
           <div id="rate_master222"></div>
             <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var avl_qty = Number($('#avl_qty').val());
                 var trf_qty = $('#trf_qty').val();
                 if(avl_qty=="")
                 {
                   alert("Avalaible Quantity is Null");
                     $('#trf_qty').val('');
                 }else{
                 
                       if(trf_qty>avl_qty)
                       {
                        amount = 
                         alert('You can not exceed Avl Qty  Quantity  is '+ avl_qty);
                         $('#trf_qty').val('');
                          $('#amount').val('');
                       }else{
                        var amt = trf_qty*myrate;
                         $('#amount').val((amt).toFixed(2));

                       }
                     }

                }
               </script>


