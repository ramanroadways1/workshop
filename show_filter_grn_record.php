<?php 
  session_start(); 
  if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
/*    include("aside_main.php");*/

  ?>
   <style type="text/css">
          @media print {
           /*  #from_date,#to_date,#filter,#print,#heading,#button,#search_box{
            display: none;
          }*/
          #grn_no_h3,#h3{
             font-size: 20px;
          }
           h3,h4  {
        font-size: 30px;
        
        }

          #print{
            display: none;
          }
           #pname{
      width: 500px;
    }

         header,footer {
            display: none !important;
          }
          body{
              margin-top: 0cm;
              zoom:40%;
            margin-bottom: 0cm;
          }
           table, tr,body,td  {
        font-size: 20pt;
        
        }
           
        }
   </style>

  <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }

    th, td {
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even){background-color: #f2f2f2}
 </style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>



</head>
<body class="hold-transition skin-blue sidebar-mini">
            <div class="box-header with-border">
              <center><span style="color: blue;"><h3 >Grn Detail Date</h3></span>
<input type="button" style="float: right; " id="print" class="btn btn-info btn-sm add-new" name="" value="Print" onclick="myprint()">
              </center>
             </div>
                    <script type="text/javascript">
                        function myprint() {
                                window.print();
                              }
                      </script>
         <div class="row-md-2"> 
          <table class="table table-hover" border="4";>
                 <tbody>
                  <tr class="table-active">
                    <th style="display: none;" scope="row">ID</th>
                    <td style="width: 360px;">Product Name</td>
                    <td>Product Number</td>
                    <td style="width: 130px;">P.O Qty</td>
                     <td>Rate/Unit</td>
                    <!-- <td>Amount</td> -->
                    <td>G.S.T(%)</td>
                    <td style="width: 150px;">Gst Amt</td>
                     <td style="width: 150px;">Total Amt</td>
                    <td style="width: 150px;">GRN Qty</td>
                    <td style="width: 150px;">GRN Total</td>
                    <!--  <td>Grn No.</td> -->
                    <td style="width: 170px;">Back Date</td>
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $valueToSearch= $_POST['grn_no'];
              $show = "SELECT id, grn_no,productno,party_name,productname,quantity,rate,amount,gst,gst_amount,total_amount,received_qty,new_total,back_date FROM grn where grn_no='$valueToSearch'";
              $result = $conn->query($show);
              
              $sum=mysqli_query($conn,"SELECT SUM(new_total) as new_total,SUM(quantity) as quantity,SUM(received_qty) as received_qty  FROM grn where grn_no='$valueToSearch'");
              $row2=mysqli_fetch_array($sum);

              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                    ?>
                    <h4>GRN No: <?php echo $valueToSearch; ?></h4>

                   <!--   <font id="grn_no_h3" style="font-size: 15px;">Grn No:<?php echo $valueToSearch; ?></font> -->

                    <?php
                  while($row = $result->fetch_assoc()) {
                      $id = $row['id'];
                      $grn_no = $row['grn_no'];
                      $productno = $row['productno'];
                      $party_name = $row['party_name'];
                      $productname = $row['productname'];
                      $quantity = $row['quantity'];
                      $rate = $row['rate'];
                      $amount = $row['amount'];
                      $gst = $row['gst'];
                      $gst_amount = $row['gst_amount'];
                      $total_amount1 = $row['total_amount'];
                      $total_amount = round($total_amount1,2);

                      $received_qty = $row['received_qty'];
                      $new_total1 = $row['new_total'];

                      $new_total = round($new_total1,2);

                      $back_date = $row['back_date'];
                  ?>
                <div class="row">
                  <div class="col-md-10">
                    <tr> 
                     
                      <td style="display: none;"><?php echo $id; ?></td>
                      <td id="pname"><?php echo $productname; ?></td>
                      <td><?php echo $productno; ?></td>
                      <td><?php echo $quantity; ?></td>
                      <td><?php echo $rate; ?></td>
                   <!--    <td><?php echo $amount; ?></td> -->
                      <td><?php echo $gst; ?></td>
                      <td><?php echo $gst_amount; ?></td>
                      <td><?php echo $total_amount; ?></td>
                      <td><?php echo $received_qty; ?></td>
                      <td><?php echo $new_total; ?></td>
                      <td><?php echo $back_date; ?></td>
                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <td colspan='2'>Total Calculation : </td>
                    <td colspan='5'>$row2[quantity]</td>
                    <td colspan='1'>$row2[received_qty]</td>
                    <td>$row2[new_total]</td>
                     <td colspan='2'></td>

                  </tr>";
                  } else {
                   
                  }
                  ?>

              </table>
           <!--  </div> -->
            
            </div>
        
      

</body>
</html>
