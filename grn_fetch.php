<?php

  session_start(); 


  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }

  require 'connect.php';
// $conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME1);
//  if ( mysqli_connect_errno() ) {
//    die ('Failed to connect to MySQL: ' . mysqli_connect_error());
//  }
   // error_reporting(0);

 $username = $_SESSION['username']; 
 $employee = $_SESSION['employee'];
 
 // print_r($employee);
 $conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );
   
         $statement = $conn->prepare("SELECT emp_data.empname,date_purchase_key.id , purchase_order, party_name, party_code, count(productno) as productno, GROUP_CONCAT(productname) as productname, date1, GROUP_CONCAT(rate) as rate, GROUP_CONCAT(rate_master) as rate_master , GROUP_CONCAT(quantity) as quantity, GROUP_CONCAT(amount) as amount , GROUP_CONCAT(gst) as gst,GROUP_CONCAT(gst_amount) as gst_amount,GROUP_CONCAT(total) as total,timestamp1 FROM date_purchase_key INNER JOIN  insert_po ON insert_po.key1=date_purchase_key.key1 LEFT JOIN emp_data ON insert_po.employee = emp_data.empcode WHERE approve_status='1'  and date_purchase_key.username ='$username' AND insert_po.username = '$username' GROUP by purchase_order order by date_purchase_key.id ASC"); 

   $statement->execute();
   $result = $statement->fetchAll();
   $count = $statement->rowCount();
   $data = array();
   
   foreach($result as $row)
   {
    
          $id = $row["id"];
          $purchase_order = $row["purchase_order"];
          $productno = $row["productno"];
          $productname = $row['productname'];
          $party_name = $row["party_name"];
          $party_code = $row["party_code"];
          $rate = $row["rate"];
          $quantity = $row["quantity"];
          $amount=$row["amount"];
          $gst=$row["gst"];
          $gst_amount=$row["gst_amount"];
          $total = $row["total"];
          $timestamp1 = $row['date1'];

       $sub_array = array();
   
       // $sub_array[] = date('d/m/Y H:i:s', strtotime($row['Txnshow_grn_detail.phpime']));

       // $sub_array[] = $row['id'];
       $sub_array[]  = "<form method=\"POST\" action=\"show_grn_detail.php\">
                              <input type=\"hidden\" name=\"id\" value='".$row['id']."'>
                              <input type=\"hidden\" name=\"purchase_order\" value='".$row["purchase_order"]."'>
                              <input type=\"submit\"  name=\"\" value=\"View\" class=\"btn btn-primary btn-sm\" >
                            </form>";
                             $sub_array[] = $purchase_order;
                            $sub_array[]  = $row["productno"]; 

                            $sub_array[]  = $row["party_name"];
                            $sub_array[]  = $row["party_code"];
                            $sub_array[]  = $row['date1'];
                             $sub_array[]  = $row['empname'];
                            $data[] = $sub_array;
   }
   
   $results = array(
       "sEcho" => 1,
       "iTotalRecords" => $count,
       "iTotalDisplayRecords" => $count,
       "aaData"=>$data);
   
    echo json_encode($results);
    exit;
?>
