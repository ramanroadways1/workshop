
 <?php  
 include("connect.php");
 ?>  
<!DOCTYPE html>
<html>
<head>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

      <style type="text/css">
          @media print {
          #button{
            display: none;
          }
          #party_name{
            width: 80px;
          }
          #productname{
            width: 500px;
          }
            body{
             page-break-before: avoid;
            width:100%;
            height:100%;
            zoom: 80%;
            size: A4;
            margin:0px; 
          }    
        }
   </style>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>GRN Detail</h1>
    </section> -->

    <section class="content">
     
    <div class="box box-info">
      <div class="box-header with-border">
       <center> <h3 class="box-title">Goods Received Notes Detail</h3></center>
      
      </div>
          <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" style="width: 100%;" class="table table-striped table-bordered" border="1">  
                  <?php 
                  $value_to_search = $_POST['grn_no1'];
                 
                     $sql = "SELECT * FROM grn where grn_no ='$value_to_search'";
                    $result = $conn->query($sql);

                     $sql2 = "SELECT * FROM grn where grn_no ='$value_to_search' group by grn_no";
                    $result2 = $conn->query($sql2);

                       $row4=mysqli_fetch_array($result2);
                       $party_name1 = $row4["party_name"];
                       $grn_no1 = $row4["grn_no"];

                     ?>
                  <thead>  
                     Grn No: <?php echo  $grn_no1; ?><br>
                        Party Name:<?php echo  $party_name1; ?>
                       <tr>  
                            <td style="display: none;">Purchase Order</td>
                            <td style="display: none;">Party Code</td>
                            <td id="productname">Product Number</td>
                            <td style="width: 300px;">Product Name</td>
                            <td>Purhcase Quantity</td>
                            <td>Rate/Unit</td>
                            <td>Amount</td>
                            <td>GST(%)</td>
                            <td>GST Amount</td>
                            <td>Total Amount(p.o)</td>
                            <td>Received Qty</td>
                            <td>Grn Amount</td>
                            
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                     $sum=mysqli_query($conn,"SELECT SUM(new_total) as new_total,party_name as party_name1,grn_no as grn_no1 FROM grn WHERE  grn_no='$value_to_search'  group by grn_no");

                      $row3=mysqli_fetch_array($sum);
                       $sum_amount = $row3["new_total"];
                    
                        $total_amount1 = $row["total_amount"];
                        $total_amount = round($total_amount1,2);
                          echo '  
                       <tr> 
                            <td style="display: none;">'.$row["purchase_order"].'</td>
                            <td style="display: none;">'.$row["party_code"].'</td>
                            <td>'.$row["productno"].'</td>  
                            <td>'.$row["productname"].'</td>  
                            <td>'.$row["quantity"].'</td>
                            <td>'.$row["rate"].'</td>
                            <td>'.$row["amount"].'</td>  
                            <td>'.$row["gst"].'</td>
                            <td>'.$row["gst_amount"].'</td>  
                             <td>'.$total_amount.'</td>
                            <td>'.$row["received_qty"].'</td>
                            <td>'.$row["new_total"].'</td> 
                           
                       </tr>  
                       ';  
                  }  

                  echo "<tr>

                    <td colspan='9'>Total Payment Amount : </td>
                    <td>$sum_amount</td>
                  </tr>";

                  ?>  
                </table>  
              </div>  
            </div>  <input type="button" id="button" style="float: right; margin-right: 50px;" class="btn btn-info add-new" name="" value="Print" onclick="myprint()"><br>  <script type="text/javascript">
              function myprint() {
                      window.print();
                    }
            </script>
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
 

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

