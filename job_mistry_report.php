<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Payment</h1>
    </section> -->

     <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Product Detils by GRN</h3>

          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div>
        <!-- /.box-header -->
       <!--  <div class="box-body"> -->
          <div style="overflow-x:auto;">
              <table class="table table-hover" border="4";>
                <!-- <center><h1 class="h3 mb-0 text-gray-800">Approve Invoice for Payment</h1></center> -->
               <!--  <a href="pay_detail.php">Detail Pay</a><br><br> -->
                <tbody>
                  <tr class="table-active">
                        <td>Product No</td>
                        <td>Product Name</td>
                        <td >Quantity</td>
                        <td>Rate</td> 
                        <td>Amount</td>
                         <td>GST(%)</td> 
                         <td>Gst Amount</td> 
                         <td>Purchase Amount</td>
                         <td>Received Quantity(IN GRN)</td>
                        <td>GRN Amount</td>
                        <td>GRN Date</td>
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $demo=$_POST['grn_no'];

              // echo $demo=$_POST['id'];
            
              $show1 = "SELECT * FROM `grn`  where grn_no='$demo' GROUP by grn_no";
              $result = $conn->query($show1);


              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) {
                      // $id = $row['id'];
                  
                    $productno = $row['productno'];
                    $grn_no = $row['grn_no'];
                    $productname = $row['productname'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $amount1 = $row['amount'];
                    $amount = round($amount1,2);
                    $gst = $row['gst'];
                    $gst_amount1 = $row['gst_amount'];
                    $gst_amount = round($gst_amount1,2);
                    $total_amount1 = $row['total_amount'];
                    $total_amount = round($total_amount1,2);
                    $received_qty = $row['received_qty'];
                    $new_total = $row['new_total'];
                    $back_date = $row['back_date'];
                   
                     
                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                      <td style="display: none;"><?php echo $id; ?></td>



                       <td><?php echo $productno; ?></td>
                       <td><?php echo $productname; ?></td>
                       <td><?php echo $quantity; ?></td>
                       <td><?php echo $rate; ?></td>
                       <td><?php echo $amount; ?></td>
                       <td><?php echo $gst; ?></td>
                       <td><?php echo $gst_amount; ?></td>
                       <td><?php echo $total_amount; ?></td>
                       <td><?php echo $received_qty; ?></td>
                       <td><?php echo $new_total; ?></td>
                       <td><?php echo $back_date; ?></td>
                  

                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <!-- <td colspan='4'>Total Calculation : </td> -->
                    <!-- <td colspan='6'>[quantity]</td> -->

                  </tr>";
                  } else {
                       echo "This Product Not Created Any GRN";
                    exit();
                  }
                  ?>

              </table>
            </div>

          </div>
        </section>
      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
