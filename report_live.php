<!DOCTYPE html>
<html>
<head>
  </style>
  <?php 
    
    include("header.php");
    include("aside_main.php");
    include("connect.php");


?>

<style>
  table 
  {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
  }

  th, td
  {
    text-align: left;
    padding: 8px;
  }
  tr:nth-child(even){background-color: #f2f2f2
  }

  .ui-autocomplete { z-index:2147483647; }
</style>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> -->

<link href="assets/font-awesome.min.css" rel="stylesheet">
<script src="assets/jquery-3.5.1.js"></script>
<script src="assets/jquery-ui.min.js"></script>
<link href="assets/jquery-ui.css" rel="stylesheet"/>
<link href="assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/searchBuilder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="assets/custom.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
</script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js">
</script>
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>
<script type="text/javascript"> 

$(function()
{    
$( "#party_code_to_search" ).autocomplete({
source: 'party_code.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});

</script> 
  
 <style type="text/css">
    <style type="text/css">
  @media print {
  button {
    display: none !important;
  }
  input,
  textarea,select {
    border: none !important;
    box-shadow: none !important;
    outline: none !important;
    display: none !important;
  }
  /*#print,#sub,#new_prblm,#issue,#sno,#main_heading {
    display: none;
  }*/

  #driver_complain {
    width: 4px;
  }
  #amount{
    width: 1;
  }
   table, tr,body,h4,form  {
        height: auto;
        font-size: 16pt;
        font: solid #000 !important;
        }
         table {
       border: solid #000 !important;
        border-width: 1px 0 0 1px !important;
    }
    th, td {
        border: solid #000 !important;
        border-width: 0 1px 1px 0 !important;
    }
     
}
    
}

</style>

</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Report OverAll</h3>
          </div>
          <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
</script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js">
</script>
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>
</head>
<body>
<div class="box-body">
<div class="col-md-12">
<div class="form-group">
<div class="col-md-12">
  <div class="form-group row">



<select id='purpose'>
    <option>Select Option</option>
    <option value="0">Model Number</option>
    <option value="1">Truck Number</option>
    <option value="2">Wheeler</option>
</select>

<script>
$(document).ready(function(){
    $('#purpose').on('change', function() {
      if ( this.value == '0')
      {
        $("#business").show();
      }
      else
      {
        $("#business").hide();
      }
    });
});
$(document).ready(function(){
    $('#purpose').on('change', function() {
      if ( this.value == '1')
      {
        $("#business1").show();
      }
      else
      {
        $("#business1").hide();
      }
    });
});
$(document).ready(function(){
    $('#purpose').on('change', function() {
      if ( this.value == '2')
      {
        $("#business2").show();
      }
      else
      {
        $("#business2").hide();
      }
    });
});
</script>
<form method="POST" action="insert_dashboard.php">
<div  style='display:none;' id='business'>
  <label>Model Number</label>
    <div class="bs-multiselect">
     <select id = "mltislct"  name="model[]" multiple = "multiple" >
  <?php 
    include ("connect.php");
      $get=mysqli_query($conn,"SELECT dairy.own_truck.model FROM dairy.own_truck  group by model");
         while($row = mysqli_fetch_assoc($get))
        {
  ?>
  <option value = "<?php echo($row['model'])?>" >
    <?php 
          echo ($row['model']."<br/>");
    ?>
  </option>
    <?php
    }
  ?>
</select>
</div>
</div>
<!-- //2nd -->

<div class="col-md-4" style='display:none;' id='business1' >
  <label>Truck Number</label>
<div class="bs-multiselect">
<select id = "mltislct2" name="tno[]" multiple = "multiple" >
<?php 
include ("connect.php");
$get=mysqli_query($conn,"SELECT dairy.own_truck.tno FROM dairy.own_truck");
while($row = mysqli_fetch_assoc($get))
{
?>
<option value = "<?php echo($row['tno'])?>" >
<?php 
echo ($row['tno']."<br/>");
?>
</option>
<?php
}
?>
</select>

</div>
</div>

<div class="col-md-4" style='display:none;' id='business2'>
    <label>Truck Wheel</label>
  <div class="bs-multiselect">
  <select id = "mltislctre" name="wheeler[]" multiple ="multiple">
  <?php 
  include ("connect.php");
  $get=mysqli_query($conn,"SELECT dairy.own_truck.wheeler  FROM dairy.own_truck group by wheeler");
  while($row = mysqli_fetch_assoc($get))
  {
  ?>
  <option value = "<?php echo($row['wheeler'])?>" >
  <?php 
  echo ($row['wheeler']."<br/>");
  ?>
  </option>
  <?php
  }
  ?>
  </select>

  </div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12"style=" margin-top: 20px;">
 <div class="col-md-6" >
    <label class="form-check-label" for="flexCheckDefault">
    Internal Job Card
    </label>
    <input class="form-check-input" type="checkbox" value="Internal" id="flexCheckDefault">
 </div>
<div class="col-md-6" >
 <label class="form-check-label" for="flexCheckDefault">
   External Job Card
  </label>
  <input class="form-check-input" type="checkbox" value="External" id="flexCheckDefault">
</div>
</div>

</div>
</div>
</div>
<script>

    $(document).ready(function() {
    $('#mltislct').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    filterPlaceholder:'Search Here..'
    });
    });

</script>
<script>

    $(document).ready(function() {
    $('#mltislct2').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    filterPlaceholder:'Search Here..'
    });
    });

</script>
<script>
    $(document).ready(function() {
    $('#mltislctre').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    filterPlaceholder:'Search Here..'
    });
    });

</script>

      <div class="row">
        <div class="col-md-12"style=" margin-top: 20px;">
             <div class="col-md-2" >
                  <b style="margin-left: 50px;">Select Date</b>
              </div>
              <div class="col-md-2">  
                   <input type="date" name="from_date" id="from_date" autocomplete="off" class="form-control" placeholder="From Date" />  
              </div>
              <div class="col-md-2">  
                   <input type="date"  name="to_date" id="to_date" autocomplete="off" class="form-control" placeholder="To Date" />  
              </div>
              <!DOCTYPE html>
              <div class="col-md-5"style="margin-top: 15px;margin-left: 295px;">  
                   <input type="submit" onclick="test()"  name="submit" id="submit" value="Submit" class="btn btn-info" />  
              </div> 
                <div style="clear:both"></div>
         </div>
              </div>
                </div>
                </div>
              </div>
            </div>

            <!DOCTYPE html>
<html>
<!-- <head>
</head> -->
<body class="hold-transition skin-blue sidebar-mini">
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                              <!-- <th>ID</th> -->
                              <th>Product Number</th>
                              <th>Product Name</th>
                              <!-- <th>Rate/Unit</th> 
                              <th>Rate Lock/Unlock</th> -->
                              <!--  <th>Employee Name</th>-->
                            </tr>
                         </thead>
                      </table>
                      <div id="dataModal" class="modal fade">
                         <div class="modal-dialog modal-lg">
                            <div class="modal-content" id="employee_detail">
                            </div>
                         </div>
                      </div>
                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "dashboard_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV' 
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                              "criteria": [
                                  {
                                      "data": ' VEHICLE ',
                                      "condition": '=',
                                      "value": ['']
                                  }
                              ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=assets/loading.gif height=25> </center>"
                          },
                         "aaSorting": [],
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          // $('title').html("rrpl");

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.

    </footer>

  <div class="control-sidebar-bg"></div>
</div>

       <script src="assets/bootstrap.js"></script>

        <script src="assets/bootstrap.bundle.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="assets/jszip.min.js"></script>
        <script type="text/javascript" src="assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="assets/dataTables.searchBuilder.min.js"></script>

  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


  </div>
</form>

 </div>
</section> 
</div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
</footer>

  <div class="control-sidebar-bg"></div>
</body>
</html>
<script type="text/javascript">

  function test() {

  var fromValue = document.getElementById('from_date').value;
  var toValue = document.getElementById('to_date').value;

  var fromDate = new Date(fromValue);
  var toDate = new Date(toValue);
  if (fromDate === toDate) 
  {

  alert ("Current Date is Greater THan the User Date");
  }
  }
</script>



