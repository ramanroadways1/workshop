
 <?php  
 include("connect.php");
 ?>  
<!DOCTYPE html>
<html>
<head>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

       <style type="text/css">
          @media print {
          #button{
            display: none;
          }
          #party_name{
            width: 80px;
          }
          #productname{
            width: 500px;
          }
            body{
             page-break-before: avoid;
            width:100%;
            height:100%;
            zoom: 80%;
            size: A4;
            margin:0px; 
          }    
        }
   </style>
  

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>GRN Detail</h1>
    </section> -->

    <section class="content">
     
    <div class="box box-info">
      <div class="box-header with-border">
       <center> <h3 class="box-title">Goods Received Notes Detail</h3></center>
      
      </div>
          <div class="box-body">
          <div class="row-md-2">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table style="width: 100%;" class="table table-striped table-bordered" border="1">  
                  <?php 
                  $value_to_search = $_POST['grn_no1'];
                 
                     $sql = "SELECT grn.id as id,grn.grn_no as grn_no,grn.party_name as party_name,grn.productno as productno,grn.productname as productname,grn.rate as rate,grn.quantity as quantity,grn.amount as amount,grn.gst as gst,grn.gst_amount as gst_amount,grn.received_qty as received_qty,grn.new_total as new_total,total_amount as total_amount,grn.back_date as back_date FROM grn INNER join files_upload  where grn.grn_no = files_upload.grn_no and grn.grn_no = '$value_to_search' GROUP by id";
                      $result = $conn->query($sql);

                       $sum=mysqli_query($conn,"SELECT grn.party_name as party_name1,grn.grn_no as grn_no1,yes_no,gst_type,files_upload.invoice_no as invoice_no,crn_no as crn_no,utr_no as utr_no,utr_date as utr_date,tds as tds,other_h_o as other_h_o,remark_h_o as remark_h_o FROM grn INNER join party INNER join files_upload INNER join insert_pay_invoice INNER join payment_confirm WHERE party.party_name = grn.party_name and grn.grn_no='$value_to_search'  group by grn.grn_no");
                       $row3=mysqli_fetch_array($sum);

                        $sum2=mysqli_query($conn,"SELECT SUM(new_total) as new_total3, SUM(quantity) as quantity, SUM(total_amount) as total_amount, SUM(received_qty) as received_qty FROM grn WHERE  grn_no='$value_to_search'  group by grn_no");

                      $row4=mysqli_fetch_array($sum2);
                       $sum_amount2 = $row4["new_total3"];
                        $total_amount2 = $row4["total_amount"];
                         $quantity2 = $row4["quantity"];
                         $received_qty2 = $row4["received_qty"];
                      
                       $grn_no1 = $row3["grn_no1"];
                       $party_name1 = $row3["party_name1"];
                        $gst_applicable = $row3["yes_no"];
                       $gst_type = $row3["gst_type"];
                        $invoice_no = $row3["invoice_no"];
                       $crn_no = $row3["crn_no"];
                        $utr_no = $row3["utr_no"];
                       $utr_date = $row3["utr_date"];
                        $tds = $row3["tds"];
                        $other_h_o = $row3["other_h_o"];
                       $remark_h_o = $row3["remark_h_o"];
                      ?>
                       <div class="row">
                        <div class="col-md-4">
                          Grn No: <?php echo  $grn_no1; ?><br>
                          Party Name: <?php echo  $party_name1; ?>
                           
                        </div>
                        <div class="col-md-3">
                           Gst Applicable:<?php echo  $gst_applicable; ?><br>
                            Gst Type: <?php echo  $gst_type; ?>
                        </div>

                        <div class="col-md-3">
                           Tds:<?php echo  $tds; ?><br>
                            Crn No: <?php echo  $crn_no; ?>
                        </div>

                        <div class="col-md-2">
                           Utr no:<?php echo  $utr_no; ?><br>
                            Utr Date: <?php echo  $utr_date; ?>
                        </div>
                      </div> 
                      <center>
                  <thead>  
                       <tr>  
                           <!--  <td >Grn NO</td>
                            <td>Party Name</td> -->
                            <td id="productname">Product Number</td>
                            <td >Product Name</td>
                            <td>Purhcase Quantity</td>
                            <td>Rate/Unit</td>
                            <td>Amount</td>
                            <td>GST(%)</td>
                            <td>GST Amount</td>
                            <td>Total Amount(p.o)</td>
                            <td>Received Qty</td>
                            <td>Grn Amount</td>
                            
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {    $total_amount1 = $row["total_amount"];
                        $total_amount = round($total_amount1,2);

                        if ($gst_type=="cgst_sgst") {
                          $gst2 = $row['gst'];
                          $gst1 = $gst2/2;
                          $gst=  "cgst/sgst".$gst1.",".$gst1;
                        }else{
                          $gst= $row['gst'];
                        }
                        
                          echo '  

                       <tr> 
                            <td>'.$row["productno"].'</td>  
                            <td>'.$row["productname"].'</td>  
                            <td>'.$row["quantity"].'</td>
                            <td>'.$row["rate"].'</td>
                            <td>'.$row["amount"].'</td>  
                            <td>'.$gst.'</td>
                            <td>'.$row["gst_amount"].'</td>  
                             <td>'.$total_amount.'</td>
                            <td>'.$row["received_qty"].'</td>
                            <td>'.$row["new_total"].'</td> 
                       </tr>  
                       ';   
                  } 

                   echo "<tr>
                    <td colspan='2'>Total Amount</td>
                    <td colspan='5'>$quantity2</td>
                    <td colspan='1'>$total_amount2</td>
                    <td  colspan='1'>$received_qty2</td>
                     <td  colspan='1'>$sum_amount2</td>
                  </tr>"; 

                  ?>  
                </table> 
              </div>  
            </div>  <br>
            <form action="UserReport_Export.php" method="POST">
              <input type="hidden"  name="grn_no" value="<?php echo $grn_no1; ?>">
              <input type="Submit" value="Export To Excel">
            </form>
             <script type="text/javascript">
                     function myprint() {
                      window.print();
                    }
            </script>
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
 

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

