<html xmlns=”http://www.w3.org/1999/xhtml”>
<head runat=”server”>
    <title></title>
    <script src=”https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js”></script>
<script src=”https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js”></script>
<script src=”https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js”></script>
    <style> 
        table { 
            font-family: arial, sans-serif; 
            border-collapse: collapse; 
            width: 100%; 
        } 
 
        td, th { 
            border: 1px solid #dddddd; 
            text-align: left; 
            padding: 8px; 
        } 
 
        tr:nth-child(even) { 
            background-color: #dddddd; 
        } 
    </style>
</head>
 <input type="button" class="btn btn-warning" id="btnExport" value="Download PDF" />
<body>
    <form id=”form1″ runat=”server”>
             
       <table>
            <tbody> 
                <tr> 
                    <th>Company</th> 
                    <th>Contact</th> 
                    <th>Country</th> 
                </tr> 
                <tr> 
                    <td>HCL</td> 
                    <td>Maria Anders</td> 
                    <td>USA</td> 
                </tr> 
                <tr> 
                    <td>Conduent</td> 
                    <td>Francisco Chang</td> 
                    <td>UK</td> 
                </tr> 
                <tr> 
                    <td>Wipro</td> 
                    <td>Roland Mendel</td> 
                    <td>Austria</td> 
                </tr> 
                <tr> 
                    <td>NIIT Technologies LTD</td> 
                    <td>Helen Bennett</td> 
                    <td>INDIA</td> 
                </tr> 
                <tr> 
                    <td>Laughing Bacchus Winecellars</td> 
                    <td>Yoshi Tannamuri</td> 
                    <td>Canada</td> 
                </tr> 
                <tr> 
                    <td>Magazzini Alimentari Riuniti</td> 
                    <td>Giovanni Rovelli</td> 
                    <td>Italy</td> 
                </tr> 
            </tbody> 
        </table>
    </form>
</body>
     <script>
         let doc = new jsPDF(‘p’, ‘pt’, ‘a4’); 
         doc.addHTML(document.body, function () {
             doc.save(‘html.pdf’);
         });
    </script>
</html>