
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Payment</h1>
    </section> -->

     <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Product Detils by GRN</h3>

          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div>
        <!-- /.box-header -->
       <!--  <div class="box-body"> -->
          <div style="overflow-x:auto;">
              <table class="table table-hover" border="4";>
                <!-- <center><h1 class="h3 mb-0 text-gray-800">Approve Invoice for Payment</h1></center> -->
               <!--  <a href="pay_detail.php">Detail Pay</a><br><br> -->
                <tbody>
                  <tr class="table-active">
                        <td>truck number</td>  
                            <td>job card number</td>  
                            <td>Product name</td>  
                            <td>Product type</td>
                            <td>Available Quantity</td> 
                            <td>Recive quantity</td> 
                            <td>Purchase Rate</td> 
                            <td>Your Amount</td>
                             <td>Mistry</td>
                              <td>Submission date</td>
                             <td>Date</td>
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $demo=$_POST['productno'];
              $demo2=$_POST['job_card_no'];

              // echo $demo=$_POST['id'];
            
              $show1 = "SELECT * FROM `add_product_external_jobcard`  where productno='$demo' AND job_card_no='$demo2' ";
              $result = $conn->query($show1);


              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) {
                      // $id = $row['id'];
                  
                    $truck_no = $row['truck_no'];
                    $job_card_no = $row["job_card_no"];
                       $partsname = $row["partsname"];
                       $partstype = $row["partstype"];
                       $available_qty = $row["available_qty"];
                       
                        $quantity = $row["quantity"];
                       $latest_rate = $row["latest_rate"];
                       $amount = $row["amount"];
                       $mistry = $row["mistry"];
                       $submission_date = $row["submission_date"];
                       $timestamp1 = $row['timestamp1'];
                   
                     
                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                      <td style="display: none;"><?php echo $id; ?></td>



                     <td><?php echo $truck_no?></td>
                    <td><?php echo $job_card_no?></td> 
                    <td><?php echo $partsname?></td>
                    <td><?php echo $partstype?></td>
                   <td><?php echo $available_qty?> </td>
                    <td><?php echo $quantity?></td>
                    <td><?php echo $latest_rate?></td>
                   <td><?php echo $amount?> </td>
                    <td><?php echo $mistry?></td>
                    <td><?php echo $submission_date?></td>
                   <td ><?php echo $timestamp1?> </td>
                  

                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <!-- <td colspan='4'>Total Calculation : </td> -->
                    <!-- <td colspan='6'>[quantity]</td> -->

                  </tr>";
                  } else {
                       echo " This Product Not USE IN jOB Card";
                    exit();
                  }
                  ?>

              </table>
            </div>

          </div>
        </section>
      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
