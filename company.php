
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_product.php">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Masters</h1>
    </section>
    <section class="content">
  
      <!-- SELECT2 EXAMPLE -->

     
                <div class="row">
                    <div class="col-md-0"></div> 
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                            <h3 class="box-title">Company Master</h3>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                  <div class="col-md-0"></div> 
                    <div class="col-md-12">
                      <div class="box box-default">
                        <div class="box-header with-border">
                          <div class="table-responsive">
                           <br />
                           <div align="right">
                             <button type="button" name="add" id="add" class="btn btn-info">Add</button>
                            </div>
                            <br />
                            <div id="alert_message"></div>
                            <table  id="user_data" class="table table-bordered table-striped">
                             <thead >
                              <tr>
                               <th>Company</th>
                               <th>Action</th>
                             </tr>
                             </thead>
                            </table>
                           </div>
                         </div>
                        </div>
                    </div>  
                </div>



    </section>
    
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>


<!-- <script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
<!-- <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
<!-- <script src="bower_components/fastclick/lib/fastclick.js"></script> -->
<script src="dist/js/adminlte.min.js"></script>
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!-- <script src="dist/js/demo.js"></script> -->
</body>
</html>
<script type="text/javascript" language="javascript" >

 
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"fetch_company.php",
     type:"POST"
    }
   });
  }
  
  function update_data(id, column_name, value)
  {
   $.ajax({
    url:"update_company.php",
    method:"POST",
    data:{id:id, column_name:column_name, value:value},
    success:function(data)
    {
     $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
     $('#user_data').DataTable().destroy();
     fetch_data();
    }
   });
   setInterval(function(){
    $('#alert_message').html('');
   }, 5000);
  }

  $('#add').click(function(){
   var html = '<tr>';
   html += '<td contenteditable id="data1"></td>';
   html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
   html += '</tr>';
   $('#user_data tbody').prepend(html);
  });

  $(document).on('blur', '.update', function(){
   var id = $(this).data("id");
   var column_name = $(this).data("column");
   var value = $(this).text();
   update_data(id, column_name, value);
  });
  

  $(document).on('click', '#insert', function(){
   var company = $('#data1').text();
   if(company != '')
   {
    $.ajax({
     url:"insert_company.php",
     method:"POST",
     data:{company: company},
     success:function(data)
     {
      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
   else
   {
    alert("Fields is required");
   }
  });
  
  
  $(document).on('click', '.delete', function(){
   var id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this?"))
   {
    $.ajax({
     url:"delete_company.php",
     method:"POST",
     data:{id:id},
     success:function(data){
      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
  });
 });
</script>

