<?php 
  session_start(); 


  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }
?>
<?php
// session_start();
require("connect.php");
include('header.php');
?>
  
<!DOCTYPE html>
<html>
<head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body>
  <style type="text/css">
  body {
  background-image: url('img/trucks.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed; 
  background-size: 100% 100%;
}

</style>
</body>
<form id="branchLogin" method="post" onsubmit="return false;"><div class="row">
<div class="">
<div class="col-md-12">
 <div class="col-md-3">
                                <div>
                                 <label> Branch:</label>
                                  <select name="to_branch" required="required" style="width: 100%;" id="to_branch" class="form-control"> 
                                  <option value="">---Select Branch---</option>
                                   <?php 
                                      include ("connect.php");
                                        $get=mysqli_query($conn,"SELECT * FROM users");
                                           while($row = mysqli_fetch_assoc($get))
                                          { 
                                    ?>
                                    <option value = "<?php echo $row['id'];?>" >
                                      <?php 
                                            echo ($row['username']."<br/>");
                                      ?>
                                    </option>
                                      <?php
                                      }
                                    ?>
                                </select>
                              </div> 
                          </div>
                      </div>
                      <div class="col-md-2" >
                           <div class="col-md-3">
                          	<button class="btn btn-success"  style="margin-top: 20px;padding: 7px;" onclick="getLogin()">Submit</button>
                          </div>
                      </div>

			</div>
      </div>
	          </form>
                                   <script type="text/javascript">
                                            	function getLogin(){
                                            		var formdata = $('#branchLogin').serializeArray();
                                            		console.log(formdata);
                                            		$.ajax({
                                              				type:"POST",
                                              				url:"updatedata.php",
                                              				data:formdata,
                                              				success:function(res)
                                              				{
                                              				if(res==1){
                                              					window.location="main_index.php";
                                              				}else{
                                              					alert("something wrong");
                                              				}
                                              				}
                                              				});
                                              				}
                            </script>