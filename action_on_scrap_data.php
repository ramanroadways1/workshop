<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_main.php'); ?> 
<div class="content-wrapper">
   
   
    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Scrap Data</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
            $username =  $_SESSION['username'];
            $valueToSearch =  $_POST['scrap_no'];

          
          $sql = "SELECT * FROM outward_stock WHERE unique_no='$valueToSearch' and office ='$username' group by unique_no";
              $result = $conn->query($sql);
                    
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
                        $scrap_no = $row['unique_no'];
                        $from_office = $row['office'];
                        $scrap_out_date = $row['stock_operation_date'];
                        $productno = $row['productno'];
                        $company = $row['company'];
                        $productname = $row['productname'];
                        $producttype = $row['producttype'];
                        $rate = $row['rate'];
                        $sub = $row['rate'];
                        $productname = $row['productname'];
                        $producttype = $row['producttype'];
               ?>
<div class="row">
<div class="col-md-3">
      <label>Scrap Number</label>
      <input type="text" autocomplete="off" id="scrap_no" name="scrap_no"  class="form-control" required readonly="readonly" value="<?php echo $scrap_no;?>" placeholder="Office" />
</div>
<div class="col-md-3">
      <label>Scrap Data From Office</label>
      <input type="text" autocomplete="off" id="from_office" name="from_office"  class="form-control" required readonly="readonly" value="<?php echo $from_office;?>" placeholder="Office" />
</div>
<div class="col-md-3">
      <label>Scrap Out Date</label>
      <input type="text" autocomplete="off" id="scrap_out_date" name="scrap_out_date" value="<?php echo $scrap_out_date;?>" class="form-control" required readonly="readonly" placeholder="truck driver" />
</div>
</div>
             
            <?php
            }
          } else{
            
          }
         
            ?><br>

             <table  id="complain_table"  class="table table-bordered" border="4"; style=" font-family:verdana; font-size:  13px;">
                  <thead>  
                    <tr class="table-active">
                      <th style="display: none;">Id</th>
                      <th>Product no</th>  
                      <th>Product Name</th>
                      <th>Company</th>
                      <th>Product Type</th>
                      <th>Avl Quantity</th> 
                      <th>Scrap Quantity</th> 
                      <th>Scrap Amount</th>
                      <th>Submisssion date</th>
                      <th class="col-md-2">Back To Inventory</th>
                      <th class="col-md-2">Delete</th>
                    </tr>
                  </thead> 
                   <?php  
                      $sql = "SELECT * FROM outward_stock WHERE unique_no='$valueToSearch' and office ='$username' and status='scrap'";
                          $result = $conn->query($sql);
                                
                      if(mysqli_num_rows($result) > 0)
                       {
                           while($row = mysqli_fetch_array($result)){
                              $id = $row['id'];
                              $avl_qty = $row['avl_qty'];
                              $store_in_operation_date = $row["stock_operation_date"];
                              $productno = $row["productno"];
                              $productname = $row["productname"];
                              $company = $row["company"];
                              $producttype = $row["producttype"];
                              $rate = $row["rate"];
                              $stock_operation_qty = $row["stock_operation_qty"];
                              $stock_operation_amount = $row['stock_operation_amount'];
                              $unique_no = $row["unique_no"];
                              $final_submisssion_date = $row['final_submisssion_date'];
                           ?>
<tr>
  <td style="display: none;"><?php echo $id?>
  <input  type="hidden" readonly="readonly" name="id" id="id<?php echo $id; ?>" value="<?php echo $id; ?>">
 </td>
   
<td><?php echo $productno?>
  <input  type="hidden" readonly="readonly" name="productno" id="productno<?php echo $id; ?>" value="<?php echo $productno; ?>">
</td>
  <td style="display: none;">
  <input  type="hidden" readonly="readonly" name="avl_qty" id="avl_qty<?php echo $id; ?>" value="<?php echo $avl_qty; ?>">
</td>

 <td><?php echo $productname?>
  <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
</td>
<td><?php echo $company?>
  <input  type="hidden" readonly="readonly" name="mistry[]" id="mistry<?php echo $id; ?>" value="<?php echo $mistry; ?>">
</td>
<td><?php echo $producttype?>
  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
</td>
<td> 
      <?php 
      $sql2 = "SELECT * FROM product WHERE productno='$productno' and username ='$username' ";
      $result2 = mysqli_query($conn, $sql2);
      if(mysqli_num_rows($result2) > 0)  
      {
      $row2 = mysqli_fetch_array($result2);
      $quantity = $row2["quantity"];
      echo $quantity;

      }
      ?>
 
</td>
<td><?php echo $stock_operation_qty?>
  <input type="hidden" readonly="readonly" name="stock_operation_qty" id="stock_operation_qty<?php echo $id; ?>" value="<?php echo $stock_operation_qty; ?>">
</td>
<td><?php echo $stock_operation_amount?>
  <input  type="hidden" readonly="readonly" name="rate[]" id="rate" value="<?php echo $rate; ?>">
</td>
<td><?php echo $final_submisssion_date?>
<input  type="hidden" readonly="readonly" name="rate[]" id="rate<?php echo $id; ?>" value="<?php echo $rate; ?>">
</td>
<td>
 <input type="button"  onclick="addmodel('<?php echo $id;?>')" name="sno" value="Inventory" class="btn btn-success" />
<!-- <button type="button" onclick="DeleteModal('<?php echo $id;?>')" va name="sno"  class="btn btn-success"><i class="fa fa-arrow-left"></i></button> -->
</td>
<td>
<input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="Delete" class="btn btn-danger" />
</td>
  
  </tr>
                  <?php  
                   }
                  }
                  ?>  
                </table> 

                  <div class="col-md-12">
             <!--  <center>
                <input type="submit" align="center" name="submit" class="btn btn-primary" >
        
              </center>  -->    
            </div><br>
          </form>
<br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>
<script>
    function addmodel(id)
    {
      var id = id;
       var username = '<?php echo $username ?>'
         var avl_qty = $('#avl_qty'+id).val();
           var stock_operation_qty = $('#stock_operation_qty'+id).val();
             var productno = $('#productno'+id).val();
             
      if (confirm("Do you want to Add this Scrap Product in Inventory..?"))
      {
        if(id!='')
        {
          $.ajax({
                type: "POST",
                url: "add_scrap_in_inventory.php",
                data: 'id='+id + '&username='+username+ '&avl_qty='+avl_qty+ '&stock_operation_qty='+stock_operation_qty+ '&productno='+productno,
                success: function(data){
                  alert(data)
                  location.reload();
                    $("#result22").html(data);
                }
            });
        }
      }
    }     
  </script>
<div id="result22"></div> 
<script>
function DeleteModal(id)
{
  var id = id;
  var username = '<?php echo $username; ?>'
      
  if (confirm("Do you want to delete this Scrap Permanently..?"))
  {
    if(id!='')
    {
      $.ajax({
            type: "POST",
            url: "delete_scrap_permanently.php",
            data: 'id='+id + '&username='+username,
            success: function(data){
              location.reload();
                $("#result22").html(data);
            }
        });
    }
  }
}     
</script> <div id="result22"></div> 

 
