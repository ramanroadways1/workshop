<!DOCTYPE html>
<html>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.6/jspdf.plugin.autotable.min.js"></script>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Payment</h1>
    </section> -->

     <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Product Detils by GRN</h3>

          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
                        <!-- <a href="Truck_by_report.php"> -->
                       <center> 
                        <a style="margin-left: 312px;"href="truck_workshop.php">
                              <button type="button">
                                   Back
                             </button>
                         </a>
                        
                        </center>
        </div>
        <!-- /.box-header -->
       <!--  <div class="box-body"> -->
          <div style="overflow-x:auto;">
              <table class="table table-hover" id="val" border="4";>
                <!-- <center><h1 class="h3 mb-0 text-gray-800">Approve Invoice for Payment</h1></center> -->
               <!--  <a href="pay_detail.php">Detail Pay</a><br><br> -->
                <tbody>
                  <tr class="table-active">
                      
                        <td>Inspection Numbur</td>
                        <td >Creted Date</td>
                        <td>Department</td> 
                        <td>Mistry Name</td>
                        <td>WORK(Job Card)</td> 
                        <td>Job Date</td>
                         
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $demo=$_POST['job_card_no'];

              // echo $demo=$_POST['id'];
            
              $show1 = "SELECT * FROM `job_card_record` where job_card_no='$demo'  ";
              $result = $conn->query($show1);


              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) {
                      // $id = $row['id'];
                    // $truck_no1 = $row['truck_no1'];
                    $inspection_no = $row['inspection_no'];
                    $date1 = $row['date1'];
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                    $job_card_work = $row['job_card_work'];
                    $job_card_date = $row['job_card_date'];
                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                     
                       <td><?php echo $inspection_no; ?></td>
                       <td><?php echo $date1; ?></td>
                       <td><?php echo $department; ?></td>
                       <td><?php echo $mistry; ?></td>
                       <td><?php echo $job_card_work; ?></td>
                       <td><?php echo $job_card_date; ?></td>

                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <!-- <td colspan='4'>Total Calculation : </td> -->
                    <!-- <td colspan='6'>[quantity]</td> -->

                  </tr>";
                  } else {
                       echo "Truck not add";
                    exit();
                  }
                  ?>
   
              </table>


            </div>

          </div>

        </section>
      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

<!-- 
<input type="button" onclick="generate()" value="Export To PDF" /> 
  --Export To PDF FORMAT CODE
<script type="text/javascript">  
function generate() {  
    var doc = new jsPDF('p', 'pt', 'letter');  
    var htmlstring = '';  
    var tempVarToCheckPageHeight = 0;  
    var pageHeight = 0;  
    pageHeight = doc.internal.pageSize.height;  
    specialElementHandlers = {  
     
        '#bypassme': function(element, renderer) {  
        
            return true  
        }  
    };  
    margins = {  
        top: 80,  
        bottom: 60,  
        left: 20,  
        right: 20,  
        width: 100  
    };  
    var y = 200;  
    doc.setLineWidth(2);  
    doc.text(200, y = y + 30, "TOTAL MARKS OF STUDENTS");  
    doc.autoTable({  
        html: '#val',  
        startY: 10,  
        theme: 'grid',  
        columnStyles: {  
          0: {  
                cellWidth: 18,  
            }
          1: {  
                cellWidth: 18,  
            }, 
          2: {  
                cellWidth: 18,  
            },  
          3: {  
                cellWidth: 18,  
            },  
          4: {  
                cellWidth: 18,  
            }  
        },  
        styles: {  
            minCellHeight: 1  
        }  
    })  
    doc.save('modelreport.pdf');  
}  
</script> 
 -->
</body>
</html>
