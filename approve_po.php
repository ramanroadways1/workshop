 <?php  
?>  
<!DOCTYPE html>
<html>
<head>

  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- <form action="approve_po_insert.php" method="post"> -->
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Approve Purchase Order</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>

      <form id="temp_form" action="show_po_detail.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php $username =  $_SESSION['username'];
                   ?>
                  <thead>  
                       <tr>  
                        <th>id</th>
                        <td>Purchase Order</td>
                       <td>Party Name</td> 
                        <td>Party Code</td>
                        <td>Product Count</td>
                        <td>Total Amount</td>
                        <td>Date</td>
                        <td>Select</td>  
                           
                       </tr>  
                  </thead>  
                  <?php  
                   include("connect.php");
                 $sql = "SELECT date_purchase_key.id as id,date_purchase_key.purchase_order, party_name, party_code, date_purchase_key.date1,
                 count(productno) as productno,GROUP_CONCAT(productname) as productname, GROUP_CONCAT(rate) as rate,
                 GROUP_CONCAT(rate_master) as rate_master ,GROUP_CONCAT(diff) as diff, GROUP_CONCAT(quantity) as quantity, GROUP_CONCAT(amount) as amount,
                 GROUP_CONCAT(gst) as gst, GROUP_CONCAT(gst_amount) as gst_amount,sum(total) as total FROM date_purchase_key,insert_po WHERE 
                 date_purchase_key.key1 = insert_po.key1 AND approve_status=0  and date_purchase_key.username ='$username' AND insert_po.username = '$username' group by purchase_order order by date_purchase_key.id ASC";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
   
                $i=1;
                $id_customer=0;
                  while($row = mysqli_fetch_array($result))  
                  {  
                    $id = $row["id"];
                    $purchase_order = $row["purchase_order"];
                    $productno = $row["productno"];
                    $productname = $row['productname'];
                    $party_name = $row["party_name"];
                    $party_code = $row["party_code"];
                    // $rate_master = $row["rate_master"];
                    // $rate = $row["rate"];
                    // $quantity = $row["quantity"];
                    $diff = $row["diff"];
                    $amount=$row["amount"];
                    $gst=$row["gst"];
                    $gst_amount=$row["gst_amount"];
                    $total1 = $row["total"];
                      $total = round($total1,2);


                    $date1 = $row["date1"];
                    $hash = sha1($purchase_order);
                  ?>
                       <tr> 
                           <td><?php echo $id?>
                            <input type="hidden" id="id<?php echo $id; ?>" name="id[]"  value='<?php echo $id; ?>'>
                            <input type="hidden" id="purchase_order" name="purchase_order[]" form="main_form" value='<?php echo $purchase_order; ?>'>
                          </td>
                          <td>
                            <!-- <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                            <a href="show_po_detail.php?purchase_order=<?php echo $hash?>"><?php echo $purchase_order?></a> -->
                            <input type="submit" class="btn btn-primary btn-sm" name="purchase_order" form="temp_form"  value="<?php echo $purchase_order?>" >
                            <input type="hidden" name="" value="<?php echo $purchase_order?>">
                          </td>
                          <td><?php echo $party_name?>
                            <input type="hidden" id="party_name" form="main_form" name="party_name[]" value='<?php echo $party_name; ?>'>
                          </td> 
                          <td><?php echo $party_code?>
                            <input type="hidden" id="party_code" form="main_form" name="party_code[]" value='<?php echo $party_code; ?>'>
                          </td>
                          <td><?php echo $productno?>
                            <input type="hidden" id="productno" form="main_form" name="productno[]" value='<?php echo $productno; ?>'>
                          </td>
                          <!--  <td><?php echo $productname?>
                            <input type="hidden" id="productname"  form="main_form" name="productname[]" value='<?php echo $productname; ?>'>
                          </td>
                          <td><?php echo $rate_master?>
                            <input type="hidden" name="rate_master[]" form="main_form" id="rate_master" value='<?php echo $rate_master; ?>'></td>
                          <td><?php echo $rate?>
                            <input type="hidden" name="rate[]" form="main_form" id="rate" value='<?php echo $rate; ?>'>
                          </td>
                          <td><?php echo $diff?>
                            <input type="hidden" readonly="readonly"  form="main_form" id="diff" name="diff[]" value="<?php echo $diff; ?>" >
                          </td>
                          <td><?php echo $quantity?>
                            <input type="hidden" name="quantity[]" form="main_form" id="quantity" value='<?php echo $quantity; ?>'>
                          </td>
                          <td><?php echo $amount?>
                            <input type="hidden" readonly="readonly" form="main_form" id="amount"  name="amount[]" value="<?php echo $amount; ?>" >
                          </td>
                          <td><?php echo $gst?>
                            <input type="hidden" readonly="readonly" form="main_form" id="gst"  name="gst[]" value="<?php echo $gst; ?>" >
                          </td>
                          <td><?php echo $gst_amount?>
                            <input type="hidden" readonly="readonly" id="gst_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="gst_amount[]" value="<?php echo $gst_amount; ?>" >
                          </td> -->

                          <td><?php echo $total?>
                            <input type="hidden" readonly="readonly" form="main_form" id="total_amount" name="total[]"   value="<?php echo $total; ?>">
                          </td>
                          <td><?php echo $date1?>
                            <input type="hidden" readonly="readonly"  form="main_form" id="date1" name="date1[]" value="<?php echo $date1; ?>" >
                          </td>  
                          <td>
                            <button type="button" class="btn btn-success btn btn-sm"  onclick="addComplainFunc(<?php echo $id; ?>);" >Approve</button>
                          </td>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                      
                  }
                  ?>  
                </table>  
                    <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var id = val;
                                  var username = '<?php echo $username ?>'
                                    $.ajax({
                                       type:"post",
                                       url:"update_approve_status.php",
                                      data:'id='+id + '&username='+username,
                                        success:function(data){
                                           alert(data)
                                           location.reload(); 
                                                
                                          }
                                      });
                                 }
                    </script>

                <center>
                 <!--  <input type="submit" class="btn btn-primary" form="main_form" name="action[]" value="Approved"> -->
                </center><br>
              </div>  
            </div>  
          </div>
            
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

