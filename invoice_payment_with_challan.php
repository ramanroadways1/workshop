<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
 <script>
  function getinvoiceno(){
      var date = new Date();
      document.getElementById("invoiceno").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_po.php">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Generate Purchase Order</h1>
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" style="width: 100%;" id="pno" name="valueToSearch" placeholder="Search...">
          <span class="input-group-btn">
            <!-- <button type="button" onclick="MyFunc1()">Search</button> -->
            <button type="button" onclick="MyFunc1()" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
        <script>
          function MyFunc1()
          {
          jQuery.ajax({
              url: "search_p_no.php",
              data: 'pno=' + $('#pno').val() ,
              type: "POST",
              success: function(data) {
              $("#r1").html(data);
              },
              error: function() {}
          });
          }
         </script>
        <div id="r1" class="container">
        </div>
      </form>
    </section>
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Generate Purchase Order</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="purchase_order" class ="control-label">Purchase Order</label>
              <input type="text" style="width: 100%;" class="form-control" id="purchase_order" onclick="getpo();" name="purchase_order" placeholder="Purchase Order" />
            </div>

              <div class="form-group">
                  <label for="rate" class ="control-label">Rate/Unit</label>
                  <input type="text" style="width: 100%;" class="form-control" id="rate" name="rate" onclick="getrate();" placeholder="Rate/Unit">
              </div>

             <div class="form-group">
                <label for="quantity" class ="control-label">Quantity</label>
                <input type="text" style="width: 100%;" class="form-control" id="quantity" name="quantity" onclick="getrate();" placeholder="Quantity" >
             </div>

            <div class="form-group">
                <label for="amount" class ="control-label">Amount(without GST)</label>
                <input type="text" style="width: 100%;" class="form-control" id="amount" name="amount" onclick="getrate();" placeholder="Amount" >
            </div>

          </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="amount">G.S.T %</label>
                <select  class="form-control" style="width: 100%;" id="gst" name="gst" onclick="getgst()">
                    <option disabled="disabled" selected="selected">G.S.T Value</option>
                        <option class="form-control" value="5">5</option>
                        <option class="form-control" value="12">12</option>
                        <option class="form-control" value="28">28</option>
                        <option class="form-control" value="0">0</option>
                  </select> 
                
                </div>

              <div class="form-group">
                  <label for="gst_amount" class ="control-label">G.S.T Amount</label>
                  <input type="number" style="width: 100%;" class="form-control" id="gst_amount" name="gst_amount" onclick="getgst();" placeholder="GST Amount">
              </div>

              <div class="form-group">
                <label for="amount">G.S.T Type</label>
                <select  class="form-control" style="width: 100%;" id="gst_type" name="gst_type"  onclick="get_gst()">
                    <option disabled="disabled" selected="selected">G.S.T Type</option>
                        <option class="form-control" value="cgst_sgst">cgst_sgst</option>
                        <option class="form-control" value="igst">igst</option>
                  </select> 
              </div>

              <div class="form-group">
                   <label for="amount">Selected G.S.T</label>
                   <input type="text" style="width: 100%;" class="form-control prc" id="sel_gst" name="sel_gst" onclick="get_gst()"  placeholder="Selected G.S.T">
              </div>
                  
              <div class="form-group">
                <label for="total_amount" class ="control-label ">Total Amount</label>
                <input type="text" style="width: 100%;" class="form-control" id="total_amount" name="total_amount" onclick="gettotal();" placeholder="Total Amount">
              </div>
          
                  
            </div>
            <!-- /.col -->
          </div>

          <!-- /.row -->

        </div>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
     <center>
      <!-- <button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button> -->
     <button type="submit" href= "" class="btn btn-primary" formaction="insert_po.php">Submit</button></center>
    </div>
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

