    

 <?php  
 include("connect.php");
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Rate Detail</h1>
    </section> -->

    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Rate Detail</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>Product Number</td>  
                            <td>Product Name</td>  
                            <td>Rate</td>
                            <td>Lock_Unlock</td>  
                       </tr>  
                  </thead>  
                  <?php  
                  
                  $username =  $_SESSION['username'];
                  $sql = "SELECT * from rate where username = '$username'";
                  $result = $conn->query($sql);
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                        <tr>  
                          <td>'.$row["productno"].'</td>  
                          <td>'.$row["productname"].'</td>  
                          <td>'.$row["rate"].'</td>
                          <td>'.$row["lock_unlock"].'</td>  
                        </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
           
        </div>
        <!-- <div class="box-footer clearfix">
          <a href="rate.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Rate</a>
        </div>  -->
      </div>
          
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

