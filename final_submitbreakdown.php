<!DOCTYPE html>
<html>
<head>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
session_start();

$empcode = $_SESSION['empcode'];
$username= $_SESSION['username'];
$employee= $_SESSION['employee'];
include ("connect.php");

$truck_no = $conn->real_escape_string(htmlspecialchars($_POST['truck_no']));
$km = $conn->real_escape_string(htmlspecialchars($_POST['km']));
$mobile_no = $conn->real_escape_string(htmlspecialchars($_POST['mobile_no']));
$comp_date = $conn->real_escape_string(htmlspecialchars($_POST['comp_date']));
$comp_time = $conn->real_escape_string(htmlspecialchars($_POST['comp_time']));

$mrketproduct_name=$_POST['mrketproduct_name'];
$qty=$_POST['qty'];
$description=$_POST['description'];
$mrketproductamt=$_POST['mrketproductamt'];
$challan=$_POST['challan_no'];
$problems =$_POST['problems'];
$workgroup=$_POST['workgroup'];
$mistry_update=$_POST['updateremark'];
$cur_time=$_POST['cur_time'];
$cdate_time=$_POST['cdate_time'];
$mistry_mobail=$_POST['emiail_mobailm'];   
$mistry_name=$_POST['mistry_name'];               
$report_by =$_POST['report_by'];
$update_info =$_POST['update_info'];
$place_brakdown =$_POST['place_brakdown'];
$remark =$_POST['remark'];
$mrketproductamt =$_POST['mrketproductamt'];
$approve_by =$_POST['approve_by'];
$uniq_id =$_POST['uniq_id'];
$emiail_mobailm =$_POST['emiail_mobailm'];
$resiondealy =$_POST['resiondealy'];
$laburamount =$_POST['laburamount'];
$mrketproductamt =$_POST['mrketproductamt'];
$challan_no =$_POST['challan_no'];
$finalsub_date =$_POST['finalsub_date'];

$time_systemc =$_POST['time_systemc'];
$date_systemc =$_POST['date_systemc'];

$file_name_parent1 = "";
$file_name_parent2 = "";

try{

$conn->query("START TRANSACTION"); 

$status='Done';
$sql = "INSERT INTO `breakdownfinal`(truck_no,km,mobile_no,status,report_by,place_brakdown,approve_by,comp_date,comp_time,date_systemc,time_systemc,finalsub_date,problem,workgroup,mistry_mobail,mistry_name,laburamount,resiondealy,remark,employee,updateremark,uniq_id,note_update,log_update,c_time,mrketproductamt,product_name,challan,marketproduct,qty,challan_no,description) select '$truck_no','$km','$mobile_no','$status','$report_by','$place_brakdown','$approve_by','$comp_date','$comp_time','$date_systemc','$time_systemc','$finalsub_date',temp_breakdown.problems,temp_breakdown.workgroup,temp_breakdown.emiail_mobailm,temp_breakdown.mistry_name,temp_breakdown.laburamount,temp_breakdown.resiondealy,temp_breakdown.remarks,'$employee',temp_breakdown.updateremark,'$uniq_id',temp_updatelog.note_update,temp_updatelog.sysdatime,temp_breakdown.cur_time,temp_marketproduct.mrketproductamt,temp_marketproduct.mrketproduct_name,temp_marketproduct.myfile1,temp_marketproduct.myfil2,temp_marketproduct.qty,temp_marketproduct.challan_no,temp_marketproduct.description FROM temp_breakdown INNER join temp_marketproduct ON temp_marketproduct.uniq_id=temp_breakdown.uniq_id INNER join temp_updatelog ON temp_marketproduct.uniq_id=temp_updatelog.uniq_id WHERE temp_breakdown.uniq_id='$uniq_id' and temp_marketproduct.uniq_id='$uniq_id' Group by temp_breakdown.problems";


    if($conn->query($sql) === FALSE) 
    { 
    throw new Exception("Code 001 : ".mysqli_error($conn));        
    }

$sql1 ="UPDATE breakdown SET status ='Done' WHERE uniq_id='$uniq_id'";
  if($conn->query($sql1) === FALSE) 
  { 
  throw new Exception("Code 002 : ".mysqli_error($conn));        
  }

$file_name= basename(__FILE__);
$type=1;
$val="Truck Add :"."Truck Name-".$uniq_id;
          
$sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$username','$empcode','$val','$type')";
if ($conn->query($sql)===FALSE) {
throw new Exception("Error");  
        
              }  
 $conn->query("COMMIT");

echo '<script>
swal({
     title: "Truck Add Successfully...",
      text: "Created ",
      type: "success"
      }).then(function() {
          window.location = "pending_breakdown.php";
      });
</script>';
}

catch (Exception $e) {

$conn->query("ROLLBACK");
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

echo '<script>
  swal({
     title: "truck Not Insert...",
     text: "",
     icon: "error",
     button: "Back"
      }).then(function() {
          window.location = "pending_breakdown.php";
      });
</script>';

$file_name = basename(__FILE__);        
$sql = "INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
if ($conn->query($sql) === FALSE) { 
echo "Error: " . $sql . "<br>" . $conn->error;
}
}

$conn->close();
?>