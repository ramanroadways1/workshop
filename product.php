<!DOCTYPE html>
<html>
<head>
 <?php 
    include("header.php");
    include("aside_main.php");
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_product.php" autocomplete="off">
  <div class="content-wrapper">
   <!--  <section class="content-header">
      <h1>Masters</h1>
    </section> -->
    <section class="content">

       
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Create Product Master</h3>
         <!--<div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>-->
        </div>
        <!--/.box-header-->
        <div class="box-body">
            <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">
              <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">
              <input type="hidden" name="empcode" value="<?php echo $_SESSION['empcode']; ?>">
          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group">
                <label for="productno">Product Number<font color="red">*</font></label>
                <input type="text" style="width: 100%;" onblur="MyFunc()" id="productno" class="form-control" name="productno" placeholder="Product Number" required>
              </div><div id="result22"></div>
              
              <script>
                    function MyFunc()
                    {
                      var productno = $('#productno').val();
                      if(productno!='')
                      {
                        $.ajax({
                            type: "POST",
                            url: "chkproductno.php",
                            data:'productno='+productno ,
                            success: function(data){
                                $("#result22").html(data);
                            }
                            });
                      }
                    }     
            </script>

              <div class="form-group">
              <label for="company" id="company">Company Of Product<font color="red">*</font></label>
              <div class="row">
                <div class="col-md-10">
                 <select name="company" style="width: 100%;" id="company" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Company</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT company FROM company");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['company'])?>" >
                        <?php 
                              echo ($row['company']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
                 <div class="col-md-2">
                  <button type="button" style="width: 100%;" class="btn btn-info" data-toggle="modal" data-target="#myModal" >Add</button>
                 </div>
                 <div id="result"></div>
                 
                </div>
             </div> 

             
              <div class="form-group">
                <label for="productname">Product Name<font color="red">*</font></label>
                <input type="text" style="width: 100%;" class="form-control" name="productname" placeholder="Product Name" required>
              </div>
<!-------------------------------------------------------------------->
              <div class="form-group">
              <label for="producttype" id="producttype">Product Type<font color="red">*</font></label>
              <div class="row">
                <div class="col-md-10">
                 <select name="producttype" style="width: 100%;" id="producttype" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Product</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT product FROM product_temp");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['product'])?>" >
                        <?php 
                              echo ($row['product']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
                 <div class="col-md-2">
                  <button type="button" style="width: 100%;" class="btn btn-info" data-toggle="modal" data-target="#myModal1" >Add</button>
                 </div>
                 <div id="result1"></div>
                </div>
             </div> 
<!------------------------------------------------------------------------->
<!--<div class="form-group">
                <label for="producttype">Product Type<font color="red">*</font></label>
                <input type="text" style="width: 100%;" class="form-control" name="producttype" placeholder="Product Type" required>
    </div>-->

              <div class="form-group">
                <label for="unit">Unit<font color="red">*</font></label>
                <select style="width: 100%;" class="form-control" name="unit">
                    <option disabled="disabled" selected="selected">Unit</option>
                    <option value="liter">liter</option>
                    <option value="pieces">Pieces</option>
                    <option value="boxes">Boxes</option>
                     <option value="miter">Meter</option>
                    <option value="kg">K.g</option>
                    
                </select>
              </div>
              
            </div>

            <!-- /.col -->
            <div class="col-md-6">

<!-------------------------------------------------------------------->

            <div class="form-group">
              <label for="product_group" id="product_group">Group<font color="red">*</font></label>
              <div class="row">
                <div class="col-md-10">
                 <select name="product_group" style="width: 100%;" id="product_group" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Group</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT productgroup FROM group_temp");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['productgroup'])?>" >
                        <?php 
                              echo ($row['productgroup']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
                 <div class="col-md-2">
                  <button type="button" style="width: 100%;" class="btn btn-info" data-toggle="modal" data-target="#myModal2" >Add</button>
                 </div>
                 <div id="result2"></div>
                </div>
             </div> 
<!------------------------------------------------------------------------->
<!-- 
              <div class="form-group">
                  <label for="group">Group<font color="red">*</font></label>
                  <input type="text" style="width: 100%;" class="form-control " name="product_group" placeholder="group" required>
              </div> -->
              <div class="form-group">
                <label for="subgroup">Sub Group<font color="red">*</font></label>
                <input type="text" style="width: 100%;" class="form-control " name="sub" placeholder=" sub group" required>
              </div>
              <div class="form-group">
                <label for="pro_loc">Product Location(Ahmedabad)<font color="red">*</font></label>
                <select name="pro_loc" style="width: 100%;" id="pro_loc" class="form-control"> 
                    <option disabled="disabled" selected="selected">Select Rack Location</option>
                    <?php 
                      include ("connect.php");
                        $get=mysqli_query($conn,"SELECT rackno FROM rack");
                           while($row = mysqli_fetch_assoc($get))
                          {
                    ?>
                    <option value = "<?php echo($row['rackno'])?>" >
                      <?php 
                            echo ($row['rackno']."<br/>");
                      ?>
                    </option>
                      <?php
                      }
                    ?>
                </select>
              </div>
            </div>
            <!-- /.col -->
          </div>

          <!-- /.row -->

        </div>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
    <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
       <a href="product_view.php" class="btn btn-warning">Show Product</a>
     </center>
    </div>
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form action="insert_company.php" id="AddCompany">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Company</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>company<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="company" name="company" placeholder="Company"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#AddCompany").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_company.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>

<!-----------------Creat Product----------------------- -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form action="insert_pro.php" id="Addproduct">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Product</h4>
        </div> 
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>Product<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="product" name="product" placeholder="product"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#Addproduct").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_pro.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result1").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>
  <!---------------------------------------- -->

  <!-----------------Creat Product----------------------- -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form action="insert_group.php" id="productgroup">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Group</h4>
        </div> 
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>Group<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="productgroup" name="productgroup" placeholder="product Group"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#productgroup").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_group.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result2").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>
  <!---------------------------------------- -->