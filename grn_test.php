<!DOCTYPE html>
<html>
<head>
  </style>
  
  <?php 

  include("header.php");
  // include("aside_main.php");
  include("connect.php");
session_start();
  $username =  $_SESSION['username'];
 
  if(isset($_SESSION['po_session']))
  {
    $po_random = $_SESSION['po_session'];
    $party_set=1;
    $result = mysqli_query($conn,"SELECT * FROM add_product_sample WHERE random_id='$_SESSION[po_session]' and username='$username'");
    if(mysqli_num_rows($result)>0)
    {
      $row = mysqli_fetch_array($result);
      $party_code_from_db = $row["party_code"];
      $party_name_from_db = $row["party_name"];
      $gst_type_from_db = $row["gst_type"];
    }
    else
    {

    $po_random="NA";
    $party_set=0;
    $party_code_from_db="";
    $party_name_from_db ="";
    $gst_type_from_db="";

    }
    
  }
  else
  {
    $po_random="NA";
  $party_set=0;
  $party_code_from_db="";
  $party_name_from_db ="";
  $gst_type_from_db="";
  }
 

  ?>
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

.ui-autocomplete { z-index:2147483647; }
</style>

<script type="text/javascript"> 





$(function()
{    
$( "#party_code_to_search" ).autocomplete({
source: 'party_code_to_search.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});

</script>  

<script>
/*function get_party_name(val) {
    $.ajax({
    type: "POST",
    url: "get_party_name.php",
    data:'party_code='+val,
    success: function(data){
        $("#party_name").html(data);
    }
    });
}*/
</script>

 <style type="text/css">
    <style type="text/css">
  @media print {
  button {
    display: none !important;
  }
  input,
  textarea,select {
    border: none !important;
    box-shadow: none !important;
    outline: none !important;
    display: none !important;
  }
  /*#print,#sub,#new_prblm,#issue,#sno,#main_heading {
    display: none;
  }*/

  #driver_complain {
    width: 4px;
  }
  #amount{
    width: 1;
  }
   table, tr,body,h4,form  {
        height: auto;
        font-size: 16pt;
        font: solid #000 !important;
        }
         table {
       border: solid #000 !important;
        border-width: 1px 0 0 1px !important;
    }
    th, td {
        border: solid #000 !important;
        border-width: 0 1px 1px 0 !important;
    }
     
}
    
}

</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Generate Purchase Order</h1><br>
      </section> -->
      
      
   
<section class="content">
   
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Generate Purchase Order</h3>

          <!-- <div class="box-tools pull-right">
            
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div>
        <!-- /.box-header -->

        <form action="insert_po.php" id="MyForm1" method="post" autocomplete="off">
          <div class="box-body">
            <div class="row-md-1">
              <div class="col-md-12">
               <div class="form-group">
                <?php
                if($party_set==0)
                {
                echo '<div class="col-md-8">
                        <div class="col-md-2">
                              <b>Party Name</b>
                        </div>
                        <div class="col-md-8">
                          <input type="text" id="party_name_to_search" name="party_name1" placeholder="Enter Party Name..." class="form-control" required />
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-search" ><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                      ';
                }
                else
                {
                  echo '
                    <div class="col-md-6">
                      <b>Party Name</b>
                      <input type="text" class="form-control" value="'.$party_name_from_db.'" name="party_name_11" class="form-control" readonly />
                    </div>
                    <div class="col-md-6">
                      <b>Party Code</b>
                      <input type="text" class="form-control" value="'.$party_code_from_db.'" name="party_code_11" class="form-control" readonly />
                    </div>
                  ';
                }
                ?>
                </div>
              </div>
              <div class="form-group col-md-12"> <br>   
                <div class="table-responsive">
                  <div id="result"></div> 
                </div>
              </div>
            </div>
              
          <div class="form-group col-md-12">                
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" style="font-size:15px;">Add Product</button>
          </div> 
        
          <div class="form-group col-md-12" style="overflow-x:auto;">
     
          <?php
          $qry1=mysqli_query($conn,"SELECT * FROM add_product_sample WHERE random_id='$po_random'  and username='$username'");
          $sum=mysqli_query($conn,"SELECT SUM(qty) as qty,SUM(amount) as amount,SUM(gst_amount) as gst,SUM(total_amount) as total FROM 
          add_product_sample WHERE random_id='$po_random' ");
          $row2=mysqli_fetch_array($sum);
          ?>
          <table  class="table table-hover small-text" id="tb">
           <tr class="tr-header">
            <td>Product Number</td>
            <td>Product Name</td>
            <td>Rate(from Rate Master)</td>
            <td>Rate/Unit</td>
            <td>Quantity</td>
            <td>Amount</td>
            <td>Gst</td>
            <td>Gst Amount</td>
            <td>Gst Type</td>
            <td>Gst Acc. Type</td>
            <td>Total Amount</td>
          </tr>
          <?php
          if(mysqli_num_rows($qry1)>0)
          {

            while($row=mysqli_fetch_array($qry1))
            {
            echo '<tr>
              <td>'.$row["p_no"].'</td>
              <td>'.$row["productname"].'</td>
              <td>'.$row["rate_master"].'</td>
              <td>'.$row["rate_unit"].'</td>
              <td>'.$row["qty"].'</td>
              <td>'.$row["amount"].'</td>
              <td>'.$row["gst_value"].'</td>
              <td>'.$row["gst_amount"].'</td>
              <td>'.$row["gst_type"].'</td>
              <td>'.$row["gst_acc_type"].'</td>
              <td>'.$row["total_amount"].'</td>
              </tr>
              ';
            }

            echo

             "<tr>
              <td colspan='2'>Total Calculation : </td>
              <td></td>
              <td></td>
              <td>$row2[qty]</td>
              <td>$row2[amount]</td>
              <td></td>
              <td>$row2[gst]</td>
              <td></td>
              <td></td>
              <td>$row2[total]</td>
            </tr>";
          }
          else
          {
            echo "<td colspan='15'> No record found !</td>";
          }
          ?>
         </table>
        </div><br><br>
        <?php
          if($party_set==0)
          {
          
          }
          else
          {
            echo '
              <input type="button" color="Primary" style="margin-left:2%;" class="btn btn-primary hide-from-printer" onclick="myFunction()" value="Print P.O">
            ';
          }
        ?>
        <center> 
                  <script>
                    function myFunction() {
                      window.print();
                    }
                  </script> 
          <!-- <input type="button" color="Primary" class="btn btn-primary" onclick="window.location.href='print_po.php'" value="Print P.O."> -->
        

          <input type="button" value="Submit" class="btn btn-primary" onclick="window.location.href='insert_po.php'" />
          <a href="generate_po_operation.php" class="btn btn-success">Update PO</a>
          <a href="get_pdfpo.php" class="btn btn-warning">PDF Get PO</a>
        </center>
       <!--  <a style="position: absolute; right: 0;  margin-right:2%; " href="show_po_not_aprv.php" class="btn btn-warning">Show PO which are not approved</a> -->
       
      </div>

    </form>
  </div>

 </div>
</section>

<form action="save_new_product.php" id="form" method="POST" autocomplete="off">
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Generate Purchase Order</h4>
        </div>
        <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">
         <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">
         <input type="hidden" name="empcode" value="<?php echo $_SESSION['empcode']; ?>">
      <input type="hidden" value="<?php echo $party_set; ?>" id="party_set" name="party_set" />

        <?php
        if($party_set==0)
        {
        echo '
        <input type="hidden" id="party_code_1" name="party_code_1" />';
        }
        else
        {
          echo '
        <input type="hidden" value="'.$party_code_from_db.'" name="party_code_1" />';
        }
        ?>

        <?php
        if($party_set==0)
        {
        echo '
        <input type="hidden" id="party_name_1" name="party_name_1" />';
        }
        else
        {
          echo '
        <input type="hidden" value="'.$party_name_from_db.'" name="party_name_1" />';
        }
        ?>
    
        <div class="modal-body">
          <div class="row">
             <div class="col-md-6">
              <div class="form-group">
                 <label>Search Product By</label>
                 <select onchange="SearchBy(this.value)" class="form-control" required id="search_product" name="search_product">
                    <option value="">---Select---</option>
                    <option value="PNO">Product Number</option>
                    <option value="PNAME">Product Name</option>
                    </select>
              </div>
            </div>
            
            <script>
            function SearchBy(elem)
            {
              if($('#party_set').val()=='0' || $('#party_set').val()=='')
              {
                alert('Please select party first !');
                window.location.href='generate_po.php';
                $('#search_product').val('');
              }
              else
              {
              $('.pname1').val(''); 
              $('.pno1').val(''); 
              $('.pno1').attr('id','productno');  
              $('.pname1').attr('id','productname');          
                
              if(elem=='PNO')
              {
                $('.pname1').attr('onblur',""); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',false);  
                $('.pno1').attr('onblur',"get_data1(this.value)");  
                $('.pno1').attr('id','productno');  
                $('.pname1').attr('id','');   
              }
              else if(elem=='PNAME')
              {
                $('.pno1').attr('onblur',""); 
                $('.pname1').attr('onblur',"get_data2(this.value)");  
                $('.pname1').attr('readonly',false);  
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('id',''); 
                $('.pname1').attr('id','productname');  
              }
              else
              {
                $('.pname1').attr('onblur',''); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('onblur',''); 
                $('.pno1').attr('id','productno');  
                $('.pname1').attr('id','productname');  
              }
              }
            }
            </script>
            
            <script type="text/javascript">
               function get_data1(val) {
                  $.ajax({
                    type: "POST",
                    url: "get_data1.php",
                    data:'productno='+val,
                    success: function(data){
                      //alert(data);
                        $("#rate_master222").html(data);
                    }
                    });
                }
               
                $(function()
                  { 
                  $( "#productno" ).autocomplete({
                  source: 'po_no_search.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                  alert('Product No does not exists.');
                  } 
                  },
                  focus: function (event, ui) {return false; } }); });
                
            </script>
            
                
            <div id="rate_master222"></div>
            <div class="col-md-6">    
                <div class="form-group">
                  <label >gst_type</label>
                  <!-- <input type="text" class="form-control" placeholder="gst type" onblur="MyGst(this.value);" id="gst_type"  name="gst_type" onblur="MyGst(this.value)"; readonly/ > -->
                  <?php
                    if($party_set==0)
                    {
                    echo '
                    <input type="text" class="form-control" placeholder="gst type" onblur="MyGst(this.value);" id="gst_type_1"  name="gst_type_1" readonly/ >';
                    }
                    else
                    {
                      echo '
                      <input type="text" class="form-control" value="'.$gst_type_from_db.'" placeholder="gst type" onblur="MyGst(this.value);" id="gst_type_1"  name="gst_type_1" readonly/ >';
                    }
                    ?>
                </div>
            </div>
            
            <div id="pno_div" class="col-md-6">
              <div class="form-group">
                <label>Product number</label>
                <input type="text" class="form-control pno1" id="productno" name="productno" placeholder="product number" required />
              </div>
            </div>
           
             <script type="text/javascript">
               function get_data2(val) {
                  $.ajax({
                    type: "POST",
                    url: "get_data2.php",
                    data:'productname='+val,
                    success: function(data){
                         $("#rate_master222").html(data);
                      }
                    });
                }
                $(function()
                { 
                  $("#productname").autocomplete({
                  source: 'po_name_search.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    location.reload();
                  alert('Product Name does not exists MAIN PAGE.');

                  } 
                },
                focus: function (event, ui) { return false; } }); });
            </script>
              
            <div class="col-md-6">
              <div class="form-group">
                <label>Rate master</label>
                <input class="form-control" name="rate_master" id="rate_master" onblur="getdiff(this.value);" placeholder="rate master"  readonly>
              </div>
            </div>
                 
            <div id="pname_div" class="col-md-6">
              <div class="form-group">
                <label>Product name</label>
                <input type="text" class="form-control pname1" name="productname" id="productname"  placeholder="product Name" required/>
              </div>
            </div>
            
            <div class="col-md-6">     
              <div class="form-group">
                 <label>Rate unit</label>
                 <input type="Number" step="any" oninput="ChkForRateMaxValue();sum1();" onblur="getdiff(this.value);"  id="rate"  class="form-control" name="rate_unit" placeholder="rate unit" required/>
              </div>
             </div>

             <input type="hidden" class="form-control" name="lock_unlock" id="lock_unlock" readonly required>
                    
             <div class="col-md-6"> 
              <div class="form-group">
                 <label>Quantity</label>
                 <input type="number" oninput="sum1()"   id="quantity"  class="form-control" name="qty" placeholder="Quantity" required/>
              </div>
             </div>

             <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var fix_rate = Number($('#rate_master').val());
                 var lock = $('#lock_unlock').val();
                 if(fix_rate=="")
                 {
                   alert('You can not enter rate before rate_master selected. ');
                     $('#rate').val('');
                 }
                 else
                 {
                    if(lock==1)
                     {
                       if(myrate>fix_rate)
                       {
                         alert('You can not exceed Master Rate. Master rate is '+ fix_rate);
                         $('#rate').val('');
                       }
                     }
                 }
                }
               </script>

              <script>
                
                function sum1()
                {
                 var qty = Number($('#quantity').val());  
                 var rate = Number($('#rate').val());  
                 var gst = Number($('#gst').val());  
                 
                var amt = qty*rate;
                 
                 $('#amount').val((amt).toFixed(2));
                 $('#gst_amount').val((amt*gst/100).toFixed(2));
                 $('#total_amount').val((Number($('#gst_amount').val())+Number($('#amount').val())).toFixed(2));
                }
              </script>

            <div class="col-md-6">    
              <div class="form-group">
                 <label>Rate Difference</label>
                 <input type="text"   id="diff" class="form-control" name="diff" placeholder="Rate Difference" readonly required/>
              </div>
            </div>
                    
              <script>
                function getdiff(i)
                {
                  var rate1 = Number($('#rate_master').val());  
                  var rate2 = Number($('#rate').val());
                  var difference = rate1-rate2;
                  $('#diff').val(difference);
                 // $('#diff').val(Number($('#rate_master').val())-Number($('#rate').val()));
                }
              </script>
              
             <div class="col-md-6"> 
              <div class="form-group">
                 <label>Amount</label>
                 <input type="text"   id="amount" class="form-control" name="amount" placeholder="amount" readonly required/>
              </div>
             </div>

                    
            <div class="col-md-6">   
             <div class="form-group">
                 <label>gst value</label>
                 <select onchange="sum1();MyGst();" class="form-control" required id="gst" name="gst_value">
                    <option value="0">0</option>
                    <option value="5">5</option>
                    <option value="12">12</option>
                    <option value="18">18</option>
                    <option value="28">28</option>
                  </select>
                </div>
              </div>
              
            <div class="col-md-6">   
              <div class="form-group">
                  <label>gst_amount</label>
                  <input type="text"  class="form-control" id="gst_amount" name="gst_amount" readonly placeholder="gst amount" />
              </div>
            </div>

            
          <script>
              function MyGst(elem)
              {
                var gst = $('#gst_type_1').val();
              if(gst=='cgst_sgst')
              {
                 $('#gst_acc_type').val($('#gst').val()/2);
              }
              else
              {
                 $('#gst_acc_type').val($('#gst').val());
              }
            }
          </script>

            <div class="col-md-6">        
                <div class="form-group">
                  <label>Selected Gst</label>
                  <input type="text"  class="form-control "  id="gst_acc_type" onblur="MyGst(this.value);" name="gst_acc_type" readonly placeholder="Selected G.S.T" required>
                </div>
           </div>

             <div class="col-md-6">   
                <div class="form-group">
                  <label>total amount</label>
                  <input type="text"  id="total_amount" class="form-control"  name="total_amount"  placeholder="Total Amount" required readonly>
                </div>
             </div>
          </div>
        </div>
        <div class="modal-footer">
          <!--  <input type="reset" align="right" name="Reset" value="Reset" tabindex="50"> -->
          <button type="submit" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
<script type="text/javascript">     
$(function()
{    
$( "#party_name_to_search" ).autocomplete({
source: 'fetch_party_name_autocomplete.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});
</script>
<script>
$(document).ready(function (e) {
$("#MyForm1").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "search_party.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 

