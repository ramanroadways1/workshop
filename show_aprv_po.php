    


 <?php  
 include("connect.php");
 $sql = "SELECT date_purchase_key.id as id,date_purchase_key.purchase_order, party_name, party_code, date_purchase_key.date1,GROUP_CONCAT(productno) as productno,GROUP_CONCAT(productname) as productname, GROUP_CONCAT(rate) as rate, GROUP_CONCAT(rate_master) as rate_master ,GROUP_CONCAT(diff) as diff, GROUP_CONCAT(quantity) as quantity, GROUP_CONCAT(amount) as amount, GROUP_CONCAT(gst) as gst, GROUP_CONCAT(gst_amount) as gst_amount,GROUP_CONCAT(total) as total FROM date_purchase_key,insert_po WHERE date_purchase_key.key1 = insert_po.key1 AND approve_status=1 group by purchase_order";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Product Detail</h1>
    </section>

    <section class="content">

    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Product Detail</h3>
        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>Purchase Order</td>
                            <td>Party Name</td>
                            <td>Party Code</td>
                            <td>Product Number</td>
                            <td>Product Name</td>
                            <td>Rate(from Rate Master)</td>
                            <td>Rate</td>
                            <td>Difference In Rates</td>
                            <td>Quantity</td>
                            <td>Amount</td>
                            <td>Gst</td>
                            <td>Gst Amount</td>
                            <td>Total Amount</td>
                            <td>Date</td>  
                           
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr> 
                            <td>'.$row["purchase_order"].'</td>
                            <td>'.$row["productno"].'</td>  
                            <td>'.$row["productname"].'</td>  
                            <td>'.$row["party_name"].'</td>
                            <td>'.$row["party_code"].'</td>
                            <td>'.$row["rate_master"].'</td>
                            <td>'.$row["rate"].'</td>
                            <td>'.$row["quantity"].'</td>  
                            <td>'.$row["diff"].'</td>
                            <td>'.$row["amount"].'</td>  
                            <td>'.$row["gst"].'</td>
                            <td>'.$row["gst_amount"].'</td>
                            <td>'.$row["total"].'</td>  
                            <td>'.$row["date1"].'</td>  
                             
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2019<a href="https://adminlte.io">RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

