<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- <form action="approve_po_insert.php" method="post"> -->
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
      Upload Invoice Over GRN</h3>
        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>

      <form id="temp_form" action="show_detail_invoice_over_grn.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php $username =  $_SESSION['username'];
                   ?>
                  <thead>  
                       <tr>  
                         <th style="display: none;" scope="row">Id</th>
                                <td>GRN Number</td>
                                <td>Purchase Order</td>
                                <td>party name</td>
                                <td>product Numbur</td>
                                <td>New Total Amount</td>
                                <td>Challan File</td>
                                <td style="display: none;">Done</td>
                           
                       </tr>  
                  </thead>  
                              <?php  
                               include("connect.php");
                                  $username =  $_SESSION['username']; 
                                  $grn_no = $_POST['grn_no'];
                                  $query = "SELECT id, party_name, grn_no,key1, productno, purchase_order,new_total, challan_file FROM files_upload WHERE  username='$username' AND length(challan_file) > 10 and approve_status='0' and grn_no='$grn_no' ";
                                 
                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){

                                   while($row = mysqli_fetch_array($result)){
                                   $id = $row['id'];
                                   $key1 = $row['key1'];
                                   $grn_no=$row['grn_no'];
                                   $party_name=$row['party_name'];
                                   $productno=$row['productno'];
                                   $purchase_order =$row['purchase_order'];
                                   $new_total=$row['new_total'];
                                   $challan_file = $row['challan_file'];
                                   $challan_file1=explode(",",$challan_file);
                                   $count3=count($challan_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                    <td> <?php echo $grn_no; ?><input type="hidden" name="grn_no[]"  id="gn<?php echo $id_customer; ?>" value='<?php echo $grn_no; ?>'></td> 
                                  <!--   <td>
                                       <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $grn_no?>" >
                                          <input type="hidden" name="" value="<?php echo $grn_no?>">
                                    </td>
 -->
                                    <td> <?php echo $purchase_order; ?><input type="hidden" name="purchase_order[]"  id="po<?php echo $id_customer; ?>" value='<?php echo $purchase_order; ?>'></td>

                                    <td><?php echo $party_name; ?><input type="hidden" name="party_name[]" id="pn<?php echo $id_customer; ?>" value='<?php echo $party_name; ?>'></td>

                                    <td> <?php echo $productno; ?><input type="hidden" name="productno[]"  id="pno<?php echo $id_customer; ?>" value='<?php echo $productno; ?>'></td>

                                    <td> <?php echo $new_total; ?><input type="hidden" name="new_total[]"  id="nt<?php echo $id_customer; ?>" value='<?php echo $new_total; ?>'></td>

                                     <td>
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        
                                        ?>
                                         <a href="<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }

                                      ?>
                                     </td>

                                    <td style="display: none;">
                                      <input type="radio" name="id_customer[]" id="i<?php echo $id_customer; ?>" onclick="myFunction1(<?php echo $id_customer; ?>)" value='<?php echo $id_customer; ?>'>
                                    </td>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                      
                  }
                  ?>  
                </table>  
                    <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var id = val;
                                  var username = '<?php echo $username ?>'
                                    $.ajax({
                                       type:"post",
                                       url:"update_approve_status.php",
                                      data:'id='+id + '&username='+username,
                                        success:function(data){
                                           alert(data)
                                           location.reload(); 
                                                
                                          }
                                      });
                                 }
                    </script>

                <center>
                 <!--  <input type="submit" class="btn btn-primary" form="main_form" name="action[]" value="Approved"> -->
                </center><br>
              </div>  
            </div>  
          </div>
            
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

