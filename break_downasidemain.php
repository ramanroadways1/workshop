<?php 
  session_start(); 

    if (!isset($_SESSION['username'])) {
      $_SESSION['msg'] = "You must log in first";
      header('location: login.php');
    }
    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['username']);
      header("location: login.php");
    }
?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>M</span>
      <span class="logo-lg"><i class="fa fa-truck"></i><b>Break</b> Down</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <br>
         <?php if (isset($_SESSION['success'])) : ?>
        <div class="error success" >
          <h3>
            <?php 
              echo $_SESSION['success']; 
              unset($_SESSION['success']);
            ?>
          </h3>
        </div>
      <?php endif ?>

      <!-- logged in user information -->
      <?php  if (isset($_SESSION['username'])) : ?>
        <p style="float:right; margin-right:20px;">Welcome <strong>
          <?php echo $_SESSION['username'];
            
           echo "/";


         echo $_SESSION['employee'];
 ?>
          </strong>

        <a href="login.php?logout='1'" style="color: red;">logout</a> </p><br>
      <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="add_breakdown.php"><i class="fa fa-truck"></i> <span>Add Vehicle</span></a>
        </li>
        <li>
          <a href="pending_breakdown.php"><i class="fa fa-book"></i> <span>Pending Completion</span></a>
        </li>
         <li>
          <a href="final_breakdown.php"><i class="fa fa-book"></i> <span>Approve Completion</span></a>
        </li>

        <li>
          <a href="report_datebreak.php"><i class="fa fa-check"></i> <span>Reports</span></a>
        </li> 
          <li>
          <a href="add_problemsmaster.php"><i class="fa fa-check"></i> <span>Add Problems Master</span></a>
        </li>
      </ul>
    </section>
  </aside>