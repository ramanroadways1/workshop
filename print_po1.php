 
 <head>
<!-- <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/minty/bootstrap.min.css">
<link href="toast.css" rel="stylesheet"> 
<style type="text/css">
body
{
  font-family: Arial;
  font-size: 10pt;
}
table
{
  border: 1px solid #ccc;
  border-collapse: collapse;
}
table th
{
  background-color:   #FFFFFF;
 /* color: #333;*/
  font-weight: bold;
}
table th, table td
{
  padding: 5px;
  border: 1px solid #ccc;
}
</style>
<style>
@media print {
#pdf-button,#link {
display: none;
}
#employee_data{
width: 100%;
}
body {
 zoom:100%; 
}
table, tr,body,h4,form  {
height: auto;
font-size: 11pt;
font: solid #000 !important;
}
table {
border: solid #000 !important;
border-width: 1px 0 0 1px !important;
}
th, td {
border: solid #000 !important;
border-width: 0 1px 1px 0 !important;
}

}

</style>
<style type="text/css">
  #resp-table {
        width: 100%;
        display: table;
    }
    #resp-table-body{
        display: table-row-group;
    }
    .resp-table-row{
        display: table-row;
    }
    .table-body-cell{
        display: table-cell;
        border: 1px solid #dddddd;
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
    }
</style>
<?php 
include("connect.php");

       $valuetosearch=$_POST['purchase_order'];
       $sql="SELECT party.*,insert_po.*,date_purchase_key.purchase_order,date_purchase_key.key1,Date(date_purchase_key.date1)as maindate FROM insert_po LEFT JOIN date_purchase_key on insert_po.key1=date_purchase_key.key1 Left join party on insert_po.party_code=party.party_code  where  date_purchase_key.purchase_order='$valuetosearch'";
    // echo $sql;
        $res=$conn->query($sql);

        if($row=mysqli_fetch_array($res))
        { 
         $purchase_order=$row["purchase_order"];
         $maindate=$row["maindate"];
         $party_name=$row["party_name"];
         $address=$row["address"];
         $email=$row["email"];
         $gstin=$row["gstin"];
         $mobile_no=$row["mobile_no"];

        }

        $newDate = date("d-m-Y", strtotime($maindate));  

?> 
<div id="tblCustomers" class="table-responsive">  
    <br>
<div style="margin-top: 14px ;"class="table-body-cell" id="resp-table" >
 <center><div style="margin-top: 20px;"><b><u>PURCHASE ORDER</u></b></div></center>
<div class="col-md-12" style="margin-top: 20px;">
  <div class="row">
    <div class="col-md-3">
        <div style="margin-top: 14px ;"class="table-body-cell" id="resp-table" >
          <div>RAMAN ROADWAYS PVT.LTD</div>
         <span class="company-name">805, Samedh Complex,</span>
          <span class="spacer"></span>
          <div class="resp-table-row">Nr. Assoiate Petrol Pump</div>
          <div>C.G.Road Ahmedabad - 380006</div>
          <span class="clearfix"></span>
          <div>ahmedabad@ramanroadways.com</div>
          <div>GSTIN/UIN - 24AAGCR0742P1Z5</div>
          <div>Phone no: 079-26460631</div>
      </div>
      </div>
      <div class="col-md-3" style="margin-left: 510px;">
          <div style=" margin-top: 14px ;" class="table-body-cell" id="resp-table">
          <div><b><u>Purchase Order</u></b></div>
         <span class="company-name">Date-<?php echo $newDate;?></span>
          <span class="spacer"></span>
          <div>PO.-<?php echo $purchase_order; ?></div>
        </div>
        </div>
  </div>
</div>

<div class="col-md-12">
  <div class="row">
        <div class="col-md-3">
       <div style="margin-top: 14px ;"class="table-body-cell" id="resp-table">
        <div><b><u>DELIVERY ADDRESS:</u></b></div>
         <span class="company-name">RAMAN ROADWAYS PVT. LTD.</span>
                <span class="spacer"></span>
                <div>Servey No. 1032/1033</div>
                <div>Near Cadila Patiya, Sarkhej dholka road,</div>
                <span class="clearfix"></span>
                <div>Visalpur - 382210</div>
                <div>Mo. No. 9879202860/9879201153</div>
                <div>workshop@ramanroadways.com</div>
      </div>
    </div>
    <div class="col-md-3"style="margin-left: 510px;">
      <div style="margin-top: 5px ;"class="table-body-cell" id="resp-table">
          <div><b><u>SUPPLIER :</u></b></div>
          <span class="company-name"><?php echo $party_name;?></span>
          <span class="spacer"></span>
          <div>Addresh.-<?php echo $address; ?></div>
          <div>Email-<?php echo $email; ?></div>
          <div>GST NO-<?php echo $gstin; ?></div>
          <div>Contact-<?php echo $mobile_no; ?></div>

      </div>
      </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row">
         <div class="col-md-4"style=" margin-top: 14px ;">
        <div><b><u>REMARKS</u></b></div>
      </div>
      </div>
    </div>

      <table  class="table table-striped table-bordered" style="font-size: 13px; margin-top: 100px; background-color:#FFFFFF; font-family: verdana; width: 90%" border="1">  
        <thead style="background-color: #FFFFFF; color:#000000;">  
             <tr>  
              <td scope="row" style="display: none;">date Purchase Id</td>
                 <td scope="row" style="display: none;">id</td>
                      <td>Purchase Order</td>
                      <td>Party Name</td>
                      <td>Product No</td>
                      <td>Product Name</td>
                      <td>Rate Master/rate</td>
                      <td>Rate/unit</td>
                      <td>Rate diff</td>
                      <td>Quantity</td>
                      <td>amount</td>
                      <td>Gst</td> 
                      <td>Gst Total</td> 
                      <td>Final Total</td> 
                     
             </tr>  
        </thead> 
        <?php
          include ("connect.php");
         $valuetosearch=$_POST['purchase_order'];
              $query = "SELECT insert_po.*,date_purchase_key.purchase_order,date_purchase_key.key1 FROM insert_po LEFT JOIN date_purchase_key on insert_po.key1=date_purchase_key.key1 where  date_purchase_key.purchase_order='$valuetosearch'";
              $result = mysqli_query($conn, $query);
              $l_u = 1;
              $id_customer = 0;
            ?>   
        <?php  
        while($row = mysqli_fetch_array($result))
        { 

          $purchase_order = $row["purchase_order"];
          $productno = $row['productno'];
          $productname = $row['productname'];
          $rate_master = $row['rate_master'];
          $rate = $row['rate'];
          $diff = $row['diff'];
          $quantity = $row['quantity'];
          $amount = $row['amount'];
          $gst = $row['gst'];
          $gst_amount = $row['gst_amount'];
          $total1 = $row['total'];
          $total = round($total1,2);
          $party_name = $row['party_name'];

        ?>
        <tr>
          <td><?php echo $purchase_order?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $purchase_order; ?>">
          </td>
         <td><?php echo $party_name?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>">
          </td>
          <td ><?php echo $productno?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $productname?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $rate_master?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $rate?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $diff?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
          
           <td ><?php echo $quantity?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $amount?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $gst?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $gst_amount?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
           <td ><?php echo $total?>
            <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
          </td>
        </tr>
        <?php  
          $id_customer++;
          $l_u++;
        } 
        ?>  

      </table>  
      <div class="col-md-12">
      <div class="row">
       <div class="col-md-4"style=" margin-top: 14px ;">
        <div><b><u>OTHER TERMS & CONDITIONS ARE AS UNDER :</u></b></div>
         <span class="company-name">PARTS SHOULD BE GENUINE AND ACCORDING TO PO.</span>
                <span class="spacer"></span>
                <div>IF MATERIAL FOUND DAMAGE MUST BE REPLACED.</div>
                <div>PARTS SHOULD BE IN PROPPER PACKING.</div>
                <span class="clearfix"></span>
                <div>PAYMENT TERMS : 60 Days</div>

      </div>
      <div class="col-md-6"style=" margin-top: 100px; margin-left:200px;">
        <div>(AUTHORISED SIGNATURE )</div>
      </div>
    </div>
  </div>
    </div>
  </div>
<!--   </table> -->
     <input type="button" class="btn btn-warning" id="btnExport" value="Download PDF" />
         <a href="generate_po.php">
      <input type="button" id="link" class="btn btn-danger" href="generate_po.php" value="Back" /></a>
      <!-- <input id="printpagebutton" type="button" value="print news" onclick="printpage()"/> -->
    
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
     <script type="text/javascript">
$("body").on("click", "#btnExport", function () {
  html2canvas($('#tblCustomers')[0], {
      onrendered: function (canvas) {
          var data = canvas.toDataURL();
          var docDefinition = {
              content: [{
                  image: data,
                  width: 500
              }]
          };
          pdfMake.createPdf(docDefinition).download("Invoic.pdf");
      }
  });
});
</script>
<script>
function Func1(){
downloadPDF();
}
</script>
<script type="text/javascript">
      function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        printButton.style.visibility = 'visible';
    }
</script>