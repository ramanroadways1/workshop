<?php 
  session_start(); 
  if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>MNGT</span>
      <span class="logo-lg"><b>Vendor</b>Management</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
       <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
        <h3>
          <?php 
            echo $_SESSION['success']; 
            unset($_SESSION['success']);
          ?>
        </h3>
      </div>
    <?php endif ?>

    <!--logged in user information-->
    <?php  if (isset($_SESSION['manager'])) : ?>
      <p style="float:right; margin-right:20px;">Welcome <strong><?php echo $_SESSION['manager']; ?></strong>
      <a href="login_payment.php?logout='1'" style="color: red;">logout</a> </p><br>
    <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
          <li>
          <a href="manager_approval_for_payment.php"><i class="fa fa-check"></i> <span>Manager Approval</span></a>
        </li>
          <li>
          <a href="Show_manager_pay_detail.php"><i class="fa fa-check"></i> <span>Show Payment Detail</span></a>
        </li>
         <li>
          <a href="show_data_after_ho_Approval.php"><i class="fa fa-check"></i> <span>Show After H.o Approval</span></a>
        </li>
        <!--   <li>
          <a href="ho_approval.php"><i class="fa fa-check"></i> <span>Final Appoval From H.O</span></a>
        </li> -->
      </ul>
    </section>
  </aside>