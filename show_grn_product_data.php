
<?php 
  session_start(); 
  if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<style type="text/css">
          @media print {
             #Export,#button{
            display: none;
          }
         header,footer {
            display: none !important;
          }
           body{
                 page-break-before: avoid;
                width:100%;
                height:100%;
                zoom: 100%;
               }
               #party_name,#date{
                width: 400px;
               }
         
        }
      </style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
            <div class="box-header with-border">
              <h3 class="box-title">Detail about Received Data</h3>
              <!--   <input type="button" id="button" style="float: right; " class="btn btn-info btn-sm add-new" name="" value="Print" onclick="myprint()"><br>   -->
             </div>
                    <script type="text/javascript">
                        function myprint() {
                                window.print();
                              }
                    </script>
         <div class="row-md-2"> 
          <table class="table table-hover" border="4";>
                 <tbody>
                    <tr class="table-active">
                      <th style="display: none;" scope="row">ID</th>
                      <td id="party_name" style="width: 400px;">Party Name</td>
                      <td>Product No</td>
                      <td>Quantity</td>
                      <td>Rate/Unit</td>
                      <td>Amount</td>
                      <td>G.S.T(%)</td>
                      <td>Gst Amt</td>
                      <td>Total Amt</td>
                      <td>Received Qty</td>
                      <td>GRN Total</td>
                      <td>Grn No.</td>
                      <td id="date" style="width: 190px;">Back Date</td>
                    </tr>
                </tbody>

            <?php
              include 'connect.php';
              $valueToSearch= $_REQUEST['link'];
              $show = "SELECT id, grn_no,productno,party_name,productname,quantity,rate,amount,gst,gst_amount,total_amount,received_qty,new_total,back_date FROM grn where productno='$valueToSearch'";
              $result = $conn->query($show);
              $sum=mysqli_query($conn,"SELECT SUM(new_total) as new_total,SUM(quantity) as quantity,SUM(received_qty) as received_qty  FROM grn where productno='$valueToSearch'");
              $row2=mysqli_fetch_array($sum);

              if ($result->num_rows > 0) {
                   $id_customer = 0;
                   while($row = $result->fetch_assoc()) {
                      $id = $row['id'];
                      $grn_no = $row['grn_no'];
                      $productno = $row['productno'];
                      $party_name = $row['party_name'];
                      $productname = $row['productname'];
                      $quantity = $row['quantity'];
                      $rate = $row['rate'];
                      $amount = $row['amount'];
                      $gst = $row['gst'];
                      $gst_amount = $row['gst_amount'];
                      $total_amount = $row['total_amount'];
                      $received_qty = $row['received_qty'];
                      $new_total = $row['new_total'];
                      $back_date = $row['back_date'];
                  ?>
                <div class="row">
                  <div class="col-md-10">
                    <tr> 
                      <td style="display: none;"><?php echo $id; ?></td>
                      <td><?php echo $party_name; ?></td>
                      <td><?php echo $productno; ?></td>
                      <td><?php echo $quantity; ?></td>
                      <td><?php echo $rate; ?></td>
                      <td><?php echo $amount; ?></td>
                      <td><?php echo $gst; ?></td>
                      <td><?php echo $gst_amount; ?></td>
                      <td><?php echo $total_amount; ?></td>
                      <td><?php echo $received_qty; ?></td>
                      <td><?php echo $new_total; ?></td>
                      <td><?php echo $grn_no; ?></td>
                      <td><?php echo $back_date; ?></td>
                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <td colspan='2'>Total Calculation : </td>
                    <td colspan='6'>$row2[quantity]</td>
                    <td colspan='1'>$row2[received_qty]</td>
                    <td>$row2[new_total]</td>
                     <td colspan='2'></td>
                  

                  </tr>";
                  } else {
                   
                  }
                  ?>
              </table>
       <form method="post" action="export_grn_product.php">
         <input type="submit" id="Export" name="export" class="btn btn-success" value="Export" />
          <input type="hidden" name="valueToSearch"  value="<?php echo $valueToSearch ?>" />
       </form>
    </div>

</body>
</html>
