
 <?php  
 include("connect.php");
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>GRN Detail</h1>
    </section> -->

    <section class="content">

     
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Goods Received Notes Detail</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
         <button onclick="window.location.href = 'grn_detail_by_date.php';" value="Show grn detail"class="btn btn-info add-new"style="float: right; margin-right: 10px;">Show Grn Detail</button><br> <br> 
    <!--   <input type="button"  style="float: right; margin-right: 10px;" class="btn btn-info add-new" name="" value="Show grn detail" > -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <?php 
                     $sql = "SELECT * FROM grn";
                     $result = $conn->query($sql);
                     ?>
                  <thead>  
                       <tr>  
                            <td>GRN No.</td>
                            <td>Purchase Order</td>
                            <td>Party Name</td>
                            <td>Party Code</td>
                            <td>Product Number</td>
                            <td>Product Name</td>
                            <td>Purhcase Quantity</td>
                            <td>Rate/Unit</td>
                            <td>Amount</td>
                            <td>GST(%)</td>
                            <td>GST Amount</td>
                            <td>Total Amount</td>
                            <td>Received Quantity</td>
                            <td>Amount over Purchase</td>
                              <td>Date</td>
                            
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr> 
                            <td>'.$row["grn_no"].'</td>
                            <td>'.$row["purchase_order"].'</td>
                            <td>'.$row["party_name"].'</td>
                            <td>'.$row["party_code"].'</td>
                            <td>'.$row["productno"].'</td>  
                            <td>'.$row["productname"].'</td>  
                            <td>'.$row["quantity"].'</td>
                            <td>'.$row["rate"].'</td>
                            <td>'.$row["amount"].'</td>  
                            <td>'.$row["gst"].'</td>
                            <td>'.$row["gst_amount"].'</td>  
                            <td>'.$row["total_amount"].'</td>
                            <td>'.$row["received_qty"].'</td>
                            <td>'.$row["new_total"].'</td> 
                            <td>'.$row["date1"].'</td>   
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  <input type="button" style="float: right; margin-right: 50px;" class="btn btn-info add-new" name="" value="Print" onclick="myprint()"><br>  <script type="text/javascript">
              function myprint() {
                      window.print();
                    }
            </script>
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

