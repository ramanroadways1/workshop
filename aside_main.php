<?php 
  session_start(); 

  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }
?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>M</span>
      <span class="logo-lg"><b>Vendor</b>Management</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
       <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success">

        <h3>
          <?php 
            echo $_SESSION['success']; 
            unset($_SESSION['success']);
          ?>
        </h3>
      </div>
    <?php endif ?>
    
    <!-- logged in user information and Show Informatision index file -->
    <?php  if (isset($_SESSION['username'])) : ?>
      <p style="float:right; margin-right:20px;"><strong><?php 
       
         echo $_SESSION['username'];

         echo "/";


         echo $_SESSION['employee'];

      ?></strong>
      <a href="logout.php?logout='1'" style="color: white;">LOG OUT</a> </p><br>
    <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">

        <li>
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Operation on Stock</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="stock_transfer_record.php"><i class="fa fa-circle-o"></i>Stock Transfer</a></li>
            <li><a href="available_scrap_stock.php"><i class="fa fa-circle-o"></i>Scrap Stock</a></li>
            <li><a href="available_theft_stock.php"><i class="fa fa-circle-o"></i>Theft Stock</a></li>
          </ul>
        </li>


        <?php if($_SESSION['username']=='visalpur'){ ?>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="party.php"><i class="fa fa-circle-o"></i> Party Master</a></li>
            <li><a href="product.php"><i class="fa fa-circle-o"></i> Product Master</a></li>
            <li><a href="rate.php"><i class="fa fa-circle-o"></i> Rate Master</a></li>
          </ul>
        </li>

      <?php }  else{
        


            }
       ?>

 
         <li class="treeview">
          <a href="#">
            <i class="fa fa-truck"></i>
            <span>Workshop</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="workshop/check_login.php"><i class="fa fa-circle-o"></i>Workshop</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Send Service Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="service/add_serviceproduct.php"><i class="fa fa-tasks"></i>Service</a></li>

          </ul>
        </li>


       <?php if($_SESSION['username']=='visalpur'){ ?>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-empire"></i>
            <span>Tyre</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="tyre/chack.php"><i class="fa fa-circle-o"></i>Tyre</a></li>

          </ul>
        </li>
      <?php }  else{
       
              }
      ?>
       <li>
          <a href="generate_po.php">
            <i class="fa fa-file-text-o"></i> <span>Generate Purchase Order</span>
          </a>
        </li>

        <li>
        <!--   <a href="approve_po.php"> -->
            <a href="approve_po.php">
            <!--   <a href="approve_view.php"> -->

            <i class="fa fa-check-square-o"></i> <span>Approve Purchase Order</span>
          </a>
        </li>

        <li>
           <!-- <a href="grn.php">  -->
            <a href="grn_view.php">
            <i class="fa   fa-newspaper-o"></i> <span>Goods Received Notes</span>
          </a>
        </li>

        <li>
          <!-- <a href="challan_payment.php"> -->
            <a href="challan_payment_view.php">
            <i class="fa  fa-file-pdf-o"></i> <span>Upload Invoice Over Challan</span>
          </a>
        </li>

         <li>
          <!-- <a href="approve_invoice.php"> -->
            <a href="approve_view.php">
            <i class="fa fa-check-square-o"></i> <span>Approve GRN</span>
          </a>
        </li>

           <li>
          <!-- <a href="grn_for_payment.php"> -->
            <a href="grn_for_payment_view.php">
            <i class="fa fa-paypal  "></i> <span>Payment</span>
          </a>
        </li>

         <li>
          <a href="payment_index.php">
            <i class="fa fa-check-square-o"></i> <span>Payment Approve</span>
          </a>
        </li>

        <li>
          <a href="product_service.php" target="_blank">
              <i class="fa fa-sticky-note"></i> <span>Product Sevice Report</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
              <i class="fa  fa-window-restore"></i>
               <span>Other  Reports</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
           <ul class="treeview-menu">
               <!--<li><a href="report_dashboard.php"><i class="fa fa-circle-o"></i> Report Dashboard</a></li> -->
               <!--part_report.php -->
                <li><a href="report_party.php"><i class="fa fa-circle-o"></i> Party Ledger</a></li>
                <li><a href="product_list.php"><i class="fa fa-circle-o"></i> Product Ledger</a></li>
                <li><a href="report_truck.php"><i class="fa fa-circle-o"></i>Truck By All (Report) </a></li>
                 <li><a href="job_card_detils_mistry.php"><i class="fa fa-circle-o"></i> Job Card By Mistry (Date)</a></li>
                  <li><a href="jobcard_mistry.php"><i class="fa fa-circle-o"></i> Job Card By Mistry</a></li>
                  <li><a href="truck_workshop.php"><i class="fa fa-circle-o"></i> Job Card By Truck</a></li>
                  <li><a href="model_truck_log.php"><i class="fa fa-circle-o"></i> Model wise Report</a></li>
                <li><a href="vendor_stock_view_report.php"><i class="fa fa-circle-o"></i> Stock Report </a></li>
               <!--<li><a href="new_stock_workshop_view.php"><i class="fa fa-circle-o"></i> Stock Report For workshop</a></li> -->
                <li><a href="truck_by_view.php"><i class="fa fa-circle-o"></i> Get In Truck </a></li>
                
                <li><a href="chanlan_bill.php"><i class="fa fa-circle-o"></i> Challan bill </a></li>
             <!--<li><a href="office_job.php"><i class="fa fa-circle-o"></i>Extera New Job Crad For Office</a></li> -->
                <li><a href="truck_battry.php"><i class="fa fa-circle-o"></i> Battery Issue By Truck </a></li>
                <li><a href="truck_oil.php"><i class="fa fa-circle-o"></i> Oil issue By Truck </a></li>
               <!--model_truck.php -->
          
                <li><a href="pending_job.php"><i class="fa fa-circle-o"></i> Pending Job Card </a></li>
                <li><a href="pending_job_date.php"><i class="fa fa-circle-o"></i> Pending Job Card (Date) </a></li>
          
                
                <li><a href="auto_po.php"><i class="fa fa-circle-o"></i>PO Reports</a></li>
              <!--   dashboard_report -->
            <li><a href="report_live.php"><i class="fa fa-circle-o"></i>GET DASHBOARD REPORT</a></li>
            <li><a href="stock_viewall.php"><i class="fa fa-circle-o"></i>Inventory Reports</a></li>
           </ul>

      <!--  <?php if($_SESSION['username']=='visalpur'){ ?> -->
<!--       <?php }  else{
        ?>

          }
        <?php } ?> -->
          <!--<li><a href="report_truck.php"><i class="fa fa-circle-o"></i>All Truck (Status) </a></li> -->

           </ul>
        </li>
      </ul>
    </section>
  </aside>
    <style>
      .table {margin:0px;}
      .dataTables_wrapper {
        padding:20px;
      }
    </style>