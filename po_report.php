<!-- SELECT insert_po.id as id,purchase_order,party_name,party_code,productname,date1,rate ,quantity, amount, total ,gst,insert_po.key1, gst_amount, rate_master,productno, date_purchase_key.date1 FROM date_purchase_key,insert_po WHERE date_purchase_key.key1 = insert_po.key1 and purchase_order = '04032020018' -->

<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="grn_report.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Payment</h1>
    </section> -->

     <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Product Detils By PO Numbur</h3>

          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->

           <a href="product_list.php">
    <button type="button">
        Back
    </button>
</a>
        </div>
        <!-- /.box-header -->
       <!--  <div class="box-body"> -->
          <div style="overflow-x:auto;">
              <table class="table table-hover" border="4";>
                <!-- <center><h1 class="h3 mb-0 text-gray-800">Approve Invoice for Payment</h1></center> -->
               <!--  <a href="pay_detail.php">Detail Pay</a><br><br> -->
                <tbody>
                  <tr class="table-active">
                        <td>Product Number</td>
                          <td>Product Name</td>
                          <td>Rate(from Rate Master)</td>
                          <td>Rate/Unit</td>
                          <td>Difference In Rates</td>
                          <td>Quantity</td>
                          <td>Amount</td>
                          <td>Gst</td>
                          <td>Gst Amount</td>
                          <td>Total Amount</td>
                          <td>Created PO Date</td>
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $demo=$_POST['purchase_order'];

              // echo $demo=$_POST['id'];
            
              $show1 = "SELECT insert_po.id as id,purchase_order,party_name,party_code,productname,date1,rate ,quantity, amount, total ,gst,insert_po.key1, gst_amount, rate_master,productno, date_purchase_key.date1 FROM date_purchase_key,insert_po WHERE date_purchase_key.key1 = insert_po.key1 and purchase_order = '$demo'";
              
              $result = $conn->query($show1);


              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) {
                      // $id = $row['id'];
                  
                    $id = $row['id'];
                    $key1 = $row['key1'];
                    $purchase_order=$row['purchase_order'];
                    $productno = $row['productno'];
                    $party_name = $row['party_name'];
                    $rate_master = $row['rate_master'];
                    $productname = $row['productname'];
                    $party_code = $row['party_code'];
                    //$product_qun = $row['product_qun'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $total1 = $row['total'];
                     $total = round($total1,2);
                    $gst_amount = $row['gst_amount'];
                    $gst = $row['gst'];
                    $amount1 = $row['amount'];
                      $amount = round($amount1,2);

                    $diff =  abs($rate_master - $rate);
                    $date=$row['date1']; 
                   
                     
                  ?>
                <div class="row">
                  <div class="col-md-5">
                    <tr> 
                      <td style="display: none;"><?php echo $id; ?></td>



                       <td><?php echo $productno; ?></td>
                       <td><?php echo $productname; ?></td>
                       <td><?php echo $rate_master; ?></td>
                       <td><?php echo $rate; ?></td>
                       
                       <td><?php echo $diff; ?></td>
                       <td><?php echo $quantity; ?></td>
                       <td><?php echo $amount; ?></td>
                       
                       <td><?php echo $gst; ?></td>
                       <td><?php echo $gst_amount; ?></td>
                       <td><?php echo $total; ?></td>
                       <td><?php echo $date; ?></td>
                  

                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <!-- <td colspan='4'>Total Calculation : </td> -->
                    <!-- <td colspan='6'>[quantity]</td> -->

                  </tr>";
                  } else {
                       echo "PO NOT Avilbaly";
                    exit();
                  }
                  ?>

              </table>

               
            </div>

          </div>
        </section>
      </div>
    </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


    