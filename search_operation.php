<html>
<head>
   <style>
    table {
      max-width: 1200px;
    }

    button {
       background: white;
       color: white;
    }

    input[type=checkbox]:checked ~ button {
       background: green;
    }
  </style>

</head>
<body>
<div class="container box">
   <h1 align="center">Live Add Edit Delete Datatables Records using PHP Ajax</h1>
   <br />
   <div class="table-responsive">
   <br />
    <br />
    <div id="alert_message"></div>
    <table id="user_data" class="table table-bordered table-striped">
     <thead>
      <tr>
       <th>id</th>
       <th>product no</th>
       <th></th>
      </tr>
     </thead>
    </table>
   </div>
  </div>
</body>
</html>
