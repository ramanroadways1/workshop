<?php 
  include "connect.php";
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

 <script src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"></script>

      <style type="text/css">
   @media print 
    {
       #from_date,#to_date,#filter,#print,#heading,#button{
      display: none;
    }
       @page :first {
         #employee_data{
        margin-top: -10cm;
      }
    }

    #pname{
      width: 450px;
    }

    #total{
      width: 500px;
    }
    .box{
     border-top-width: 0px;
    }

   header,footer,h3 { 
      display: none !important;
    }

    body{
       page-break-before: avoid;
      width:100%;
      height:100%;
      zoom: 80%;
      size: A4;
      margin:0px; 
    }
   
   h4 {
    margin-top: 0cm;
    float: left;
  }
  h5 {
    margin-top: -1cm;
    float: right;
  }
    #employee_data{
      margin-right: -10px;
      margin-left: -10px;
     }
    thead
    { display: table-header-group;  }
    tfoot
    { display: none;   }
  }
</style>

     <style type="text/css">
     tfoot input{
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
      <div class="container">    
     <!--  <input type="button" id="button" style="float: right; margin-right: 80px;" class="btn btn-info btn-sm add-new" name="" value="Print" onclick="myprint()"><br> -->
        <table id="example"  style="width:100%">
                   <script type="text/javascript">
                        function myprint() {
                                window.print();
                              }
                    </script>
                        <thead>
                             <tr>
                               <th  style="width: 120px;">Grn No</th>
                               <th style="width: 300px;">Party Name</th>
                               <th>No. of Product</th>
                               <th>Direct issue</th>
                               <th>New Total</th>
                               <th>Back Date</th>
                             </tr>
                        </thead>
                        <!-- <center><h4>GRN Detail Date</h4>
              <?php 
                 $username = $_POST['username'];
                 $from_date = $_POST["from_date"];
                 $to_date  =$_POST["to_date"];
                 echo $from_date; echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; echo $to_date; ?>
            </center> -->
                
          <?php  
            $query = "  
              SELECT id, grn_no,timestamp1,back_date, purchase_order, party_name, key1,count(productno) as productno,truck_no, sum(new_total) as new_total, challan_file, GROUP_CONCAT(remain) as remain, invoice_file,invoice_no FROM files_upload   WHERE  username='visalpur' and back_date   BETWEEN '2020-01-01' AND '2020-01-06'   group by grn_no";  
                  $result = mysqli_query($conn, $query);  
                   while($row = mysqli_fetch_array($result))  
                   {  
                    $grn_no = $row['grn_no'] ;
                     $productno = $row['productno'] ;
                    $purchase_order =  $row["purchase_order"];
                    $party_name =  $row["party_name"];
                     $truck_no =  $row["truck_no"];
                    $new_total =  $row["new_total"];
                     $back_date =  $row["back_date"];
                 ?>
               
                 <tr>
                      <td>
                       <form method="POST" action="show_filter_grn_record.php" target="_blank">
                              <input type="submit"  name="grn_no" value="<?php echo $grn_no; ?>" class="btn btn-xs fa fa-eye" >
                        </form>
                          </td>
                            <td><?php echo $party_name; ?></td> 
                            <td><?php echo $productno; ?></td> 
                            <td><?php echo $truck_no; ?></td> 
                            <td><?php echo $new_total; ?></td> 
                            <td><?php echo $back_date; ?></td> 
                      <?php }  ?>
                  </tr>
                  <tfoot id="search_box">
                          <tr>
                             <th>Grn No</th>
                             <th>Party Name</th>
                             <th>Product</th>
                             <th>Truck no</th>
                             <th>New Total</th>
                             <th>Back date</th>
                          </tr>
                  </tfoot>
        </table> 

         <form method="post" action="export.php">
              <input type="submit" name="export" class="btn btn-success" value="Export" />
              <input type="hidden" name="username"  value="<?php echo $username ?>" />
              <input type="hidden" name="from_date"  value="<?php echo $from_date ?>" />
              <input type="hidden" name="to_date"  value="<?php echo $to_date ?>" />
         </form>

     </div> 
  </body>
  </html>

 
<script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                       
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            }
        ]
    } );
} );
    </script>