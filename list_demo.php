
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

 <style type="text/css">
  @media print {

     #from_date,#to_date,#filter,#print,#heading{
    display: none;
  }
 header,footer,h3 {
    
    display: none !important;
  }
  body{
      margin-top: -2cm;
    margin-bottom: 0cm;
  }
 
/* #employee_data{
   page-break-after: always; 
 }*/

  
}
</style>
 

        
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
  <div class="box ">
      <div class="box-header with-border">
        <h3 class="box-title" id="heading" style="font-family: verdana; font-size:16px; ">G R N-P O Detail</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
             <?php $username =  $_SESSION['username'];  
              ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">

            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <div id="order_table">
                <table id="employee_data" class="table table-striped table-bordered" style="font-family: verdana; font-size:13px; ">  
                  <thead>  
                       <tr>  
                        <th>View</th>
                        <th>Product Name</th>
<!--                         <th>Back Date</th>
                        <th>P.o Date</th> -->
                       </tr>  
                  </thead>  
                   <?php
              include 'connect.php';
              // $demo="SELECT id,productno,company,quantity,productname FROM product_inventory";

              $show = "SELECT * FROM product group by productname";
              $result = $conn->query($show);

              if ($result->num_rows > 0) {
                 
                  while($row = $result->fetch_assoc()) {
                       $id=$row['id'];
                      $productno = $row['productno'];
                      
                      // $grn_no = $row['grn_no'];
                      $productname=$row['productname'];
                      // $party_name=$row['party_name'];
                      // $back_date = $row['back_date'];
                      // $date1 = $row['date1'];
                     ?>
                       <tr> 
                          <td>
                             <form method="POST" action="listdemo.php">
                           <!--    <input type=hidden name=grn_no1 value='".$row["grn_no"]."'> -->
                           
                              <input type="hidden"  name="productno" value="<?php echo $productno; ?>" >
                             <!--  <input type="hidden"  name="purchase_order" value="<?php echo $purchase_order; ?>" > -->
                              <button name="submit" value="bar" type="submit"><i class="fa fa-pencil"></i></button>

                            </form>
                          </td>

                            <td><?php echo $productname; ?></td>

                        </tr>  
                  <?php 
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
         </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  <footer id="footer" class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"filter_grn_interval.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  

                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>


 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

