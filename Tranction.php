<!DOCTYPE html>
<html>
<head>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <?php 
    include("header.php");
    include("aside_main.php");
  
  ?>
  <script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("txtMobId");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = ^(\+[\d]{1,5}|0)?[7-9]\d{9}$;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#txtMobId').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                }
            }
      } 
</script>

   <script type="text/javascript">
     function validateEmail(sEmail) {
     var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

        if(!sEmail.match(reEmail)) {
          // alert("Invalid email address");
          Swal.fire({
              title: "Good job!",
              text: "You clicked the button!",
              icon: "success",
              button: "Aww yiss!",
            });
      
          document.getElementById('email').value = "";
        }

        return true;

      }
        
   </script>


</head>
<form method="post" action="inseert_tra.php" id="formid" role="form" autocomplete="off">
<body class="hold-transition skin-blue sidebar-mini">
   

   <div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#ffffff; z-index: 30001; opacity:0.8; cursor: wait;">
  <center><img src="assets/pintu.gif" style="margin-top:50px;" /> </center>
  </div>
<div class="wrapper">
 <form method="post" action="inseert_tra.php" autocomplete="off">
  <div class="content-wrapper">
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Test  Party</h3>

        </div>
     
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Name<font color="red">*</font></label>
                <input type="text" style="width: 100%;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" onblur="MyFunc()" id="name" name="name" placeholder=" Name" required/>
              </div>
              <div id="result2"></div>
            <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">


              <div class="form-group">
                 <label for="product_no">Mobile number<font color="red">*</font></label>

                 <input type="text" style="width: 100%;" MaxLength="10" class="form-control" name="mobile_no" id="txtMobId" onchange="IsMobileNumber(this);" placeholder="Mobile Number" required/>
              </div>
              
              <div class="form-group">
                 <label for="PAN">PAN Number<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" id="textPanNo" MaxLength="10" class="form-control" name="pan" placeholder="PAN Number"onchange="ValidatePAN(this);" required/>
              </div>
            </div>
      
            <div class="col-md-6">
               <div class="form-group">
                 <label for="product_no">Email<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" onblur="validateEmail(this.value);" name="email" id="email" placeholder="Email" required/>
              </div>

             
          <br>

            <div class="form-group">
              <label for="bank_name">Bank Name<font color="red">*</font></label>
                <input type="text" style="width: 100%;" id="bank" class="form-control" name="bank" placeholder="Bank Name" required/>
              </div>

                  <div class="form-group">
                    <label for="IFSC_code">IFSC Code<font color="red">*</font></label>
                      <input type="text" style="width: 100%;" id="textIFSC" class="form-control" name="ifsc_code" placeholder="IFSC Code" onchange="ValidateIFSC(this);" required/>
                  </div>


    

            </div>
  
          </div>

        </div>
      </div>
    </section>

<div id="response"></div>
<script type="text/javascript">
  

   $(document).ready(function()
              { 
                $(document).on('submit', '#formid', function()
                {  
                  $("#formbutton").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'inseert_tra.php',
                    data : data,
                    success: function(data) {  
                      $('#formid')[0].reset();
                      $("#formbutton").attr("disabled", false);
                      // $('#exampleModal').modal("hide");  // agar koi modal show hide karna h
                      $('#response').html(data);  // response div par data aaega
                      // $('#user_data').DataTable().ajax.reload(null,false); // datatable refresh hoga 
                      $('#loadicon').hide();   // loading division hide kar dega

                    }
                  });
                  return false;
                });
              }); 
</script>

    <div class="col-sm-offset-2 col-sm-8">
     <center>
      <input type="submit" id="formbutton" class="btn btn-primary" name="submit" value="SAVE" />
     </center>
    </div>
 
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</form>
</html>


