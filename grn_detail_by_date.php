
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

 <style type="text/css">
   @media print 
    {
       #from_date,#to_date,#filter,#print,#heading,#p_o_qty{
      display: none;
    }
       @page :first {
         
        margin-top: -1cm;
      
    }
    .box{
     border-top-width: 0px;
    }

    #pname{
      width: 450px;
    }

    #total{
      width: 500px;
    }

   header,footer,h3 { 
      display: none !important;
    }

    body{
       page-break-before: avoid;
      width:100%;
      height:100%;
      zoom: 80%;
      size: A4;
      margin:0px; 
    }
   
   h4 {
/*    margin-top: -2cm;*/
    float: left;
  }
  h5 {
    /*margin-top: -2cm;*/
    float: right;
  }
   
    thead
          {
              display: table-header-group;
          }
          tfoot
          {
              display: table-footer-group;
          }
  }
</style>
 

        
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
  <div class="box ">
      <div class="box-header">
        <h3 class="box-title" id="heading" style="font-family: verdana; font-size:14px; ">G R N Detail by date</h3>
       <!--  <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
             <?php $username =  $_SESSION['username'];  ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">

            <div class="col-md-0"></div> 
            <div class="col-md-12">
             <!--  <div class="table-responsive">  -->

               <div class="col-md-3">  
                     <input type="text" name="from_date" autocomplete="off" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date"  autocomplete="off" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                     
                </div>  
                <div style="clear:both"></div>                 
                <br />  
                <div id="order_table">


                <!-- <table id="employee_data" class="table table-striped table-bordered" style="font-family: verdana; font-size:13px; ">  
                  <thead>  
                       <tr>  
                       <th style="display: none;" scope="row">ID</th>
                        <th>GRN No</th>
                        <th>Purchase Order</th>
                        <th>Party Name</th>
                        <th>Product Number</th>
                         <th>Truck no</th>
                        <th style="display: none;">Total Amount</th>
                        <th style="display: none;">Remaining Amount</th>
                        <th>GRN Date(Back Date)</th>
                          <th style="display: none;">GRN date</th>
                       </tr>  
                  </thead>  
                   <?php
              include 'connect.php';
              $show = "SELECT id, grn_no,timestamp1,back_date, purchase_order, party_name, key1,count(productno) as productno,truck_no, GROUP_CONCAT(new_total) as new_total, challan_file, GROUP_CONCAT(remain) as remain, invoice_file,invoice_no FROM files_upload where username='$username' group by grn_no order by id DESC";
              $result = $conn->query($show);



              if ($result->num_rows > 0) {
                 
                  while($row = $result->fetch_assoc()) {
                      $id = $row['id'];
                       $truck_no = $row['truck_no'];
                      $grn_no = $row['grn_no'];
                      $purchase_order = $row['purchase_order'];
                      $party_name = $row['party_name'];
                      $productno = $row['productno'];
                      $timestamp1 = $row['timestamp1'];
                      $back_date = $row['back_date'];
                      /*$challan_file = $row['challan_file'];
                      $invoice_file = $row['invoice_file'];
                      $invoice_no = $row['invoice_no'];
                      */
                      $new_total = $row['new_total'];
                      $remain = $row['remain'];
                      /*$challan_file1=explode(",",$challan_file);
                      $count3=count($challan_file1);*/
                  ?>
                       <tr> 
                           <td style="display: none;"><?php echo $id?>
                            
                          </td>
                          <td>
                             <form method="POST" action="from_date_to_date1.php">
                              <input type="submit"  name="grn_no" value="<?php echo $grn_no; ?>" class="btn btn-primary btn-sm" >
                            </form>
                          </td>
                           <td><?php echo $purchase_order; ?></td>
                      <td><?php echo $party_name; ?></td>
                      <td><?php echo $productno; ?></td>
                        <td><?php echo $truck_no; ?></td>
                      <td style="display: none;"><?php echo $new_total; ?></td>
                      <td style="display: none;"><?php echo $remain; ?></td>
                      <td><?php echo $back_date; ?></td>
                       <td style="display: none;"><?php echo $timestamp1; ?></td>
                       </tr>  
                  <?php 
                    }
                  } else {
                      echo "0 results";
                  }
                  ?>  
                </table>   -->
              </div>  
         <!--    </div>   -->
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  <footer id="footer" class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"filter.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  

                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>


 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

