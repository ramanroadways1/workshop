<?php  
//export.php  
include 'connect.php';
$output = '';
$valueToSearch = $_POST['valueToSearch'];

if(isset($_POST["export"]))
{
 $query = "SELECT id, grn_no,productno,party_name,productname,quantity,rate,amount,gst,gst_amount,total_amount,received_qty,new_total,back_date FROM grn where productno='$valueToSearch'";
 $result = mysqli_query($conn, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table class="table" bordered="1">  
                      <tr> 
                          <th>Grn No</th>
                          <th>Party Name</th>
                          <th>Product No</th>
                          <th>Product Name</th>
                          <th>Quantity</th>
                          <th>Rate</th>
                          <th>Amount</th>
                          <th>Gst(%)</th>
                          <th>Gst Amount</th>
                           <th>Total Amount</th>
                          <th>Receive Quantity</th>
                          <th>GRN Amount</th>
                          <th>Grn Date</th>
                     </tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
      $output .= '
            <tr>  
               <td>'.$row["grn_no"].'</td>  
               <td>'.$row["party_name"].'</td>  
               <td>'.$row["productno"].'</td>  
               <td>'.$row["productname"].'</td>  
               <td>'.$row["quantity"].'</td>  
               <td>'.$row["rate"].'</td> 
               <td>'.$row["amount"].'</td>  
               <td>'.$row["gst"].'</td>  
               <td>'.$row["gst_amount"].'</td>  
               <td>'.$row["total_amount"].'</td> 
               <td>'.$row["received_qty"].'</td>  
               <td>'.$row["new_total"].'</td>  
               <td>'.$row["back_date"].'</td>  
            </tr>
       ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=product_detail.xls');
  echo $output;
 }
}
?>