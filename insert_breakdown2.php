<!DOCTYPE html>
<html>
<head>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
session_start();
$empcode = $_SESSION['empcode'];
$username= $_SESSION['username'];
$employee= $_SESSION['employee'];
include ("connect.php");
$truck_no = $conn->real_escape_string(htmlspecialchars($_POST['truck_no']));
$km = $conn->real_escape_string(htmlspecialchars($_POST['km']));
$mobile_no = $conn->real_escape_string(htmlspecialchars($_POST['mobile_no']));
$comp_date = $conn->real_escape_string(htmlspecialchars($_POST['comp_date']));
$comp_time = $conn->real_escape_string(htmlspecialchars($_POST['comp_time']));
// $problem = $conn->real_escape_string(htmlspecialchars($_POST['problem']));
// $workgroup = $conn->real_escape_string(htmlspecialchars($_POST['workgroup']));
$status = $conn->real_escape_string(htmlspecialchars($_POST['status']));
$report_by = $conn->real_escape_string(htmlspecialchars($_POST['report_by']));
// $update_info = $conn->real_escape_string(htmlspecialchars($_POST['update_info']));
$place_brakdown = $conn->real_escape_string(htmlspecialchars($_POST['place_brakdown']));
$approve_by = $conn->real_escape_string(htmlspecialchars($_POST['approve_by']));
$remark = $conn->real_escape_string(htmlspecialchars($_POST['remark']));
$time_systemc= $conn->real_escape_string(htmlspecialchars($_POST['time_systemc']));
$date_systemc= $conn->real_escape_string(htmlspecialchars($_POST['date_systemc']));

try{

$conn->query("START TRANSACTION"); 

    $datevar= date('y-m-d');
    $remould_no2 =  "BREAK_DOWN-".$datevar;
    $query= "SELECT uniq_id FROM  breakdown where uniq_id like '$remould_no2%' order by id desc limit 1";
    $result = mysqli_query($conn, $query);
    if(mysqli_num_rows($result) > 0)  
      {
          $row = mysqli_fetch_array($result);
          $remould_no = $row["uniq_id"];
          $last_digits = substr($remould_no, -4);
          $next = $last_digits + 1;
          $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);


          $temp_outward = $remould_no2."-".$next1;

      }
        else
        {
            $temp_outward =  $remould_no2."-0001";

        }
        
$sql = "INSERT INTO `breakdown`(truck_no,km,mobile_no,comp_date,comp_time,status,report_by,place_brakdown,approve_by,remark,employee,uniq_id,date_systemc,time_systemc) VALUES('$truck_no','$km','$mobile_no','$comp_date','$comp_time','$status','$report_by','$place_brakdown','$approve_by','$remark','$employee','$temp_outward','$date_systemc','$time_systemc')"; 
// echo $sql;
// exit();
if($conn->query($sql) === FALSE) 
 { 
	throw new Exception("Code 001 : ".mysqli_error($conn));        
 }

$file_name= basename(__FILE__);
$type=1;
$val="Truck Add :"."Truck Name-".$truck_no;

$sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$username','$empcode','$val','$type')";
if ($conn->query($sql)===FALSE) {
throw new Exception("Error");  
        
             }  
$conn->query("COMMIT");

echo '<script>
swal({
     title: "Truck Add Successfully...",
      text: "Created ",
      type: "success"
      }).then(function() {
          window.location = "add_breakdown.php";
      });
</script>';
}

catch (Exception $e) {

$conn->query("ROLLBACK");
$content = htmlspecialchars($e->getMessage());
$content = htmlentities($conn->real_escape_string($content));

echo '<script>
  swal({
     title: "truck Not Insert...",
     text: "",
     icon: "error",
     button: "Back"
      }).then(function() {
         window.location = "add_breakdown.php";
     });
</script>';

$file_name = basename(__FILE__);        
$sql = "INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
if ($conn->query($sql) === FALSE) { 
echo "Error: " . $sql . "<br>" . $conn->error;
}
}

$conn->close();
?>

