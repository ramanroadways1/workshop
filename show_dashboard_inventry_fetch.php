<?php

session_start(); 
if (!isset($_SESSION['userid'])) {
$_SESSION['msg'] = "You must log in first";
header('location: login.php');
}
if (isset($_GET['logout'])) {
session_destroy();
unset($_SESSION['username']);
unset($_SESSION['userid']);
header("location: login.php");
}

require 'connect.php';

$username = $_SESSION['username']; 

$conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );

$statement = $conn->prepare("SELECT * from product_inventory where username = '$username'"); 

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

foreach($result as $row)
{

$sub_array = array();

    $sub_array[] = $row['productno'];
      $sub_array[] = $row['productname'];
      $sub_array[] = $row['producttype'];    
      $sub_array[] = $row['company'];  
      $sub_array[] = $row['pro_loc'];
      $sub_array[] = $row['quantity'];    
      $sub_array[] = $row['latest_rate'];  
      $sub_array[] = $row['timestamp1'];
      
      $data[] = $sub_array;
}

$results = array(
"sEcho" => 1,
"iTotalRecords" => $count,
"iTotalDisplayRecords" => $count,
"aaData"=>$data);

echo json_encode($results);
exit;
?>
