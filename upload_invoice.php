<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
   <script>
  function myFunction1(id_customer) {
      var v1 = document.getElementById("pn"+id_customer).value;
      /*alert(v1);*/
       $('#party_name').val(v1); 

     var v2 = document.getElementById("po"+id_customer).value;
     $('#purchase_order').val(v2); 

     var v4 = document.getElementById("p"+id_customer).value;
     $('#productno').val(v4);

     var v5 = document.getElementById("t"+id_customer).value;
     $('#total_amount').val(v5); 

     var date = new Date();
     document.getElementById("key").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes();

   } 
 </script>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <form method="post" action="insert_challan.php" enctype="multipart/form-data">
    <div class="content-wrapper">
      <section class="content-header">
      <h1>Upload Invoice Over Challans</h1>
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" style="width: 100%;" id="party_name" name="valueToSearch" placeholder="Search...">
          <span class="input-group-btn">
            <!-- <button type="button" onclick="MyFunc1()">Search</button> -->
            <button type="button" onclick="MyFunc1()" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
        <script>
          function MyFunc1()
          {
          jQuery.ajax({
              url: "search_value.php",
              data: 'party_name=' + $('#party_name').val() ,
              type: "POST",
              success: function(data) {
              $("#r1").html(data);
              },
              error: function() {}
          });
          }
         </script>
        <div id="r1" class="container">
        </div>
      </form>
    </section>
   
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
            <input type="file" id="file" name="file"  />
            <input type="hidden" name="id" id="id" >
            <input type="hidden" id="purchase_order" name="purchase_order[]"/>
            <input type="hidden" id="productno" name="productno[]">
            <input type="hidden" id="party_name" name="party_name[]">
            <input type="hidden" id="total_amount" name="total_amount[]">
            <input type="hidden" id="challan_file" name="challan_file[]"/>
            <input type="hidden" class="form-control" id="key" name="key" onclick="myFunction1()"  placeholder="Purchase Order">

            <!-- <a href="invoice_payment_with_challan.php" class="button">Open</a><br><br><br> -->
              <!-- <button type="button" class="btn btn-info" name="open" formaction="invoice_payment_with_challan.php" >Open</button> -->
            
          </form>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
     <center>
       <button type="submit" name="submit" formaction="insert_invoice.php" color="Primary" class="btn btn-primary">Submit</button>
     </center>
    </div>
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

