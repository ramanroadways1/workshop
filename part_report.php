
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
        <link href="assets/font-awesome.min.css" rel="stylesheet">
      <script src="assets/jquery-3.5.1.js"></script>
      <script src="assets/jquery-ui.min.js"></script>
      <link href="assets/jquery-ui.css" rel="stylesheet"/>
      <link href="assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="assets/custom.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                          
                              <td>View</td>
                              <td>Party Name</td>

                            </tr>
                         </thead>
                         <tfoot class="thead-light">
                            <tr>
                               <th style=" ">  </th>
                               <th style=" ">  </th>
                            </tr>
                         </tfoot>
                      </table>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "party_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV',
                             
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                              "criteria": [
                                  {
                                      "data": ' VEHICLE ',
                                      "condition": '=',
                                      "value": ['']
                                  }
                              ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          // $('title').html(" RRPL");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "txn_home_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

        <script src="assets/bootstrap.js"></script>

        <script src="assets/bootstrap.bundle.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="assets/jszip.min.js"></script>
        <script type="text/javascript" src="assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="assets/dataTables.searchBuilder.min.js"></script>


</body>
</html>


