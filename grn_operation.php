
<?php  include("connect.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>

<script >
  function getrate() {

      var rate = document.getElementById("rate").value;

      var quantity = document.getElementById("quantity").value;

      var amount = document.getElementById("amount").value;
       
      if(!(rate==0))
       {
        if(!(quantity==0))
        {
           document.getElementById("amount").value=rate*quantity;
        }
      }
      else if(!(quantity==0))
         {
          if(!(amount==0))
          {
             document.getElementById("rate").value=amount/quantity;
          }
         }
      }
function getgst()

    {
        var amount1 = document.getElementById("amount").value;
       
        var gst1=document.getElementById("gst").value;
        var total_gst1=document.getElementById("gst_amount").value;
        if(!(amount1==0))
        {
          if(!(gst1==0))
          {
            document.getElementById("gst_amount").value=amount1*gst1/100;
          }
          else if(!(total_gst1==0))
          {
            document.getElementById("gst").value=total_gst1*100/amount1;
          }
        }

    }


    function get_total(){
  /*    alert("hi");*/
      var gst_amount = document.getElementById("gst_amount").value;
      //alert(gst_amount);
      var amount = document.getElementById("amount").value;
      //alert(amount);
      document.getElementById("total_amount").value=parseInt(amount)+parseInt(gst_amount);
    }


     function get_gst()
    { 
        var gst_value1=document.getElementById("gst").value;
        var gst_type1= document.getElementById("gst_type").value;
        if(gst_type1=="cgst_sgst"){
            var result = gst_type1+ "," +gst_value1/2;
            //alert(result);
          }else
          {
            var result = gst_type1+ "," +gst_value1;
          }
          //var result= parseInt(gst_value1)*parseInt(gst_type1);
          document.getElementById("sel_gst").value=result;
          
    }
    function gettotal(){
      var received_qty = document.getElementById("received_qty").value;
      var rate = document.getElementById("rate11").value;
      var new_amount = received_qty*rate;
      var gst= document.getElementById("gst11").value;
      var gst_amount=new_amount*gst/100;
     
      if(!(received_qty==0)){
        document.getElementById("new_total_amount").value=parseInt(new_amount)+parseInt(gst_amount);
      }
    }

</script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

     
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On G.R.N</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-size: 13px; font-family: verdana;">  
                  <thead>  
                       <tr>  
                        <td scope="row" style="display: none;">date Purchase Id</td>
                           <td scope="row" style="display: none;">id</td>
                            <td>View</td>
                             <td>GRN No</td>
                           <td>P.O No</td>
                              <td>Product Count</td>
                              <td>Party Name</td>
                              <td>Party Code</td>
                              <td>Total(GRN Total)</td>
                              <td>Date</td>
                              <td>Branch</td>
                               <td>View</td> 
                               <td>Delete GRN</td> 
                       </tr>    
                  </thead> 
                    <?php
                        $query = "SELECT grn.username,grn.id, grn.grn_no, grn.purchase_order,COUNT(grn.productno) as productno, grn.productname, grn.party_name, grn.party_code, grn.quantity, grn.rate,grn.amount,grn.gst,grn.gst_amount,grn.total_amount,grn.received_qty,sum(grn.new_total) as new_total,grn.key1,grn.timestamp1,files_upload.approve_status FROM grn join files_upload where grn.grn_no=files_upload.grn_no and files_upload.approve_status = '0' or files_upload.approve_status = '1' or files_upload.approve_status = '1' group by grn.purchase_order";

                        $result = mysqli_query($conn, $query);
                        $l_u = 1;
                        $id_customer = 0;
                      ?>   
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $key1 = $row['key1'];
                    $grn_no=$row['grn_no'];
                    $purchase_order = $row['purchase_order'];
                    $productno = $row['productno'];
                    $productname = $row['productname'];
                    $party_name = $row['party_name'];
                    $total_amount = $row['total_amount'];
                    $party_code = $row['party_code'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $gst_amount = $row['gst_amount'];
                    $gst = $row['gst'];
                    $amount = $row['amount'];
                    $date=$row['timestamp1']; 
                    $received_qty = $row['received_qty'];
                    $new_total=$row['new_total']; 
                     $username=$row['username']; 

                     $query2 = "SELECT id,purchase_order FROM date_purchase_key where   purchase_order='$purchase_order' group by purchase_order";
                      $result2 = mysqli_query($conn, $query2);
                       while($row3 = mysqli_fetch_array($result2))
                      {
                       $date_purchase_id = $row3['id'];
                       $purchase_order = $row3['purchase_order'];
                       
                  ?>
                  <tr>
                    <input type="hidden" name="key1[]" id="key1<?php echo $date_purchase_id; ?>" value='<?php echo $key1; ?>'>
                     <td style="display: none;"><?php echo $date_purchase_id?>
                      <input type="hidden" name="date_purchase_id[]" id="date_purchase_id<?php echo $date_purchase_id; ?>" value="<?php echo $date_purchase_id; ?>">
                      <input type="hidden" name="purchase_order[]" value="<?php echo $purchase_order; ?>">
                    </td>

                    <td style="display: none;"><?php echo $id?>
                      <input type="hidden" name="id[]" id="id<?php echo $date_purchase_id; ?>" value="<?php echo $id; ?>">
                    </td>
                    <td >
                     <form method="POST" action="show_grn_operation_detail.php" target="_blank">
                        <input type="hidden" name="purchase_order1" value='<?php echo $purchase_order; ?>'>
                        <input type="submit"  value="View" class="btn btn-primary btn-sm" >
                      </form>
                     </td>
                      <td ><?php echo $grn_no?>
                      <input  type="hidden" readonly="readonly" name="purchase_order[]" value="<?php echo $grn_no; ?>" id="grn_no<?php echo $date_purchase_id; ?>">
                    </td>
                     <td ><?php echo $purchase_order?>
                      <input  type="hidden" readonly="readonly" name="purchase_order[]" value="<?php echo $purchase_order; ?>" id="purchase_order<?php echo $date_purchase_id; ?>">
                    </td>
                    
                    <td >
                          <?php
                               $sql5 = "SELECT count(productno) as productno FROM grn where   purchase_order='$purchase_order' group by purchase_order  ";
                               $result5 = mysqli_query($conn, $sql5);
                                  if(mysqli_num_rows($result5) > 0)  
                                  {
                                    $row2 = mysqli_fetch_array($result5);
                                     $productno = $row2["productno"];
                                     echo $productno;

                                   }
                            ?>

                      <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>" id="a<?php echo $id; ?>">
                    </td>
                    <td ><?php echo $party_name?>
                      <input  type="hidden" readonly="readonly" name="party_name[]" value="<?php echo $party_name; ?>" id="a<?php echo $id; ?>">
                    </td>
                     <td ><?php echo $party_code?>
                      <input  type="hidden" readonly="readonly" name="party_code[]" value="<?php echo $party_code; ?>" id="a<?php echo $id; ?>">
                    </td>
                    
                  <!--   <td><?php echo $quantity?>
                     <input type="hidden" readonly="readonly" id="quantity<?php echo $l_u; ?>"  onclick="getrate(<?php echo $l_u; ?>);" name="quantity[]" value="<?php echo $quantity; ?>" >
                   </td>
              
                    <td><?php echo $rate?>
                        <input type="hidden" readonly="readonly" id="rate<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="rate[]" value="<?php echo $rate; ?>" >
                      </td> -->

                   <!--   <td><?php echo $amount?>
                        <input type="hidden" readonly="readonly" id="amount<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="amount[]" value="<?php echo $amount; ?>" >
                     </td>

                     <td><?php echo $gst?>
                      <input type="hidden" readonly="readonly" id="gst<?php echo $l_u; ?>" onclick="getgst(<?php echo $l_u; ?>)" name="gst[]" value="<?php echo $gst; ?>" >
                     </td>

                    <td><?php echo $gst_amount?>
                      <input type="hidden" readonly="readonly" id="gst_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="gst_amount[]" value="<?php echo $gst_amount; ?>" >
                    </td> -->
                     <!-- <td><?php echo $total_amount?>
                      <input type="hidden" readonly="readonly" id="total_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="total_amount[]" value="<?php echo $total_amount; ?>" >
                    </td>
                    <td><?php echo $received_qty?>
                      <input type="hidden" readonly="readonly" id="received_qty<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" oninput="ChkForQtyMaxValue();gettotal();" name="received_qty[]" value="<?php echo $received_qty; ?>" >
                    </td> -->
                    <td><?php echo $new_total?>
                      <input type="hidden" readonly="readonly" id="new_total<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="new_total[]" value="<?php echo $new_total; ?>" >
                    </td>

                    
                    <td><?php echo $date?>
                      <input type="hidden" readonly="readonly" name="date[]" value="<?php echo $date; ?>" >
                    </td>

                    <td><?php echo $username?>
                      <input type="hidden" readonly="readonly" name="username[]" value="<?php echo $username; ?>" >
                    </td>
                    <td>
                       <form method="POST" action="detail_data_to_update_grn.php" target="_blank">
                        <input type="hidden" name="purchase_order" value='<?php echo $purchase_order; ?>'>
                        <button type="submit" class="btn btn-primary btn-sm" style="height: 27.7152px; width: 38px;"><i class="fa fa-edit"></i></button>
                         </form>
                    </td> 
                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $date_purchase_id;?>')" name="delete" value="X" class="btn btn-danger btn-sm"/>
                    </td>
                    <div id="result_main22"></div>
                            <script>
                              function DeleteModal(purchase_id){
                                var purchase_id = purchase_id;
                                var grn_no = $('#grn_no'+purchase_id).val();
                                //alert(grn_no);
                                var purchase_order = $('#purchase_order'+purchase_id).val();
                                var id = $('#id'+purchase_id).val();
                                if (confirm("Do you want to delete this GRN..?"))
                              {
                                if(purchase_id!='')
                               {
                                 jQuery.ajax({
                                    url: "delete_grn.php",
                                    data: 'purchase_id=' + purchase_id  + '&grn_no='+grn_no   + '&purchase_order='+purchase_order   + '&id='+id ,
                                    type: "POST",
                                    success: function(data) { 
                                      alert(data)
                                         location.reload(); 
                                    $("#result_main22").html(data);
                                    },
                                        error: function() {}
                                  });
                                }

                             }

                          }


                              function EditModal(id)
                              {
                                jQuery.ajax({
                                    url: "fetch_grn.php",
                                    data: 'id=' + id,
                                    type: "POST",
                                    success: function(data) {
                                    $("#result_main").html(data);
                                    },
                                        error: function() {}
                                    });
                                document.getElementById("modal_button1").click();
                                $('#ModalId').val(id);

                                }
                           </script><div id="result_main"></div>
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  } }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update GRN</h4>
      </div>
      <form action="update_grn.php" method="post" id="FormGRNUpdate">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
              <input type="hidden" id="ModalId" name="id"/>
              <div class="form-group">
                <label>Purchase Order</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="purchase_order11" autocomplete="off" class="form-control" name="purchase_order" required/>
              </div>

              <div class="form-group">
                <label>Product Number</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productno11" autocomplete="off" class="form-control" name="productno" required/>
              </div>

               <div class="form-group">
                <label>Product Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productname11" autocomplete="off" class="form-control" name="productname" required/>
              </div>

               <div class="form-group">
                <label>Party Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="party_name11" autocomplete="off" class="form-control" name="party_name" required/>
              </div>
              
              <div class="form-group">
                 <label>Party Code</label>
                 <input type="text" style="width: 100%;" readonly="readonly" id="party_code11" autocomplete="off" class="form-control" name="party_code" required/>
              </div>

              <div class="form-group">
                 <label>Quantity</label>
                 <input type="text" style="width: 100%;" id="quantity11" autocomplete="off" class="form-control" name="quantity" readonly="readonly" required/>
              </div>             

              <div class="form-group">
                 <label>Rate/unit</label>
                 <input type="text" style="width: 100%;" id="rate11" readonly="readonly" autocomplete="off" class="form-control" name="rate" placeholder="rate unit" required/>
              </div>

             </div>
            <!-- /.col -->
            <div class="col-md-6">
              
               <div class="form-group">
                 <label>Amount</label>
                 <input type="text" style="width: 100%;" autocomplete="off" readonly="readonly" id="amount11" class="form-control" name="amount" placeholder="amount" required/>
              </div>
               <div class="form-group">
                 <label>Gst</label>
                 <input type="number" style="width: 100%;" class="form-control" id="gst11" name="gst" readonly />
               </div>
        
                <div class="form-group">
                  <label>Gst Amount</label>
                  <input type="number" style="width: 100%;" readonly="readonly" class="form-control" id="gst_amount" name="gst_amount" />
                </div>

                <div class="form-group">
                  <label>Total Amount</label>
                    <input type="text" style="width: 100%;" readonly="readonly" id="total_amount" class="form-control" autocomplete="off" name="total_amount" required>
                </div>

                <div class="form-group">
                  <label>Received Quantity</label>
                    <input type="text" style="width: 100%;" id="received_qty" class="form-control" autocomplete="off" readonly="readonly" oninput="ChkForQtyMaxValue();gettotal();"name="received_qty" required>
                </div>

                <div class="form-group">
                  <label>New Total</label>
                    <input type="text" style="width: 100%;" id="new_total_amount" class="form-control" autocomplete="off" name="new_total" readonly>
                </div>
                
                  
            </div>
            <!-- /.col -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Submit</button>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          </div>
      </div>
    </form>
      
    </div>

  </div>
</div>
<script>
function ChkForQtyMaxValue(myVal)
{
 var qty = Number($('#quantity11').val());
 var received_qty = Number($('#received_qty').val());
 if(received_qty>qty)
  {
     alert('You can not exceed Quantity '+ qty);
     $('#received_qty').val('');
 }
}
</script>

