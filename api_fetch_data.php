<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "insert_pay_invoice";
    // object properties
    public $grn_no;
    public $party_name;
    public $username;
    public $company;
    public $payment;
    public $bank_name;
    public $pan;
    public $ifsc_code;
    public $mobile_no;
    public $grn_date;
    public $payment_date;
    public $acc_holder_name;
    public $acc_no;
 
    // constructor with $db as database connection
            public function __construct($db){
                $this->conn = $db;
            }

            function read(){
                     $query = "SELECT * FROM " . $this->table_name ." where approve='1' ORDER BY grn_no DESC";
                     
                        // prepare query statement
                        $stmt = $this->conn->prepare($query);
                     
                        // execute query
                        $stmt->execute();
                 
                        return $stmt;
                    }
           

}
?>