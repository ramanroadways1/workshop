<?php 
  include "connect.php";
?>

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <?php include("aside_main.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" autocomplete="off">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Product Detail By Grn</h1>
    </section>

    <section class="content">
    <div class="box">
       <div class="box-body">
           <div class="row">
            
               <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                     <?php  $username = $_SESSION['username']; ?>
                     <input type="hidden" value="<?php echo $username; ?>" name="username" id="username" class="form-control"/>  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
           
              <table id="order_table"  style="width:100%">
                       
              </table>  
           </div>
        
        </div>

      </section>

    </div>
  </form>

</div>
</body>
</html>

 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
              var username = document.getElementById('username').value;
              //alert(username);
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"filter_grn.php",  
                          method:"POST",  
                          data:{from_date:from_date,to_date:to_date,username:username},  
                          success:function(data)  
                          {  
                            $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>
<!-- <script>
  $( function() {
    $( "#start_date" ).datepicker();
  } );
  </script>

  <script>
  $( function() {
    $( "#end_date" ).datepicker();
  } );
  </script> -->

<!-- <script type="text/javascript" language="javascript" >

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#order').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order" : [],
   "ajax" : {
    url:"filter_grn.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }
 

</script> -->