<?php
require("connect.php");
include('header.php');
include('aside_main.php');
?>
<!DOCTYPE html>
<html>
<head>
<style>
     
      .leftDiv
      {
        color: #000;
        height: 170px;
        width: 60%;
        float: left;
      }

      .rightDiv
      {
        
        color: #000;
        height: 170px;
        width: 40%;
        float: right;
      }     
    </style>



 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>

<style type="text/css">
  @media print {
  button,footer {
    display: none !important;
  }
  input,
  textarea,select {
    border: none !important;
    box-shadow: none !important;
    outline: none !important;
    display: none !important;
  }
  .box{
    border-top-width: 0px;
  }
  #print,#sub,#new_prblm,#issue,#sec,#extra_bill {
    display: none;
  }
  #first{
    float: left;
  }

  .page-break 
{  display: block; 
       page-break-before: always; 
}


  div{
       font-size: 16pt;
         font-family:verdana;
  }
   table, tr,body,form,td  {
        height: auto;
        font-size: 16pt;
         font-family:verdana;
        font: solid #000 !important;
        }
             table {
       border: solid #000 !important;
        border-width: 1px 0 0 1px !important;
    }
    th, td,tr {
        border: solid #000 !important;
        border-width: 0 1px 1px 0 !important;
    }
       
  body {
  zoom:50%; /*or whatever percentage you need, play around with this number*/
}
 #print_page
  {
    width: 500px;
  }
}
    
}
    
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  
  <div class="content-wrapper">
    <section class="content-header">
    
    </section>
   
    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">External jobcard Data</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $username = $_SESSION['username'];
          @$valueToSearch=$_POST['job_card_no'];

          $sql = "SELECT * FROM  external_job_cards_main WHERE  job_card_no='$valueToSearch' and username='$username'";
              $result = $conn->query($sql);
                  
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
                       $truck_no2 = $row["truck_no2"];
                       $truck_driver = $row["truck_driver"];
                       $break_point = $row["break_point"];
                       $state = $row["state"];
                       $job_card_no = $row["job_card_no"];
                       $date1 = $row["date1"];
                        $submission_date = $row["submission_date"];
                       $yes_no = $row["yes_no"];
                       $vehicle_no = $row["vehicle_no"];
                       $employee_name = $row["employee_name"];
                       $submit_by = $row["submit_by"];

                       $challan_file = $row["challan_file"];
                       $challan_file1=explode(",",$challan_file);
                       $count3=count($challan_file1);

                       $extra_bill = $row["extra_bill"];
                       $extra_bill1=explode(",",$extra_bill);
                       $count4=count($extra_bill1);

                  }
               ?>
               <script>
        function myFunction() {
          window.print();
        }

        /*  function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }*/
        
       </script>
               <div class="col-md-12">
                   <input type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning" onclick="myFunction();" value="Print jobcard">
                </div>

              <div id="mydiv" >  

             <div class="leftDiv" >
               <div class="row" id="first">
                 <div class="col-md-6" >
                   <label>Truck Number:</label>
                 <?php echo $truck_no2;?><br>
                
                <label>Truck Driver:</label>
                <?php echo $truck_driver;?><br>
                </div>
                 <div class="col-md-6" >
                <label>Brake Point:</label>
                <?php echo $break_point;?><br>

                 <label>State:</label>
                <?php echo $state;?><br>

                 </div>
               </div>

                <div class="row" >
                 <div class="col-md-6" id="sec">

                  <label>Own vehicle pickup sent:</label>
                  <?php echo $yes_no;?><br>

                   <label>JobCard No:</label>
                 <?php echo $job_card_no;?><br>
                
                <label>JobCard Start Date:</label>
                <?php echo $date1;?><br>
                </div>

                <div class="col-md-6" >

                  <label>Employee Name:</label>
                <?php echo $employee_name;?><br>
                

                 <label>vehicle no:</label>
                <?php echo $vehicle_no;?><br>

                <label>Submited By:</label>
                <?php echo $submit_by;?><br>
                 </div>
               </div>
             </div>

             <div class="rightDiv">
                  <label>Jobcard Submit Date:</label>
                <?php echo $submission_date;?><br>
       <div id="hide">
               <label id="extra_bill">Challan Bill:</label>
                <?php
                if (strlen($challan_file) > 0) {
                   for($i=0; $i<$count3; $i++){
                    
                    ?>
                    <a id="extra_bill" href="../gate/<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a><br>
                     <?php }  }  
                     else{
                      ?>
                         <font id="extra_bill"><?php echo "No file"; ?></font>
                      <?php
                      
                     }
                     ?><br>

                <label id="extra_bill">Extra Bill:</label>
                <?php
                if (strlen($extra_bill) > 0) {
                   for($j=0; $j<$count4; $j++){
                    ?>
                    <a id="extra_bill" href="../gate/<?php echo $extra_bill1[$j]; ?>" target="_blank">File <?php echo $j+1; ?></a><br>
                     <?php } 
                     }  
                     else{
                      ?>
                             <font id="extra_bill"><?php echo "No file"; ?></font>
                      <?php
                     }

                      ?>
            </div>

              </div>  

            <?php
            
          }
          else{
            echo "<SCRIPT>
                        window.location.href='found_external_job.php';
           </SCRIPT>";
            exit();
          }
            ?>
          <!--    <button type="button"  class="btn btn-info" style='margin-left:87%;' data-toggle="modal" data-target="#myModal">Add New Problem</button> -->
        <center><h4 style="color: #7c795d; font-family: 'verdana', sans-serif; font-size: 15px; font-weight: 400; line-height: 32px; margin: 0 0 24px;">External Complaint Table</h4></center>
        <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  13px;">  
                 <thead>   
                              <tr class="table-active">
                                  <th class="col-md-10" >Complaint</th>
                                </tr>
                      </thead> 
                   <?php  
                      $query = "SELECT * FROM  external_jc_problem_main WHERE  job_card_no='$valueToSearch' and username='$username'";
                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $problem=$row['problem'];
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $problem?>            
                </td>
           
           
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }
                  ?>  
                </table> 



                   <center><h4 style="color: #7c795d; font-family: 'verdana', sans-serif; font-size: 15px; font-weight: 400; line-height: 32px; margin: 0 0 24px;">External Product Table</h4></center>
                <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:arial; font-size:  15px;">  
                 <thead>   
                              <tr class="table-active">
                                  <th class="col-md-4">Product Name</th>
                                   <th class="col-md-4">Product Quantity</th>
                                   <th class="col-md-4">Amount</th>
                                </tr>
                      </thead> 
                   <?php  
                      $query = "SELECT * FROM  external_jc_product_main WHERE  job_card_no='$valueToSearch' and username='$username'";
                         
                      $sum=mysqli_query($conn,"SELECT SUM(amount) as amount FROM external_jc_product_main WHERE job_card_no='$valueToSearch' and username='$username'");
                      $row2=mysqli_fetch_array($sum);

                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $product_name=$row['product_name'];
                    $quantity=$row['quantity'];
                    $amount=$row['amount'];
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $product_name?>            
                </td>
                <td> <?php echo $quantity?>            
                </td>
                <td> <?php echo $amount?>            
                </td>
           
           
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }

                  echo "<tr>
                    
                    <td colspan='2'>Total Calculation : </td>
                    <td>$row2[amount]</td>
                  </tr>";
                  ?>  
                </table> 

                <center><h4 style="color: #7c795d; font-family: 'verdana', sans-serif; font-size: 15px; font-weight: 400; line-height: 32px; margin: 0 0 24px;">External Service Table</h4></center>
                <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:verdana; font-size:  13px;">  
                 <thead>   
                              <tr class="table-active">
                                  <th class="col-md-4">Service Name</th>
                                   <th class="col-md-4">Service Amount</th>
                                </tr>
                      </thead> 
                   <?php  
                      $query = "SELECT * FROM  external_jc_service_main WHERE  job_card_no='$valueToSearch' and username='$username'";

                      $sum4=mysqli_query($conn,"SELECT SUM(service_amount) as service_amount FROM external_jc_service_main WHERE job_card_no='$valueToSearch' and username='$username'");
                      $row4=mysqli_fetch_array($sum4);


                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1; 
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $service_name=$row['service_name'];
                    $service_amount=$row['service_amount'];
                 
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $service_name?>            
                </td>
                <td> <?php echo $service_amount?>            
                </td>
                
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }
                   echo "<tr>
                    
                    <td colspan='1'>Total Calculation : </td>
                    <td>$row4[service_amount]</td>
                  </tr>";

                  ?>  
                </table> 

                <!-- Reports External Mistry -->
                  <center><h4 style="color: #7c795d; font-family: 'verdana', sans-serif; font-size: 15px; font-weight: 400; line-height: 32px; margin: 0 0 24px;">External Mistry Table</h4></center>
                <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:arial; font-size:  15px;">  
                 <thead>   
                              <tr class="table-active">
                                  <th class="col-md-4">Mistry Name</th>
                                   <th class="col-md-4">Service(Expencive)</th>
                                   <th class="col-md-4">Amount</th>
                                </tr>
                      </thead> 
                   <?php  
                      $query = "SELECT * FROM  external_jc_mistry_main WHERE  job_card_no='$valueToSearch' and username='$username'";
                         
                      $sum=mysqli_query($conn,"SELECT SUM(work_amount) as amount FROM external_jc_mistry_main WHERE job_card_no='$valueToSearch' and username='$username'");
                      $row2=mysqli_fetch_array($sum);

                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $job_card_no=$row['job_card_no'];
                    $mistry_name=$row['mistry_name'];
                    $expencive=$row['expencive'];
                    $amount=$row['work_amount'];
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $mistry_name?>            
                </td>
                <td> <?php echo $expencive?>            
                </td>
                <td> <?php echo $amount?>            
                </td>
           
           
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }

                  echo "<tr>
                    
                    <td colspan='2'>Total Calculation : </td>
                    <td>$row2[amount]</td>
                  </tr>";
                  ?>  
                </table> 
                <!-- Close Mistry -->
                <center><h4 style="color: #7c795d; font-family: 'verdana', sans-serif; font-size: 15px; font-weight: 400; line-height: 32px; margin: 0 0 24px;">Issue product in External jobcard</h4></center>
                 <div class="table-responsive">  
                    
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  
                  <?php  
                         $sql = "SELECT * from add_product_external_jobcard where job_card_no='$job_card_no' and username='$username'";

                           $sum2=mysqli_query($conn,"SELECT SUM(amount) as amount FROM add_product_external_jobcard WHERE job_card_no='$job_card_no' and username='$username'");
                      $row4=mysqli_fetch_array($sum2);


                         $result = $conn->query($sql);
                      if(mysqli_num_rows($result)!=0)
                      {
                        ?>
                         <thead>  
                       <tr>  
                            <td>truck number</td>  
                            <td>job card number</td>  
                            <td>Product name</td>  
                            <td>Product type</td>
                            <td>Available Quantity</td>  
                            <td>Recive quantity</td>  
                            <td>Mistry</td>
                            <td>Purchase Rate</td>
                            <td>Your Amount</td>
                             <td>Date/Time</td>
                       </tr>  
                  </thead> 
                        <?php
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                            <td>'.$row["truck_no"].'</td>  
                            <td>'.$row["job_card_no"].'</td>  
                            <td>'.$row["partsname"].'</td>
                            <td>'.$row["partstype"].'</td>  
                            <td>'.$row["available_qty"].'</td>
                            <td>'.$row["quantity"].'</td>  
                            <td>'.$row["mistry"].'</td> 
                            <td>'.$row["latest_rate"].'</td>
                              <td>'.$row["amount"].'</td> 
                            <td>'.$row["timestamp1"].'</td> 
                       </tr>  
                       ';  
                  }
                  } else{
                    echo "No data";
                  }   echo "<tr>
                   
                    <td colspan='8'>Total Calculation : </td>
                    <td>$row4[amount]</td>
                  </tr>";
                  ?>  
                </table>  
              </div>


              </div>  
              
                  
          </form>
<br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

