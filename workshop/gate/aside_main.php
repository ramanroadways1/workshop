<!-- UPDATE own_truck 
INNER JOIN rrpl_workshop.registered_truck
  ON e_dairy.own_truck.tno = rrpl_workshop.registered_truck.truck_no and rrpl_workshop.registered_truck.status >0
SET e_dairy.own_truck.status = '2' -->

<!-- SELECT e_dairy.own_truck.tno, rrpl_workshop.registered_truck.truck_no
  FROM e_dairy.own_truck INNER JOIN rrpl_workshop.registered_truck
  ON e_dairy.own_truck.tno = rrpl_workshop.registered_truck.truck_no where rrpl_workshop.registered_truck.status >0 --> 


  <header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>MNGT</span>
      <span class="logo-lg"><b>WorkShop</b></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <!-- <span class="sr-only">Toggle navigation</span> -->
      </a>

    <br>
        <?php  if (isset($_SESSION['username'])) : ?>
      <p style="float:right; margin-right:20px;"><strong><?php 
         echo $_SESSION['username'];

         echo "/";

         echo $_SESSION['employee'];

      ?></strong>
      <a href="../../main_index.php" style="color: #FFFF00;">Back(Vendor)</a> </p><br>
    <?php endif ?>

<!--       <?php  if (isset($_SESSION['username'])) : ?>
      <p style="float:right; margin-right:20px;" align="right">Welcome <strong><?php echo $_SESSION['username']; ?>  </strong>
        <strong><?php echo $_SESSION['employee']; ?>  </strong>
      <a href="../../logout.php?logout='1'" style="color: red;">logout</a> </p>
    <?php endif ?> -->
     
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="home.php"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
        </li>

          <li class="treeview">

          <a href="#">
            <i class="fa fa-sign-in"></i> <span>Gate Login</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="gate_login.php"><i class="fa fa-circle-o"></i>Gate Login(Reg. Truck)</a></li>
            <li><a href="chassis_truck.php"><i class="fa fa-circle-o"></i>Chassis Truck</a></li>
          </ul>
        </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil"></i> <span>Inspection</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="inspection_report.php"><i class="fa fa-circle-o"></i>Inspection Reports</a></li>
            <li><a href="fount_data.php"><i class="fa fa-circle-o"></i>Search Inspection Reports</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Jobcard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="office_jobcard_record.php"><i class="fa fa-circle-o"></i>Office Jobcard</a></li>
            <li><a href="internal_job_card.php"><i class="fa fa-circle-o"></i>Internal Job card</a></li>
            <li><a href="external_job_cards.php"><i class="fa fa-circle-o"></i>External Job Card </a></li>
            <li><a href="pending_external_jobcard.php"><i class="fa fa-circle-o"></i>Pending External Jobcard</a></li>
             <li><a href="internal_job_card_view.php"><i class="fa fa-circle-o"></i>All Internal Jobcard</a></li>
            <li><a href="found_external_job_view.php"><i class="fa fa-circle-o"></i>All External Jobcard</a></li>
            <li><a href="show_add_product_jobcard1.php"><i class="fa fa-circle-o"></i>Issue Product In Internaljobcard</a></li>
            <li><a href="show_external_issue_product1.php"><i class="fa fa-circle-o"></i>Issue Product In External JobCard</a></li>
          </ul>
        </li>
        <li class="treeview">
        <a href="#">
        <i class="fa fa-pencil"></i> <span>All Pending Job Card</span>
        <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="overall_pendingjobcard.php"><i class="fa fa-circle-o"></i>Pending Job card</a></li>
        <li><a href="edit_gate.php"><i class="fa fa-circle-o"></i>GET IN Truck</a></li>
       <!--  <li><a href="fount_data.php"><i class="fa fa-circle-o"></i>Search Inspection Reports</a></li> -->
        </ul>
        </li>

<!--<li>
      <a href="gate_login.php">
      <i class="fa fa-sign-in"></i> <span>Gate Login</span>
      </a>
  </li>-->

<!--<li>
          <a href="index_inspection.php">
            <i class="fa fa-check-square-o"></i> <span>Inspection Report</span>
          </a>
        </li> 
         <li>
          <a href="index_jobcard.php">
            <i class="fa fa-tasks"></i> <span>Jobcard Record</span>
          </a>
        </li> --> 

      </ul>
    </section>
  </aside>