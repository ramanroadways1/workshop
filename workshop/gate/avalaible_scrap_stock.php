    

<?php
require("connect.php");
include('header.php');

?>
 <?php  

 $sql = "SELECT * from scrap_stock group by scrap_no";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_jobcard.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Scrap Stock</h3>
         <input type="button" style="float: right;" onclick="window.location.href = 'scrap_stock.php';" name="filter" id="filter" value="Add New" class="btn btn-info" />  
        
      </div>
      <script>
        function myFunction() {
          window.print();
        }
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }
            
       </script>
         <div class="row">
             <!--    
                <div class="col-md-12"> -->
               <!--  <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-2">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>
            </div>  -->
           
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row-2">
           
            <div class="col-md-12">
              <div id="order_table"> 
              <div class="table-responsive">  
               <table  id="employee_data"  class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px; background-color: #EEEDEC ">
                  <thead>  
                       <tr class="table-active">
                            <td>Scrap Number</td>  
                            <td>Scrap Out Date</td>  
                            <td >Product no</td>  
                            <td>Product Name</td>
                            <td>Company</td>
                             <td>Product Type</td>
                            <td>Product Location</td> 
                            <td>Avl Quantity</td> 
                            <td>Scrap Quantity</td> 
                            <td>Amount</td>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       $scrap_no = $row["scrap_no"];
                       $scrap_out_date = $row["scrap_out_date"];
                       $productno = $row["productno"];
                       $productname = $row["productname"];
                       $company = $row["company"];
                       $producttype = $row["producttype"];
                        $pro_loc = $row["pro_loc"];
                       $avl_qty = $row["avl_qty"];
                       $scrap_qty = $row["scrap_qty"];
                       $amount = $row["amount"];
                       $now_date = $row["now_date"]; 
                      ?>
                   <tr>
                   <td>
                     <form method="POST" action="action_on_scrap_data.php">
                      <input type="submit"  name="scrap_no" value="<?php echo $scrap_no?>" class="btn btn-primary btn-sm" >
                    </form>
                    </td>
                    <td><?php echo $scrap_out_date?></td>
                    <td><?php echo $productno?></td>
                    <td><?php echo $productname?></td>
                   <td><?php echo $company?> </td>
                    <td><?php echo $producttype?></td>
                    <td><?php echo $pro_loc?></td>
                   <td><?php echo $avl_qty?> </td>
                    <td><?php echo $scrap_qty?></td>
                    <td><?php echo $amount?></td>
                  <!--  <td><?php echo $now_date?> </td> -->
                </tr>
                        
                 <?php }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
      
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_internal_job.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

