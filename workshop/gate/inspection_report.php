<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php'); ?>
  
  <div class="content-wrapper">

   
    <section class="content-header">
        </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Inspection Report</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                    
                     <?php $username =  $_SESSION['username']; ?>
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:arial; font-size:  14px;">  
                  <thead>  
                       <tr>  
                           <td>Truck Number</td>
                            <td>Inspection Number</td>
                            <td>Truck Driver</td>
                             <td>km</td>
                              <td>Employee Name</td>
                             <td>Date</td>
                           
                       </tr>  
                  </thead>  
                   <?php
                    $show = "SELECT truck_driver.*,emp_data.empname from truck_driver LEFT JOIN emp_data ON truck_driver.employee=emp_data.empcode where truck_driver.status='0' and truck_driver.username='$username' ORDER BY insp_no DESC";

                    $result = $conn->query($show);  

                    if ($result->num_rows > 0) {
                    
                      while($row = $result->fetch_assoc()) {
                       
                        $truck_no = $row["truck_no"];
                         $truck_driver = $row["truck_driver"];
                         $inspection_no = $row["insp_no"];
                         $km = $row['km'];

                        $date1 = $row["date1"];
                        $empname=$row["empname"];
                        
                      ?>
                      <tr> 
                        <input type="hidden" name="username" id="username" value='<?php echo $username; ?>'>
                  
                   <input type="hidden" name="truck_no" id="truck_no" value='<?php echo $truck_no; ?>'>
                 
                   <td>
                    <form method="POST" action="fetch_inspectiondata.php">
                      <input type="submit"  name="truck_no" value="<?php echo $truck_no; ?>" class="btn btn-primary btn-sm" >
                    </form>
                  </td>
                  <td><?php echo $inspection_no?>
                    <input type="hidden" name="inspection_no" id="inspection_no1" value='<?php echo $inspection_no; ?>'>
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $truck_driver; ?>" >
                  </td>
                 <!--  <td><?php echo $km?>
                    <input type="hidden" readonly="readonly" name="km" value="<?php echo $km; ?>" >
                  </td> -->
                  <td><?php echo $km?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $km; ?>" >
                  </td>

                  <td><?php echo $empname?>
                    <input type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" >
                  </td>

                   <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                  
                     
                </tr>
                 <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
                </table>  

              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>

     
    </section>
 </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

