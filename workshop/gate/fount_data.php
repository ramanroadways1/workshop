<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper">

<?php include('aside_main.php');
 ?>
  
  <div class="content-wrapper">
 
    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title" style="font-family:verdana; font-size:  15px;">Submited Inspection Report(Pending in jobcard)</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                    
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                           <td>Truck Number</td>
                            <td>Inspection Number</td>
                            <td>Truck Driver</td>
                            <td>Date</td>
                            <td>View</td>
                       </tr>  
                  </thead>  
                   <?php
                    $username =  $_SESSION['username'];  
                    $show = "SELECT * from inspection_record  where username='$username' group by inspection_no ORDER BY inspection_no DESC";

                    $result = $conn->query($show);  

                    if ($result->num_rows > 0) {
                    
                      while($row = $result->fetch_assoc()) {
                       
                        $truck_no = $row["truck_no1"];
                         $truck_driver = $row["truck_driver"];
                         $inspection_no = $row["inspection_no"];
                        /* $km = $row['km'];*/
                      
                        $date1 = $row["date1"];
                        
                      ?>
                      <tr>
                   <input type="hidden" name="truck_no" id="truck_no" value='<?php echo $truck_no; ?>'>
                 
                  <td><?php echo $truck_no?>
                    <input type="hidden" name="truck_no" value='<?php echo $truck_no; ?>'>
                  </td>
                  <td><?php echo $inspection_no?>
                    <input type="hidden" name="inspection_no" id="inspection_no1" value='<?php echo $inspection_no; ?>'>
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $truck_driver; ?>" >
                  </td>
                 <!--  <td><?php echo $km?>
                    <input type="hidden" readonly="readonly" name="km" value="<?php echo $km; ?>" >
                  </td> -->
                   

                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td>
                    <!--  <a href="inspection_detail_data.php?link=<?php echo $inspection_no; ?>"><?php echo "view"?></a> -->
                    <form method="POST" action="inspection_detail_data.php">
                      <input type="hidden" name="username" value="<?php echo $username ?>">
                      <input type="submit" align="center" name="inspection_no" value="<?php echo $inspection_no; ?>" class="btn btn-primary btn-sm" >
                    </form>
                  </td>
                     
                </tr>
                 <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

