  <?php
require("connect.php");
include('header.php');
?>

<!DOCTYPE html>
<html>
<head>
  
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

                      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
                       <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
                       <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-family: verdana;">
<div class="wrapper">
   <?php include('aside_main.php') ?>

  <div class="content-wrapper">
    
    <section class="content">

      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Chassis  Truck Login</h3>

          <div class="box-tools pull-right">
           <!--  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-5">
           <form  method="post" id="MyForm1">
            <div class="row">
               <div class="col-md-8">
                 <b>Chassis  Number</b>
                  <input type="text" style="width: 140%;" id="truck_no" name="truck_no" placeholder="Enter Chassis Number.." onchange="get_data1(this.value)" required="required" autocomplete="off" class="form-control" required='required'/>
               </div><br>
                    <div id="rate_master1"></div>
                    <script type="text/javascript">
                      function get_data1(val) {
                            $.ajax({
                              type: "POST",
                              url: "chk_chassis_truck.php",
                              data:'truck_no='+val,
                              success: function(data){
                                  $("#rate_master1").html(data);
                               }
                              });
                          }
                    </script>
              </div>  <div id="rate_master222"></div> 
         </form>
         <br>
         <?php $username =  $_SESSION['username']; ?>
         <form method="post" id="form" action="insert_gate_login.php">
              <input type="hidden" name="username" value="<?php echo $username ?>">
              <input type="hidden" autocomplete="off" id="truck_no1" name="truck_no1" autocomplete="off" class="form-control" required  placeholder="truck driver" />
              
              <b>Truck Driver</b>
               <input type="text" style="width: 90%;" autocomplete="off" id="truck_driver" autocomplete="off" name="truck_driver" class="form-control" placeholder="truck driver" /><br>
              
                <b>Km Reading</b>
                  <input type="Number" style="width: 90%;" autocomplete="off" id="km" name="km" autocomplete="off" class="form-control" required  placeholder="Truck K.M" />
                  <br><br>
                 <center>
                  <input type="submit" name="submit" class="btn btn-primary" >
                  </center>
           </form>
         </div>
           <div class="col-md-7">
                     <div class="box-body">
                                    <div class="table-responsive">  
                                      <table id="employee_data1"  table border="1"; style="font-family:verdana; font-size:  13px; height: auto; width: 100%; outline:none;border:none;" class="table table-striped table-bordered">  
                                        <thead>  
                                             <tr>  
                                                  <td>Truck Number</td>  
                                                  <td>Truck Driver</td> 
                                                    <td>Truck Enter Date</td>  
                                                     <td>Status</td> 
                                                     <td>Jobcard Date</td> 
                                             </tr>  
                                        </thead>  

                                        <?php  
                                        $sql = "SELECT * from truck_driver where username='$username'";
                                        $result = $conn->query($sql); 
                                      
                                        while($row = mysqli_fetch_array($result))  
                                        {  
                                          $truck_no = $row["truck_no"];
                                          $truck_driver = $row["truck_driver"];
                                          $date1 = $row["date1"]; 
                                          $status = $row["status"];
                                           $job_card_date = $row["job_card_date"];
                                         
                                          ?>
                                             
                <tr>
                       <td><?php echo $truck_no?>
                        <input type="hidden" readonly="readonly" name="truck_no" value="<?php echo $truck_no; ?>" >
                      </td>
                      <td><?php echo $truck_driver?>
                        <input type="hidden" readonly="readonly" name="km" value="<?php echo $truck_driver; ?>" >
                      </td>
                       

                      <td><?php echo $date1?>
                        <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                      </td> 
                      <?php
                       //echo $status;
                                         if($status > 0)
                                          { ?>
                                         <td>jobcard</td>  
                                
                                        <?php } else {?>
                                        <td>inspection</td> 
                                        <?php } ?>
                          <td><?php echo $job_card_date ?>
                            <input type="hidden" readonly="readonly" name="date1" value="<?php echo $job_card_date  ; ?>" >
                         </td>            
                                       </tr>
                                       
                                        <?php } ?>
                                          
                                      </table>  
                                    </div>  
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                          

                  </section>     
                     
  <div class="control-sidebar-bg"></div>
</div>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data1').DataTable();  
 });  
 </script> 




