
  <?php
require("connect.php");
include('header.php');

?>
<!DOCTYPE html>
<html>
<head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

           <script>  
                $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'yy-mm-dd'   
                     });  
                     $(function(){  
                          $("#theft_date").datepicker();  
                          
                     });  
                   
                });  
           </script>

            <script type="text/javascript">  
                 $(function()
                { 
                  $("#productno").autocomplete({
                  source: 'autocomplete_product.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val("");
                  $('#rate').val(""); 
                  $(event.target).focus();
                  alert('Product Number does not exists');
                    $('#rate').val('');
                     $('#avl_qty').val('');
                  } 
                },
                focus: function (event, ui) { return false; } }); });
               </script>

               <div id="rate_master222"></div>

                  <script type="text/javascript">
                        
                          function get_data1(val) {
                                $.ajax({
                                  type: "POST",
                                  url: "stock_transfer_detail.php",
                                  data:'productno='+val,
                                  success: function(data){
                                      $("#rate_master222").html(data);
                                  }
                                  });
                              }
                    </script>
                    <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var avl_qty = Number($('#avl_qty').val());
                 var trf_qty = $('#trf_qty').val();
                 if(avl_qty=="")
                 {
                   alert("Avalaible Quantity is Null");
                     $('#trf_qty').val('');
                 }else{
                 
                       if(trf_qty>avl_qty)
                       {
                         alert('You can not exceed Avl Qty  Quantity  is '+ avl_qty);
                         $('#trf_qty').val('');
                          $('#amount').val('');
                       }else{
                        var amt = trf_qty*myrate;
                         $('#amount').val(amt);

                       }
                     }

                }
               </script>

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_jobcard.php'); 
?> 

  <div class="content-wrapper">
    

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Theft Stock</h3>
       
      </div>
        <!-- /.box-header -->
     


        <div class="box-body">
         <!--  <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">    

                       <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-3">  
                             <input type="text" name="from_stock" id="from_stock" class="form-control" placeholder="From Stock" />  
                        </div>  
                        <div class="col-md-3">
                             <input type="text" name="to_stock" id="to_stock" class="form-control" placeholder="To Stock" />  
                        </div>  
                      </div> 
                      </div> 

            </div>  
          </div>
             
        </div> -->
            <!-- /.box-body -->
             <form method="post" action="temp_theft_stock.php" autocomplete="off">
                  <?php 
                      $office =  $_SESSION['logged_in_jobcard_id'];
                      $password =  $_SESSION['password']; 
                  ?>
              <div class="row">
                             <div class="col-md-6">
                                <div class="col-md-8">
                                  <label>Office:</label>
                                     <select class="form-control" required id="from_office" name="from_office">
                                      <option value="">---Select---</option>
                                      <option value="<?php echo $office; ?>"><?php echo $office; ?></option>
                                      </select>
                                </div>
                            </div>   
                           <div class="col-md-6">
                                <div class="col-md-8">
                                 <label>Theft Date:</label>
                                   <input type="text" name="theft_date" id="theft_date" class="form-control" placeholder="Theft Date" /> 
                                    <input type="hidden" name="password" id="password" class="form-control" value="<?php echo $password ?>" />  
                              </div> 
                          </div>
                </div>
                
              <br>
              <div class="row-4">
                 <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px; background-color: #EEEDEC ">
                  <tr>
                 <td>
                     <label>Product No</label>
                    <input type="text" name="productno" id="productno"  onchange="get_data1(this.value);"  style="width: 280px;" class="form-control"  required="required">

                    <input type="hidden" name="productname" id="productname"   style="width: 280px;" class="form-control"  required="required">
                 </td>
                
                 <td>
                     <label>product type</label>
                    <input type="text" name="producttype" readonly="readonly" style="width: 220px;" id="producttype"  class="form-control">
                  </td>

                  <td>
                     <label>Company</label>
                    <input type="text" name="company" readonly="readonly" style="width: 150px;" id="company"  class="form-control">
                  </td>

                   <td>
                     <label>Product Group</label>
                    <input type="text" name="product_group" readonly="readonly" style="width: 180px;" id="product_group"  class="form-control">
                  </td>
                  
                   <td >
                    
                    <label >Location</label>
                    <input type="text" name="pro_loc" readonly="readonly" style="width: 90px;" id="pro_loc"  class="form-control">
                    
                  </td>
                 
                </tr>
                <tr>
                   <td>
                    <label>Product Sub Group</label>
                    <input type="text" name="sub" readonly="readonly" style="width: 200px;" id="sub"  class="form-control">
                  </td>

                   <td>
                     <label>Rate/Unit</label>
                    <input type="number" id="rate" name="rate" readonly="readonly" required="required" style="width: 150px;"  class="form-control">
                   </td>


                  <td>
                     <label>Avl/Qty</label>
                    <input type="text" name="avl_qty" readonly="readonly" style="width: 150px;" id="avl_qty" class="form-control">
                    
                  </td>

                  

                  <td>
                     <label>Theft/Qty</label>
                    <input type="number" id="trf_qty" name="theft_qty" oninput="ChkForRateMaxValue();" required="required" style="width: 150px;"  class="form-control">
                   </td>

                  <td>
                     <label>Theft Amount</label>
                    <input type="text" name="amount" style="width: 150px;" autocomplete="off" id="amount" class="form-control">
                  
                  </td>
                  </tr>
                  <tr>
                    <td colspan='4'></td>
                    <td><br> <input type="submit" name="submit" style="width: 80px;"  class="btn btn-success ml-5" ></td>

                  </tr>
                  
                </table>
                </div>
               </form>


               <div style="overflow-x:auto;">
<table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
        <tbody>
           <tr>
      <th style="display: none;">Id</th>
      <th>Office</th>
       <th>Theft Date</th>
      <th>Product No</th>
      <th>Product Name</th>
      <th>Company</th>
      <th>Avl Qty</th>
      <th>Theft Qty</th>
      <th>Total Amount</th>
      <th>Remove</th>
    
    </tr>
    </tbody>  
              <?php
              // $stock_from =  $_SESSION['logged_in_jobcard_id'];

             $query = "SELECT * FROM temp_theft_stock WHERE from_office='$office' and password='$password' ";
          
              $result = mysqli_query($conn, $query);
           
              while($row = mysqli_fetch_array($result)){
             
               $id = $row['id'];
               $from_office=$row['from_office'];
                $productname=$row['productname'];

               $theft_date = $row['theft_date'];
             
               $productno = $row['productno'];
               $company = $row['company'];
               $avl_qty = $row['avl_qty'];
               $theft_qty = $row['theft_qty'];
               $amount = $row['amount'];
               
                  
          ?>
              <tr>
                <td style="display: none;"><?php echo $id?>
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                </td>
                <td style="display: none;">

                 <input  type="text" readonly="readonly" name="from_stock"  value="<?php echo $from_stock; ?>" id="from_stock" >
               </td>
                <td ><?php echo $from_office?>
                  <input  type="hidden" readonly="readonly" name="from_office[]" value="<?php echo $from_office; ?>">
                </td>
                <td ><?php echo $theft_date?>
                  <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $theft_date; ?>">
                </td>
                <td ><?php echo $productno?>
                  <input  type="hidden" readonly="readonly" name="state[]" value="<?php echo $state; ?>">
                </td>
                <td ><?php echo $productname?>
                  <input  type="hidden" readonly="readonly" name="state[]" value="<?php echo $state; ?>">
                </td>

                 <td ><?php echo $company?>
                  <input  type="hidden" readonly="readonly" name="break_point[]" value="<?php echo $break_point; ?>" >
                </td>
                
                <td ><?php echo $avl_qty?>
                  <input  type="hidden" readonly="readonly" name="job_card_no[]" value="<?php echo $job_card_no; ?>">
                </td>
                <td ><?php echo $theft_qty?>
                  <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $theft_qty; ?>">
                </td>
                <td><?php echo $amount?>
                 <input type="hidden" readonly="readonly"  name="yes_no[]" value="<?php echo $yes_no; ?>" >
               </td>

                 <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="X" class="btn btn-danger" />
                </td>
                <!-- <td>
                    <input type="button" onclick="DeleteModal('<?php /*echo*/ $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                </td> --> <script>
                     
                      function DeleteModal(id)
                      {
                       
                        var id = id;
                          var from_office = '<?php echo $from_office; ?>'
                            
                        if (confirm("Do you want to delete this data To Add Scrap..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_scrap_temp_data.php",

                                  data: 'id='+id + '&from_office='+from_office,
                                  success: function(data){
                                  
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div> 
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="insert_final_theft_stock.php" id="main_form">

               <input type="hidden" name="user_name" id="user_name" class="form-control" fo value="<?php echo $office ?>" />  

                <input type="hidden" name="password" id="password" class="form-control" value="<?php echo $password ?>" />  

             <center>
              <button type="submit" value="Submit" style="width: 120px;"  class="btn btn-primary ml-5" >Add To Theft</button>
             </center>

           </form>
              
          </section>
          <div class="col-sm-offset-2 col-sm-8">
     
       
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

