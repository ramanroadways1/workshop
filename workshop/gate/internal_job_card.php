
  <?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php  include('aside_main.php'); ?>
  
 
  <div class="content-wrapper">
    

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Internal jobcard Record Detail</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                    
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                              <td >View</td>
                              <td >Truck No</td>
                              <td>Truck Driver</td>
                              <td>Inspection Number</td>
                              <td>Inspection Date</td>
                              <td>Jobcard Date</td>
                              <td>Employee Name</td>
                              <td>Delay(1 month)</td>
                       </tr>  
                  </thead>  
                   <?php
                     $username =  $_SESSION['username'];
                      $show = "SELECT inspection_record.*,emp_data.empname from inspection_record  LEFT JOIN emp_data ON inspection_record.employee=emp_data.empcode where inspection_record.username='$username' group by inspection_no,start_time";
                        $result = $conn->query($show);
                       
                    if ($result->num_rows > 0) {
                    
                      while($row = $result->fetch_assoc()) {
                       
                            $sno = $row["prob_id"];
                            $truck_no1 = $row["truck_no1"];
                            $truck_driver = $row["truck_driver"];
                            $inspection_no = $row["inspection_no"];
                            $job_card_no = $row["job_card_no"];
                            $job_card_date = $row["job_card_date"];
                            $date1 = $row["date1"];
                            $empname = $row["empname"];
                        
                      ?>
                      <tr>                  
                       <td> 
                    <form method="POST" action="fetch_job_card_detail.php" target="_blank">
                      <input type="submit" class="form-control btn btn-primary btn-sm" style="width: 100px;height: 30px;" name="truck_no" value="<?php echo $truck_no1; ?>"  >
                    </form>
                  </td>
                  <td><?php echo $truck_no1?>
                    
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" name="truck_driver" value='<?php echo $truck_driver; ?>'>
                  </td>

                  <td><?php echo $inspection_no?>
                    <input type="hidden" name="inspection_no" value='<?php echo $inspection_no; ?>'>
                  </td>
                   

                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td><?php echo $job_card_date?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $job_card_date; ?>" >
                  </td>

                  <td><?php echo $empname?>
                    <input type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" >
                  </td>

                  <?php $today_date = date('yy-m-d');
                    $diff = strtotime($job_card_date) - strtotime($today_date);
                     $final_diff = abs(round($diff / 86400));  
                     if ($final_diff > '30') {
                      ?>
                      <td><?php echo "<font style='color:red;'> Close Jobcard immediately<font>"; ?> </td>
                      <?php
                     }else{
                      ?>
                        <td><?php echo "Pending"; ?> </td>
                      <?php
                     }
                   ?>
                  
                 
                </tr>
           
              <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
       
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

