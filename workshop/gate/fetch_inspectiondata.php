<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-family:verdana; font-size:  13px; ">

<div class="wrapper">

<?php include('aside_main.php'); ?>

<div class="content-wrapper">
   
   
    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Inspection Report</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $username =  $_SESSION['username'];
          $valueToSearch=$_POST['truck_no'];
      
          $sql = "SELECT * FROM truck_driver WHERE truck_no='$valueToSearch' and username='$username'";
              $result = $conn->query($sql);
                  
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
             
                  $truck_driver = $row['truck_driver'];
                  $truck_no = $row['truck_no'];
                  $job_card_no =  rand();
                  $date1 = $row['date1'];
                  $inspection_no = $row['insp_no'];

              $sql1 = "SELECT * FROM start_time WHERE truck_no='$valueToSearch' and username='$username'";
              $result1 = $conn->query($sql1);
              $date = date_default_timezone_set('Asia/Kolkata');
              $start_time = date("g:i a,Y-m-d");
           
             if(mysqli_num_rows($result1) > 0)
               {
               while($row = mysqli_fetch_array($result1)){
             
                   $start_time1 = $row['start_time'];
                  

                 }
               }else{
                   $sql2 = "INSERT into start_time(truck_no,start_time,username) VALUES ('$truck_no','$start_time','$username')";
              $result2 = $conn->query($sql2);
              echo "
                  <SCRIPT>
                         alert('Inspection time start from now...');
                          location.reload();
                    </SCRIPT>";

                 } 
               ?>
           <div class="row">

            <input type="hidden" name="username" id="username" value="<?php echo $username ?>">

              <div class="col-md-4">
                <label>Truck Number</label>
                <input type="text" autocomplete="off" id="truck_no1" name="truck_no1"  class="form-control" required readonly="readonly" value="<?php echo $truck_no;?>" placeholder="Truck Number" />
              </div>
              <div class="col-md-4">
                <label>Truck Driver</label>
                <input type="text" autocomplete="off" id="truck_driver" name="truck_driver" value="<?php echo $truck_driver;?>" class="form-control" required readonly="readonly" placeholder="truck driver" />
              </div>
              <div class="col-md-4">
                <label>Inspection Number</label>
                <input type="text" autocomplete="off" id="inspection_no" name="inspection_no"  class="form-control"  value="<?php echo $inspection_no;?>" placeholder="Inspection No." readonly/>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Job Card No</label>
                <input type="text" autocomplete="off" id="job_card_no" value="<?php echo $job_card_no;?>" placeholder="Job card no." name="job_card_no" class="form-control" required readonly/>
              </div> 
              <div class="col-md-4">
                <label>Inspection Date(Truck Enter in inspection)</label>
                <input type="text" autocomplete="off" id="date1" name="date1" placeholder="Date"  class="form-control" value="<?php echo $date1;?>" required readonly/>
              </div> 
              <div class="col-md-4">
                <label>Inspection Start Time</label>
                <input type="text" autocomplete="off" id="start_time" placeholder="Start Time" name="start_time" value="<?php echo $start_time1;?>" class="form-control" required readonly/>
              </div>
            </div>

            <?php
            }
          }
          else{
            
            echo "<SCRIPT>
               window.location.href='inspection_report.php';
          </SCRIPT>";
          exit();

          }
            ?><br>
             <button type="button"  class="btn btn-info btn-sm" style='margin-left:85%;' data-toggle="modal" data-target="#myModal">New Complaint</button>
        <br>
        <br> <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px; height: auto; width: 100%; outline:none;border:none;">  
                  <thead>  
                       
                               <tr class="table-active">
                                  <th scope="row">SNo</th>
                                   <th class="col-md-4">Department</th>
                                  <th  class="col-md-2" style="display: none;">Mistry</th>
                                  <th class="col-md-4">Complaint</th>
                                  <th class="col-md-2">Status</th>
                                  <th style="display: none;" class="col-md-2">else</th>
                                  <th>Work Action</th>
                                  <th style="display: none;">Work Action</th>
                                </tr>

                         
                  </thead> 
                   <script type="text/javascript">
                        $(document).ready(function(){
                            $('input[type="checkbox"]').click(function(){
                          
                          var val1 = $(this).attr("value");
                          var val2 = $(this).attr("data1");
                          
                          $('#WrkAct1'+val1).val(val2);
                          
                          if($(this).prop("checked") == true){
                            $('#WrkAct1'+val1).attr('readonly',false);
                            
                                }
                                else if($(this).prop("checked") == false){
                            $('#WrkAct1'+val1).attr('readonly',true);
                                }
                            });
                        });
                    </script> 
                   <?php
                     
                      $query = "SELECT * from problem_record";
                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                    <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $problem = $row['problem'];
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $val?>            
                </td>
                <td style="display: none;">
                   </td>
                 <td><?php echo $department?>
                  <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
                </td>
                <td style="display: none;"><?php echo $mistry?>
                  <input  type="hidden" readonly="readonly" name="mistry[]" id="mistry<?php echo $id; ?>" value="<?php echo $mistry; ?>">
                </td>
                <td><?php echo $problem?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>

                 <?php
           $sql21 = "SELECT * FROM tmp_insp_data  WHERE  insp_no='".$inspection_no."' and prob_id = '".$id."' and username='".$username."' order by prob_id desc limit 1";

           $result21 = $conn->query($sql21);
           $row21 = mysqli_fetch_array($result21);
           ?>
           <?php if(mysqli_num_rows($result21) > 0){ ?>
               <td>
                  Not OK <input type="checkbox" onclick="delete_func('<?php echo $id; ?>');" name="id_customer[]" 
          value='<?php echo $id; ?>' data1="<?php echo $problem; ?>" checked>
                </td>
            <?php }  
                  else {
                  ?>
              <td>
                  Not OK <input type="checkbox" 
          name="id_customer[]" data1="<?php echo $problem; ?>" onclick="MyFunc('<?php echo $id; ?>');" value='<?php echo $id; ?>' >
                </td>
            <?php } ?>
            
                   <td>
                  <input class="form-control" id="WrkAct1<?php echo $id; ?>" onchange="myfunction('<?php echo $id; ?>');"  type="text" style="width: 500px;"  autocomplete="off" value="<?php echo $row21['work_action'] ?? null; ?>" name= "work_action[]" readonly/>
                </td>
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }
                  ?>  
                </table> 

                <script>
                    function delete_func(elem)
                    {
                      var prob_id = elem;
                      var username = '<?php echo $_SESSION['username']; ?>';
                       var truck_no = '<?php echo $truck_no; ?>';
                       var insp_no = '<?php echo  $inspection_no ?>';
                        if(prob_id!='')
                        {
                           $.ajax({
                                  type: "POST",
                                  url: "delete_tmp_values.php",
                                 data:'prob_id='+prob_id + '&username='+username+ '&insp_no='+insp_no+ '&truck_no='+truck_no,
                                  success: function(data){
                                   $("#result22").html(data);
                                }
                            });
                         }
                      }     
              </script>          
                 <div id="result22"></div>
                <script>
                    function MyFunc(elem)
                    {
                      document.getElementById("WrkAct1"+elem).readOnly = false;
                      var prob_id = elem;
                       var department = $('#department'+elem).val();
                       var mistry = $('#mistry'+elem).val();
                       var problem = $('#problem'+elem).val();
                       var work_action = $('#problem'+elem).val();
                       $('#WrkAct1'+elem).val(problem);
                      var insp_no = '<?php echo  $inspection_no ?>';
                      var username = '<?php echo $_SESSION['username']; ?>'
                     if(work_action!='')
                      {
                        $.ajax({
                                  type: "POST",
                                  url: "insert_tmp_values1.php",
                                 data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&username='+username + '&work_action='+work_action,
                                  success: function(data){
                                   $("#result22").html(data);
                                }
                            });
                         }
                      }     
              </script>          
                 <div id="result22"></div>
            <br> 
                  <script>
                  function myprint() {
                    window.print();
                  }
                  </script> 
                  <div class="col-md-12">
              <center>
                <input type="submit" align="center" name="submit" class="btn btn-primary" >
        
              </center>     
            </div><br>
          </form>
        <br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form  id="AddCompany" method="POST" action="insert_inspection_problem.php">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Complaint</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <input type="hidden" name="truck_no" value="<?php echo $valueToSearch; ?>">
            </div>
            <div class="col-md-10">
              </div>
            <div class="col-md-2">
              <b>Complaint<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="problem" name="problem" placeholder="problem" required="required" />
            </div><br><br><br>
            <div class="col-md-2">
                <b>Department<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
                            <select name="department" id="department" style="width: 100%;" id="mistry" class="form-control" required="required" class="demoInputBox" onChange="getmistry(this.value);">
                                    <option value="">Select department</option>
                                    <?php
                                    $sql1="SELECT * FROM labour group by department";
                                         $results=$conn->query($sql1); 
                                    while($rs=$results->fetch_assoc()) { 
                                    ?>
                                    <option value="<?php echo $rs["department"]; ?>"><?php echo $rs["department"]; ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                  </div><br><br><br>

                                 <script>
                                      function getmistry(val) {
                                          $.ajax({
                                          type: "POST",
                                          url: "dependent_labour.php",
                                          data:'department='+val,
                                          success: function(data){
                                            $("#mistry").html(data);
                                          }
                                          });
                                        }
                                        </script>
                                 <div class="col-md-2">
                                    <b>Mistry<font color="red">*</font></b>
                                 </div>
                        <div class="col-md-10">
                      <select name="mistry" style="width: 100%;" id="mistry" class="form-control" required="required"> 
                          <option value="">Select department first</option>
                      </select>
                </div><br><br><br>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
    </div>
  </div>