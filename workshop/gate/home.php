
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RRpl</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php
  include('header.php');
 include('connect.php');
 error_reporting(0);
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">  
  
  <?php
include('aside_main.php');
  ?>
<!--     <?php
          $demo=$_SESSION['admin'];
          $demo1=$_SESSION['employee'];
          print_r($demo);
          print_r($demo1)
    
      ?>  -->
  <div class="content-wrapper">
    <section class="content-header">
      
      <h1>
     Dashboard(Workshop)
     </h1>
    </section>
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Dashboard</h3>

          
        </div>
         <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="icon ion-android-bus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Trucks</span>
              <span class="info-box-number">
              <?php
              // include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT tno FROM own_truck";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Inspection Record</span>
              <span class="info-box-number">
            <?php
            
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT inspection_no FROM inspection_record group by inspection_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="icon ion-android-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Internal Jobcard</span>
              <span class="info-box-number">
                <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT job_card_no FROM job_card_record group by job_card_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              // mysqli_close($conn);
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="icon ion-ios-skipforward"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">External Jobcard</span>
              <span class="info-box-number">
                  <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT job_card_no FROM external_job_cards_main group by job_card_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
       
        </div>

    </section>
          </div>
  

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

