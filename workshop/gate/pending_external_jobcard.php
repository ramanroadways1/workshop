    

<?php
require("connect.php");
include('header.php');

?>
   
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_main.php'); ?> 
  
  <div class="content-wrapper">
   
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 style="font-family:verdana; font-size:15px;" class="box-title">External Jobcard Detail Data</h3>
        
      </div>
        <!-- /.box-header -->
         <script>
        function myFunction() {
          window.print();
        }
       </script>
          
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                  <div id="order_table"> 
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                            <td>id</td>
                            <td>Truck Number</td>
                            <td>Truck Driver</td>
                            <td>Employee Name</td>
                            <td>Jobcard Date</td>
                            <td>JobCard Number</td>
                           
                       </tr>  
                  </thead>  
                  
              <?php
              $username = $_SESSION['username'];
              $show = "SELECT external_job_cards.*,emp_data.empname from external_job_cards LEFT JOIN emp_data ON external_job_cards.employee=emp_data.empcode where external_job_cards.status='0' and external_job_cards.username='$username' group by job_card_no";

              $result = $conn->query($show);
              if ($result->num_rows > 0) {
                  // output data of each row
                $i=1;
                $id_customer=1;
                while($row = $result->fetch_assoc()) {
                  $id = $row["id"];
                  $truck_no2 = $row["truck_no2"];
                   $job_card_no = $row["job_card_no"];
                   $truck_driver = $row["truck_driver"];
                  $date1 = $row["date1"];
                  $empname = $row["empname"];
                  
                ?>
                <tr> 
                
                  <td><?php echo $id?>
                    <input type="hidden" name="sno" value='<?php echo $id; ?>'>
                  </td>
                 
                  <td><?php echo $truck_no2?>
                    <input type="hidden" name="truck_no2" value='<?php echo $truck_no2; ?>'>
                  </td>
                   

                  <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="job_card_no" value="<?php echo $truck_driver; ?>" >
                  </td>

                  <td><?php echo $empname?>
                    <input type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" >
                  </td>

                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td>
                    <form method="POST" action="external_job_card_detail.php">
                      <input type="submit"  name="job_card_no" value="<?php echo $job_card_no; ?>" class="btn btn-primary btn-sm" >
                    </form>
                  </td>
                 
                     
                </tr>
           
               <?php
                   $id_customer++;
                   $i++;
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
              </div>  
            </div>  
          </div>
    
        </div>
     
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>  
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter2.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

