    

<?php
require("connect.php");
include('header.php');

?>
    <?php
     $query = "SELECT * FROM  inspection_record ORDER BY  inspection_no desc";  
     $result = mysqli_query($conn, $query);  
    ?>   
<!DOCTYPE html>
<html>
<head>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
 


    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  --> 
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include('aside_inspection.php'); ?>

  
  <div class="content-wrapper">
      <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Search data from Inspection Report</h3>
        
      </div>
       <script>
        function myFunction() {
          window.print();
        }
       </script>
      
        <!-- /.box-header -->

              <div class="row">
                <div class="col-md-12">
                   <input type="button"  style='margin-left:87%;'  color="Primary" class="btn btn-warning"onclick="myFunction()" value="Print Inspection">
                </div>
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div>                 
           
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <div id="order_table"> 
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                           <th width="5%">Truck number</th>  
                               <th width="20%">Truck driver</th>  
                               <th width="5%">inspection no</th>  
                               <th width="5%">jobcard no</th>  
                               <th width="10%">Department</th> 
                               <th width="5%">Mistry</th> 
                               <th width="20%">Work action</th> 
                               <th width="30%">Jobcard Work</th>  
                               <th width="8%">Date</th>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                           <td>'.$row["truck_no1"].'</td>  
                            <td>'.$row["truck_driver"].'</td>  
                            <td>'.$row["inspection_no"].'</td>
                             <td>'.$row["job_card_no"].'</td>  
                            <td>'.$row["department"].'</td>
                            <td>'.$row["mistry"].'</td>  
                             <td>'.$row["work_action"].'</td>
                              <td>'.$row["job_card_work"].'</td>  
                                <td>'.$row["date1"].'</td>   
                             
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table> 
                </div> 
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  

 </script> 

 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'Y-m-d'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>


