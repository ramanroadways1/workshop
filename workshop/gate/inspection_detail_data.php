<?php
require("connect.php");
include('header.php');
include('aside_main.php');
?>
<!DOCTYPE html>
<html>
<head>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>
 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-family:verdana; font-size:  13px;">
<div class="wrapper">
   
  
  <div class="content-wrapper">
    <section class="content-header">
    
    </section>
   

    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Inspection Submited Report</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $valueToSearch=$_POST['inspection_no'];
           $username =  $_POST['username'];
             $sql = "SELECT * FROM inspection_record WHERE  inspection_no='$valueToSearch' and username='$username'";
              $result = $conn->query($sql);
                  
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
             
                  $truck_driver = $row['truck_driver'];
                  $truck_no = $row['truck_no1'];
                  $job_card_no = $row['job_card_no'];
                  $date1 = $row['date1'];
                   $start_time = $row['start_time'];
                  $inspection_no = $row['inspection_no'];

             }

               ?>
           <div class="row">
              <div class="col-md-4">
                <label>Truck Number</label>
                <input type="text" autocomplete="off" id="truck_no1" name="truck_no1"  class="form-control" required readonly="readonly" value="<?php echo $truck_no;?>" placeholder="Truck Number" />
              </div>
              <div class="col-md-4">
                <label>Truck Driver</label>
                <input type="text" autocomplete="off" id="truck_driver" name="truck_driver" value="<?php echo $truck_driver;?>" class="form-control" required readonly="readonly" placeholder="truck driver" />
              </div>
              <div class="col-md-4">
                <label>Inspection Number</label>
                <input type="text" autocomplete="off" id="inspection_no" name="inspection_no"  class="form-control"  value="<?php echo $inspection_no;?>" placeholder="Inspection No." readonly/>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Job Card No</label>
                <input type="text" autocomplete="off" id="job_card_no" value="<?php echo $job_card_no;?>" placeholder="Job card no." name="job_card_no" class="form-control" required readonly/>
              </div> 
              <div class="col-md-4">
                <label>Date</label>
                <input type="text" autocomplete="off" id="date1" name="date1" placeholder="Date"  class="form-control" value="<?php echo $date1;?>" required readonly/>
              </div> 
              <div class="col-md-4">
                <label>Start Time</label>
                <input type="text" autocomplete="off" id="start_time" placeholder="Start Time" name="start_time" value="<?php echo $start_time;?>" class="form-control" required readonly/>
              </div>
            </div>

            <?php
            
          }
           else{
           /* echo "<SCRIPT>
                        window.location.href='fount_data.php';
           </SCRIPT>";
            exit();*/
          }
            ?><br>
             <button type="button"  class="btn btn-info btn-sm" style='margin-left:89%;' data-toggle="modal" data-target="#myModal">New Complaint</button>
        <br>
        <br> <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                              <tr class="table-active">
                                  <th scope="row">SNo</th>
                                   <th class="col-md-2">Department</th>
                                  <th  class="col-md-1" style="display: none;">Mistry</th>
                                  <th class="col-md-5">Complaint</th>
                                  <th class="col-md-5">Work Action</th>
                                  <th class="col-md-2">Remove</th>
                                </tr>

                  </thead> 
                   <script type="text/javascript">
                    $(document).ready(function(){
                        $('input[type="checkbox"]').click(function(){
                      
                      var val1 = $(this).attr("value");
                      var val2 = $(this).attr("data1");
                      
                      $('#WrkAct1'+val1).val(val2);
                      
                      if($(this).prop("checked") == true){
                        $('#WrkAct1'+val1).attr('readonly',false);
                        
                            }
                            else if($(this).prop("checked") == false){
                        $('#WrkAct1'+val1).attr('readonly',true);
                            }
                        });
                    });
                </script> 
                                   <?php  
                      $query = "SELECT * FROM inspection_record WHERE  inspection_no='$valueToSearch' and username='$username'";
                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $sno=$row['prob_id'];
                    $problem = $row['problem'];
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                     $work_action = $row['work_action'];
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $val?>            
                </td>
                <td style="display: none;"><?php echo $sno?></td>

                 <td><?php echo $department?>
                </td>
                <td style="display: none;"><?php echo $mistry?>
                 
                </td>
                <td><?php echo $problem?>
                  
                </td>

                <td><?php echo $work_action?>
                 
                </td>

               <td>
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="X" class="btn btn-danger" />
                </td>
            <script>
                      function DeleteModal(id)
                      {
                        //alert(id);
                        var id = id;
                        var inspection_no = $("#inspection_no").val();
                         var username = '<?php echo $username ?>' 
                        
                        if (confirm("Do you want to delete this Problem..?"))
                        {
                          if(id!='')
                          {
                             $.ajax({
                                  type: "POST",
                                  url: "delete_problem.php",
                                  data: 'id='+id + '&inspection_no='+inspection_no + '&username='+username,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div>
           
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }
                  ?>  
                </table> 

                <script>
      function MyFunc(elem)
      {
        document.getElementById("WrkAct1"+elem).readOnly = false;
        var prob_id = elem;

         var department = $('#department'+elem).val();
         var mistry = $('#mistry'+elem).val();
         var problem = $('#problem'+elem).val();
         var work_action = $('#problem'+elem).val();
         $('#WrkAct1'+elem).val(problem);
        var insp_no = '<?php echo  $inspection_no ?>';
        if(work_action!='')
        {
          $.ajax({
                    type: "POST",
                    url: "insert_tmp_values.php",
                   data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&work_action='+work_action,
                    success: function(data){

                        $("#result22").html(data);
                    }
                    });
        }
      }     
       </script>            <div id="result22"></div>

                  <script>
                function myfunction(i)
                {
                  var prob_id = i;
                  var work_action = $('#WrkAct1'+i).val();
                   var department = $('#department'+i).val();
                   var mistry = $('#mistry'+i).val();
                   var problem = $('#problem'+i).val();
                   var insp_no = '<?php echo $inspection_no ?>';
                  
                  if(work_action!='')
                  {
                    $.ajax({
                     type: "POST",
                              url: "insert_tmp_values.php",
                              data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&work_action='+work_action,
                              success: function(data){
                                  $("#result223").html(data);
                               }
                              });
                            }
                          }     
             </script>  
              <div id="result223"></div>
            <br> 
                  <script>
                  function myprint() {
                    window.print();
                  }
                  </script> 
                  <div class="col-md-12">
              <center>
                <!-- <input type="submit" align="center" name="submit" class="btn btn-primary" > -->
        
              </center>     
            </div><br>
          </form>
<br><br>
        </div>
      </div>
    </section>
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>
<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}
</style>
 <script type="text/javascript">     
                           $(function()
                        { 
                        $( "#problem" ).autocomplete({
                        source: 'autocomplete_problem.php',
                        change: function (event, ui) {
                        if(!ui.item){
                        $(event.target).val(""); 
                        $(event.target).focus();
                        alert('problem does not exists.');
                        } 
                        },
                        focus: function (event, ui) {return false; } }); });



                         function get_data1(val) {
                           var inspection_no = $('#inspection_no12').val();
                              var username = '<?php echo $username ?>' 
                            $.ajax({
                           
                              type: "POST",
                              url: "fetch_problem_detail.php",
                              data:'problem='+val + '&inspection_no='+inspection_no+ '&username='+username,
                              success: function(data){
                               
                                  $("#rate_master222").html(data);
                              }
                              });
                          }
                    </script>

  <!--  <div id="result"></div> -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form  id="AddCompany" method="POST" action="insert_inspection_problem_by_inspectionno.php">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Complaint</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <input type="hidden" id="inspection_no12" name="inspection_no" value="<?php echo $valueToSearch; ?>">
                <input type="hidden"  name="username" value="<?php echo $username; ?>">
                <input type="hidden"  name="truck_no1" value="<?php echo $truck_no; ?>">
                <input type="hidden" name="truck_driver" value="<?php echo $truck_driver; ?>">
                <input type="hidden" name="job_card_no" value="<?php echo $job_card_no; ?>">
                <input type="hidden" name="date1" value="<?php echo $date1; ?>">
               
                <input type="hidden" name="start_time" value="<?php echo $start_time; ?>">
      
            </div>
            <div class="col-md-10">
              <input type="hidden" style="width: 100%;"  value="<?php echo $sno; ?>" autocomplete="off" class="form-control" id="sno" name="sno" placeholder="sno"/>
            </div>
            <div class="col-md-2">
                <b>Complaint<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;"  class="form-control" id="problem" name="problem" placeholder="Problem" onchange="get_data1(this.value);" required />
            </div><br><br><br>

                    <div id="rate_master222"></div>

             <div class="col-md-2">
                <b>Work Action<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;"  class="form-control" id="work_action" name="work_action" placeholder="Work ACtion" autocomplete="off" required="required" />
            </div><br><br><br>
             <div class="col-md-2">
                <b>Department<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
               <input type="text" style="width: 100%;"  class="form-control" id="department" name="department" placeholder="Department" readonly="readOnly" />
            </div><br><br><br>

            <div class="col-md-2">
                <b>Mistry<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
                <input type="text" style="width: 100%;"  class="form-control" id="mistry" name="mistry" placeholder="Mistry"  readonly="readOnly" />
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>


      <!-- <script type="text/javascript">
        $(document).ready(function (e) {
        $("#AddCompany").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_inspection_problem_by_inspectionno.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script> -->


    </div>
  </div>


   