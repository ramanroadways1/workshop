
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>MNGT</span>
      <span class="logo-lg"><b>Workshop</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a><br>  

      <?php  if (isset($_SESSION['username'])&&($_SESSION['username'])) : ?>
      <p style="float:right; margin-right:20px;">Welcome <strong>
       <!--  <?php echo $_SESSION['username']; ?>   -->
        <?php echo $_SESSION['employee']; ?></strong>
         <?php echo $_SESSION['username']; ?></strong>
      <a href="logout.php?logout='1'" style="color: red;">logout</a> </p>
      <?php endif ?>
    </nav>
</header>
    
<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
            <li>
              <a href="office_jobcard_record.php">
                <i class="fa fa-check"></i> <span>Office Jobcard</span>
              </a>
            </li> 
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Job Card</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="internal_job_card.php"><i class="fa fa-circle-o"></i>Internal Job Card</a></li>

            <li><a href="external_job_cards.php"><i class="fa fa-circle-o"></i>External Job Card</a></li>
             
             <li><a href="pending_external_jobcard.php"><i class="fa fa-circle-o"></i>Pending External Jobcard</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-history"></i> <span>Jobcard Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="internal_job_card_view.php"><i class="fa fa-circle-o"></i>All Internal Job Card</a></li>
           <!--    
                Old File Name
            <li><a href="found_job.php"><i class="fa fa-circle-o"></i>All Internal Job Card</a></li> -->
            <li><a href="found_external_job_view.php"><i class="fa fa-circle-o"></i>All External Job Card</a></li>
           <!--  
                Old File Name
            <li><a href="found_external_job.php"><i class="fa fa-circle-o"></i>All External Job Card</a></li> -->
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-search"></i> <span>All Filters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="show_add_product_jobcard1.php"><i class="fa fa-circle-o"></i>Issue Product in internal job</a></li>

            <li><a href="show_external_issue_product1.php"><i class="fa fa-circle-o"></i>Issue Product in external job</a></li>

            <!--  <li><a href="total_product.php"><i class="fa fa-circle-o"></i>Stock Ledger</a></li>

              <li><a href="internal_job_card_ledger.php"><i class="fa fa-circle-o"></i>Internal Jobcard Ledger</a></li> -->
             
          </ul>
        </li><br>

          <!--  <li>
              <a href="stock_transfer.php">
                <i class="fa fa-share"></i> <span>Stock Transfer</span>
              </a>
            </li> 

            <li>
              <a href="avalaible_scrap_stock.php">
                <i class="fa fa-recycle"></i> <span>Scrap Stock</span>
              </a>
            </li> 

             <li>
              <a href="avalaible_theft_stock.php">
                <i class="fa fa-close"></i> <span>Thefts Stock</span>
              </a>
            </li> 
         -->

        <!--  <li class="treeview">
          <a href="#">
            <i class="fa fa-search"></i> <span>Direct Issue</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> 
          </a>
          <ul class="treeview-menu">
             <li><a href="direct_issue_internal.php"><i class="fa fa-circle-o"></i>Direct Issue In internal</a></li>
             
            <li><a href="direct_issue_external.php"><i class="fa fa-circle-o"></i>Direct Issue In External</a></li>
          </ul>
        </li> -->

       
      </ul>
      
    </section>
  </aside>
