
  <?php
require("connect.php");
include('header.php');

?>
<!DOCTYPE html>
<html>
<head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     

           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

           <script>  
                $(document).ready(function(){  
                     $.datepicker.setDefaults({  
                          dateFormat: 'yy-mm-dd'   
                     });  
                     $(function(){  
                          $("#transfer_out_date").datepicker();  
                          
                     });  
                   
                });  
           </script>

            <script type="text/javascript">  
                 $(function()
                { 
                  $("#productno").autocomplete({
                  source: 'autocomplete_product.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val("");
                  $('#rate').val(""); 
                  $(event.target).focus();
                  alert('Product Number does not exists');
                    $('#rate').val('');
                     $('#avl_qty').val('');
                  } 
                },
                focus: function (event, ui) { return false; } }); });
               </script>

               <div id="rate_master222"></div>

                  <script type="text/javascript">
                        
                          function get_data1(val) {
                                $.ajax({
                                  type: "POST",
                                  url: "stock_transfer_detail.php",
                                  data:'productno='+val,
                                  success: function(data){
                                      $("#rate_master222").html(data);
                                  }
                                  });
                              }
                    </script>
                    <script>
             function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var avl_qty = Number($('#avl_qty').val());
                 var trf_qty = $('#trf_qty').val();
                 if(avl_qty=="")
                 {
                   alert("Avalaible Quantity is Null");
                     $('#trf_qty').val('');
                 }else{
                 
                       if(trf_qty>avl_qty)
                       {
                         alert('You can not exceed Avl Qty  Quantity  is '+ avl_qty);
                         $('#trf_qty').val('');
                          $('#amount').val('');
                       }else{
                        var amt = trf_qty*myrate;
                         $('#amount').val(amt);

                       }
                     }

                }
               </script>

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_jobcard.php'); 
?> 

  <div class="content-wrapper">
    

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Stock Transfer</h3>
       
      </div>
        <!-- /.box-header -->
     


        <div class="box-body">
         <!--  <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">    

                       <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-3">  
                             <input type="text" name="from_stock" id="from_stock" class="form-control" placeholder="From Stock" />  
                        </div>  
                        <div class="col-md-3">
                             <input type="text" name="to_stock" id="to_stock" class="form-control" placeholder="To Stock" />  
                        </div>  
                      </div> 
                      </div> 

            </div>  
          </div>
             
        </div> -->
            <!-- /.box-body -->
             <form method="post" action="insert_stock_transfer.php" autocomplete="off">

              <div class="row">
                     <!--    <div class="col-md-12"> -->
                       <?php $stock_from =  $_SESSION['username']; ?>
                            <div class="col-md-3">  
                              <label>From Stock:</label>
                                 <select class="form-control" required id="from_stock" name="from_stock">
                                  <option value="">---Select---</option>
                                  <option value="<?php echo $stock_from; ?>"><?php echo $stock_from; ?></option>
                                  </select>
                            </div>  
                             
                              <div class="col-md-3">
                                 <label>To Stock:</label>
                                   <input type="text" name="to_stock" id="to_stock" class="form-control" placeholder="To Stock" />  
                                </div>  
                            <div class="col-md-3">
                               <label>Transfer Out Date:</label>
                                 <input type="text" name="transfer_out_date" id="transfer_out_date" class="form-control" placeholder="Transfer Out" />  
                            </div>  
                     <!--  </div>  -->
                </div>
                
              <br>
              <div class="row-4">
                 <table  id="complain_table"  class="table table-bordered" border="2"; style=" font-family:arial; font-size:  14px; background-color: #EEEDEC ">
                  <tr>
                 <td>
                     <label>Product No</label>
                    <input type="text" name="productno" id="productno"  onchange="get_data1(this.value);"  style="width: 280px;" class="form-control"  required="required">

                    <input type="hidden" name="productname" id="productname"   style="width: 280px;" class="form-control"  required="required">
                 </td>
                
                 <td>
                     <label>product type</label>
                    <input type="text" name="producttype" readonly="readonly" style="width: 220px;" id="producttype"  class="form-control">
                  </td>

                  <td>
                     <label>Company</label>
                    <input type="text" name="company" readonly="readonly" style="width: 150px;" id="company"  class="form-control">
                  </td>

                   <td>
                     <label>Product Group</label>
                    <input type="text" name="product_group" readonly="readonly" style="width: 200px;" id="product_group"  class="form-control">
                  </td>
                  
                   <td >
                    
                    <label >Location</label>
                    <input type="text" name="pro_loc" readonly="readonly" style="width: 90px;" id="pro_loc"  class="form-control">
                    
                  </td>
                 
                </tr>
                <tr>
                   <td>
                    <label>Product Sub Group</label>
                    <input type="text" name="sub" readonly="readonly" style="width: 200px;" id="sub"  class="form-control">
                  </td>

                   <td>
                     <label>Rate/Unit</label>
                    <input type="number" id="rate" name="rate" readonly="readonly" required="required" style="width: 150px;"  class="form-control">
                   </td>


                  <td>
                     <label>Avl/Qty</label>
                    <input type="text" name="avl_qty" readonly="readonly" style="width: 150px;" id="avl_qty" class="form-control">
                    
                  </td>

                  

                  <td>
                     <label>transfer/Qty</label>
                    <input type="number" id="trf_qty" name="trf_qty" oninput="ChkForRateMaxValue();" required="required" style="width: 150px;"  class="form-control">
                   </td>

                  <td>
                     
                     <label>Amount</label>
                    <input type="text" name="amount" style="width: 150px;" autocomplete="off" id="amount" class="form-control">
                  
                  </td>
                  </tr>
                  <tr>
                    <td colspan='4'></td>
                    <td><br> <input type="submit" name="submit" style="width: 80px;"  class="btn btn-success ml-5" ></td>

                  </tr>
                  
                </table>
                </div>
               </form>


               <div style="overflow-x:auto;">
<table id="employee_data1" class="table table-hover" style="font-family:arial; font-size:  14px;" border="2">
        <tbody>
           <tr>
      <th style="display: none;">Id</th>
      <th>To Stock</th>
       <th>Transfer Out Date</th>
      <th>Product No</th>
      <th>Product Name</th>
      <th>Company</th>
      <th>Avl Qty</th>
      <th>Transfer Qty</th>
      <th>Total Amount</th>
      <th>Remove</th>
    
    </tr>
    </tbody>  
              <?php
              // $stock_from =  $_SESSION['logged_in_jobcard_id'];
              
             $query = "SELECT * FROM temp_product WHERE from_stock='$stock_from' ";
          
              $result = mysqli_query($conn, $query);
           
              while($row = mysqli_fetch_array($result)){
             
               $id = $row['id'];
               $from_stock=$row['from_stock'];
                $productname=$row['productname'];

               $to_stock = $row['to_stock'];
               $transfer_out_date = $row['transfer_out_date'];
               $productno = $row['productno'];
               $company = $row['company'];
               $avl_qty = $row['avl_qty'];
               $trf_qty = $row['trf_qty'];
               $amount = $row['amount'];
               
                  
          ?>
              <tr>
                <td style="display: none;"><?php echo $id?>
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                </td>
                <td style="display: none;">

                 <input  type="text" readonly="readonly" name="from_stock"  value="<?php echo $from_stock; ?>" id="from_stock" >
               </td>
                <td ><?php echo $to_stock?>
                  <input  type="hidden" readonly="readonly" name="truck_no2[]" value="<?php echo $truck_no2; ?>">
                </td>
                <td ><?php echo $transfer_out_date?>
                  <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $truck_driver; ?>">
                </td>
                <td ><?php echo $productno?>
                  <input  type="hidden" readonly="readonly" name="state[]" value="<?php echo $state; ?>">
                </td>
                <td ><?php echo $productname?>
                  <input  type="hidden" readonly="readonly" name="state[]" value="<?php echo $state; ?>">
                </td>

                 <td ><?php echo $company?>
                  <input  type="hidden" readonly="readonly" name="break_point[]" value="<?php echo $break_point; ?>" >
                </td>
                
                <td ><?php echo $avl_qty?>
                  <input  type="hidden" readonly="readonly" name="job_card_no[]" value="<?php echo $job_card_no; ?>">
                </td>
                <td ><?php echo $trf_qty?>
                  <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>">
                </td>
                <td><?php echo $amount?>
                 <input type="hidden" readonly="readonly"  name="yes_no[]" value="<?php echo $yes_no; ?>" >
               </td>

                 <td> 
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="X" class="btn btn-danger" />
                </td>
                <!-- <td>
                    <input type="button" onclick="DeleteModal('<?php /*echo*/ $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                </td> --> <script>
                     
                      function DeleteModal(id)
                      {
                       
                        var id = id;
                          var from_stock = '<?php echo $from_stock; ?>'
                            
                        if (confirm("Do you want to delete this data To Transfer..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_store_temp_data.php",

                                  data: 'id='+id + '&from_stock='+from_stock,
                                  success: function(data){
                                  
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script> <div id="result22"></div>
              </tr>
            <?php } ?>
            </table></div>
            <form method="post" action="save.php" id="main_form">
               


             <center>
              <button type="submit"  form="main_form"  value="Submit" style="width: 80px;"  class="btn btn-primary ml-5" >Transfer</button>
             </center>

           </form>
              
          </section>
          <div class="col-sm-offset-2 col-sm-8">
     
       
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

