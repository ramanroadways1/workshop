<?php
require("connect.php");
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
<?php  include('aside_jobcard.php'); ?> 
<div class="content-wrapper">
   
   
    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Scraps Data</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $valueToSearch=$_POST['scrap_no'];
      
          $sql = "SELECT * FROM scrap_stock WHERE scrap_no='$valueToSearch' group by scrap_no";
              $result = $conn->query($sql);
                    
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
                   $scrap_no = $row['scrap_no'];
                  $from_office = $row['from_office'];
                  $scrap_out_date = $row['scrap_out_date'];
                  $productno = $row['productno'];
                  $company = $row['company'];
                  $productname = $row['productname'];
                  $producttype = $row['producttype'];
                   $product_group = $row['product_group'];
                   $sub = $row['sub'];
                  $productname = $row['productname'];
                  $producttype = $row['producttype'];
               ?>
           <div class="row">
            <div class="col-md-3">
                <label>Scrap Number</label>
                <input type="text" autocomplete="off" id="scrap_no" name="scrap_no"  class="form-control" required readonly="readonly" value="<?php echo $scrap_no;?>" placeholder="Office" />
              </div>
              <div class="col-md-3">
                <label>Scrap Data From Office</label>
                <input type="text" autocomplete="off" id="from_office" name="from_office"  class="form-control" required readonly="readonly" value="<?php echo $from_office;?>" placeholder="Office" />
              </div>
              <div class="col-md-3">
                <label>Scrap Out Date</label>
                <input type="text" autocomplete="off" id="scrap_out_date" name="scrap_out_date" value="<?php echo $scrap_out_date;?>" class="form-control" required readonly="readonly" placeholder="truck driver" />
              </div>
          </div>
             
            <?php
            }
          } else{
            echo "<SCRIPT>
                 window.location.href='avalaible_scrap_stock.php';
                </SCRIPT>";
             exit();

            
          }
         
            ?><br>

             <table  id="complain_table"  class="table table-bordered" border="4"; style=" font-family:arial; font-size:  14px;">
                  <thead>  
                           <tr class="table-active">
                              <th scope="row">Product Number</th>
                               <th scope="row">Product Name</th>
                               <th class="col-md-4">Company</th>
                               <th class="col-md-4">Product Type</th>
                              <th class="col-md-4">Product Group</th>
                              <th class="col-md-2">Sub Group</th>
                              <th class="col-md-4">Product Location</th>
                              <th class="col-md-4">Available Quantity</th>
                              <th class="col-md-2">Scrap Quantity</th>
                              <th class="col-md-2">Scrap Amount</th>
                              <th class="col-md-2">Back To Inventory</th>
                              <th class="col-md-2">Destroy</th>
                            </tr>
                  </thead> 
                   <?php  
                      $sql = "SELECT * FROM scrap_stock WHERE scrap_no='$valueToSearch'";
                          $result = $conn->query($sql);
                                
                      if(mysqli_num_rows($result) > 0)
                       {
                           while($row = mysqli_fetch_array($result)){
                               $scrap_no = $row['scrap_no'];
                              $from_office = $row['from_office'];
                              $scrap_out_date = $row['scrap_out_date'];
                              $productno = $row['productno'];
                              $company = $row['company'];
                              $productname = $row['productname'];
                              $producttype = $row['producttype'];
                               $product_group = $row['product_group'];
                               $sub = $row['sub'];
                                $pro_loc = $row['pro_loc'];
                              $avl_qty = $row['avl_qty'];
                              $scrap_qty = $row['scrap_qty'];
                               $amount = $row['amount'];
                           ?>
                  <tr>
                   
                <td ><?php echo $productno?>
                  <input  type="hidden" readonly="readonly" name="sno[]" id="sno<?php echo $id_customer; ?>" value="<?php echo $sno; ?>">
                </td>
                 <td><?php echo $productname?>
                  <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
                </td>
                <td><?php echo $company?>
                  <input  type="hidden" readonly="readonly" name="mistry[]" id="mistry<?php echo $id; ?>" value="<?php echo $mistry; ?>">
                </td>
                <td><?php echo $producttype?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>
                <td><?php echo $product_group?>
                  <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
                </td>
                <td><?php echo $sub?>
                  <input  type="hidden" readonly="readonly" name="mistry[]" id="mistry<?php echo $id; ?>" value="<?php echo $mistry; ?>">
                </td>
                <td><?php echo $pro_loc?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>
                  <td><?php echo $avl_qty?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>
                  <td><?php echo $scrap_qty?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>
                <td><?php echo $amount?>
                  <input type="hidden" readonly="readonly" name="amount[]" id="amount<?php echo $id; ?>" value="<?php echo $amount; ?>">
                </td>
                 <td>
                   <input type="button"  onclick="addmodel('<?php echo $id;?>')" name="sno" value="Add" class="btn btn-success" />
                  <!-- <button type="button" onclick="DeleteModal('<?php echo $id;?>')" va name="sno"  class="btn btn-success"><i class="fa fa-arrow-left"></i></button> -->
                </td>
                <td>
                  <input type="button"  onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="Destroy" class="btn btn-danger" />
                </td>

                  
                  </tr>
                  <?php  
                   }
                  }
                  ?>  
                </table> 

                  <div class="col-md-12">
             <!--  <center>
                <input type="submit" align="center" name="submit" class="btn btn-primary" >
        
              </center>  -->    
            </div><br>
          </form>
<br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>

 
