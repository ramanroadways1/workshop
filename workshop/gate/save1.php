    

<?php
require("connect.php");
include('header.php');

?>
 <?php  

 $sql = "SELECT * from add_product_jobcard";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_jobcard.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Issue Product in internal jobcard</h3>
        
      </div>


      <script>
        function myFunction() {
          window.print();
        }
       </script>
         <div class="row">
                <div class="col-md-12">
                   <input type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning" onclick="myFunction()" value="Print jobcard">
                </div>
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div> 
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div id="order_table"> 
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:arial; font-size:  13px;" style="width: auto;">  
                  <thead>  
                       <tr class="table-active">
                            <td>truck number</td>  
                            <td>job card number</td>  
                            <th >Product name</th>  
                            <td>Product type</td>
                            <td>Available Quantity</td> 
                            <td>Recive quantity</td> 
                            <td>Purchase Rate</td> 
                            <td>Your Amount</td>
                             <td>Mistry</td>
                              <td>Submission date</td>
                             <td>Time</td>
                             <td>Market Bill</td>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                            <td>'.$row["truck_no"].'</td>  
                            <td>'.$row["job_card_no"].'</td>  
                            <td >'.$row["partsname"].'</td>
                             <td>'.$row["partstype"].'</td>  
                            <td>'.$row["available_qty"].'</td>
                            <td>'.$row["quantity"].'</td> 
                             <td>'.$row["latest_rate"].'</td>
                              <td>'.$row["amount"].'</td>
                             
                              <td>'.$row["mistry"].'</td> 
                              <td>'.$row["submission_date"].'</td>  
                               <td>'.$row["timestamp1"].'</td>  
                                <td>'.$row["market_bill"].'</td>  
                             
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_internal_job.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

