    

<?php
require("connect.php");
include('header.php');

?>
 
<!DOCTYPE html>
<html>
<head>
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php  include('aside_main.php'); ?> 
  
  <div class="content-wrapper">
    
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Office Jobcard(Use for office only)</h3>
       <button onclick="location.href = 'basic_jobcard.php';"  class="btn btn-warning" style="float: right;">Add New</button>
      </div>


      <script>
        function myFunction() {
          window.print();
        }
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }
            
       </script>
        <!--  <div class="row">
                
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div>  -->
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div id="order_table"> 
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;" style="width: auto;">  
                  <thead>  
                       <tr class="table-active">
                         <td>office jobcard no</td> 
                            <td>Office Complaint</td>  
                            <!-- <td>Office jobcard action</td>  
                            <td>office extra mistry</td>   -->
                            <td>Jobcard type</td>
                           
                            <td>bill</td> 
                            <td>insert date</td> 
                       </tr>  
                  </thead>  
                  <?php  
                    $username = $_SESSION['username'];
                     $sql = "SELECT count(office_complaint) as office_complaint,office_jobcard_action,office_extra_mistry,jobcard_type,office_jobcard_no,bill,insert_date from office_jobcard_only where username='$username' group by office_jobcard_no";
                    $result = $conn->query($sql);
 
                  while($row = mysqli_fetch_array($result))  
                  {  
                     $office_complaint = $row["office_complaint"];
                       //@$office_complaint1 =  count($office_complaint);
                       $office_jobcard_action = $row["office_jobcard_action"];
                       $office_extra_mistry = $row["office_extra_mistry"];
                       $jobcard_type = $row["jobcard_type"];
                       $office_jobcard_no = $row["office_jobcard_no"];
                        $bill = $row["bill"];
                       $insert_date = $row["insert_date"];
                       $bill1=explode(",",$bill);
                       $count3=count($bill1);
                      ?>

                   <tr>
                    <td>
                      <form method="POST" action="fetch_office_jobcard_detail.php">
                         <input type="submit" class="form-control btn btn-link " name="office_jobcard_no" value="<?php echo $office_jobcard_no; ?>"  >
                      </form>
                   </td>
                   <td><?php echo $office_complaint?></td>
                    <!-- <td><?php echo $office_jobcard_action?></td>
                    <td><?php echo $office_extra_mistry?></td>
                    -->
                   <td><?php echo $jobcard_type?> </td>
                 
                   <td> <?php
                        if (strlen($bill) > 0) {
                           for($j=0; $j<$count3; $j++){
                            ?>
                            <a href="../jobcard/<?php echo $bill1[$j]; ?>" target="_blank">File <?php echo $j+1; ?></a><br>
                             <?php } 
                             }  
                             else{
                              echo "no file";
                             }
                          ?>
                    </td>
                      <td><?php echo $insert_date?> </td>
                     
                </tr>
                        
                 <?php }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter_internal_job.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

