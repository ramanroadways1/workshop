  <?php
require("connect.php");
include('header.php');
?>
<?php 

date_default_timezone_set('Asia/Calcutta'); 
    $date=date("d-m-Y");
    $date1=date("Y-m-d");
    $current=date("Y-m-d H:i:s");
    $time=date("h:i:A");

 $sql22 = "SELECT COUNT(DISTINCT inspection_no) AS total FROM `inspection_record`;";

  $result22 = $conn->query($sql22);
if(mysqli_num_rows($result22) > 0)
   { 
        while($row1 = mysqli_fetch_array($result22)){
     
       $total = $row1['total'];
     }
   }

$sql22 = "SELECT COUNT(DISTINCT job_card_no) AS job from external_job_cards where external_job_cards.status='0'";

$result22 = $conn->query($sql22);
if(mysqli_num_rows($result22) > 0)
{ 
    while($row1 = mysqli_fetch_array($result22)){
 
   $job = $row1['job'];
 }
}

?>

<!DOCTYPE html>
<html>
<head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php  include('aside_main.php'); ?>
  <div class="content-wrapper">
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Internal / External Jobcard Pending</h3>
        <p style="color:#008000">PENDING INTERNAL JOBCARD  [<?php echo $total ;?>]</p>
        <p style="color:#FF0000">PENDING EXTERNAL JOBCARD  [<?php echo $job ;?>]</p>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                    
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                            <td >View</td>
                            <td >Truck No</td>
                            <td>Truck Driver</td>
                            <td>Inspection Number</td>
                            <td>Inspection Date</td>
                            <td>Jobcard Date</td>
                            <td>Pending Days</td>
                            <td>Employee Name</td>
                            <td>Delay(1 month)</td>
                       </tr>  
                  </thead>  
                   <?php
                     $username =  $_SESSION['username'];
                      $show = "SELECT inspection_record.*,emp_data.empname,datediff(job_card_date,'$date1') as days from inspection_record  LEFT JOIN emp_data ON inspection_record.employee=emp_data.empcode where inspection_record.username='$username' group by inspection_no,start_time";
                        $result = $conn->query($show);
                       
                    if ($result->num_rows > 0) {
                    $countdaysall='';
                      while($row = $result->fetch_assoc()) {
                       
                            $sno = $row["prob_id"];
                            $truck_no1 = $row["truck_no1"];
                            $truck_driver = $row["truck_driver"];
                            $inspection_no = $row["inspection_no"];
                            $job_card_no = $row["job_card_no"];
                            $job_card_date = $row["job_card_date"];
                            $countdaysall=$row['days'];
                            $date1 = $row["date1"];
                            $empname = $row["empname"];
                        
                      ?>
                      <tr>                  
                       <td> 
                    <form method="POST" action="fetch_job_card_detail.php" target="_blank">
                      <input type="submit" class="form-control btn btn-success btn-sm" style="width: 100px;height: 30px;" name="truck_no" value="<?php echo $truck_no1; ?>"  >
                    </form>
                  </td>
                  <td><?php echo $truck_no1?>
                    
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" name="truck_driver" value='<?php echo $truck_driver; ?>'>
                  </td>

                  <td><?php echo $inspection_no?>
                    <input type="hidden" name="inspection_no" value='<?php echo $inspection_no; ?>'>
                  </td>
                   

                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td><?php echo $job_card_date?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $job_card_date; ?>" >
                  </td>
                  <td><?php echo (abs($countdaysall))?>
                    <input type="hidden" readonly="readonly" name="countdaysall" value="<?php echo $countdaysall; ?>" >
                  </td>

                  <td><?php echo $empname?>
                    <input type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" >
                  </td>
                  <?php 
                  $today_date = date('yy-m-d');
                    $diff = strtotime($job_card_date) - strtotime($today_date);
                     $final_diff = abs(round($diff / 86400));  
                     if ($final_diff > '30') {
                      ?>
                      <td><?php echo "<font style='color:red;'> Close Jobcard immediately<font>"; ?> </td>
                      <?php
                     }else{
                      ?>
                  <td><?php echo "Pending"; ?> </td>
                      <?php
                     }
                   ?>
                  
                 
                </tr>
           
              <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
           <!--  Pending Job Crad External -->
           <head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
  <div class="table-responsive"> 
                <div class="col-md-0"></div> 
            <div class="col-md-12">
                          <table id="employee_data1" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                            <td>Job Crad Number</td>
                            <td>id</td>
                            <td>Truck Number</td>
                            <td>Truck Driver</td>
                            <td>Employee Name</td>
                            <td>Pending Days</td>
                            <td>Jobcard Date</td>
                       </tr>  
                  </thead>  
                  
              <?php
              $username = $_SESSION['username'];
              $show = "SELECT external_job_cards.*,emp_data.empname,datediff(date1,'$date1') as daysext from external_job_cards LEFT JOIN emp_data ON external_job_cards.employee=emp_data.empcode where external_job_cards.status='0' and external_job_cards.username='$username' group by job_card_no";

              $result = $conn->query($show);
              if ($result->num_rows > 0) {
                  // output data of each row
                $i=1;
                $id_customer=1;
                while($row = $result->fetch_assoc()) {
                  $id = $row["id"];
                  $truck_no2 = $row["truck_no2"];
                   $job_card_no = $row["job_card_no"];
                   $truck_driver = $row["truck_driver"];
                  $date1 = $row["date1"];
                  $daysext = $row["daysext"];
                  $empname = $row["empname"];
                  
                ?>
                <tr> 
                                   <td>
                    <form method="POST" action="external_job_card_detail.php">
                      <input type="submit"  name="job_card_no" value="<?php echo $job_card_no; ?>" class="btn btn-danger btn-sm" >
                    </form>
                  </td>
                  <td><?php echo $id?>
                    <input type="hidden" name="sno" value='<?php echo $id; ?>'>
                  </td>
                 
                  <td><?php echo $truck_no2?>
                    <input type="hidden" name="truck_no2" value='<?php echo $truck_no2; ?>'>
                  </td>
                   

                  <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="job_card_no" value="<?php echo $truck_driver; ?>" >
                  </td>

                  <td><?php echo $empname?>
                    <input type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" >
                  </td>
                 <td><?php echo (abs($daysext))?>
                    <input type="hidden" readonly="readonly" name="daysext" value="<?php echo $daysext; ?>" >
                  </td>

                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>

                 
                     
                </tr>
           
               <?php
                   $id_customer++;
                   $i++;
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
              <div class="control-sidebar-bg"></div>
          <!--  Ending pending external Jobcard -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
<!--         <div class="box-footer clearfix">
       
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
  <script>  
 $(document).ready(function(){  
      $('#employee_data1').DataTable();  
 });  
 </script>





