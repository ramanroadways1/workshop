
  <?php
require("connect.php");
include('header.php');

?>
<!DOCTYPE html>
<html>
<head>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


   
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 


</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-size: 13px; font-family: verdana">
<div class="wrapper">
<?php  include('aside_jobcard.php'); ?> 
  <div class="content-wrapper">
    
    <section class="content-header">
        </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Internal jobcard Record Detail</h3>
       
      </div>
        <!-- /.box-header -->

      <script>
        function myFunction() {
          window.print();
        }

         function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }
            
       </script>
         <div class="row">
          
               
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" autocomplete="off" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" autocomplete="off" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div>      


        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                      <div id="order_table"> 
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                           <td>Truck Number</td>
                            <td>Truck Driver</td>
                            <td>Jobcard Number</td>
                             <td>Complaint</td>
                            <td>Jobcard Work</td>
                            <td>Mistry</td>
                            <td>Submission Date</td>
                       </tr>  
                  </thead>  
                   <?php
                   $username = $_SESSION['username'];
                    $show = "SELECT * from  job_card_record  where username='$username' ORDER BY inspection_no ASC";

                        $result = $conn->query($show); 

                    if ($result->num_rows > 0) {
                    
                      while($row = $result->fetch_assoc()) {
                       
                       $truck_no = $row["truck_no1"];
                       $truck_driver = $row["truck_driver"];
                       $job_card_no = $row["job_card_no"];
                        $work_action = $row["work_action"];
                         $job_card_work = $row["job_card_work"];
                       $manually_mistry = $row["manually_mistry"];
                       $now_date = $row['now_date'];
                        
                      ?>
              <tr> 
                
                  <td><?php echo $truck_no?>
                    <input type="hidden" name="truck_no" value='<?php echo $truck_no; ?>'>
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $truck_driver; ?>" >
                  </td>
                   <td><?php echo $job_card_no?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $job_card_no; ?>" >
                  </td>
                  <td><?php echo $work_action?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $work_action; ?>" >
                  </td>
                  <td><?php echo $job_card_work?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $job_card_work; ?>" >
                  </td>
                  <td><?php echo $manually_mistry?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $work_action; ?>" >
                  </td>
                  <td><?php echo $now_date?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $submission_date; ?>" >
                  </td>

              </tr>
           
              <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
       
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                        
                          url:"Internal_ledger_filter.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

