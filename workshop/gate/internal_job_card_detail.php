<?php
require("connect.php");
include('header.php');

?>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
  @media print {
  button {
    display: none !important;
  }
  input,
  textarea,select {
    border: none !important;
    box-shadow: none !important;
    outline: none !important;
    display: none !important;
  }
  #print,#sub,#new_prblm,#issue,#h4 {
    display: none;
  }

  #second {
    float: right;
  }
   #first {
    float: left;
  }
  div{
       font-size: 16pt;
         font-family:verdana;
  }
   table, tr,body,h4,form,td  {
        height: auto;
        font-size: 16pt;
         font-family:verdana;
        font: solid #000 !important;
        }
  
  body {
  zoom:50%; 
}
 #print_page
  {
    width: 150px;
  }


}
    
}
    
}
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini" style="font-family: verdana; font-size: 13px;">
<div class="wrapper">
  <?php include('aside_main.php'); ?>
 
  
  <div class="content-wrapper">
    <section class="content-header">
    
    </section>
   

    <section class="content">
     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 style="font-family:verdana; "class="box-title">Internal Jobcard Record</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $valueToSearch=$_POST['inspection_no'];

          $sql = "SELECT * FROM  job_card_record WHERE  inspection_no='$valueToSearch'";
              $result = $conn->query($sql);
                  
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
             
                  $truck_driver = $row['truck_driver'];
                  $truck_no = $row['truck_no1'];
                  $job_card_no = $row['job_card_no'];
                  $date1 = $row['date1'];

                   $start_time1 = $row['start_time'];
                   $start_time =  substr($start_time1,0,-2);

                  $inspection_no = $row['inspection_no'];
                  $now_date = $row['now_date'];

                   $job_card_date = $row['job_card_date'];

             }
               ?>
      <script>
        function myFunction() {
          window.print();
        }

           function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            var enteredtext = $('#text').val();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            $('#text').html(enteredtext);
            }
    
       </script>


               <div class="col-md-12">
                   <input type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning" onclick="printContent('mydiv');" value="Print jobcard">
                </div>

                <div id="mydiv" >     


                 <div class="row">
                 <div id="first" class="col-md-4">
                   <label>Truck Number:</label>
                 <?php echo $truck_no;?><br>
                
                <label>Truck Driver:</label>
                <?php echo $truck_driver;?><br>
                
                 <label>Inspection no:</label>
                <?php echo $inspection_no;?><br>
               </div>

                <div class="col-md-4" id="second">
                 
                   <label>Inspection Date:</label>
                <?php echo $date1;?><br>
                
                 <label>Inspection start Time:</label>
                <?php echo $start_time;?><br>

                <label>Jobcard No:</label>
                <?php echo $job_card_no;?><br>
                 </div>

                 <div class="col-md-4">
                 <label>Jobcard Start Date:</label>
                <?php echo $job_card_date;?><br>

                 <label>Jobcard Submission  Date:</label>
                <?php echo $now_date;?><br>

                 </div>
              

             </div>

            <?php
            
          }
          else{
            echo "<SCRIPT>
                        window.location.href='found_job.php';
           </SCRIPT>";
            exit();
          }
            ?><br>
          <!--    <button type="button"  class="btn btn-info" style='margin-left:87%;' data-toggle="modal" data-target="#myModal">Add New Problem</button> -->
        
        <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       
                               <tr class="table-active">
                                  <th scope="row">SNo</th>
                                   <th class="col-md-2">Department</th>
                                  <th  class="col-md-2" style="display: none;">Mistry</th>
                                  <th class="col-md-4">Inspection Complaint</th>
                                  <th class="col-md-4">Jobcard Work</th>
                                  <th class="col-md-4">Jobcard Mistry</th>
                                  <th class="col-md-4">Mistry Amount</th> 

                                </tr>

                         
                  </thead> 
                   <script type="text/javascript">
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
      
      var val1 = $(this).attr("value");
      var val2 = $(this).attr("data1");
      
      $('#WrkAct1'+val1).val(val2);
      
      if($(this).prop("checked") == true){
        $('#WrkAct1'+val1).attr('readonly',false);
        
            }
            else if($(this).prop("checked") == false){
        $('#WrkAct1'+val1).attr('readonly',true);
            }
        });
    });
</script> 
                   <?php  
                      $query = "SELECT * FROM  job_card_record WHERE  inspection_no='$valueToSearch'";
                     $sum=mysqli_query($conn,"SELECT SUM(work_amount) as work_amount FROM job_card_record WHERE inspection_no='$valueToSearch' ");
                      $row2=mysqli_fetch_array($sum);
                     


                      $result = mysqli_query($conn,$query);
                      $val = 1;
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id=$row['id'];
                    $sno=$row['sno'];
                  
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                     $work_action = $row['work_action'];
                     $job_card_work = $row['job_card_work'];
                    $manually_mistry = $row['manually_mistry'];
                    $work_amount = $row['work_amount'];
                   
                  ?>
                  <tr>
                    <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                  <td> <?php echo $val?>            
                </td>
                <td style="display: none;"><?php echo $sno?>
                </td>
                 <td><?php echo $department?>
                </td>
                <td style="display: none;"><?php echo $mistry?>
                </td>
                <td><?php echo $work_action?>
                </td>
                <td><?php echo $job_card_work?>
                </td>
                 <td><?php echo $manually_mistry?>
                </td>
                 <td><?php echo $work_amount?>
                </td>
               
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                     $val++;
                  }
                   echo "<tr>
                    <td colspan='1'> </td>
                    <td colspan='4'>Total Calculation : </td>
                    <td>$row2[work_amount]</td>
                  </tr>";
                  ?> 
                  <tr>
              <!-- <td colspan='5'>Total Calculation : </td>
              <td><?php echo $work_amount?></td>
            </tr>  -->
                </table> 
                <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:arial; font-size:  15px;"> 
                 
                </table>
                   <center><h4 style="color: #7c795d; font-family: 'Source Sans Pro', sans-serif; font-size: 20px; font-weight: 400; line-height: 32px; margin: 0 0 24px;" id="h4">Issue product in internal jobcard</h4></center>
                 <table id="employee_data" class="table table-striped table-bordered" style="font-family:arial; font-size:  14px;">  
                  <thead>  
                       <tr>  
                            <td>truck number</td>  
                            <td>job card number</td>  
                            <td>Product name</td>  
                            <td>Product type</td>
                            <td>Available Quantity</td> 
                            <td>Recive quantity</td> 
                            <td>Purchase Rate</td> 
                            <td>Your Amount</td>
                             <td>Mistry</td>
                             <td>Inserted Date</td>
                             <td>Time</td>
                           <!--    <td>Market Bill</td> -->
                       </tr>  
                  </thead>  
                  <?php  
                  $sql2 = "SELECT * from add_product_jobcard WHERE job_card_no='$job_card_no'" ;
                  $result2 = $conn->query($sql2);

                  $sum2=mysqli_query($conn,"SELECT SUM(amount) as amount FROM add_product_jobcard WHERE job_card_no='$job_card_no' ");
                      $row4=mysqli_fetch_array($sum2);
                       $amount1 = $row4["amount"];
                       $amount_final = round($amount1,2);
                       //echo $amount_final;

                  while($row = mysqli_fetch_array($result2))  
                  {  
                      $truck_no = $row["truck_no"];
                       $partsname = $row["partsname"];
                       $partstype = $row["partstype"];
                       $available_qty = $row["available_qty"];
                       $job_card_no = $row["job_card_no"];
                        $quantity = $row["quantity"];
                       $latest_rate = $row["latest_rate"];
                       $amount = $row["amount"];
                       $mistry = $row["mistry"];
                       $submission_date = $row["submission_date"];
                       $timestamp1 = $row['timestamp1'];
/*                       $market_bill = $row["market_bill"];
                       $market_bill1=explode(",",$market_bill);
                       $count3=count($market_bill1);*/
                       ?>

                        
                   <tr>
               
                   <td><?php echo $truck_no?></td>
                    <td><?php echo $job_card_no?></td>
                    <td><?php echo $partsname?></td>
                    <td><?php echo $partstype?></td>
                   <td><?php echo $available_qty?> </td>
                    <td><?php echo $quantity?></td>
                    <td><?php echo $latest_rate?></td>
                   <td><?php echo $amount?> </td>
                    <td><?php echo $mistry?></td>
                    <td><?php echo $submission_date?></td>
                   <td><?php echo $timestamp1?> </td>
                   <!-- <td> <?php
                        if (strlen($market_bill) > 0) {
                           for($j=0; $j<$count3; $j++){
                            ?>
                            <a href="../jobcard/<?php echo $market_bill1[$j]; ?>" target="_blank">File <?php echo $j+1; ?></a><br>
                             <?php } 
                             }  
                             else{
                              echo "no file";
                             }
                          ?>
                    </td> -->
                     
                </tr>
                        
                 <?php }  
                  echo "<tr>
                    <td colspan='7'>Total Calculation : </td>
                    <td>$amount_final</td>
                  </tr>";
                  ?>    
                </table> 
                 </div>







                <script>
      function MyFunc(elem)
      {
        document.getElementById("WrkAct1"+elem).readOnly = false;
        var prob_id = elem;

         var department = $('#department'+elem).val();
         var mistry = $('#mistry'+elem).val();
         var problem = $('#problem'+elem).val();
         var work_action = $('#problem'+elem).val();
         $('#WrkAct1'+elem).val(problem);
        var insp_no = '<?php echo  $inspection_no ?>';
        if(work_action!='')
        {

          $.ajax({
                    type: "POST",
                    url: "insert_tmp_values.php",
                   data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&work_action='+work_action,
                    success: function(data){

                        $("#result22").html(data);
                    }
                    });
        }
      }     
       </script>            <div id="result22"></div>

                  <script>
                function myfunction(i)
                {
                  var prob_id = i;
                  var work_action = $('#WrkAct1'+i).val();
                   var department = $('#department'+i).val();
                   var mistry = $('#mistry'+i).val();
                   var problem = $('#problem'+i).val();

                    //alert(mistry)
                  var insp_no = '<?php echo $inspection_no ?>';
                  
                  if(work_action!='')
                  {
                    $.ajax({

                              type: "POST",
                              url: "insert_tmp_values.php",
              data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&work_action='+work_action,
                              success: function(data){
                                  $("#result223").html(data);
                              }
                              });
                  }
           }     
             </script>  
              <div id="result223"></div>
          
          
            <br> 
                  <script>
                  function myprint() {
                    window.print();
                  }
                  </script> 
                  <div class="col-md-12">
              <center>
                <!-- <input type="submit" align="center" name="submit" class="btn btn-primary" > -->
        
              </center>     
            </div><br>
          </form>
<br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


  <!--  <div id="result"></div> -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form  id="AddCompany" method="POST" action="insert_inspection_problem_by_inspectionno.php">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add problem</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <input type="hidden" name="inspection_no" value="<?php echo $valueToSearch; ?>">
                <input type="hidden" name="truck_no1" value="<?php echo $truck_no; ?>">
                <input type="hidden" name="truck_driver" value="<?php echo $truck_driver; ?>">
                <input type="hidden" name="job_card_no" value="<?php echo $job_card_no; ?>">
                <input type="hidden" name="date1" value="<?php echo $date1; ?>">
               
                <input type="hidden" name="start_time" value="<?php echo $start_time; ?>">
      
            </div>
            <div class="col-md-10">
              <input type="hidden" style="width: 100%;"  value="<?php echo $sno; ?>" autocomplete="off" class="form-control" id="sno" name="sno" placeholder="sno"/>
            </div>
            <div class="col-md-2">
                <b>Problem<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;"  class="form-control" id="problem" name="problem" placeholder="Problem" onchange="get_data1(this.value);" required />
            </div><br><br><br>
                  <script type="text/javascript">
                         function get_data1(val) {
                            $.ajax({
                              type: "POST",
                              url: "fetch_problem_detail.php",
                              data:'problem='+val,
                              success: function(data){
                                  $("#rate_master222").html(data);
                              }
                              });
                          }
                    </script>
                    <div id="rate_master222"></div>

             <div class="col-md-2">
                <b>Work Action<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;"  class="form-control" id="work_action" name="work_action" placeholder="Work ACtion" required="required" />
            </div><br><br><br>
             <div class="col-md-2">
                <b>Department<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
               <input type="text" style="width: 100%;"  class="form-control" id="department" name="department" placeholder="Department" readonly="readOnly" />
            </div><br><br><br>

            <div class="col-md-2">
                <b>Mistry<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
                <input type="text" style="width: 100%;"  class="form-control" id="mistry" name="mistry" placeholder="Mistry"  readonly="readOnly" />
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>


      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#AddCompany").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_inspection_problem_by_inspectionno.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>



   <script type="text/javascript">     
      $(function()
      {    
      $( "#problem" ).autocomplete({
      source: 'autocomplete_problem.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      } 
      },
      focus: function (event, ui) {  return false;
      }
      });
      });
    </script>     