
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="shortcut icon" type="image/x-icon" href="../../favicon.ico">

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->


<link rel="stylesheet" href="../../mis_css/css/style.css?id=145">
<link rel="stylesheet" href="../../mis_css/css/bootstrap.css">
<link rel="stylesheet" href="../../mis_css/css/font-awesome.css">
<link rel="stylesheet" href="../../mis_css/css/ionicons.css">
<link rel="stylesheet" href="../../mis_css/css/AdminLTE.css?id=170">
<link rel="stylesheet" href="../../mis_css/css/skins/all-skins.min.css?id=145">
<link rel="stylesheet" href="../../mis_css/css/theme.blue.css?id=155">
<link rel="stylesheet" href="../../mis_css/css/datepicker3.css"    >

<link rel="stylesheet" href="../../mis_css/css/main_time.css" >
<link rel="stylesheet" href="../../mis_css/css/animate.css"   id="bootstrap-css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="../../mis_css/js/jquery-2.2.0.js"></script>   
<script src="../../mis_css/js/html2canvas.js?version=1.2"></script>




<title>BATTERY</title>

 <script>    
  var js_acc_likesrch =  true ;
  var js_itm_likesrch =  true ;
  var js_mach_likesrch =  true ;
  var js_req_charsrch =  3 ;
    
      js_req_charsrch =  1 ;
   </script> 

<script src="../../includes/generaljs.js?version=6.452"></script>
<script  >
  
   function hideLoader() {
       
     $('#loadig_resp').hide();
    }
  
  function showLoader() {
     $('#loadig_resp').show();
    }
  
    function notify(message, timeOut) {
    // Set the message using text method and chain fadeIn with it
    // apply simple setTimeout to fadeOut the message

      message = message.replace(".", "<br>"); 
    $('#boxd').html(message).fadeIn();
    setTimeout(function(){
       $('#boxd').fadeOut();
    }, timeOut*1000);
    } 
    function hideMsg() {
     $('#boxd').fadeOut();
    }
    </script>
<script>
function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>

    
</head>
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini fixed">

<div class="wrapper">
<div class="modal fade bs-example-modal-lg in" id="loadig_resp"  style="padding:200px;display:none ;background: rgba(0, 0, 0, 0.7); " >
  <div class="modal-content" style="background-color: transparent;">
    <div class="" style="height:100px  ;    text-align: center;    font-size: 40px;    padding-top: 20px; color: #fff;" > 
  <i class="fa fa-refresh fa-spin"   onDblClick="hideLoader();"></i><br>
  <div class="login-logo" style="color:#0389fa">
    
    <a href="#">    <img id="logo_img" src="../../images/nway-logo.png" style="border-radius:0%;" alt="Nway Technologies"> <b style="color:#fff">ERP</b></a>
    
    <!--<a href="#" style="color: #fff;" ><b>Nway</b>ERP</a>-->
  

<!--<div class="container">
  <span class="txt anim-text-flow">How are you? Bacon ipsum dolor sit amet.</span>
</div>-->
 
    <!--<img src="images/logo.png"  style="    border: 0px;
    position: fixed;
    bottom: 0;
    right: 0;" > -->    
  </div>
   </div>
  </div>
</div>

 
<script >
try {
  
  if(typeof (Storage) !== "undefined") {
    var tmp = localStorage.getItem('skin'); 
    if (tmp ){
       $("body").addClass(tmp);
    }
   } 
    
  }catch (e) {
     
  }
  
</script>
<script >  
 showLoader(); 
</script>


<!-- Main Header -->
<header class="main-header">
  <!-- Logo -->
  <a href="../../home_new.php" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>ERP</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>NwayERP</b></span>
  
  <span class="logo-mobile" title="HEAD OFFICE"> 
         <b style="font-size:14px">SHALIMAR ROADWAYS </b> <b style="font-size:13px">(HEAD OFFICE )</b> 
          
    
  </span>
  
  
  
   </a>
  <!-- Header Navbar -->

  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu customcompany"  style="float:left" >
     <ul class="nav navbar-nav" onDblClick="return showcalxxx()">
        <li >
                    <a href="javascript:void(0)" onClick="return false" style="padding-bottom: 10px;    overflow: hidden;    white-space: nowrap;    text-overflow: ellipsis; " >
                    <span class=""><b style="font-size:14px">SHALIMAR ROADWAYS </b> <b style="font-size:13px">(HEAD OFFICE )</b> </span> </a> </li>
      </ul>
    </div>
    <div class="navbar-custom-menu" style="float:right">
      <ul class="nav navbar-nav">
      
      
      
        <!--User Account Menu -->
        <li><a href="JavaScript:void(0)"  style="padding-top:5px;padding-bottom:5px;text-align:center;"> <i class="fa fa-calendar"></i> 
        <span style="font-size:14px">&nbsp; Fin-Yr. : &nbsp;2020-2021</span><br>
         <span style="">           
             <div class="clock">              
             <i class="fa fa-clock-o"></i>&nbsp;
                    <div class="day">
                        <p class="sunday">sun</p>
                    </div> 
                    <div class="day">
                        <p class="monday">mon</p>
                    </div> 
                    <div class="day">
                        <p class="tuesday">tue</p>
                    </div> 
                    <div class="day">
                        <p class="wednesday">wed</p>
                    </div> 
                    <div class="day">
                        <p class="thursday">thu</p>
                    </div> 
                    <div class="day">
                        <p class="friday">fri</p>
                    </div> 
                    <div class="day">
                        <p class="saturday">sat</p>
                    </div> 
                    <!-- HOUR -->
                    <div class="numbers">
                        <p class="hours"></p> 
                    </div> 
                    <div class="colon">
                        <p>:</p>
                    </div> 
                    <!-- MINUTE -->
                    <div class="numbers">
                        <p class="minutes"></p> 
                    </div> 
                    <div class="colon">
                        <p>:</p>
                    </div> 
                    <!-- SECOND -->
                    <div class="numbers">
                        <p class="seconds"></p> 
                    </div> 
                    <!-- AM / PM -->
                    <div class="am-pm">  <!-- AM --> 
                        <div>
                            <p class="am">am</p> 
                            <p class="pm">pm</p>
                        </div>
                    </div> 
                </div>
        </span> 
        </a> </li>
        
         
   <li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onClick="flip_dropdown(this.id);this.ischanged==1"   >
            <i class="fa fa-envelope-o faa-shake animated" id="animated_msg_icon" style="font-size: 18px;" ></i>
            <i class="fa fa-envelope-o" style="font-size: 18px; display:none;" id="simple_msg_icon"></i>
            <span class="label label-danger" style="border-radius: 20px;" id="span_full_msg">
                <span class="heartbit"></span>
                <span class="point"></span>
                <span id="msg_num_count"></span>
            </span> 
    </a>
    <ul class="dropdown-menu " data-dropdown-in="flipInY" data-dropdown-out="flipOutY" id="msg_drop_down" style="box-shadow:4px 4px 4px #cccccc;">
      <li class="header" id="new_msg" >You have No New messages</li>
      <li>
       
      <ul class="menu">
                          <script>
            document.getElementById('span_full_msg').style.display='none';
            document.getElementById('animated_msg_icon').style.display='none';
            document.getElementById('simple_msg_icon').style.display='';
            </script>
                          
      </ul>
      </li>
      <li class="footer"><a href="javascript:void(0)" onClick="show_Iframe_popup('../../admin/userpreferences/msg_mgm_detail.php?bpuserrights=1&see_all=1','All Message Detail')">See All Messages</a></li>
    </ul>
    </li>
     
   <script>function flip_dropdown(id) {
    //$('#msg_drop_down').toggle();
   $('#msg_drop_down').removeClass('animated flipInY') ; 
   $('#msg_drop_down').toggleClass('animated flipInY') ;  } 
     </script>        
        
        
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <!-- The user image in the navbar-->
                    
          <img src="../../mis_css/img/avatar5.png" class="user-image" alt="User Image">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          
          
          
          
          <span class="hidden-xs">SCPL</span> </a>
          <ul class="dropdown-menu">
           
           
          
            <!-- The user image in the menu -->
            <li class="user-header"> 
              
            
            <img src="../../mis_css/img/avatar5.png" class="img-circle" alt="User Image">
              <p style="font-size:14px"> NWAY TEAM <small>Emp Code : 1</small>
              
              
              
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-left"> <a href="../../changepass_user.php" class="btn btn-default btn-flat">Change Password</a> </div>
              <!--
                <div class="pull-left" style="padding-left:6%; padding-right:6%">
                  <a href="../../userprefrence/changepass_user.php" class="btn btn-default btn-flat">Change Password</a>
                </div>-->
              <div class="pull-right"> <a href="../../logout.php" class="btn btn-default btn-flat">Sign out</a> </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li> <a href="#" data-toggle="control-sidebar">
        <!--<i class="fa fa-cog fa-spin fa-fw fa-1x" style="font-size:18px;"></i>-->
        <i id='btnside' class="fa fa-gears"></i> 
        
        </a> 
        </li>
        <!--fa fa-gears-->
      </ul>
    </div>
  </nav>
</header>
<div id="boxd" class="boxmsg" onClick="hideMsg()" title="Click to close"  > </div>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar"  id="left-menu">
    <style >
    .leftselect { 
  border-radius: 5px !important;
  background-color: #374850  !important;
  color: #c5c5c5  !important;
  
   }
   

   .searchmn:active ,  .searchmn:focus{
     border:0px !important;
     border-color:transparent !important;
   }  
  </style>
    <!-- search form -->
    <div class="user-panel"  >
      <div class="pull-left image"  >
                <img src="../../mis_css/img/mis_icon/machinery.png" style="background: #fff;"  class="img-circle" >
              </div>
      <div class="pull-left info">
                <select name="gdept_id121xx" id="gdept_id121xx"  class="form-control leftselect" style="padding: 7.5px 4px !important;margin-top: 0px !important; margin-bottom: 0px !important;z-index: 100;"  onChange="fn_change_dept121xx()"  >
                    <option value="1"   
          >
          ADMIN          </option>
                    <option value="7"   
          >
          ADMIN-ACCOUNTS          </option>
                    <option value="24"   
          >
          BUILTY-TRANSPORT          </option>
                    <option value="25"   
        selected="selected"  >
          FLEET MANAGEMENT          </option>
                    <option value="2"   
          >
          HUMAN-RESOURCE          </option>
                    <option value="22"   
          >
          MIS-ADMIN          </option>
                    <option value="3"   
          >
          PURCHASE          </option>
                    <option value="6"   
          >
          STORE          </option>
                    <option value="23"   
          >
          VEHICLE-TRANSPOR          </option>
                  </select>         
      </div>
      
      
      
      
    </div>
    <div  class="sidebar-form" style="border:0px">
      <div class="input-group" style="padding-bottom: 0px;border:0px">
        <input type="text" name="search_id121xx" id = "search_id121xx" class="form-control searchmn" style="width:100% ;margin-bottom: 0px!important;     z-index: 0;" placeholder="Search..." value="" onKeyUp="fn_search_menu()" >
        <span class="input-group-btn">
        <button type="button" name="search" id="search-btn" style="margin-bottom: 0px!important;border:0px" class="btn btn-flat"><i class="fa fa-search"></i> </button>
        </span> </div>
    </div>
    <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
                        <li  class=" "  > <a   href="../../fleet/master/dashboard.php"><i class="fa fa-home"></i> <span> Dashboard </span> </a> </li>
      
<li class="treeview" > <a href="#"><i class="fa fa-dashboard"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
    <li><a href="#"> Vehicle Category <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/vehicle_category.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/vehicle_category_list.php"  class=""> List</a> </li>
      </ul>
    </li>
    <li><a href="#"> Vehicle <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/vehicle.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/vehicle_list.php"  class=""> List</a> </li>
      </ul>
    </li>
        <li ><a href="../../fleet/master/owner_detail_verify.php"  class=""> Owner Detail Verify</a> </li>
      
  
  <li> <a href="#"> Document <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/document.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/document_list.php"  class=""> List</a> </li>
        <li> <a href="#"> Document Upload <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/master/machine_document.php"  class=""> Add</a> </li>
            <li ><a href="../../fleet/master/machine_document_list.php"  class="">List</a> </li>
          </ul>
        </li>
      </ul>
    </li>
  
  <li ><a href="#" >Service Unit <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/unit.php"  class="">Add</a> </li>
      </ul>
    </li>
  
  
    <li><a href="#"> Service Item Group <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/serviceitemgroup.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/serviceitemgrouplist.php"  class=""> List</a> </li>
      </ul>
    </li>
    <li><a href="#"> Service Item <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/serviceitem.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/serviceitemlist.php"  class=""> List</a> </li>
      </ul>
    </li>
  
  <li ><a href="#" >Service Schedule <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li><a href="#"  class=""> Category Wise <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/master/serviceschedule_category.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/master/serviceschedulelist.php"  class=""> List</a> </li>
          </ul>
        </li>
        <li><a href="#"  class="">Single <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/master/serviceschedule.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/master/serviceschedulelist.php"  class=""> List</a> </li>
          </ul>
        </li>
      </ul>
    </li>
  
    <li><a href="#">Brand<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/brandmaster.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/brandmasterlist.php"  class=""> List</a> </li>
      </ul>
    </li>
    <li><a href="#">Model<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/modelmaster.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/modelmasterlist.php"  class=""> List</a> </li>
      </ul>
    </li>
  
  
    <li ><a href="../../fleet/master/tyre_size.php"  class=""> Tyre Size </a> </li>
    
    <li ><a href="../../fleet/master/tyre_position.php"  class=""> Tyre Position </a> </li>
    
    <li><a href="#">Tyre Master<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/master/tyremaster.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/tyremasterlist.php"  class=""> List</a> </li>
      </ul>
    </li>
  
  
  
    <li><a href="#">Battery Master<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li  id='actv_name' ><a href="../../fleet/master/batterymaster.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/master/batterymasterlist.php"  class=""> List</a> </li>
      </ul>
    </li>
  
  </ul>
</li>
<li class="treeview" > <a href="#"> <i class="fa fa-cubes"></i> <span>Transaction</span> <i class="fa fa-angle-left pull-right"></i> </a>
  <ul class="treeview-menu">
    <li><a href="#"> Service / Job Sheet <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/servicejoblogentry.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/transaction/servicejobloglist.php"  class=""> List</a> </li>
    
    
    <li ><a href="../../fleet/transaction/external_services_appr.php"  class=""> External Services Approval</a> </li>
      </ul>
    </li>
    <li><a href="#"> Accidental Records <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/accident_record.php"  class=""> Add</a> </li>
        <li ><a href="../../fleet/transaction/accident_record_list.php"  class=""> List</a> </li>
      </ul>
    </li>
    <li> <a href="" >Tyre Scrap/Claim/Remould <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/tyre_scrap_sold_remo.php" >Add</a> </li>
        <li ><a href="../../fleet/transaction/tyre_scrap_sold_remo_list.php" >List</a> </li>
      </ul>
    </li>
    <li> <a href="" >Battery Scrap/Sold/Remould <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/battery_scrap_sold_remo.php" >Add</a> </li>
        <li ><a href="../../fleet/transaction/battery_scrap_sold_remo_list.php" >List</a> </li>
      </ul>
    </li>
    <li> <a href="" >Annual Maint. Contract<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/annual_maintenance.php" >Add</a> </li>
        <li ><a href="../../fleet/transaction/ann_maintenance_list.php" >List</a> </li>
        <li ><a href="../../fleet/transaction/annual_mntnc_reco.php" >Reco</a> </li>
      </ul>
    </li>
    <li> <a href="" >Wheel Alignment Exp.<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/wheel_alignment_expence.php" >Add</a> </li>
        <li ><a href="../../fleet/transaction/wheel_alignment_expence_list.php" >List</a> </li>
      </ul>
    </li>
    <li> <a href="" >Vehicle Sold<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/vehicle_sold.php" >Add</a> </li>
        <li ><a href="../../fleet/transaction/vehicle_sold_list.php" >List</a> </li>
      </ul>
    </li>
    <li> <a href="" >External Services<i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/external_service_entry_gst.php" >Add GST</a> </li>
        <li ><a href="../../fleet/transaction/extrenal_service_list.php" >List</a> </li>
      </ul>
    </li>
  
  <li> <a href="" >Trolley Allocation <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/trolley_allocation.php" >Add </a> </li>
        <li ><a href="../../fleet/transaction/trolley_allocation_list.php" >List</a> </li>
      </ul>
    </li>
  
  
  <li><a href="#" >Documentation <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li><a href="#" >Machine Detail <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/machine_detail.php" class="" >Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_detail_list.php"  class=""> List</a> </li>
          </ul>
        </li>
        <li><a href="#" >Fitness <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/machine_fitness.php" >Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_fitness_list.php" > List</a> </li>
          </ul>
        </li>
        <li><a href="#" >Green Tax <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/green_tax.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_greentax_list.php"  class=""> List</a> </li>
          </ul>
        </li>
        <li><a href="#" >Insurance <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/machine_insurance.php" >Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_insurance_list.php" > List</a> </li>
          </ul>
        </li>
        <li ><a href="#">Permit <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/machine_permit.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_permit_list.php"  class=""> List</a> </li>
          </ul>
        </li>
        <li><a href="#" >PUC <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/puc.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/transaction/machine_puc_list.php"  class=""> List</a> </li>
          </ul>
        </li>
        <li ><a href="#" >Taxation <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/taxation.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/transaction/taxation_list.php"  class=""> List</a> </li>
          </ul>
        </li>
         
        <li ><a href="#" >I Form <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li ><a href="../../fleet/transaction/i_form.php"  class="">Add</a> </li>
            <li ><a href="../../fleet/transaction/i_form_list.php"  class="">List</a> </li>
          </ul>
        </li>
    
     
      </ul>
    </li>
  
  
  <li><a href="#" >Loan / Finance <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/loan_finance.php"  class="">Add</a> </li>
        <li ><a href="../../fleet/transaction/loanfinancelist.php"  class=""> List</a> </li>
      </ul>
    </li>
  
  <li> <a href="" >Tyre Issue <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/tyre_iss_rec.php" >Add </a> </li>
        <li ><a href="../../fleet/transaction/tyre_iss_rec_list.php" >List</a> </li>
      </ul>
    </li>
  
  <li> <a href="" >Battery Issue <i class="fa fa-angle-left pull-right"></i> </a>
      <ul class="treeview-menu">
        <li ><a href="../../fleet/transaction/battery_iss_rec.php" >Add </a> </li>
        <li ><a href="../../fleet/transaction/battery_iss_rec_list.php" >List</a> </li>
      </ul>
    </li>
  
  
  
  </ul>
</li>
<li class="treeview"> <a href="#"> <i class="fa fa-file-pdf-o"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i> </a> </li>
       
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"><!-- Content Header (Page header) --><link rel="stylesheet" href="../../mis_css/css/jquery.autocomplete.css?id=1051" type="text/css">
<script type="text/javascript" src="../../mis_css/js/jquery.autocomplete.js?id=1051"></script>
<script type="text/javascript" src="../../mis_css/js/auto_complete_fill.js?id=1051"></script>
<script type="text/javascript" src="../../mis_css/js/machine_group_fill.js?id=1051"></script>
<script type="text/javascript" src="../../mis_css/js/account_fill.js?id=1051"></script>
<script type="text/javascript" language="javascript" >
  function validate_form(thisform)
  {
    var ctl=document.getElementById("battery_sno")
    var ctl1=document.getElementById("battery_no")

    if (ctl.value=="" || ctl1.value=="") {
        alert("Battery SrNo. And Battery No. Must Be Filled Out");
        ctl1.focus();
        return false;
      }
  
  
  
  var ctl3=document.getElementById("model_id")
    if (ctl3.value==0)
      {
        alert("Please Select Model");
        ctl3.focus();
        return false;
      }
  
  
  
  var ChkBox = document.myform.is_in_vehicle.checked;
    if(ChkBox==true)
    {
      var ctlveh=document.getElementById("cmbmachine")
      if (ctlveh.value==0)
        {
          alert("Please Select Vehicle");
          $('#machine_no').focus();
          return false;
        }
    }
  
  
          
    if (confirm("Sure To Save")==false )
    {
     return false;
    }
      
  }
  

</script>
<script language="javascript" >
function fn_model_change(){

      var url='http://demo.transporterp.org/fleet/master/batterymaster.php?ajax=BRANDMODEL&modelid='+ document.myform.model_id.value  ;
      
      xmlHttp=GetXmlHttpObject();   
      if (xmlHttp==null)
       {
        alert ("Your browser does not support AJAX");
        return;
       } 
       
      xmlHttp.open("get",url,false);
      xmlHttp.send(null); 
      var mystring =xmlHttp.responseText; 

      var arr = mystring.split("||");
      if( arr.length > 1 ) {
        var arr1 = arr[1].split("<|>");
        if( arr1.length > 2 ) {
          
          document.myform.warranty.value=arr1[0];  
          document.myform.battery_type.value=arr1[1];  
          document.myform.volt.value=arr1[2];  
          document.myform.watt.value=arr1[3];  
          document.myform.amp.value=arr1[4];   
          document.myform.noofplates.value=arr1[5];  
          document.myform.fullchargetime.value=arr1[6];  
          document.myform.runaftercharge.value=arr1[7];  
          
            
        }
        
      }        
  
}
</script>
<section class="content-header">
<h1>
  Battery</h1>
<h3 class="breadcrumb"><a href="batterymasterlist.php"><i class="fa fa-list"></i><strong> List</strong></a> </h3>
</section>
<!-- Main content -->
<section class="content">
  <div class="has-error" style="text-align:center">
    <label class="control-label" for="inputError"> New Mode </label>
  </div>
  <form name="myform" method="post"  action='/fleet/master/batterymaster.php?battery_id=0&amp;mode=INSERT' >
    <table cellspacing=0 cellpadding=0 width="100%"   border="0" align="center" >
      <tr>
        <td width="15%" align="right" class="tabletd" >Battery Sr. No.<font color="#FF0000">*</font></td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="battery_sno" type="text"  id="battery_sno" 
value='7'  maxlength="30" size="10"  readonly="readonly"  >
              </td>
              <td align="right" width="30%" class="tabletd"> Battery No.<font color="#FF0000">*</font> </td>
              <td align="right" width="35%"><input name="battery_no" type="text" class="form-control" id="battery_no" 
value=''  maxlength="30" size="30" >
              </td>
            </tr>
          </table></td>
        <td width="15%" align="right" class="tabletd" >Battery Type</td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><select  name="battery_type" >
                  <option value="Maintainance Free"   >Maintenance Free</option>
                  <option value="Maintainance Required"  >Maintenance Required</option>
                </select>
              </td>
              <td align="right" width="25%" class="tabletd"> Model <font color="#FF0000">*</font> </td>
              <td align="right" width="40%"><SELECT id='model_id' name='model_id' class='form-control list'  onchange="this.ischanged=1;"   onBlur="if(this.ischanged==1){ this.ischanged=0;fn_model_change() } "    ><Option Value='0' >Please Select</Option><Option Value = '3' >AMARON</Option><Option Value = '11' >CAPRO CPR01</Option><Option Value = '1' >DYNEX 100L</Option></SELECT>              </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td width="15%" align="right" class="tabletd" >Volt</td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="volt" type="text" class="form-control" id="volt"  value=''  maxlength="30" size="30"></td>
              <td align="right" width="30%" class="tabletd"> Watt </td>
              <td align="right" width="35%"><input name="watt" type="text" class="form-control" id="watt"  value=''  maxlength="30" size="30" >
              </td>
            </tr>
          </table></td>
        <td width="15%" align="right" class="tabletd" >Amp.</td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="amp" type="text" class="form-control" id="amp"   value=''  maxlength="30" size="30" >
              </td>
              <td align="right" width="25%" class="tabletd"> No. Of Plates </td>
              <td align="right" width="40%"><input name="noofplates" type="text" class="form-control" id="noofplates"  value=''  maxlength="30" size="30"  >
              </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td width="15%" align="right" class="tabletd" >Full Charge Time </td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="fullchargetime" type="text" class="form-control" id="fullchargetime" value=''  maxlength="30" size="30" />
              </td>
              <td align="right" width="30%" class="tabletd"> Run After Charge (Days) </td>
              <td align="right" width="35%"><input name="runaftercharge" type="text" class="form-control" id="runaftercharge" value=''  maxlength="30" size="30" />
              </td>
            </tr>
          </table></td>
        <td width="15%" align="right" class="tabletd"> Warranty Years <font color="#FF0000"></font></td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="warranty" type="text" class='Numeric_Text form-control' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="warranty"   value='0'  maxlength="30" size="30" >
              </td>
              <td align="right" width="25%" class="tabletd"> Status <font color="#FF0000"></font> </td>
              <td align="left" width="40%">&nbsp;
                <select  name="status"   >
                  <option value="Active"   >New</option>
                  <option value="Old"  >Old</option>
                  <option value="Scrap"   >Scrap</option>
                </select>
              </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="1" width="15%" align="right" class="tabletd" ><strong>Purchase Info.</strong></td>
        <td colspan="3" align="left" class="tabletd" >&nbsp;</td>
      </tr>
      <tr>
        <td width="15%" align="right" class="tabletd" >Vendor</td>
        <td width="35%" align="" class="tabletd" >              
                       <input name="txt_account_name" type="text" id="txt_account_name" onfocus="focustxt=this;"  style="float:left;width:89%; margin-right:0px !important" value="" autocomplete="off" 
         onBlur="set_on_blur('txt_account_name','vendor_id',account_name_arr.arr , ''); "   placeholder="Select Cash Vendor" />
        <input type="hidden" name="vendor_id" id="vendor_id" value=""   >
        <script >
                var account_name_arr = {arr:[]} ;                             
                get_auto_complete_ajax_para('txt_account_name','vendor_id',"Select+top+100+percent+account_id%2C++account_name++++from+dbo.fn_GetAccName%2813%2C1%2C14%29+Where+%28rtrim%28ltrim%28account_name%29%29+like+%27%25XXYYZZ%25%27+OR+account_id+like+%27XXYYZZ%25%27%29+And+%28+%27SELVAL%27+%3D+0++OR++account_id+%3D+%27SELVAL%27+%29%09+And+account_head_id++in+%2820%2C6%2C31%2C32%2C7%2C49%2C19%2C8%2C33%2C34%2C35%2C36%2C37%2C38%2C39%2C40%2C41%2C42%2C43%2C44%2C45%2C46%2C47%2C48%2C54%2C56%2C57%2C60%29++order+by+account_name+",account_name_arr,)
                </script>
        <div class="input-group-addon popover-markup" ><a href="javascript:void(0)" class="trigger" style="color:#000000" ><i class="fa fa-gears"></i></a> <span class="content hide"> <a href="javascript:void(0)"  target="_blank" title="Edit this Account." style="display:inline-block;cursor:pointer;" id="link_acc_edit_0" ><i class="fa fa-edit" style="font-size:17px"></i></a>&nbsp;|&nbsp;<a href="../../accountadmin/master/account.php" target="_blank" title="Add New Account if not exist."> <i class="fa fa-plus-circle" style="font-size:17px"></i> </a> &nbsp;|&nbsp; <a href="../../accountadmin/master/unasign_acc_list.php?bpuserrights=1" title="Un-Assign Account List" target="_blank"> <i class="fa fa-user-times" style="font-size:17px"></i></a> <a href="javascript:void(0);" style="position:absolute; right:-5px ; top:-10px ;" onClick="$('.popover').popover('hide');"> <i class="fa fa-times-circle" style="font-size:17px ;color:#F50307"></i></a> </span> </div>
        </td>
        <td width="15%" align="right" class="tabletd" >Invoice No.</td>
        <td width="35%" align="" class="tabletd" ><table width="96%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" width="35%"><input name="invno" type="text" class="Numeric_Text form-control" id="invno"   value='0'  maxlength="30" size="30" >
              </td>
              <td align="right" width="25%" class="tabletd"> Pur. Date </td>
              <td align="right" width="40%"><input name='purchasedate' type="text" id='purchasedate' class="datecss form-control"  data-inputmask="'alias': 'dd/mm/yyyy'" data-mask  value='16/08/2021'    />
                <div class="input-group-addon" id="div_pur_pick" > <i class="fa fa-calendar"></i> </div></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td width="15%" align="right" class="tabletd" >Amount (Cost)</td>
        <td width="35%" align="" class="tabletd" ><input name="amount" type="text" class='Numeric_Text form-control' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="amount" value='0'  maxlength="30" size="30" style="width:150px;" />
        </td>
        <td width="15%" align="right" class="tabletd" >
        
            
        
        <input type="checkbox" name="is_in_vehicle" id="is_in_vehicle"  } value="1" onClick="fn_invehicle()" /></td>
        <td width="35%" align="" class="tabletd" >Is in Vehicle </td>
      </tr>
      <tr>
        <td colspan="4"  class="tabletd" ><div id="div_invehicle" style="display:block">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="Table_Border">
              <tr>
                <td  width="15%" class= "tabletd" align="right">Vehicle Category </td>
                <td  width="35%" class= "tabletd"><SELECT id='machinecategory' name='machinecategory'  class='form-control select-8-hidden-accessible'  onchange="fn_fill_machine(); "  ><Option Value='0'   selected='selected' >ALL</Option><Option    Value='47'    >@#$%</Option><Option    Value='27'    >@#$%^</Option><Option    Value='28'    >@ABC HYWA</Option><Option    Value='31'    >10 WHEELER</Option><Option    Value='57'    >&nbsp; &nbsp; &nbsp;10 MT- PBR</Option><Option    Value='81'    >&nbsp; &nbsp; &nbsp;10 WHEELER 19MT- NM </Option><Option    Value='97'    >&nbsp; &nbsp; &nbsp;TRUCK</Option><Option    Value='62'    >10 WHEELER 19MT</Option><Option    Value='71'    >&nbsp; &nbsp; &nbsp;10 WHEELER 19MT- SURYA </Option><Option    Value='74'    >10 WHEELER 19MT HGH</Option><Option    Value='78'    >&nbsp; &nbsp; &nbsp;10 WHEELER 19MT-123</Option><Option    Value='79'    >10 WHEELER 19MT- SS</Option><Option    Value='32'    >10 WHEELER DEDICATED</Option><Option    Value='33'    >12 WHEELER</Option><Option    Value='58'    >&nbsp; &nbsp; &nbsp;12WHEELER-PBR</Option><Option    Value='96'    >&nbsp; &nbsp; &nbsp;70 MT</Option><Option    Value='34'    >12 WHEELER GUJARAT</Option><Option    Value='77'    >123 </Option><Option    Value='35'    >14 WHEELER</Option><Option    Value='59'    >&nbsp; &nbsp; &nbsp;PBR</Option><Option    Value='90'    >15 WHEELER</Option><Option    Value='87'    >1500CC</Option><Option    Value='89'    >16 WHEELER </Option><Option    Value='36'    >18 WHEELER</Option><Option    Value='42'    >22 WHEELER</Option><Option    Value='83'    >&nbsp; &nbsp; &nbsp;20 FT </Option><Option    Value='46'    >&nbsp; &nbsp; &nbsp;TATA LOADER</Option><Option    Value='26'    >24 WHEELER 2431*</Option><Option    Value='7'    >24MT*35 MT 12 TYRES</Option><Option    Value='9'    >29MT*42 MT 14 TYRE</Option><Option    Value='18'    >&nbsp; &nbsp; &nbsp;REFUSE TRUCKS</Option><Option    Value='20'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;MULTI-TRAILER TRUCKS</Option><Option    Value='15'    >32 FEET : 28 TON CONTAINERS</Option><Option    Value='63'    >32 FT SINGLE EXCELL CONTAINER 9MT</Option><Option    Value='82'    >32 FT SINGLE EXCELL CONTAINER 9MT- 756</Option><Option    Value='10'    >33MT * 45.5 MT 18 TYRES</Option><Option    Value='37'    >4 WHEELER</Option><Option    Value='11'    >40MT*55 MT 22 TYRES</Option><Option    Value='17'    >&nbsp; &nbsp; &nbsp;TANKERS TRUCK</Option><Option    Value='13'    >&nbsp; &nbsp; &nbsp;TRAILOR</Option><Option    Value='16'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;FLATE-BED-TRAILER</Option><Option    Value='19'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;FLAT-BED TRAILER</Option><Option    Value='94'    >50 MT</Option><Option    Value='95'    >50 MT TEST</Option><Option    Value='38'    >6 WHEELER</Option><Option    Value='64'    >6 WHEELER 11MT 24 FT CONTAINOR</Option><Option    Value='65'    >6 WHEELER 12MT</Option><Option    Value='88'    >8 WHEELER</Option><Option    Value='39'    >CONTAINER</Option><Option    Value='14'    >DUMPER TRUCK</Option><Option    Value='40'    >EICHER</Option><Option    Value='66'    >EICHER 12WHEELER 25MT</Option><Option    Value='69'    >EICHER 12WHEELER 25MT- SURYA</Option><Option    Value='29'    >LGV</Option><Option    Value='41'    >MAHINDRA</Option><Option    Value='53'    >OPEN BODY</Option><Option    Value='56'    >&nbsp; &nbsp; &nbsp;CATEGORY N</Option><Option    Value='54'    >&nbsp; &nbsp; &nbsp;OP 1</Option><Option    Value='55'    >&nbsp; &nbsp; &nbsp;OP 2</Option><Option    Value='60'    >&nbsp; &nbsp; &nbsp;OPEN BODT-PBR</Option><Option    Value='72'    >OPEN BODY 1 </Option><Option    Value='73'    >&nbsp; &nbsp; &nbsp;10 WHEELER 39 </Option><Option    Value='67'    >PICK UP</Option><Option    Value='80'    >PICK UP- SURYA </Option><Option    Value='70'    >PICK UP-SS</Option><Option    Value='30'    >TANKER</Option><Option    Value='91'    >&nbsp; &nbsp; &nbsp;20 MT</Option><Option    Value='92'    >&nbsp; &nbsp; &nbsp;30 MT</Option><Option    Value='93'    >&nbsp; &nbsp; &nbsp;40 MT </Option><Option    Value='84'    >&nbsp; &nbsp; &nbsp;NWAY</Option><Option    Value='85'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;NWAY-1</Option><Option    Value='86'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;NWAY-2</Option><Option    Value='48'    >TANKER2</Option><Option    Value='22'    >TATA LPT 3118 @</Option><Option    Value='45'    >TATA LPT 3119 @</Option><Option    Value='61'    >TEST 10 MT VEHICLE CATAGORY</Option><Option    Value='21'    >TEST VEHICLE CATAGORY</Option><Option    Value='12'    >TRAILER</Option><Option    Value='68'    >TRAILOR BODY 8049 40MT</Option><Option    Value='50'    >TRUCK TEST</Option><Option    Value='52'    >&nbsp; &nbsp; &nbsp;@12 WHEELER</Option><Option    Value='76'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;14 FT </Option><Option    Value='51'    >&nbsp; &nbsp; &nbsp;10 WHEELER TRUCK</Option><Option    Value='75'    >&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;N 123 </Option><Option    Value='44'    >VEHICLE TEST</Option><Option    Value='43'    >VEHICLE-</Option><Option    Value='49'    >VEHICLE-TEST 2</Option></SELECT></td>
                <td  width="15%" class= "tabletd" align="right">Machine / Vehicle <span style="color:red">*</span></td>
                <td  width="35%" class= "tabletd" ><input type="text" name="machine_no"  id="machine_no" placeholder="Search Machine ..." class="form-control"  onBlur="set_machine_onblur('machine_no','cmbmachine',machine_jsonD_arr , '');" onchange=""  >
                  <input type="hidden" name="cmbmachine" id="cmbmachine" value="0"  />
                </td>
              </tr>
              <tr >
                <td  width="15%" align="right" class="tabletd">Vehicle KM. Read.</td>
                <td  width="35%" align="left" class="tabletd">
        <table width="97%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="30%"><input name="vehicle_km" type="text" class='Numeric_Text' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="vehicle_km"   value='0'  maxlength="20" size="20"  >
                      </td>
             <td width="35%"  align="right">Vehicle Hrs. Read. &nbsp;</td>
                      <td width="30%" align="left"><input name="vehicle_hrs_read" type="text" class='Numeric_Text' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="vehicle_hrs_read"   value='0'  maxlength="20" size="20"  ></td>
          
                      
                    </tr>
                  </table></td>
         <td  align="right" width="15%"  class="tabletd"> Issue Date</td>
      <td  align="left"  width="35%" class="tabletd"><table width="100%">
        <tr>
          <td width="40%"><input name='issuedate' type="text" id='issuedate' class="datecss form-control"  data-inputmask="'alias': 'dd/mm/yyyy'" data-mask  value='16/08/2021'    onBlur="dtcng_new('issuedate')" />
          <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div></td>
          <td width="25%"  align="right"></td>
          <td width="30%" align="left" class="tabletd"></td>
        </tr>
        </table></td>
        
              </tr>
        
        
        <tr style="display:none">
          <td  width="15%" align="right" class="tabletd"> Battery Run Km</td>
                <td  width="35%" align="left" class="tabletd">
        <table width="97%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="30%"><input name="battery_run" type="text" class='Numeric_Text' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="battery_run"   value='0' maxlength="20" size="20" >
                      </td>
            <td width="35%"  align="right">Battery Run Hrs.  &nbsp;</td>
                      <td width="30%" align="left"><input name="battery_run_hrs" type="text" class='Numeric_Text' onFocus="SetBlank_Zero(this,'ONFOCUS')" onBlur="SetBlank_Zero(this,'LOSTFOCUS')" onKeyPress="return fn_validateNumericNew(this,event)"  id="battery_run_hrs"   value='0' maxlength="20" size="20" ></td>
                    </tr>
                  </table></td>
          <td  align="right" width="15%"  class="tabletd">&nbsp;</td>
          <td  align="left"  width="35%" class="tabletd">&nbsp;
          
          </td>
        
              </tr>
        
        
            </table>
      
      
          </div></td>
      </tr>
      <tr >
        <td   align="center" colspan="4"  class="tabletd" >&nbsp;</td>
      </tr>
      <tr >
      <tr>
        <td colspan="4" align="center">
        
         <input  type="hidden" name="grntrans_id" id="grntrans_id" value="0">
        
        
        
        <input  type="submit" name="btnsubmit" value="Save" onClick="return validate_form(this);" class="btn">
          &nbsp;
          <input type="button" name="btnclose" value="Close" onClick="close_wind();" class="btn">
        </td>
      </tr>
    </table>
  </form>
</section>
</div>                           
<!-- <style >
 .table-responsive
 {
   overflow:scroll !important ;
   overflow: -moz-hidden-unscrollable !important ;
 } 
.table-responsive::-webkit-scrollbar { width: 0 !important }
</style>-->

<style >
.overflow-x-scroll
{
  overflow-x : scroll !important ;
}
</style>

  <!-- Main Footer -->
  <script > $('.control-label').text($.trim($('.control-label').text()).toLowerCase());
  
 // $('.content-header').find('h1').text($.trim($('.content-header').find('h1').text()).toLowerCase());
  
  </script>
  <footer class="main-footer">
    <!-- To the right -->
   
    <!-- Default to the left -->
    <div class="row" >
     <div class="col-md-6 col-sm-6 col-xs-12" >
    Copyright &copy; 2011 <a href="http://nwaytech.com" target="_blank" >Nway Technologies Pvt. Ltd.</a>. All rights reserved.    <br>

    Site Best Viewed in 1366 x 768 resolution  
  
  &nbsp; 
    <a href="javascript:void(0)" onclick="return showcalxxx()">    
  <i class="fa fa-calculator" aria-hidden="true"></i>
  </a>
  
  <span style="color:#fff" >
    0.18811702728271    

  
  
  </span>
   
   
   
   
   </div>
        
      <div class="pull-right hidden-xs col-md-6 col-sm-6 col-xs-12" style="text-align:right">
       
      
        <strong >To Create Issue </strong>
        <a href="http://ims.creativewebdesigner.in" target="_blank"> Issue Mng System</a>  <br>
        <a href="#"  id="hrefPrint" onclick="Popup_print('.content')" > <i class="fa fa-fw fa-print"></i> Print</a>    &nbsp;  &nbsp;         Think ERP Think Nway
    </div>
      </div>
 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark" >
    <!-- Create the tabs --> 
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">  
        
      <form role="form"  action="/fleet/master/batterymaster.php?" method="post" > 
      <div class="input-group"> 
      <label for="exampleInputEmail1">Company - Site </label> 
                  <select name="cmbcompsite121xx" id="cmbcompsite121xx"  class="form-control leftselect select_new" style="padding: 7.5px 4px !important;margin-top: 0px !important; margin-bottom: 0px !important;z-index: 100;"/>              
            <optgroup label="SHALIMAR ROADWAYS" > <option style="color:#FFA6A6" value="1400001" selected="selected"  > 
                  HEAD OFFICE (HO - NWAY)                </option>
            <option  value="100001"   > 
                  INDORE BRANCH  (IND - NWAY)                </option>
            <option  value="1500001"   > 
                  LOHA MANDI(SCPL) (LM - NWAY)                </option>
            </optgroup><optgroup label="PITHAMPUR BOMBAY ROADWAYS" > <option  value="1700004"   > 
                  ANDHERI  (AND - PBR)                </option>
            <option  value="900004"   > 
                  ANKLESHWAR BRANCH (ANK - PBR)                </option>
            <option  value="1200004"   > 
                  KOTA BRANCH (JBT - PBR)                </option>
            <option  value="1800004"   > 
                  MUMBAI (MUM - PBR)                </option>
            <option style="color:#FFA6A6" value="1300004"   > 
                  PITHAMBUR BRANCH (PBR - PBR)                </option>
            <option style="color:#FFA6A6" value="1000004"   > 
                  PUNE BRANCH (PUN - PBR)                </option>
                          </optgroup>
            </select>    
    </div>       
        <div class="input-group"> 
        <label for="exampleInputEmail1"> Financial Year    </label> 
                    <select name="cmbfinyears121xx" id="cmbfinyears121xx"  class="form-control leftselect" style="padding: 7.5px 4px !important;margin-top: 0px !important; margin-bottom: 0px !important;z-index: 100;"    >              
                          <option value="14"    > 
                  01/04/2021-31/03/2022                </option>
                          <option value="13"  selected="selected"  > 
                  01/04/2020-31/03/2021                </option>
                          <option value="12"    > 
                  01/04/2019-31/03/2020                </option>
                          <option value="11"    > 
                  01/04/2018-31/03/2019                </option>
                          <option value="10"    > 
                  01/04/2017-31/03/2018                </option>
             
            </select>     
        </div>
        
        <div class="box-body" style="margin-bottom:-15px">
           <button type="submit"  id='btncsfupdate121xx' name="btncsfupdate121xx"  class="btn btn-primary">Change</button>
        &nbsp;&nbsp;
      
       <button type="button"  id='btncsfupdate151xx' name="btncsfupdate151xx"  onclick="window.location.href='../../home_new.php'" class="btn btn-primary"> Module </button>
       
        </div>  
         </form>
         
        <!-- /.control-sidebar-menu -->

      </div>
    
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
       
      
       
    </div>
    
      <div style="width:100%; text-align:center; color:#222d32" > 
    SHIVANI            
      </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
      <link rel="stylesheet" href="../../mis_css/css/select2.min.css?id=110">
    <script src="../../mis_css/js/select2.full.min.js?id=105"></script> 
      <script>
    $(function () { 
       $(".select_new").select_new();
         
    }); 
    </script>

</div>  

<div class="modal fade bs-example-modal-lg in" id="xxx_Iframe_popup"  style="display:none"  role="dialog"> 
 <div class="modal-content" style=" margin-left: 5%; margin-right: 5%;  margin-top: 5%; -webkit-box-shadow: 0 0 90px #000 !important;">
         <div class="modal-header">           
          <h4 class="modal-title pull-left" id= "xxx_Iframe_popup_title" > Vehicle / Machine Detail </h4>
      <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool btn-default  btn-flat" onclick="$('#xxx_Iframe_popup').hide();" ><i class="fa fa-times"></i> </button>
          </div>
      
        </div> 
      <div class="box-body"  style="height:490px"   >  
      <iframe  src="" id="xxx_Iframe_popup_popup" allowtransparency="1" frameborder="0" marginheight="0" marginwidth="0"   width="100%" style="height:480px"   ></iframe>
      
      </div>
  </div>
  </div>
  
  <div class="modal fade bs-example-modal-lg in" id="xxx_Iframe_popup_1"  style="display:none"  role="dialog"> 
 <div class="modal-content" style=" margin-left: 5%; margin-right: 5%;  margin-top: 5%; -webkit-box-shadow: 0 0 90px #000 !important;">
         <div class="modal-header">           
          <h4 class="modal-title pull-left" id= "xxx_Iframe_popup_title_1" >   </h4>
      <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool btn-default  btn-flat" onclick="$('#xxx_Iframe_popup_1').hide();" ><i class="fa fa-times"></i> </button>
          </div>
      
        </div> 
      <div class="box-body"  style="height:490px"   >  
      <iframe  src="" id="xxx_Iframe_popup_popup_1" allowtransparency="1" frameborder="0" marginheight="0" marginwidth="0"   width="100%" style="height:480px"   ></iframe>
      
      </div>
  </div>
  </div>
  
  
  <script >
  function show_Iframe_popup(src_txt,title_txt) 
  {
   $("#xxx_Iframe_popup_popup").html(''); 
     $("#xxx_Iframe_popup_popup").attr("src", src_txt);
     $('#xxx_Iframe_popup_title').html(title_txt);
   $('#xxx_Iframe_popup').show();  
  }
  function show_Iframe_popup_1(src_txt,title_txt) 
  {
   $("#xxx_Iframe_popup_popup_1").html(''); 
     $("#xxx_Iframe_popup_popup_1").attr("src", src_txt);
     $('#xxx_Iframe_popup_title_1').html(title_txt);
   $('#xxx_Iframe_popup_1').show();  
  }
  </script>
  

 <script src="../../mis_css/js/jquery.inputmask.js"></script>
<script src="../../mis_css/js/jquery.inputmask.extensions.js"></script>
<script src="../../mis_css/js/jquery.inputmask.date.extensions.js?id=12"></script> 

<script src="../../mis_css/js/bootstrap-datepicker.js?id=12"></script> 
<script src="../../mis_css/js/bootstrap.js"></script> 
<script src="../../mis_css/js/jquery.slimscroll.js?id=11"></script>
<script src="../../mis_css/js/app.js?id=1245"></script>
<script src="../../mis_css/js/demo.js?id=12"></script>

<script src="../../mis_css/js/jquery.tablesorter.js"></script>
<script src="../../mis_css/js/jquery.tablesorter.widgets.js?id=411"></script>
 <script src="../../mis_css/js/main_time.js?id=12"></script> 
<script > 


function get_DateObject(dateString)
{ 
  var dtCh= "/";  
  var cDate,cMonth,cYear; 
  dtStr = dateString ;
  var pos1=dtStr.indexOf(dtCh);
  var pos2=dtStr.indexOf(dtCh,pos1+1);
  var strDay   =dtStr.substring(0,pos1);
  var strMonth =dtStr.substring(pos1+1,pos2);
  var strYear  =dtStr.substring(pos2+1);
  //Create Date Object
  dtObject=new Date(strYear,strMonth-1,strDay); 
  return dtObject;
}

var finyear_start_date=get_DateObject("01/04/2020");
var finyear_end_date=get_DateObject("31/03/2021"); 

var js_root_dir ='http://demo.transporterp.org/' ;
var js_session_id = 'N2w5dDVsZ2Ywa20zdWZiczk0azNya3R2NzY%3D' ;  
var js_finyear_id = 'MTM%3D';
var js_comp_id = 'MQ%3D%3D';
var js_site_id = 'MTQ%3D'; 

</script> 
<script src="../../mis_css/js/enway.js?id=160"></script>   
<script>
$(document).ready(function () {

    $("input[data-mask]").inputmask();       
        
    $('.input-group-addon .fa-calendar').click(function () {   
           $(this).closest('div').prev('input').datepicker('show');  
    });   
  
 });  
</script>
<script>
  var cntrlIsPressed = false;
  
  $(document).keydown(function(event){
    if(event.which=="17")
        cntrlIsPressed = true;
     
  });
  
  $(document).keyup(function(){
    cntrlIsPressed = false;
  });
   

    $('.tablesorter tbody tr').on('click', function(event) {      
       if ($(this).hasClass( "tablesorter-childRow" ) == false )
       {    
             if(cntrlIsPressed == false)       
         {
            $('td').removeClass('highlight');          
         }
        
        $('td', this).each(function() {               
            $(this).addClass('highlight') ;         
        }); 
          
      }
  }); 
  
   
    

  $(function(){ 
    $("#tablesorter").tablesorter({ 
        theme : 'blue', 
      showProcessing: true,
      widgets: ['reorder',  'resizable',  'zebra', 'stickyHeaders' ,'filter'] ,
      sortReset      : true,
      sortRestart    : true,
      widgetOptions: { 
        resizable_addLastColumn : true,
        // include child row content while filtering, if true
        filter_childRows  : true,
        // class name applied to filter row and each input
        filter_cssFilter  : 'filtercss',
        // search from beginning
        filter_startsWith : false,
        // Set this option to false to make the searches case sensitive 
        filter_ignoreCase : true ,
        stickyHeaders_offset:50,
         

        } 
      });
  
  });   
  
  $("#tablesorter_resizable").tablesorter({    
    // initialize zebra striping and resizable widgets on the table,    
    theme : 'blue', 
    showProcessing: true,
    widgets: ['zebra', 'resizable', 'stickyHeaders','filter' ],
    sortReset      : true,
    sortRestart    : true,
    widgetOptions: {
     // include child row content while filtering, if true
          resizable_addLastColumn : true,
        filter_childRows  : true,
        // class name applied to filter row and each input
        filter_cssFilter  : 'filtercss',
        // search from beginning
        filter_startsWith : false,
        // Set this option to false to make the searches case sensitive 
        filter_ignoreCase : true
    }
  });
    
   
  
   $(document).ready(function() {  
   
     $('#scrolldiv_table').scroll(function(){ 
       $('.tablesorter-sticky-wrapper').scrollLeft($(this).scrollLeft());    
       $(window).trigger('resize'); 
     });  
     if($(window).width() < 1000)  
     { 
       $('#scrolldiv_table').css('overflow-x','scroll') ; 
     $('.tablesorter-sticky-wrapper').css('top', '95px');
     $("#scrolldiv_table").addClass("overflow-x-scroll");  
     }
   
   });
     
 
$(document).ready(function () {  
  $('form').find(':text,:radio,:checkbox,select,textarea').each(function(){
    if(!this.readOnly && !this.disabled &&   $(this).parentsUntil('form', 'div').css('display') != "none" && this.id != 'search_id121xx') {
      this.focus();  //Dom method
      return false;
    }
  });
});  
    
$('.sidebar-toggle').click(function(e) {
  $('.tablesorter-resizable-handle').css('position', 'absolute') ; 
});  

function fn_change_dept121xx()
{  
     if(document.getElementById('gdept_id121xx') != null){
    var url = '/fleet/master/batterymaster.php?dpt121xx=121xx&dptid='+ eval(document.getElementById('gdept_id121xx').value); 
   }else{
    var url = '../../home_new.php'; 
   }
     window.location.href=url;
}

function fn_change_dept121aa(gdept_id121xx)
{  
   var url = '/fleet/master/batterymaster.php?dpt121xx=121xx&dptid='+ eval(gdept_id121xx); 
     window.location.href=url;
}

$('.sidebar-menu > li > a').click(function () {    
  if ( $("body").hasClass( "sidebar-collapse" ) )
  {
    $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');  
    $(".slimScrollBar").removeClass('ScrollBarhide');          
    $(".slimScrollBar").addClass('ScrollBarshow');   
    return false;
  }  
});

$('.sidebar-toggle').click(function () {    
  if ($("body").hasClass( "sidebar-collapse" ) )
  {        
    $(".slimScrollBar").removeClass('ScrollBarhide');          
    $(".slimScrollBar").addClass('ScrollBarshow');   
  } else{ 
    $(".slimScrollBar").removeClass('ScrollBarshow');          
    $(".slimScrollBar").addClass('ScrollBarhide'); 
  }
});
     
$(document).keydown(function(event){
    if(event.which=="113") {  
        $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');     
      $("#search_id121xx").focus();
       $(".slimScrollBar").removeClass('ScrollBarhide');           
         $(".slimScrollBar").addClass('ScrollBarshow'); 
       return false;
  }
  
  if(event.which=="27") { 
       $("body").removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');    
      $(".slimScrollBar").removeClass('ScrollBarshow');          
         $(".slimScrollBar").addClass('ScrollBarhide'); 
       return false;
  }
}); 
</script>

<script>    
hideLoader();
</script>

<script > 
function Popup_print(contid)
{ 
  
   var data  = $('#my_divprn').html($(contid).html()) ; 
   
   $('#my_divprn').find('a').removeAttr('href');
  
   var data  = $('#my_divprn').html() ;
   $('#my_divprn').html('') ;   
  
  var mywindow = window.open('', 'my div', 'height=500,width=1000');
  mywindow.document.write('<html><head><title></title>');   
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/style.css?id=101">');
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/bootstrap.css">');
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/font-awesome.css">');
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/ionicons.css">');
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/AdminLTE.css">');
  mywindow.document.write('<link rel="stylesheet" href="../../mis_css/css/skins/_all-skins.min.css">');
  mywindow.document.write('<link href="../../mis_css/css/theme.blue.css?id=102" rel="stylesheet">');
  mywindow.document.write('<style > .tablesorter-resizable-container{ display:none} .tablesorter-sticky-visible{ display:none} </style>');
  mywindow.document.write('<scr'+'ipt type="text/javascript" src="../../mis_css/js/jquery-2.2.0.js"></scr'+'ipt>');
   
 

  mywindow.document.write('<scr'+'ipt>function exportToExcel(){ var link = document.createElement("a"); link.download = "exportdata.xls"; link.href = \'data:application/vnd.ms-excel;base64,\'+ window.btoa(unescape(encodeURIComponent($(\'#pdf_print_data\').html()))) ;document.body.appendChild(link);  link.click(); } </scr'+'ipt>');  
  
  mywindow.document.write('</head><body >'); 
  mywindow.document.write('<div id="pdf_print_data">'+data+'</div>');
  mywindow.document.write(' <button id="cmd" class="btn btn-primary" onclick="exportToExcel();"  >Excel</button>   </body></html>');  
  mywindow.document.close();   
    
  setTimeout(function(){
    mywindow.print();           
  },1000);
 

 
  
}
//develop by rajat
 function exportTableToExcel(tableID , buttonid, filename){
 
    //var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
  if(typeof filename ==='undefined') filename='list';
  
  $tableHTML = $('#'+tableID).clone();
  
    $tableHTML.find('[style*="display:none"]').remove();
 
  tableHTML = $tableHTML.html();
 
  
  tableHTML = tableHTML.replace(/(\r\n|\n|\r)/gm, ""); //remove \n in string
  tableHTML = tableHTML.replace(/<A[^>]*>|<\/A>|onclick[^>]*"/gi, ""); //remove if u want links in your table
    //tableHTML = tableHTML.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    //tableHTML = tableHTML.replace(/<input[^>]*>|<\/input>/gi, ""); 
  tableHTML = tableHTML.replace(/>[\n\t ]+</gi, "><");
  //console.log(tableHTML);return false;
    var fileName = filename+'.xls';
    var blob = new Blob([ tableHTML ], {
    type : "application/csv;charset=utf-8;"
  });

  if (window.navigator.msSaveBlob) {
    // FOR IE BROWSER
    navigator.msSaveBlob(blob, fileName);
  } else {
    // FOR OTHER BROWSERS
    var csvUrl = URL.createObjectURL(blob);
    $('#'+buttonid).attr({
      'download': fileName,
      'href': csvUrl
    });
  }
}
 
function close_wind()
{
  document.location.replace('../../fleet/master/dashboard.php');  

}


 
</script>

<script type="text/javascript">
function RowScroll(tblcnt){
  var trows = document.getElementById(tblcnt).rows, t = trows.length, trow, nextrow,
  addEvent = (function(){return window.addEventListener? function(el, ev, f){
      el.addEventListener(ev, f, false); //modern browsers
    }:window.attachEvent? function(el, ev, f){
      el.attachEvent('on' + ev, function(e){f.apply(el, [e]);}); //IE 8 and less
    }:function(){return;}; //a very old browser (IE 4 or less, or Mozilla, others, before Netscape 6), so let's skip those
  })();

  while (--t > -1) {
     trow = trows[t];
     $(trow).removeClass("highlight");
     //trow.className = '';
     addEvent(trow, 'click', highlightRow);
  }//end while

  function highlightRow(gethighlight) { //now dual use - either set or get the highlight row
    gethighlight = gethighlight === true;
    var t = trows.length;
    while (--t > -1) {
      trow = trows[t];
       
      if(gethighlight &&  $(trow).hasClass('highlight')){return t;}
      else if (!gethighlight && trow !== this) {        
          //trow.className = '';  
        $(trow).removeClass('highlight') ;    
          $(trow).find('td').each(function() {  
           //this.className =  'highlight' ;
           $(this).removeClass('highlight') ;
        });  
          
        //trow.className = ''; 
      
      }
    }//end while
     
      
     
    $(this).addClass('highlight') ;
    $(this).find('td').each(function() {  
      $(this).addClass('highlight') ;
    });
    
    
    return 'highlight' ; 
    
  }//end function

  function movehighlight(way, e){
    e.preventDefault && e.preventDefault();
    e.returnValue = false;
    var idx = highlightRow(true); //gets current index or null if none highlight
    if(typeof idx === 'number'){//there was a highlight row
      idx += way; //increment\decrement the index value
      if(idx && (nextrow = trows[idx])){ 
          
        var rh = $(nextrow).height()  ;
        scrw_2 =scrw  ;       
        window.scrollTo(scrw, scrw_2); 
        scrw = scrw + (way*rh)  ;
        scrw_2 =scrw + 2 * ((way*rh) ) ;  
        return highlightRow.apply(nextrow); } //index is > 0 and a row exists at that index
      else if(idx){ 
           scrw =   0
         scrw_2 = 0 ;
         window.scrollTo(scrw, scrw_2);
             
          return highlightRow.apply(trows[1]); 
      
      } //index is out of range high, go to first row
      
      scrw =    $(document).height() - 500 ;
      scrw_2 =  scrw ;
      window.scrollTo(scrw, scrw_2);

       
      return highlightRow.apply(trows[trows.length - 1]); //index is out of range low, go to last row
    }
    
    scrw =  $(window).height()/2 ;     
    window.scrollTo(scrw, scrw_2);
     
    return highlightRow.apply(trows[way > 0? 1 : trows.length - 1]); //none was highlight - go to 1st if down arrow, last if up arrow
  }//end function


    var scrw = 0 ; 

  function processkey(e){
       
    switch(e.keyCode){
      case 38: {//up arrow
        return movehighlight(-1, e)
      }
      case 40: {//down arrow
        return movehighlight(1, e);
      }
      default: {
        return true;
      }
    }
  }//end function
  
  var el = document.getElementById(tblcnt);
  el.addEventListener('click', processmouse, true);
  
  function processmouse(e){
       scrw =  $(window).scrollTop() ;    
     
  }//end function

  addEvent(document, 'keydown', processkey);
     
  

} 

</script>
 
<div id="my_divprn" style="display:none" ></div>

 

<div  id='mycalxxx' style="position:fixed;bottom:50px;left:0px;width:100%;z-index: 99999;display:none">

<script >
function GetCaretPosition(ctrl){
var CaretPos = 0;
if (document.selection){
ctrl.focus ();
var Sel = document.selection.createRange ();
Sel.moveStart ('character', -ctrl.value.length);
CaretPos = Sel.text.length;
}else if (ctrl.selectionStart || ctrl.selectionStart == '0')
CaretPos = ctrl.selectionStart;
return (CaretPos);
}


function SetCaretPosition(ctrl, pos){
if(ctrl.setSelectionRange){
ctrl.focus();
ctrl.setSelectionRange(pos,pos);
}else if (ctrl.createTextRange){
var range = ctrl.createTextRange();
range.collapse(true);
range.moveEnd('character', pos);
range.moveStart('character', pos);
range.select();
}
}

(function($){
$.fn.jsRapCalculator = function(options){
var defaults = {
showMode:true,
showBitwise:false,
showHistory:true,
maximumFractionDigits:20
}

function AddButton(base,d,t,c,i){
var o = $('<button class="calc-button ' + c + '" title="' + i + '">' + t + '</button>').appendTo(d);
$(o).bind({
click : function(e){
var k = $(this).text();
var t = $(base.divin).val();
var e = $.Event('keyup');
switch(k){
case '=': e.keyCode = 13;break;
case 'DEL': t = t.substring(0,t.length - 1);$(base.divin).val(t);break;
case 'AC': $(base.divin).val('');$(base.divre).text('0');break;
case 'DEC': base.SetMode(10,k);break;
case 'HEX': base.SetMode(16,k);break;
case 'BIN': base.SetMode(2,k);break;
case 'OCT': base.SetMode(8,k);break;
default:
switch(k){
case '�': k = '/';break;
case '�': k = '*';break;
case 'p': k = 'PI';break;
case 'e': k = 'E';break;
case 'EXP': k = 'e';break;
case 'MOD': k = '%';break;
case 'OR': k = '|';break;
case 'AND': k = '&';break;
case 'XOR': k = '^';break;
case 'NOT': k = '~';break;
case 'SHL': k = '<<';break;
case 'SHR': k = '>>>';break;
}
var cp = GetCaretPosition(base.divin[0]);
var s = t.substring(0,cp) + k + t.substring(cp, t.length);
$(base.divin).val(s);
SetCaretPosition(base.divin[0],cp + k.length);
}
$(base.divin).trigger(e);
}
});
return o;
}

return this.each(function(){
this.SetMode = function(m,n){
this.mode = m;
this.modeName = n;
$('.calc-mode2').removeClass('calc-active');
$('.calc-mode8').removeClass('calc-active');
$('.calc-mode10').removeClass('calc-active');
$('.calc-mode16').removeClass('calc-active');
$('.calc-mode' + m).addClass('calc-active');
}
this.settings = $.extend(defaults,options);
$(this).addClass('calc-main');
this.divre = $('<div>').addClass('calc-edit calc-display').text('0').appendTo(this);
if(this.settings.showMode){
var d = $('<div>').appendTo(this);
AddButton(this,d,'HEX','calc-button-green calc-mode16','Mode Hexadecimal');
AddButton(this,d,'DEC','calc-button-green calc-mode10 calc-active','Mode Decimal');
AddButton(this,d,'OCT','calc-button-green calc-mode8','Mode Octal');
AddButton(this,d,'BIN','calc-button-green calc-mode2','Mode Binary');
}
var d = $('<div>').appendTo(this);
AddButton(this,d,'0','calc-button-black','');
AddButton(this,d,'1','calc-button-black','');
AddButton(this,d,'2','calc-button-black','');
AddButton(this,d,'3','calc-button-black','');
AddButton(this,d,'4','calc-button-black','');
AddButton(this,d,'5','calc-button-black','');
AddButton(this,d,'6','calc-button-black','');
AddButton(this,d,'7','calc-button-black','');
AddButton(this,d,'8','calc-button-black','');
AddButton(this,d,'9','calc-button-black','');
AddButton(this,d,'.','calc-button-black','');
if(this.settings.showBitwise){
var d = $('<div>').appendTo(this);
AddButton(this,d,'EXP','calc-button-blue','Exponentiation');
AddButton(this,d,'MOD','calc-button-blue','Division Remainder');
AddButton(this,d,'OR','calc-button-blue','Bitwise OR');
AddButton(this,d,'AND','calc-button-blue','Bitwise AND');
AddButton(this,d,'XOR','calc-button-blue','Bitwise XOR');
AddButton(this,d,'NOT','calc-button-blue','Bitwise NOT');
AddButton(this,d,'SHL','calc-button-blue','Zero fill left shift');
AddButton(this,d,'SHR','calc-button-blue','Zero fill right shift');
AddButton(this,d,'&pi;','calc-button-purple','PI');
AddButton(this,d,'e','calc-button-purple','Euler\'s number');
}
var d = $('<div>').appendTo(this);
AddButton(this,d,'+','calc-button-brown','');
AddButton(this,d,'-','calc-button-brown','');
AddButton(this,d,'&times;','calc-button-brown','');
AddButton(this,d,'&divide;','calc-button-brown','');
this.butOpen = AddButton(this,d,'(','calc-button-brown calc-open','');
this.butClose = AddButton(this,d,')','calc-button-brown calc-close','');
if(this.settings.showHistory)
this.butEqual = AddButton(this,d,'=','calc-button-brown calc-equal','');
var d = $('<div>').appendTo(this);
AddButton(this,d,'AC','calc-button-orange','All Clear');
AddButton(this,d,'DEL','calc-button-orange','Delete');
this.divin = $('<input>').addClass('calc-edit calc-input').appendTo(this);
if(this.settings.showHistory)
this.divHistory = $('<div>').addClass('calc-edit calc-history').appendTo(this);
var base = this;
$(this.divin).bind({
keyup : function(e){
base.Calculate(e);
}
});

this.Calculate=function(e){
base.divin.focus();
var c = $(base.divin).val();
var bo = (c.match(/\(/g) || []).length;
var bc = (c.match(/\)/g) || []).length;
if(bo > bc) $(base.butClose).addClass('calc-active');else $(base.butClose).removeClass('calc-active');
if(bo < bc) $(base.butOpen).addClass('calc-active');else $(base.butOpen).removeClass('calc-active');
c = c.replace(new RegExp(' ','g'),'');
var c2 = c;
c2 = c2.replace('E','Math.E');
c2 = c2.replace('PI','Math.PI');
c2 = c2.replace('asin','Math.asin');
c2 = c2.replace('atan','Math.atan');
c2 = c2.replace('cos','Math.cos');
c2 = c2.replace('exp','Math.exp');
c2 = c2.replace('log','Math.log');
c2 = c2.replace('pow','Math.pow');
c2 = c2.replace('sin','Math.sin');
c2 = c2.replace('tan','Math.tan');
if(base.butEqual)
$(base.butEqual).removeClass('calc-active');
$(base.divre).text(0);
try{
var v = eval(c2);
}
catch(err){
return;
}
if(isNaN(v))return;
if(base.butEqual)
$(base.butEqual).addClass('calc-active');
if(base.mode != 10)
v = v.toString(base.mode);
else
v = v.toLocaleString(undefined,{maximumFractionDigits:base.settings.maximumFractionDigits});
$(base.divre).text(v);
if(this.divHistory && e && e.keyCode == 13){
var d = $(base.divHistory);
d.append(c + ' ' + this.modeName + '<br>' + v + '<br>');
d.scrollTop(d[0].scrollHeight - d[0].clientHeight);
}
}

this.SetMode(10,'DEC');
})
}})(jQuery);
</script>
<style>
 

.calc-edit{
background: #eed ;
border-radius:5px;
/*box-shadow:inset -2px -2px 2px rgba(255,255,255,0.5),inset 2px 2px 2px rgba(0,0,0,0.5);*/
color:#333;
}
.calc-history,.calc-input{
font-family:Courier New, Courier New, monospace;
font-size:large;
font-weight:bold;
box-sizing:border-box;
display:block;
margin:0px 0px 0px 5px;
padding:8px;
width:100%;
padding-left:5px;
}
.calc-history{
text-align:right;
height:240px;
overflow:auto;
}
.calc-main{
display: block;
    padding: 5px 0 5px 0;
    font: bold 15px/28px Arial,Helvetica,sans-serif;
    /*border: 1px solid #888;
     border-radius: 12px; */
   background: radial-gradient(#222d32,#222d32,#222d32);
    /* box-shadow: inset -8px -8px 4px rgba(0,0,0,0.4), inset 8px 8px 4px rgba(255,255,255,0.4), 8px 8px 4px rgba(0,0,0,0.4); */
}
.calc-button{
cursor:pointer;
width:50px;
height:28px;
color:#eee;
margin:5px 4px;
border:1px solid black;
box-shadow:0 1px rgba(255,255,255,0.1);
border-radius:6px;
box-shadow:inset 0 -15px 1px rgba(0,0,0,0.5),inset 0 1px 1px rgba(255,255,255,0.5),inset 0 8px 8px rgba(255,255,255,0.5);
font:bold 15px/28px Arial,Helvetica,sans-serif;
}
.calc-button:hover{
color:#fff;
text-shadow:0px 0px 8px rgb(255,230,186),0px 0px 22px rgb(255,255,255),0px -1px 0px rgba(0,0,0,0.5);
}
.calc-button-black{
background-color: #222;
background:radial-gradient(#444,#222,#000);
}
.calc-button-blue{
background-color: #004;
background:radial-gradient(#008,#004,#002);
}
.calc-button-green{
background-color: #040;
background:radial-gradient(#080,#040,#020);
}
.calc-button-brown{
background-color: #322;
background:radial-gradient(#433,#322,#211);
}
.calc-button-orange{
color: black;
background-color: #d82;
background:radial-gradient(#fa4,#d82,#b60);
}
.calc-button-purple{
background-color: #704;
background:radial-gradient(#a06,#704,#402);
}
.calc-active{
color:yellow;
border-color:yellow;
}
.calc-display{
font:bold 15px/28px Arial,Helvetica,sans-serif;
font-size:large;
height: 40px;
overflow-x:auto;
overflow-y:hidden;
padding: 0 1% 0 0;
line-height:40px;
text-align:right;
white-space:nowrap;
cursor:default;
text-shadow:0 -1px 0 rgba(0,0,0,0.01);
}

.container {width: 100%; }
.calc-button{display:none !important}
.calc-main{position: absolute; width: 100%;}
.calc-display {float: left;   width: 25%;}
.calc-input {float: left;  width: 70%;height: 40px;}


</style>
<script>
$(document).ready(function(){
$('#demo2xxx').jsRapCalculator({showMode:false,showLogic:false,showHistory:false,name:'name2'});


$('.calc-input').keydown(function(event){
    if(event.which=="27")
  {
    $('#mycalxxx').hide();
  }
   
});



});

function showcalxxx()
{
  $('#mycalxxx').show();
      
  
  return false ;
}
function hidecalxxx()
{
  $('#mycalxxx').hide();
  return false ;
}
</script>

<div id="demo2xxx"  >

 <a href="javascript:void(0)" onclick="return hidecalxxx()" style="    position: absolute;   right: 5px;    color: #fff;    font-size: 35px;    top: 10px;" >    
   <i class="fa fa-times-circle"></i>

    </a>
</div>

</div>


</body>
</html> 

<script language="javascript" >
function fn_invehicle()
{
  var ChkBox = document.myform.is_in_vehicle.checked;
  if(ChkBox==true)
  {
    document.getElementById('div_invehicle').style.display = 'block';
  }
  else
  {   document.getElementById('div_invehicle').style.display = 'none'; }
}

fn_invehicle()
</script>
<script language="javascript" >
function dtcng_new(edate)
{
  var cashdate     = document.getElementById(edate).value ;
  if(check_date(cashdate) == false )  { return false } 
}
</script>
<script >
var machine_jsonD_arr ;
function fn_fill_machine(cnf)
{  
   if(typeof cnf === 'undefined'  ) { cnf = 0 ;   }
   if(cnf == 0)
   {
    $('#cmbmachine').val(0);
    $('#machine_no').val(''); 
   }
   var mach_cat_id = $('#machinecategory').val();
   machine_jsonD_arr = get_machine_data('machine_no','cmbmachine', '' , '' ,0,mach_cat_id) ;
}
 fn_fill_machine(1) ;
</script>
<link href="../../includes/jquery/select2.min.css" rel="stylesheet" />
<script src="../../includes/jquery/select2.min.js"></script>
<script>$('.select-8-hidden-accessible').select2({placeholder: "Please select",}); </script> 
<style >
.select2-results, .select2-search {
    width: 300px !important;
}
.select2-container--default .select2-results__option[aria-disabled=true] {
   color: #060 !important;
   font-weight:bold !important;
}
</style>
<script>

$(document).ready(function()
{ 
  $('.popover').popover('show'); 
  $('.popover-markup').popover({ 
    html      : true,
    trigger   : 'click',
    placement : 'top',
    delay     : {show: 100, hide: 10000} , 
    content   : function()
    {
      return $(this).parent().find('.content').html();  
    }
  }); 
  $('#link_acc_edit_0').css('pointer-events' , 'none');
  $('#txt_account_name').blur(function(){
    if($('#txt_account_name').val() == "")
    {
      document.myform.vendor_id.value="0"; 
      $('#link_acc_edit_0').css('pointer-events' , 'none');
    }else{ 
      var acc_id = document.myform.vendor_id.value ;  
      $('#link_acc_edit_0').css('pointer-events' , ''); 
      document.getElementById('link_acc_edit_0').href = '../../accountadmin/master/account_upd.php?mode=EDIT&account_id='+acc_id ;  
    } 
    $('.popover').popover('hide'); 
  });  
});


</script>