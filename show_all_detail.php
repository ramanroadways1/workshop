

<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="pay_invoice.php" style="font-size:18px;" autocomplete="off">
    <div class="content-wrapper">
      <!-- <section class="content-header">
        <h1>Show All Details</h1>
      </section> -->
      <section class="content">
       
        <div class="box box-default">
          <h3 style="margin-left: 10px;">Payment Details</h3>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

        <?php
        include 'connect.php';
        $valueToSearch=$_POST['grn_no'];
        $username =  $_SESSION['username'];  
              
        $show = "SELECT * FROM files_upload where grn_no=$valueToSearch and username='$username'";
        $result = mysqli_query($conn,$show);
        if(mysqli_num_rows($result) > 0){

           while($row = mysqli_fetch_array($result))
           {
            $id = $row['id'];
            $grn_no = $row['grn_no'];
            $purchase_order = $row['purchase_order'];
            $party_name = $row['party_name'];
            $productno = $row['productno'];
            /*$challan_file = $row['challan_file'];*/
            $invoice_file = $row['invoice_file'];
            $invoice_no = $row['invoice_no'];
            $new_total = $row['new_total'];
            $remain = $row['remain'];
            /*$challan_file1=explode(",",$challan_file);
            $count3=count($challan_file1);*/
            $invoice_file1=explode(",",$invoice_file);
            $count4=count($invoice_file1);
        ?>
         <input type="hidden" name="username" value="<?php echo $username ?>">
            <div class="col-md-4">
              <div class="form-group">
                <label>GRN No:</label>
                <?php echo $grn_no;?><input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'/>
              </div>
            <!--   <div class="form-group" style="display: none;">
                <label>ID:</label>
                <?php echo $id;?><input type="hidden" name="id[]" value='<?php echo $id; ?>' />
              </div>
              <div class="form-group" style="display: none;">
                <label>Product No:</label>
                <?php echo $productno;?><input type="hidden" name="productno[]" value='<?php echo $productno; ?>' />
              </div> -->
              <div class="form-group">
                <label>Party Name:</label>
                <?php echo $party_name;?><input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>' />
              </div>
            </div>
            <div class="col-md-4">
              
              <div class="form-group">
                <label>Purchase Order:</label>
                <?php echo $purchase_order;?><input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'/>
              </div>
              <div class="form-group">
                <label>Total Amount:</label>
                <?php echo $new_total;?><input type="hidden" id="new_total" name="new_total[]" value='<?php echo $new_total; ?>' />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Invoice No:</label>
                <?php echo $invoice_no;?><input type="hidden" name="invoice_no[]" value='<?php echo $invoice_no; ?>' />
              </div>
              <div class="form-group">
                <label>Invoice File:</label>
                <!-- <label>Invoice File:</label>
                <a href="<?php echo $invoice_file; ?>" target="_blank"><?php echo $invoice_file; ?></a>
                <input type="hidden" name="invoice_file[]" value='<?php echo $invoice_file; ?>'> -->
                <?php
                 for($j=0; $j<$count4; $j++){
                   echo "<br>";
                  ?>
                   <a href="<?php echo $invoice_file1[$j]; ?>" target="_blank">Invoice <?php echo $j+1; ?></a>
                   <input type="hidden" name="invoice_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $invoice_file; ?>'>
                   <?php
                 }

                ?>
              </div>
            
          <?php 
          } } else{
                  echo "<SCRIPT>
                 
                  window.location.href='grn_for_payment.php';
                </SCRIPT>";
                exit();
          }
          ?>
          <!-- <h3 style="margin-left: 15px;">Party Details</h3><br> -->
          <?php
            include 'connect.php';
            $show2 = "SELECT * FROM party where party_name='$party_name' and username='$username'";
            $result2 = mysqli_query($conn,$show2);
             while($row = mysqli_fetch_array($result2))
             {
                $id = $row['id'];
                $party_name = $row['party_name'];
                $party_code = $row['party_code'];
                $mobile_no = $row['mobile_no'];
                $pan = $row['pan'];
                $gstin = $row['gstin'];
                $acc_no = $row['acc_no'];
                $ifsc_code = $row['ifsc_code'];
              ?>
            </div>
            
            <!-- <h3 style="float: left; margin: 18px;">Account Detail</h3><br><br><br><br><br><br><br><br> -->


            
            <div class="col-md-4">
              
              <div class="form-group">
                <label>IFSC Code:</label>
                <?php echo $ifsc_code;?><input type="hidden" name="ifsc_code[]" value='<?php echo $ifsc_code; ?>' />
              </div>
              <div class="form-group">
                <label>Account No:</label>
                <?php echo $acc_no;?><input type="hidden" name="acc_no[]" value='<?php echo $acc_no; ?>'/>
              </div>
              
            </div>
            
            <div class="col-md-4">
              <div class="form-group">
                <label>PAN No:</label>
                <?php echo $pan;?><input type="hidden" name="pan[]" value='<?php echo $pan; ?>' />
              </div>
              <div class="form-group">
                <label>GSTIN No:</label>
                <?php echo $gstin;?><input type="hidden" name="gstin[]" value='<?php echo $gstin; ?>' />
              </div>
            </div>
            <div class="col-md-4">
             <div class="form-group">
                <label>Mobile No:</label>
                <?php echo $mobile_no;?><input type="hidden" name="mobile_no[]" value='<?php echo $mobile_no; ?>'/>
              </div>
              <div class="form-group">
                <label>Remain Amount:</label>
                <?php echo $remain;?><input type="hidden" id="remain" name="remain[]" value='<?php echo $remain;?>'/>
                <?php
                /*include("connect.php");

                  $remain1 = mysqli_query($conn,"SELECT remain FROM remain_pay where productno = '$productno'");
                  $count = mysqli_num_rows($remain1);
                  if($count > 0){
                  while($row = mysqli_fetch_array($remain1))
                    {
                      $remain= $row['remain'];
                      echo $remain;*/
                      ?>
                      <!-- <input type="hidden" id="remain" name="remain[]" value='<?php echo $remain; ?>'/>
                      <input type="hidden" id="new_total" name="new_total[]" value='<?php echo ""; ?>'/> -->
                      <?php
                    /*}
                  }
                  else{
                      echo $new_total;*/
                 ?>
                      <!-- <input type="hidden" id="new_total" name="new_total[]" value='<?php echo $new_total; ?>'/>
                      <input type="hidden" id="remain"  value='<?php echo "" ?>'> -->
                      <?php
                    /*}*/
                  ?>
                </div>
              </div>
             <div class="col-md-4">
               <div class="form-group">
                 <label>Enter Amount For Pay:</label>
                 <input type="number" id="payment" name="payment[]" oninput="challan_remain();" placeholder="Pay Amount" required/>
               </div>
             </div>
              
               <div class="col-md-6">
               <div class="form-group">
               <br>
                 <a href="grn_for_payment.php" style="float: right;" class="btn btn-warning">Back</a>
               </div>
             </div>
             <script>
                 function challan_remain(val)
                 {
                    var payment = Number($('#payment').val());
                    var new_total = Number($('#new_total').val());
                    var remain = Number($('#remain').val());
                     if (remain==new_total)
                     {
                       if(payment>new_total)
                         {
                           alert('You can not exceed new_total is '+ new_total);
                           $('#payment').val('');
                         }
                     }
                     else
                     {
                        if(payment>remain)
                         {
                           alert('You can not exceed Remain is '+ remain);
                           $('#payment').val('');
                         }
                      }
                  }
               </script>
            
              <?php 
                } 
              ?>
              </div>
            </div>
          </div>
          <center>
            <button type="submit" name="submit"color="Primary" class="btn btn-primary">Proceed to Pay</button>
          </center>
        </section>
      </div>
    </form>
  </div>
  
      
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2019 <a href="">RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
