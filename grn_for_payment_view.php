<!DOCTYPE html>
<html>
<head>
 <?php 
    include("header.php");
    include("aside_main.php");
  ?>
     <link href="assets/font-awesome.min.css" rel="stylesheet">
      <script src="assets/jquery-3.5.1.js"></script>
      <script src="assets/jquery-ui.min.js"></script>
      <link href="assets/jquery-ui.css" rel="stylesheet"/>
      <link href="assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="assets/custom.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
              <?php $username =  $_SESSION['username'];  ?>
            <input type="hidden" name="username" value="<?php echo $username ?>">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                                      <th>View</th>
                                      <th>Purchase Order</th>
                                      <th>Party Name</th>
                                      <th>Product Count</th>
                                      <th>Challan Bill</th>
                                      <th>Invoice Bill</th>
                                      <th>Total Amount</th>
                                      <th >GRN Date</th>
                                      <th >Payment Date</th>
                                      <th>Action</th>
              
                            </tr>
                         </thead>
                        
                      </table>
                      <div id="dataModal" class="modal fade">
                         <div class="modal-dialog modal-lg">
                            <div class="modal-content" id="employee_detail">
                            </div>
                         </div>
                      </div>
                     


                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "grn_for_payment_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV' 
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                              "criteria": [
                                  {
                                      "data": ' VEHICLE ',
                                      "condition": '=',
                                      "value": ['']
                                  }
                              ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=assets/loading.gif height=25> </center>"
                          },
                         "aaSorting": [],
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          // $('title').html("Raman Roadways Pvt Ltd - Vendor Management");

                          

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.

    </footer>

  <div class="control-sidebar-bg"></div>
</div>

       <script src="assets/bootstrap.js"></script>

        <script src="assets/bootstrap.bundle.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="assets/jszip.min.js"></script>
        <script type="text/javascript" src="assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="assets/dataTables.searchBuilder.min.js"></script>

  </footer>
</section>
</div>
</body>
<script>
  
                   function EditModal(id)
                              {
                                 var grn_no = id;
                                 var username = '<?php echo $_SESSION['username'] ?>'
                                 //alert(username);
                                jQuery.ajax({
                                    url: "fetch_grn_for_payment.php",
                                    data: 'username=' + username+ '&grn_no='+grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main").html(data);
                                    }, 
                                        error: function() {}
                                    });
                                document.getElementById("modal_button1").click();
                                $('#ModalId').val(id);

                                }
                </script><div id="result_main"></div>
                 <script type="text/javascript">
                   function Editdate(id)
                              {
                                 var grn_no = id;
                                 var username = '<?php echo $_SESSION['username'] ?>'
                                 //alert(grn_no);
                                jQuery.ajax({
                                    url: "fetch_update_date.php",
                                    data: 'username=' + username+ '&grn_no='+grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main1").html(data);
                                    }, 
                                        error: function() {}
                                    });
                                document.getElementById("modal_button5").click();
                                $('#ModalId5').val(id);

                                }
                </script>
</html>

<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 <script>
                      $(document).ready(function() {
                        $("#datepicker").datepicker({
                           dateFormat: "yy-mm-dd",
                           maxDate: '90',
                            minDate: '0'
                        });
                      });


                      $(document).ready(function() {
                        $("#datepicker2").datepicker({
                           dateFormat: "yy-mm-dd",
                           maxDate: '90',
                            minDate: '0'
                        });
                      });



                       /* $( function() {
                          $( "#datepicker" ).datepicker();

                        } );*/
                    </script>

  <div id="result_main1"></div>

 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 900px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Detail</h4>
      </div>
      
      <div class="modal-body">
        <form action="insert_payment_Approve.php" method="post" id="FormGRNUpdate">
          <div class="row">
             <input type="hidden" id="ModalId" name="id"/>
             <div class="col-md-10">
              <div class="form-group">
                <input type="hidden" name="username" value="<?php echo $username ?>">         
              </div>
          
             
            </div>

        <div class="col-md-4">
              <div class="form-group">
                <label>GRN No:</label>
               <input type="text" id="grn_no1" readonly="readonly" class="form-control" name="grn_no"/>
              </div>
          
              <div class="form-group">
                <label>Party Name:</label>
                <input type="text" id="party_name1"  readonly="readonly" class="form-control" name="party_name"  />
            </div>
            <div class="form-group">
                <label>Purchase Order:</label>
              <input type="text" id="purchase_order1" readonly="readonly"  class="form-control" name="purchase_order">
              </div>
              <div class="form-group">
                <label>Bank Name:</label><br>
              <textarea id="bank" readonly="readonly"  class="form-control" name="bank_name">
            
              </textarea>
              </div>

               <div class="form-group">
                <label>Acc Holder:</label>
                <textarea id="acc_holder_name" readonly="readonly"  class="form-control" name="acc_holder_name">
              </textarea>
              </div>

              <div class="form-group">
                <label>Email:</label>
                <input type="text" id="email" readonly="readonly"  class="form-control" name="email">
              </div>

          </div>
            <div class="col-md-4">
               <div class="form-group">
                <label>Invoice No:</label>
               <input type="text" id="invoice_no1" readonly="readonly" class="form-control" name="invoice_no" >
              </div>
               <div class="form-group">
                <label>Mobile No:</label>
               <input type="text" id="mobile_no" readonly="readonly" class="form-control" name="mobile_no" >
              </div>
              <div class="form-group">
                <label>Pan:</label>
               <input type="text" id="pan" readonly="readonly" class="form-control" name="pan"  />
              </div>

            </div>

             <div class="col-md-4">

              <div class="form-group">
                <label>gstin No:</label>
               <input type="text" id="gstin" readonly="readonly"  class="form-control" name="gstin" >
              </div>
              
               <div class="form-group">
                <label>Acount No:</label>
               <input type="text" id="acc_no" readonly="readonly"  class="form-control" name="acc_no" >
              </div>
              <div class="form-group">
                <label>IFSC Code:</label>
               <input type="text" id="ifsc_code" readonly="readonly" class="form-control" name="ifsc_code"  />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>GRN Amount:</label>
               <input type="text" id="new_total1"  readonly="readonly" class="form-control" name="new_total" />
              </div>
            </div>

            
            <div class="col-md-4">
              <div class="form-group">
                <label>Total(After deduction):</label>
               <input type="text" readonly="readonly" id="remain" class="form-control" name="remain"  />
              </div>
            </div>

           <div class="col-md-4"><br>
              <div class="form-group">
                <label>Other(Deduct Amount):</label>
               <input type="Number" id="other" required="required" oninput="sum1();" autocomplete="off" class="form-control" name="other_amount" min="0" />
              </div>
               <div class="form-group">
                <label>Payment Due Date:</label><br>
              <input type="text" id="datepicker" required="required" autocomplete="off" class="form-control" name="payment_date" >
              </div>
            </div>
            <div class="col-md-4"><br>
              <div class="form-group">
                <label>Remark:</label>
               <input type="text" id="remark" required="required"  autocomplete="off" class="form-control" name="remark"  />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Company:</label>
                <select onchange="SearchBy(this.value)" required="required" class="form-control office_type"  id="company" name="company">
                    <option value="">--Company--</option>
                     <option value="rrpl">RRPL</option>
                     <option value="rr">RR</option>
                    </select>
              </div>

            </div>
            
              <script>
              function sum1()
              {
                 var new_total1 = Number($('#new_total1').val());
               //var remain_amount = Number($('#remain').val());  
               var other_amount = Number($('#other').val()); 
                 
                 if (other_amount > 0) {
                  $("#remark").attr('readonly',false); 
                 }if (other_amount == 0) {
                   $("#remark").attr('readonly',true);
                   $("#remark").val(''); 
                 }


                if(other_amount>=new_total1) 
                   {
                         alert('You can not exceed  or same Amount '+ new_total1);
                         $('#other').val('');
                          $('#remain').val('');
                    }

              
              var final_amount = new_total1-other_amount;
               $('#remain').val((final_amount).toFixed(2));
              
              }
              </script>


            
           </div>

          </div>
         
         
       <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
        </form>
    </div>
  
    </div>
  
</div>
</div>
<button type="button" id="modal_button5" data-toggle="modal" data-target="#myModal23" style="display:none">DEMO</button>
<div id="myModal23" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 900px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Detail</h4>
      </div>
      
      <div class="modal-body">
        <form action="update_date_payment_Approve.php" method="post" id="FormGRNUpdate">
          <div class="row">
             <input type="hidden" id="ModalId5" name="id"/>
             <div class="col-md-10">
              <div class="form-group">
           <input type="hidden" name="username" value="<?php echo $username ?>">
              </div>
             
            </div>

        <div class="col-md-4">
              <div class="form-group">
                <label>GRN No:</label>
               <input type="text" id="grn_no2" readonly="readonly" class="form-control" name="grn_no"/>
              </div>
          
              <div class="form-group">
                <label>Party Name:</label>
                <input type="text" id="party_name2"  readonly="readonly" class="form-control" name="party_name"  />
            </div>
            <div class="form-group">
                <label>Purchase Order:</label>
              <input type="text" id="purchase_order2" readonly="readonly"  class="form-control" name="purchase_order">
              </div>
              <div class="form-group">
                <label>Bank Name:</label><br>
              <textarea id="bank2" readonly="readonly"  class="form-control" name="bank_name">
            
              </textarea>
              </div>

              <div class="form-group">
                <label>Payment Due Date:</label><br>
              <input type="text" id="datepicker2" required="required" autocomplete="off" class="form-control" name="payment_date" >
              </div>
          </div>
            <div class="col-md-4">
               <div class="form-group">
                <label>Invoice No:</label>
               <input type="text" id="invoice_no2" readonly="readonly" class="form-control" name="invoice_no" >
              </div>
               <div class="form-group">
                <label>Mobile No:</label>
               <input type="text" id="mobile_no2" readonly="readonly" class="form-control" name="mobile_no" >
              </div>
              <div class="form-group">
                <label>Pan:</label>
               <input type="text" id="pan2" readonly="readonly" class="form-control" name="pan"  />
              </div>

            </div>

             <div class="col-md-4">

              <div class="form-group">
                <label>gstin No:</label>
               <input type="text" id="gstin2" readonly="readonly"  class="form-control" name="gstin" >
              </div>
              
               <div class="form-group">
                <label>Acount No:</label>
               <input type="text" id="acc_no2" readonly="readonly"  class="form-control" name="acc_no" >
              </div>
              <div class="form-group">
                <label>IFSC Code:</label>
               <input type="text" id="ifsc_code2" readonly="readonly" class="form-control" name="ifsc_code"  />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>GRN Amount:</label>
               <input type="text" id="new_total2"  readonly="readonly" class="form-control" name="new_total" />
              </div>
              
            </div>

            
            <div class="col-md-4">
              <div class="form-group">
                <label>Total(After deduction):</label>
               <input type="text" readonly="readonly" id="payment2" class="form-control" name="remain"  />
              </div>
            </div>


          </div>
           </div>

       <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
        </form>
    </div>
  
    </div>
  </div>
</div>
