
<!DOCTYPE html>
<html>
<head>
  <?php 
   include("connect.php");
    include("header.php");
    include("aside_main.php");
  ?>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_rate.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Masters</h1>
    </section> -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Create Rate Master</h3>

        </div>
         <div class="box-body">
          <div class="row">
              <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">
               <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">
                <input type="hidden" name="empcode" value="<?php echo $_SESSION['empcode']; ?>">
            <div class="col-md-5">
            <label>Product No</label><br>
                <input type="text" class="auto" id="productno" style="width:433px; height: 35px;" onchange="get_data1(this.value)" name="productno" placeholder="Product No" required />

                <div class="form-group">
                <label>Product name</label>
                <input type="text"  id="productname" readonly="readonly" name="productname" placeholder="Product Name" class="form-control" required />
               </div>
              </div>
            <script type="text/javascript">
              $(function() {
                 $(".auto").autocomplete({
                  alert("hi")
                    source: "auto_complete_productno.php",
                     minLength: 2
                 });                             
              });
            </script>
              <script type="text/javascript">    
               
                    $(function()
                    {    
                      $( "#productno" ).autocomplete({
                          source: 'auto_complete_productno.php',
                          change: function (event, ui) {
                            if(!ui.item){
                              $(event.target).val(""); 
                              $(event.target).focus();
                            } 
                          },
                          focus: function (event, ui) {  
                            return false;
                          }
                      });
                    });

                     function get_data1(val) {
                            $.ajax({
                              type: "POST",
                              url: "fetch_rate_detail.php",
                              data:'productno='+val,
                              success: function(data){  
                              
                                  $("#rate_master1").html(data);
                              }
                              });
                          }
                </script>
             <div class="col-md-5">
              <label for="rate">Rate/Unit<font color="red">*</font></label>
              <input type="text" style="width: 80%;" class="form-control" name="rate" placeholder="Rate" required>

              <div class="form-group">
                <label for="Lock/Unlock">Rate Lock/Unlock<font color="red">*</font></label>
              <select name="lock_unlock" style="width: 80%;" id="party_name" class="form-control" required>   
                <option value=""> -- Please Select -- </option>
                    <option value="1">Lock</option>
                    <option value="0">Unlock</option>
                </select>
            </div>
            </div>
             <div id="rate_master1"></div>
          </div><br><br>
           <div class="col-sm-offset-2 col-sm-6">
     <center><button type="submit" name="submit" color="Primary" form-action="insert_rate.php" class="btn btn-primary">Submit</button>
       <a href="rate_view.php" class="btn btn-warning">Show Rate</a>
     </center><br><br>
    </div>

        </div>
        
      </div>
    </section>
   
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

