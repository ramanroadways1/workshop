

<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('ho_aside.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!--   <form action="insert_approve_invoice.php" method="post"> -->
  <div class="content-wrapper">
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
       Payment Approval(Head Office)</h3>
      </div>
   
    <div id="result11"></div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
            
                <div class="table-responsive">  

                   <table id="employee_data" class="table table-striped table-bordered" style="width: 100%;">  
                  <thead>  
                       <tr> 
                         <td>Grn No</td>
                        <td>Party Name</td>
                        <td>Payment Date</td>
                        <td>Manager Payment</td>
                         <td>Payment After Deduction</td> 
                        <td>TDS</td>
                         <td>Other Ho</td>
                           <td>H.o Remark</td>
                        <td>Final Payment</td>
                         <td>H.o Approve Time</td>
                        </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $show = "SELECT insert_pay_invoice.grn_no as grn_no,new_total,party_name,username,insert_pay_invoice.payment as manager_payment,payment_date,tds,other_h_o,remark_h_o,payment_confirm.payment as head_payment,payment_confirm.timestamp as timestamp1 FROM insert_pay_invoice,payment_confirm where insert_pay_invoice.id = payment_confirm.grn_id AND  length(invoice_file) > 10  and approve='2' group by grn_no";
                  $result = $conn->query($show);
                  if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { $grn_no = $row['grn_no'];
                    $party_name = $row['party_name'];
                    $new_total = $row['new_total'];
                    $username = $row['username'];
                     $manager_payment = $row['manager_payment'];
                       $payment_date = $row['payment_date'];
                     $tds = $row['tds'];
                     $other_h_o = $row['other_h_o'];
                     $remark_h_o = $row['remark_h_o'];
                    $head_payment = $row['head_payment'];

                      $timestamp = $row['timestamp1'];
                  ?>
                       <tr>
                         <td>
                          <form method="POST" action="show_grn_from_ho.php" target="_blank">
                            <input type="submit"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-xs fa fa-eye">
                          </form>
                         </td>
                      
                      <td><?php echo $party_name; ?>
                        <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                      </td>
                       <td><?php echo $payment_date; ?>
                        <input type="hidden" name="acc_holder_name[]" value='<?php echo $acc_holder_name; ?>'>
                      </td>
                       <td><?php echo $new_total; ?>
                        <input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'>
                      </td> 
                       <td><?php echo $manager_payment; ?>
                        <input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'>
                      </td> 
                      <td><?php echo $tds; ?>
                        <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                      </td>
                       <td><?php echo $other_h_o; ?>
                        <input type="hidden" name="acc_holder_name[]" value='<?php echo $acc_holder_name; ?>'>
                      </td>
                       <td><?php echo $remark_h_o; ?>
                        <input type="hidden" name="acc_holder_name[]" value='<?php echo $acc_holder_name; ?>'>
                      </td>
                       <td><?php echo $head_payment; ?>
                        <input type="hidden" name="acc_holder_name[]" value='<?php echo $acc_holder_name; ?>'>
                      </td>

                      <td><?php echo $timestamp; ?>
                        <input type="hidden" name="timestamp[]" value='<?php echo $timestamp; ?>'>
                      </td>
                     
                      
                     <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var grn_no = val;
                                  //alert(grn_no);
                                  var username = '<?php echo $username ?>'
                                    if (confirm("Are You Sure To Approve this GRN?"))
                                   {
                                        $.ajax({
                                           type:"post",
                                           url:"payment_final_approve.php",
                                          data:'grn_no='+grn_no + '&username='+username,
                                            success:function(data){
                                               alert(data)
                                               location.reload(); 
                                                    
                                              }
                                          });

                                     }
                                 }
                    </script>

                    <script>
                      function DeleteModal(grn_no)
                      {
                        var grn_no = grn_no;
                        //alert(grn_no);
                         var username = '<?php echo $username ?>'

                       
                        if (confirm("Do you want to delete this grn?"))
                        {
                          if(grn_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_grn_from_payment.php",
                                  data:'grn_no='+grn_no + '&username='+username ,
                                  success: function(data){
                                    alert(data)
                                           location.reload(); 
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                    </tr>  
                 <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>
                </table>  
                    
               
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
<!--   </form> -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

