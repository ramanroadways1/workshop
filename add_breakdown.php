<!DOCTYPE html>
<html>
<head>
  <?php 
    include("connect.php");
    include("header.php");
    include("break_downasidemain.php");
    $empcode = $_SESSION['empcode'];
    date_default_timezone_set('Asia/Calcutta'); 
    $date_sys=date("d-m-Y");
    $time_sys=date("h:i");
    $intime=date("h:i:A");
    $time1=date("A");
    // exit();

  ?>
  <script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("txtMobId");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = ^(\+[\d]{1,5}|0)?[7-9]\d{9}$;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#txtMobId').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                  }
            }
      } 
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_breakdown2.php" autocomplete="off">
  <div class="content-wrapper">
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add break down Vehicle</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
<!-- onchange="get_data15(this.value)" -->
                <label>Truck Number<font color="red">*</font></label>
                <input type="text" style="width: 100%;" onkeyup="this.value = this.value.toUpperCase();" onchange="getTyreNo1(this.value);" class="form-control" id="truck_no" name="truck_no" placeholder="Truck Numbur"  required/>
              <script type="text/javascript"> 
                  $(function()
                  {    
                  $( "#truck_no" ).autocomplete({
                  source: 'breakdoen_trucksearch.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();

                  } 
                  },
                  focus: function (event, ui) {  return false;
                  }
                  });
                  });
              </script>

              <script type="text/javascript">

                  function get_data15(val) {
                  $.ajax({
                  type: "POST",
                  url: "driver_mobailfetch.php",
                  data:'truck_no='+val,
                  success: function(data){

                  $("#rate_master41").html(data);
                  }
                  });
                  }

              </script>
               <div id="rate_master41"></div>
            <script type="text/javascript">
            function getTyreNo1(elem)
            {
            var truck_no = elem;
            if(truck_no!='')
            {
            $.ajax({
            type: "POST",
            url: "truck_breakdownchack.php",
            data:'truck_no='+truck_no,
            success: function(data){
            $("#result534").html(data);
            }
            });
            }
            }
            </script>
            <div id="result534"></div> 
              </div>

          <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">
          <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">
           <input type="hidden" name="empcode" value="<?php echo $_SESSION['empcode']; ?>">
              <div class="form-group">
                 <label for="driver_number">Driver Number<font color="red">*</font></label>
                 <input type="text" style="width: 100%;"  class="form-control" name="mobile_no" id="txtMobId" onchange="IsMobileNumber(this);" placeholder="Mobile Number" required/>
              </div>
              <div class="form-group">
                 <label for="km">KM<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" name="km" placeholder="km" required/>
              </div>
              
              <div class="form-group">
                <label for="date_start">Complain Start Date<font color="red">*</font></label>
                 <input type="date" style="width: 100%;" id="comp_date"  class="form-control" name="comp_date" placeholder="PAN Number"required/>
              </div>
              <!-- After Chnage This Values -->
              <div class="form-group">
                <label for="date_start">System Date<font color="red">*</font></label>
                <input type="text" style="width: 100%;" id="date_sys" value="<?php echo 
                $date_sys;?>"  class="form-control" name="date_systemc"required/>
              </div>

               <div class="form-group">
                <label for="date_start">System Time<font color="red">*</font></label>
                <input type="text" style="width: 100%;" id="time_sys" value="<?php echo 
                $time_sys;?>"  class="form-control" name="time_systemc"required/>
              </div>
              <!-- New Close Chnage  -->
              <div class="form-group">
                 <label for="date_start">Complain Time<font color="red">*</font></label>
                 <input type="time" style="width: 100%;" id="comp_time"  class="form-control" name="comp_time" placeholder="PAN Number"required/>
              </div>
           <!--       <div class="form-group">
                 <label for="Problem">Problem</label>
                 <input type="text" style="width: 100%;" id="textGSTINNo"  class="form-control " name="problem" placeholder="Problem">
              </div>
           <div class="form-group">
              <label for="workgroup" id="workgroup">Problem Head<font color="red">*</font></label>
              <div class="row">
                <div class="col-md-10">
                 <select name="workgroup" style="width: 100%;" id="workgroup" class="form-control"> 
                      <option disabled="disabled" selected="selected">Select Problem</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT groupbreak FROM group_break");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['groupbreak'])?>" >
                        <?php 
                              echo ($row['groupbreak']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
                 <div class="col-md-2">
                  <button type="button" style="width: 100%;" class="btn btn-info" data-toggle="modal" data-target="#myModal1" >Add</button>
                 </div>
                 <div id="result1"></div>
                </div>
             </div>  -->

              <div class="form-group">
                 <label for="status">Status</label>
                 <input type="text" style="width: 100%;" id="status" value="Pending"  class="form-control " name="status" placeholder="Status" readonly>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
               <div class="form-group">
                 <label for="report_by">Call Report By<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control"  name="report_by" id="report_by" placeholder="Call Report" required/>
              </div>
<!--          <div class="form-group">
                 <label for="update">Update<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" name="update" placeholder="Update" required/>
              </div> -->
            <div class="form-group">
              <label for="bank_name">Break Down Place<font color="red">*</font></label>
                <input type="text" style="width: 100%;" id="place_brakdown" class="form-control" name="place_brakdown" placeholder="Break Down Place" required/>
              </div>
                <div class="row">
                <div class="col-md-10">
                 <select name="approve_by" style="width: 100%;" id="approve_by1" class="form-control"> 
                      <option disabled="disabled" selected="selected">Approve By</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT approve_person FROM insert_approve");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['approve_person'])?>" >
                        <?php 
                              echo ($row['approve_person']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
                 </div>
                 <div class="col-md-2">
                  <button type="button" style="width: 100%;" class="btn btn-info" data-toggle="modal" data-target="#myModal11" >Add</button>
                 </div>
                 <div id="result11"></div>
                </div>
                  <div class="form-group">
                    <div  style="margin-top: 15px;">
                    <label class="a">Remark<font color="#FF0000">*</font></label>
                      <div style=" width: 100px;" class="input-container">
                        <textarea id="remark"  name="remark" rows="4" cols="80"></textarea>
                     </div>
                  </div>
                  </div>
             </div>
    <div class="col-sm-offset-2 col-sm-8">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
     </center>
    </div>
            </div>
          </div>
        </div>

      </div>
    </section>

        
  <br><br><br>
  </div>
</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
<!-----------------Creat Product----------------------- -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form action="insert_groupbrek.php" id="groupbreak">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Product</h4>
        </div> 
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>Product<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="groupbreak" name="groupbreak" placeholder="groupbreak"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#groupbreak").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_groupbrek.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result1").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>
  <!---------------------------------------- -->
<!-----------------Creat Product----------------------- -->
<div id="myModal11" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
    <form action="insert_approve.php" id="approve">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Approve</h4>
        </div> 
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <b>Product<font color="red">*</font></b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="approve" name="approve" placeholder="approve"/>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#approve").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_approve.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $("#result11").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>
  <!---------------------------------------- -->
