<?php

  session_start();

  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }

  require 'connect.php';

 $username = $_SESSION['username']; 
 $employee = $_SESSION['employee'];
 

 $conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );
   
         $statement = $conn->prepare("SELECT grn.*,files_upload.truck_no,files_upload.type_of_issue FROM grn LEFT JOIN files_upload on grn.grn_no=files_upload.grn_no WHERE grn.productname LIKE '%Labour%' group by grn.grn_no"); 

   $statement->execute();
   $result = $statement->fetchAll();
   $count = $statement->rowCount();
   $data = array();
   
   foreach($result as $row)
   {
        



       $sub_array = array();

                    $sub_array[] = $row["purchase_order"];
                    $sub_array[] = $row["grn_no"];
                    $sub_array[]  = $row["productname"];
                    $sub_array[]  = $row["productno"]; 
                    $sub_array[]  = $row["party_name"];
                    $sub_array[]  = $row['received_qty'];
                    $sub_array[]  = $row['new_total'];
                    $sub_array[]  = $row['type_of_issue'];
                    $sub_array[]  = $row['truck_no'];
                    $sub_array[]  = $row['employee'];
                    $sub_array[]  = $row['insert_date'];

                    $data[] = $sub_array;
   }
   
   $results = array(
       "sEcho" => 1,
       "iTotalRecords" => $count,
       "iTotalDisplayRecords" => $count,
       "aaData"=>$data);
   
    echo json_encode($results);
    exit;
?>
