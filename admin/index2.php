
 <?php 
 include('connect.php');
  include("header.php");
   
  ?>
<!DOCTYPE html>
<html>
<head>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include("aside_main.php"); ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard <sup> Admin </sup>
        <!-- <small>ADMIN</small> -->
      </h1>
    </section>

    <section class="content">

     
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"> <i style="padding-top:20px;" class="fa fa-dropbox" aria-hidden="true"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="text-align: center; padding-top:10px;">Total Product</span>
              <span class="info-box-number">
             <?php
             
              $sql="SELECT productno FROM product ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-yellow"><i style="padding-top:20px;" class="fa fa-users"></i></span>
            <div class="info-box-content">
              <span class="info-box-text" style="text-align: center; padding-top:10px;">Total Party</span>
              <span class="info-box-number">
              <?php
            
             
           $sql="SELECT party_name FROM party ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }


            
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-blue"><i style="padding-top:20px;" class="fa fa-file-text-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="text-align: center; padding-top:10px;">Total Purchase Order</span>
              <span class="info-box-number">
              <?php
        

              $sql="SELECT purchase_order FROM date_purchase_key group by purchase_order";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-maroon"><i style="padding-top:20px;" class="fa fa-check-square-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="text-align: center; padding-top:10px;">Approved PO</span>
              <span class="info-box-number">
              <?php
            

              $sql="SELECT purchase_order FROM date_purchase_key where approve_status='1'";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-green"><i  style="padding-top:20px;" class="fa fa-address-book-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="text-align: center; padding-top:10px;">Total GRN</span>
              <span class="info-box-number">
              <?php
             
               $sql="SELECT grn_no FROM grn group by grn_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
      </div>
      
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
