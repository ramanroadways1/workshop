<!DOCTYPE html>
<html>
<head><script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
/*echo"success";*/
include("connect.php");
  $admin = $_SESSION['admin'];
   $empcode = $_SESSION['empcode'];
$company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
// $company= $_POST['company'];
try{

$conn->query("START TRANSACTION");
if($company=='')
{
	echo "<SCRIPT>
	       alert('Company cannot be blank !');
	       window.location.href='company_view.php';
		</SCRIPT>";	

} else{

	     $sql ="INSERT INTO company (company) VALUES ('$company') ";
	     if($conn->query($sql) === FALSE) 
				 { 
				    throw new Exception("Code 001 : ".mysqli_error($conn));        
				 }
	/*echo $sql;*/
	// $sqld = $conn->query($sql);
        $conn->query("COMMIT");
        echo '<script>
            swal({
                 title: "Company Added Successfully.",
                  text: "",
                  icon: "success",
                  type: "success"
                  }).then(function() {
                      window.location = "company_view.php";
                  });
        </script>';

        $file_name= basename(__FILE__);
        $type=1;
        $val="Add Company: "." Company Name ".$company;
                    
        $sql1="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$admin','$empcode','$val','$type')";
        if ($conn->query($sql1)===FALSE) {
          echo "Error: log not updated !";
        }
}

} catch (Exception $e) {

  $conn->query("ROLLBACK");
  $content = htmlspecialchars($e->getMessage());
  $content = htmlentities($conn->real_escape_string($content));

  echo '<script>
            swal({
               title: "Something went wrong!",
               text: "'.$content.'",
               icon: "error",
               button: "OK"
                }).then(function() {
                    window.location = "company_view.php";
                });
          </script>';

  $file_name = basename(__FILE__); 
  $username="Admin";      
  $sql2 = "INSERT INTO `allerror`(`file_name`, `user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
  if ($conn->query($sql2) === FALSE) { 
    echo "Error: " . $sql2 . "<br>" . $conn->error;
  }
}		
?>