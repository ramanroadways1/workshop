<!DOCTYPE html>
<html>
<head>
  <?php 
   include("header.php");
   include("connect.php");
   include('aside_main.php');
   
   if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
  ?>

      <link href="../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../assets/jquery-3.5.1.js"></script>
      <script src="../assets/jquery-ui.min.js"></script>
      <link href="../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../assets/custom.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
           <!--  <div align="right">
                <a href='product_add.php' type="submit" name="add" id="add" class="btn btn-info">Add New Product</a>
            </div> -->
          <div class="row">
                          <div id="result"></div> 

                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                               <!-- <th style=" "> TRANSACTIONDATE </th> -->
                                                  <th>Id</th>
                                                  <th>Product no</th>
                                                  <th>Company</th>
                                                  <th>Product name</th>
                                                  <th>Product type</th>
                                                  <th>Unit</th>
                                                  <th>Product Group</th>
                                                  <th>Sub</th>
                                                  <th>Product location</th>
                                                  <th>Quantity</th>
                                                  <th>Update</th>
                                                  <!-- <th>Delete</th> -->
                                  
                    
                            </tr>
                         </thead>
                         <tfoot class="thead-light">
                            <tr>
                               

                            </tr>
                         </tfoot>
                      </table>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "product_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV',
                             
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                           
                         ],
                          "searchBuilder": {
                          "preDefined": {
                               // "criteria": [
                              //     {
                              //         "data": ' VEHICLE ',
                              //         "condition": '=',
                              //         "value": ['']
                              //     }
                              // ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          $('title').html(" RRPL - Vendor Management System ADMIN");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "party_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div id="result_main"></div>

          <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

        <script src="../assets/bootstrap.js"></script>

        <script src="../assets/bootstrap.bundle.js"></script>
        <script src="../assets/scripts.js"></script>
        <script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>


</body>
</html>

<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_product.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>
<script type="text/javascript">
$(document).ready(function (e) {
$("#FormProductUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_product.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script>

<!-- <script>
function EditModal45(id)
{
  jQuery.ajax({
      url: "fetch_product.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>

<script type="text/javascript">

  $(document).ready(function (e) {
  $("#FormProductUpdate").on('submit',(function(e) {
  e.preventDefault();
      $.ajax({
      url: "update_product_main.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
          $("#result").html(data);
      },
      error: function() 
      {} });}));});

</script>  -->



<script>
  function DeleteModal(id)
  {
    var id = id;
    var productno = $('#productno'+id).val();
    if (confirm("Do you want to delete this product?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_product.php",
              data:'id='+id + '&productno='+productno ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="modal_button1" style="display: none;" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Product</h4>
      </div>
            <form id="FormProductUpdate" action="update_product.php">

      <div class="modal-body">
         <div class="row">
          <div class="col-md-6">
            <!-- <div class="form-group"> -->
              <!-- <label for="id">ID</label> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>
            <!-- </div> -->
                        <div class="form-group">
                <b>Product Number</b>
                <input type="text" style="width: 100%;" id="productno" autocomplete="off" class="form-control" name="productno" readonly="readonly" required>
              </div>
                            <div class="form-group">
                <b>Company</b>
                <select name="company" style="width: 100%;" id="company" name="company" class="form-control"> 
                      <option disabled="disabled" id="company" selected="selected">Select Company</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT company FROM company");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['company'])?>" >
                        <?php 
                              echo ($row['company']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
              </div>
              <div class="form-group">
                <b>Product Name</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="productname" name="productname"  required>
              </div>
                            <div class="form-group">
                <b>Product Type</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="producttype" name="producttype" readonly="readonly" required>
              </div>

               <div class="form-group">
                <b>Unit</b>
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="unit" name="unit" readonly="readonly" required>
              </div>
   
           
          </div>
          <!-- /.col -->
          <div class="col-md-6">
               <div class="form-group">
                  <b>Group</b>
                  <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="product_group" name="product_group" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Sub Group</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="sub" name="sub" readonly="readonly" required>
              </div>


              <div class="form-group">
                <b>Product Location(Ahmedabad)</b>
                <select name="pro_loc" style="width: 100%;" id="pro_loc" class="form-control"> 
                    <option disabled="disabled" selected="selected">Select Rack Location</option>
                    <?php 
                      include ("connect.php");
                        $get=mysqli_query($conn,"SELECT rackno FROM rack");
                           while($row = mysqli_fetch_assoc($get))
                          {
                    ?>
                    <option value = "<?php echo($row['rackno'])?>" >
                      <?php 
                            echo ($row['rackno']."<br/>");
                      ?>
                    </option>
                      <?php
                      }
                    ?>
                </select>
              </div>
        
          </div>
          <!-- /.col -->
        </div>
      </div>
           <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
       </div>
     </form>
    </div>

  </div>
</div>


