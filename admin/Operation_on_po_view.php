<!DOCTYPE html>
<html>
<head>
 <?php 
    include("header.php");
    include("aside_main.php");
  $valueTosearch = $_POST['purchase_order'];


  ?>
     <link href="../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../assets/jquery-3.5.1.js"></script>
      <script src="../assets/jquery-ui.min.js"></script>
      <link href="../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../assets/custom.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                                    <td>Product Number</td>
                                    <td>Product Name</td>
                                    <td>Rate(from Rate Master)</td>
                                    <td>Rate/Unit</td>
                                    <td>Quantity</td>
                                    <td>Difference In Rates</td>
                                    <td>Amount</td>
                                    <td>Gst</td>
                                    <td>Gst Amount</td>
                                    <td>Total Amount</td>
                                    <td>Date</td>
                                     <td>Update</td>
                                    <td>Delete</td>
                          <!--  <td>Update</td>
                          <td>Delete</td> -->
              
                            </tr>
                         </thead>
                        
                      </table>
                      <div id="dataModal" class="modal fade">
                         <div class="modal-dialog modal-lg">
                            <div class="modal-content" id="employee_detail">
                            </div>
                         </div>
                      </div>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "Operation_on_po_fetch.php?id=<?=$valueTosearch; ?>"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV' 
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                         ],
                          "searchBuilder": {
                          "preDefined": {
                               // "criteria": [
                              //     {
                              //         "data": ' VEHICLE ',
                              //         "condition": '=',
                              //         "value": ['']
                              //     }
                              // ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=assets/loading.gif height=25> </center>"
                          },
                         "aaSorting": [],
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          $('title').html(" RRPL - Vendor Management System ADMIN");

                          

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.

    </footer> -->

  <div class="control-sidebar-bg"></div>
</div>

       <script src="../assets/bootstrap.js"></script>

        <script src="../assets/bootstrap.bundle.js"></script>
        <script src="../assets/scripts.js"></script>
        <script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>

  </footer>
</section>
</div>


         <script>
                function EditModal(id)
                {
                  jQuery.ajax({
                      url: "fetch_po.php",
                      data: 'id=' + id,
                      type: "POST",
                      success: function(data) {
                        
                      $("#result_main").html(data);
                      },
                          error: function() {}
                      });
                  document.getElementById("modal_button1").click();
                  $('#myModal_id').val(id);

                  }
        </script>
          <div id="result_main"></div>
        <script>
                function DeleteModal(id)
                {
                  //alert(id);
                  var id = id;
                  var key1 = $('#key1'+id).val();
                  if (confirm("Do you want to delete this purchase order?"))
                  {
                    if(id!='')
                    {
                      $.ajax({
                            type: "POST",
                            url: "delete_po.php",
                            data:'id='+id + '&key1='+key1 ,
                            success: function(data){
                                $("#result22").html(data);
                            }
                        });
                    }
                  }
                }     
        </script>
</body>
  <script>  
       $(document).ready(function(){  
            $('#employee_data').DataTable();  
       });  
   </script> 
 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

      <form action="update_po.php" autocomplete="off" id="FormPOUpdate" method="POST">
     
       <div id="myModal22" class="modal fade" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update PO</h4>
          </div>
          
    <script type="text/javascript"> 
          $(function()
          { 
          $( "#productno" ).autocomplete({
          source: 'po_no_search.php',
          change: function (event, ui) {
          if(!ui.item){
          $(event.target).val(""); 
          $(event.target).focus();
          alert('Product does not exists.');
          $('#form')[0].reset();
          } 
          },
          focus: function (event, ui) { $('#form')[0].reset(); return false; } }); });
    </script>  
          
          <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="hidden" style="width: 100%;" class="form-control" id="id" name="id" readonly/>
              <label>Product number</label>
           
              <input type="text" style="width: 100%;" class="form-control" id="productno" onblur="get_data(this.value);" name="productno"  placeholder="product number" readonly required />
              </div>
              <div id="rate_master222"></div>
                  <script>
                       function get_data(val) {
                      $.ajax({
                        type: "POST",
                        url: "get_data1.php",
                        data:'productno='+val,
                        success: function(data){
                          $("#rate_master222").html(data);
                        }
                        });
                      }
                </script>
               <div class="form-group">
              <label>Product name</label>
              <div>
                <div  class="rs-select2 js-select-simple select--no-search">
                   <input class="form-control" name="productname" id="productname"  placeholder="product Name" >
                </div>
              </div>
              </div>

               <div class="form-group">
              <label>Rate master</label>
              <div  class="rs-select2 js-select-simple select--no-search">
                <input class="form-control" name="rate_master" id="rate_master" onblur="getdiff(this.value);" placeholder="rate master"  readonly>
              </div>
              </div>

               <input type="hidden" class="form-control" name="lock_unlock" id="lock_unlock" readonly required>
              
              <div class="form-group">
               <label>Quantity</label>
               <input type="number" oninput="sum1()" style="width: 100%;"  id="quantity" class="form-control" name="quantity" placeholder="Quantity" required/>
              </div>

              <script>
              function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var fix_rate = Number($('#rate_master').val());
                 var lock = $('#lock_unlock').val();
                 if(lock==1)
                 {
                   if(myrate>fix_rate)
                   {
                     alert('You can not exceed Master Rate. Master rate is '+ fix_rate);
                     $('#rate').val('');
                   }
                 }
                }
              </script>

              <div class="form-group">
               <label>Rate unit</label>
               <input type="Number" step="any" oninput="ChkForRateMaxValue();sum1();" onblur="getdiff(this.value);" style="width: 100%;" id="rate" class="form-control" name="rate" placeholder="rate unit" required/>
              </div>
              
                <script>
                function sum1()
                {
                 var qty = Number($('#quantity').val());  
                 var rate = Number($('#rate').val());  
                 var rate_master = Number($('#rate_master').val());  
                 var gst = Number($('#gst').val());  
                 var difference = rate_master-rate;
                 var amt = qty*rate;
                 
                 $('#amount').val(amt);
                 $('#gst_amount').val((amt*gst/100).toFixed(2));
                 $('#total').val(Number($('#gst_amount').val())+Number($('#amount').val()));
                 $('#diff').val(difference);
                }
                </script>

              <div class="form-group">
               <label>Rate Difference</label>
               <input type="text" style="width: 100%;" id="diff" class="form-control" name="diff" placeholder="Rate Difference" readonly required/>
              </div>
              <script>
              /*function getdiff(i)
              {
                var rate1 = Number($('#rate_master').val());  
                var rate2 = Number($('#rate').val());
                var difference = rate1-rate2;
                $('#diff').val(difference);
               // $('#diff').val(Number($('#rate_master').val())-Number($('#rate').val()));
              }*/
              </script>
              
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
               <label>Amount</label>
               <input type="text" style="width: 100%;" id="amount" class="form-control" name="amount" placeholder="amount" readonly required/>
              </div>
              
             <div class="form-group">
               <label>gst value</label>
               <select onchange="sum1();MyGst(this.value);" class="form-control" style="width: 100%;" required id="gst" name="gst_value">
                  <option value="">0</option>
                  <option value="5">5</option>
                  <option value="12">12</option>
                  <option value="18">18</option>
                  <option value="28">28</option>
                </select>
              </div>
          

                <div class="form-group">
                <label>gst_amount</label>
                   <input type="text" style="width: 100%;" class="form-control" id="gst_amount" name="gst_amount" readonly placeholder="gst amount" />
                </div>

                <div class="form-group">
                <label >gst_type</label>
                  <input type="text" style="width: 100%;" class="form-control" id="gst_type" name="gst_type" readonly/>
                </select> 
                </div>
                
              <script>
              function MyGst(i)
              { 
                var gst = Number($('#gst').val());  
                var gst_type=document.getElementById("gst_type").value;
                if(gst_type=='cgst_sgst')
                {
                  $('#gst_acc_type').val(gst/2);
                }
                else{
                  $('#gst_acc_type').val(gst);
                }
              }
              </script>

                <div class="form-group">
                <label>Selected Gst</label>
                  <input type="text" style="width: 100%;" class="form-control prc" id="gst_acc_type" name="gst_acc_type" readonly placeholder="Selected G.S.T" required>
                </div>

                <div class="form-group">
                <label>total amount</label>
                  <input type="text" style="width: 100%;" id="total" class="form-control" name="total_amount"  placeholder="Total Amount" required readonly>
                </div>
                
            </div>
            <!-- /.col -->

            </div>
          </div>
          <div class="modal-footer">
          <!--  <input type="reset" align="right" name="Reset" value="Reset" tabindex="50"> -->
              
          <button type="submit" class="btn btn-danger">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        </div>
      </div>
    </form>

