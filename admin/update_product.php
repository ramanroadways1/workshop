<?php
// echo "success";
include("connect.php");
// $id = $_POST['id'];
// $productno= $_POST['productno'];
// $company= $_POST['company'];
// $productname= $_POST['productname'];
// $producttype= $_POST['producttype'];
// $unit= $_POST['unit'];
// $product_group= $_POST['product_group'];
// $sub= $_POST['sub'];
// $pro_loc= $_POST['pro_loc'];

$table_add_ex_product_temp1 = 0;
$table_add_ex_product_temp2 = 0;
$table_add_product_external_jobcard = 0;
$table_add_product_jobcard1 = 0;
$table_add_product_jobcard2 = 0;
$table_add_product_sample = 0;
$table_dhule_product = 0;
$table_grn = 0;
$table_insert_po = 0;
$table_outward_stock = 0;
$table_product1 = 0;
$table_product_inventory = 0;
$table_rate = 0;
$table_product2 = 0;

 $id = $conn->real_escape_string(htmlspecialchars($_POST['id']));
 $productno = $conn->real_escape_string(htmlspecialchars($_POST['productno']));
 @$company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
 $unit = $conn->real_escape_string(htmlspecialchars($_POST['unit']));
 $productname = $conn->real_escape_string(htmlspecialchars($_POST['productname']));
 $producttype = $conn->real_escape_string(htmlspecialchars($_POST['producttype']));
 $product_group = $conn->real_escape_string(htmlspecialchars($_POST['product_group']));
 $sub = $conn->real_escape_string(htmlspecialchars($_POST['sub']));
 @$pro_loc = $conn->real_escape_string(htmlspecialchars($_POST['pro_loc']));
 // $username="Admin";
   $admin = $_SESSION['admin'];
   $empcode = $_SESSION['empcode'];
  try{
    
      $cnx = new PDO("mysql:host=$server;dbname=$db", $user, $password);
      $cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $cnx->beginTransaction();

      $sql = "select id,productno,productname from product where id='$id'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute(); 

      $row = $stmt->fetch(PDO::FETCH_OBJ);

      $old_productno = $row->productno;
      $old_productname = $row->productname;

      $new_productname = trim($productname);
      $new_productno = $productno;

      if($old_productno != $new_productno) 
      { 
        throw new PDOException("Product no mismatch !");        
      }

//ham product name se bhi replace kar rhe h data because kuch table mein product no nahi dala h toh reference kha se lege isliye aab product name ko check karege bhale hi other company ka kyu na ho agar same product name exist karta h toh usko change nahi karne dege because phir sab mix ho jaega.
$sql = "select id,productno,productname from product where TRIM(productname)=TRIM('$old_productname') and productno!='$old_productno'";
$stmt = $cnx->prepare($sql);
$stmt->execute();
$number_of_rows = $stmt->rowCount(); 
if($number_of_rows>0) 
{ 
  throw new PDOException("Old product name ($old_productname) already exist with other product no !");        
}

$sql = "select id,productno,productname from product where TRIM(productname)=TRIM('$new_productname') and productno!='$old_productno'";
$stmt = $cnx->prepare($sql);
$stmt->execute();
$number_of_rows = $stmt->rowCount(); 
if($number_of_rows>0) 
{ 
  throw new PDOException("New product name ($new_productname) already exist with other product no !");        
}

if(trim($new_productname)!=trim($old_productname)){

      $sql = "update add_ex_product_temp set partsname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_ex_product_temp1 = $stmt->rowCount();

      $sql = "update add_ex_product_temp set partsname='$new_productname' where partsname='$old_productname'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_ex_product_temp2 = $stmt->rowCount();

      $sql = "update add_product_external_jobcard set partsname='$new_productname' where partsname='$old_productname'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_product_external_jobcard = $stmt->rowCount();

      $sql = "update add_product_jobcard set partsname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_product_jobcard1 = $stmt->rowCount();

      $sql = "update add_product_jobcard set partsname='$new_productname' where partsname='$old_productname'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_product_jobcard2 = $stmt->rowCount();

      $sql = "update add_product_sample set productname='$new_productname' where p_no='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_add_product_sample = $stmt->rowCount();

      $sql = "update dhule_product set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_dhule_product = $stmt->rowCount();

      $sql = "update grn set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_grn = $stmt->rowCount();

      $sql = "update insert_po set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_insert_po = $stmt->rowCount();

      $sql = "update outward_stock set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_outward_stock = $stmt->rowCount();

      $sql = "update product set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_product1 = $stmt->rowCount();

      $sql = "update product_inventory set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_product_inventory = $stmt->rowCount();

      $sql = "update rate set productname='$new_productname' where productno='$old_productno'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_rate = $stmt->rowCount();

}

      $sql ="UPDATE product SET company='$company', pro_loc='$pro_loc' where id = '$id'";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
      $table_product2 = $stmt->rowCount();

$affected_rows="add_ex_product_temp (by No) : $table_add_ex_product_temp1
add_ex_product_temp (by Name) : $table_add_ex_product_temp2
add_product_external_jobcard : $table_add_product_external_jobcard
add_product_jobcard (by No) : $table_add_product_jobcard1
add_product_jobcard (by Name) : $table_add_product_jobcard2
add_product_sample : $table_add_product_sample
dhule_product : $table_dhule_product
grn : $table_grn
insert_po : $table_insert_po
outward_stock : $table_outward_stock
product (Name) : $table_product1
product_inventory : $table_product_inventory
rate : $table_rate
product (Details) : $table_product2";

        $sql = "insert into product_update_log (productno,old_productname,new_productname,affected_rows) values ('$old_productno', '$old_productname', '$new_productname', '$affected_rows')";
        $stmt = $cnx->prepare($sql);
        $stmt->execute();
 
        $cnx->commit();

        echo "<SCRIPT>
        alert('Product Updated Successfully.');
        window.location.href='product_view.php';
        </SCRIPT>";

        $file_name= basename(__FILE__);
        $type=1;
        $val="Product Update: "."Product Number-".$productno.", OldProductName-".$old_productname.", NewProductName-".$new_productname.", Company-".$company.",  ProductLocation-".$pro_loc;
        $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$admin','$empcode','$val','$type')";
        $stmt = $cnx->prepare($sql);
        $stmt->execute();
  }
    
catch (PDOException $e) { 

      $cnx->rollback();
      $content = htmlspecialchars($e->getMessage());
      $content = htmlentities($conn->real_escape_string($content));

      echo "<SCRIPT>
       alert('Error: $content');
       window.location.href='product_view.php';
      </SCRIPT>";

      $file_name = basename(__FILE__);        
      $sql = "INSERT INTO `allerror`(`file_name`,,`user_name`,`employee`,`error`) VALUES ('$file_name ','$admin','$empcode',$content')";
      $stmt = $cnx->prepare($sql);
      $stmt->execute();
} 



/*$quantity= $_POST['quantity'];
*/
                // $sql1="INSERT INTO `allerror`(`file_name`,`user_name`,`error`,`type`) VALUES ('$file_name','$username','$val','$sql')";
                //   if ($conn->query($sql1)===TRUE) {
                //   // echo "Product Update Successfully";
                //                     # code...
                //                   }

      // $sql="UPDATE product
      // INNER join insert_po on product.productno=insert_po.productno
      // INNER join grn on product.productno=grn.productno
      // INNER JOIN add_product_jobcard ON product.productno=add_product_jobcard.productno
      // INNER JOIN add_product_external_jobcard ON product.productno=add_product_external_jobcard.productno
      // SET product.productname='$productname',insert_po.productname='$productname',grn.productname='$productname',add_product_jobcard.partsname='$productname',add_product_external_jobcard.partsname='$productname' WHERE product.productno='$productno'";
      
      // $sql ="UPDATE product SET productno='$productno', company='$company', productname='$productname', producttype='$producttype', unit='$unit', product_group='$product_group', sub='$sub', pro_loc='$pro_loc' where id = '$id'";
      // 
      //   // }else{
   //   echo "<SCRIPT>
   //       alert('Product not Updated');
   //       window.location.href='product_view.php';
    // </SCRIPT>";
   // }
?>