
<!DOCTYPE html>
<html>
<head>
  <?php 
   include("header.php");
   include("connect.php");
   include('aside_main.php');

  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
   $empcode = $_SESSION['empcode'];
   //    print_r($empcode);
   // print_r($employee);
   // exit();
  ?>

      <link href="../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../assets/jquery-3.5.1.js"></script>
      <script src="../assets/jquery-ui.min.js"></script>
      <link href="../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../assets/custom.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
        <!--     <div align="right">
                <a href='party_add.php' type="submit" name="add" id="add" class="btn btn-info">Add New Party</a>
            </div> -->

          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                               <!-- <th style=" "> TRANSACTIONDATE </th> -->
                                <th>ID</th>
                                <th>Party Name</th>
                                <th>Party Location</th>
                                <th>Mobile number</th>
                                <th>Address</th>
                                <th>Contact Person</th>
                                <th>Email</th> 
                                <th>PAN Number</th>
                                <th>GSTIN Number</th>
                                <th>GST Type</th> 
                                <th>Account Number</th>
                                <th>Account Holder Name</th> 
                                <th>IFSC Code</th>  
                                <th>Bank Name</th>
                                <th>Branch Name</th>          
                                <th>GSTIN Applicable</th>
                                 <th>Party Code</th>
                                <th>Update</th>
                                <th>Delete</th>
                        
          
                    
                            </tr>
                         </thead>
                         <tfoot class="thead-light">
                            <tr>
                               

                            </tr>
                         </tfoot>
                      </table>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "party_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV',
                             
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                           
                         ],
                          "searchBuilder": {
                          "preDefined": {
                               // "criteria": [
                              //     {
                              //         "data": ' VEHICLE ',
                              //         "condition": '=',
                              //         "value": ['']
                              //     }
                              // ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          $('title').html(" RRPL - Vendor Management System ADMIN");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "party_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div id="result_main"></div>
              <div id="result"></div> 

  <footer class="main-footer">
    <div class="pull-right hidden-xs">x`x`
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

        <script src="../assets/bootstrap.js"></script>

        <script src="../assets/bootstrap.bundle.js"></script>
        <script src="../assets/scripts.js"></script>
        <script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>


</body>
</html>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_party.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>
<script type="text/javascript">
$(document).ready(function (e) {
$("#FormPartyUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_party.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 
<script>
  function DeleteModal(id)
  {
    var id = id;
    var party_code = $('#party_code'+id).val();
    if (confirm("Do you want to delete this party?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_party.php",
              data:'id='+id + '&party_code='+party_code ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }


  }     
</script>
<div id="result22"></div>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="modal_button1" style="display: none;" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Party</h4>
      </div>
            <form id="FormPartyUpdate" action="update_party.php">

      <div class="modal-body">
         <div class="row">
          <div class="col-md-6">
<!-- <?php

// $employee = $conn->real_escape_string(htmlspecialchars($_POST['employee']));
print_r($employee);
?>  -->
  <!-- <div class="form-group"> -->
              <!-- <label for="id">ID</label> -->
               <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="admin" name="admin"/>

                <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="employee" name="employee"/>

              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>

            <!-- </div> -->
            <div class="form-group">
              <b>Party Name</b>
              <input type="text" style="width: 100%;" readonly="readonly" required="required" autocomplete="off" class="form-control" id="party_name" name="party_name"/>
            </div>
            <div class="form-group">
              <b>Party Location</b>
              <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" name="party_location" id="party_location" />
            </div>
            <div class="form-group">
               <b>Mobile number</b>
               <input type="number" style="width: 100%;" required="required" required="required" onchange="IsMobileNumber(this);" autocomplete="off" class="form-control" id="mobile_no" name="mobile_no" value="<?php echo $mobile_no; ?>" />
            </div>
            <div class="form-group">
               <b>Address</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" id="address" name="address" />
            </div>
            <div class="form-group">
               <b>Contact Person</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" id="contact_person" name="contact_person" />
            </div>

            <div class="form-group">
               <b>Email</b>
               <input type="text" style="width: 100%;" autocomplete="off"  required="required" onblur="validateEmail(this.value);" class="form-control" id="email" name="email" />
            </div>
            <div class="form-group">
               <b>PAN Number</b>
               <input type="text" style="width: 100%;" required="required" autocomplete="off" id="pan" MaxLength="10" class="form-control" name="pan" onchange="ValidatePAN(this);">
            </div>
            <div class="form-group">
               <b>GSTIN Number</b>
               <input type="text" style="width: 100%;" autocomplete="off" id="gstin" MaxLength="15" class="form-control" name="gstin" oninput="chkGstType();" onchange="ValidateGSTIN(this);"/>
            </div>
           
          </div>
          <!-- /.col -->
          <div class="col-md-6">
             <div class="form-group">
               <b>GST Type</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" id="gst_type" MaxLength="15" class="form-control" name="gst_type" readonly/>
            </div>
            <script>
                function chkGstType()
              {
               var gstinno = $('#gstin').val();
               if(gstinno.substr(0,2)==24)
               {
                $('#gst_type').val('cgst_sgst');
               }
               else{
                $('#gst_type').val('igst');
               }
              }
              </script>

            <div class="form-group">
                  <h2>Account Details</h2>
            </div>
          <div class="form-group">
            <b>Bank Name</b>
            <input type="text" style="width: 100%;" autocomplete="off" id="bank" required="required" class="form-control" name="bank" />  
    
           </div>
      

              <div class="form-group" >
                <b>Branch Name</b>
                  <input type="text"  value="<?php echo $branch; ?>" required="required" style="width: 100%;" id="branch" class="form-control" name="branch" />
              </div>
                <div class="form-group">
                  <b>Account Number</b>
                     <input type="number" style="width: 100%;"  required="required" value="<?php echo $acc_no; ?>" id="acc_no" class="form-control" name="acc_no" />
                </div>
                <div class="form-group">
                  <b>Account Holder Name</b>
                     <input type="text"  value="<?php echo $acc_holder_name; ?>" required="required" style="width: 100%;" autocomplete="off" id="acc_holder_name" class="form-control" name="acc_holder_name" />
                </div>
                <div class="form-group">
                  <b>IFSC Code</b>
                    <input type="text"  value="<?php echo $ifsc_code; ?>" required="required" style="width: 100%;" autocomplete="off" id="ifsc_code" class="form-control" name="ifsc_code" onchange="ValidateIFSC(this);"/>
                </div>
                <div class="form-group">
                  <b>Party Code</b>
                    <input type="text" style="width: 100%;" autocomplete="off" required="required" id="party_code" class="form-control" name="party_code" readonly="readonly" />
                </div>
        
          </div>
          <!-- /.col -->
        </div>
      </div>
           <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
       </div>
     </form>
    </div>

  </div>
</div>
 

   <script type="text/javascript">
     function validateEmail(sEmail) {
     var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

        if(!sEmail.match(reEmail)) {
          alert("Invalid email address");
          document.getElementById('email').value = "";
        }

        return true;

      }
   </script>

<script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("mobile_no");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = /^[1-9]{1}[0-9]{9}$/;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#mobile_no').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                  }
            }
      } 

    function ValidatePAN() { 
      var Obj = document.getElementById("pan");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                if (ObjVal.search(panPat) == -1) {
                    alert("Invalid Pan No... Please Enter In This Format('ABCDE1234F')");
                    $('#pan').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Pan No.");
                  }
            }
      } 
      function ValidateGSTIN() { 
      var Obj = document.getElementById("gstin");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var gstinPat = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
                if (ObjVal.search(gstinPat) == -1) {
                    alert("Invalid GSTIN No... Please Enter In This Format('12ABCDE1234F1ZQ')");
                    $('#gstin').val('');
                    $('#gst_type').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct GSTIN No.");
                  }
            }
      }
      function ValidateIFSC() { 
      var Obj = document.getElementById("ifsc_code");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var ifscPat = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/;
                if (ObjVal.search(ifscPat) == -1) {
                    alert("Invalid IFSC No... Please Enter In This Format('ABCD0123456')");
                    $('#ifsc_code').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct IFSC No.");
                  }
            }
      }
   </script>


