<?php include('connect.php'); ?>
<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>

 <table id="employee_data" class="table table-hover" border="4"; >
<?php

$productno=$_POST['productno'];
$query = "SELECT * FROM product WHERE productno='$productno'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
 echo '
  <div class="table-responsive">
 
    <tr>
      <th>Id</th>
      <th>Product no</th>
      <th>Company</th>
      <th>Product name</th>
      <th>Product type</th>
      <th>Unit</th>
      <th>Product Group</th>
      <th>Sub</th>
      <th>Product location</th>
      <th>Quantity</th>
      <th>Update</th>
      <th>Delete</th>
    </tr>
 ';
 while($row = mysqli_fetch_array($result))

 {
  echo '
   <tr>
     <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
    
     <td>'.$row["productno"].'<input type="hidden" id="productno'.$row["id"].'" name="productno[]" value="'.$row["productno"].'"></td>
     <td>'.$row["company"].'<input type="hidden" name="company[]" value="'.$row["company"].'"></td>
     <td>'.$row["productname"].'<input type="hidden" name="productname[]" value="'.$row["productname"].'"></td>
     <td>'.$row["producttype"].'<input type="hidden" name="producttype[]" value="'.$row["producttype"].'"></td>
     <td>'.$row["unit"].'<input type="hidden" name="unit[]" value="'.$row["unit"].'"></td>
     <td>'.$row["product_group"].'<input type="hidden" name="product_group[]" value="'.$row["product_group"].'"></td>
     <td>'.$row["sub"].'<input type="hidden" name="sub[]" value="'.$row["sub"].'"></td>
     <td>'.$row["pro_loc"].'<input type="hidden" name="pro_loc[]" value='.$row["pro_loc"].'></td>
     <td>'.$row["quantity"].'<input type="hidden" name="quantity[]" value='.$row["quantity"].'></td>     
     <td>
      <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="update" class="btn btn-info" />
    </td>
    
    <td>
      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
    </td>
    
               
   </tr>
  ';
 } 

}
else
{
 echo 'Data Not Found';
}

?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var productno = $('#productno'+id).val();
    if (confirm("Do you want to delete this product?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_product.php",
              data:'id='+id + '&productno='+productno ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>
