
 <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li>
          <a href="admin_login.php">
            <i class="fa fa-user"></i> <span>Admin</span>
          </a>
        </li>
         <li>
          <a href="inventory.php">
            <i class="fa-bar-chart "></i> <span>Inventory</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>update Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="party.php"><i class="fa fa-circle-o"></i>Party</a></li>
            <li><a href="product.php"><i class="fa fa-circle-o"></i>Product</a></li>
            <li><a href="rate.php"><i class="fa fa-circle-o"></i>Rate</a></li>
            <li><a href="company.php"><i class="fa fa-circle-o"></i>Company</a></li>
            <li><a href="rack.php"><i class="fa fa-circle-o"></i>Rack</a></li>
          </ul>
        </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Update Purchase Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="generate_po_operation.php"><i class="fa fa-circle-o"></i>Update Purchase Order</a></li>
            <!-- <li><a href="search_by_po.php"><i class="fa fa-circle-o"></i> Search Purchase Order</a></li> -->
          </ul>
        </li>
       
        <!-- <li>
          <a href="approve_po.php">
            <i class="fa fa-envelope"></i> <span>Approve Purchase Order</span>
          </a>
        </li>
 -->
        <li>
          <a href="grn_operation.php">
            <i class="fa fa-envelope"></i> <span>Update GRN</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Payment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="challan_payment.php"><i class="fa fa-circle-o"></i> Challan Payment</a></li>
            <!-- <li><a href="invoice_payment.php"><i class="fa fa-circle-o"></i> Invoice Payment</a></li> -->
          </ul>
        </li>

      </ul>
    </section>
  </aside>
