<?php include('connect.php'); ?>
<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>

<table id="employee_data" class="table table-hover" border="4"; >
<?php

$productno=$_POST['productno'];
 $query = "SELECT * FROM rate WHERE productno='$productno'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
 echo '
  <div class="table-responsive">
   
    <tr>
    <th>Id</th>
    <th>Product no</th>
    <th>Product Name</th>
    <th>Rate</th>
    <th>Lock=1/Unlock=0</th>
    <th>Update</th>
    <th>Delete</th>
     
    </tr>
 ';
 while($row = mysqli_fetch_array($result))

 {
  echo '
   <tr>
     <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
     <td>'.$row["productno"].'<input type="hidden" id="productno'.$row["id"].'" name="productno[]" value="'.$row["productno"].'"></td>
     <td>'.$row["productname"].'<input type="hidden" id="productname'.$row["id"].'" name="productname[]" value="'.$row["productname"].'"></td>
     <td>'.$row["rate"].'<input type="hidden" id="rate'.$row["id"].'" name="rate[]" value="'.$row["rate"].'"></td>
     <td>'.$row["lock_unlock"].'<input type="hidden" id="lock_unlock'.$row["id"].'" name="lock_unlock[]" value="'.$row["lock_unlock"].'"></td>
     
     <td>
      <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="Update" class="btn btn-info" />
    </td>
    
    <td>
      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
    </td>
    
               
   </tr>
  ';
 } 

}
else
{
 echo 'Data Not Found';
}

?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var productno = $('#productno'+id).val();
    if (confirm("Do you want to delete this rate?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_rate.php",
              data:'id='+id + '&productno='+productno ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>