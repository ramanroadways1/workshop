
<?php  include("connect.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>


</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
 
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
       Approve Purchase Order</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-size: 13px; font-family: verdana;">  
                  <thead>  
                       <tr>  
                        <td scope="row" style="display: none;">date Purchase Id</td>
                           <td scope="row" style="display: none;">id</td>
                                        <td>View</td>
                                        <td>P.O No</td>
                                        <td>Date And Time</td>
                                          
                       </tr>  
                  </thead> 
                    <?php
                        $query = "SELECT purchase_order,date1,key1 FROM date_purchase_key where approve_status = '1'";

                        $result = mysqli_query($conn, $query);
                        $l_u = 1;
                        $id_customer = 0;
                      ?>   
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                 
                    $key1 = $row['key1'];
                    $purchase_order = $row['purchase_order'];
                    $date1 = $row['date1'];
                    
                  ?>
                  <tr>
                   
                    <td >
                     <form method="POST" action="show_po_print.php">
                        <input type="hidden" name="key1" value='<?php echo $key1; ?>'>
                        <input type="submit"  value="View" class="btn btn-primary btn-sm" >
                      </form>
                     </td>
                     <td ><?php echo $purchase_order?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $date1?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  } 
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 