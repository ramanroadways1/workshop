<!DOCTYPE html>
<html>
 <head><script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
 include ("connect.php");

 $product = $conn->real_escape_string(htmlspecialchars($_POST['productno']));
 $company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
 $unit = $conn->real_escape_string(htmlspecialchars($_POST['unit']));
 $productname = $conn->real_escape_string(htmlspecialchars($_POST['productname']));
 $producttype = $conn->real_escape_string(htmlspecialchars($_POST['producttype']));
 $product_group = $conn->real_escape_string(htmlspecialchars($_POST['product_group']));
 $sub = $conn->real_escape_string(htmlspecialchars($_POST['sub']));
 $pro_loc = $conn->real_escape_string(htmlspecialchars($_POST['pro_loc']));
 $username="Admin";
  try{

          $conn->query("START TRANSACTION");

 // $product = $_POST['productno'];
 // $company = $_POST['company'];
 // $unit = $_POST['unit'];
 // $productname = $_POST['productname'];
 // $producttype = $_POST['producttype'];
 // $product_group = $_POST['product_group'];
 // $sub = $_POST['sub'];
 // $pro_loc = $_POST['pro_loc'];

if(empty($company) && empty($unit) && empty($pro_loc))
{
	echo "
      <script>
      alert('Please insert Company,Unit and Product location...');
      window.location.href='product_add.php';
      </script>";
      exit();
}
$sql = "insert into product(productno,company,productname,producttype,unit, product_group,	sub, pro_loc) value ('$product','$company','$productname','$producttype','$unit','$product_group','$sub','$pro_loc')";

      if($conn->query($sql) === FALSE) 
          { 
            throw new Exception("Code 001 : ".mysqli_error($conn));        
          }

// if ($conn->query($sql) === TRUE)
          $conn->query("COMMIT");

 // {
               echo '<script>
                    swal({
                         title: "Product Inserted Successfully.",
                          text: "Product Add(Admin)",
                          type: "success"
                          }).then(function() {
                              window.location = "product_add.php";
                          });
                </script>';

                $file_name= basename(__FILE__);
                $type=1;
                $val="Add Product:"."Product Number-".$product.",Product Name-".$productname.",Product Location-".$pro_loc;
                $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`error`,`type`) VALUES ('$file_name','$username','$val','$type')";
                  if ($conn->query($sql)===FALSE) {
                  echo "Product No Add Some Technical Issue";
                                    # code...
                                  }
    //  echo "<SCRIPT>
    //    alert('Product Inserted Successfully.');
    //    window.location.href='product_add.php';

    // </SCRIPT>";
// } else {
   /*echo "Error: " . $sql . "<br>" . $conn->error;*/
   // echo "<SCRIPT>
   //     alert('Product not Inserted');
   //     window.location.href='product_add.php';

   //  </SCRIPT>";
// }
 }
    
catch (Exception $e) {
  //role back to using
    $conn->query("ROLLBACK");
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

            // echo "<SCRIPT>
            //    alert('Error: Party Not Inserted Successfully...');
            //    window.location.href='party.php';
            // </SCRIPT>";

           echo '<script>
                      swal({
                         title: "Product not Inserted",
                         text: "Soory Connect you Technical Team",
                         icon: "error",
                         button: "Back"
                          }).then(function() {
                              window.location = "product_add.php";
                          });
                    </script>';

    $file_name = basename(__FILE__);  

    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name ','$username','$content')";
    if ($conn->query($sql) === FALSE) { 
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>