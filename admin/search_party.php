<?php include('connect.php'); ?>
<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>


  <table id="employee_data" class="table table-hover" border="4"; >
<?php

$party_name=$_POST['party_name'];
 $query = "SELECT * FROM party WHERE party_name='$party_name'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
 echo '
  <div class="table-responsive">
    <tr>
    <th>Id</th>
     <th>Party Name</th>
     <th>Party Location</th>
     <th>Mobile No</th>
     <th>Address</th>
     <th>Contact Person</th>
     <th>pan</th>
     <th>Gstin</th>
     <th>Gst Type</th>
     <th>Acc No</th>
      <th>Acc Holder Name</th>
      <th>Ifsc Code</th>
      <th>bank</th>
      <th>Branch</th>
      <th>Party Code</th>
      <th>Update</th>
      <th>Delete</th>
     
    </tr>
 ';
 while($row = mysqli_fetch_array($result))

 {
  echo '
   <tr>
     <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
    
     <td>'.$row["party_name"].'<input type="hidden" name="party_name[]" value="'.$row["party_name"].'"></td>
     <td>'.$row["party_location"].'<input type="hidden" name="party_location[]" value="'.$row["party_location"].'"></td>
     <td>'.$row["mobile_no"].'<input type="hidden" name="mobile_no[]" value="'.$row["mobile_no"].'"></td>
     <td>'.$row["address"].'<input type="hidden" name="address[]" value="'.$row["address"].'"></td>
     <td>'.$row["contact_person"].'<input type="hidden" name="contact_person[]" value="'.$row["contact_person"].'"></td>
     <td>'.$row["pan"].'<input type="hidden" name="pan[]" value="'.$row["pan"].'"></td>
     <td>'.$row["gstin"].'<input type="hidden" name="gstin[]" value="'.$row["gstin"].'"></td>
     <td>'.$row["gst_type"].'<input type="hidden" name="gst_type[]" value="'.$row["gst_type"].'"></td>
     <td>'.$row["acc_no"].'<input type="hidden" name="acc_no[]" value='.$row["acc_no"].'></td>
     <td>'.$row["acc_holder_name"].'<input type="hidden" name="acc_holder_name[]" value='.$row["acc_holder_name"].'></td>
     <td>'.$row["ifsc_code"].'<input type="hidden" name="ifsc_code[]" value='.$row["ifsc_code"].'></td>
     <td>'.$row["bank"].'<input type="hidden" name="bank[]" value='.$row["bank"].'></td>
     <td>'.$row["branch"].'<input type="hidden" name="branch[]" value='.$row["branch"].'></td>
     <td>'.$row["party_code"].'<input type="hidden" id="party_code'.$row["id"].'" name="party_code[]" value='.$row["party_code"].'></td>
     <td>
      <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="Update" class="btn btn-info" />
    </td>
    
    <td>
      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
    </td>
    
               
   </tr>
        ';
       } 

      }
      else
      {
       echo 'Data Not Found';
      }

      ?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var party_code = $('#party_code'+id).val();
    if (confirm("Do you want to delete this party?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_party.php",
              data:'id='+id + '&party_code='+party_code ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>
