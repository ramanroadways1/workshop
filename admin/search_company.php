<?php include("connect.php"); ?>
<style>
    table {
      max-width: 1200px;
    }
    button {
	   background: green;
	   color: white;
	}
	input[type=radio]:checked ~ button {
	   background: green;
	}
</style>

<table id="employee_data" class="table table-hover" border="4"; >
<?php

$company=$_POST['company'];
$query = "SELECT * FROM company WHERE company='$company'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
	{
		 echo '
		  <div class="table-responsive">
		    <tr>
		    <th>Id</th>
		    <th>Company</th>
		    <th>Delete</th>
		     
		    </tr>
		 ';
		 while($row = mysqli_fetch_array($result))

		 {
		  echo '
		   <tr>
		     <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
		     <td>'.$row["company"].' <input type="hidden" id="company'.$row["id"].'" name="company[]" value='.$row["company"].'></td>
		    
		    <td>
		      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
		    </td>
		    
		               
		   </tr>
		  ';
		 } 

		}
		else
		{
		 echo 'Data Not Found';
		}

		?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var company = $('#company'+id).val();
    if (confirm("Do you want to delete this company?"))
    {
    	if(id!='')
	    {
	      $.ajax({
	            type: "POST",
	            url: "delete_company.php",
	            data:'id='+id + '&company='+company ,
	            success: function(data){
	                $("#result22").html(data);
	            }
	        });
	    }
	}
  }     
</script>
<div id="result22"></div>