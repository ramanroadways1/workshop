<html>
 <head><script src="swity_alert.js" type="text/javascript"></script>
</head>
<body>
</body>
</html>
<?php
include ("connect.php");
// $username = $conn->real_escape_string(htmlspecialchars($_POST['username']));
// $yes_no = $conn->real_escape_string(htmlspecialchars($_POST['yes_no']));
$party_name = $conn->real_escape_string(htmlspecialchars($_POST['party_name']));
$party_location = $conn->real_escape_string(htmlspecialchars($_POST['party_location']));
$mobile_no = $conn->real_escape_string(htmlspecialchars($_POST['mobile_no']));

$address = $conn->real_escape_string(htmlspecialchars($_POST['address']));
$contact_person = $conn->real_escape_string(htmlspecialchars($_POST['contact_person']));
$pan = $conn->real_escape_string(htmlspecialchars($_POST['pan']));
$gstin = $conn->real_escape_string(htmlspecialchars($_POST['gstin']));

$gst_type = $conn->real_escape_string(htmlspecialchars($_POST['gst_type']));
$bank = $conn->real_escape_string(htmlspecialchars($_POST['bank']));
$acc_no = $conn->real_escape_string(htmlspecialchars($_POST['acc_no']));
$acc_holder_name = $conn->real_escape_string(htmlspecialchars($_POST['acc_holder_name']));
$ifsc_code = $conn->real_escape_string(htmlspecialchars($_POST['ifsc_code']));
$branch = $conn->real_escape_string(htmlspecialchars($_POST['branch']));
    try{

        $conn->query("START TRANSACTION"); 


// $yes_no = $_POST['yes_no'];
// $party_name = $_POST['party_name'];
// $party_location= $_POST['party_location'];
// $mobile_no = $_POST['mobile_no'];
// $address = $_POST['address'];
// $contact_person = $_POST['contact_person'];
// $pan = $_POST['pan'];
// $gstin = $_POST['gstin'];
// $gst_type = $_POST['gst_type'];
// $bank = $_POST['bank'];
// $acc_no = $_POST['acc_no'];
// $acc_holder_name = $_POST['acc_holder_name'];
// $ifsc_code = $_POST['ifsc_code'];

// $branch= $_POST['branch'];


    $datevar= date('Y');
    $party_code1 =  "WPVIS".$datevar;

        $query= "SELECT party_code FROM party where party_code like '$party_code1%' order by id desc limit 1";
        // echo $query;
    $result = mysqli_query($conn, $query);
    if(mysqli_num_rows($result) > 0)  
      {
        $row = mysqli_fetch_array($result);
        $party_code = $row["party_code"];
        
        $last_digits = substr($party_code, -4);
        
        $next = $last_digits + 1;
       
        $next1 = str_pad($next, strlen($last_digits), '0', STR_PAD_LEFT);
        $p_c = $party_code1.$next1;
       
    }
    else
    { 
      $p_c =  $party_code1."0001";
      
    }
        //Remove in $yes_no in this query
    $sql = "insert into party(party_name,party_location,mobile_no,address,contact_person,  pan, gstin,gst_type, bank,branch, acc_no, acc_holder_name, ifsc_code,party_code) value ('$party_name','$party_location','$mobile_no','$address','$contact_person', '$pan','$gstin','$gst_type','$bank','$branch','$acc_no', '$acc_holder_name', '$ifsc_code','$p_c')";
    if($conn->query($sql) === FALSE) 
      { 
        throw new Exception("Code 002 : ".mysqli_error($conn));        
      }

    // echo $sql;
    $result = $conn->query($sql);
// echo "<SCRIPT>
//        alert('Party Inserted Successfully...');
//        window.location.href='party.php';

//     </SCRIPT>";
     $conn->query("COMMIT");
     echo '<script>
                    swal({
                         title: "Party Add Successfully...",
                          text: "Created Party",
                          type: "success"
                          }).then(function() {
                              window.location = "party_view.php";
                          });
                </script>';
                $file_name= basename(__FILE__);
                $type=1;
                $val="Party Add :"."Party Name-".$party_name.",Party Location-".$party_location;
                $username="Admin";               
                $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`error`,`type`) VALUES ('$file_name','$username','$val','$type')";
                  if ($conn->query($sql)===FALSE) {
                  echo "Party No Add Some Technical Issue";
                            
                                  }                

/*if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Record not cretaed";
}*/
  }
    
catch (Exception $e) {
  //role back to using
    $conn->query("ROLLBACK");
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

            // echo "<SCRIPT>
            //    alert('Error: Party Not Inserted Successfully...');
            //    window.location.href='party.php';
            // </SCRIPT>";

           echo '<script>
                      swal({
                         title: "Party Not Insert...",
                         text: "Soory Connect you Technical Team",
                         icon: "error",
                         button: "Back"
                          }).then(function() {
                              window.location = "party_view.php";
                          });
                    </script>';

    $file_name = basename(__FILE__); 
    $type=0;       
    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`,`type`) VALUES ('$file_name ','$username','$content','$type')";
    if ($conn->query($sql) === FALSE) { 
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
$conn->close();
?>