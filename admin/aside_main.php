<!-- <title> Vendor Management System - ADMIN </title> -->
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>V</b>MNGT</span>
      <span class="logo-lg"><b>Vendor</b>Management</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
     

    <?php  if (isset($_SESSION['admin'])) : ?>
      <p style="float:right; margin-right:20px; color: #fff;">Welcome <strong style="text-transform: uppercase;"><?php echo $_SESSION['admin']; ?></strong> <span style="color: #000;"> <b> | </b> </span>
        <strong style="text-transform: uppercase;"><?php echo $_SESSION['employee']; ?></strong> <span style="color: #000;"> <b> | </b> </span>
      <a href="logout.php" style="color: yellow;"> <i class="fa fa-sign-out" aria-hidden="true"></i>
 Logout</a> </p>
    <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <!-- <li>
          <a href="admin_login.php">
            <i class="fa fa-user"></i> <span>Admin</span>
          </a>
        </li> -->
        <!--  <li>
          <a href="inventory.php">
            <i class="fa-bar-chart "></i> <span>Inventory</span>
          </a>
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>update Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="party_view.php"><i class="fa fa-circle-o"></i>Party</a></li>
           <!--  <li><a href="party.php"><i class="fa fa-circle-o"></i>Party</a></li> -->
<!--<li><a href="product.php"><i class="fa fa-circle-o"></i>Product</a></li>
 -->            <li><a href="product_view.php"><i class="fa fa-circle-o"></i>Product</a></li>
            
              <!--   <li><a href="rate.php"><i class="fa fa-circle-o"></i>Rate</a></li> -->

                <li><a href="rate_view.php"><i class="fa fa-circle-o"></i>Rate</a></li>

                <!-- <li><a href="company.php"><i class="fa fa-circle-o"></i>Company</a></li> -->

            <li><a href="company_view.php"><i class="fa fa-circle-o"></i>Company</a></li>

            <!--   <li><a href="rack.php"><i class="fa fa-circle-o"></i>Rack</a></li> -->

            <li><a href="rack_view.php"><i class="fa fa-circle-o"></i>Rack</a></li>


          </ul>
        </li>
         
         <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Workshop(Admin)</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/index2.php"><i class="fa fa-circle-o"></i>Workshop(Admin)</a></li>
<!--             <li><a href="product.php"><i class="fa fa-circle-o"></i> Product Master</a></li>
            <li><a href="rate.php"><i class="fa fa-circle-o"></i> Rate Master</a></li> -->
          </ul>
        </li>

           

        <li>
<!--      <a href="generate_po_operation.php">
             <i class="fa fa-edit"></i> <span>Update Purchase Order</span>
          </a> -->
           <a href=" generate_po_operation_view.php">
              <i class="fa fa-file-text-o"></i> <span>Update Purchase Order</span>
          </a>
        </li>
          <li>
<!--           <a href="approve_po_detail.php">
            <i class="fa fa-edit"></i> <span>Purchase Order After Approve</span>
          </a> -->

          <a href="approve_po_detail_view.php">
                 <i class="fa fa-check-square-o"></i>  <span>Approved Purchase Order</span>
          </a>

        </li>
        <li>
          <a href="pay_detail.php">
           <i class="fa fa-paypal  "></i><span>Payment Detail</span>
          </a>
        </li>
          <li>
<!--           <a href="grn_operation.php">
            <i class="fa fa-edit"></i> <span>Update GRN</span>
          </a>-->
        <a href="grn_operation_view.php">
            <i class="fa   fa-newspaper-o"></i> <span>Update GRN</span>
        </a>

        </li> 
         <li>
          <a href="user_login.php">
           <i class="fa fa-user"></i><span>User Registration</span>
          </a>
        </li>
         <li>
          <a href="superadmin/admin_login.php">
           <i class="fa fa-user  "></i><span>Super Admin Login</span>
          </a>
        </li>
      </ul>
    </section>
  </aside>
