
 <?php  
 include("connect.php");
 $sql = "SELECT * from product";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Operation of Product</h1>
    </section>

    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Product Detail</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div align="right">
                <a href='product_add.php' type="submit" name="add" id="add" class="btn btn-info">Add New Product</a>
              </div><br>
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                          <th>Id</th>
                          <th>Product no</th>
                          <th>Company</th>
                          <th>Product name</th>
                          <th>Product type</th>
                          <th>Unit</th>
                          <th>Product Group</th>
                          <th>Sub</th>
                          <th>Product location</th>
                          <th>Quantity</th>
                          <th>Update</th>
                          <th>Delete</th>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                         <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
                         <td>'.$row["productno"].'<input type="hidden" id="productno'.$row["id"].'" name="productno[]" value="'.$row["productno"].'"></td>
                         <td>'.$row["company"].'<input type="hidden" name="company[]" value="'.$row["company"].'"></td>
                         <td>'.$row["productname"].'<input type="hidden" name="productname[]" value="'.$row["productname"].'"></td>
                         <td>'.$row["producttype"].'<input type="hidden" name="producttype[]" value="'.$row["producttype"].'"></td>
                         <td>'.$row["unit"].'<input type="hidden" name="unit[]" value="'.$row["unit"].'"></td>
                         <td>'.$row["product_group"].'<input type="hidden" name="product_group[]" value="'.$row["product_group"].'"></td>
                         <td>'.$row["sub"].'<input type="hidden" name="sub[]" value="'.$row["sub"].'"></td>
                         <td>'.$row["pro_loc"].'<input type="hidden" name="pro_loc[]" value='.$row["pro_loc"].'></td>
                         <td>'.$row["quantity"].'<input type="hidden" name="quantity[]" value='.$row["quantity"].'></td>     
                         <td>
                          <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="update" class="btn btn-info" />
                        </td>
                        
                        <td>
                          <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
                        </td>  
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
              <div id="result_main"></div>
              <div id="result"></div>
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
       <!--  <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 



<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_product.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>



<script type="text/javascript">
$(document).ready(function (e) {
$("#FormProductUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_product.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script>


<script>
  function DeleteModal(id)
  {
    var id = id;
    var productno = $('#productno'+id).val();
    if (confirm("Do you want to delete this product?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_product.php",
              data:'id='+id + '&productno='+productno ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>

<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>


<div id="myModal22" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Product</h4>
      </div>
      <form id="FormProductUpdate" action="update_product.php">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <!-- <div class="form-group"> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>
            <!-- </div> -->
            <div class="form-group">
                <b>Product Number</b>
                <input type="text" style="width: 100%;" id="productno" autocomplete="off" class="form-control" name="productno" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Company</b>
                <select name="company" style="width: 100%;" id="company" name="company" class="form-control"> 
                      <option disabled="disabled" id="company" selected="selected">Select Company</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT company FROM company");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['company'])?>" >
                        <?php 
                              echo ($row['company']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
              </div> 
               
              <div class="form-group">
                <b>Product Name</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="productname" name="productname" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Product Type</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="producttype" name="producttype" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Unit</b>
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="unit" name="unit" readonly="readonly" required>
              </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                  <b>Group</b>
                  <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="product_group" name="product_group" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Sub Group</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="sub" name="sub" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Product Location(Ahmedabad)</b>
                <select name="pro_loc" style="width: 100%;" id="pro_loc" class="form-control"> 
                    <option disabled="disabled" selected="selected">Select Rack Location</option>
                    <?php 
                      include ("connect.php");
                        $get=mysqli_query($conn,"SELECT rackno FROM rack");
                           while($row = mysqli_fetch_assoc($get))
                          {
                    ?>
                    <option value = "<?php echo($row['rackno'])?>" >
                      <?php 
                            echo ($row['rackno']."<br/>");
                      ?>
                    </option>
                      <?php
                      }
                    ?>
                </select>
              </div>
          </div>
          <!-- /.col -->
         
        </div>

        <!-- /.row -->

      </div>
      </div>

        <!-- <input id="myModal_id"> -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
        <!-- <button type="button" class="close" data-dismiss="modal">Close</button> -->
      </div>
      </form>
    </div>
  </div>
</div>

