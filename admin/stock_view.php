
 <?php  
 include("connect.php");
 $sql = "SELECT * FROM `outward_stock` WHERE avl_qty=0";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Stock Report</h1>
    </section>
    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Stock Detail</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div align="right">
                <a href='product_add.php' type="submit" name="add" id="add" class="btn btn-info">Add New Product</a>
              </div><br>
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                          <th>Id</th>
                          <th>Product no</th>
                          <th>Company</th>
                          <th>Product name</th>
                          <th>Product type</th>
                          <th>Quantity</th>

                       </tr>  
                  </thead>  
                  <?php  
                  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                         <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
                         <td>'.$row["productno"].'<input type="hidden" id="productno'.$row["id"].'" name="productno[]" value="'.$row["productno"].'"></td>
                         <td>'.$row["company"].'<input type="hidden" name="company[]" value="'.$row["company"].'"></td>
                         <td>'.$row["productname"].'<input type="hidden" name="productname[]" value="'.$row["productname"].'"></td>
                         <td>'.$row["producttype"].'<input type="hidden" name="producttype[]" value="'.$row["producttype"].'"></td>
                         <td>'.$row["avl_qty"].'<input type="hidden" name="avl_qty[]" value='.$row["avl_qty"].'></td>
                     
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
<!--               <div id="result_main"></div>
              <div id="result"></div> -->
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
       <!--  <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 



<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_product.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>

<div id="result22"></div>

<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>


