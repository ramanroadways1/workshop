
<?php  include("connect.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>


     <style type="text/css">
  @media print {
  button,header,footer {
    display: none !important;
  }
   #print_button{
    display: none;
  }
   #party{
     font-size: 16pt;
  }
   table, tr,body,h4,form  {
        height: auto;
        font-size: 16pt;
        font: solid #000 !important;
        }
         table {
       border: solid #000 !important;
        border-width: 1px 0 0 1px !important;
    }
    th, td {
        border: solid #000 !important;
        border-width: 0 1px 1px 0 !important;
    }
      body {
  zoom:50%; 
}

 
}
</style>


</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
 
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
       Approve Purchase Order</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <?php
                    $valuetosearch = $_POST['key1'];
                        $query = "SELECT * FROM insert_po where key1='$valuetosearch'";

                        $result = mysqli_query($conn, $query);
                        $l_u = 1;
                        $id_customer = 0;
                      ?>   
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                 
                    $party_name = $row['party_name'];
                  }
                    
                  ?>

               <td scope="row" style="display: none;">Party Name</td>
               <input type="text" readonly="readonly" id="party" class="form-control" value="<?php echo $party_name; ?>" >

              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-size: 13px; font-family: verdana;">  
                  <thead>  
                       <tr>  
                        <td scope="row" style="display: none;">date Purchase Id</td>
                           <td scope="row" style="display: none;">id</td>
                            <td>Product No</td>
                           <td>Product Name</td>
                              <td>Rate Master/rate</td>
                              <td>Rate/unit</td>
                              <td>Rate diff</td>
                              <td>Quantity</td>
                              <td>amount</td>
                               <td>Gst</td> 
                               <td>Gst Total</td> 
                               <td>Final Total</td> 
                       </tr>  
                  </thead> 
                    <?php
                    $valuetosearch = $_POST['key1'];
                        $query = "SELECT * FROM insert_po where key1='$valuetosearch'";

                        $result = mysqli_query($conn, $query);
                        $l_u = 1;
                        $id_customer = 0;
                      ?>   
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                 
                    $productno = $row['productno'];
                    $productname = $row['productname'];
                    $rate_master = $row['rate_master'];
                    $rate = $row['rate'];
                    $diff = $row['diff'];
                    $quantity = $row['quantity'];
                    $amount = $row['amount'];
                    $gst = $row['gst'];
                    $gst_amount = $row['gst_amount'];
                    $total1 = $row['total'];
                     $total = round($total1,2);
                    
                  ?>
                  <tr>
                   
                    <td ><?php echo $productno?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $productname?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $rate_master?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $rate?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $diff?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                    
                     <td ><?php echo $quantity?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $amount?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $gst?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $gst_amount?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                     <td ><?php echo $total?>
                      <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>" >
                    </td>
                    
                    
                    
                    
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  } 
                  ?>  
                </table>  
              </div>  
            </div>
             <div class="col-md-6">
                    <input type="button" style='margin-right:10%' color="Primary" class="btn btn-primary hide-from-printer" onclick="myFunction()" id="print_button" value="Print">
                 </div>  
          </div>
           <script>
                  $(document).ready(function() {

                      $('#print_button').click(function() {
                      
                         window.print();
                           location.reload(true);
                      });

                  });
                  </script> 
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 