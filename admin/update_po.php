<?php
// echo "success";
include("connect.php");

$id = $conn->real_escape_string(htmlspecialchars($_POST['id']));
$productno = $conn->real_escape_string(htmlspecialchars($_POST['productno']));
$productname = $conn->real_escape_string(htmlspecialchars($_POST['productname']));
$rate_master = $conn->real_escape_string(htmlspecialchars($_POST['rate_master']));
$rate = $conn->real_escape_string(htmlspecialchars($_POST['rate']));
$diff = $conn->real_escape_string(htmlspecialchars($_POST['diff']));
$quantity = $conn->real_escape_string(htmlspecialchars($_POST['quantity']));
$amount = $conn->real_escape_string(htmlspecialchars($_POST['amount']));
$gst = $conn->real_escape_string(htmlspecialchars($_POST['gst_value']));
$gst_type = $conn->real_escape_string(htmlspecialchars($_POST['gst_type']));
$gst_acc_type = $conn->real_escape_string(htmlspecialchars($_POST['gst_acc_type']));
$gst_amount = $conn->real_escape_string(htmlspecialchars($_POST['gst_amount']));
$total_amount = $conn->real_escape_string(htmlspecialchars($_POST['total_amount']));

  $admin = $_SESSION['admin'];
   $empcode = $_SESSION['empcode'];

// $id = $_POST['id'];
// $productno= $_POST['productno'];
// $productname= $_POST['productname'];
// $rate_master= $_POST['rate_master'];
// $rate= $_POST['rate'];
// $diff= $_POST['diff'];
// $quantity= $_POST['quantity'];
// $amount= $_POST['amount'];
// $gst= $_POST['gst_value'];
// $gst_type= $_POST['gst_type'];
// $gst_acc_type= $_POST['gst_acc_type'];
// $gst_amount= $_POST['gst_amount'];
// $total_amount= $_POST['total_amount'];
 try{

		$conn->query("START TRANSACTION"); //productno='$productno', productname='$productname', 

				$sql ="UPDATE insert_po SET rate_master='$rate_master', diff='$diff' ,rate='$rate',quantity='$quantity', amount='$amount', gst='$gst',gst_type='$gst_type',gst_acc_type='$gst_acc_type',gst_amount='$gst_amount', total='$total_amount' where id = '$id'";
				if($conn->query($sql) === FALSE) 
				  { 
				    throw new Exception("Code 001 : ".mysqli_error($conn));        
				  }

				/*echo $sql;*/
				// $conn->query("START TRANSACTION");
				// $sqld = $conn->query($sql);

				// $username="Admin";
				$file_name = basename(__FILE__);
				$type=1;
				$val="PO Updated:"." Rate:".$rate.", Quantity:".$quantity.", ProductNumber:".$productno.", ProductName:".$productname;	
    			$sqld = "INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$admin','$empcode','$val','$type')";
    				if ($conn->query($sqld) === FALSE) 
    				{ 
      					echo "Error: log not updated ";
    				}
    				
				 // }else{
				 // 	echo "<SCRIPT>
				 //       alert('Purchase Order not Updated.');
				 //       window.location.href='generate_po_opertaion_view.php';
					// </SCRIPT>";
				 // }

    			$conn->query("COMMIT");
				// if ($sqld == TRUE) 
				
				//  {
				  echo "<SCRIPT>
				       alert('PO Updated Successfully.');
				       window.location.href='generate_po_operation_view.php';
					</SCRIPT>";
} catch (Exception $e) {

		$conn->query("ROLLBACK");
		$content = htmlspecialchars($e->getMessage());
		$content = htmlentities($conn->real_escape_string($content));

		echo  "<SCRIPT>
					alert('Error: $content');
					window.location.href='generate_po_opertaion_view.php';
				</SCRIPT>";
		// $username="Admin";
		$file_name = basename(__FILE__); 
		$type=0;       
		$sql = "INSERT INTO `allerror`(`file_name`, `user_name`,`employee`,`error`,`type`) VALUES ('$file_name ','$username','$empcode','$content','$type')";
		if ($conn->query($sql) === FALSE) { 
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
}				 

  
?>