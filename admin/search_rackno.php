<?php include('connect.php'); ?>
<style>
    table {
      max-width: 1200px;
    }
    button {
	   background: green;
	   color: white;
	}
	input[type=radio]:checked ~ button {
	   background: green;
	}
</style>

<table id="employee_data" class="table table-hover" border="4"; >
<?php

$rackno=$_POST['rackno'];
$query = "SELECT * FROM rack WHERE rackno='$rackno'";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
	{
		 echo '
		  <div class="table-responsive">
		    <tr>
		    <th>Id</th>
		    <th>Rack Number</th>
		    <th>Delete</th>
		     
		    </tr>
		 ';
		 while($row = mysqli_fetch_array($result))

		 {
		  echo '
		   <tr>
		     <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
		     <td>'.$row["rackno"].' <input type="hidden" id="rackno'.$row["id"].'" name="rackno[]" value='.$row["rackno"].'></td>
		    
		    <td>
		      <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
		    </td>
		    
		               
		   </tr>
		  ';
		 } 

		}
		else
		{
		 echo 'Data Not Found';
		}

		?>
</table>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var rackno = $('#rackno'+id).val();
    if (confirm("Do you want to delete this rack number?"))
    {
    	if(id!='')
	    {
	      $.ajax({
	            type: "POST",
	            url: "delete_rackno.php",
	            data:'id='+id + '&rackno='+rackno ,
	            success: function(data){
	                $("#result22").html(data);
	            }
	        });
	    }
	}
  }     
</script>
<div id="result22"></div>