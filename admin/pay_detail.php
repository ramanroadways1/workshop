
<!DOCTYPE html>
<html>
<head>
  <?php 
  include("header.php");
     include("connect.php");

  include("aside_main.php");
  ?>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<style type="text/css">
    @media print {
       #search,#heading_grn,#button,#print,#fetch_grnno,#invoice_file{
      display: none;
    }
   header,footer,h1 {
      display: none !important;
    }
       body {
        margin:0;
        padding:0;
    }
  }
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <div class="content-wrapper">
      <section class="content-header">
        <h1>Show Payment Deatail</h1>
      </section>
      
        <section class="content">
       
         <form id="MyForm1">
            <div class="row">
              <div class="col-md-0"></div> 
                <div class="col-md-12">
                  <div class="box box-default">
                    <div class="box-header with-border">
                      <div class="col-md-8">
                        <div class="form-group">
                          <div class="col-md-3">
                              <b id="heading_grn">GRN Number</b>
                          </div>
                          <div class="col-md-7">
                            <input type="text" autocomplete="off" id="fetch_grnno" name="grnno" placeholder="Enter GRN Number..." class="form-control" required />
                          </div>
                          <div class="col-md-2">
                              <button type="submit" id="search" class="btn btn-search" ><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                    
                    </div>
                    </div>
                  </div>
                </div> 
              </div>
            </form>

            <div class="table-responsive">
                 <div id="result" style="background: #fff; padding: 20px; border: 1px solid #ccc;"></div> 
            </div>
            
         </section>

      <div class="col-sm-offset-2 col-sm-8">
      </div>
        
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>


<script>
$(document).ready(function (e) {
$("#MyForm1").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "search_grn.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 

<script type="text/javascript">     
$(function()
{    
$( "#fetch_grnno" ).autocomplete({
source: 'fetch_grnno_autocomplete.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});

</script>


