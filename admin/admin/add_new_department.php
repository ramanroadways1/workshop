<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>


<!DOCTYPE html>
<html>
<head>
 <?php
  include('header.php');
 
  ?>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php  include('aside_main.php'); ?>
  
  <div class="content-wrapper">
    <section class="content-header">
     
      <h1>Add Labour</h1>
    </section>

    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add Departent</h3>
        </div>
       <button style="width: 12%;float: right;  margin-right: 15px;" class="btn btn-primary" onclick="window.location.href='operation_on_department.php'">Avl department</button>

        <div class="box-body">
          <form method="post" id="form" action="insert_department.php">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-2">
                <label>Add department</label>
              </div>
              <div class="col-md-4">
                <input type="text" autocomplete="off" id="department2" name="department"  class="form-control" required placeholder="Enter department" />
              </div>
              <script>
                  $("#department2").keyup(function(){
                    var department2=$("#department2").val();
                    var res = department2.replace("&", "and");
                    $("#department2").val(res);
                  });
                </script>
              <div class="col-md-4"></div>
            </div>
            <br>
           
            <center>
              <button type="submit" class="btn btn-info" >Submit</button>
            </center>
          </form>
        </div>
    </section>
</div>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">RRPL</a>.</strong> All rights
        reserved.
      </footer>
 <div class="control-sidebar-bg"></div> 
</body>
</html>
