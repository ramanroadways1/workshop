
 
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Registered Trucks</h3>
       
      </div>
        <!-- /.box-header -->
         <!-- <div>
              <button type="button" style="width: 5%;float: right;  margin-right: 15px;" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" >Add</button>
            </div><br><br> -->
            <div id="result1"></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>
                          <th>Id</th>
                          <th>Truck Number</th>
                          <th>Company</th>
                          <th>wheeler</th>
                          <th>model Number</th>
                          <th>Tag acno  </th>
                          <th>Tag srno</th>
                          <th>driver code</th>
                          <th>sal per day</th>
                          <th>superv id</th>
                          <th>nrr</th>

                        </tr>
                  </thead>  
                   <?php  
                      include("connect.php");
                      $query = "SELECT * FROM own_truck";
                      $result = mysqli_query($conn, $query);;
                      $result = mysqli_query($conn,$query);
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $tno = $row['tno'];
                    $comp=$row['comp'];
                    $wheeler = $row['wheeler'];
                    $model = $row['model'];
                    $tag_acno=$row['tag_acno'];
                    $tag_srno = $row['tag_srno'];
                    $driver_code = $row['driver_code'];
                    $sal_per_day=$row['sal_per_day'];
                    $superv_id=$row['superv_id'];
                    $nrr=$row['nrr'];

                  ?>
                  <tr>
                    <td><?php echo $id?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id; ?>" id="id">
                    </td>
                    <td><?php echo $tno?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $tno; ?>" id="tno">
                    </td>
                    <td ><?php echo $comp?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $comp; ?>" id="truck_driver">
                    </td>
                    <td><?php echo $wheeler?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $wheeler; ?>" id="tno">
                    </td>
                   
                    <td><?php echo $model?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $model; ?>" id="tno">
                    </td>
                    <td ><?php echo $tag_acno?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $tag_acno; ?>" id="truck_driver">
                    </td>
                    <td><?php echo $tag_srno?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $tag_srno; ?>" id="tno">
                    </td>
                    <td ><?php echo $driver_code?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $driver_code; ?>" id="truck_driver">
                    </td>
                    
                    <td ><?php echo $sal_per_day?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $sal_per_day; ?>" id="truck_driver">
                    </td>
                    <td><?php echo $superv_id?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $superv_id; ?>" id="tno">
                    </td>
                    <td ><?php echo $nrr?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $nrr; ?>" id="truck_driver">
                    </td>
                    <script>
                      function DeleteModal(truck_no)
                      { var truck_no = truck_no;
                        
                        if (confirm("Do you want to delete this Truck Permanently..?"))
                        {
                          if(truck_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_registred_truck.php",
                                  data:'truck_no='+truck_no,
                                  success: function(data){
                                      $("#result22").html(data);
                                      alert(data)
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                            <script type="text/javascript">
                               function EditModal(id)
                               {
                                jQuery.ajax({
                                    url: "fetch_registred_truck.php",
                                    data: 'id=' + id,
                                    type: "POST",
                                    success: function(data) {
                                       //alert(data)
                                    $("#result_main").html(data);
                                  },
                                        error: function() {}
                                    });
                               document.getElementById("modal_button1").click();
                                }
                            </script><div id="result_main"></div>
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
             
        </div>
           
      </div>
        
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form  id="AddCompany" autocomplete="off">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Truck</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-4">
                <input type="hidden" name="truck_no" >
            </div>
            <div class="col-md-8">
              <input type="hidden" style="width: 100%;"  autocomplete="off" class="form-control" id="sno" placeholder="sno"/>
            </div>
            <div class="col-md-4">
                <b>Truck Number</b>
            </div>
            <div class="col-md-12">
              <input type="text" style="width: 100%;" class="form-control" onkeyup="MyFunc()" id="truck_no1" name="truck_no" placeholder="truck number" required/>
            </div>
            <div id="result22"></div>
             <script>
              function MyFunc()
              {
                var truck_no = $('#truck_no1').val();
                 var truck_reg_no = $('#truck_reg_no').val();
                if(truck_no!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chktruck_no.php",
                      data:'truck_no='+truck_no ,
                      success: function(data){
                          $("#result22").html(data);
                      }
                      });
                }
              }     
            </script>
            <br><br><br>
            <div class="col-md-4">
                <b>truck Driver</b>
            </div>
            <div class="col-md-12">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_driver1" placeholder="truck driver" required="required" />
            </div><br><br><br><br>

            <div class="row-md-2">

              <div class="col-md-6">
                <b>truck Registration no.</b>
                <input type="text" style="width: 100%;" autocomplete="off" onkeyup="MyFunc()" class="form-control"  name="truck_reg_no" placeholder="Date" required="required" />
              </div>
              <div class="col-md-6">
                <b>Truck Registration date</b>
                   <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_no1" placeholder="Date" required="required" />
              </div>
            </div><br><br><br><br>

            <div class="col-md-4">
                <b>Vehicle Model</b>
            </div>
            <div class="col-md-12">

              <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_no1" placeholder="truck no" required="required" />
            </div><br><br><br>

            <div class="col-md-4">
                <b>Vehicle Make</b>
            </div>
            <div class="col-md-12">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_driver1" placeholder="truck driver" required="required" />
            </div><br><br><br><br>

            <div class="row-md-2">

              <div class="col-md-6">
                <b>Chassis no</b>
               <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_driver1" placeholder="Chassis no." required="required" /> 
               
                </textarea>
              </div>
              <div class="col-md-6">
                <b>Engine number</b>
                  <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_driver1" placeholder="Engine number" required="required" />
               
                </textarea>
              </div>
            </div>

             <div class="col-md-4">
                <b>Trolly Chassis no</b>
            </div>
            <div class="col-md-12">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control"  name="truck_driver1" placeholder="truck driver" required="required" />
            </div><br><br><br>

          

             
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#AddCompany").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_new_truck.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
              alert(data)
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>



<script type="text/javascript">     
$(function()
{    
$( "#truck_no" ).autocomplete({
source: 'autocomplete_truck_no.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});
</script>

<script>
  $(document).ready(function (e) {
  $("#MyForm1").on('submit',(function(e) {
  e.preventDefault();
      $.ajax({
      url: "fetch_truck_detail_for_inspection.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
          $("#r1").html(data);
          //$('#r1').text("Post save as draft");  
          setInterval(function(){  
               $('#r1').text('');  
          }, 5000); 
          //$('#truck_no').val('');
      },
      error: function() 
      {} });}));});
 </script>