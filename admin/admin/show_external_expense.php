
<!DOCTYPE html>
<html>
<head>
 <?php
  include('header.php');
 
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}
</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content-header">
   
      <h1>
      External Job Card
     </h1>
    </section>

    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">External Job Card</h3>

          
        </div>
  
        <div class="box-body">

           <form  method="post" id="MyForm1">
              <div class="row">
               <div class="col-md-8">
                <div class="form-group">
                  <div class="col-md-2">
                    <b>External Job Card</b>
                  </div>
                  <div class="col-md-8">
                    <input type="text" autocomplete="off" id="job_card_no" name="job_card_no" placeholder="Enter Jobcard Number.." class="form-control"  required />
                  </div>
                  <div class="col-md-2">
                      <button type="submit" class="btn btn-search" ><i class="fa fa-search"></i></button>
                      
                  </div>
                 
                </div>
              </div>
           </div><br>
           <script>
        function myFunction() {
          window.print();
        }
       </script>
           <script>
          $(document).ready(function (e) {
          $("#MyForm1").on('submit',(function(e) {
          e.preventDefault();
              $.ajax({
              url: "fetch_external_jobcard_expense.php",
              type: "POST",
              data:  new FormData(this),
              contentType: false,
              cache: false,
              processData:false,
              success: function(data)
              {
                  $("#r1").html(data);
              },
              error: function() 
              {} });}));});
         </script>
         
         </form>
      
         <div id="r1"></div>  
         
        </div>
      </div>
    </section>
  </div><footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">RRPL</a>.</strong> All rights
        reserved.
      </footer>

<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
<div id="result_main"></div>
<script type="text/javascript">     
$(function()
{    
$( "#job_card_no" ).autocomplete({
source: 'autocomplete_external.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
alert('External Job Card does not exists.');
} 
},
focus: function (event, ui) {  return false;
}});});
</script>



