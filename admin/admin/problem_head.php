<style>
    table {
      max-width: 1200px;
    }

    button {
   background: green;
   color: white;
}

input[type=radio]:checked ~ button {
   background: green;
}
  
  </style>


<!DOCTYPE html>
<html>
<head>
 <?php
  include('header.php');

  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>



</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content-header">
    
    </section>

    <section class="content">
      
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Update Problem Record</h3>
        </div>
        <div class="box-body">
          <table id="complain_table" style="font-size: 13px; font-family: verdana;" class="table table-hover" border="4"; >
           <tbody>
           <tr>
                <th>SNo</th>
                <th>Problem</th>
                <th>Department</th>
                <th>Mistry</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
                </tbody>
                <?php 
                include("connect.php");

              $query = "SELECT * from problem_record";
              $result = mysqli_query($conn,$query);
              $val = 1;
              $id_customer = 0;
               while($row = mysqli_fetch_array($result))
             {
                $id=$row['id'];
                //$sno=$row['sno'];
                $problem = $row['problem'];
                $department = $row['department'];
                $mistry = $row['mistry'];
            ?>
            <tr>
               
        <td style="display: none;">
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>
                 <td><?php echo $val?>
                 
                </td> 
                <!-- <td style="display: none;"><?php echo $sno?>
                  <input  type="hidden" readonly="readonly" name="sno[]"  value="<?php echo $sno; ?>">
                </td> -->
                  <td><?php echo $problem?>
                  <input type="hidden" readonly="readonly" name="problem[]" id="problem<?php echo $id; ?>" value="<?php echo $problem; ?>">
                </td>
                 <td><?php echo $department?>
                  <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
                </td>
                <td><?php echo $mistry?>
                  <input  type="hidden" readonly="readonly" name="mistry[]" id="mistry2<?php echo $id; ?>" value="<?php echo $mistry; ?>">
                </td>
                <td>
               <!--    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" onclick="EditModal(<?php echo $sno; ?>)" data-target="#myModal">Open Modal</button> -->
                     <input type="button" onclick="EditModal(<?php echo $sno; ?>)" data-target="#myModal" name="update" value="update" class="btn btn-info btn btn-sm" />
                      </td>
                    <td>
                      <input type="button" onclick="DeleteModal('<?php echo $sno;?>')" name="sno" value="Delete" class="btn btn-danger btn btn-sm" />
                    </td>
              
              <?php $val++; } ?>
            </tr>
            <script>
                      function DeleteModal(sno)
                      {
                        //alert(id);
                        var sno = sno;
                       /* var key1 = $('#key1'+id).val();*/
                        if (confirm("Do you want to delete this Problem..?"))
                        {
                          if(sno!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_complain_record.php",
                                  data:'sno='+sno  ,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
            </script> <div id="result22"></div>

            <script type="text/javascript">
$(document).ready(function (e) {
$("#FormProblemUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_problem.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {    alert(data)
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 

          </table><br>
          <div>
           <!--  <input type="button" value="Add Complain" class="btn btn-info add-new" onclick="addComplain('complain_table')" /> -->
             
              <button type="button" style="width: 5%;" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" >Add</button>
            </div>
            <div id="result1"></div><br> 
          <br>
          
        </div>
      </div>
    </section>
  </div>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">RRPL</a>.</strong> All rights
        reserved.


        <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
      </footer>

  <div class="control-sidebar-bg"></div>
</body>
</html>
<script type="text/javascript">
   function EditModal(sno)
                {
                  //alert(prob_id)
                  jQuery.ajax({
                      url: "fetch_problem_record_model.php",
                      data: 'sno=' + sno,
                      type: "POST",
                      success: function(data) {
                         //alert(data)
                      $("#result_main").html(data);
                    },
                          error: function() {}
                      });
                 document.getElementById("modal_button1").click();
                  
                  //$('#myModal22').val(prob_id);

                  }

</script>
<!-- <script type="text/javascript">
$(document).ready(function (e) {
$("#FormProblemUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_problem.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {    alert(data)
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script>  -->


<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

 <div id="myModal22" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Problem Record</h4>
      </div>
      <form id="FormProblemUpdate" method="Post" action="update_problem.php">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-12">
           
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="sno" name="sno1"/>
            <!-- </div> -->
            <div class="form-group">
              <div class="col-md-2">
                              <b>Problem<font color="red">*</font></b>
                          </div><br>
              <div class="col-md-10">
              <input type="text" style="width: 100%;"  autocomplete="off" class="form-control" id="problem" name="problem1"/>
              </div>
            </div><br><br>
              <div class="col-md-2">
                              <b>Department<font color="red">*</font></b>
                          </div><br>
                          <div class="col-md-10">
                            <select name="department1" id="department" style="width: 100%;" id="mistry" class="form-control" required="required" class="demoInputBox" onChange="getmistry(this.value);">
                                    <option value="">Select department</option>
                                    <?php
                                    $sql1="SELECT * FROM labour group by department";
                                         $results=$conn->query($sql1); 
                                    while($rs=$results->fetch_assoc()) { 
                                    ?>
                                    <option value="<?php echo $rs["department"]; ?>"><?php echo $rs["department"]; ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
                                 <script>
                                      function getmistry(val) {
                                          $.ajax({
                                          type: "POST",
                                          url: "dependent_labour.php",
                                          data:'department='+val,
                                          success: function(data){
                                        
                                            $("#mistry").html(data);
                                          }
                                          });
                                        }
                                        </script>
                                  </div><br><br><br>


                          <div class="col-md-2">
                              <b>Mistry<font color="red">*</font></b>
                          </div><br>
                          <div class="col-md-10">
                            <select name="mistry" style="width: 100%;" id="mistry" class="form-control" required="required"> 
                                <option value="">Select department first</option>
                            </select>
                             </div>
            
      

        
          </div>
          <!-- /.col -->
        </div>

        <!-- /.row -->

      </div>
      </div>
  

        <!-- <input id="myModal_id"> -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
        <!-- <button type="button" class="close" data-dismiss="modal">Close</button> -->
      </div>
      </form>
    </div>
  </div>
</div>
<div id="result_main"></div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <form  id="AddCompany">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add problem</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <input type="hidden" name="truck_no" >
            </div><?php
                      /*  include("connect.php");
                        $sql = "SELECT  max(id, firstname, lastname FROM MyGuests";
                         $result = $conn->query($sql);
                         if ($result->num_rows > 0) {
    // output data of each row
                          while($row = $result->fetch_assoc()) {
                          }
                        }*/
                        ?>
            <div class="col-md-10">
              <input type="hidden" style="width: 100%;"  autocomplete="off" class="form-control" id="sno" placeholder="sno"/>
            </div>
            <div class="col-md-2">
                <b>Problem</b>
            </div>
            <div class="col-md-10">
              <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="problem" name="problem" placeholder="problem" required="required" />
            </div><br><br><br>
           <div class="col-md-2">
                              <b>Department<font color="red">*</font></b>
                          </div>
                          <div class="col-md-10">
                            <select name="department" id="department4" style="width: 100%;"  class="form-control" required="required" class="demoInputBox" onChange="getmistry2(this.value);">
                                    <option value="">Select department</option>
                                    <?php
                                    $sql1="SELECT * FROM labour group by department";
                                         $results=$conn->query($sql1); 
                                    while($rs2=$results->fetch_assoc()) { 
                                    ?>
                                    <option value="<?php echo $rs2["department"]; ?>"><?php echo $rs2["department"]; ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
                                 <script>
                                      function getmistry2(val) {
                                          $.ajax({
                                          type: "POST",
                                          url: "dependent_labour2.php",
                                          data:'department4='+val,
                                          success: function(data){
                                       
                                            $("#mistry4").html(data);
                                          }
                                          });
                                        }
                                        </script>
                                  </div><br><br><br>


                          <div class="col-md-2">
                              <b>Mistry<font color="red">*</font></b>
                          </div>
                          <div class="col-md-10">
                            <select name="mistry" style="width: 100%;" id="mistry4" class="form-control" required="required"> 
                                <option value="">Select department first</option>
                            </select>
                             </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      <script type="text/javascript">
        $(document).ready(function (e) {
        $("#AddCompany").on('submit',(function(e) {
        e.preventDefault();
            $.ajax({
            url: "insert_inspection_problem.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
              alert(data)
               location.reload(); 
                $("#result").html(data);
            },
            error: function() 
            {} });}));});
        </script>


    </div>
  </div>



<script type="text/javascript">     
$(function()
{    
$( "#truck_no" ).autocomplete({
source: 'autocomplete_truck_no.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});
</script>

<script>
  $(document).ready(function (e) {
  $("#MyForm1").on('submit',(function(e) {
  e.preventDefault();
      $.ajax({
      url: "fetch_truck_detail_for_inspection.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
          $("#r1").html(data);
          //$('#r1').text("Post save as draft");  
          setInterval(function(){  
               $('#r1').text('');  
          }, 5000); 
          //$('#truck_no').val('');
      },
      error: function() 
      {} });}));});
 </script>