<?php 
  session_start(); 

  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }
?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>RRPL</b></span>
      <span class="logo-lg"><b>RRPL</b> Maintenance</span>
    </a>
<!--     <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
        <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
        <h3>
          <?php 
            echo $_SESSION['success']; 
            unset($_SESSION['success']);
          ?>
        </h3>
      </div>
    <?php endif ?>

    <?php  if (isset($_SESSION['admin'])) : ?>
      <p style="float:right; margin-right:20px;">Welcome <strong><?php echo $_SESSION['admin']; ?></strong>
      <a href="logout.php" style="color: red;">logout</a> </p>
    <?php endif ?>
    
    </nav> -->
        <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
<!--          <?php
          $demo=$_SESSION['admin'];
          $demo1=$_SESSION['employee'];
          print_r($demo);
          print_r($demo1)
    
      ?> -->

    <?php  if (isset($_SESSION['admin'])) : ?>
      <p style="float:right; margin-right:20px; color: #fff;">Welcome <strong style="text-transform: uppercase;"><?php echo $_SESSION['admin']; ?></strong> <span style="color: #000;"> <b> | </b> </span>
        <strong style="text-transform: uppercase;"><?php echo $_SESSION['employee']; ?></strong> <span style="color: #000;"> <b> | </b> </span>
        <a href="../index2.php" style="color: #FFFF00;">Back(Admin[Vendor])</a> </p><br>
    <!--<a href="logout.php" style="color: yellow;"> <i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>-->
</p>
    <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li>
          <a href="index2.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
       <!--   <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="show_available_department.php"><i class="fa fa-circle-o"></i>Avalable Department</a></li>
             <li><a href="add_new_truck.php"><i class="fa fa-circle-o"></i>Avalable Trucks</a></li>
              <li><a href="registered_driver.php"><i class="fa fa-circle-o"></i>Registered Driver</a></li>
            <li><a href="show_available_product.php"><i class="fa fa-circle-o"></i>Avalable Product</a></li>
            <li><a href="show_add_product_jobcard.php"><i class="fa fa-circle-o"></i>Issue Product in internal job</a></li>
            <li><a href="show_external_issue_product.php"><i class="fa fa-circle-o"></i>Issue Product in external job</a></li>
             <li><a href="inspection_inventory.php"><i class="fa fa-circle-o"></i>Inspection Record</a></li>
              <li><a href="show_internal_jobcard.php"><i class="fa fa-circle-o"></i>Internal Job Card</a></li>
               <li><a href="show_external_jobcard.php"><i class="fa fa-circle-o"></i>External Job Card</a></li>
          </ul>
        </li> -->


        <li>
          <!-- <a href="add_new_truck.php">
            <i class="fa fa-edit"></i> <span>Registered Truck</span>
          </a>
        </li>
        <li>
          <a href="registered_driver.php">
            <i class="fa fa-edit"></i> <span>Registered Driver</span>
          </a>
        </li> -->
         <li>
          <a href="add_labour.php">
            <i class="fa fa-edit"></i> <span>Add Labour</span>
          </a>
        </li>
        <li>
          <a href="add_new_department.php">
            <i class="fa fa-edit"></i> <span>Add Department</span>
          </a>
        </li>
         <li>
          <a href="update_truck_driver.php">
            <i class="fa fa-edit"></i> <span>Update Truck Detail</span>
          </a>
        </li>
        <li>
          <a href="update_inspection.php">
            <i class="fa fa-edit"></i> <span>Update Inspection</span>
          </a>
        </li>
         <li>
          <a href="found_internal_jobcard.php">
            <!-- <a href="internal_job_card_admin_view.php"> -->
            <i class="fa fa-edit"></i> <span>Update Internal JobCard</span>
          </a>
        </li>
        
        <li>
          <!-- <a href="external_job_card.php"> -->
          <a href="found_ext_jobcard_view.php">
            <i class="fa fa-edit"></i> <span>Update External JobCard</span>
          </a>
        </li>

        <li>
          <a href="problem_head.php">
            <i class="fa fa-laptop"></i> <span>Update Problem Head</span>
          </a>
        </li>

       <li>
          <a href="found_internal_jobcardupdate.php">
            <i class="fa fa-laptop"></i> <span>Internal Jobcard Update After Done</span>
          </a>
        </li>
       

<!--         <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="party.php"><i class="fa fa-circle-o"></i> Party Master</a></li>
            <li><a href="product.php"><i class="fa fa-circle-o"></i> Product Master</a></li>
            <li><a href="rate.php"><i class="fa fa-circle-o"></i> Rate Master</a></li>
            <li><a href="company.php"><i class="fa fa-circle-o"></i> Company Master</a></li>
          </ul>
        </li> -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Inspection</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="inspection_report.php"><i class="fa fa-circle-o"></i> Generate Inspection</a></li>
            <li><a href="search_by_po.php"><i class="fa fa-circle-o"></i> Search Purchase Order</a></li>
          </ul>
        </li>
       
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Job Cards</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="internal_job_cards.php"><i class="fa fa-circle-o"></i>Internal Job Cards</a></li>
            <li><a href="external_job_cards.php"><i class="fa fa-circle-o"></i>External Job Cards</a></li>
            <li><a href="invoice_payment.php"><i class="fa fa-circle-o"></i>Do Payment</a></li>
          </ul>
        </li> -->

      </ul>
    </section>
  </aside>