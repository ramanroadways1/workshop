 
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include('aside_main.php');?>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Truck Record</h1>
    </section>

    <section class="content">

     
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Truck Record</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <?php  
                     include("connect.php");
                     $sql = "SELECT department.*,emp_data.empname from department left join emp_data on department.employee=emp_data.empcode";
                    $result = $conn->query($sql);
                     ?>  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td style="display: none;">id</td>  
                            <td>Department</td> 
                            <td>Employee Name</td> 
                            <td>Update</td>  
                           <!--  <td>Delete</td>  -->
                       </tr>    
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                      $id=$row['id'];
                      $department = $row['department'];
                      $empname = $row['empname'];   
                  
                  ?>  
                  <tr>
                   <td style="display: none;"><?php echo $id?>
                       <input type="hidden" id="id<?php echo $id; ?>" name="id[]"  value='<?php echo $id; ?>' >
                    </td>
                    <td><?php echo $department?>
                       <input  type="hidden" readonly="readonly" name="department[]" id="department<?php echo $id; ?>" value="<?php echo $department; ?>">
                    </td>

                    <td><?php echo $empname?>
                       <input  type="hidden" readonly="readonly" name="empname" id="empname<?php echo $id; ?>" value="<?php echo $empname; ?>">
                    </td>

                    <td>
                     <input type="button" onclick="EditModal(<?php echo $id; ?>)" name="update" value="update" class="btn btn-info" />
                    </td>
<!--                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="sno" value="Delete" class="btn btn-danger" />
                    </td> -->
                 
                  </tr>
                    <?php } ?> 
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="add_new_department.php" class="btn btn-sm btn-info btn-flat pull-left">Place New department</a>
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

 <script type="text/javascript">
   function EditModal(id)
                {
                  jQuery.ajax({
                      url: "fetch_department_model.php",
                      data: 'id=' + id,
                      type: "POST",
                      success: function(data) {
                         //alert(data)
                      $("#result_main").html(data);
                    },
                          error: function() {}
                      });
                 document.getElementById("modal_button1").click();
                  
                  //$('#myModal22').val(prob_id);

                  }

</script>
<script>
                      function DeleteModal(id)
                      {
                        //alert(id);
                        var id = id;
                       /* var key1 = $('#key1'+id).val();*/
                        if (confirm("Do you want to delete this Department..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_department.php",
                                  data:'id='+id  ,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
 </script> <div id="result22"></div>

<div id="result_main"></div>
<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

 <div id="myModal22" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Problem Record</h4>
      </div>
      <form id="FormProblemUpdate" method="Post" action="update_department.php">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-12">
           
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>
            <!-- </div> -->
            
             <div class="form-group">
              <b>Department</b><br>
                 <input type="text" style="width: 100%;"  autocomplete="off" class="form-control" id="department" required="required" name="department"/>
            </div>
            
        
          </div>
          <!-- /.col -->
        </div>

        <!-- /.row -->

      </div>
      </div>
  

        <!-- <input id="myModal_id"> -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
        <!-- <button type="button" class="close" data-dismiss="modal">Close</button> -->
      </div>
      </form>
    </div>
  </div>
</div>

