
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php");
      include('aside_main.php');
         // include("../connect.php");

  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
   $empcode = $_SESSION['empcode'];
   // $admin = $_SESSION['admin'];
   // $employee = $_SESSION['employee'];
   ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php ?>
  <div class="content-wrapper">
   
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On Labours</h3>
        <?php   $admin = $_SESSION['admin'];
         $employee = $_SESSION['employee'];
// print_r($employee);
// print_r($admin);
   ?>
        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>
        <!-- /.box-header -->
         <div>
              <button type="button" style="width: 5%;float: right;  margin-right: 15px;" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" >Add</button>
            </div><br><br>
            <div id="result1"></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>
                          <th scope="row">ID</th>
                          <th>Department</th>
                          <th class="col-md-3">Mistry</th>
                           <th class="col-md-3">Branch Name</th>
                           <th class="col-md-3">Employee Name</th>
                          <th class="col-md-3">Delete</th>
                        </tr>
                  </thead>  
                   <?php  
                      include("connect.php");
                      $query = "SELECT labour.*,emp_data.empname FROM  labour LEFT JOIN emp_data on labour.employee=emp_data.empcode";
                      $result = mysqli_query($conn, $query);;
                      $result = mysqli_query($conn,$query);
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $department = $row['department'];
                    $mistry=$row['mistry'];
                    $username=$row['username'];

                    $empname=$row['empname'];
                  ?>
                  <tr>
                    <td><?php echo $id?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id; ?>" id="id">
                    </td>
                    <td><?php echo $department?>
                      <input  type="hidden" readonly="readonly" name="department[]" value="<?php echo $department; ?>" id="department">
                    </td>
                    <td ><?php echo $mistry?>
                      <input  type="hidden" readonly="readonly" name="mistry[]" value="<?php echo $mistry; ?>" id="mistry">
                    </td>
                    <td ><?php echo $username?>
                      <input  type="hidden" readonly="readonly" name="username[]" value="<?php echo $username; ?>" id="username">
                    </td>
                    
                    <td ><?php echo $empname?>
                      <input  type="hidden" readonly="readonly" name="empname" value="<?php echo $empname; ?>" id="empname">
                    </td>

                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="delete" value="Delete" class="btn btn-danger" />
                    </td>

                    <script>
                      function DeleteModal(id)
                      {
                       var id = id;
                        var mistry = $('#mistry'+id).val();
                      
                        if (confirm("Do you want to delete this Mistry..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_labour.php",
                                  data:'id='+id + '&mistry='+mistry,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                           
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<div id="result_main"></div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form  id="AddCompany" action="insert_labour.php" method="post">
      <div class="modal-content">
        <div class="modal-header">
                  <?php   $admin = $_SESSION['admin'];
                  $employee = $_SESSION['employee'];
                   $empcode = $_SESSION['empcode'];
// print_r($employee);
// print_r($empcode);
   ?>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Labour</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="col-md-2">
                <input type="hidden" name="truck_no" >
            </div><?php
                      /*  include("connect.php");
                        $sql = "SELECT  max(id, firstname, lastname FROM MyGuests";
                         $result = $conn->query($sql);
                         if ($result->num_rows > 0) {
    // output data of each row
                          while($row = $result->fetch_assoc()) {
                          }
                        }*/
                        ?>
            <div class="col-md-10">
              <input type="hidden" style="width: 100%;"  autocomplete="off" class="form-control" id="sno" placeholder="sno"/>
            </div>
           
            <div class="col-md-2">
                <b>Mistry</b>
            </div>
            <div class="col-md-10">
               <input type="text" style="width: 100%;"  name="mistry" required="required" autocomplete="off" class="form-control" id="mistry2" placeholder="Mistry"/>
            </div><br><br><br>
             <script>
                  $("#mistry2").keyup(function(){
                    var mistry2=$("#mistry2").val();
                    var res = mistry2.replace("&", "and");
                    $("#mistry2").val(res);

                    //alert(mistry2);
                    /*var mistry2=$("#mistry2").val();
                    var res = mistry2.replace("&", "AND");
                   // var res1 = product.replace('"', "''");
                    $("#mistry").val(res);*/
                    //$("#product").val(res1);
                  });
                </script>
                 <div class="col-md-2">
                <b>Branch</b>
            </div>
            <div class="col-md-10">
              <select name="branch" style="width: 100%;" id="branch" class="form-control"> 
                  <option disabled="disabled" id="branch" selected="selected">Select Branch</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT username FROM users");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['username'])?>" >
                        <?php 
                              echo ($row['username']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
            </div><br><br><br>
             <div class="col-md-2">
                <b>Department</b>
            </div>
            <div class="col-md-10">
              <select name="department" style="width: 100%;" id="department" class="form-control"> 
                  <option disabled="disabled" id="department" selected="selected">Select department</option>
                      <?php 
                        include ("connect.php");
                          $get=mysqli_query($conn,"SELECT department FROM department");
                             while($row = mysqli_fetch_assoc($get))
                            {
                      ?>
                      <option value = "<?php echo($row['department'])?>" >
                        <?php 
                              echo ($row['department']."<br/>");
                        ?>
                      </option>
                        <?php
                        }
                      ?>
                  </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </form>
      

    </div>
  </div>
