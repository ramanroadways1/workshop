
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

     
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On Truck Is in Inspection</h3>
        
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>
                          <th>Id</th>
                          <th>Truck Number</th>
                          <th>Truck Driver</th>
                          <th>Inspection No</th>
                          <th>Km Reading</th>
                          <th>Update</th>
                          <th>Delete</th>
                        </tr>
                  </thead>  
                   <?php  
                      include("connect.php");
                     /* $username = $_POST['username'];
                         if (empty($username))
                        {
                          echo "
                          <script>
                          window.location.href='update_truck_driver.php';
                          </script>";
                         exit();
                        }*/
                      $query = "SELECT * FROM truck_driver where status='0' ";
                      $result = mysqli_query($conn, $query);
                      $result = mysqli_query($conn,$query);
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $truck_no = $row['truck_no'];
                    $truck_driver=$row['truck_driver'];
                    $inspection_no = $row['insp_no'];
                    $km = $row['km'];
                  ?>
                  <tr>
                    <td><?php echo $id?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id; ?>" id="id">
                    </td>
                    <td><?php echo $truck_no?>
                      <input  type="hidden" readonly="readonly" name="truck_no[]" value="<?php echo $truck_no; ?>" id="truck_no">
                    </td>
                    <td ><?php echo $truck_driver?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $truck_driver; ?>" id="truck_driver">
                    </td>
                     <td ><?php echo $inspection_no?>
                      <input  type="hidden" readonly="readonly" name="inspection_no[]" value="<?php echo $inspection_no; ?>" id="inspection_no">
                    </td>
                    <td ><?php echo $km?>
                      <input  type="hidden" readonly="readonly" name="km[]" value="<?php echo $km; ?>" id="km">
                    </td>
                    <td>
                      <input type="button" onclick="EditModal(<?php echo $id; ?>)" name="id" value="Update" class="btn btn-info btn btn-sm" />
                    </td>
                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $truck_no;?>')" name="delete" value="Delete" class="btn btn-danger btn btn-sm" />
                    </td>
                    <script>
                      function DeleteModal(truck_no)
                      { var truck_no = truck_no;
                        
                        if (confirm("Do you want to delete this Truck From Inspection..?"))
                        {
                          if(truck_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_truck_driver.php",
                                  data:'truck_no='+truck_no,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                            <script>
                                  function EditModal(id)
                                  {
                                    jQuery.ajax({
                                        url: "search_truck_for_update.php",
                                        data: 'id=' + id,
                                        type: "POST",
                                        success: function(data) {
                                          
                                        $("#result_main").html(data);
                                        },
                                            error: function() {}
                                        });
                                    document.getElementById("modal_button1").click();
                                    $('#myModal_id').val(id);

                                    }
                              </script><div id="result_main"></div>
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
             
        </div>
           
      </div>
        
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal" role="dialog">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Truck From Inspection</h4>
      </div>
      
      <form id="InspectionUpdate"  action="edit_truck_detail.php" method="post">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <!-- <div class="form-group"> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id1" name="id"/>
            <!-- </div> -->
            <div class="form-group">
                <b>Truck Number</b>
                <input type="text" name="truck_no" readonly="readonly" style="width: 100%;" id="truck_no1" class="form-control">

                  
              </div>
              <div class="form-group">
                <b>Truck Driver</b>
                <input type="text"name="truck_driver" readonly="readonly" style="width: 100%;" id="truck_driver1" class="form-control">
              </div> 
               
              <div class="form-group">
                <b>Inspection Number</b>
                 <input type="text" autocomplete="off"class="form-control" style="width: 100%;" id="insp_no1" name="insp_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Km Reading</b>
                <input type="Number"  autocomplete="off"class="form-control" style="width: 100%;" id="km1" name="km" required>
              </div>
              
          </div>
         
        </div>
      </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
      
      </div>
      </form>
   
    </div>
  </div>
</div><div id="result_main"></div>