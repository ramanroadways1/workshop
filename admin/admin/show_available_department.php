
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Truck Record</h1>
    </section>

    <section class="content">

      
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Truck Record</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <?php  
                     include("connect.php");
                     $sql = "SELECT * from department";
                    $result = $conn->query($sql);
                     ?>  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>id</td>  
                            <td>Department</td>  
                           
                           
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                            <td>'.$row["id"].'</td>  
                            <td>'.$row["department"].'</td>  
                            
                             
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="add_new_department.php" class="btn btn-sm btn-info btn-flat pull-left">Place New department</a>
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

