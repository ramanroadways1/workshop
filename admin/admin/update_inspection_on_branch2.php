
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On Inspection</h3>
        
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="font-size: 13px;font-family: verdana;">  
                  <thead>  
                       <tr>  
                              <td>Id</td>
                             <td>department</td>  
                              <td>mistry</td>
                              <td>Driver Complain</td>
                              <td>Date</td> 
                              <td>Update</td> 
                              <td>Delete</td>  

                        </tr>  
                  </thead>  
                   <?php  
                        
                      include("connect.php");
                       $inspection_no = $_POST['inspection_no'];
                         if (empty($inspection_no))
                        {
                          /*echo "
                          <script>
                          window.location.href='update_inspection_on_branch.php';
                          </script>";
                         exit();*/
                        }

                      $query = "SELECT * from inspection_record where inspection_no='$inspection_no'";
                      $result = mysqli_query($conn,$query);
                      $l_u = 1;
                      $id_customer = 0;
                    ?> 
                     
                  <?php  
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $truck_no1 = $row['truck_no1'];
                    $truck_driver=$row['truck_driver'];
                    $inspection_no = $row['inspection_no'];
                    $start_time = $row['start_time'];
                    $job_card_no = $row['job_card_no'];
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                    $work_action = $row['work_action'];
                    $date1 = $row['date1'];
                  ?>
                  <tr>
                     <td><?php echo $id?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id; ?>" id="id">
                    </td>
                   
                    <td><?php echo $department?>
                      <input type="hidden" readonly="readonly" style="width: 100%;" id="department" name="department[]" value="<?php echo $department; ?>" >
                    </td>

                    <td><?php echo $mistry?>
                        <input type="hidden" readonly="readonly" id="mistry" name="mistry[]" value="<?php echo $mistry; ?>">
                      </td>

                     <td><?php echo $work_action?>
                     <input type="hidden" readonly="readonly" id="work_action"   name="work_action[]" value="<?php echo $work_action; ?>" ></td>
     
                     <td><?php echo $date1?>
                      <input type="hidden" readonly="readonly" id="date1" name="gst[]" value="<?php echo $date1; ?>" >
                     </td>
                       <td>
                      <input type="button" onclick="EditModal(<?php echo $id; ?>)" name="id" value="Update" class="btn btn-info  btn-sm" />
                    </td>
                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="delete" value="Delete" class="btn btn-danger btn-sm" />
                    </td>
                    <script>
                      function DeleteModal(id)
                      { var id = id;
                        if (confirm("Do you want to delete this inspection Record..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_inspection_record.php",
                                  data:'id='+id,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                            <script>
                                  function EditModal(id)
                                  {
                                    jQuery.ajax({
                                        url: "search_insp_no.php",
                                        data: 'id=' + id,
                                        type: "POST",
                                        success: function(data) {
                                          
                                        $("#result_main").html(data);
                                        },
                                            error: function() {}
                                        });
                                    document.getElementById("modal_button1").click();
                                    $('#myModal_id').val(id);

                                    }
                              </script><div id="result_main"></div>
                  
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
             
        </div>
           
      </div>
        
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal" role="dialog">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Inspection</h4>
      </div>
      <form id="InspectionUpdate" action="edit_inspection.php" method="post">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <!-- <div class="form-group"> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id1" name="id"/>
            <!-- </div> -->
            <div class="form-group">
                <b>Truck Number</b>
                <input type="text" style="width: 100%;" id="truck_no2" autocomplete="off" class="form-control" name="truck_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Truck Driver</b>
                 <input type="text" style="width: 100%;" id="truck_driver1" autocomplete="off" class="form-control" name="truck_driver" readonly="readonly" required>
              </div> 
               
              <div class="form-group">
                <b>Inspection Number</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="inspection_no1" name="inspection_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Job Card Number</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="job_card_no1" name="job_card_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Date</b>
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="date11" name="date1" readonly="readonly" required>
              </div>
              
          </div>
          <div class="col-md-6">
              <div class="form-group">
                  <b>Start Time</b>
                  <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="start_time1" name="start_time" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Sno</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="sno1" name="sno" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Department</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="department1" name="department" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Mistry</b>
               <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="mistry1" name="mistry" readonly="readonly" required>
              </div>
              
              <div class="form-group">
                <b>Work Action</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="work_action1" name="work_action" required="required">
              </div>
          </div>   
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
      </div>
      </form>
    </div>
  </div>
</div><div id="result_main"></div>
<script type="text/javascript">
   $(function()
                        { 
                        $( "#problem" ).autocomplete({
                        source: 'autocomplete_problem.php',
                        change: function (event, ui) {
                        if(!ui.item){
                        $(event.target).val(""); 
                        $(event.target).focus();
                        alert('problem does not exists.');
                        } 
                        },
                        focus: function (event, ui) {return false; } }); });
</script>