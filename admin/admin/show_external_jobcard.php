
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>External job card</h1>
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">External job card</h3>
        
      </div>
        <!-- /.box-header -->
        <button onclick="window.location.href = 'show_external_expense.php';" value="Show grn detail"class="btn btn-info add-new"style="float: right; margin-right: 10px;">External jobcard Detail</button><br> <br> 
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                
                  <?php  
                   include("connect.php");
                   $sql = "SELECT * from external_job_cards_main";
                  $result = $conn->query($sql);
                   ?> 
                   <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>truck number</td>
                            <td>truck driver</td>
                            <td>break point</td>  
                            <td>job card number</td>  
                            <td>vehicle pick/send</td>  
                            <td>vehicle no</td>
                            <td>Employee name</td>  
                            <td>submit</td>  
                            <td>challan file</td>
                             <td>Date</td>
                       </tr>  
                  </thead>  
                  <?php

              while($row = mysqli_fetch_array($result)){

               $truck_no2=$row['truck_no2'];
               $truck_driver = $row['truck_driver'];
               $break_point = $row['break_point'];
               $job_card_no = $row['job_card_no'];
               $date1 = $row['date1'];
               $yes_no = $row['yes_no'];
               $vehicle_no = $row['vehicle_no'];
               $employee_name=$row['employee_name'];
               $submit_by=$row['submit_by'];
               $challan_file = $row['challan_file'];
                $challan_file1=explode(",",$challan_file);
               $count3=count($challan_file1);
               $date1 = $row['date1'];
             
           ?>
              
                 <tr>
                
                <td ><?php echo $truck_no2?>
                  <input  type="hidden" readonly="readonly" name="truck_no2[]" value="<?php echo $truck_no2; ?>">
                </td>
                <td ><?php echo $truck_driver?>
                  <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $truck_driver; ?>">
                </td>
                 <td ><?php echo $break_point?>
                  <input  type="hidden" readonly="readonly" name="break_point[]" value="<?php echo $break_point; ?>" >
                </td>
                
                <td ><?php echo $job_card_no?>
                  <input  type="hidden" readonly="readonly" name="job_card_no[]" value="<?php echo $job_card_no; ?>">
                </td>
                
                <td><?php echo $yes_no?>
                 <input type="hidden" readonly="readonly"  name="yes_no[]" value="<?php echo $yes_no; ?>" >
               </td>

                 <td ><?php echo $vehicle_no?>
                  <input  type="hidden" readonly="readonly" name="vehicle_no[]" value="<?php echo $vehicle_no; ?>" >
                </td>

                <td ><?php echo $employee_name?>
                  <input  type="hidden" readonly="readonly" name="employee_name[]" value="<?php echo $employee_name; ?>" >
                </td>
                
                <td ><?php echo $submit_by?>
                  <input  type="hidden" readonly="readonly" name="submit_by[]" value="<?php echo $submit_by; ?>" >
                </td>
                 <td>
                 <?php
                   for($i=0; $i<$count3; $i++){
                    
                    ?>
                    <a href="../jobcard/<?php echo $challan_file1[$i]; ?>" target="_blank"><?php echo $challan_file1[$i]; ?></a>
                     <?php } ?>
                 </td>
                 <td ><?php echo $date1?>
                  <input  type="hidden" readonly="readonly" name="date1[]" value="<?php echo $date1; ?>">
                </td>

               </tr><?php } ?>
             
                 
                 
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

