  
<!DOCTYPE html>
<html>
<head>
  <?php 
  include("header.php");
  include("connect.php");
   ?>
     
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 


       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include('aside_main.php'); ?>
  <div class="content-wrapper">
   
    <section class="content-header">
     
    </section>

    <section class="content">

   
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">External Jobcard Detail Data</h3>
        
      </div>
        <!-- /.box-header -->
         <script>
        function myFunction() {
          window.print();
        }
       </script>
           <div class="row">
                <div class="col-md-12">
                   <input type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning"onclick="myFunction()" value="Print jobcard">
                </div>
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div>  


        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                  <div id="order_table"> 
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:verdana; font-size:  13px;">  
                  <thead>  
                       <tr>  
                            <td>Truck Number</td>  
                            <td>Truck Driver</td>   
                            
                            <td>Jobcard start date</td>
                             <td>Jobcard submission date</td>
                            <td>Jobcard Number</td>  
                           
                       </tr>  
                  </thead>  
                  <?php  
                  $sql = "SELECT * from external_job_cards_main";
                   $result = $conn->query($sql);
                  while($row = mysqli_fetch_array($result))  
                  {  
                       $truck_no2 = $row["truck_no2"];
                       $truck_driver = $row["truck_driver"];
                       $job_card_no = $row["job_card_no"];
                       $date1 = $row["date1"];
                       $submission_date = $row['submission_date'];
                  ?>  
                <tr> 
                
                  <td><?php echo $truck_no2?>
                    <input type="hidden" name="truck_no" value='<?php echo $truck_no; ?>'>
                  </td>
                   <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $truck_driver; ?>" >
                  </td>
                   
                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td><?php echo $submission_date?>
                    <input type="hidden" readonly="readonly" name="submission_date" value="<?php echo $submission_date; ?>" >
                  </td>
                   <td>
                     <form method="POST" action="fetch_external_jobcard.php">
                      <input type="submit"  name="job_card_no" value="<?php echo $job_card_no; ?>" class="btn btn-primary btn btn-sm" >
                    </form>
                  </td>
                     
                </tr>
           
              <?php
                   
                }
             
              ?>
            </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 



