
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include('aside_main.php');?>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Issue Product In External Jobcard</h1>
    </section>

    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Issue Product In External Jobcard</h3>
       
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                     <?php  
                         include("connect.php");
                         $sql = "SELECT * from add_product_external_jobcard";
                         $result = $conn->query($sql);
                      ?> 
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>truck number</td>  
                            <td>job card number</td>  
                            <td>Product name</td>  
                            <td>Product type</td>
                            <td>Available Quantity</td>  
                            <td>Recive quantity</td>  
                            <td>Mistry</td>
                             <td>Date</td>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                       echo '  
                       <tr>  
                            <td>'.$row["truck_no"].'</td>  
                            <td>'.$row["job_card_no"].'</td>  
                            <td>'.$row["partsname"].'</td>
                            <td>'.$row["partstype"].'</td>  
                            <td>'.$row["available_qty"].'</td>
                            <td>'.$row["quantity"].'</td>  
                            <td>'.$row["mistry"].'</td>  
                            <td>'.$row["date1"].'</td> 
                       </tr>  
                       ';  
                  }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

