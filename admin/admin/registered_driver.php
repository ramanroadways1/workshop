
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <section class="content-header">
     
    </section>

    <section class="content">

    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Registered Driver</h3>
        
      </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                
                  <?php  
                   include("connect.php");
                   $sql = "SELECT * from driver";
                  $result = $conn->query($sql);
                   ?> 
                   <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                            <td>Driver name</td>
                            <td>Mobile</td>
                            <td>lic no</td>  
                            <td>Date</td>  
                            <td>Branch</td>
                            <td>Active status</td>  
                            <td>Driver code</td>  
                            <td>Last salary</td>
                             <td>Time with date</td>
                       </tr>  
                  </thead>  
                  <?php

              while($row = mysqli_fetch_array($result)){

               $name=$row['name'];
               $mobile = $row['mobile'];
               $pan = $row['pan'];
               $lic = $row['lic'];
               $date = $row['date'];
               $branch = $row['branch'];
               $active = $row['active'];
               $code=$row['code'];
               $last_salary=$row['last_salary'];
               $timestamp = $row['timestamp'];
           ?><tr>
                <td ><?php echo $name?></td>
                <td ><?php echo $mobile?></td>
                
                 <td ><?php echo $lic?></td> 
                 <td><?php echo $date?></td>
                 <td ><?php echo $branch?> </td>
                <td ><?php echo $active?> </td>
                <td ><?php echo $code?></td>
                 <td ><?php echo $last_salary?></td>
                 <td ><?php echo $timestamp?></td>

               </tr><?php } ?>
             
                 
                 
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
         <!--  <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a> -->
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

