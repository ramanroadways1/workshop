
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('aside_main.php');?>
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

     
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Select Branch For update data</h3>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">
           <form method="POST" action="update_truck_driver_on_branch.php"> 
                   <div class="col-md-4"> 
                       <div class="form-group">
                          <select name="pro_loc" style="width: 100%;" id="pro_loc" class="form-control"> 
                              <option disabled="disabled" selected="selected">Select Branch For Update</option>
                              <?php 
                                include ("connect.php");
                                  $get=mysqli_query($conn,"SELECT * FROM users");
                                     while($row = mysqli_fetch_assoc($get))
                                    {
                              ?>
                              <option value = "<?php echo($row['username'])?>" >
                                <?php 
                                      echo ($row['username']."<br/>");
                                ?>
                              </option>
                                <?php
                                }
                              ?>
                          </select>
                      </div>
                   </div> 

                    <div class="col-md-3"> 
                         <div class="form-group">
                            <input type="submit" name="submit">
                         </div>
                    </div> 

              </form>
            </div>  
          </div>  
        </div>
      </div>
    </div>
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal" role="dialog">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Truck From Inspection</h4>
      </div>
      
      <form id="InspectionUpdate"  action="edit_truck_detail.php" method="post">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <!-- <div class="form-group"> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id1" name="id"/>
            <!-- </div> -->
            <div class="form-group">
                <b>Truck Number</b>
                <input type="text" name="truck_no" readonly="readonly" style="width: 100%;" id="truck_no1" class="form-control">

                  
              </div>
              <div class="form-group">
                <b>Truck Driver</b>
                <input type="text"name="truck_driver" readonly="readonly" style="width: 100%;" id="truck_driver1" class="form-control">
              </div> 
               
              <div class="form-group">
                <b>Inspection Number</b>
                 <input type="text" autocomplete="off"class="form-control" style="width: 100%;" id="insp_no1" name="insp_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Km Reading</b>
                <input type="Number"  autocomplete="off"class="form-control" style="width: 100%;" id="km1" name="km" required>
              </div>
              
          </div>
         
        </div>
      </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
      
      </div>
      </form>
   
    </div>
  </div>
</div><div id="result_main"></div>