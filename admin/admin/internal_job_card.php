
  <?php
include("connect.php");
include('header.php');

?>
 <?php include('aside_main.php');?>
<!DOCTYPE html>
<html>
<head>
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}
th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>


             <style type="text/css">
  @media print {
  button {
    display: none !important;
  }

  textarea,select,footer,header {
    border: none !important;
    box-shadow: none !important;
    outline: none !important;
    display: none !important;
  }
  #print,#sub,#new_prblm,#issue,#h4,#bill,#bill1,#Update,#delete {
    display: none;
  }

  #first {
    float: left;
  }
   #second {
    float: center;
    width: 400px;
  }
   #third {
    float: right;
  }

    
  div{
        font-size: 16pt;
         font-family:verdana;
  }
   table, tr,body,h4,form,td,text{
        height: auto;
        font-size: 20pt;
         font-family:verdana;
        font: solid #000 !important;
        }
  
  body {
  zoom:50%; 
}
 #print_page
  {
    width: 140px;
  }


}
    
}
    
}
</style>



</head>
<body class="hold-transition skin-blue sidebar-mini">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="wrapper">

  
  <div class="content-wrapper">
    <section class="content-header">
    
    </section>

    <section class="content">

     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Internal Jobcard Record</h3>

         
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <form method="POST" action="insert_inspection_report.php">
          <?php
          $valueToSearch=$_POST['inspection_no'];

          $sql = "SELECT * FROM  job_card_record WHERE  inspection_no='$valueToSearch'";
              $result = $conn->query($sql);
                  
          if(mysqli_num_rows($result) > 0)
           {
               while($row = mysqli_fetch_array($result)){
             
                  $truck_driver = $row['truck_driver'];
                  $truck_no = $row['truck_no1'];
                  $job_card_no = $row['job_card_no'];
                  $date1 = $row['date1'];
                   $start_time = $row['start_time'];
                  $inspection_no = $row['inspection_no'];
                  $jobcard_end_Date = $row['now_date'];
                  $job_card_date = $row['job_card_date'];

             }
               ?>
                 <script>
            function printContent() {
              window.print();
            }
         </script>
              <!--  <div class="col-md-12">
                   <button type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning" onclick="printContent();" value="Print jobcard">Print Jobcard</button>
                </div> -->
           <div class="row" >

              <div class="col-md-4" id="first">
                <label>Truck Number</label>
                <?php echo $truck_no;?>
                <input type="hidden" autocomplete="off" id="truck_no1" name="truck_no1"  class="form-control" required readonly="readonly" value="<?php echo $truck_no;?>" placeholder="Truck Number" />
              </div>
              <div class="col-md-4"  id="first">
                <label>Truck Driver</label>
                <?php echo $truck_driver;?>
                <input type="hidden" autocomplete="off" id="truck_driver" name="truck_driver" value="<?php echo $truck_driver;?>" class="form-control" required readonly="readonly" placeholder="truck driver" />
              </div>
              <div class="col-md-4"  id="first">
                <label>Inspection Number</label>
                <?php echo $inspection_no;?>
                <input type="hidden" autocomplete="off" id="inspection_no" name="inspection_no"  class="form-control"  value="<?php echo $inspection_no;?>" placeholder="Inspection No." readonly/>
              </div> 
            </div>
            <div class="row" >
              <div class="col-md-4" id="second">
                <label>Job Card No</label>
                <?php echo $job_card_no;?>
                <input type="hidden" autocomplete="off" id="job_card_no" value="<?php echo $job_card_no;?>" placeholder="Job card no." name="job_card_no" class="form-control" required readonly/>
              </div> 
              <div class="col-md-4" id="second">
                <label>Inspection Date</label>
                <?php echo $date1;?>
                <input type="hidden" autocomplete="off" id="date1" name="date1" placeholder="Date"  class="form-control" value="<?php echo $date1;?>" required readonly/>
              </div> 
              <div class="col-md-4" id="second">
                <label>Start Time(Inspection)</label>
                <?php echo $start_time;?>
                <input type="hidden" autocomplete="off" id="start_time" placeholder="Start Time" name="start_time" value="<?php echo $start_time;?>" class="form-control" required readonly/>
              </div>
               <div class="col-md-4" id="third">
                <label>Jobcard Start date</label>
                <?php echo $job_card_date;?>
                <input type="hidden" autocomplete="off" id="start_time" placeholder="Start Time" name="job_card_date" value="<?php echo $job_card_date;?>" class="form-control" required readonly/>
              </div>
               <div class="col-md-4" id="third">
                <label>Jobcard end Date</label>
                <?php echo $jobcard_end_Date;?>
                <input type="hidden" autocomplete="off" id="start_time" placeholder="Start Time" name="start_time" value="<?php echo $jobcard_end_Date;?>" class="form-control" required readonly/>
              </div>
            </div>

            <?php
            
          }
          else{
            echo "<SCRIPT>
                        window.location.href='found_job.php';
           </SCRIPT>";
            exit();
          }
            ?><br>
            <div class="row">
                <div class="col-md-6">
                  <h4>Internal Jobcard Issue Product</h4>
                </div>

                 <?php $today_date = date('yy-m-d');
                    $diff = strtotime($jobcard_end_Date) - strtotime($today_date);
                     $final_diff = abs(round($diff / 86400));  
                     if ($final_diff > '7') {
                      ?>


<!--                               <script type="text/javascript">
                                setTimeout(function() {
                                $('#pintu').fadeOut('$final_diff');
                                }, 1000); 
                                // <-- time in milliseconds
                              </script> -->

                       

                   

                    <div class="col-md-6">
                        <?php echo "<font style='float:right;font-size:14px; color:red;'>Jobcard Has Been Closed</font>"; ?>
                     </div>
                      <?php
                     }else{
                      ?>


                      <?php
                     }
                   ?><br>

                 
            </div>

              <div class="table-responsive">  
              <table id="employee_data1" class="table table-striped table-bordered" border="4"; style="font-family:arial; font-size:  13px;">  
                  <thead>  
                       
                              <td id="id" >Id</td>
                              <td>Parts No</td>  
                              <td>Parts Name</td>
                              <td>Parts Type</td>
                              <td>Available Qty</td>
                              <td>Issue Qty</td>  
                              <td>mistry</td>  
                              <td>Latest Rate</td>  
                              <td>Amount</td>
                              <td>Date/Time</td>
                              <td id="Update">Update</td> 
                              <td id="delete">Delete</td>
                  </thead> 
                    <?php  
                      include("connect.php");
                      $query1 = "SELECT * from add_product_jobcard  WHERE  job_card_no='$job_card_no' and truck_no = '$truck_no'";
                      $result1 = mysqli_query($conn,$query1);
                      $l_u = 1;
                      $id_customer = 0;
                  while($row = mysqli_fetch_array($result1))
                  {             
                    $id_issue_product = $row['id'];
                    $partstype = $row['partstype'];
                    $productno = $row['productno'];
                    $partsname=$row['partsname'];
                    $available_qty = $row['available_qty'];
                    $quantity  = $row['quantity'];
                    $mistry = $row['mistry'];
                    $latest_rate = $row['latest_rate'];
                    $amount = $row['amount'];
                    $timestamp1 = $row['timestamp1'];
                   ?>
                  <tr>
                    <td ><?php echo $id_issue_product?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id_issue_product; ?>" id="id_issue_product">
                    </td>
                   
                        <td ><?php echo $productno?>
                      <input  type="hidden" readonly="readonly" name="partsname[]" value="<?php echo $productno; ?>" id="productno_main_page<?php echo $id_issue_product ?>">
                    </td>
                     
                    <td ><?php echo $partsname?>
                      <input  type="hidden" readonly="readonly" name="partsname[]" value="<?php echo $partsname; ?>" id="partsname1">
                    </td>
                    <td > <?php echo $partstype?>
                      <input  type="hidden" readonly="readonly" name="partstype[]" value="<?php echo $truck_no1; ?>" id="partstype1">
                    </td>
                    </td>
                     <td ><?php echo $available_qty?>
                      <input  type="hidden" readonly="readonly" name="available_qty" value="<?php echo $available_qty; ?>" id="available_qty1<?php echo $id_issue_product; ?>">
                    </td>
                    <td ><?php echo $quantity?>
                      <input  type="hidden" readonly="readonly" oninput="update_qty();" name="quantity[]" value="<?php echo $quantity; ?>" id="quantity1<?php echo $id_issue_product ?>">
                    </td>
                    <td ><?php echo $mistry?>
                      <input  type="hidden" readonly="readonly" name="mistry[]" value="<?php echo $mistry; ?>" id="mistry1">
                    </td>
                    
                    <td><?php echo $latest_rate?>
                      <input type="hidden" readonly="readonly" style="width: 100%;" id="latest_rate1" name="latest_rate[]" value="<?php echo $latest_rate; ?>" >
                    </td>

                    <td><?php echo $amount?>
                        <input type="hidden" readonly="readonly" id="amount1" name="amount[]" value="<?php echo $amount; ?>" >
                      </td>

                     <td><?php echo $timestamp1?>
                     <input type="hidden" readonly="readonly" id="timestamp11"   name="timestamp1[]" value="<?php echo $timestamp1; ?>" ></td>
     
                    <td>
                      <button type="button" onclick="EditModal_product(<?php echo $id_issue_product; ?>)" name="id" value="Update" class="btn btn-info btn-sm">Update</button>
                    </td>

                    <?php $today_date = date('yy-m-d');
                    $diff = strtotime($jobcard_end_Date) - strtotime($today_date);
                     $final_diff = abs(round($diff / 86400));  
                     if ($final_diff > '7') {
                      ?>
                     <td id="delete" >
                      <?php echo "can't delete"; ?>
                    </td> 
                      <?php
                     }else{
                      ?>
                    <td id="delete" >
                      <input type="button" onclick="Delete_product('<?php echo $id_issue_product;?>')" id='delete<?php echo $id_issue_product; ?>' name="delete" value="Delete" class="btn btn-danger btn-sm" />
                    </td> 
                      <?php
                     }
                   ?>        
                     <script>
                        function Delete_product(id)
                         {
                          id = id;
                          var inspection_no = '<?php echo $inspection_no; ?>';
                          var productno = document.getElementById('productno_main_page'+id).value;
                          var available_qty = document.getElementById('available_qty1'+id).value;
                          var issue_quantity = document.getElementById('quantity1'+id).value;
                          if (!productno=='') {
                              if (confirm("Are you sure you wnat to delete this record?")) {
                                   jQuery.ajax({
                                        url: "delete_issue_product.php",
                                        data: {id:id,productno:productno,available_qty:available_qty,inspection_no:inspection_no,issue_quantity:issue_quantity},
                                        type: "POST",
                                        success: function(data) {
                                        $("#result_main22").html(data);
                                        },
                                            error: function() {}
                                        });
                          }
                          return false;
                      
                          }
                          alert("You Can't delete This product");
                        }
                        
                     </script><div id="result_main23"></div>

                            <script>
                                  function EditModal_product(id)
                                  {
                                    jQuery.ajax({
                                        url: "search_internal_issue_product.php",
                                        data: 'id=' + id,
                                        type: "POST",
                                        success: function(data) {
                                            
                                        $("#result_main22").html(data);
                                        },
                                            error: function() {}
                                        });
                                    document.getElementById("modal_button22").click();
                                    $('#myModal_id').val(id);

                                    }


                              </script><div id="result_main22"></div>
                    
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div> 





               <h4>Internal Jobcard Detail</h4>
              <div class="table-responsive">  
              <table id="employee_data" class="table table-striped table-bordered" border="4"; style="font-family:arial; font-size:  13px;">  
                  <thead>  
                       
                              <td id="id" style="display: none;">Id</td>
                              <td style="display: none;">Truck Number</td>  
                              <td style="display: none;">Truck Driver</td>
                              <td style="display: none;">Inspection_no</td>
                              <td style="display: none;">start time</td>  
                              <td style="display: none;">job card no</td>  
                              <td>department</td>  
                              <td>mistry</td>
                              <td>Driver Complain</td>
                              <td>Job Card Work</td> 
                              <td>Date</td> 
                              <td id="Update">Update</td> 
                              <td id="delete" style="display: none;">Delete</td> 

                         
                  </thead> 
                     <script type="text/javascript">
                        $(document).ready(function(){
                            $('input[type="checkbox"]').click(function(){
                          
                          var val1 = $(this).attr("value");
                          var val2 = $(this).attr("data1");
                          
                          $('#WrkAct1'+val1).val(val2);
                          
                          if($(this).prop("checked") == true){
                            $('#WrkAct1'+val1).attr('readonly',false);
                            
                                }
                                else if($(this).prop("checked") == false){
                            $('#WrkAct1'+val1).attr('readonly',true);
                                }
                            });
                        });
                    </script> 
                    <?php  
                      include("connect.php");
                      $query = "SELECT * from job_card_record  WHERE  inspection_no='$valueToSearch'";
                      $result = mysqli_query($conn,$query);
                      $l_u = 1;
                      $id_customer = 0;
                    
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $truck_no1 = $row['truck_no1'];
                    $truck_driver=$row['truck_driver'];
                    $inspection_no = $row['inspection_no'];
                    $start_time = $row['start_time'];
                    $job_card_no = $row['job_card_no'];
                    $department = $row['department'];
                    $mistry = $row['mistry'];
                    $work_action = $row['work_action'];
                    $job_card_work = $row['job_card_work'];
                    $date1 = $row['date1'];
                  ?>
                  <tr>
                    <td style="display: none;"><?php echo $id?>
                      <input  type="hidden" readonly="readonly" name="id[]" value="<?php echo $id; ?>" id="id">
                    </td>
                    <td style="display: none;"><?php echo $truck_no1?>
                      <input  type="hidden" readonly="readonly" name="truck_no1[]" value="<?php echo $truck_no1; ?>" id="truck_no1">
                    </td>
                    <td style="display: none;"><?php echo $truck_driver?>
                      <input  type="hidden" readonly="readonly" name="truck_driver[]" value="<?php echo $truck_driver; ?>" id="truck_driver">
                    </td>
                     <td style="display: none;"><?php echo $inspection_no?>
                      <input  type="hidden" readonly="readonly" name="inspection_no" value="<?php echo $inspection_no; ?>" id="inspection_no">
                    </td>
                    <td style="display: none;"><?php echo $start_time?>
                      <input  type="hidden" readonly="readonly" name="start_time[]" value="<?php echo $start_time; ?>" id="start_time">
                    </td>
                    <td style="display: none;"><?php echo $job_card_no?>
                      <input  type="hidden" readonly="readonly" name="job_card_no[]" value="<?php echo $job_card_no; ?>" id="job_card_no">
                    </td>
                    
                    <td><?php echo $department?>
                      <input type="hidden" readonly="readonly" style="width: 100%;" id="department" name="department[]" value="<?php echo $department; ?>" >
                    </td>

                    <td><?php echo $mistry?>
                        <input type="hidden" readonly="readonly" id="mistry" name="mistry[]" value="<?php echo $mistry; ?>" >
                      </td>

                     <td><?php echo $work_action?>
                     <input type="hidden" readonly="readonly" id="work_action"   name="work_action[]" value="<?php echo $work_action; ?>" ></td>
     
                      <td><?php echo $job_card_work?>
                       <input type="hidden" readonly="readonly" name="job_card_work[]" value="<?php echo $job_card_work; ?>" >
                      </td>
                     <td><?php echo $date1?>
                      <input type="hidden" readonly="readonly" id="date1" name="gst[]" value="<?php echo $date1; ?>" >
                     </td>
                       <td>
                        <button type="button" onclick="EditModal(<?php echo $id; ?>)" name="id" value="Update" class="btn btn-info btn-sm">Update</button>
                      </td>
                      <td  id="delete" style="display: none;">
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="delete" value="Delete" class="btn btn-danger btn-sm" />
                    </td> 
                    <script>
                      function DeleteModal(id)
                      {
                        //alert(id);
                           var inspection_no = $("#inspection_no").val();
                            var truck_no = $("#truck_no1").val();
                             var id = id;
                        if (confirm("Do you want to delete this Internal jobcard..?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_internal_jobcard.php",
                                  data:'id='+id + '&inspection_no='+inspection_no+ '&truck_no='+truck_no,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                            <script>
                                  function EditModal(id)
                                  {
                                    jQuery.ajax({
                                        url: "search_internal_jobcard.php",
                                        data: 'id=' + id,
                                        type: "POST",
                                        success: function(data) {
                                          
                                        $("#result_main").html(data);
                                        },
                                            error: function() {}
                                        });
                                    document.getElementById("modal_button1").click();
                                    $('#myModal_id').val(id);

                                    }
                              </script><div id="result_main"></div>
                    
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  

                <script>
                function MyFunc(elem)
                {
                  document.getElementById("WrkAct1"+elem).readOnly = false;
                  var prob_id = elem;

                   var department = $('#department'+elem).val();
                   var mistry = $('#mistry'+elem).val();
                   var problem = $('#problem'+elem).val();
                   var work_action = $('#problem'+elem).val();
                   $('#WrkAct1'+elem).val(problem);
                  var insp_no = '<?php echo  $inspection_no ?>';
                  if(work_action!='')
                  {

          $.ajax({
                    type: "POST",
                    url: "insert_tmp_values.php",
                   data:'insp_no='+insp_no + '&prob_id='+prob_id + '&problem='+problem + '&department='+department + '&mistry='+mistry + '&work_action='+work_action,
                    success: function(data){

                        $("#result22").html(data);
                    }
                    });
        }
      }     
       </script>           
                  <div class="col-md-12">
              <center>
                <!-- <input type="submit" align="center" name="submit" class="btn btn-primary" > -->
        
              </center>     
            </div><br>
          </form>
<br><br>
        </div>
      </div>
    </section>
   
 </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

    <div class="control-sidebar-bg"></div>
  </body>
</html>

<button type="button" id="modal_button22" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update JobCard Issue Product</h4>
      </div>
      <form id="InspectionUpdate" action="edit_internaljob_issue_product.php" method="post">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-6">
             <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id2" name="id"/>
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" value="<?php echo $inspection_no; ?>" name="inspection_no"/>
            <div class="form-group">
                <b>Parts Number</b>
                <input type="text" style="width: 100%;" id="partstype" autocomplete="off" class="form-control" name="partstype" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>parts Name</b>
                 <input type="text" style="width: 100%;" id="partsname" autocomplete="off" class="form-control" name="partsname" readonly="readonly" required>
              </div> 
               <div class="form-group">
                <b>Available Qty</b>
                <input type="text" style="width: 100%;"  autocomplete="off"class="form-control " id="available_qty" name="available_qty" readonly="readonly" required>
              </div>
              <script type="text/javascript">
                             function update_qty(){
                               var quantity1 = Number($('#quantity').val());
                               var available_qty = Number($('#available_qty').val());
                               if (quantity1>available_qty)
                               {
                                alert("you can't insert more then avl qty");
                                   $('#quantity').val('');
                                    $('#amount').val('');
                               }
                          /*     var amount = Number($('#amount').val());
                               var available_qty = Number($('#available_qty').val());*/
                               //$('#gst_amount').val((amt*gst/100).toFixed(2));
                                var latest_rate = Number($('#latest_rate').val());
                                var final_amt = latest_rate* quantity1;
                                 $('#amount').val(final_amt);
                            }
              </script>
              <div class="form-group">
                <b>Issue Quantity</b>
                <input type="text" style="width: 100%;" oninput="update_qty()" autocomplete="off"class="form-control " id="quantity" name="quantity" required>
              </div> 
          </div>
          <div class="col-md-6">
             
              <div class="form-group">
                <b>Mistry</b>
                 <input type="text" style="width: 100%;" autocomplete="off" readonly="readonly" class="form-control " id="mistry2" name="mistry"  required> 
              </div>
               <div class="form-group">
                <b>Latest Rate</b>
               <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="latest_rate" name="latest_rate" readonly="readonly" required>
              </div>
               <div class="form-group">
                <b>Amount</b>
               <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="amount" name="amount" readonly="readonly" required>
              </div>



          </div>
         
        </div>
      </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
      
      </div>
      </form>
    </div>
  </div>
</div><div id="result_main"></div>

      <script type="text/javascript">
         $(function()
                      { 
                        $("#mistry2").autocomplete({
                        source: 'autocomplete_labour.php',
                        change: function (event, ui) {
                        if(!ui.item){
                        $(event.target).val(""); 
                        $(event.target).focus();
                        alert('Mistry does not exists In our record.');
                        }  
                      },
                      focus: function (event, ui) { return false; } }); });
      </script>

<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal11" style="display:none">DEMO</button>

<div id="myModal11" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update JobCard</h4>
      </div>
      <form id="InspectionUpdate" action="edit_internal_job.php" method="post">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <!-- <div class="form-group"> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id1" name="id"/>
            <!-- </div> -->
            <div class="form-group">
                <b>Truck Number</b>
                <input type="text" style="width: 100%;" id="truck_no2" autocomplete="off" class="form-control" name="truck_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Truck Driver</b>
                 <input type="text" style="width: 100%;" id="truck_driver1" autocomplete="off" class="form-control" name="truck_driver" readonly="readonly" required>
              </div> 
               
              <div class="form-group">
                <b>Inspection Number</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="inspection_no1" name="inspection_no" value="<?php $inspection_no ?>" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Job Card Number</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="job_card_no1" name="job_card_no" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Date</b>
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="date11" name="date1" readonly="readonly" required>
              </div>
              <div class="form-group">
                  <b>Start Time</b>
                  <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="start_time1" name="start_time" readonly="readonly" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <b>Sno</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="sno1" name="sno" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Department</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="department1" name="department" readonly="readonly" required>
              </div>
              <div class="form-group">
                <b>Mistry</b>
               <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="mistry1" name="mistry" readonly="readonly" required>
              </div>
              
              <div class="form-group">
                <b>Work Action</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="work_action1" name="work_action" readonly="readonly">
              </div>
              <div class="form-group">
                <b>Job Card Work</b>
                <input type="text" style="width: 100%;" autocomplete="off"class="form-control " id="job_card_work1" name="job_card_work" required>
              </div>
          </div>
         
        </div>
      </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
      
      </div>
      </form>
    </div>
  </div>
</div><div id="result_main"></div>



 
<style>
ul.ui-autocomplete{
  z-index: 999999 !important;
}
</style>
 


