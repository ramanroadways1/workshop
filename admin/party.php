
 <?php  
 include("connect.php");
 $sql = "SELECT * from party";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Operation of Party</h1>
    </section>

    <section class="content">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Party Detail</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div align="right">
                <a href='party_add.php' type="submit" name="add" id="add" class="btn btn-info">Add New Party</a>
              </div><br>
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                         <th>Id</th>
                         <th>Party Name</th>
                         <th>Party Location</th>
                         <th>Mobile No</th>
                         <th>Address</th>
                         <th>Contact Person</th>
                          <th>Email</th>
                         <th>pan</th>
                         <th>Gstin</th>
                         <th>Gst Type</th>
                         <th>Acc No</th>
                         <th>Acc Holder Name</th>
                         <th>Ifsc Code</th>
                         <th>bank</th>
                         <th>Branch</th>
                         <th>Party Code</th>
                         <th>Update</th>
                         <th>Delete</th>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                     echo '  
                     <tr>  
                       <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
                       <td>'.$row["party_name"].'<input type="hidden" name="party_name[]" value="'.$row["party_name"].'"></td>
                       <td>'.$row["party_location"].'<input type="hidden" name="party_location[]" value="'.$row["party_location"].'"></td>
                       <td>'.$row["mobile_no"].'<input type="hidden" name="mobile_no[]" value="'.$row["mobile_no"].'"></td>
                       <td>'.$row["address"].'<input type="hidden" name="address[]" value="'.$row["address"].'"></td>
                       <td>'.$row["contact_person"].'<input type="hidden" name="contact_person[]" value="'.$row["contact_person"].'"></td>
                        <td>'.$row["email"].'<input type="hidden" name="email[]" value="'.$row["email"].'"></td>
                       <td>'.$row["pan"].'<input type="hidden" name="pan[]" value="'.$row["pan"].'"></td>
                       <td>'.$row["gstin"].'<input type="hidden" name="gstin[]" value="'.$row["gstin"].'"></td>
                       <td>'.$row["gst_type"].'<input type="hidden" name="gst_type[]" value="'.$row["gst_type"].'"></td>
                       <td>'.$row["acc_no"].'<input type="hidden" name="acc_no[]" value='.$row["acc_no"].'></td>
                       <td>'.$row["acc_holder_name"].'<input type="hidden" name="acc_holder_name[]" value='.$row["acc_holder_name"].'></td>
                       <td>'.$row["ifsc_code"].'<input type="hidden" name="ifsc_code[]" value='.$row["ifsc_code"].'></td>
                       <td>'.$row["bank"].'<input type="hidden" name="bank[]" value='.$row["bank"].'></td>
                       <td>'.$row["branch"].'<input type="hidden" name="branch[]" value='.$row["branch"].'></td>
                       <td>'.$row["party_code"].'<input type="hidden" id="party_code'.$row["id"].'" name="party_code[]" value='.$row["party_code"].'></td>
                       <td>
                        <input type="button" onclick="EditModal('.$row["id"].')" name="update" value="Update" class="btn btn-info" />
                      </td>
                      <td>
                        <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
                      </td>
                     </tr>  
                     ';  
                    }  
                  ?>  
                </table>  
              </div>  
              <div id="result_main"></div>
              <div id="result"></div> 
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
       <!--  <div class="box-footer clearfix">
          <a href="party.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Party</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

<script>
function EditModal(id)
{
  jQuery.ajax({
      url: "fetch_party.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
          error: function() {}
      });
  document.getElementById("modal_button1").click();
  $('#myModal_id').val(id);

  }
</script>
<script type="text/javascript">
$(document).ready(function (e) {
$("#FormPartyUpdate").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "update_party.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 
<script>
  function DeleteModal(id)
  {
    var id = id;
    var party_code = $('#party_code'+id).val();
    if (confirm("Do you want to delete this party?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_party.php",
              data:'id='+id + '&party_code='+party_code ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
    }
  }     
</script>
<div id="result22"></div>

<button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

 <div id="myModal22" class="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Party</h4>
      </div>
      <form id="FormPartyUpdate" action="update_party.php">
        <div class="modal-body">
        <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <!-- <div class="form-group"> -->
              <!-- <label for="id">ID</label> -->
              <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>
            <!-- </div> -->
            <div class="form-group">
              <b>Party Name</b>
              <input type="text" style="width: 100%;" readonly="readonly" required="required" autocomplete="off" class="form-control" id="party_name" name="party_name"/>
            </div>
            <div class="form-group">
              <b>Party Location</b>
              <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" name="party_location" id="party_location" />
            </div>
            <div class="form-group">
               <b>Mobile number</b>
               <input type="number" style="width: 100%;" required="required" required="required" onchange="IsMobileNumber(this);" autocomplete="off" class="form-control" id="mobile_no" name="mobile_no" value="<?php echo $mobile_no; ?>" />
            </div>
            <div class="form-group">
               <b>Address</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" id="address" name="address" />
            </div>
            <div class="form-group">
               <b>Contact Person</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" class="form-control" id="contact_person" name="contact_person" />
            </div>

            <div class="form-group">
               <b>Email</b>
               <input type="text" style="width: 100%;" autocomplete="off"  required="required" onblur="validateEmail(this.value);" class="form-control" id="email" name="email" />
            </div>

            <div class="form-group">
               <b>PAN Number</b>
               <input type="text" style="width: 100%;" required="required" autocomplete="off" id="pan" MaxLength="10" class="form-control" name="pan" onchange="ValidatePAN(this);">
            </div>
            <div class="form-group">
               <b>GSTIN Number</b>
               <input type="text" style="width: 100%;" autocomplete="off" id="gstin" MaxLength="15" class="form-control" name="gstin" oninput="chkGstType();" onchange="ValidateGSTIN(this);"/>
            </div>
           
          </div>
          <!-- /.col -->
          <div class="col-md-6">
             <div class="form-group">
               <b>GST Type</b>
               <input type="text" style="width: 100%;" autocomplete="off" required="required" id="gst_type" MaxLength="15" class="form-control" name="gst_type" readonly/>
            </div>
            <script>
                function chkGstType()
              {
               var gstinno = $('#gstin').val();
               if(gstinno.substr(0,2)==24)
               {
                $('#gst_type').val('cgst_sgst');
               }
               else{
                $('#gst_type').val('igst');
               }
              }
              </script>

            <div class="form-group">
                  <h2>Account Details</h2>
            </div>
          <div class="form-group">
            <b>Bank Name</b>
            <input type="text" style="width: 100%;" autocomplete="off" id="bank" required="required" class="form-control" name="bank" />  
    
           </div>
      

              <div class="form-group" >
                <b>Branch Name</b>
                  <input type="text"  value="<?php echo $branch; ?>" required="required" style="width: 100%;" id="branch" class="form-control" name="branch" />
              </div>
                <div class="form-group">
                  <b>Account Number</b>
                     <input type="number" style="width: 100%;"  required="required" value="<?php echo $acc_no; ?>" id="acc_no" class="form-control" name="acc_no" />
                </div>
                <div class="form-group">
                  <b>Account Holder Name</b>
                     <input type="text"  value="<?php echo $acc_holder_name; ?>" required="required" style="width: 100%;" autocomplete="off" id="acc_holder_name" class="form-control" name="acc_holder_name" />
                </div>
                <div class="form-group">
                  <b>IFSC Code</b>
                    <input type="text"  value="<?php echo $ifsc_code; ?>" required="required" style="width: 100%;" autocomplete="off" id="ifsc_code" class="form-control" name="ifsc_code" onchange="ValidateIFSC(this);"/>
                </div>
                <div class="form-group">
                  <b>Party Code</b>
                    <input type="text" style="width: 100%;" autocomplete="off" required="required" id="party_code" class="form-control" name="party_code" readonly="readonly" />
                </div>
        
          </div>
          <!-- /.col -->
        </div>

        <!-- /.row -->

      </div>
      </div>

        <!-- <input id="myModal_id"> -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Update</button>
       </div>
      </form>
    </div>
  </div>
</div>

   <script type="text/javascript">
     function validateEmail(sEmail) {
     var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

        if(!sEmail.match(reEmail)) {
          alert("Invalid email address");
          document.getElementById('email').value = "";
        }

        return true;

      }
   </script>

<script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("mobile_no");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = /^[1-9]{1}[0-9]{9}$/;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#mobile_no').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                  }
            }
      } 

    function ValidatePAN() { 
      var Obj = document.getElementById("pan");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                if (ObjVal.search(panPat) == -1) {
                    alert("Invalid Pan No... Please Enter In This Format('ABCDE1234F')");
                    $('#pan').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Pan No.");
                  }
            }
      } 
      function ValidateGSTIN() { 
      var Obj = document.getElementById("gstin");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var gstinPat = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
                if (ObjVal.search(gstinPat) == -1) {
                    alert("Invalid GSTIN No... Please Enter In This Format('12ABCDE1234F1ZQ')");
                    $('#gstin').val('');
                    $('#gst_type').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct GSTIN No.");
                  }
            }
      }
      function ValidateIFSC() { 
      var Obj = document.getElementById("ifsc_code");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var ifscPat = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/;
                if (ObjVal.search(ifscPat) == -1) {
                    alert("Invalid IFSC No... Please Enter In This Format('ABCD0123456')");
                    $('#ifsc_code').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct IFSC No.");
                  }
            }
      }
   </script>
