
<!DOCTYPE html>
<html>
<head>
<?php 
include("header.php");
include("connect.php");
include("aside_main.php");
?>
<style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>
 <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>

 <script type="text/javascript">
    function ModalNew(val){
    // $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"password_update.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     // $('#loadicon').hide(); 
                }  
           });
    }



 </script>
 
<script type="text/javascript">
      $(document).on('submit', '#addreq', function()
    {  
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'update_password.php',
        data : data,
        success: function(data) {  
        $('#exampleModal').modal("hide");  
        $('#response').html(data);  
        // $('#user_data').DataTable().ajax.reload();  
        }
      });
      return false;
    }); 
</script>

<div id="response"></div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> 
<link href="../assets/font-awesome.min.css" rel="stylesheet">
<script src="../assets/jquery-3.5.1.js"></script>
<script src="../assets/jquery-ui.min.js"></script>
<link href="../assets/jquery-ui.css" rel="stylesheet"/>
<link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../assets/custom.css">

<style type="text/css">
  label.btn {
  padding: 0;
}

label.btn input {
  opacity: 0;
  position: absolute;
}

label.btn span {
  text-align: center;
  padding: 6px 12px;
  display: block;
}

label.btn input:checked+span {
  background-color: rgb(80, 110, 228);
  color: #fff;
}
</style>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<style type="text/css">
      .MultiCheckBox {
      border:1px solid #e2e2e2;
      padding: 5px;
      border-radius:4px;
      cursor:pointer;
      }

      .MultiCheckBox .k-icon{ 
      font-size: 15px;
      float: right;
      font-weight: bolder;
      margin-top: -7px;
      height: 10px;
      width: 14px;
      color:#787878;
      } 

      .MultiCheckBoxDetail {
      display:none;
      position:absolute;
      border:1px solid #e2e2e2;
      overflow-y:hidden;
      }

      .MultiCheckBoxDetailBody {
      overflow-y:scroll;
      }

      .MultiCheckBoxDetail .cont  {
      clear:both;
      overflow: hidden;
      padding: 2px;
      }

      .MultiCheckBoxDetail .cont:hover  {
      background-color:#cfcfcf;
      }

      .MultiCheckBoxDetailBody > div > div {
      float:left;
      }

      .MultiCheckBoxDetail>div>div:nth-child(1) {

      }

      .MultiCheckBoxDetailHeader {
      overflow:hidden;
      position:relative;
      height: 28px;
      background-color:#3d3d3d;
      }

      .MultiCheckBoxDetailHeader>input {
      position: absolute;
      top: 4px;
      left: 3px;
      }

      .MultiCheckBoxDetailHeader>div {
      position: absolute;
      top: 5px;
      left: 24px;
      color:#fff;
      }
</style>
<style type="text/css">
  .tgl {
  display: none;
}
.tgl, .tgl:after, .tgl:before, .tgl *, .tgl *:after, .tgl *:before, .tgl + .tgl-btn {
  box-sizing: border-box;
}
.tgl::-moz-selection, .tgl:after::-moz-selection, .tgl:before::-moz-selection, .tgl *::-moz-selection, .tgl *:after::-moz-selection, .tgl *:before::-moz-selection, .tgl + .tgl-btn::-moz-selection {
  background: none;
}
.tgl::selection, .tgl:after::selection, .tgl:before::selection, .tgl *::selection, .tgl *:after::selection, .tgl *:before::selection, .tgl + .tgl-btn::selection {
  background: none;
}
.tgl + .tgl-btn {
  outline: 0;
  display: block;
  width: 4em;
  height: 2em;
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}
.tgl + .tgl-btn:after, .tgl + .tgl-btn:before {
  position: relative;
  display: block;
  content: "";
  width: 50%;
  height: 100%;
}
.tgl + .tgl-btn:after {
  right: -50%;
}
.tgl + .tgl-btn:before {
  display: none;
}
.tgl:checked + .tgl-btn:after {
  right: 50%;
}

.tgl-flat + .tgl-btn {
  padding: 2px;
  transition: all .2s ease;
  background: #fff;
  border: 4px solid #f2f2f2;
  border-radius: 2em;
}
.tgl-flat + .tgl-btn:after {
  transition: all .2s ease;
  background: #f2f2f2;
  content: "";
  border-radius: 1em;
}
.tgl-flat:checked + .tgl-btn {
  border: 4px solid #7FC6A6;
}
.tgl-flat:checked + .tgl-btn:after {
  right: 0%;
  background: #7FC6A6;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
<div class="wrapper">
<form method="post" action="insert_emp.php" autocomplete="off">
    <div class="content-wrapper">

<section class="content">

<div class="box box-default">
    <div class="box-header with-border">
   <center> <h3 class="box-title">Employee </h3></center>
    </div>
<div class="box-body">
<div class="row">
<div class="col-md-6">

    <div id="result22"></div>


 <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">

<div class="form-group ">
        <label for="product_no">Employee Name<font color="red">*</font></label>
        <input type="text" style="width: 100%;"  class="form-control" name="employee_name" id="employee_name"  placeholder="Employee Name" required/>
</div>
<div class="form-group">

  <label>Password</label>
    <div class="input-group" id="show_hide_password">
    <input class="form-control" type="password" name="password" id="password">
    <div class="input-group-addon">
    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
    </div>
    </div>

</div>

<script type="text/javascript">

      $(document).ready(function() {
      $("#show_hide_password a").on('click', function(event) {
      event.preventDefault();
      if($('#show_hide_password input').attr("type") == "text"){
      $('#show_hide_password input').attr('type', 'password');
      $('#show_hide_password i').addClass( "fa-eye-slash" );
      $('#show_hide_password i').removeClass( "fa-eye" );
      }else if($('#show_hide_password input').attr("type") == "password"){
      $('#show_hide_password input').attr('type', 'text');
      $('#show_hide_password i').removeClass( "fa-eye-slash" );
      $('#show_hide_password i').addClass( "fa-eye" );
      }
      });
      });
</script>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script>
      $(document).ready(function () {
      $("#test").CreateMultiCheckBox({ width: '230px', defaultText : 'Select Branch', height:'250px' });
      });
</script>
  

 <div class="col-md-3">
    <label>Login With Admin</label>
      <select  style="font-size: 13px; font-family: verdana;" class="form-control" id="admin1" name="admin1">

         <option value="">---Select---</option>
         <option value="1">Yes</option>
         <option value="0">No</option>

      </select>
   </div>

    <div class="col-md-3">
      <label>Select Status</label>
        <select  style="font-size: 13px; font-family: verdana;" class="form-control" id="status" name="status">
            <option value="">---Select---</option>
            <option value="1">Active</option>
            <option value="0">UnActive</option>
        </select>
    </div>

      <div class="col-md-6">
          <label>Select Branch</label>
          <select id="test" name="branch[]">
          <option value="gandhidham">gandhidham</option>
          <option value="sirohiroad">sirohiroad</option>
          <option value="dariba">dariba</option>
          <option value="vapi">vapi</option>
          <option value="dhule">dhule</option>
          <option value="korba">korba</option>
          <option value="visalpur">visalpur</option>
          <option value="aburoad">aburoad</option>
          <option value="dahej">dahej</option>
          <option value="jharsuguda">jharsuguda</option>
          <option value="dindigul">dindigul</option>
          <option value="jamnagar">jamnagar</option>
          <option value="gopalpur">gopalpur</option>



          </select>
    </div>

<script type="text/javascript">

        $(document).ready(function () {
        $(document).on("click", ".MultiCheckBox", function () {
        var detail = $(this).next();
        detail.show();
        });

        $(document).on("click", ".MultiCheckBoxDetailHeader input", function (e) {
        e.stopPropagation();
        var hc = $(this).prop("checked");
        $(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input").prop("checked", hc);
        $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
        });

        $(document).on("click", ".MultiCheckBoxDetailHeader", function (e) {
        var inp = $(this).find("input");
        var chk = inp.prop("checked");
        inp.prop("checked", !chk);
        $(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input").prop("checked", !chk);
        $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
        });

        $(document).on("click", ".MultiCheckBoxDetail .cont input", function (e) {
        e.stopPropagation();
        $(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();

        var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
        $(".MultiCheckBoxDetailHeader input").prop("checked", val);
        });

        $(document).on("click", ".MultiCheckBoxDetail .cont", function (e) {
        var inp = $(this).find("input");
        var chk = inp.prop("checked");
        inp.prop("checked", !chk);

        var multiCheckBoxDetail = $(this).closest(".MultiCheckBoxDetail");
        var multiCheckBoxDetailBody = $(this).closest(".MultiCheckBoxDetailBody");
        multiCheckBoxDetail.next().UpdateSelect();

        var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
        $(".MultiCheckBoxDetailHeader input").prop("checked", val);
        });

        $(document).mouseup(function (e) {
        var container = $(".MultiCheckBoxDetail");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
        }
        });
        });

        var defaultMultiCheckBoxOption = { width: '220px', defaultText: 'Select Below', height: '200px' };

        jQuery.fn.extend({
        CreateMultiCheckBox: function (options) {

        var localOption = {};
        localOption.width = (options != null && options.width != null && options.width != undefined) ? options.width : defaultMultiCheckBoxOption.width;
        localOption.defaultText = (options != null && options.defaultText != null && options.defaultText != undefined) ? options.defaultText : defaultMultiCheckBoxOption.defaultText;
        localOption.height = (options != null && options.height != null && options.height != undefined) ? options.height : defaultMultiCheckBoxOption.height;

        this.hide();
        this.attr("multiple", "multiple");
        var divSel = $("<div class='MultiCheckBox'>" + localOption.defaultText + "<span class='k-icon k-i-arrow-60-down'><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='sort-down' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-sort-down fa-w-10 fa-2x'><path fill='currentColor' d='M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z' class=''></path></svg></span></div>").insertBefore(this);
        divSel.css({ "width": localOption.width });

        var detail = $("<div class='MultiCheckBoxDetail'><div class='MultiCheckBoxDetailHeader'><input type='checkbox' class='mulinput' value='-1982' /><div>Select All</div></div><div class='MultiCheckBoxDetailBody'></div></div>").insertAfter(divSel);
        detail.css({ "width": parseInt(options.width) + 10, "max-height": localOption.height });
        var multiCheckBoxDetailBody = detail.find(".MultiCheckBoxDetailBody");

        this.find("option").each(function () {
        var val = $(this).attr("value");

        if (val == undefined)
        val = '';

        multiCheckBoxDetailBody.append("<div class='cont'><div><input type='checkbox' class='mulinput' value='" + val + "' /></div><div>" + $(this).text() + "</div></div>");
        });

        multiCheckBoxDetailBody.css("max-height", (parseInt($(".MultiCheckBoxDetail").css("max-height")) - 28) + "px");
        },
        UpdateSelect: function () {
        var arr = [];

        this.prev().find(".mulinput:checked").each(function () {
        arr.push($(this).val());
        });

        this.val(arr);
        },
        });
</script>



<script type="text/javascript">
function IsMobileNumber() { 
var Obj = document.getElementById("employee_mob");
if (Obj.value != "") {
ObjVal = Obj.value;
var moPat = ^(\+[\d]{1,5}|0)?[7-9]\d{9}$;
if (ObjVal.search(moPat) == -1) {
alert("Invalid Mobile No");
$('#employee_mob').val('');
Obj.focus();
return false;
}
else
{
alert("Correct Mobile No");
}
}
} 
</script>
</div>

<div class="col-md-4">
<div class="form-group">
<label for="employee_mob">Employee number<font color="red">*</font></label>
<input type="text" style="width: 100%;" MaxLength="10" class="form-control" name="employee_mob" id="employee_mob" onchange="IsMobileNumber(this);" placeholder="Mobile Number" required/>
</div>

<script type="text/javascript">
function validateEmail(sEmail) {

var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

if(!sEmail.match(reEmail)) {

alert("Invalid email address");
document.getElementById('emp_mail').value = "";
}

return true;

}
</script>

<div class="col-md-12">

<label for="emp_mail">Email<font color="red">*</font></label>
<input type="text" style="width: 100%;" class="form-control" onblur="validateEmail(this.value);" name="emp_mail" id="emp_mail" placeholder="Email" required/>
</div>


</div>
<div class="col-md-4" style="margin-top: 200px; ,margin-right: 50px;">
   <button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>

    
</div>
</div>

</div>


</body>

  <br><br><br>
</div>
</form>

<!-- <div class="wrapper"> -->
<!-- <div class="content-wrapper"> --> 
<section class="content" > 
<div class="box box-default"> 
<div class="box-body">

<div class="row">

<table id="user_data" class="table table-bordered table-hover">
  <thead class="thead-light">
    <tr>
          <th>Id</th>
          <th>Employee Code</th>
          <th>Employee Name</th>
          <th>Mobail Numbur</th>
          <th>Email ID</th>
          <th>Branch</th>
          <th>(Status)Active=1/OFF=0</th>
          <th>Update</th>
          <th>Password Rest</th>
    </tr>
  </thead>
    <tfoot class="thead-light">
      <tr>
    </tr>
  </tfoot>
</table>

<script type="text/javascript">  

    jQuery( document ).ready(function() {

    var loadurl = "fetch_emp.php"; 
    $('#loadicon').show(); 
    var table = jQuery('#user_data').DataTable({ 
    "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
    "bProcessing": true, 
    "searchDelay": 1000, 
    "scrollY": 300,
    "scrollX": true, 
    "sAjaxSource": loadurl,
    "iDisplayLength": 10,
    "order": [[ 0, "asc" ]], 
    "dom": 'QlBfrtip',
    "buttons": [

    {
    "extend": 'csv',
    "text": '<i class="fa fa-file-text-o"></i> CSV',
    customize: function (csv) { 

    if (window) {

    }
    }
    },
    {
    "extend": 'excel',
    "text": '<i class="fa fa-list-ul"></i> EXCEL'
    },
    {
    "extend": 'print',
    "text": '<i class="fa fa-print"></i> PRINT'
    },
    {
    "extend": 'colvis',
    "text": '<i class="fa fa-columns"></i> COLUMNS'
    }

    ],
    "searchBuilder": {
    "preDefined": {

    }
    },
    "language": {
    "loadingRecords": "&nbsp;",
    "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../assets/loading.gif height=25> </center>"
    },

    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
    });  

    $('title').html(" RRPL - Vendor Management System ADMIN");

    $("#getPAGE").submit(function(e) {  
    e.preventDefault();
    var data = $(this).serialize();
    loadurl = "party_fetch.php?".concat(data);
    table.ajax.url(loadurl).load();  
    }); 

    });  

</script> 
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<!-- </div> -->
</div>


<body>
  <script>
      function EditModal(id)
      {
      jQuery.ajax({
      url: "fetch_empdata.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main").html(data);
      },
      error: function() {}
      });
      document.getElementById("modal_button1").click();
      $('#myModal_id').val(id);

      }
</script>

<div id="result_main"></div>
 <script type="text/javascript">

    $(document).ready(function (e) {
    $("#FormProductUpdate").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
    url: "update_emp.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
    $("#result").html(data);
    },
    error: function() 
    {} });}));});

</script> 
<div id="result"></div>

  <script>
      function DeleteModal(id)
      {
      jQuery.ajax({
      url: "fetchpassword.php",
      data: 'id=' + id,
      type: "POST",
      success: function(data) {
      $("#result_main1").html(data);
      },
      error: function() {}
      });

      }
</script>
<footer class="main-footer">
<div class="pull-right hidden-xs">
<b>Version</b> 1.2
</div>
<strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
reserved.
</footer>
<div id="result_main1"></div>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="modal_button1" style="display: none;" data-target="#myModal">Open Modal</button>
 <div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">

 <div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Product</h4>
</div>
<form id="FormProductUpdate" action="update_product.php">

<div class="modal-body">
<div class="row">
<div class="col-md-6"> 

      <input type="hidden" style="width: 100%;" autocomplete="off" class="form-control" id="id" name="id"/>

   <div class="form-group">
<b>Employee Name</b>
<input type="text" style="width: 100%;" id="empname" autocomplete="off" class="form-control" name="empname"  required>
</div> 

<div class="form-group">
<b>Employee number</b>
<input type="text" style="width: 100%;" autocomplete="off"class="form-control" id="empmobail" name="empmobail"  required>
</div>
   <div class="form-group">
<b>Employee Email</b>
<input type="text" style="width: 100%;" id="empemail" autocomplete="off" class="form-control" name="empemail"  required>
</div>
</div>
<div class="col-md-6">
     <div class="form-group">
      <b>Branch Login Active/UnActive</b>
      <select name="status" style="width: 100%;" id="lock_unlock" class="form-control"> 
    
          <option value="1">Active</option>
          <option value="0">UnActive</option>
      </select>
    </div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-info">Update</button>
</div>
</form>
   </div>
</div>

<script src="../assets/bootstrap.js"></script>
<script src="../assets/bootstrap.bundle.js"></script>
<script src="../assets/scripts.js"></script>
<script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
<script type="text/javascript" src="../assets/jszip.min.js"></script>
<script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/buttons.print.min.js"></script>
<script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
<script type="text/javascript" src="../assets/vfs_fonts.js"></script>
<script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>    
</body>

</html>