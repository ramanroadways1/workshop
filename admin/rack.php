
 <?php  
 include("connect.php");
 $sql = "SELECT * FROM rack";
$result = $conn->query($sql);
 ?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Operation of Rack</h1>
    </section>

    <section class="content">

    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Rack Detail</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div align="right">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add New Rack</button>
              </div><br>
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                        <th>Id</th>
                        <th>Rack Number</th>
                        <th>Delete</th>
                       </tr>  
                  </thead>  
                  <?php  
                  while($row = mysqli_fetch_array($result))  
                  {  
                    echo '  
                    <tr>  
                      <td>'.$row["id"].' <input type="hidden" name="id[]" value='.$row["id"].'></td>
                      <td>'.$row["rackno"].' <input type="hidden" id="rackno'.$row["id"].'" name="rackno[]" value='.$row["rackno"].'></td>
                      <td>
                        <input type="button" onclick="DeleteModal('.$row["id"].')" name="delete" value="Delete" class="btn btn-danger" />
                      </td>
                    </tr>  
                     ';  
                    }  
                  ?>  
                </table>  
              </div>  
              <div id="result"></div> 
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
       
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddRackNo").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "insert_rackno.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script>
<script>
  function DeleteModal(id)
  {
    var id = id;
    var rackno = $('#rackno'+id).val();
    if (confirm("Do you want to delete this rack number?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_rackno.php",
              data:'id='+id + '&rackno='+rackno ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
  }
  }     
</script>
<div id="result22"></div>
<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Rack</h4>
      </div>
      <form action="insert_rackno.php" id="AddRackNo">
        <div class="modal-body">
          <div class="box-body">
              <div class="col-md-2">
                  <b>Rack</b>
              </div>
              <div class="col-md-10">
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="rackno" name="rackno" placeholder="Rack Number"/>
              </div>
          </div>
      </div>

        <!-- <input id="myModal_id"> -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
        </div>
      </form>
    </div>
  </div>
</div>