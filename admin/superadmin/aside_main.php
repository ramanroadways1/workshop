<?php 

  session_start(); 

  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }
$demo3=$_SESSION['username'];  
?>
<header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>RRPL</b></span>
      <span class="logo-lg">Vendor/Workshop</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <br>
    <?php  if (isset($_SESSION['admin'])) : ?>
      <p style="float:right; margin-right:20px; color: #fff;"><?php echo $demo3 ;?> <span style="color: #000;"> <b> | </b> </span><strong style="text-transform: uppercase;"><?php echo $_SESSION['admin']; ?></strong>
       <span style="color: #000;"> <b> | </b> </span>

        <strong style="text-transform: uppercase;"><?php echo $_SESSION['employee']; ?></strong> <span style="color: #000;"> <b> | </b> </span>
      <a href="admin_login.php" style="color: yellow;"><i class="fa fa-sign-out" aria-hidden="true"></i>
        Logout</a> </p>
    <?php endif ?>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li>
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li>
         <li>
          <a href="party_update_grn.php">
            <i class="fa fa-edit"></i> <span>Party Name Update</span>
          </a>
        </li>
        <li>
<!--           found_internal_jobcardupdate.php(For Backup) -->
          <a href="fount_internal.php">
            <i class="fa fa-book"></i> <span>Internal JobCard Update</span>
          </a>
        </li>

        <li>
          <a href="found_ext_jobcard_view.php">
            <i class="fa fa-book"></i> <span>External JobCard Update</span>
          </a>
        </li>

      </ul>
    </section>
  </aside>