

<?php 

include("header.php");
include("aside_main.php");
include('connect.php');

$demo=$_SESSION['admin'];
$demo1=$_SESSION['employee'];
$demo1=$_SESSION['empcode'];
$demo3=$_SESSION['username'];  
    



?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RRpl</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1> Dashboard </h1>
    </section>
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Dashboard</h3>
        </div>
         <div class="row">
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="icon ion-android-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Internal Jobcard</span>
              <span class="info-box-number">
                <?php
              include('../../connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT job_card_no FROM job_card_record group by job_card_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              </span>
            </div>

          </div>

        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="icon ion-ios-skipforward"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">External Jobcard</span>
              <span class="info-box-number">
                  <?php
              include('../../connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT job_card_no FROM external_job_cards_main group by job_card_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              </span>
            </div>

          </div>

        </div>

      </div>
       
        </div>

    </section>
          </div>
  

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2021-2022 <a href="https://adminlte.io">RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

