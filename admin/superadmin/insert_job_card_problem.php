<?php 

  session_start(); 

  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

?>
<?php
include("connect.php");
include('header.php');

$username =$_SESSION['username'];
$empcode =$_SESSION['empcode'];
// $sno = $conn->real_escape_string(htmlspecialchars($_POST['sno']));
// $sno_no = $sno +1;
// echo $username;
// echo $empcode;
// echo $sno;
// exit();
$username = $conn->real_escape_string(htmlspecialchars($_POST['username']));
$truck_driver = $conn->real_escape_string(htmlspecialchars($_POST['truck_driver']));
$truck_no1 = $conn->real_escape_string(htmlspecialchars($_POST['truck_no1']));
$inspection_no = $conn->real_escape_string(htmlspecialchars($_POST['inspection_no']));

$km =$conn->real_escape_string(htmlspecialchars($_POST['km']));
$now_date = $conn->real_escape_string(htmlspecialchars($_POST['now_date']));
$job_card_date = $conn->real_escape_string(htmlspecialchars($_POST['job_card_date']));
// echo $km;
// echo $jobcard_end_date;
// echo $jobcard_start_date;
// exit();

$job_card_no = $conn->real_escape_string(htmlspecialchars($_POST['job_card_no']));
$date1 = $conn->real_escape_string(htmlspecialchars($_POST['date1']));
$start_time = $conn->real_escape_string(htmlspecialchars($_POST['start_time']));
$work_action = $conn->real_escape_string(htmlspecialchars($_POST['work_action']));
$jobcard_work = $conn->real_escape_string(htmlspecialchars($_POST['jobcard_work']));

$mistry_amount = $conn->real_escape_string(htmlspecialchars($_POST['mistry_amount']));

$department = $conn->real_escape_string(htmlspecialchars($_POST['department']));
$mistry = $conn->real_escape_string(htmlspecialchars($_POST['mistry']));
try{

$conn->query("START TRANSACTION");
// $sno = $_POST['sno'];
// $sno_no = $sno +1;
// $username =  $_POST['username'];
// $truck_driver = $_POST['truck_driver'];
// $truck_no1 = $_POST['truck_no1'];
// $inspection_no = $_POST['inspection_no'];
// $job_card_no = $_POST['job_card_no'];
// $date1 = $_POST['date1'];
// $start_time = $_POST['start_time'];
// $work_action= $_POST['work_action'];
// $department= $_POST['department'];
// $mistry= $_POST['mistry'];

if(empty($department)) {
  throw new Exception("Please select department");
// echo "Please select department";
// exit();


}if(empty($mistry)) {
   throw new Exception("Please select mistry");
// echo "Please select mistry";
// exit();
}

$sql ="INSERT INTO  job_card_record(truck_no1,truck_driver,inspection_no,job_card_no,date1,start_time,work_action,now_date,department,manually_mistry,job_card_work,work_amount,username,employee,km,job_card_date,logs) VALUES('$truck_no1','$truck_driver','$inspection_no','$job_card_no','$date1','$start_time','$work_action','$now_date','$department','$mistry','$jobcard_work','$mistry_amount','$username','$empcode','$km','$job_card_date','Close jobcard then Add Service') ";
  // echo $sql;
if($conn->query($sql) === FALSE) 
            { 
              throw new Exception("Code 001 : ".mysqli_error($conn));        
            }
// $sqld = $conn->query($sql);

$file_name= basename(__FILE__);
$type=1;
$val="Add New Problem After Insperction:"."Department-".$department.",Mistry-".$mistry.",Driver Compltion-".$work_action;  
$sql="INSERT INTO `allerror`(`file_name`,`user_name`,`employee`,`error`,`type`) VALUES ('$file_name','$username','$empcode','$val','$type')";
// echo $sql;
    if ($conn->query($sql)===FALSE) {
    echo "Error";
     } 


     $conn->query("COMMIT");
echo "
<form id='Form1' action='fount_internal.php' method='POST'>
<input type='hidden' name='truck_no' value='$truck_no1'>
<input type='hidden' name='username' value='$username'>
</form>
<SCRIPT>
 alert('Problem add Successfully');
 document.getElementById('Form1').submit();
</SCRIPT>";
}

  catch (Exception $e) {
    //role back to using
      $conn->query("ROLLBACK");
      $content = htmlspecialchars($e->getMessage());
      $content = htmlentities($conn->real_escape_string($content));

             echo "<SCRIPT>
	       alert('$content');
	       window.location.href='fount_internal.php';
	 </SCRIPT>";

      $file_name = basename(__FILE__);        
      $sql = "INSERT INTO `allerror`(`file_name`, `user_name`,`employee`,`error`) VALUES ('$file_name ','$username','$empcode','$content')";
      if ($conn->query($sql) === FALSE) { 
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
  }
?>