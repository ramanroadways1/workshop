   <?php
   
include("connect.php");
include('header.php');
include('aside_main.php');

?>
<!DOCTYPE html>
<html>
<head>

      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php ?>
  <div class="content-wrapper">
    
    <section class="content-header">
        </section>

    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Internal jobcard Record Detail</h3>
       
      </div>
        <!-- /.box-header -->

      <script>
        function myFunction() {
          window.print();
        }
       </script>
         <div class="row">
                <div class="col-md-12">
                   <input type="button"  style='margin-left:90%;'  color="Primary" class="btn btn-warning" onclick="myFunction()" value="Print jobcard">
                </div>
                <div class="col-md-12">
                <div class="col-md-3">  
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-3">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-5">  
                     <input type="button" name="filter" id="filter" value="Search" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>

                </div>
              </div>      

        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                      <div id="order_table"> 
                <table id="employee_data" class="table table-striped table-bordered" style="font-family:arial; font-size:  14px;">  
                  <thead>  
                       <tr>  
                           <td>Truck Number</td>
                            <td>Truck Driver</td>
                              <td>Jobcard Number</td>
                            <td>Jobcard Date</td>
                            <td>View</td>
                           <!--  <td>View</td> -->
                       </tr>  
                  </thead>  
                   <?php
                    
                    $show = "SELECT * from  job_card_record  group by job_card_no ORDER BY inspection_no ASC";

                        $result = $conn->query($show); 

                    if ($result->num_rows > 0) {
                    
                      while($row = $result->fetch_assoc()) {
                       
                       $truck_no = $row["truck_no1"];
                       $truck_driver = $row["truck_driver"];
                       $inspection_no = $row["inspection_no"];
                       $job_card_no = $row["job_card_no"];
                       $date1 = $row["now_date"];
                        
                      ?>
                      <tr> 
                
                  <td><?php echo $truck_no?>
                    <input type="hidden" name="truck_no" value='<?php echo $truck_no; ?>'>
                  </td>
                  
                   <td><?php echo $truck_driver?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $truck_driver; ?>" >
                  </td>
                   <td><?php echo $job_card_no?>
                    <input type="hidden" readonly="readonly" name="truck_driver" value="<?php echo $job_card_no; ?>" >
                  </td>
                
                  <td><?php echo $date1?>
                    <input type="hidden" readonly="readonly" name="date1" value="<?php echo $date1; ?>" >
                  </td>
                   <td>
                    <form method="POST" action="internal_jobcard_update.php" target="_blank">
                      <input type="submit"  name="inspection_no" value="<?php echo $inspection_no; ?>" class="btn btn-primary btn btn-sm" >
                    </form>
                  </td>
                     
                </tr>
           
              <?php
                   
                }
              } else {
                  echo "0 results";
              }
              ?>
            </table>
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <div class="box-footer clearfix">
       
        </div>
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  

                          url:"filter1.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                          //alert(data)
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>

