<!DOCTYPE html>
<html>
<head>
  <?php 


   include("header.php");
   include("aside_main.php");

  ?>
  <style type="text/css"> 
.modal-backdrop
{
    opacity:0.5 !important;
}
</style>

   <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>

 <script type="text/javascript">
    function ModalNew(val){
    // $('#loadicon').show(); 
    var id = val;  
           $.ajax({  
                url:"update_partyongrn.php",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     // $('#loadicon').hide(); 
                }  
           });
    }

 </script>
<script type="text/javascript">
      $(document).on('submit', '#emp', function()
    {  
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'update_empname.php',
        data : data,
        success: function(data) {  
        $('#exampleModal').modal("hide");  
        $('#response').html(data);  
        // $('#user_data').DataTable().ajax.reload();  
        }
      });
      return false;
    }); 
</script>

<div id="response"></div>


      <link href="../../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../../assets/jquery-3.5.1.js"></script>
      <script src="../../assets/jquery-ui.min.js"></script>
      <link href="../../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../../assets/custom.css">

</head>
 

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
            
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                          
                            <tr>
                                 <!--<td>Delete GRN</td>-->
                                     <td>GRN No</td>
                                     <td>P.O No</td>
                                     <td>Party Name</td>
                                     <td>Total(GRN Total)</td>
                                     <td>Branch</td>
                                    <td>Update Party Name</td>
                                  
                            </tr>

                         </thead>
                         <tfoot class="thead-light">
                            <tr>

                            </tr>
                         </tfoot>
                      </table>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "fetch_grn_party.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV' 
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                           
                         ],
                          "searchBuilder": {
                          "preDefined": {
                               // "criteria": [
                              //     {
                              //         "data": ' VEHICLE ',
                              //         "condition": '=',
                              //         "value": ['']
                              //     }
                              // ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../../assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          $('title').html(" RRPL - Vendor Management System ADMIN");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "party_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div id="result_main"></div>
              <div id="result"></div> 
 

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

        <script src="../../assets/bootstrap.js"></script>

        <script src="../../assets/bootstrap.bundle.js"></script>
        <script src="../../assets/scripts.js"></script>
        <script src="../../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../../assets/dataTables.searchBuilder.min.js"></script>


</body>
</html>
                    <div id="result_main22"></div>
                            <script>
                              function DeleteModal(purchase_id){
                                var purchase_id = purchase_id;
                                var grn_no = $('#grn_no'+purchase_id).val();
                                //alert(grn_no);
                                var purchase_order = $('#purchase_order'+purchase_id).val();
                                var id = $('#id'+purchase_id).val();
                                if (confirm("Do you want to delete this GRN..?"))
                              {
                                if(purchase_id!='')
                               {
                                 jQuery.ajax({
                                    url: "delete_grn.php",
                                    data: 'purchase_id=' + purchase_id  + '&grn_no='+grn_no   + '&purchase_order='+purchase_order   + '&id='+id ,
                                    type: "POST",
                                    success: function(data) { 
                                      alert(data)
                                         location.reload(); 
                                    $("#result_main22").html(data);
                                    },
                                        error: function() {}
                                  });
                                }

                             }

                          }


                              function EditModal(id)
                              {
                                jQuery.ajax({
                                    url: "fetch_grn.php",
                                    data: 'id=' + id,
                                    type: "POST",
                                    success: function(data) {
                                    $("#result_main").html(data);
                                    },
                                        error: function() {}
                                    });
                                document.getElementById("modal_button1").click();
                                $('#ModalId').val(id);

                                }
                           </script><div id="result_main"></div>
                           <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update GRN</h4>
      </div>
      <form action="update_grn.php" method="post" id="FormGRNUpdate">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
              <input type="hidden" id="ModalId" name="id"/>
              <div class="form-group">
                <label>Purchase Order</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="purchase_order11" autocomplete="off" class="form-control" name="purchase_order" required/>
              </div>

              <div class="form-group">
                <label>Product Number</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productno11" autocomplete="off" class="form-control" name="productno" required/>
              </div>

               <div class="form-group">
                <label>Product Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productname11" autocomplete="off" class="form-control" name="productname" required/>
              </div>

               <div class="form-group">
                <label>Party Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="party_name11" autocomplete="off" class="form-control" name="party_name" required/>
              </div>
              
              <div class="form-group">
                 <label>Party Code</label>
                 <input type="text" style="width: 100%;" readonly="readonly" id="party_code11" autocomplete="off" class="form-control" name="party_code" required/>
              </div>

              <div class="form-group">
                 <label>Quantity</label>
                 <input type="text" style="width: 100%;" id="quantity11" autocomplete="off" class="form-control" name="quantity" readonly="readonly" required/>
              </div>             

              <div class="form-group">
                 <label>Rate/unit</label>
                 <input type="text" style="width: 100%;" id="rate11" readonly="readonly" autocomplete="off" class="form-control" name="rate" placeholder="rate unit" required/>
              </div>

             </div>
            <!-- /.col -->
            <div class="col-md-6">
              
               <div class="form-group">
                 <label>Amount</label>
                 <input type="text" style="width: 100%;" autocomplete="off" readonly="readonly" id="amount11" class="form-control" name="amount" placeholder="amount" required/>
              </div>
               <div class="form-group">
                 <label>Gst</label>
                 <input type="number" style="width: 100%;" class="form-control" id="gst11" name="gst" readonly />
               </div>
        
                <div class="form-group">
                  <label>Gst Amount</label>
                  <input type="number" style="width: 100%;" readonly="readonly" class="form-control" id="gst_amount" name="gst_amount" />
                </div>

                <div class="form-group">
                  <label>Total Amount</label>
                    <input type="text" style="width: 100%;" readonly="readonly" id="total_amount" class="form-control" autocomplete="off" name="total_amount" required>
                </div>

                <div class="form-group">
                  <label>Received Quantity</label>
                    <input type="text" style="width: 100%;" id="received_qty" class="form-control" autocomplete="off" readonly="readonly" oninput="ChkForQtyMaxValue();gettotal();"name="received_qty" required>
                </div>

                <div class="form-group">
                  <label>New Total</label>
                    <input type="text" style="width: 100%;" id="new_total_amount" class="form-control" autocomplete="off" name="new_total" readonly>
                </div>
                
                  
            </div>
            <!-- /.col -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Submit</button>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          </div>
      </div>
    </form>
      
    </div>

  </div>
</div>

<script>
  function ChkForQtyMaxValue(myVal)
  {
   var qty = Number($('#quantity11').val());
   var received_qty = Number($('#received_qty').val());
   if(received_qty>qty)
    {
       alert('You can not exceed Quantity '+ qty);
       $('#received_qty').val('');
   }
  }
</script>
