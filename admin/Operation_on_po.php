
 <?php  
  include("connect.php");
       if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
?>  
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

     
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On Purchase Order</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                          <td style="display: none;" scope="row">id</td>
                         <!--  <td>purchase Order</td> -->
                         <!--  <td>Party Name</td>
                          <td>Party Code</td> -->
                          <td>Product Number</td>
                          <td>Product Name</td>
                          <td>Rate(from Rate Master)</td>
                          <td>Rate/Unit</td>
                          <td>Quantity</td>
                          <td>Difference In Rates</td>
                          <td>Amount</td>
                          <td>Gst</td>
                          <td>Gst Amount</td>
                          <td>Total Amount</td>
                          <td>Date</td>
                           <td>Update</td>
                          <td>Delete</td>
                           
                       </tr>  
                  </thead>  
                  <?php  
                  $valueTosearch = $_POST['purchase_order'];

                  $query = "SELECT insert_po.id as id,purchase_order,party_name,party_code,productname,date1,rate ,quantity, amount, total ,gst,insert_po.key1, gst_amount, rate_master,productno, date_purchase_key.date1 FROM date_purchase_key,insert_po WHERE date_purchase_key.key1 = insert_po.key1 and purchase_order='$valueTosearch' order by insert_po.id ASC";
                  $result = mysqli_query($conn,$query);
                  $l_u = 1;
                  $id_customer = 0;
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $key1 = $row['key1'];
                    $purchase_order=$row['purchase_order'];
                    $productno = $row['productno'];
                    $party_name = $row['party_name'];
                    $rate_master = $row['rate_master'];
                    $productname = $row['productname'];
                    $party_code = $row['party_code'];
                    //$product_qun = $row['product_qun'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $total = $row['total'];
                    $gst_amount = $row['gst_amount'];
                    $gst = $row['gst'];
                    $amount = $row['amount'];
                    $diff =  abs($rate_master - $rate);
                    $date=$row['date1']; 
                  ?>
                  <tr>
                    <input type="hidden" name="key1[]" id="key1<?php echo $id; ?>" value='<?php echo $key1; ?>'>
                    <td  style="display: none;">
                      <input type="hidden" name="id[]" value="<?php echo $id; ?>">
                    </td>
                    <td style="display: none;"><?php echo $purchase_order?>
                      <input  type="hidden" readonly="readonly" name="purchase_order[]" value="<?php echo $purchase_order; ?>" id="a<?php echo $id; ?>">
                    </td>
                   <!--  <td ><?php echo $party_name?>
                      <input  type="hidden" readonly="readonly" name="party_name[]" value="<?php echo $party_name; ?>" id="a<?php echo $id; ?>">
                    </td>
                     <td ><?php echo $party_code?>
                      <input  type="hidden" readonly="readonly" name="party_code[]" value="<?php echo $party_code; ?>" id="a<?php echo $id; ?>">
                    </td> -->
                    <td ><?php echo $productno?>
                      <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>" id="a<?php echo $id; ?>">
                    </td>
                    <td ><?php echo $productname?>
                      <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>" id="a<?php echo $id; ?>">
                    </td>
                    
                    <td><?php echo $rate_master?>
                      <input type="hidden" readonly="readonly" style="width: 100%;" id="rate_master<?php echo $l_u; ?>" name="rate_master[]" value="<?php echo $rate_master; ?>" >
                    </td>

                    <td><?php echo $rate?>
                        <input type="hidden" readonly="readonly" id="rate<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="rate[]" value="<?php echo $rate; ?>" >
                      </td>

                     <td><?php echo $quantity?>
                     <input type="hidden" readonly="readonly" id="quantity<?php echo $l_u; ?>"  onclick="getrate(<?php echo $l_u; ?>);" name="quantity[]" value="<?php echo $quantity; ?>" ></td>
     
                      <td><?php echo $diff?>
                       <input type="hidden" readonly="readonly" name="diff[]" value="<?php echo $diff; ?>" >
                      </td>

                     <td><?php echo $amount?>
                        <input type="hidden" readonly="readonly" id="amount<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="amount[]" value="<?php echo $amount; ?>" >
                     </td>

                     <td><?php echo $gst?>
                      <input type="hidden" readonly="readonly" id="gst<?php echo $l_u; ?>" onclick="getgst(<?php echo $l_u; ?>)" name="gst[]" value="<?php echo $gst; ?>" >
                     </td>

                    <td><?php echo $gst_amount?>
                      <input type="hidden" readonly="readonly" id="gst_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="gst_amount[]" value="<?php echo $gst_amount; ?>" >
                    </td>

                     <td><?php echo $total?>
                        <input type="hidden" readonly="readonly" id="total_amount<?php echo $l_u; ?>" name="total[]" onclick="gettotal(<?php echo $l_u; ?>);"  value="<?php echo $total; ?>">
                    </td>

                    <td><?php echo $date?>
                      <input type="hidden" readonly="readonly" name="date[]" value="<?php echo $date; ?>" >
                    </td>
                    <td>
                      <input type="button" onclick="EditModal(<?php echo $id; ?>)" name="id" value="Update" class="btn btn-info btn-sm" />
                    </td>
                    
                    <td>
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="delete" value="Delete" class="btn btn-danger btn-sm" />
                    </td>
                            <script>
                                  function EditModal(id)
                                  {
                                    jQuery.ajax({
                                        url: "fetch_po.php",
                                        data: 'id=' + id,
                                        type: "POST",
                                        success: function(data) {
                                          
                                        $("#result_main").html(data);
                                        },
                                            error: function() {}
                                        });
                                    document.getElementById("modal_button1").click();
                                    $('#myModal_id').val(id);

                                    }
                              </script><div id="result_main"></div>
                    <script>
                      function DeleteModal(id)
                      {
                        //alert(id);
                        var id = id;
                        var key1 = $('#key1'+id).val();
                        if (confirm("Do you want to delete this purchase order?"))
                        {
                          if(id!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_po.php",
                                  data:'id='+id + '&key1='+key1 ,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>

      <form action="update_po.php" autocomplete="off" id="FormPOUpdate" method="POST">
     
       <div id="myModal22" class="modal fade" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update PO</h4>
          </div>
          
      <script type="text/javascript"> 
      $(function()
      { 
      $( "#productno" ).autocomplete({
      source: 'po_no_search.php',
      change: function (event, ui) {
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      alert('Product does not exists.');
      $('#form')[0].reset();
      } 
      },
      focus: function (event, ui) { $('#form')[0].reset(); return false; } }); });
      </script>  
          
          <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="hidden" style="width: 100%;" class="form-control" id="id" name="id" readonly/>
              <label>Product number</label>
           
              <input type="text" style="width: 100%;" class="form-control" id="productno" onblur="get_data(this.value);" name="productno"  placeholder="product number" readonly required />
              </div>
              <div id="rate_master222"></div>
                  <script>
                   function get_data(val) {
                  $.ajax({
                    type: "POST",
                    url: "get_data1.php",
                    data:'productno='+val,
                    success: function(data){
                      $("#rate_master222").html(data);
                    }
                    });
                  }
                </script>
               <div class="form-group">
              <label>Product name</label>
              <div>
                <div  class="rs-select2 js-select-simple select--no-search">
                   <input class="form-control" name="productname" id="productname"  placeholder="product Name" readonly>
                </div>
              </div>
              </div>

               <div class="form-group">
              <label>Rate master</label>
              <div  class="rs-select2 js-select-simple select--no-search">
                <input class="form-control" name="rate_master" id="rate_master" onblur="getdiff(this.value);" placeholder="rate master"  readonly>
              </div>
              </div>

               <input type="hidden" class="form-control" name="lock_unlock" id="lock_unlock" readonly required>
              
              <div class="form-group">
               <label>Quantity</label>
               <input type="number" oninput="sum1()" style="width: 100%;"  id="quantity" class="form-control" name="quantity" placeholder="Quantity" required/>
              </div>

              <script>
              function ChkForRateMaxValue(myVal)
               {
                 var myrate = Number($('#rate').val());
                 var fix_rate = Number($('#rate_master').val());
                 var lock = $('#lock_unlock').val();
                 if(lock==1)
                 {
                   if(myrate>fix_rate)
                   {
                     alert('You can not exceed Master Rate. Master rate is '+ fix_rate);
                     $('#rate').val('');
                   }
                 }
                }
              </script>

              <div class="form-group">
               <label>Rate unit</label>
               <input type="Number" step="any" oninput="ChkForRateMaxValue();sum1();" onblur="getdiff(this.value);" style="width: 100%;" id="rate" class="form-control" name="rate" placeholder="rate unit" required/>
              </div>
              
                <script>
                function sum1()
                {
                 var qty = Number($('#quantity').val());  
                 var rate = Number($('#rate').val());  
                 var rate_master = Number($('#rate_master').val());  
                 var gst = Number($('#gst').val());  
                 var difference = rate_master-rate;
                 var amt = qty*rate;
                 
                 $('#amount').val(amt);
                 $('#gst_amount').val((amt*gst/100).toFixed(2));
                 $('#total').val(Number($('#gst_amount').val())+Number($('#amount').val()));
                 $('#diff').val(difference);
                }
                </script>

              <div class="form-group">
               <label>Rate Difference</label>
               <input type="text" style="width: 100%;" id="diff" class="form-control" name="diff" placeholder="Rate Difference" readonly required/>
              </div>
              <script>
              /*function getdiff(i)
              {
                var rate1 = Number($('#rate_master').val());  
                var rate2 = Number($('#rate').val());
                var difference = rate1-rate2;
                $('#diff').val(difference);
               // $('#diff').val(Number($('#rate_master').val())-Number($('#rate').val()));
              }*/
              </script>
              
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
               <label>Amount</label>
               <input type="text" style="width: 100%;" id="amount" class="form-control" name="amount" placeholder="amount" readonly required/>
              </div>
              
             <div class="form-group">
               <label>gst value</label>
               <select onchange="sum1();MyGst(this.value);" class="form-control" style="width: 100%;" required id="gst" name="gst_value">
                  <option value="0">0</option>
                  <option value="5">5</option>
                  <option value="12">12</option>
                  <option value="18">18</option>
                  <option value="28">28</option>
                </select>
              </div>
          

                <div class="form-group">
                <label>gst_amount</label>
                   <input type="text" style="width: 100%;" class="form-control" id="gst_amount" name="gst_amount" readonly placeholder="gst amount" />
                </div>

                <div class="form-group">
                <label >gst_type</label>
                  <input type="text" style="width: 100%;" class="form-control" id="gst_type" name="gst_type" readonly/>
                </select> 
                </div>
                
              <script>
              function MyGst(i)
              { 
                var gst = Number($('#gst').val());  
                var gst_type=document.getElementById("gst_type").value;
                if(gst_type=='cgst_sgst')
                {
                  $('#gst_acc_type').val(gst/2);
                }
                else{
                  $('#gst_acc_type').val(gst);
                }
              }
              </script>

                <div class="form-group">
                <label>Selected Gst</label>
                  <input type="text" style="width: 100%;" class="form-control prc" id="gst_acc_type" name="gst_acc_type" readonly placeholder="Selected G.S.T" required>
                </div>

                <div class="form-group">
                <label>total amount</label>
                  <input type="text" style="width: 100%;" id="total" class="form-control" name="total_amount"  placeholder="Total Amount" required readonly>
                </div>
                
            </div>
            <!-- /.col -->

            </div>
          </div>
          <div class="modal-footer">
          <!--  <input type="reset" align="right" name="Reset" value="Reset" tabindex="50"> -->
              
          <button type="submit" class="btn btn-danger">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

        </div>
      </div>
    </form>

