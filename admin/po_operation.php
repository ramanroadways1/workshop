
<!DOCTYPE html>
<html>
<head>
  <?php 
  include("header.php");
  include("aside_main.php");
  ?>
 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script >
    function getrate(i) {
      var rate = document.getElementById("rate"+i).value;

      var quantity = document.getElementById("quantity"+i).value;
      var amount = document.getElementById("amount"+i).value;
       
      if(!(rate==0))
       {
        if(!(quantity==0))
        {
           document.getElementById("amount"+i).value=rate*quantity;
        }
      }
      else if(!(quantity==0))
         {
          if(!(amount==0))
          {
             document.getElementById("rate"+i).value=amount/quantity;
          }
         }
      }

    function getgst(i)

    {
        var amount1 = document.getElementById("amount"+i).value;
       
        var gst1=document.getElementById("gst"+i).value;
        var total_gst1=document.getElementById("gst_amount"+i).value;
        if(!(amount1==0))
        {
          if(!(gst1==0))
          {
            document.getElementById("gst_amount"+i).value=amount1*gst1/100;
          }
          else if(!(total_gst1==0))
          {
            document.getElementById("gst"+i).value=total_gst1*100/amount1;
          }
        }

    }


    function gettotal(i){
      var gst_amount = document.getElementById("gst_amount"+i).value;
      var amount = document.getElementById("amount"+i).value;
      document.getElementById("total_amount"+i).value=parseInt(amount)+parseInt(gst_amount);
    }

</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Search Purchase Order</h1><br>
      </section> -->
      
      
   
    <section class="content">

      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Generate Purchase Order Operation</h3>


          <div class="box-tools pull-right">
            
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
       

      
        <div class="box-body">
          <div class="row">
              
            <!-- /.col -->
         

  
        <center>
            
             <input type="text" id="purchase_order" autocomplete="off" style="width: 200px;" name="valueToSearch" placeholder="Purchase Order">
             <button type="button" onclick="MyFunc1();">Search</button>
           
          </center>
        <script>
          function MyFunc1()
          {
          jQuery.ajax({
              url: "fetch_po_edit.php",
              data: 'purchase_order=' + $('#purchase_order').val() ,
              type: "POST",
              success: function(data) {
              $("#r1").html(data);
              },
              error: function() {}
          });
          }
         </script>
        <div id="r1" class="container">
        </div><br>
     
              
       </div><!-- /.col --></div> <!-- /.row --><br>
     
     
         </section>


              <div class="col-sm-offset-2 col-sm-8">
              </div>
        
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

