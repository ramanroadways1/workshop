
<!DOCTYPE html>
<html>
<head>
   <head><script src="swity_alert.js" type="text/javascript"></script>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <script>
    function IsMobileNumber() { 
      var Obj = document.getElementById("txtMobId");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var moPat = /^[1-9]{1}[0-9]{9}$/;
                if (ObjVal.search(moPat) == -1) {
                    alert("Invalid Mobile No");
                    $('#txtMobId').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Mobile No");
                  }
            }
      } 

    function ValidatePAN() { 
      var Obj = document.getElementById("textPanNo");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                if (ObjVal.search(panPat) == -1) {
                    alert("Invalid Pan No... Please Enter In This Format('ABCDE1234F')");
                    $('#textPanNo').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct Pan No.");
                  }
            }
      } 
    </script>
     <script>
      function ValidateGSTIN() { 
      var Obj = document.getElementById("textGSTINNo");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var gstinPat = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
                if (ObjVal.search(gstinPat) == -1) {
                    alert("Invalid GSTIN No... Please Enter In This Format('12ABCDE1234F1ZQ')");
                    $('#textGSTINNo').val('');
                    $('#gst_type').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct GSTIN No.");
                  }
            }
      }
      function ValidateIFSC() { 
      var Obj = document.getElementById("textIFSC");
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var ifscPat = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/;
                if (ObjVal.search(ifscPat) == -1) {
                    alert("Invalid IFSC No... Please Enter In This Format('ABCD0123456')");
                    $('#textIFSC').val('');
                    Obj.focus();
                    return false;
                }
              else
                {
                  alert("Correct IFSC No.");
                  }
            }
      }
   </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_party.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Masters</h1>
    </section> -->
    <section class="content">
      
    
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Create Party Master</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Party Name<font color="red">*</font></label>
                <input type="text" style="width: 100%;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" onblur="MyFunc()" id="party_name" name="party_name" placeholder="Party Name" required/>
              </div>
              <div id="result22"></div>
              <script>
              function MyFunc()
              {
                var party_name = $('#party_name').val();
                if(party_name!='')
                {
                  $.ajax({
                      type: "POST",
                      url: "chkPartyname.php",
                      data:'party_name='+party_name ,
                      success: function(data){
                          $("#result22").html(data);
                      }
                      });
                }
              }     
            </script>
              <div class="form-group">
                <label for="product_no">Party Location<font color="red">*</font></label>
                <input type="text" style="width: 100%;" class="form-control" name="party_location" placeholder="Party location" required/>
              </div>
              <div class="form-group">
                 <label for="product_no">Mobile number<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" MaxLength="10" class="form-control" name="mobile_no" id="txtMobId" onchange="IsMobileNumber(this);" placeholder="Mobile Number" required/>
              </div>
              <div class="form-group">
                 <label for="product_no">Address<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" name="address" placeholder="Address" required/>
              </div>
              <div class="form-group">
                 <label for="product_no">Contact Person<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" class="form-control" name="contact_person" placeholder="Contact Person" required/>
              </div>
              <div class="form-group">
                 <label for="PAN">PAN Number<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" id="textPanNo" MaxLength="10" class="form-control" name="pan" placeholder="PAN Number"onchange="ValidatePAN(this);" required/>
              </div>
              <div class="form-group">
                 <label for="GSTIN">GSTIN Number<font color="red">*</font></label>
                 <input type="text" style="width: 100%;" id="textGSTINNo" MaxLength="15" class="form-control" name="gstin" placeholder="GSTIN Number" oninput="chkGstType();" onchange="ValidateGSTIN(this);" required/>
              </div>
              <div class="form-group">
                 <label for="gst_type">GST Type</label>
                 <input type="text" style="width: 100%;" id="gst_type" class="form-control" name="gst_type" placeholder="GST Type" readonly required />
              </div>
              <script>
                function chkGstType()
              {
               var gstinno = $('#textGSTINNo').val();
               if(gstinno.substr(0,2)==24)
               {
                $('#gst_type').val('cgst_sgst');
               }
               else{
                $('#gst_type').val('igst');
               }
              }
              </script>

            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                    <h2>Account Details</h2>
              </div>
            <div class="form-group">
              <label for="bank_name" >Bank Name<font color="red">*</font></label>
              <!-- <select class="form-control" id="bank" name="bank"  autocomplete="off" onchange="get_company(this.value);" required>
                    <option disabled="disabled" selected="selected">Bank Name</option>
                    <option>State Bank of India</option>
                    <option>Bank of Baroda</option>
                    <option>Panjab National Bank</option>
                    <option>HDFC</option>
                    <option>ICICI</option>
                   </select>
                <div class="select-dropdown"></div> -->
                <input type="text" style="width: 100%;" id="bank" class="form-control" name="bank" placeholder="Bank Name" required/>
              </div>
        

                <div class="form-group" >
                  <label for="branch" >Branch Name</label>
                    <input type="text" style="width: 100%;" id="branch" class="form-control" name="branch" placeholder="Branch Name" />
                </div>
                  <div class="form-group">
                    <label for="Account_no" >Account Number<font color="red">*</font></label>
                       <input type="number" style="width: 100%;" id="textAccNo" class="form-control" name="acc_no" placeholder="Account Number" />
                  </div>
                  <div class="form-group">
                    <label for="Account Holder Name" >Account Holder Name<font color="red">*</font></label>
                       <input type="text" style="width: 100%;" id="textAccHolderName" class="form-control" name="acc_holder_name" placeholder="Account Holder Name" required/>
                  </div>
                  <div class="form-group">
                    <label for="IFSC_code">IFSC Code<font color="red">*</font></label>
                      <input type="text" style="width: 100%;" id="textIFSC" class="form-control" name="ifsc_code" placeholder="IFSC Code" onchange="ValidateIFSC(this);" required/>
                  </div>
                 
            </div>
            <!-- /.col -->
          </div>

          <!-- /.row -->

        </div>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
     <center><button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
      <a href="party_view.php" class="btn btn-warning">Show Party</a>
     </center>
    </div>
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>


