
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form action="insert_approve_invoice.php" method="post">
  <div class="content-wrapper">
   
    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Approve Goods Recive Notes</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
               <?php $username =  $_SESSION['username'];  ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">
              
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                         <th scope="row">GRN No</th>
                        <th style="display: none;">ID</th>
                        <td style="display: none;">GRN No</td>
                        <td  style="display: none;">Purchase Order</td> 
                        <td>Party Name</td>
                        <td>Product Number</td>
                        <td>Challan File</td>
                        <td>Invoice File</td>
                        <td>Total Amount</td>
                        <td>Date/Time</td>
                        <td>Done</td>
                         <td>Delete</td>
                       </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $show = "SELECT GROUP_CONCAT(id) as id, grn_no, GROUP_CONCAT(purchase_order) as purchase_order, party_name, key1, count(productno) as productno,  sum(new_total) as new_total, challan_file, invoice_file,timestamp1 FROM files_upload where approve_status='0' and username='$username' and length(invoice_file) > 10 group by grn_no order by timestamp1 asc";
                  $result = $conn->query($show);
                  if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { $id = $row['id'];
                    $grn_no = $row['grn_no'];
                                      $key1 = $row['key1'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                    $productno = $row['productno'];
                    $challan_file = $row['challan_file'];
                    $invoice_file = $row['invoice_file'];
                    $new_total = $row['new_total'];
                    $challan_file1=explode(",",$challan_file);
                    $count3=count($challan_file1);
                    $invoice_file1=explode(",",$invoice_file);
                    $count4=count($invoice_file1);
                    $timestamp1 = $row['timestamp1'];
                  ?>

                       <tr> 
                         <td>
                           
                              <input type="submit" formaction="show_detail_approve_invoice.php"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-primary btn-sm">
                           
                          </td>

                          <td style="display: none;"><?php echo $id; ?>
                        <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                      </td>
                       <td style="display: none;"><?php echo $grn_no; ?>
                        <input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'>
                      </td>
                      <td style="display: none;"><?php echo $purchase_order; ?>
                        <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                      </td> 
                      <td><?php echo $party_name; ?>
                        <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                      </td>
                      <td><?php echo $productno; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>
                      <td>
                       <?php
                        if (strlen($challan_file) > 0) {
                         for($i=0; $i<$count3; $i++){
                         
                          ?>
                           <a href="<?php echo $challan_file1[$i]; ?>" target="_blank">Challan <?php echo $i+1; ?></a>
                           <input type="hidden" name="challan_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $challan_file; ?>'>
                          <?php }  }  
                     else{
                      echo "no file";
                     }
                     ?>
                      </td>
                      <td>
                         <?php
                         for($j=0; $j<$count4; $j++){
                         
                          ?>
                           <a href="<?php echo $invoice_file1[$j]; ?>" target="_blank">Invoice <?php echo $j+1; ?></a>
                           <input type="hidden" name="invoice_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $invoice_file; ?>'>
                           <?php
                         }

                        ?>
                        <!-- <a href="<?php echo $invoice_file; ?>" target="_blank"><?php echo $invoice_file; ?></a>
                        <input type="hidden" name="invoice_file[]" value='<?php echo $invoice_file; ?>'> -->
                      </td>
                      <td><?php echo $new_total; ?>
                        <input type="hidden" name="new_total[]" value='<?php echo $new_total; ?>'>
                      </td>
                     <!--  <td>
                        <input type="number"  class="form-control" id="payment" name="payment[]" placeholder="Pay Amount"/>
                      </td> -->
                      <td><?php echo $timestamp1; ?>
                        <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                      </td>
                      <td>
                         <button type="button"  class="btn btn-success btn btn-sm" onclick="addComplainFunc('<?php echo $grn_no; ?>');" >Approve</button>
                      </td>
                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $grn_no;?>')" name="delete" value="Delete" class="btn btn-danger btn btn-sm" />
                    </td> 

                     <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                  var grn_no = val;
                                  //alert(grn_no);
                                  var username = '<?php echo $username ?>'
                                   if (confirm("Do you want to Approve this GRN..?"))
                              {
                                if(grn_no!='')
                               {
                                    $.ajax({
                                       type:"post",
                                       url:"update_payment_status.php",
                                      data:'grn_no='+grn_no + '&username='+username,
                                        success:function(data){
                                           alert(data)
                                           location.reload(); 
                                                
                                          }
                                      });
                                 }
                               }
                             }
                    </script>

                    <script>
                      function DeleteModal(grn_no)
                      {
                        var grn_no = grn_no;
                        var key1 = $('#key1'+grn_no).val();
                        if (confirm("Do you want to delete this grn?"))
                        {
                          if(grn_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_approve_payment.php",
                                  data:'grn_no='+grn_no + '&key1='+key1 ,
                                  success: function(data){
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                    </tr>  
                 <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>
                </table>  
               <!--  <center>
                  <input type="submit" class="btn btn-primary" name="action[]" value="Approved">
                </center> -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

