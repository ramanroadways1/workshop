
<?php  include("connect.php"); 

     if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
   ?>
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <?php include('aside_main.php');?>

<script >
  function getrate() {

      var rate = document.getElementById("rate").value;

      var quantity = document.getElementById("quantity").value;

      var amount = document.getElementById("amount").value;
       
      if(!(rate==0))
       {
        if(!(quantity==0))
        {
           document.getElementById("amount").value=rate*quantity;
        }
      }
      else if(!(quantity==0))
         {
          if(!(amount==0))
          {
             document.getElementById("rate").value=amount/quantity;
          }
         }
      }
function getgst()

    {
        var amount1 = document.getElementById("amount").value;
       
        var gst1=document.getElementById("gst").value;
        var total_gst1=document.getElementById("gst_amount").value;
        if(!(amount1==0))
        {
          if(!(gst1==0))
          {
            document.getElementById("gst_amount").value=amount1*gst1/100;
          }
          else if(!(total_gst1==0))
          {
            document.getElementById("gst").value=total_gst1*100/amount1;
          }
        }

    }


    function get_total(){
  /*    alert("hi");*/
      var gst_amount = document.getElementById("gst_amount").value;
      //alert(gst_amount);
      var amount = document.getElementById("amount").value;
      //alert(amount);
      document.getElementById("total_amount").value=parseInt(amount)+parseInt(gst_amount);
    }


     function get_gst()
    { 
        var gst_value1=document.getElementById("gst").value;
        var gst_type1= document.getElementById("gst_type").value;
        if(gst_type1=="cgst_sgst"){
            var result = gst_type1+ "," +gst_value1/2;
            //alert(result);
          }else
          {
            var result = gst_type1+ "," +gst_value1;
          }
          //var result= parseInt(gst_value1)*parseInt(gst_type1);
          document.getElementById("sel_gst").value=result;
          
    }
    function gettotal(){
      var received_qty = document.getElementById("received_qty").value;
      var rate = document.getElementById("rate11").value;
      var new_amount = received_qty*rate;
      var gst= document.getElementById("gst11").value;
      var gst_amount=new_amount*gst/100;
     
      if(!(received_qty==0)){
        document.getElementById("new_total_amount").value=parseInt(new_amount)+parseInt(gst_amount);
      }
    }

</script>

  <script>
                      $(document).ready(function() {
                        $("#datepicker").datepicker({
                           dateFormat: "yy-mm-dd",
                          /* maxDate: '0',
                            minDate: '-30'*/
                        });
                      });



                       /* $( function() {
                          $( "#datepicker" ).datepicker();

                        } );*/
                    </script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">

     
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Operation On G.R.N</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table  class="table table-striped table-bordered" style="width: 100%"> 
                    <?php
                       $value_To_search = $_POST['purchase_order'];
                       //echo $value_To_search;
                        $query = "SELECT id, grn_no, purchase_order, productno, productname, party_name, party_code, quantity, rate,amount,gst,gst_amount,total_amount,received_qty,new_total,key1,timestamp1 FROM grn where purchase_order='$value_To_search'";

                         /* $query5 = "SELECT challan_no,challan_file,invoice_file,invoice_no FROM files_upload where purchase_order='$value_To_search'";*/
                     
                        $query3 = "SELECT grn_no,id,challan_no,invoice_no,challan_file,invoice_file,back_date FROM files_upload where purchase_order='$value_To_search'";
                        $result3 = mysqli_query($conn, $query3);
                        while($row2 = mysqli_fetch_array($result3))
                            {   
                              $id1=$row2['id'];
                              $grn_no=$row2['grn_no'];
                              $back_date=$row2['back_date'];
                              $challan_no=$row2['challan_no'];
                              $challan_file1 = $row2['challan_file'];
                              $challan_file=explode(",",$challan_file1);
                              $count2=count($challan_file);

                              $invoice_no = $row2['invoice_no'];
                              $invoice_file1 = $row2['invoice_file'];
                              $invoice_file=explode(",",$invoice_file1);
                              $count3=count($invoice_file);
                              
                            }
                        
                        $result = mysqli_query($conn, $query);
                        $l_u = 1;
                        $id_customer = 0;
                      ?> 
                     <thead>  
                           <tr>  
                                 <td >Challan No</td>
                                   <td >Challan Bill</td>
                                  <td>Invoice No</td>
                                  <td>Invoice Bill</td>
                                   <td>GRN Date(Back Date)</td>
                                  <td>Update</td> 
                           </tr>  
                      </thead>
                  <tr> 
                      <td ><?php echo $challan_no?>
                        <input type="hidden" name="purchase_order" id="purchase_order" value="<?php echo $purchase_order; ?>">
                      </td>
                        <td >
                             <?php 
                                  if (strlen(@$challan_file1) > 0) {
                                   for($i=0; $i<$count2; $i++){
                                    ?>
                                     <a href="../<?php echo $challan_file[$i]; ?>" target="_blank"><?php echo $challan_file[$i]; ?></a>
                                     <?php
                                   }
                                   ?>
                              
                                   <?php
                                   }
                                 else{
                                  echo "no file";
                               }  
                              ?>
                    </td>
                    <td ><?php echo $invoice_no?>
                      <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>" id="a<?php echo $id; ?>">
                    </td>
                    <td> 
                          <?php 
                                if (strlen(@$invoice_file1) > 0) {
                                 for($i=0; $i<$count3; $i++){
                                  ?>
                                   <a href="../<?php echo $invoice_file[$i]; ?>" target="_blank"><?php echo $invoice_file[$i]; ?></a>
                                   <?php
                                 }
                                 ?>
                                 
                                 <?php
                                 }
                               else{
                                echo "no file";
                               } 
                              ?>
                    </td> 
                    <td>
                      <input type="text" readonly="readonly" name="back_date" value="<?php echo $back_date; ?>" />
                    </td>
                     <td>
                      <input type="button" onclick="EditModal2('<?php echo $grn_no; ?>');" onblur="check_remain()" class="btn btn-info btn-sm" name="grn_no" value="Update Bill" />
                    </td>
                  </tr>
                   <script>
                              function EditModal2(grn_no)
                              {
                                jQuery.ajax({
                                    url: "fetch_grn_bill.php",
                                    data: 'grn_no=' + grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main2").html(data);
                                    }, 
                                        error: function() {}
                                    });

                                  document.getElementById("modal_button2").click();
                                $('#ModalId2').val(grn_no_bill);
                                }


                           </script><div id="result_main2"></div>
                


                    <thead>  
                       <tr>  
                          
                           <td scope="row">id</td>
                              <td >grn no</td>
                               <td style="display: none;">Party Name</td>
                              <td>Product Number</td>
                              <td>Product Name</td>
                            <!--   <td>Party Code</td> -->
                              <td>Quantity</td>
                              <td>Rate/Unit</td>
                              <td>Amount</td>
                              <td>Gst</td>
                              <td>Gst Amount</td>
                              <td>Total Amount</td>
                              <td>Received Quantity</td>
                              <td>New Total</td>
                              <td>Date</td>
                              <td>Update</td> 
                              <td>Delete</td>  
                       </tr>  
                  </thead> 

                  <?php  

                 /* $query4 = "SELECT grn.id ,grn.grn_no, grn.rate, files_upload.remain,files_upload.id as files_upload_id FROM grn INNER JOIN files_upload ON grn.id=files_upload.grn_id WHERE grn.grn_no='$value_To_search'";
                   $result4 = mysqli_query($conn, $query4);
                   echo $query4;
                   */
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id']; 
                    $key1 = $row['key1'];
                    $grn_no=$row['grn_no'];
                    $productno = $row['productno'];
                    $productname = $row['productname'];
                    $party_name = $row['party_name'];
                    $total_amount = $row['total_amount'];
                    $party_code = $row['party_code'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $gst_amount = $row['gst_amount'];
                    $gst = $row['gst'];
                    $amount = $row['amount'];
                    $date=$row['timestamp1']; 
                    $received_qty = $row['received_qty'];
                    $new_total=$row['new_total']; 
                    $purchase_order = $row['purchase_order'];

                    /*$query2 = "SELECT id,grn_no FROM files_upload  where grn_no='$value_To_search' ";
                      $result2 = mysqli_query($conn, $query2);
                       while($row3 = mysqli_fetch_array($result2))
                      {
                       $files_upload_id = $row3['id'];*/
                  ?>
                  <tr>
                    <input type="hidden" name="key1[]" id="key1<?php echo $id; ?>" value='<?php echo $key1; ?>'>

                   <!--  <td ><?php echo $files_upload_id?>
                      <input type="hidden" name="id" id="id" value="<?php echo $files_upload_id; ?>">
                    </td>
 -->
                    
                    <td ><?php echo $id?>
                      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                    </td>
                      <td ><?php echo $grn_no?>
                      <input type="hidden" name="purchase_order" id="purchase_order" value="<?php echo $purchase_order; ?>">
                    </td>
                    <td style="display: none;"><?php echo $grn_no?>
                      <input  type="hidden" readonly="readonly" name="grn_no[]" id="grn_no" value="<?php echo $grn_no; ?>">
                    </td>

                    <td style="display: none;"><?php echo $party_name?>
                      <input  type="hidden" readonly="readonly" name="party_name[]" value="<?php echo $party_name; ?>" id="a<?php echo $id; ?>">
                    </td> 
                    <td ><?php echo $productno?>
                      <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>" id="productno">
                    </td>
                    <td ><?php echo $productname?>
                      <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>" id="a<?php echo $id; ?>">
                    </td> 

                     
                  
                  <!--    <td ><?php echo $party_code?>
                      <input  type="hidden" readonly="readonly" name="party_code[]" value="<?php echo $party_code; ?>" id="a<?php echo $id; ?>">
                    </td> -->
                    
                    <td><?php echo $quantity?>
                     <input type="hidden" readonly="readonly" id="quantity<?php echo $l_u; ?>"  onclick="getrate(<?php echo $l_u; ?>);" name="quantity[]" value="<?php echo $quantity; ?>" >
                   </td>
              
                    <td><?php echo $rate?>
                        <input type="hidden" readonly="readonly" id="rate<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="rate[]" value="<?php echo $rate; ?>" >
                      </td>

                     <td><?php echo $amount?>
                        <input type="hidden" readonly="readonly" id="amount<?php echo $l_u; ?>" onclick="getrate(<?php echo $l_u; ?>);" name="amount[]" value="<?php echo $amount; ?>" >
                     </td>

                     <td><?php echo $gst?>
                      <input type="hidden" readonly="readonly" id="gst<?php echo $l_u; ?>" onclick="getgst(<?php echo $l_u; ?>)" name="gst[]" value="<?php echo $gst; ?>" >
                     </td>

                    <td><?php echo $gst_amount?>
                      <input type="hidden" readonly="readonly" id="gst_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="gst_amount[]" value="<?php echo $gst_amount; ?>" >
                    </td>
                     <td><?php echo $total_amount?>
                      <input type="hidden" readonly="readonly" id="total_amount<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="total_amount[]" value="<?php echo $total_amount; ?>" >
                    </td>
                    <td><?php echo $received_qty?>
                      <input type="hidden" readonly="readonly" id="received_qty<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" oninput="ChkForQtyMaxValue();gettotal();" name="received_qty[]" value="<?php echo $received_qty; ?>" >
                    </td>
                    <td><?php echo $new_total?>
                      <input type="hidden" readonly="readonly" id="new_total<?php echo $l_u; ?>"  onclick="getgst(<?php echo $l_u; ?>);" name="new_total[]" value="<?php echo $new_total; ?>" >
                    </td>
                    
                    <td><?php echo $date?>
                      <input type="hidden" readonly="readonly" name="date[]" value="<?php echo $date; ?>" >
                    </td>
                    <td>
                      <input type="button" onclick="EditModal('<?php echo $id; ?>');" onblur="check_remain()" class="btn btn-info btn-sm" name="id" value="Update" />
                    </td> 

                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $id;?>')" name="delete" value="Delete" class="btn btn-danger btn-sm" >
                    </td>     

   <script>
          function DeleteModal(id)
          {
            var id = id;
            var productno=productno;
            // var party_code = $('#party_code'+id).val();
            if (confirm("Do you want to delete this grn ?"))
            {
              if(id!='')
              {
                $.ajax({
                      type: "POST",

                      url: "delet_grn.php",
                      data:'id='+id+'&productno='+productno,
                      success: function(data){
                          $("#result22").html(data);
                      }
                  });
              }
            }

          }     
   </script>

<div id="result22"></div>                
                            <script>
                              function EditModal(id)
                              {
                                var grn_no = '<?php echo $grn_no ?>';
                                 var id = id;
                                // alert(id);
                                jQuery.ajax({
                                    url: "fetch_grn.php",
                                    data: 'id=' + id+ '&grn_no='+grn_no,
                                    type: "POST",
                                    success: function(data) {
                                      //alert(data);
                                    $("#result_main").html(data);
                                    }, 
                                        error: function() {}
                                    });
                                document.getElementById("modal_button1").click();
                                $('#ModalId').val(id);

                                }

                             function check_remain()
                             {
                                 var new_total_amount1 = Number($('#new_total_amount').val());  
                                 var remain2 = Number($('#remain11').val());  
                             
                                  if(remain2<new_total_amount1){
                                     document.getElementById("received_qty").disabled  = true;
                                   }
                                   
                             }
                                
                           </script><div id="result_main"></div>
                             
                      </tr>
                      <?php 
                    /*  } */
                        $id_customer++;
                        $l_u++;
                             }

                      ?>  
                </table>  
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


 <button type="button" id="modal_button2" data-toggle="modal" data-target="#myModal22" style="display:none">DEMO</button>
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update GRN Bill</h4>
      </div>
      <form action="update_grn_bill.php" method="post" id="FormGRNUpdate" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
              <input type="hidden" name="grn_no_bill" value="<?php echo $grn_no; ?>" />
                <input type="hidden" name="purchase_order" value="<?php echo $purchase_order; ?>" />
              <input type="hidden" name="id[]" value="<?php echo $id; ?>" />
              

              
 
               
                 <input type="hidden" style="width: 100%;" name="challan_file" value="<?php echo$challan_file1 ?>" id="file"/>
                  <?php 
                                  if (strlen(@$challan_file1) > 0) {
                                    ?>
                                     <div class="form-group">
                                      <label>Challan No</label>
                                      <input type="text" style="width: 100%;"  id="challan_no" autocomplete="off" class="form-control" name="challan_no" required/>
                                    </div>

                                <div class="form-group">
                                  <label>Challan File</label>
                                  <input type="file" style="width: 100%;" name="myfile1[]" id="file" multiple="multiple" />
                                </div>
                              
                                   <?php
                                   }
                                 else{
                                  echo "no file";
                               }  
                              ?>
               
             

                         <input type="hidden" style="width: 100%;" name="invoice_file" value="<?php echo$invoice_file1 ?>" id="file"/>
                              <?php 
                                  if (strlen(@$invoice_file1) > 0) {
                                    ?>
                                     <div class="form-group">
                                      <label>Invice No</label>
                                      <input type="text" style="width: 100%;"  id="invoice_no" autocomplete="off" class="form-control" name="invoice_no" required/>
                                    </div>

                                <div class="form-group">
                                  <label>Invoice File</label>
                                  <input type="file" style="width: 100%;" name="myfile2[]" id="file" multiple="multiple" />
                                </div>
                              
                                   <?php
                                   }
                                 else{
                                  echo "no file";
                               }  
                              ?>
                               <label>Back Date</label>
                              <input type="text" style="width: 100%;" name="back_date"  id="datepicker"/>

              
              
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
      </div>
    </form>
      
    </div>

  </div>
</div>






 <button type="button" id="modal_button1" data-toggle="modal" data-target="#myModal23" style="display:none">DEMO</button>
<div id="myModal23" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update GRN1</h4>
      </div>
      <form action="update_grn.php" method="post" id="FormGRNUpdate">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
              <input type="hidden" id="ModalId" name="id"/>
              <div class="form-group">
                <label>GRN No</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="grn_no11" autocomplete="off" class="form-control" name="grn_no" required/>

                <input type="hidden" style="width: 100%;" readonly="readonly" class="form-control" value="<?php echo $purchase_order ?>" name="purchase_order" required/>

              </div>

              <div class="form-group">
                <label>Product Number</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productno11" autocomplete="off" class="form-control" name="productno" required/>
              </div>

               <div class="form-group">
                <label>Product Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="productname11" autocomplete="off" class="form-control" name="productname" required/>
              </div>

               <div class="form-group">
                <label>Party Name</label>
                <input type="text" style="width: 100%;" readonly="readonly" id="party_name11" autocomplete="off" class="form-control" name="party_name" required/>
              </div>
              
              <div class="form-group">
                 <label>Party Code</label>
                 <input type="text" style="width: 100%;" readonly="readonly" id="party_code11" autocomplete="off" class="form-control" name="party_code" required/>
              </div>

              <div class="form-group">
                 <label>Quantity</label>
                 <input type="text" style="width: 100%;" id="quantity11" autocomplete="off" class="form-control" name="quantity" readonly="readonly" required/>
              </div>             

              <div class="form-group">
                 <label>Rate/unit</label>
                 <input type="text" style="width: 100%;" id="rate11" readonly="readonly" autocomplete="off" class="form-control" name="rate" placeholder="rate unit" required/>
              </div>

             </div>
            <!-- /.col -->
            <div class="col-md-6">
              
               <div class="form-group">
                 <label>Amount</label>
                 <input type="text" style="width: 100%;" autocomplete="off" readonly="readonly" id="amount11" class="form-control" name="amount" placeholder="amount" required/>
              </div>
               <div class="form-group">
                 <label>Gst</label>
                 <input type="number" style="width: 100%;" class="form-control" id="gst11" name="gst" readonly />
               </div>
        
                <div class="form-group">
                  <label>Gst Amount</label>
                  <input type="number" style="width: 100%;" readonly="readonly" class="form-control" id="gst_amount" name="gst_amount" />
                </div>

                <div class="form-group">
                  <label>Total Amount</label>
                    <input type="text" style="width: 100%;" readonly="readonly" id="total_amount" class="form-control" autocomplete="off" name="total_amount" required>
                </div>

                <div class="form-group">
                  <label>Received Quantity</label>
                    <input type="Number" style="width: 100%;" id="received_qty" class="form-control" autocomplete="off"  oninput="ChkForQtyMaxValue();gettotal();"name="received_qty" required>
                  <!--   <input type="text" style="width: 100%;" id="files_upload_id" class="form-control" autocomplete="off" required>  -->
                </div>

                <div class="form-group">
                  <label>New Total</label>
                    <input type="text" style="width: 100%;" id="new_total_amount" class="form-control" autocomplete="off" name="new_total" readonly>
                </div>

                <div class="form-group">
                  <label>Remain Amount</label>
                    <input type="text" style="width: 100%;" id="remain1" class="form-control" autocomplete="off" name="remain1" readonly>
                </div>

                  <!-- <div class="form-group">
                  <label>Remain Amount</label>
                    <input type="text" style="width: 100%;" id="remain11" class="form-control" autocomplete="off" name="remain11" readonly>
                   
                </div> -->
                  
            </div>
            <!-- /.col -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
      </div>
    </form>
      
    </div>

  </div>
</div>
<script>
  function ChkForQtyMaxValue(myVal)
  {
   var qty = Number($('#quantity11').val());
   var received_qty = Number($('#received_qty').val());
   if (received_qty==0) {
    
     alert('You can not enter 0 quantity');
       $('#received_qty').val('');
   }
   
   if(received_qty>qty)
    {
       alert('You can not exceed Quantity '+ qty);
       $('#received_qty').val('');
   }
  }
</script>
