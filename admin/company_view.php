
<!DOCTYPE html>
<html>
<head>
  <?php 
   include("header.php");
   include("connect.php");
   include('aside_main.php');
  if (!isset($_SESSION['admin'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../admin_login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['admin']);
    header("location: ../admin_login.php");
  }

   $admin = $_SESSION['admin'];
   $employee = $_SESSION['employee'];
  ?>

        <link href="../assets/font-awesome.min.css" rel="stylesheet">
      <script src="../assets/jquery-3.5.1.js"></script>
      <script src="../assets/jquery-ui.min.js"></script>
      <link href="../assets/jquery-ui.css" rel="stylesheet"/>
      <link href="../assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" type="text/css" href="../assets/searchBuilder.dataTables.min.css">
      <link rel="stylesheet" type="text/css" href="../assets/custom.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 
  <div class="content-wrapper" > 
    <section class="content" > 
      <div class="box box-default"> 
        <div class="box-body">
            <div align="right">
               <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add New Company</button>

        </div>
                
            </div>
          <div class="row">
                      <table id="user_data" class="table table-bordered table-hover">
                         <thead class="thead-light">
                            <tr>
                               <!-- <th style=" "> TRANSACTIONDATE </th> -->
                                                  <th>ID</th>
                                                  <th>Comapny Name</th>
                                                  <th>Delete</th>
          
                    
                            </tr>
                         </thead>
                         <tfoot class="thead-light">
                            <tr>
                               

                            </tr>
                         </tfoot>
                      </table>

                      <script type="text/javascript">  
                         jQuery( document ).ready(function() {
  
                         var loadurl = "company_fetch.php"; 
                         $('#loadicon').show(); 
                         var table = jQuery('#user_data').DataTable({ 
                         "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
                         "bProcessing": true, 
                         "searchDelay": 1000, 
                         "scrollY": 300,
                         "scrollX": true, 
                         "sAjaxSource": loadurl,
                         "iDisplayLength": 10,
                         "order": [[ 0, "asc" ]], 
                         "dom": 'QlBfrtip',
                         "buttons": [
                         // "colvis"
                           {
                              "extend": 'csv',
                              "text": '<i class="fa fa-file-text-o"></i> CSV',
                              
                           },
                           {
                              "extend": 'excel',
                              "text": '<i class="fa fa-list-ul"></i> EXCEL'
                           },
                           {
                              "extend": 'print',
                              "text": '<i class="fa fa-print"></i> PRINT'
                           },
                           {
                              "extend": 'colvis',
                              "text": '<i class="fa fa-columns"></i> COLUMNS'
                           }
                           
                         ],
                          "searchBuilder": {
                          "preDefined": {
                               // "criteria": [
                              //     {
                              //         "data": ' VEHICLE ',
                              //         "condition": '=',
                              //         "value": ['']
                              //     }
                              // ]
                          }
                          },
                          "language": {
                          "loadingRecords": "&nbsp;",
                          "processing": "<center> <font color=black> कृपया लोड होने तक प्रतीक्षा करें </font> <img src=../assets/loading.gif height=25> </center>"
                          },
                        
                         "initComplete": function( settings, json ) {
                          $('#loadicon').hide();
                         }
                         });  
                        
                          $('title').html(" RRPL - Vendor Management System ADMIN");

                          $("#getPAGE").submit(function(e) {  
                            e.preventDefault();
                            var data = $(this).serialize();
                            loadurl = "party_fetch.php?".concat(data);
                            table.ajax.url(loadurl).load();  
                          }); 

                         });     
                      </script> 
                    
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  
               

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.

    <!-- Trigger the modal with a button -->

  </footer>

  <div class="control-sidebar-bg"></div>
</div>

        <script src="../assets/bootstrap.js"></script>

        <script src="../assets/bootstrap.bundle.js"></script>
        <script src="../assets/scripts.js"></script>
        <script src="../assets/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="../assets/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.flash.min.js"></script>
        <script type="text/javascript" src="../assets/jszip.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.print.min.js"></script>
        <script type="text/javascript" src="../assets/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/dataTables.searchBuilder.min.js"></script>
<script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 
 
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddCompany").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "insert_company.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 

</body>
</html>
 
<div id="result"></div> 

<script>

  function DeleteModal(id)
  {
    var id = id;
    var company = $('#company'+id).val();
    if (confirm("Do you want to delete this company?"))
    {
      if(id!='')
      {
        $.ajax({
              type: "POST",
              url: "delete_company.php",
              data:'id='+id + '&company='+company ,
              success: function(data){
                  $("#result22").html(data);
              }
          });
      }
  }
  }     
</script>
<div id="result22"></div>


<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add New Party</button> -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Company</h4>
      </div>
      <div class="modal-body">
             <form action="insert_company.php" id="AddCompany">
        <div class="modal-body">
          <div class="box-body">
              <div class="col-md-2">
                  <b>company</b>
              </div>
              <div class="col-md-10">
                <input type="text" style="width: 100%;" autocomplete="off" class="form-control" id="company" name="company" placeholder="Company"/>
              </div>
          </div>
      </div>

        <!-- <input id="myModal_id"> -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" >Insert</button>
        </div>
      </form>
      </div>

    </div>

  </div>
</div>