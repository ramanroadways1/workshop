<?php include('connect.php'); ?>
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <script>
   function get_productname(val) {
    $.ajax({
    type: "POST",
    url: "get_product_name.php",
    data:'productno='+val,
    success: function(data){
        $("#productname").html(data);
    }
    });
}

</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_rate.php" autocomplete="off">
  <div class="content-wrapper">
   
    <section class="content">

     
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Create Rate Master</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="productno">Product Number<font color="red">*</font></label>
                <select name="productno" style="width: 80%;" id="productno" onchange="get_productname(this.value);" class="form-control" required> 
                    <option disabled="disabled" selected="selected">Select Product Number</option>
                    <?php 
                        $get=mysqli_query($conn,"SELECT productno FROM product");
                           while($row = mysqli_fetch_assoc($get))
                          {
                    ?>
                    <option value = "<?php echo($row['productno'])?>" >
                      <?php 
                            echo ($row['productno']."<br/>");
                      ?>
                    </option>
                      <?php
                      }
                    ?>
                </select><div class="select-dropdown"></div>
              </div>
              <div class="form-group">
               
                <div  class="rs-select2 js-select-simple select--no-search">
                   <select style="display: none"; class="form-control" name="productname" id="productname" required>
                  <!--   <option value="rate_master" selected="selected">Product Name</option> -->
                   </select>
                  <!--  <div class="select-dropdown"></div> -->
                </div>
              </div>
              <label for="rate">Rate/Unit<font color="red">*</font></label>
              <input type="text" style="width: 80%;" class="form-control" name="rate" placeholder="Rate" required><br>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="Lock/Unlock">Rate Lock/Unlock<font color="red">*</font></label>
                <select name="lock_unlock" style="width: 80%;" id="party_name" class="form-control"> 
                    <option disabled="disabled" selected="selected">Select Rate lock/unlock</option>
                    <option value="1">Lock</option>
                    <option value="0">Unlock</option>
                </select>
              </div>
            </div>
            <!-- /.col -->
          </div>

          <!-- /.row -->

        </div>
      </div>
    </section>
    <div class="col-sm-offset-2 col-sm-8">
     <center><button type="submit" name="submit" color="Primary" form-action="insert_rate.php" class="btn btn-primary">Submit</button>
       <a href="rate_view.php" class="btn btn-warning">Show Rate</a>
     </center>
    </div>
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong><strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

