<?php

  session_start(); 


  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }

  require '../connect.php';


 $username = $_SESSION['username']; 
 $employee = $_SESSION['employee'];
 
 // print_r($employee);
 $conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );
   
         $statement = $conn->prepare("SELECT emp_data.empname,product_servicetemp.id , purchase_order, party_name,system_date,party_code, count(productno) as productno, GROUP_CONCAT(productname) as productname FROM product_servicetemp  LEFT JOIN emp_data ON product_servicetemp.employee = emp_data.empcode WHERE product_servicetemp.status='1' GROUP by purchase_order;"); 

   $statement->execute();
   $result = $statement->fetchAll();
   $count = $statement->rowCount();
   $data = array();
   
   foreach($result as $row)
   {
    
          $id = $row["id"];
          $purchase_order = $row["purchase_order"];
          $productno = $row["productno"];
          $productname = $row['productname'];
          $party_name = $row["party_name"];
          $party_code = $row["party_code"];
          // $rate = $row["rate"];
          // $quantity = $row["quantity"];
          // $amount=$row["amount"];
          // $gst=$row["gst"];
          // $gst_amount=$row["gst_amount"];
          // $total = $row["total"];
          // $timestamp1 = $row['date1'];

       $sub_array = array();
       $sub_array[]  = "<form method=\"POST\" action=\"show_grn_detail.php\">
                              <input type=\"hidden\" name=\"id\" value='".$row['id']."'>
                              <input type=\"hidden\" name=\"purchase_order\" value='".$row["purchase_order"]."'>
                              <input type=\"submit\"  name=\"\" value=\"View\" class=\"btn btn-primary btn-sm\" >
                            </form>";
                             $sub_array[] = $purchase_order;
                            $sub_array[]  = $row["productno"]; 

                            $sub_array[]  = $row["party_name"];
                            $sub_array[]  = $row["party_code"];
                           $sub_array[] =$row["system_date"];
                             $sub_array[]  = $row['empname'];
                             
                            $data[] = $sub_array;
   }
   
   $results = array(
       "sEcho" => 1,
       "iTotalRecords" => $count,
       "iTotalDisplayRecords" => $count,
       "aaData"=>$data);
   
    echo json_encode($results);
    exit;
?>
