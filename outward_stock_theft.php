<?php
include ("connect.php");
session_start();
$empcode = $_SESSION['empcode'];

error_reporting(0);
$office = $conn->real_escape_string(htmlspecialchars($_POST['office']));
$stock_operation_date = $conn->real_escape_string(htmlspecialchars($_POST['theft_out_date']));
$vendor = $conn->real_escape_string(htmlspecialchars($_POST['vendor']));
$productno = $conn->real_escape_string(htmlspecialchars($_POST['productno']));

$productname = $conn->real_escape_string(htmlspecialchars($_POST['productname']));
$producttype = $conn->real_escape_string(htmlspecialchars($_POST['producttype']));
$company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
$product_group = $conn->real_escape_string(htmlspecialchars($_POST['product_group']));

$rate = $conn->real_escape_string(htmlspecialchars($_POST['rate']));
$avl_qty = $conn->real_escape_string(htmlspecialchars($_POST['avl_qty']));
$stock_operation_qty = $conn->real_escape_string(htmlspecialchars($_POST['stock_operation_qty']));

$stock_operation_amount = $conn->real_escape_string(htmlspecialchars($_POST['stock_operation_amount']));

 // $office= $_POST['office']; 
 //  $stock_operation_date= $_POST['theft_out_date']; 
 //  $vendor = $_POST['vendor'];  
 //  $productno = $_POST['productno'];
 //  $productname = $_POST['productname'];
 //  $producttype = $_POST['producttype'];
 //  $company = $_POST['company'];
 //  $product_group = $_POST['product_group'];
 //  $rate = $_POST['rate'];
 //   $avl_qty = $_POST['avl_qty'];
 //  $stock_operation_qty = $_POST['stock_operation_qty'];
 //  $stock_operation_amount = $_POST['stock_operation_amount'];
    try{

          $conn->query("START TRANSACTION"); 

  $now_date = date('Y-m-d');
  $status_temp="theft_temp";

  if (empty($stock_operation_amount)) {
     echo "
        <script>
        alert('Fill All Detail About Product');
         window.location.href='add_scrap_stock.php';
         </script>";
    exit();
  }

    $sql = "insert into outward_stock(vendor,office,stock_operation_date,productno,productname,producttype, company,product_group,rate,avl_qty,stock_operation_qty,stock_operation_amount,now_date,status,employee) values ('$vendor','$office','$stock_operation_date','$productno', '$productname','$producttype', '$company','$product_group','$rate','$avl_qty','$stock_operation_qty','$stock_operation_amount','$now_date','$status_temp','$empcode')";

    if($conn->query($sql) === FALSE) 
          { 
            throw new Exception("Code 001 : ".mysqli_error($conn));        
          }

     // $sql1 = $conn->query($sql); cooment by p..
     

  $quantity = $avl_qty - $stock_operation_qty;
       $conn->query("COMMIT");
                 echo " <script>
                      window.location.href='add_theft_stock.php';
                      </script>";
                $file_name= basename(__FILE__);
                $type=1;
                $val="Product Numbur:"."Product Numbur-".$productno.",Product Name-".$productname.",Theft Quantity-".$stock_operation_qty;
                $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`error`,`type`) VALUES ('$file_name','$username','$val','$type')";
                  if ($conn->query($sql)===FALSE) {
                  echo "Theft No Add Some Technical Issue";
                            
                                  }  
                     exit();
     /*$sql6 = "UPDATE product SET quantity ='$quantity',scrap_stock = '$stock_operation_qty' WHERE username = '$office' and productno='$productno'";
              $result6 = $conn->query($sql6);
              echo $sql6;*/

 }
    
catch (Exception $e) {
  //role back to using
    $conn->query("ROLLBACK");
    $content = htmlspecialchars($e->getMessage());
    $content = htmlentities($conn->real_escape_string($content));

            echo "<SCRIPT>
               alert('Error Connect Technical Team');
               window.location.href='add_theft_stock.php';
            </SCRIPT>";

  

    $file_name = basename(__FILE__);        
    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name ','$username','$content')";
    if ($conn->query($sql) === FALSE) { 
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>    