<?php
include ("connect.php");
session_start();
$empcode = $_SESSION['empcode'];

error_reporting(0);
$office = $conn->real_escape_string(htmlspecialchars($_POST['from_office']));
$to_branch = $conn->real_escape_string(htmlspecialchars($_POST['to_branch']));
$stock_operation_date = $conn->real_escape_string(htmlspecialchars($_POST['theft_out_date']));
$productno = $conn->real_escape_string(htmlspecialchars($_POST['productno']));

$productname = $conn->real_escape_string(htmlspecialchars($_POST['productname']));
$producttype = $conn->real_escape_string(htmlspecialchars($_POST['producttype']));
$company = $conn->real_escape_string(htmlspecialchars($_POST['company']));
$product_group = $conn->real_escape_string(htmlspecialchars($_POST['product_group']));

$rate = $conn->real_escape_string(htmlspecialchars($_POST['rate']));
$avl_qty = $conn->real_escape_string(htmlspecialchars($_POST['avl_qty']));
$stock_operation_qty = $conn->real_escape_string(htmlspecialchars($_POST['stock_operation_qty']));
$stock_operation_amount = $conn->real_escape_string(htmlspecialchars($_POST['stock_operation_amount']));
$unit = $conn->real_escape_string(htmlspecialchars($_POST['unit']));
$sub = $conn->real_escape_string(htmlspecialchars($_POST['sub']));
   try{

          $conn->query("START TRANSACTION"); 

 // $office= $_POST['from_office']; 
 // $to_branch = $_POST['to_branch'];
 //  $stock_operation_date= $_POST['theft_out_date']; 
 //  $productno = $_POST['productno'];
 //  $productname = $_POST['productname'];
 //  $producttype = $_POST['producttype'];
 //  $company = $_POST['company'];
 //  $product_group = $_POST['product_group'];
 //  $rate = $_POST['rate'];
 //  $avl_qty = $_POST['avl_qty'];
 //  $stock_operation_qty = $_POST['stock_operation_qty'];
 //  $stock_operation_amount = $_POST['stock_operation_amount'];

 //  $unit = $_POST['unit'];
 //  $sub = $_POST['sub'];

  $now_date = date('Y-m-d');
  $status_temp="transfer_temp";

  if (empty($stock_operation_amount)) {
     echo "
        <script>
        alert('Fill All Detail About Product');
         window.location.href='add_stock_transfer.php';
         </script>";
    exit();
  }

    $sql = "insert into outward_stock(office,to_branch,stock_operation_date,productno,productname,producttype, company,product_group,rate,avl_qty,stock_operation_qty,stock_operation_amount,now_date,status,employee,unit,sub) values ('$office','$to_branch','$stock_operation_date','$productno', '$productname','$producttype', '$company','$product_group','$rate','$avl_qty','$stock_operation_qty','$stock_operation_amount','$now_date','$status_temp','$empcode','$unit','$sub')";

            if($conn->query($sql) === FALSE) 
                { 
                  throw new Exception("Code 001 : ".mysqli_error($conn));        
                }
     // $sql1 = $conn->query($sql);
     

 $quantity = $avl_qty - $stock_operation_qty;
 $conn->query("COMMIT");
                 echo " <script>
                      window.location.href='add_stock_transfer.php';
                      </script>";
                $file_name= basename(__FILE__);
                $type=1;
                $val="Stock Transfer(Outward):"."To Branch-".$to_branch.",Product Number-".$productno.",Product Name-".$productname.",Transfer quantity-".$stock_operation_qty;
                              
                $sql="INSERT INTO `allerror`(`file_name`,`user_name`,`error`,`type`) VALUES ('$file_name','$username','$val','$type')";
                  if ($conn->query($sql)===FALSE) {
                  echo "Party No Add Some Technical Issue";
                      }
                    exit();
     /*$sql6 = "UPDATE product SET quantity ='$quantity',scrap_stock = '$stock_operation_qty' WHERE username = '$office' and productno='$productno'";
              $result6 = $conn->query($sql6);
              echo $sql6;*/
}
    catch (Exception $e) {
                  //role back to using
                    $conn->query("ROLLBACK");
                    $content = htmlspecialchars($e->getMessage());
                    $content = htmlentities($conn->real_escape_string($content));

                            // echo "<SCRIPT>
                            //    alert('Error: Party Not Inserted Successfully...');
                            //    window.location.href='party.php';
                            // </SCRIPT>";

                           echo  " <script>
                                      window.location.href='add_stock_transfer.php';
                                      </script>";

                    $file_name = basename(__FILE__);        
                    $sql = "INSERT INTO `allerror`(`file_name`, `user_name`, `error`) VALUES ('$file_name ','$username','$content')";
                    if ($conn->query($sql) === FALSE) { 
                      echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                }
$conn->close();

?>    