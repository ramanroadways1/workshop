
<!DOCTYPE html>
<html>
<head>
  
   <?php 
    include("header.php");
    include("break_downasidemain.php");
    include("connect.php");

?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
   <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
<style type="text/css">

.ui-autocomplete { z-index:2147483647; }
</style>

<style type="text/css">
  @media print {
  .noPrint{
    display:none;
  }
}
h1{
  color:#f6f6;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="final_submitbreakdown.php" autocomplete="off" enctype="multipart/form-data">
  <div class="content-wrapper">
  
    <section class="content">
      
    <div class="box box-info">
      <div class="box-header with-border">
  <!--       <h3 class="box-title">
        Final Submit</h3> -->
        
      </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
               <?php $username =  $_SESSION['username'];  ?>
             
          <input type="hidden" name="username" value="<?php echo $username ?>">
            <input type="hidden" name="employee" value="<?php echo $_SESSION['employee']; ?>">
            <input type="hidden" name="empcode" value="<?php echo $_SESSION['empcode']; ?>">
              <div class="table-responsive">  
                              <table class="table table-bordered" style="font-family: verdana; font-size: 12px;" >  
                  <thead>  
                       <tr>  
                          <th style="display: none;">Id</th>
                          <th style="display: none;">Purchase order</th>
                          <th>Truck Number</th>
                          <th>Complaint Date</th>
                          <th style="display: none;">Party name</th>
                          <th style="display: none;">Party code</th>
                          <th>Break Down Place</th>
                          <th>Problem</th>
                          <th>Work Group</th>
                          <th>Call Reports</th>
                          <th>Driver Mobail No.</th>
                          <th>KM</th>
                          <th>Remarks</th>
                       </tr>  
                  </thead> 

                  <?php  
                  include("connect.php");
                  $truck_no=$_POST['truck_no'];
                  $query = "SELECT * from breakdownfinal where truck_no='$truck_no'"; 
                  if($conn->query($query) === FALSE) 
                   
                    { 
                      throw new Exception("Code 001 : ".mysqli_error($conn));
                    }

                  $result = mysqli_query($conn,$query);
                  if ($result->num_rows > 0){
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $truck_no=$row['truck_no'];
                    $comp_date = $row['comp_date'];
                    $place_brakdown = $row['place_brakdown'];
                    $approve_by = $row['approve_by'];
                    $employee = $row['employee'];
                    $problem = $row['problem'];
                    $workgroup = $row['workgroup'];
                    $status=$row['status'];
                    $report_by=$row['report_by'];
                    $update_info = $row['update_info'];
                    $emiail_mobail = $row['emiail_mobail'];
                    $mobile_no = $row['mobile_no'];
                    $km = $row['km'];
                    $remark = $row['remark'];
                    $comp_time=$row['comp_time'];
                    
                  ?>

                  <tr>
                    <td style="display: none;"><?php echo $id?>
                      <input type="hidden" name="id[]" value="<?php echo $id; ?>">
                    </td>
                    <td ><?php echo $truck_no?>
                      <input  type="hidden" readonly="readonly" name="truck_no" value="<?php echo $truck_no; ?>" >
                    </td>
                    <td ><?php echo $comp_date?>
                      <input  type="hidden" readonly="readonly" name="comp_date" value="<?php echo $comp_date; ?>">
                    </td>
                     <td ><?php echo $place_brakdown?>
                      <input  type="hidden" readonly="readonly" name="place_brakdown" value="<?php echo $place_brakdown; ?>" >
                    </td>
                    
                    <td><?php echo $problem?>
                      <input  type="hidden" readonly="readonly" name="problem" value="<?php echo $problem; ?>" >
                    </td>

                    <td style="display: none;"><?php echo $approve_by?>
                      <input  type="hidden" readonly="readonly" name="approve_by" value="<?php echo $approve_by; ?>" >
                    </td>
                <td style="display: none;"><?php echo $comp_time?>
                      <input  type="hidden" readonly="readonly" name="comp_time" value="<?php echo $comp_time; ?>" >
                    </td>

                <td style="display: none;"><?php echo $status?>
                      <input  type="hidden" readonly="readonly" name="status" value="<?php echo $status; ?>" >
                    </td>

                     <td><?php echo $workgroup?>
                      <input  type="hidden" readonly="readonly" name="workgroup" value="<?php echo $workgroup; ?>" id="workgroup<?php echo $workgroup; ?>">
                    </td>

                    <td ><?php echo $report_by?>
                      <input  type="hidden" readonly="readonly" name="report_by" value="<?php echo $report_by; ?>" id="report_by<?php echo $report_by; ?>" >
                    </td>
                    
                    <td style="display: none;" ><?php echo $update_info?>
                      <input  type="hidden" readonly="readonly" name="update_info" value="<?php echo $update_info; ?>" id="update_info<?php echo $update_info; ?>">
                    </td>

                    <td style="display: none;" ><?php echo $emiail_mobail?>
                      <input  type="hidden" readonly="readonly" name="emiail_mobail" value="<?php echo $emiail_mobail; ?>" id="emiail_mobail<?php echo $emiail_mobail; ?>">
                    </td>
                     
                     <td><?php echo $mobile_no?>
                        <input type="hidden" readonly="readonly" id="total<?php echo $mobile_no; ?>" name="mobile_no" value="<?php echo $mobile_no; ?>">
                    </td>
                   <td><?php echo $km?>
                        <input type="hidden" readonly="readonly" id="total<?php echo $km; ?>" name="km" value="<?php echo $km; ?>">
                    </td>
                    <td><?php echo $remark?>
                        <input type="hidden" readonly="readonly" id="total<?php echo $remark; ?>" name="remark" value="<?php echo $remark; ?>">
                    </td>
                  </tr>
                  <?php  

                  }
                }
                  ?> 
                
                </table>
                   
                </div> 
                   <br><br> <br><br> <br><br>
                  <input type="hidden" name="date_purchase_key_id" value="<?php echo $date_purchase_key_id; ?>">
              
                <center>
                 <!--  <input type="text" name="challan_no" id="challan_no">
                  <input type="text" name="invoice_no" id="invoice_no"> -->
                   <div class="col-sm-offset-2 col-sm-8">
                     <button type="button" onclick="window.print();" id="submit_disable"  class="btn btn-primary">Print</button>
                   </div>
                 </center><br><br><br>  
              </div>  
            </div>  
          </div>
          <script type="text/javascript">
            function disableMe(){
             document.getElementById("submit_disable").disabled = true;
              /* $('submit_disable').css('disabled','none');*/
            }
          </script>
              <!-- /.table-responsive -->
        </div>
            <!-- /.box-body -->
        <!-- <div class="box-footer clearfix">
          <a href="product.php" class="btn btn-sm btn-info btn-flat pull-left">Place New Product</a>
        </div> -->
        <!-- /.box-footer --> 
      </div>
          <!-- /.box -->
      </section>
    </div>
  </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script>
 
<script>
    function diff(i){
      var rate_value1=document.getElementById('rate_master'+i).value;
      var rate_value2= document.getElementById('rate'+i).value;
      var result= parseInt(rate_value1)-parseInt(rate_value2);
      /*  document.getElementById('rate_value1').value=result;*/
      alert(result);
    }

    function gettotal(i){
       var received_qty = Number($('#rec_qty'+i).val());  
       var quantity = Number($('#quantity'+i).val());  
     /* var received_qty = document.getElementById("rec_qty"+i).value;
        var quantity = document.getElementById("quantity"+i).value;*/
          if(received_qty>quantity){
            $('#new_total_amount'+i).val('');
           
        }
        else{
           var rate = document.getElementById("rate"+i).value;
            var new_amount = received_qty*rate;
            var gst= document.getElementById("gst"+i).value;
            var gst_amount=new_amount*gst/100;
            $('#new_total_amount'+i).val((new_amount+gst_amount).toFixed(2));
            }
     }

    function getgrn(){
      var date = new Date();
      document.getElementById("grnno").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
  
    function challan_disable() {
      document.getElementById("myfile1").disabled = true;
       document.getElementById("invoice_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
       /* $("#generatenumber").html(randomnumber );
        $("#invoice_no").val(randomnumber);*/
      $(document).ready(function() {
          $ ('#myfile2').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile2').val('');
              }
          });
      });
    }

    function invoice_disable() {
      document.getElementById("myfile2").disabled = true;
       
          document.getElementById("challan_no").readOnly  = false;
      var randomnumber = Math.random().toString().slice(-8);
        /*$("#generatenumber").html(randomnumber );
        $("#challan_no").val(randomnumber);*/
        $(document).ready(function() {
          $ ('#myfile1').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile1').val('');
              }
          });
      });
    }
</script>



<script>
function addProduct(product_table) {

    var rowCount = document.getElementById("product_table").rows.length;
    var row = document.getElementById("product_table").insertRow(rowCount);
    /*alert(rowCount);*/
    /*var cell1 = row.insertCell(0);
    cell1.innerHTML =rowCount; */
    
    var cell2 = row.insertCell(0);
    cell2.innerHTML ="<input type='text' style='width:100%' name='product_name' id='product_name"+rowCount+"' required/>";

    var cell4 = row.insertCell(1);
    cell4.innerHTML = "<input type='number' step='.01' min='0.1' style='width:100%' name='rate' id='rate_product"+rowCount+"' required/> ";
    
    var cell5 = row.insertCell(2);
    cell5.innerHTML = "<input type='number' name='quantity[]' min='1' id='quantity_issue"+rowCount+"' style='width:100%;' required autocomplete='off'/>";
    
    var cell6 = row.insertCell(3);
    cell6.innerHTML =  "<input type='number' step='.01' style='width:100%' name='amount[]' id='amount_issue"+rowCount+"'  required />";

    var cell7 = row.insertCell(4);
    cell7.innerHTML =  "<button type='button'  id='remove"+rowCount+"'><i class='fa fa-minus'></i></button>";

    $("#remove"+rowCount).click(function(){
      /*alert("#remove"+rowCount);*/
     $(this).parents("tr").remove();
    });



    $("#rate_product"+rowCount).blur(function(){
      var rate=$("#rate_product"+rowCount).val();
     
      var quantity=$("#quantity_issue"+rowCount).val();
      var amt = rate*quantity;
       /*alert(amt);*/
       $("#amount_issue"+rowCount).val(amt);
       if(rate< 0.1){
       // alert('You need to enter minimum rate is 0.1!');
        $("#rate_product"+rowCount).val("");
       }
     
    });
    $("#quantity_issue"+rowCount).keyup(function(){
      var rate=$("#rate_product"+rowCount).val();
      var quantity=$("#quantity_issue"+rowCount).val();
      var amt = rate*quantity;
       /*alert(amt);*/
       $("#amount_issue"+rowCount).val(amt);
      if(quantity< 1){
       // alert('You need to enter minimum quantity is 1!');
        $("#quantity_issue"+rowCount).val("");
       }
    });


   
    }
</script>
 
            

             <script>
                  function SearchBy(elem)
                  {
                    /*if($('#party_set').val()=='0' || $('#party_set').val()=='')
                    {
                      alert('Please select party first !');
                      window.location.href='generate_po.php';
                      $('#search_product').val('');
                    }*/
                   /* else
                    {*/
                    $('.pname1').val(''); 
                    $('.pno1').val(''); 
                    $('.office').val(''); 
                    
                      $('.pno1').attr('id','internal_truck');  
                      $('.pname1').attr('id','external_truck');   
                      $('.office').attr('id','remark');   

                       if(elem=='office')
                    {

                      $('.pname1').attr('onblur',""); 
                       $('.office_type').css('display','block'); 
                      $('.pname1').css('display','none'); 
                      $('.pno1').css('display','none');  
                      $('.pno1').attr('id','');  
                      $('.pname1').attr('id',''); 
                       $('#external_jobcard_no').val('');
                    }       
                      
                   else if(elem=='internal')
                    {

                      $('.pname1').attr('onblur',""); 
                      $('.pname1').css('display','none'); 
                       $('.office_type').css('display','none'); 
                      $('.pno1').css('display','block');  
                      $('.pno1').attr('onblur',"get_data1(this.value)");  
                      $('.pno1').attr('id','internal_truck');  
                      $('.pname1').attr('id',''); 


                       $('#external_jobcard_no').val('');
                    }
                    else if(elem=='external') 
                    {
                      $('.pno1').attr('onblur',""); 
                      $('.pname1').attr('onblur',"get_data2(this.value)");  
                      $('.pname1').css('display','block'); 
                      $('.office_type').css('display','none');
                        $('.pno1').css('display','none'); 
                      $('.pno1').attr('id',''); 
                      $('.pname1').attr('id','external_truck');  

                       $('#internal_jobcard_no').val('');
                    }
                    else
                    {
                      $('.pname1').attr('onblur',''); 
                      // $('.pname1').attr('readonly',true);
                      // $('.pname1').attr('readonly',true);
                      $('.pname1').attr('',true); 
                      $('.pno1').attr('',true); 
                       $('.office').attr('readonly',true); 
                      $('.pno1').attr('onblur',''); 
                      $('.pno1').attr('id','internal_truck');  
                      $('.pname1').attr('id','external_truck');  
                       $('.office').attr('id','remark');  
                    }
                   /* }*/
                  }
            </script>