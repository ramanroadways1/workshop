<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
 <script>
  function getinvoiceno(){
      var date = new Date();
      document.getElementById("invoiceno").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" class="form-inline" autocomplete="off" >
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Show Challan & Invoice</h1>
      <div class="form-group">
          <label for="invoiceno">Invoice Number</label>
          <input type="text" class="form-control" id="invoiceno" onclick="getinvoiceno();" name="invoiceno" placeholder="Invoice Number" />
      </div>
    </section>
    
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Approve for Payment</h3>
        </div>
        <!-- /.box-header -->
        <table class="table table-hover" border="4";>

              <tbody>
                <tr class="table-active">
                  <th scope="row"><center>ID</center></th>
                  <td><center>Party Name</center></td>
                  <td><center>Purchase Order</center></td>
                  <td><center>Product No.</center></td>
                  <td><center>Challan Files</center></td>
                  <td><center>Invoice File</center></td>
                  <td><center>Total Amount</center></td>
                  <!-- <td><center>Invoice Number</center></td> -->
                  <td><center>Done</center></td>
                </tr>
              </tbody>

              <?php

            include 'connect.php';
           
            $show="SELECT key_challan.id as id, party_name, invoice_file, GROUP_CONCAT(purchase_order) as purchase_order, GROUP_CONCAT(productno) as productno, sum(total_amount) as total_amount, GROUP_CONCAT(challan_file)as challan_file FROM key_challan, key_invoice WHERE key_challan.value= key_invoice.value AND length(invoice_file) > 10 GROUP BY invoice_file";

            $result = $conn->query($show);

            if ($result->num_rows > 0) {
                // output data of each row
                  $id_customer=0;
                while($row = $result->fetch_assoc()) {
                    $id = $row["id"];
                    $party_name = $row["party_name"];
                    $total_amount = $row["total_amount"];
                    $invoice_file = $row["invoice_file"];
                    $purchase_order = $row["purchase_order"];
                    $productno = $row["productno"];
                    $challan_file = $row["challan_file"];

                    $purchase_order1=explode(",",$purchase_order);
                    $count1=count($purchase_order1);
                    $productno1=explode(",", $productno);
                    $count2=count($productno1);
                    $challan_file1=explode(",",$challan_file);
                    $count3=count($challan_file1);
                    
                   ?>
            <tr> 
               
               <td><?php echo $id; ?>
                  <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                </td>

                <td><?php echo $party_name?>
                  <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                </td>

                <td>
                  <?php
                   for($i=0; $i<$count1; $i++){
                    echo $purchase_order1[$i]."<br>";
                   }

                  ?>
                  <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                </td>
                
                <td> <?php
                   for($i=0; $i<$count2; $i++){
                    echo $productno1[$i]."<br>";
                   }

                  ?>
                  <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                </td>
                
                <td>
                 
                  <?php
                   for($i=0; $i<$count3; $i++){
                    ?>
                     <a href="<?php echo $challan_file1[$i]; ?>" target="_blank"><?php echo $challan_file1[$i]; ?></a>
                     <input type="hidden" name="challan_file[]" value='<?php echo $challan_file; ?>'>
                     <?php
                   }

                  ?>
                </td>

                <td>
                  <img src="<?php echo $invoice_file;?>" style="width: 100px;height: 80px;">
                  <input type="button" value="open" onclick="window.open('<?php echo $invoice_file;?>')">
                  <input type="hidden" name="invoice_file[]" value="<?php echo $invoice_file;?>">
                </td>

                <td><?php echo $total_amount;?>
                  <input type="hidden" name="total_amount[]" value='<?php echo $total_amount;?>'>
                </td>

                <!-- <td>
                    <input type="text" class="form-control" id="invoiceno" onclick="getinvoiceno();" name="invoiceno[]" placeholder="Invoice Number" />
                </td>
 -->
                <td>
                   <input type="radio" name="id_customer[]" id="<?php echo $id_customer; ?>" value='<?php echo $id_customer; ?>'>
                </td>
 
              </tr>
              <?php
              $id_customer++;
              }
          } else {
              echo "0 results";
          }
          ?>
        </table>
          <div class="col-sm-offset-2 col-sm-8">
           <center><button type="submit" name="submit" color="Primary" formaction="approve_for_payment.php" class="btn btn-primary">Submit</button></center>
          </div>
      <br><br><br>
      </div>
    </section>
    
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2019<a href="https://adminlte.io">RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

