<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
 <script>
  function myFunction1(id_customer) {
      var v1 = document.getElementById("pn"+id_customer).value;
      /*alert(v1);*/
       $('#party_name').val(v1); 

     var v2 = document.getElementById("po"+id_customer).value;
     $('#purchase_order').val(v2); 

     var v4 = document.getElementById("p"+id_customer).value;
     $('#productno').val(v4);

     var v5 = document.getElementById("t"+id_customer).value;
     $('#total_amount').val(v5); 

     var date = new Date();
     document.getElementById("key").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes();

   } 
 </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" class="form-inline">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Payment</h1>
    </section>
    
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Payment</h3>
        </div>
        <!-- /.box-header -->
        <table class="table table-hover" border="4";>

              <tbody>
                <tr class="table-active">
                  <th scope="row"><center>ID</center></th>
                    <td><center>Purchase Order</center></td>
                    <td><center>Invoice Number</center></td>
                    <td><center>Party Name</center></td>
                    <td><center>Total Amount</center></td>
                    <td><center>Payment</center></td>
                    <td><center>Done</center></td>
                </tr>
              </tbody>
              <?php
                    include 'connect.php';
                    $show = "SELECT id,purchase_order,invoiceno,party_name,total_amount FROM approve_for_payment ";
                    $result = $conn->query($show);
                    if ($result->num_rows > 0) {
                          $id_customer = 0;
                        while($row = $result->fetch_assoc()) {
                            $id = $row['id'];
                            $purchase_order = $row['purchase_order'];
                            $invoiceno = $row['invoiceno'];
                            $party_name = $row['party_name'];
                            $total_amount = $row['total_amount'];
                            /*$invoice_file = $row['invoice_file'];*/
                   ?>
                      <tr> 
                    
                        <td><?php echo $id; ?>
                          <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                        </td>
                        <td> 
                          <?php echo $purchase_order; ?>
                          <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                        </td>

                        <td> 
                          <?php echo $invoiceno; ?>
                          <input type="hidden" name="invoiceno[]" value='<?php echo $invoiceno; ?>'>
                        </td>

                        <td><?php echo $party_name; ?>
                          <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                        </td>
                         
                         <td> 
                          <?php echo $total_amount; ?>
                          <input type="hidden" name="total_amount[]" value='<?php echo $total_amount; ?>'>
                        </td>

                        <td>
                            <input type="number"  class="form-control" id="payment" name="payment[]" placeholder="Pay Amount"/>
                        </td>

                        <td>
                           <input type="radio" name="id_customer[]" id="<?php echo $id_customer; ?>" value='<?php echo $id_customer; ?>'>
                        </td>
                    
                      </tr>
                <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>

        </table>
          <div class="col-sm-offset-2 col-sm-8">
           <center><button type="submit" name="submit" color="Primary" formaction="pay_invoice.php" class="btn btn-primary">Submit</button></center>
          </div>
      <br><br><br>
      </div>
    </section>
    
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

