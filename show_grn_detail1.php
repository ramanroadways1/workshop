
<!DOCTYPE html>
<html>
<head>
  
   <?php 
    include("header.php");

    include("aside_main.php");
 
  include("connect.php");
?>
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  --> 
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    
<style type="text/css">

.ui-autocomplete { z-index:2147483647; }
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form method="post" action="insert_grn.php" autocomplete="off" enctype="multipart/form-data">
  <div class="content-wrapper">
  
    <section class="content">
      
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
        Create GRN</h3>
        
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
             
          
              <div class="table-responsive">  
                <table class="table table-bordered">  
                  <thead>  
                       <tr>  
                          <th>Id</th>
                          <th>Purchase order</th>
                          <th>Product no.</th>
                          <th>Product Name</th>
                          <th>Party name</th>
                          <th>Party code</th>
                          <th>Quantity</th>
                          <th>Rate/Unit</th>
                          <th>Amount</th>
                          <th>GST</th>
                          <th>GST Amount</th>
                          <th>Total Amount</th>
                          
                          <th>Received Quantity</th>
                          <th>New Total</th>
                         
                          <th>Select</th>
                          <!-- <th>key1</th> -->
                       </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $valueToSearch=$_POST['purchase_order'];
                  $query = "SELECT insert_po.id,purchase_order,party_name,party_code,quantity,amount, gst,total, rate,rate_master, gst_amount, productno,productname,date1,date_purchase_key.key1 FROM insert_po INNER JOIN  date_purchase_key ON insert_po.key1=date_purchase_key.key1 WHERE purchase_order = '$valueToSearch'"; 
                  $result = mysqli_query($conn,$query);
                  $l_u = 1;
                  $id_customer = 0;
                  while($row = mysqli_fetch_array($result))
                  {             
                    $id = $row['id'];
                    $key1=$row['key1'];
                    $purchase_order=$row['purchase_order'];
                    $productno = $row['productno'];
                    $productname = $row['productname'];
                    $party_name = $row['party_name'];
                    $party_code = $row['party_code'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $amount=$row['amount'];
                    $gst=$row['gst'];
                    $gst_amount = $row['gst_amount'];
                    $total_amount = $row['total'];
                    $date1 = $row['date1'];
                  ?>
                  <tr>
                    <td><?php echo $id?>
                      <input type="hidden" name="id[]" value="<?php echo $id; ?>">
                    </td>
                    <td ><?php echo $purchase_order?>
                      <input  type="hidden" readonly="readonly" name="purchase_order[]" value="<?php echo $purchase_order; ?>" >
                    </td>
                    <td ><?php echo $productno?>
                      <input  type="hidden" readonly="readonly" name="productno[]" value="<?php echo $productno; ?>">
                    </td>
                     <td ><?php echo $productname?>
                      <input  type="hidden" readonly="readonly" name="productname[]" value="<?php echo $productname; ?>" >
                    </td>
                    
                    <td ><?php echo $party_name?>
                      <input  type="hidden" readonly="readonly" name="party_name[]" value="<?php echo $party_name; ?>" >
                    </td>
                    <td ><?php echo $party_code?>
                      <input  type="hidden" readonly="readonly" name="party_code[]" value="<?php echo $party_code; ?>" >
                    </td>
                    <td><?php echo $quantity?>
                     <input type="hidden" readonly="readonly" id="quantity<?php echo $l_u; ?>"  oninput="getrate(<?php echo $l_u; ?>);" name="quantity[]" value="<?php echo $quantity; ?>" >
                   </td>

                     <td ><?php echo $rate?>
                      <input  type="hidden" readonly="readonly" name="rate[]" value="<?php echo $rate; ?>" id="rate<?php echo $l_u; ?>" oninput="gettotal(<?php echo $l_u; ?>);">
                    </td>

                    <td ><?php echo $amount?>
                      <input  type="hidden" readonly="readonly" name="amount[]" value="<?php echo $amount; ?>" id="amount<?php echo $l_u; ?>" oninput="gettotal(<?php echo $l_u; ?>);">
                    </td>
                    
                    <td ><?php echo $gst?>
                      <input  type="hidden" readonly="readonly" name="gst[]" value="<?php echo $gst; ?>" id="gst<?php echo $l_u; ?>" oninput="gettotal(<?php echo $l_u; ?>);">
                    </td>

                    <td ><?php echo $gst_amount?>
                      <input  type="hidden" readonly="readonly" name="gst_amount[]" value="<?php echo $gst_amount; ?>" id="gst_amount<?php echo $l_u; ?>" oninput="gettotal(<?php echo $l_u; ?>);">
                    </td>
                     
                     <td><?php echo $total_amount?>
                        <input type="hidden" readonly="readonly" id="total<?php echo $l_u; ?>" name="total_amount[]" value="<?php echo $total_amount; ?>">
                    </td>
                    <td>
                        <input type="number" class="form-control" style="width: 130px;" id="rec_qty<?php echo $l_u; ?>" name="rec_qty[]" oninput="gettotal(<?php echo $l_u; ?>);CheckQuantity(<?php echo $quantity; ?>);" placeholder="Received Quantity"/>
                    </td>
                    <script>
                      function CheckQuantity(i)
                       {
                         var myVal=$(event.target).val();
                         
                         if(myVal>i)
                         {
                          alert('Max Qty allowed is '+i);
                          $(event.target).val('');
                         }
                       }
                   </script>
                     <td>
                      <input type="number" class="form-control" readonly style="width: 130px;" id="new_total_amount<?php echo $l_u; ?>" name="new_total_amount[]" placeholder="New Amount" >
                    </td>
                    <td>
                      <input type="checkbox" name="id_customer[]" value='<?php echo $id_customer; ?>'>
                    </td>

                    <td>
                      <?php 
                          include ("connect.php");
                            $get=mysqli_query($conn,"SELECT quantity FROM product where productno='$productno'");
                               while($row = mysqli_fetch_assoc($get))
                              {
                           ?>
                            <input type="hidden" readonly="readonly"  name="quantity_product[]" value="<?php echo  ($row['quantity']) ?>">
                            <?php 
                            }
                          ?>

                    </td>
                    <td style="display:none;">
                      <input type="hidden" name="key1[]" value="<?php echo $key1; ?>">
                    </td>
                  </tr>
                  <?php  
                    $id_customer++;
                    $l_u++;
                  }
                  ?> 
                </table>
              </div>

                <div>
                  <a class="btn btn-info" style="float: right;" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
                    aria-controls="multiCollapseExample1">Direct Product Issue</a>

                  
                </div>


                <div class="row">
                <div class="col">
                  <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body">
                      <br>
                      <div class="container">
                      <table style="width: 70%;" id="employee_data" class="table table-bordered">
                  <thead>  
                       <tr>  
                          <th style="width: 15%;">Direct Issue</th>
                          <th style="width: 15%;">Internal Truck</th>
                           <th style="width: 15%;">External Truck</th>
                            
                          
                       </tr>  
                  </thead>  
                  <tr>
                     <td>
                
                 <select onchange="SearchBy(this.value)" class="form-control"  id="search_product" name="search_truck">
                    <option value="">---Select---</option> 
                    <option value="internal">Internal Issue</option>
                    <option value="external">External Issue</option>
                    </select>
                  </td>
              
                <td>
                <input type="text" class="form-control pno1"  id="internal_truck" name="internal_truck" placeholder="Internal Truck"  />
              </td>

               <td>
                <input type="text" class="form-control pname1"  style="width: 250px;" name="external_truck" id="external_truck"  placeholder="External Truck" />
              </td><br><br>

                   </tr>
                   <tr>

                    <td>
                      <label>Internal Jobcard No</label>
                 <input class="form-control internal_tno" style="width: 270px;" name="internal_jobcard_no" id="internal_jobcard_no"  placeholder="Internal Jobcard no "  readonly>
              </td>

              <td>
                  <label>External Jobcard No</label>
                <input type="text" class="form-control external_tno" style="width: 270px;" name="external_jobcard_no" id="external_jobcard_no"  placeholder="External jobcard no" readonly/>
              </td>
                   </tr>

                </table>
                 <div id="rate_master222"></div>

               <div id="rate_master221"></div>
                </div>

                <div class="container" style="width: 100%">
                <div class="table-wrapper">
              <div class="table-title">
                <div class="row">
                  <div class="col-sm-10"><h3>Market<b> Product</b></h3></div>
                  <div class="col-sm-2">
                    <div align="right">
                      <button type="button" class="btn btn-info add-new" onclick="addProduct('product_table')"> <i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <table id="product_table" class="table table-bordered">
                <thead>
                  <tr>
                      <th>Product Name</th>
                    
                      <th>Rate/Unit</th>
                      <th>Quantity</th>
                      <th>Amount</th>
                      <th>Remove</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>


                    </div>
                  </div>

                </div>

              </div>



                <br>
                <div class="row-2">
                   <div class="col-md-6">
                      <b>Challan Upload</b>
                      <input type="file" id="myfile1" name="myfile1[]" onclick="invoice_disable()" multiple="multiple" required>
                   </div>
                   <div class="col-md-6">
                       <b>Invoice Upload</b>
                       <input type="file" id="myfile2" name="myfile2[]" onclick="challan_disable()" multiple="multiple" required>          
                   </div>
                </div><br><br><br><br>
                <center>
                  <input type="hidden" name="challan_no" id="challan_no">
                  <input type="hidden" name="invoice_no" id="invoice_no">
                   <div class="col-sm-offset-2 col-sm-8">
                     <button type="submit" href= "" class="btn btn-success">Submit</button>
                   </div>
                 </center><br><br><br>  
             
            </div>  
          </div>
              <!-- /.table-responsive -->
        </div>
            
      </div>
          <!-- /.box -->
      </section>
    </div>
  </form>




  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2014-2016 <a>RRPL</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>

<script>
    function diff(i){
      var rate_value1=document.getElementById('rate_master'+i).value;
      var rate_value2= document.getElementById('rate'+i).value;
      var result= parseInt(rate_value1)-parseInt(rate_value2);
      /*  document.getElementById('rate_value1').value=result;*/
      alert(result);
    }

    function gettotal(i){
      var received_qty = document.getElementById("rec_qty"+i).value;
      var rate = document.getElementById("rate"+i).value;
      var new_amount = received_qty*rate;
      var gst= document.getElementById("gst"+i).value;
      var gst_amount=new_amount*gst/100;
     
      if(!(received_qty==0)){
        document.getElementById("new_total_amount"+i).value=parseInt(new_amount)+parseInt(gst_amount);
      }
    }

    function getgrn(){
      var date = new Date();
      document.getElementById("grnno").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
  
    function challan_disable() {
      document.getElementById("myfile1").disabled = true;
      var randomnumber = Math.random().toString().slice(-8);
        $("#generatenumber").html(randomnumber );
        $("#invoice_no").val(randomnumber);
      $(document).ready(function() {
          $ ('#myfile2').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile2').val('');
              }
          });
      });
    }

    function invoice_disable() {
      document.getElementById("myfile2").disabled = true;
      var randomnumber = Math.random().toString().slice(-8);
        $("#generatenumber").html(randomnumber );
        $("#challan_no").val(randomnumber);
        $(document).ready(function() {
          $ ('#myfile1').bind('change', function() {
              var fileSize = this.files[0].size/1024/1024;
              if (fileSize > 2) { // 2M
                  alert('Your max file size exceeded');
                  $('#myfile1').val('');
              }
          });
      });
    }
</script>


<script>
function addProduct(product_table) {

    var rowCount = document.getElementById("product_table").rows.length;
    var row = document.getElementById("product_table").insertRow(rowCount);
    /*alert(rowCount);*/
    /*var cell1 = row.insertCell(0);
    cell1.innerHTML =rowCount; */
    
    var cell2 = row.insertCell(0);
    cell2.innerHTML ="<input type='text' style='width:100%' name='product_name[]' id='product_name"+rowCount+"' required/>";

    var cell4 = row.insertCell(1);
    cell4.innerHTML = "<input type='number' step='.01' min='0.1' style='width:100%' name='rate[]' id='rate_product"+rowCount+"' required/> ";
    
    var cell5 = row.insertCell(2);
    cell5.innerHTML = "<input type='number' name='quantity[]' min='1' id='quantity_issue"+rowCount+"' style='width:100%;' required autocomplete='off'/>";
    
    var cell6 = row.insertCell(3);
    cell6.innerHTML =  "<input type='number' step='.01' style='width:100%' name='amount[]' id='amount_issue"+rowCount+"'  required />";

    var cell7 = row.insertCell(4);
    cell7.innerHTML =  "<button type='button'  id='remove"+rowCount+"'><i class='fa fa-minus'></i></button>";

    $("#remove"+rowCount).click(function(){
      /*alert("#remove"+rowCount);*/
     $(this).parents("tr").remove();
    });



    $("#rate_product"+rowCount).blur(function(){
      var rate=$("#rate_product"+rowCount).val();
     
      var quantity=$("#quantity_issue"+rowCount).val();
      var amt = rate*quantity;
       /*alert(amt);*/
       $("#amount_issue"+rowCount).val(amt);
       if(rate< 0.1){
       // alert('You need to enter minimum rate is 0.1!');
        $("#rate_product"+rowCount).val("");
       }
     
    });
    $("#quantity_issue"+rowCount).keyup(function(){
      var rate=$("#rate_product"+rowCount).val();
      var quantity=$("#quantity_issue"+rowCount).val();
      var amt = rate*quantity;
       /*alert(amt);*/
       $("#amount_issue"+rowCount).val(amt);
      if(quantity< 1){
       // alert('You need to enter minimum quantity is 1!');
        $("#quantity_issue"+rowCount).val("");
       }
    });


   
    }
</script>
 <script type="text/javascript">
               function get_data1(val) {
                  $.ajax({
                    type: "POST",
                    url: "get_internal_truck.php",
                    data:'internal_truck='+val,
                    success: function(data){
              
                        $("#rate_master221").html(data);
                    }
                    });
                }
               
                $(function()
                  { 
                  $( "#internal_truck" ).autocomplete({
                  source: 'auto_complete_internal_truck.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                  alert('Truck No does not exists.');
                  } 
                  },
                  focus: function (event, ui) {return false; } }); });
                
            </script>


             <script type="text/javascript">
               function get_data2(val) {
                  $.ajax({
                    type: "POST",
                    url: "get_external_truck.php",
                    data:'external_truck='+val,
                    success: function(data){
                        $("#rate_master222").html(data);
                    }
                    });
                }

                $(function()
                { 
                  $("#external_truck").autocomplete({
                  source: 'auto_complete_external_truck.php',
                  change: function (event, ui) {
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                  alert('Truck No Does Not exists');
                  } 
                },
                focus: function (event, ui) { return false; } }); });
            </script>

             <script>
            function SearchBy(elem)
            {
              /*if($('#party_set').val()=='0' || $('#party_set').val()=='')
              {
                alert('Please select party first !');
                window.location.href='generate_po.php';
                $('#search_product').val('');
              }*/
             /* else
              {*/
              $('.pname1').val(''); 
              $('.pno1').val(''); 
              
                $('.pno1').attr('id','internal_truck');  
                $('.pname1').attr('id','external_truck');          
                
              if(elem=='internal')
              {

                $('.pname1').attr('onblur',""); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',false);  
                $('.pno1').attr('onblur',"get_data1(this.value)");  
                $('.pno1').attr('id','internal_truck');  
                $('.pname1').attr('id',''); 


                 $('#external_jobcard_no').val('');
              }
              else if(elem=='external') 
              {
                $('.pno1').attr('onblur',""); 
                $('.pname1').attr('onblur',"get_data2(this.value)");  
                $('.pname1').attr('readonly',false);  
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('id',''); 
                $('.pname1').attr('id','external_truck');  

                 $('#internal_jobcard_no').val('');
              }
              else
              {
                $('.pname1').attr('onblur',''); 
                $('.pname1').attr('readonly',true); 
                $('.pno1').attr('readonly',true); 
                $('.pno1').attr('onblur',''); 
                $('.pno1').attr('id','internal_truck');  
                $('.pname1').attr('id','external_truck');  
              }
             /* }*/
            }
            </script>