<?php

 $curl = curl_init();
 
 curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://rrpl.online/RRPL_API/vendor_payment_confirm.php',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        
      "crn_no":"RR-V21000001"
      

    }',
    CURLOPT_HTTPHEADER => array(
      'Content-Type: text/plain'
      ),
    ));

    $response = curl_exec($curl);
    
    curl_close($curl);
    // echo $response;
    // On Success response :

    // {
    //   "status":"SUCCESS",
    //   "response":"Payment Request Inserted Successfully.",
    //   "crn":"RR-V21000001"
    // }

    // Error Response :
    // {
    //   "status":"error",
    //   "errormsg":"Invalid Voucher date. Acceptable format is YYYY-MM-DD"
    // }