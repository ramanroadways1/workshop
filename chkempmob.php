  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="apple-touch-icon" type="image/png" href="https://cpwebassets.codepen.io/assets/favicon/apple-touch-icon-5ae1a0698dcc2402e9712f7d01ed509a57814f994c660df9f7a952f3060705ee.png" />
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico" />

<link rel="mask-icon" type="" href="https://cpwebassets.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg" color="#111" />
  <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'>
<style>
body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
select{
    /*width: 10%;
    padding: 15px;
    font-size: 16px;
    font-weight: 700; 
    font-family: 'Poppins', sans-serif;
    border: none;
    border-radius:8px;
    border: 2px solid #3f51b5;
    box-shadow: 0 15px 15px #efefef;
  */

  }.menu,
  .content{;

  }
.login-fg .form-container{color:#ccc;position:relative}
.login-fg .login{min-height:100vh;position:relative;display:-webkit-box;display:-moz-box;display:-ms-flexbox;display:-webkit-flex;display:flex;justify-content:center;align-items:center;padding:30px 15px}
.login-fg .login-section{max-width:370px;margin:0 auto;text-align:center;width:100%}
.login-fg .form-fg{width:100%;text-align:center}
.login-fg .form-container .form-group{margin-bottom:25px}
.login-fg .form-container .form-fg{float:left;width:100%;position:relative}
.login-fg .form-container .input-text{font-size:14px;outline:none;color:#616161;border-radius:3px;font-weight:500;border:1px solid transparent;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container img{margin-bottom:5px;height:40px}
.login-fg .form-container .form-fg input{float:left;width:100%;padding:11px 45px 11px 20px;border-radius:50px}
.login-fg .form-container .form-fg i{position:absolute;top:13px;right:20px;font-size:19px;color:#616161}
.login-fg .form-container label{font-weight:500;font-size:14px;margin-bottom:5px}
.login-fg .form-container .forgot{margin:0;line-height:45px;color:#535353;font-size:15px;float:right}
.login-fg .bg{background:rgba(0,0,0,0.04) repeat;background-size:cover;top:0;width:100%;bottom:0;opacity:1;z-index:999;min-height:100vh;position:relative;display:flex;justify-content:center;align-items:center;padding:30px}
.login-fg .info h1{font-size:60px;color:#fff;font-weight:700;margin-bottom:15px;text-transform:uppercase;text-shadow:2px 0px #000}
.login-fg .info{text-align:center}
.login-fg .info p{margin-bottom:0;color:#fff;line-height:28px;text-shadow:1px 1px #000}
.login-fg .form-container .btn-md{cursor:pointer;padding:10px 30px 9px;height:45px;letter-spacing:1px;font-size:14px;font-weight:400;font-family:'Open Sans',sans-serif;border-radius:50px;color:#d6d6d6}
.login-fg .form-container p{margin:0;color:#616161}
.login-fg .form-container p a{color:#616161}
.login-fg .form-container button:focus{outline:none;outline:0 auto -webkit-focus-ring-color}
.login-fg .form-container .btn-fg.focus,.btn-fg:focus{box-shadow:none}
.login-fg .form-container .btn-fg{background:#0f96f9;border:none;color:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container .btn-fg:hover{background:#108ae4}
.login-fg .logo a{font-weight:700;color:#333;font-size:39px;text-shadow:1px 0px #000}
.login-fg .form-container .checkbox{margin-bottom:25px;font-size:14px}
.login-fg .form-container .form-check{float:left;margin-bottom:0}
.login-fg .form-container .form-check a{color:#d6d6d6;float:right}
.login-fg .form-container .form-check-input{position:absolute;margin-left:0}
.login-fg .form-container .form-check label::before{content:"";display:inline-block;position:absolute;width:18px;height:18px;top:2px;margin-left:-25px;border:none;border-radius:3px;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container .form-check-label{padding-left:25px;margin-bottom:0;font-size:14px;color:#616161}
.login-fg .form-container .checkbox-fg input[type="checkbox"]:checked + label::before{color:#fff;background:#0f96f9}
.login-fg .form-container input[type=checkbox]:checked + label:before{font-weight:300;color:#f3f3f3;font-size:14px;content:"\2713";line-height:17px}
.login-fg .form-container input[type=checkbox],input[type=radio]{margin-top:4px}
.login-fg .form-container .checkbox a{font-size:14px;color:#616161;float:right;margin-left:3px}
.login-fg .login-section h3{font-size:20px;margin-bottom:40px;font-family:'Open Sans',sans-serif;font-weight:400;color:#505050}
.login-fg .login-section p{margin:25px 0 0;font-size:15px;color:#616161}
.login-fg .login-section p a{color:#616161}
.login-fg .login-section ul{list-style:none;padding:0;margin:0}
.login-fg .login-section .social li{display:inline-block;margin-bottom:5px}
.login-fg .login-section .social li a{font-size:12px;font-weight:600;width:120px;margin:2px 0 3px;height:35px;line-height:35px;border-radius:20px;display:inline-block;text-align:center;text-decoration:none;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .login-section .social li a i{height:35px;width:35px;line-height:35px;float:left;color:#fff;border-radius:20px}
.login-fg .login-section .social li a span{margin-right:7px}
.login-fg .login-section .or-login{float:left;width:100%;margin:20px 0 25px;text-align:center;position:relative}
.login-fg .login-section .or-login::before{position:absolute;left:0;top:10px;width:100%;height:1px;background:#d8dcdc;content:""}
.login-fg .login-section .or-login > span{width:auto;float:none;display:inline-block;background:#fff;padding:1px 20px;z-index:1;position:relative;font-family:Open Sans;font-size:13px;color:#616161;text-transform:capitalize}
.login-fg .facebook-i{background:#4867aa;color:#fff}
.login-fg .twitter-i{background:#3CF;color:#fff}
.login-fg .google-i{background:#db4437;color:#fff}
.login-fg .facebook{color:#4867aa}
.login-fg .twitter{color:#3CF}
.login-fg .google{color:#db4437}
@media (max-width: 1200px) {
.login-fg .info h1{font-size:45px}
}
@media (max-width: 992px) {
.login-fg .bg{display:none}
}
@media (max-width: 768px) {
.login-fg .login-section .social li a{width:100px}
  .login-fg .logo a{font-size:26px;}
}
</style>
<?php

    include("connect.php");
   // $demo=$_POST['empmobail'];
 $demo = mysqli_real_escape_string($conn, $_POST['empmobail']);

if(!empty($demo)) // 
{
    if(preg_match('/^\d{10}$/',$demo)) // phone number is valid
    {
      $demo = '' . $demo;

     
    }
    else // phone number is not valid
    { 

        echo "<SCRIPT>
               alert('Phone number invalid !');
               window.location.href='login.php';
          </SCRIPT>";

      exit();
    }
}
    
    $fetch1=mysqli_query($conn,"SELECT empmobail FROM emp_data WHERE empmobail='$demo'");
    if(mysqli_num_rows($fetch1)<=0)

    {
  
    echo "
    <script>
    alert('Mobail Numbur Not Registered');
    window.location.href='login.php';
    </script>";
    exit();

    }


  $query = mysqli_query($conn, "SELECT * FROM emp_data WHERE empmobail='$demo'");

  $row = mysqli_fetch_array($query);
      $empname=$row["empname"];
      $status=$row["status"];

  $branch = $row["branch"];  
  $branch2 = explode(',', $branch);
  $branch3 = implode(',', $branch2);
  ?>
  
  <div>

   <b><label style="text-transform: uppercase; margin-bottom: 5px;"> BRANCH </label></b>

  </div>
<div class="form-group form-fg">
  <?php
                $selected = $branch3;
               
                echo "<select  name=\"username\" id=\"username\" class=\"form-group form-fg\" style=\"border-radius: 25px;\">";
                foreach($branch2 as $option){
                    if($selected == $option) {
                        echo "<option selected='selected' value='$option'>$option</option>";
                    }
                    else {
                        echo "<option value='$option'>$option</option>";
                    }
                }
                echo "</select>";
            ?>
</div>
<!--               <div class="form-group form-fg">
 -->              <input type="hidden" id="employee" name="employee" class="input-text"   value="<?php echo $row["empname"]; ?>" required>
             
           <!--    </div> -->

<!--             <div class="form-group form-fg">
 -->              <input type="hidden" id="status" name="status" class="input-text"   value="<?php echo $row["status"]; ?>" required>

                <input type="hidden" id="mobail" name="mobail" class="input-text"   value="<?php echo $row["empmobail"]; ?>" required>
                
                 <input type="hidden" id="empcode" name="empcode" class="input-text"   value="<?php echo $row["empcode"]; ?>" required>
          
<!--               </div>
 -->
    <script type="text/javascript">
          $(document).ready(function() {
              $('#branch3').multiselect();
          });
    </script>
