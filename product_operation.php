
<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
    include("aside_main.php");
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <form method="post" action="insert_product.php">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Masters</h1>
    </section>
    <section class="content">
     

      <!-- SELECT2 EXAMPLE -->

     
                <div class="row">
                    <div class="col-md-0"></div> 
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                            <h3 class="box-title">Product Master</h3>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-0"></div> 
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                            <div class="table-responsive">
                                 <br />
                                  <br />
                                  <div id="alert_message"></div>
                                  <table  id="user_data" class="table table-bordered table-striped">
                                   <thead >
                                    <tr>
                                     <th>Id</th>
                                     <th>Product no</th>
                                      <th>Company</th>
                                      <th>Product name</th>
                                      <th>Product type</th>
                                      <th>Unit</th>
                                      <th>Product Group</th>
                                     <th>Sub</th>
                                     <th>Product location</th>
                                     <th>Action</th>
                                   </tr>
                                   </thead>
                                  </table>
                                 </div>
                            </div>
                        </div>
                    </div>  
                </div>



    </section>
    
        
  <br><br><br>
  </div>
</form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"fetch.php",
     type:"POST"
    }
   });
  }
  
  function update_data(id, column_name, value)
  {
   $.ajax({
    url:"update.php",
    method:"POST",
    data:{id:id, column_name:column_name, value:value},
    success:function(data)
    {
     $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
     $('#user_data').DataTable().destroy();
     fetch_data();
    }
   });
   setInterval(function(){
    $('#alert_message').html('');
   }, 5000);
  }

  $(document).on('blur', '.update', function(){
   var id = $(this).data("id");
   var column_name = $(this).data("column");
   var value = $(this).text();
   update_data(id, column_name, value);
  });
  

  
  
  
  $(document).on('click', '.delete', function(){
   var id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this?"))
   {
    $.ajax({
     url:"delete.php",
     method:"POST",
     data:{id:id},
     success:function(data){
      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
  });
 });
</script>

