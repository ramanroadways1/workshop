
<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form action="insert_approve_invoice.php" method="post">
  <div class="content-wrapper">
   
    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
      GRN Detail</h3>
        </div>
         <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
               <?php $username =  $_SESSION['username'];  ?>
              <input type="hidden" name="username" value="<?php echo $username ?>">
              <div class="table-responsive">  
                <table class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                        <td>Product No</td>
                         <td>Product Name</td>
                        <td >Quantity</td>
                        <td>Rate</td> 
                        <td>Amount</td>
                         <td>GST(%)</td> 
                         <td>Gst Amount</td> 
                        <td>Purchase Amount</td>
                         <td>Received Quantity(IN GRN)</td>
                        <td>GRN Amount</td>
                        <td>GRN Date</td>
                       </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $valuetosearch = $conn->real_escape_string(htmlspecialchars($_POST['grn_no1']));

                  // $valuetosearch = $_POST['grn_no1'];
                  $show = "SELECT * FROM grn where  username='$username' and grn_no='$valuetosearch'";
                  $result = $conn->query($show);

                     $sum=mysqli_query($conn,"SELECT SUM(new_total) as new_total,SUM(quantity) as quantity,SUM(received_qty) as received_qty  FROM grn where grn_no='$valuetosearch'");
              $row2=mysqli_fetch_array($sum);
                  if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { $productno = $row['productno'];
                    $productname = $row['productname'];
                    $quantity = $row['quantity'];
                    $rate = $row['rate'];
                    $amount1 = $row['amount'];
                    $amount = round($amount1,2);
                    $gst = $row['gst'];
                    $gst_amount1 = $row['gst_amount'];
                    $gst_amount = round($gst_amount1,2);
                    $total_amount1 = $row['total_amount'];
                     $total_amount = round($total_amount1,2);
                    $received_qty = $row['received_qty'];
                    $new_total = $row['new_total'];
                    $back_date = $row['back_date'];
                  ?> <tr> 
                        <td ><?php echo $productno; ?>
                            <input type="hidden" id="id" name="id[]" value='<?php echo $id; ?>' >
                          </td>
                       <td ><?php echo $productname; ?>
                        <input type="hidden" name="grn_no[]" value='<?php echo $grn_no; ?>'>
                      </td>
                      <td ><?php echo $quantity; ?>
                        <input type="hidden" name="purchase_order[]" value='<?php echo $purchase_order; ?>'>
                      </td> 
                      <td><?php echo $rate; ?>
                        <input type="hidden" name="party_name[]" value='<?php echo $party_name; ?>'>
                      </td>
                      <td><?php echo $amount; ?>
                        <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                      </td>
                        <td><?php echo $gst; ?>
                          <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                        </td>
                         <td><?php echo $gst_amount; ?>
                          <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                        </td>
                        <td><?php echo $total_amount; ?>
                          <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                        </td>
                        <td><?php echo $received_qty; ?>
                          <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                        </td>
                         <td><?php echo $new_total; ?>
                          <input type="hidden" name="productno[]" value='<?php echo $productno; ?>'>
                        </td>
                        <td><?php echo $back_date; ?>
                          <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                        </td>
                      
                    </tr>  
                 <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>
                </table>  
                <!-- <center>
                  <input type="submit" class="btn btn-primary" name="action[]" value="Approved">
                </center> -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

