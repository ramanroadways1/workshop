<?php

  session_start(); 


  if (!isset($_SESSION['userid'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    unset($_SESSION['userid']);
    header("location: login.php");
  }

  require 'connect.php';
// $conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME1);
//  if ( mysqli_connect_errno() ) {
//    die ('Failed to connect to MySQL: ' . mysqli_connect_error());
//  }
   // error_reporting(0);

 $username = $_SESSION['username']; 

 $conn = new PDO( 'mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_NAME1.';', $DATABASE_USER, $DATABASE_PASS );
   
         $statement = $conn->prepare("SELECT GROUP_CONCAT(files_upload.id) as id, grn_no, purchase_order as purchase_order, party_name, key1, count(productno) as productno,  sum(new_total) as new_total, challan_file, invoice_file,timestamp1,emp_data.empname FROM files_upload LEFT JOIN emp_data ON files_upload.employee = emp_data.empcode where approve_status='0' and files_upload.username='$username' and length(invoice_file) > 10 group by grn_no order by timestamp1 ASC "); 

        

   $statement->execute();
   $result = $statement->fetchAll();
   $count = $statement->rowCount();
   $data = array();
   
   foreach($result as $row)
   {

                    $id = $row['id'];
                    $grn_no = $row['grn_no'];
                    $key1 = $row['key1'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                    $productno = $row['productno'];
                    $challan_file = $row['challan_file'];
                    $invoice_file = $row['invoice_file'];
                    $new_total = $row['new_total'];
                    $challan_file1=explode(",",$challan_file);
                    $count3=count($challan_file1);
                    $invoice_file1=explode(",",$invoice_file);
                    $count4=count($invoice_file1);
                    $timestamp1 = $row['timestamp1'];

       $sub_array = array();
   
       // $sub_array[] = date('d/m/Y H:i:s', strtotime($row['TxnTime']));

       // $sub_array[] = $row['id'];
       // $sub_array[] = $row['grn_no'];
       $sub_array[]  = "<form method=\"POST\" action=\"show_detail_approve_invoice.php\">
                                <input type=\"hidden\" name=\"grn_no1\" value='".$row["grn_no"]."'>
                              <input type=\"submit\"  name=\"\" value=\"View\" class=\"btn btn-primary btn-sm\" >
                            </form>";

       $sub_array[] = $grn_no;
       
       $sub_array[] =$row['purchase_order'];
       $sub_array[] = $row['party_name'];
       $sub_array[] = $row['productno'];

      
      
        if (strlen($challan_file) > 0) {
          for($i=0; $i<$count3; $i++){ 
            $dm=$i+1;
           $cha =  "<a href=\"$challan_file1[$i]\" target=\"_blank\">Challan ".$dm."</a>";
          }  
        } else {
           $cha = "no file";
        }


       $sub_array[] = $cha;

      // for($j=0; $j<$count4; $j++){ 
      //     $tik = $j+1;
      //     $inv = '<a href="'.$invoice_file1[$j].'" target="_blank">Invoice '.$tik.'</a>';
      // }

      //  $sub_array[] = $inv;
                      $chalan = array();
                      $copy_no = 0;
                      foreach(explode(",",$row['invoice_file']) as $chalan_file)
                      {
                          $copy_no++;
                          $chalan[] = "<a href='".$chalan_file."' target='_blank'>File: ".$copy_no."</a>";
                      }
                      $sub_array[] = $chalan;


       
       $sub_array[] = $new_total;
       $sub_array[] = $timestamp1;
        $sub_array[] = $row['empname'];
       // $sub_array[] = $row['grn_no'];

       $sub_array[] = '<button type="button"  class="btn btn-success btn btn-sm" onclick="addComplainFunc(\''.$grn_no.'\');" >Approve</button>';

        $sub_array[] = '<input type="button" onclick="DeleteModal(\''.$grn_no.'\')" name="delete" value="Delete" class="btn btn-danger btn btn-sm" />';

       $sub_array[] = "";

      $data[] = $sub_array;
   }
   
   $results = array(
       "sEcho" => 1,
       "iTotalRecords" => $count,
       "iTotalDisplayRecords" => $count,
       "aaData"=>$data);
   
    echo json_encode($results);
    exit;
?>
  