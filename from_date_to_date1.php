
<!DOCTYPE html>
<html>
<head>
  <?php 
   /* include("header.php");
    include("aside_main.php");*/
  ?>
  <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

<script>
function getinvoice(){
      var date = new Date();
      document.getElementById("Invoice").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }
</script>



</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <form  method="post" action="pay_invoice.php" autocomplete="off">
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Payment</h1>
    </section> -->

    <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Detail about Grn</h3>

          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div> -->
        </div>
        <!-- /.box-header -->
       <!--  <div class="box-body"> -->
          <div style="overflow-x:auto;">
              <table class="table table-hover" border="4";>
                <!-- <center><h1 class="h3 mb-0 text-gray-800">Approve Invoice for Payment</h1></center> -->
               <!--  <a href="pay_detail.php">Detail Pay</a><br><br> -->
                <tbody>
                  <tr class="table-active">
                    <th  scope="row">Sno</th>
                      <td>Grn No</td>
                    <td>Party Name</td>
                    <td>Product Number</td>
                    <td>Product Name</td>
                    <td>Quantity</td>
                     <td>Rate/Unit</td>
                    <td>Amount</td>
                    <td>G.S.T (In %)</td>
                    <td>Gst Amount</td>
                     <td>Total Amount</td>
                    <td>Received Qty</td>
                    <td>New Total</td>
                   
                    <!-- <td>Payment</td> -->
                   
                    
                  </tr>
                </tbody>

            <?php
              include 'connect.php';
              $valueToSearch=$_GET['link'];

              $show = "SELECT id, grn_no,productno,party_name,productname,quantity,rate,amount,gst,gst_amount,total_amount,received_qty,new_total FROM grn where grn_no=$valueToSearch";
              $result = $conn->query($show);

               $sum=mysqli_query($conn,"SELECT SUM(new_total) as new_total,SUM(quantity) as quantity,SUM(received_qty) as received_qty  FROM grn where grn_no=$valueToSearch");
              $row2=mysqli_fetch_array($sum);


              if ($result->num_rows > 0) {
                  // output data of each row
                    $id_customer = 1;
                  while($row = $result->fetch_assoc()) {
                      $id = $row['id'];
                      $grn_no = $row['grn_no'];
                      $productno = $row['productno'];
                       $party_name = $row['party_name'];

                      $productname = $row['productname'];
                      $quantity = $row['quantity'];
                      $rate = $row['rate'];

                      $amount = $row['amount'];
                      $gst = $row['gst'];
                      $gst_amount = $row['gst_amount'];

                      $total_amount = $row['total_amount'];
                      $received_qty = $row['received_qty'];
                      $new_total = $row['new_total'];
                     
                      
                      
                  ?>
                <div class="row">
                  <div class="col-md-10">
                    <tr> 
                      <td><?php echo $id_customer; ?></td>
                        <td><?php echo $grn_no; ?></td>
                      <td><?php echo $party_name; ?></td>
                      <td><?php echo $productno; ?></td>
                      <td><?php echo $productname; ?></td>
                      <td><?php echo $quantity; ?></td>
                      <td><?php echo $rate; ?></td>
                      <td><?php echo $amount; ?></td>
                      <td><?php echo $gst; ?></td>
                      <td><?php echo $gst_amount; ?></td>
                      <td><?php echo $total_amount; ?></td>
                      <td><?php echo $received_qty; ?></td>
                      <td><?php echo $new_total; ?></td>
                    </tr></div></div>
                <?php 
                      $id_customer++;
                    }  echo "<tr>

                    <td colspan='4'>Total Calculation : </td>
                    <td colspan='6'>$row2[quantity]</td>
                    <td colspan='1'>$row2[received_qty]</td>
                    <td>$row2[new_total]</td>
                  </tr>";
                  } else {
                       echo "
                    <script>
                    window.location.href='grn_detail_by_date.php';
                    </script>";
                    exit();
                  }
                  ?>

              </table>
            </div>
             <!--  <center>
                <button type="submit" name="submit"color="Primary" class="btn btn-primary">Submit</button>
              </center><br> -->
            <!-- </div> -->
          </div>
        </section>
      </div>
    </form>
      
 <!--  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer> -->

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
