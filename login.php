<?php include('server.php');

 $user_agent = $_SERVER['HTTP_USER_AGENT']; 

  if(!stripos( $user_agent, 'Chrome') !== false)
  { 
      echo "<script>
    alert('Please Use Google Chrome.');
    </script>";
    exit(); 
  }
  
 ?>
<!DOCTYPE html>
<html lang="en" >
<head>

  <meta charset="UTF-8">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="apple-touch-icon" type="image/png" href="https://cpwebassets.codepen.io/assets/favicon/apple-touch-icon-5ae1a0698dcc2402e9712f7d01ed509a57814f994c660df9f7a952f3060705ee.png" />
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico" />

<link rel="mask-icon" type="" href="https://cpwebassets.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg" color="#111" />


  <title>Raman Roadways Pvt. Ltd.</title>
  
  
  <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'>
  
<style>
body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
select{
    /*width: 10%;
    padding: 15px;
    font-size: 16px;
    font-weight: 700; 
    font-family: 'Poppins', sans-serif;
    border: none;
    border-radius:8px;
    border: 2px solid #3f51b5;
    box-shadow: 0 15px 15px #efefef;
  */

  }.menu,
  .content{;

  }
.login-fg .form-container{color:#ccc;position:relative}
.login-fg .login{min-height:100vh;position:relative;display:-webkit-box;display:-moz-box;display:-ms-flexbox;display:-webkit-flex;display:flex;justify-content:center;align-items:center;padding:30px 15px}
.login-fg .login-section{max-width:370px;margin:0 auto;text-align:center;width:100%}
.login-fg .form-fg{width:100%;text-align:center}
.login-fg .form-container .form-group{margin-bottom:25px}
.login-fg .form-container .form-fg{float:left;width:100%;position:relative}
.login-fg .form-container .input-text{font-size:14px;outline:none;color:#616161;border-radius:3px;font-weight:500;border:1px solid transparent;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container img{margin-bottom:5px;height:40px}
.login-fg .form-container .form-fg input{float:left;width:100%;padding:11px 45px 11px 20px;border-radius:50px}
.login-fg .form-container .form-fg i{position:absolute;top:13px;right:20px;font-size:19px;color:#616161}
.login-fg .form-container label{font-weight:500;font-size:14px;margin-bottom:5px}
.login-fg .form-container .forgot{margin:0;line-height:45px;color:#535353;font-size:15px;float:right}
.login-fg .bg{background:rgba(0,0,0,0.04) repeat;background-size:cover;top:0;width:100%;bottom:0;opacity:1;z-index:999;min-height:100vh;position:relative;display:flex;justify-content:center;align-items:center;padding:30px}
.login-fg .info h1{font-size:60px;color:#fff;font-weight:700;margin-bottom:15px;text-transform:uppercase;text-shadow:2px 0px #000}
.login-fg .info{text-align:center}
.login-fg .info p{margin-bottom:0;color:#fff;line-height:28px;text-shadow:1px 1px #000}
.login-fg .form-container .btn-md{cursor:pointer;padding:10px 30px 9px;height:45px;letter-spacing:1px;font-size:14px;font-weight:400;font-family:'Open Sans',sans-serif;border-radius:50px;color:#d6d6d6}
.login-fg .form-container p{margin:0;color:#616161}
.login-fg .form-container p a{color:#616161}
.login-fg .form-container button:focus{outline:none;outline:0 auto -webkit-focus-ring-color}
.login-fg .form-container .btn-fg.focus,.btn-fg:focus{box-shadow:none}
.login-fg .form-container .btn-fg{background:#0f96f9;border:none;color:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container .btn-fg:hover{background:#108ae4}
.login-fg .logo a{font-weight:700;color:#333;font-size:39px;text-shadow:1px 0px #000}
.login-fg .form-container .checkbox{margin-bottom:25px;font-size:14px}
.login-fg .form-container .form-check{float:left;margin-bottom:0}
.login-fg .form-container .form-check a{color:#d6d6d6;float:right}
.login-fg .form-container .form-check-input{position:absolute;margin-left:0}
.login-fg .form-container .form-check label::before{content:"";display:inline-block;position:absolute;width:18px;height:18px;top:2px;margin-left:-25px;border:none;border-radius:3px;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .form-container .form-check-label{padding-left:25px;margin-bottom:0;font-size:14px;color:#616161}
.login-fg .form-container .checkbox-fg input[type="checkbox"]:checked + label::before{color:#fff;background:#0f96f9}
.login-fg .form-container input[type=checkbox]:checked + label:before{font-weight:300;color:#f3f3f3;font-size:14px;content:"\2713";line-height:17px}
.login-fg .form-container input[type=checkbox],input[type=radio]{margin-top:4px}
.login-fg .form-container .checkbox a{font-size:14px;color:#616161;float:right;margin-left:3px}
.login-fg .login-section h3{font-size:20px;margin-bottom:40px;font-family:'Open Sans',sans-serif;font-weight:400;color:#505050}
.login-fg .login-section p{margin:25px 0 0;font-size:15px;color:#616161}
.login-fg .login-section p a{color:#616161}
.login-fg .login-section ul{list-style:none;padding:0;margin:0}
.login-fg .login-section .social li{display:inline-block;margin-bottom:5px}
.login-fg .login-section .social li a{font-size:12px;font-weight:600;width:120px;margin:2px 0 3px;height:35px;line-height:35px;border-radius:20px;display:inline-block;text-align:center;text-decoration:none;background:#fff;box-shadow:0 0 5px rgba(0,0,0,0.2)}
.login-fg .login-section .social li a i{height:35px;width:35px;line-height:35px;float:left;color:#fff;border-radius:20px}
.login-fg .login-section .social li a span{margin-right:7px}
.login-fg .login-section .or-login{float:left;width:100%;margin:20px 0 25px;text-align:center;position:relative}
.login-fg .login-section .or-login::before{position:absolute;left:0;top:10px;width:100%;height:1px;background:#d8dcdc;content:""}
.login-fg .login-section .or-login > span{width:auto;float:none;display:inline-block;background:#fff;padding:1px 20px;z-index:1;position:relative;font-family:Open Sans;font-size:13px;color:#616161;text-transform:capitalize}
.login-fg .facebook-i{background:#4867aa;color:#fff}
.login-fg .twitter-i{background:#3CF;color:#fff}
.login-fg .google-i{background:#db4437;color:#fff}
.login-fg .facebook{color:#4867aa}
.login-fg .twitter{color:#3CF}
.login-fg .google{color:#db4437}
@media (max-width: 1200px) {
.login-fg .info h1{font-size:45px}
}
@media (max-width: 992px) {
.login-fg .bg{display:none}
}
@media (max-width: 768px) {
.login-fg .login-section .social li a{width:100px}
  .login-fg .logo a{font-size:26px;}
}
</style>
 
  <script>
  window.console = window.console || function(t) {};
</script>

  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>
</head>

<body translate="no"  oncontextmenu="return false;">
  <script type="text/javascript">
    document.onkeydown = function(e) {
if(event.keyCode == 123) {
return false;
}
if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
return false;
}
if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){ 
return false;
}
if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
return false;
}
}
  </script>
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 -->  
  <div class="login-fg">
    <div class="container-fluid">
        <div class="row">
         <!--  style="background-image:url('https://source.unsplash.com/user/danielacuevas/1600x900')" -->
     <!--     <div style="background-image: url(../images/test-background.gif); height: 200px; width: 400px; border: 1px solid black;">Example of a DIV element with a background image:</div> -->
            <div class="col-xl-8 col-lg-7 col-md-12 bg" style="background-image: url(img/Mercedes.jpg)" >
                <div class="info">
                    <h1>Raman Roadways PVT. LTD.</h1>
                    <p></p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-12 login">
                <div class="login-section">
<!--                     <div class="menu">
                      <?php include('errors.php'); ?>
                    <select id="name" class="input-text" style="    border-radius: 20px;
                            border-color: #000000;">
                        <option value="">Select Login Option</option>
                        <option value="username">Vendor</option>
                        <option value="superadmin">Workshop </option>
                    </select>
                </div> -->
                    <div class="logo clearfix">
                        <a href="#">
                            RRPL
                        </a>
                    </div>
                    <h3>Sign in Your account</h3>
                  
<!--                    <form method="post" action="../vendor_may/workshop/check_login.php">
                       <div id="superadmin" class="data">
                          <div class="form-container">
                            <div class="form-group form-fg">
                                <input type="text" class="input-text"  name="username" placeholder="Enter Branch Name workshop" autocomplete="off">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="form-group form-fg">
                                <input type="password" class="input-text" name="password" placeholder="Password" autocomplete="off">
                                <i class="fa fa-unlock-alt"></i>
                            </div>
                            <div class="checkbox clearfix">
                            </div>
                            <div class="form-group mt-2">
                                <button type="submit"  class="btn" name="submit">Login</button>
                            </div>
                          </div>
                         </div>
                      </form> -->

                  <form method="post">
                       <div id="username" class="data">
                     <div class="form-container">
                        <form action="#" method="GET">

                            <div class="form-group form-fg">
                                <input type="numbur"  onblur="MyFunc()"  onkeypress="return isNumber(event)"  id="empmobail" name="empmobail" class="input-text" required placeholder="Username/Mobile No" autocomplete="on" >
                                <i class="fa fa-phone"></i>
                            </div>

                            <script type="text/javascript">
                              function isNumber(evt) {
                          evt = (evt) ? evt : window.event;
                          var charCode = (evt.which) ? evt.which : evt.keyCode;
                          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                            alert("Please enter only Numbers.");
                            return false;
                          }

                          return true;
                        }

                        // function ValidateNo() {
                        //   var phoneNo = document.getElementById('txtPhoneNo');

                        //   if (phoneNo.value == "" || phoneNo.value == null) {
                        //     alert("Please enter your Mobile No.");
                        //     return false;
                        //   }
                        //   if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
                        //     alert("Mobile No. is not valid, Please Enter 10 Digit Mobile No.");
                        //     return false;
                        //   }

                        //   alert("Success ");
                        //   return true;
                        // }
                            </script>

                              <div id="result22"></div>
                               <script>
                                      function MyFunc()
                                      {
                                         var empmobail = $('#empmobail').val();
                                        if(empmobail!='')
                                        {
                                          $.ajax({
                                              type: "POST",
                                              url: "chkempmob.php",
                                              data:'empmobail='+empmobail ,
                                              success: function(data){
                                                  $("#result22").html(data);
                                              }
                                              });
                                        }
                                      }     
                                  </script>
                            <script type="text/javascript">
                                  var val = empmobail
                                    if (/^\d{10}$/.test(val)) {
                                    // value is ok, use it
                                    } else {
                                    alert("Invalid number; must be ten digits")
                                    return false
                                  }
                            </script>
<!--                             <div class="form-group form-fg">
                                <input type="text" id="user" name="username" class="input-text" placeholder="Enter User Name" autocomplete="off" required>
                                <i class="fa fa-user"></i>
                            </div> -->
                            <div class="form-group form-fg">
                                <input type="password" name="password" class="input-text" placeholder="Password" autocomplete="off" required>
                                <i class="fa fa-unlock-alt"></i>
                            </div>
                            <div class="checkbox clearfix">
                                <!-- <a href="#">Forgot Password</a> -->
                            </div>
                            <div class="form-group mt-2">
                                <button type="submit" id="submit" class="btn" name="login_user">Login</button>
                            </div>
                        </form>
                       </div>
                     </div>
                  </form>
                    <!-- <p>Don't have an account? <a href="#" class="linkButton"> Register</a></p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>

      <script id="rendered-js" >
// Used bootstrap v4.5, font awesome v4.7.0 and unsplash api | free beautiful images
//# sourceURL=pen.js

    </script>
    <script>
  $(document).ready(function(){
    $("#name").on('change',function(){
      $(".data").hide()
      $("#" + $(this).val()).fadeIn(700);
    }).change();
  });

</script>
<script type="text/javascript">
  $('#button-b').click(function(){
    Swal("hello demo")
    
  });
</script>
</body>
</html>

