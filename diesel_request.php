<!DOCTYPE html>
<html>
<head>
  <title>Diesel Request</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
    <h2>Diesel Request</h2>
  </div>
   
  <form method="post" action="insert_diesel_req.php" autocomplete="off" >
    <div class="input-group">
      <label>Card No.</label>
      <input type="text" name="card_no" class="form-control" required/>
    </div>
    <div class="input-group">
      <label>Date</label>
      <input id="date" name="date" onblur="date1();" class="form-control" required/>
      <script type="text/javascript">
        function date1(){
        var date = new Date();
        document.getElementById("date").value = (date.getDate()<10?'0':'') + date.getDate() +"/"+ (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+"/"+ date.getFullYear() ;
      }
      </script>
    </div>
    <div class="input-group">
      <label>Amount</label>
      <input type="text" name="amount" class="form-control" required/>
    </div>
    <div class="input-group">
      <label>Fuel Company</label>
      <select class="form-control" name="company" style="width: 98%;height: 40px;" class="form-control">
          <option value="">----Select----</option>
          <option value="IOCL">IOCL</option>
          <option value="HPCL">HPCL</option>
          <option value="BPCL">BPCL</option>
          <option value="RELIANCE">RELIANCE</option>
      </select>
    </div>
    <div class="input-group">
      <button type="submit" class="btn" name="login_user">Submit</button>
      <a href="show_diesel_req.php" class="btn btn-primary">Upload</a>
    </div>
  </form>
</body>
</html>