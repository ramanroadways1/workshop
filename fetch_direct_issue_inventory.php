<?php
session_start();

include "connect.php";
$username =  $_SESSION['username'];


$columns = array('grn_no', 'party_name', 'productno', 'timestamp1');

$query = "SELECT grn_no,count(productno) as productno,party_name,timestamp1 FROM files_upload where type_of_issue !='' and username='$username' and ";

if($_POST["is_date_search"] == "yes")
{
 $query .= 'back_date BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}

if(isset($_POST["search"]["value"]))
{
 $query .= '
  (productno LIKE "%'.$_POST["search"]["value"].'%" 
   OR grn_no LIKE "%'.$_POST["search"]["value"].'%" 
  OR party_name LIKE "%'.$_POST["search"]["value"].'%" 
  OR timestamp1 LIKE "%'.$_POST["search"]["value"].'%" )
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'GROUP BY grn_no DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<a href="show_direct_issue_inventory.php?link='.$row["grn_no"].'" target="_blank" class="btn btn-xs fa fa-eye"></a>';
 $sub_array[] = $row["grn_no"];
 $sub_array[] = $row["party_name"];
 $sub_array[] = $row["productno"];
 $sub_array[] = $row["timestamp1"];
 
 $data[] = $sub_array;
}

function get_all_data($conn)
{
	$username =  $_SESSION['username'];
 $query = "SELECT grn_no,count(productno) as productno,party_name,timestamp1 FROM files_upload where type_of_issue !='' AND username='$username'";
 $result = mysqli_query($conn, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data" => $data
);

echo json_encode($output);
?>