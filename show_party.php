<!DOCTYPE html>
<html>
<head>
  <?php
  include("connect.php");?>
  <?php include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Party Detail</h3>

      </div>
  
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                          <td>Party Name</td>
                          <td>Party Code</td>
                          <td>Party Location</td>
                          <td>Mobile No.</td>
                          <td>Address</td>
                          <td>Contact Person</td>
                          <td>PAN No.</td>
                          <td>Gst Applicable.</td>
                          <td>GSTIN No.</td>
                          <td>GSTIN Type</td>
                          <td>Bank</td>
                          <td>Branch</td>
                          <td>Account No.</td>
                          <td>Account Holder Name</td>
                          <td>IFSC Code</td>
                           <td>Time And Date</td>
                       </tr>  
                  </thead>  
                 
                  <?php  
                   include("connect.php");
                     $username =  $_SESSION['username'];
                     $sql = "SELECT * from party ";
                     $result = $conn->query($sql);
                   
                      while($row = mysqli_fetch_array($result))  
                      {   echo '  
                       <tr>  
                            <td>'.$row["party_name"].'</td>
                            <td>'.$row["party_code"].'</td>
                            <td>'.$row["party_location"].'</td>
                            <td>'.$row["mobile_no"].'</td>
                            <td>'.$row["address"].'</td>
                            <td>'.$row["contact_person"].'</td>
                            <td>'.$row["pan"].'</td>
                            <td>'.$row["yes_no"].'</td>
                            <td>'.$row["gstin"].'</td>
                            <td>'.$row["gst_type"].'</td>
                            <td>'.$row["bank"].'</td>
                            <td>'.$row["branch"].'</td>
                            <td>'.$row["acc_no"].'</td>
                            <td>'.$row["acc_holder_name"].'</td>
                            <td>'.$row["ifsc_code"].'</td>
                              <td>'.$row["date_time"].'</td>
                       </tr>  
                       ';  
                    }  
                  ?>  
                </table>  
              </div>  
            </div>  
          </div>

        </div>
 
      </div>
     
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 


