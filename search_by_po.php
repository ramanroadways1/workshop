
<!DOCTYPE html>
<html>
<head>
 <?php 
    include("header.php");
    include("aside_main.php");
  ?>
  <script>
   
    function getpo(){
      var date = new Date();
      document.getElementById("purchase_order").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes()  + (date.getSeconds()<10?'0':'') + date.getSeconds();
    }

     function autodate(){
      var date = new Date();
      document.getElementById("d").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() 
      
    }

    function getrate() {
      var rate = document.getElementById("rate").value;
      var quantity = document.getElementById("quantity").value;
      var amount = document.getElementById("amount").value;
       
      if(!(rate==0))
       {
        if(!(quantity==0))
        {
           document.getElementById("amount").value=rate*quantity;
        }
      }
      else if(!(quantity==0))
         {
          if(!(amount==0))
          {
             document.getElementById("rate").value=amount/quantity;
          }
         }
      }

    function getgst()
    {
        var amount1 = document.getElementById("amount").value;
        var gst1=document.getElementById("gst").value;
        var total_gst1=document.getElementById("gst_amount").value;
        if(!(amount1==0))
        {
          if(!(gst1==0))
          {
            document.getElementById("gst_amount").value=amount1*gst1/100;
          }
          else if(!(total_gst1==0))
          {
            document.getElementById("gst").value=total_gst1*100/amount1;
          }
        }

    }


    function gettotal(){
      var gst_amount = document.getElementById("gst_amount").value;
      var amount = document.getElementById("amount").value;
      document.getElementById("total_amount").value=parseInt(amount)+parseInt(gst_amount);
    }

     function get_gst()
    {
        var gst_value1=document.getElementById("gst").value;
        var gst_type1= document.getElementById('gst_type').value;
        if(gst_type1=="cgst_sgst"){
            var result = gst_type1+ "," +gst_value1/2;
          }else
          {
            var result = gst_type1+ "," +gst_value1;
          }
          //var result= parseInt(gst_value1)*parseInt(gst_type1);
          document.getElementById('sel_gst').value=result;
          
    }

    function myFunction1() {

     var v1 = document.getElementById("a"+id_customer).value;
     
    
     $('#productno').val(v1); 
     
     var v3 = document.getElementById("c"+id_customer).value;
     $('#rate_master').val(v3);

      var date1 = new Date();
     document.getElementById("key").value = (date.getDate()<10?'0':'') + date.getDate() + (date.getMonth()<10?'0':'') + (date.getMonth() + 1)+ date.getFullYear() + "-"
      + (date.getHours()<10?'0':'') + date.getHours()  + (date.getMinutes()<10?'0':'') + date.getMinutes(); 
      alert(date);
   }
 </script>

 <style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Search Purchase Order</h1>
    </section>

    <section class="content">
     
    <!-- <div class="row">
        <div class="col-md-0"></div> 
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                <h3 class="box-title">Search Purchase Order</h3>
                </div>
            </div>
        </div>  
    </div> -->

    <form id="MyForm1">
      <div class="row">
        <div class="col-md-0"></div> 
          <div class="col-md-12">
              <div class="box box-default">
                  <div class="box-header with-border">
                    <div class="col-md-8">
                      <div class="form-group">
                        <div class="col-md-2">
                            <b>Purchase Order</b>
                        </div>
                        <div class="col-md-8">
                          <input type="text" autocomplete="off" id="po_to_search" name="po_to_search" placeholder="Enter Purchase Order..." class="form-control" required />
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-search" ><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                     <!--  <div align="right">
                        <a href='../party.php' type="submit" name="add" id="add" class="btn btn-info">Add</a>
                      </div> -->
                    </div> 
                  </div>
              </div>
          </div> 
      </div>
    </form>

      <br>

      <div class="table-responsive">
           <div id="result"></div> 
      </div>
    </section>
  </div>
</div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>

<script type="text/javascript">     
$(function()
{    
$( "#po_to_search" ).autocomplete({
source: 'fetch_po_autocomplete.php',
change: function (event, ui) {
if(!ui.item){
$(event.target).val(""); 
$(event.target).focus();
/*alert('Party No does not exists.');*/
} 
},
focus: function (event, ui) {  return false;
}
});
});
</script>

<script>
$(document).ready(function (e) {
$("#MyForm1").on('submit',(function(e) {
e.preventDefault();
    $.ajax({
    url: "fetch_search_po.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data)
    {
        $("#result").html(data);
    },
    error: function() 
    {} });}));});
</script> 