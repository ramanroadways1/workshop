<?php
//fetch.php
include('connect.php');
$columns = array('id', 'productno');

$query = "SELECT * FROM grn ";

if(isset($_POST["search"]["value"]))
{
 $query .= '
 WHERE purchase_order LIKE "%'.$_POST["search"]["value"].'%" 
 OR productno LIKE "%'.$_POST["search"]["value"].'%" 
 OR party_name LIKE "%'.$_POST["search"]["value"].'%"
 
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="id">' . $row["id"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="purchase_order ">'.$row["purchase_order"].'</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="productno">' . $row["productno"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="party_name">' . $row["party_name"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="quantity">' . $row["quantity"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="total">' . $row["total"] . '</div>';
 /*$sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="location">' . $row["location"] . '</div>';*/
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="challan_file">' . $row["challan_file"] . '</div>';
 $sub_array[] = '<div contenteditable class="update" data-id="'.$row["id"].'" data-column="invoice_file">' . $row["invoice_file"] . '</div>';
 

 $sub_array[] = '<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row["id"].'">Delete</button>';
 
 $data[] = $sub_array;
}

function get_all_data($conn)
{
 $query = "SELECT * FROM product";
 $result = mysqli_query($conn, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
