
<!DOCTYPE html>
<html>
<head>
  <?php

     include("header.php"); ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('manager_aside.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <form action="insert_approve_invoice.php" method="post">
  <div class="content-wrapper">

    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
         Payment Approval</h3>
      </div>
	   <div id="result11"></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
               <?php $username =  $_SESSION['username'];?>
              <input type="hidden" name="username" value="<?php echo $username ?>">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered">  
                  <thead>  
                       <tr>  
                        
                        <th style="display: none;">ID</th>
                         <td>Grn No</td>
                        <td style="display: none;">GRN No</td>
                        <td style="display: none;">Purchase Order</td> 
                        <td>Party Name</td>
                         <td>Comapany</td>
                         <td style="width: 100px;">Account Detail</td>
                        <td>Other Amount</td>
                         <td>Payment</td>
                         <td>Goods Date(GRN)</td>
                           <td>Payment Date</td>
                        <td>Invoice Bill</td>
                        <td style="display: none;">Time</td>
                         <td>approve</td>
                          <td>Delete</td>
                       </tr>  
                  </thead>  
                  <?php  
                  include("connect.php");
                  $show = "SELECT * FROM insert_pay_invoice where  username='$username' and length(invoice_file) > 10  and approve='0' group by grn_no order by id asc";
                  $result = $conn->query($show);
                  if ($result->num_rows > 0) {
                   $id_customer = 0;
                  while($row = $result->fetch_assoc()) 
                  { $id = $row['id'];
                    $grn_no = $row['grn_no'];
                    $purchase_order = $row['purchase_order'];
                    $party_name = $row['party_name'];
                     $acc_holder_name = $row['acc_holder_name'];
                    $mobile_no = $row['mobile_no'];
                    $pan = $row['pan'];
                    $gstin = $row['gstin'];
                    $bank_name = $row['bank_name'];
                    $payment = $row['payment'];
                     $remark = $row['remark'];
                    $payment_date = $row['payment_date'];
                    $other_amount = $row['other_amount'];
                    $invoice_file = $row['invoice_file'];
                    $invoice_file1=explode(",",$invoice_file);
                    $count4=count($invoice_file1);
                    $timestamp1 = $row['timestamp1'];
                    //echo $timestamp1;
                     $grn_date = $row['grn_date'];
                     $company = $row['company'];
                    $acc_no = $row['acc_no'];
                    $ifsc_code = $row['ifsc_code'];
                  ?>

                       <tr> 
                         <td>
                            <input type="submit" formaction="manager_approval_page_detail.php"  name="grn_no1" value="<?php echo $grn_no; ?>" class="btn btn-primary btn-sm"> 
                          </td>
                        <td style="display: none;"><?php echo $id; ?>
                            <input type="hidden" name="id" id="id<?php echo $id; ?>" value='<?php echo $id; ?>' >
                        </td>
                       <td style="display: none;"><?php echo $grn_no; ?>
                        <input type="hidden" name="grn_no" id="grn_no<?php echo $id; ?>" value='<?php echo $grn_no; ?>'>
                      </td>
                      <td style="display: none;"><?php echo $purchase_order; ?>
                        <input type="hidden" name="purchase_order" value='<?php echo $purchase_order; ?>'>
                      </td> 
                      <td><?php echo $party_name; ?>
                        <input type="hidden" name="party_name" value='<?php echo $party_name; ?>'>
                      </td>

                       <td><?php echo $company; ?>
                        <input type="hidden" id="company<?php echo $id; ?>" name="company" value='<?php echo $company; ?>'>
                      </td>
                     
                         <td>
                          Bank:<?php echo $bank_name; ?>
                          <input type="hidden" id="bank_name<?php echo $id; ?>" name="bank_name" value="<?php echo $bank_name ?>">
                            <?php  echo "<br>"; ?>
                             AccNO:<?php echo $acc_no; ?>
                              <input type="hidden" id="acc_no<?php echo $id; ?>" name="acc_no" value="<?php echo $acc_no ?>">
                          <input type="hidden" name="acc_holder_name" id="acc_holder_name<?php echo $id; ?>" value="<?php echo $acc_holder_name ?>">
                            <?php  echo "<br>"; ?>
                         Mobile No:<?php echo $mobile_no; ?>
                          <input type="hidden" name="mobile_no" id="mobile_no<?php echo $id; ?>" value="<?php echo $mobile_no ?>">
                         
                           <input type="hidden" name="pan" id="pan<?php echo $id; ?>" value="<?php echo $pan ?>">
                           
                        IFSC:<?php echo $ifsc_code; ?>
                         <input type="hidden" name="gstin" id="gstin<?php echo $id; ?>" value="<?php echo $gstin ?>">
                          <input type="hidden" name="ifsc_code" id="ifsc_code<?php echo $id; ?><?php echo $grn_no; ?>" value="<?php echo $ifsc_code ?>">
                           
                        <input type="hidden" name="productno" value='<?php echo $productno; ?>'>
                      </td>
                       
                         <td>Other:<?php echo $other_amount; ?>
                          <input type="hidden" name="other_amount" value="<?php echo $other_amount ?>">
                         
                         <?php echo "<br>"; ?>
                       Remark:<?php echo $remark; ?>

                        <input type="hidden" name="remark" value='<?php echo $remark; ?>'>
                      </td>
                       <td><?php echo $payment; ?>

                        <input type="hidden" name="payment" id="payment<?php echo $id; ?>" value='<?php echo $payment; ?>'>
                      </td>

                       <td><?php echo $grn_date; ?>
                        <input type="hidden" name="grn_date" id="grn_date<?php echo $id; ?>" value='<?php echo $grn_date; ?>'>
                      </td>

                       <td><?php echo $payment_date; ?>
                        <input type="hidden" name="payment_due_date" id="payment_due_date<?php echo $id; ?>" value='<?php echo $payment_date; ?>'>
                      </td>
                      
                      <td>
                         <?php
                          if (strlen($invoice_file) > 15) {
                         for($j=0; $j<$count4; $j++){
                         
                          ?>
                           <a href="<?php echo $invoice_file1[$j]; ?>" target="_blank">Invoice <?php echo $j+1; ?></a>
                           <input type="hidden" name="invoice_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $invoice_file; ?>'>
                           <?php
                         }
                          }  
                     else{
                      echo "no file";
                     }

                        ?>
                        <!-- <a href="<?php echo $invoice_file; ?>" target="_blank"><?php echo $invoice_file; ?></a>
                        <input type="hidden" name="invoice_file[]" value='<?php echo $invoice_file; ?>'> -->
                      </td>
                    
                      <td  style="display: none;"><?php echo $timestamp1; ?>
                        <input type="hidden" name="timestamp1[]" value='<?php echo $timestamp1; ?>'>
                      </td>
                     <!--  <td>
                         <button type="button"  class="btn btn-success btn btn-sm" onclick="addComplainFunc('<?php echo $grn_no; ?>');" >Approve</button>
                      </td> -->
                       <?php
                      $sql21 = "SELECT * FROM insert_pay_invoice  WHERE  grn_no='".$grn_no."' and username = '$username' and approve='0'";
                      //echo $sql21;echo "<br>";

                     $result21 = $conn->query($sql21);
                    //$row21 = mysqli_fetch_array($result21);
                   ?>
               <?php if(mysqli_num_rows($result21) > 0){ 
                $date_now = date("d-m-Y");
                 //echo $date_now;

                if ($date_now > $payment_date) {
                  //echo "hi";
                ?>
                   <td>
                     <button type="button" onclick="addComplainFunc('<?php echo $id; ?>');" class="btn btn-danger btn-sm" >Approve immed.</button>
                    </td>

                <?php } else{
                  ?>
                   <td>
                     <button type="button" onclick="addComplainFunc('<?php echo $id; ?>');" class="btn btn-success btn-sm" >Approve</button>
                    </td>
                  <?php
                 } 
               }
                  ?>
                     <td>
                      <input type="button" onclick="DeleteModal('<?php echo $grn_no;?>')" name="delete" value="Delete" class="btn btn-danger btn btn-sm" />
                    </td> 

                     <script type="text/javascript">
                        function addComplainFunc(val)
                                {
                                   var id = val;
                                   var grn_no =  $('#grn_no'+val).val();
                                   var acc_no = $('#acc_no'+val).val();
                                   var acc_holder_name = $('#acc_holder_name'+val).val();
                                   var pan = $('#pan'+val).val();
                                   var grn_date = $('#grn_date'+val).val();

                                   var payment_due_date =  $('#payment_due_date'+val).val();
                                  
                                   var payment = $('#payment'+val).val();
                                   var company = $('#company'+val).val();
                                    var bank_name = $('#bank_name'+val).val();
                                   var ifsc_code = $('#ifsc_code'+val).val();
                                    var mobile_no = $('#mobile_no'+val).val();
                                   var gstin = $('#gstin'+val).val();
                                 
                                   var username = '<?php echo $username ?>';
                                 if (confirm("Are You Sure To Approve this GRN?"))
                                   {
                                      $.ajax({
                                         type:"post",
                                         url:"payment_final_approve.php",
                                        data:'grn_no='+grn_no + '&username='+username+ '&acc_no='+acc_no+ '&acc_holder_name='+acc_holder_name+ '&pan='+pan+ '&grn_date='+grn_date+ '&payment_due_date='+payment_due_date+ '&payment='+payment+ '&company='+company+ '&bank_name='+bank_name+ '&ifsc_code='+ifsc_code+ '&mobile_no='+mobile_no+ '&gstin='+gstin+ '&id='+id,
                                          success:function(data){
                                           $('#result11').html(data);
                                         
                                            }
                                          });
                                      
                                     }
                                 }
                    </script>

                    <script>
                      function DeleteModal(grn_no)
                      {
                        var grn_no = grn_no;
                        //alert(grn_no);
                         var username = '<?php echo $username ?>'
                       
                        if (confirm("Do you want to delete this grn?"))
                        {
                          if(grn_no!='')
                          {
                            $.ajax({
                                  type: "POST",
                                  url: "delete_grn_from_payment.php",
                                  data:'grn_no='+grn_no + '&username='+username ,
                                  success: function(data){
                                    alert(data)
                                           location.reload(); 
                                      $("#result22").html(data);
                                  }
                              });
                          }
                        }
                      }     
                    </script>
                    <div id="result22"></div>
                    </tr>  
                 <?php 
                      $id_customer++;
                    } 
                  } else {
                      echo "0 results";
                  }
                  ?>
                </table>  
               <!--  <center>
                  <input type="submit" class="btn btn-primary" name="action[]" value="Approved">
                </center> -->
              </div>  
            </div>  
          </div>
              <!-- /.table-responsive -->
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  </form>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
    
  </footer>

<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

