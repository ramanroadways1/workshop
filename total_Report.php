<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php 
    include("aside_main.php");
  ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        All Reports 
        <small>Control panel</small>
      </h1>
    </section>
    <section class="content">
          <?php 
          $username =  $_SESSION['username'];
          ?>
      <div class="row">
        <!-- <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-yellow"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="show_product_master.php">product(Master)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT productno FROM product ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>
            
          </div>
         
        </div>
 -->



<!-- 
          <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="icon ion-android-menu"></i></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="show_product.php">product(Inventory)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT productno FROM product_inventory ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>

          </div>
   
        </div> -->
<!-- 
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="icon ion-ios-person-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total<a href="show_party.php">party(master)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT party_name FROM party";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              
              </span>
            </div>
           
          </div>
         
        </div> -->
        
        

       <!--  <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="grn_detail_by_date.php">grn Report</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT grn_no FROM grn   group by grn_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
        
          </div>
         
        </div> -->


        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-red"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="pro_grn_detilsmanin.php">GRN AND PO REPORT</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

             /* $sql="SELECT grn_no FROM grn where username='$username'  group by grn_no";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);*/
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    
<!--          <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-blue"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="direct_issue_grn_inventory.php">Direct Issue Grn</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql3="SELECT * FROM files_upload where type_of_issue !='' GROUP by grn_no";

              if ($result3=mysqli_query($conn,$sql3))
                {
                $rowcount=mysqli_num_rows($result3);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result3);
                }
              mysqli_close($conn);
              ?><br>
              </span>
            </div>
          </div>
        </div> -->

        
      </div>
    </section>

  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
