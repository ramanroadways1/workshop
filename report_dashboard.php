<!DOCTYPE html>
<html>
<head>
  <?php 
    include("header.php");
  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php 
    include("aside_main.php");
  ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Reports
        <small>Control panel</small>
      </h1>
    </section>
    <section class="content">
          <?php 
          $username =  $_SESSION['username'];
          ?>
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-yellow"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="pending_job.php">Pending(Job Card)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT job_card_no FROM external_job_cards ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>
           
          </div>
        
        </div>


          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="icon ion-android-menu"></i></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="part_report.php">Party(Lazer)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT party_name FROM party ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

<!--         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="icon ion-ios-person-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total<a href="show_party.php">party(master)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT party_name FROM party";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }

              mysqli_close($conn);
              ?>
              
              </span>
            </div>
           
          </div>
         
        </div> -->
        
        

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-aqua"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="product_list.php">Product Report</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql="SELECT productname FROM product  ";

              if ($result=mysqli_query($conn,$sql))
                {
                $rowcount=mysqli_num_rows($result);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
             <span class="info-box-icon bg-red"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total <a href="truck_workshop.php">Trucks(Job Card)</a></span>
              <span class="info-box-number">
              <?php
              include('connect.php');
              if (mysqli_connect_errno())
                {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

              $sql3="SELECT truck_no2 FROM external_job_cards_main ";

              if ($result3=mysqli_query($conn,$sql3))
                {
                $rowcount=mysqli_num_rows($result3);
                echo "<center>";
                echo $rowcount;
                mysqli_free_result($result3);
                }
              mysqli_close($conn);
              ?><br>
             
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

   

 

        
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

</body>
</html>
