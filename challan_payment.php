<!DOCTYPE html>
<html>
<head>
  <?php include("header.php"); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
     <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
     <?php include('aside_main.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper"> 
  <!-- <form action="approve_po_insert.php" method="post"> -->
  <div class="content-wrapper">
    <!-- <section class="content-header">
      <h1>Purchase Order Detail</h1>
    </section> -->

    <section class="content">
    
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">
      Upload Invoice Over GRN</h3>
        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div> -->
      </div>

      <form id="temp_form" action="show_detail_invoice_over_grn.php" method="post"></form>
      <form id="main_form" action="approve_po_insert.php" method="post"></form>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-0"></div> 
            <div class="col-md-12">
              <div class="table-responsive">  
                <table id="employee_data" class="table table-striped table-bordered" style="width: 100%; font-family: verdana; font-size: 12px;">  
                  <?php $username =  $_SESSION['username'];
                   ?>
                  <thead>  
                       <tr>  
                         <th style="display: none;" scope="row">Id</th>
                                <td>GRN Number</td>
                                <td>Purchase Order</td>
                                <td>party name</td>
                                <td>product name</td>
                                <td>Total Amount</td>
                                <td>Challan File</td>
                                <!-- <td>Invoice Upload</td> -->
                                <!--  <td>Invoice No</td> -->
                                  <td>Submit</td>
                       </tr>  
                  </thead>  
                              <?php  
                               include("connect.php");
                                  $username =  $_SESSION['username']; 
                                  //$party_name = $_POST['party_name'];
                                  $query = "SELECT GROUP_CONCAT(id) as id, party_name, grn_no,key1, count(productno) as productno, purchase_order, sum(new_total) as new_total, challan_file,challan_no FROM files_upload WHERE  username='$username' AND length(challan_file) > 10 and length(invoice_file) < 10 and approve_status='0' group by grn_no";
                                 
                                  $result = mysqli_query($conn,$query);
                                   $id_customer = 0;

                                   if(mysqli_num_rows($result) > 0){

                                   while($row = mysqli_fetch_array($result)){
                                   $id = $row['id'];
                                   $challan_no = $row['challan_no'];
                                   $key1 = $row['key1'];
                                   $grn_no=$row['grn_no'];
                                   $party_name=$row['party_name'];
                                   $productno=$row['productno'];
                                   $purchase_order =$row['purchase_order'];
                                   $new_total=$row['new_total'];
                                   $challan_file = $row['challan_file'];
                                   $challan_file1=explode(",",$challan_file);
                                   $count3=count($challan_file1);
                              ?>
                       <tr> 
                            <td style="display: none;"><?php echo $id; ?><input type="hidden" name="id[]" id="id<?php echo $id_customer; ?>" value='<?php echo $id; ?>'></td>

                                  <!--   <td> <?php echo $grn_no; ?><input type="hidden" name="grn_no[]"  id="gn<?php echo $id_customer; ?>" value='<?php echo $grn_no; ?>'></td> -->
                                    <td>
                                       <input type="submit" class="btn btn-primary btn-sm" name="grn_no" form="temp_form"  value="<?php echo $grn_no?>" >
                                          <input type="hidden" id="grn_no<?php echo $grn_no?>" value="<?php echo $grn_no?>">
                                    </td>

                                    <td> <?php echo $purchase_order; ?><input type="hidden" name="purchase_order[]"  id="po<?php echo $id_customer; ?>" value='<?php echo $purchase_order; ?>'></td>

                                    <td><?php echo $party_name; ?><input type="hidden" name="party_name[]" id="pn<?php echo $id_customer; ?>" value='<?php echo $party_name; ?>'></td>

                                    <td> <?php echo $productno; ?><input type="hidden" name="productno[]"  id="pno<?php echo $id_customer; ?>" value='<?php echo $productno; ?>'></td>

                                    <td> <?php echo $new_total; ?><input type="hidden" name="new_total[]"  id="nt<?php echo $id_customer; ?>" value='<?php echo $new_total; ?>'></td>


                                     <td>
                                     <p>Challan No: <?php echo $challan_no; echo "<br>"; ?></p>
                                     <?php
                                       for($i=0; $i<$count3; $i++){
                                        ?>
                                         <a href="<?php echo $challan_file1[$i]; ?>" target="_blank">File <?php echo $i+1; ?></a>
                                         <input type="hidden" name="challan_file[]" id="cf<?php echo $id_customer; ?>" value='<?php echo $challan_file; ?>'>
                                         <?php
                                       }
                                      ?>
                                     </td>

                                    <!-- <td>
                                      <input type="radio" name="id_customer[]" id="i<?php echo $id_customer; ?>" onclick="myFunction1(<?php echo $id_customer; ?>)" value='<?php echo $id_customer; ?>'> 
                                      <input type="file" id="file<?php echo $grn_no; ?>" name="file[]" multiple="multiple" onclick="invoice_num();" required/>
                                    </td> -->
                                    <!-- <td style="width: 74px;">
                                      
                                    </td> -->
                                    <td>
                                       <form method="POST" action="insert_challan.php"  enctype="multipart/form-data">
                                        <input type="text"  id="invoice_no<?php echo $grn_no; ?>" placeholder="Invoice No" oninput="addInvoiceFunc('<?php echo $grn_no; ?>')" name="invoice_no" autocomplete='off'  required> <br><br>
                                        
                                        <input type="file" id="file" name="file[]" multiple="multiple"  required/>

                                         <input type="hidden" name="username" value="<?php echo $username; ?>">

                                        <br>
                                         <input type="hidden" name="id[]"  value='<?php echo $id; ?>'>
                                      <input type="hidden" name="grn_no"  value='<?php echo $grn_no; ?>'>
                                      <input type="submit" class="btn btn-success btn-sm" name="submit" value="submit">
                                    </form>
                                    </td>
                       </tr>  
                     <?php   
                      $id_customer++;
                      $i++;
                    }
                  } else {
                      
                  }
                  ?>  
                </table>  
                        <!--  <script>
                            $(document).ready(function (e) {
                            $("#MyForm1").on('submit',(function(e) {
                            e.preventDefault();
                                $.ajax({
                                url: "insert_challan.php",
                                type: "POST",
                                data:  new FormData(this),
                                contentType: false,
                                cache: false,
                                processData:false,
                                success: function(data)
                                {
                                    alert(data)
                                    $("#r1").html(data);
                                },
                                error: function() 
                                {} });}));});
                           </script> -->
                           <div id="r1"></div> 
                            
                            <script type="text/javascript">
                                function addInvoiceFunc(elem)
                                        {
                                       /* var file=document.getElementById('file'+val).value;
                                        alert(file);*/
                                           var grn_no = $('#grn_no'+elem).val();
                                         //alert(grn_no);
                                        var invoice_no= $('#invoice_no'+elem).val();
                                        //alert(invoice_no)
                                        $('#invoice_no1'+elem).val(invoice_no);

                                          //var grn_no = val;
                                         
                                          //var username = '<?php echo $username ?>'
                                           /* $.ajax({
                                               type:"post",
                                               url:"insert_challan.php",
                                              data:'grn_no='+grn_no + '&username='+username+ '&file='+file+  '&invoice_no='+invoice_no,
                                                success:function(data){
                                                   alert(data)
                                                   location.reload(); 
                                                        
                                                  }
                                              });*/
                                         }
                            </script>

                <center>
                 <!--  <input type="submit" class="btn btn-primary" form="main_form" name="action[]" value="Approved"> -->
                </center><br>
              </div>  
            </div>  
          </div>
            
          </div>
              
        </div>
            <!-- /.box -->
      </section>
    </div>
  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2
    </div>
    <strong>Copyright &copy; 2021 <a href='#'>Raman Roadways Pvt Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
 <script>  
 $(document).ready(function(){  
      $('#employee_data').DataTable();  
 });  
 </script> 

